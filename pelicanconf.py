#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'neurone'
SITENAME = u'Il neurone proteso'
SITEURL = 'https://neurone.gitlab.io/'

PATH = 'content'
STATIC_PATHS = ['images', 'pdfs', 'misc']
EXTRA_PATH_METADATA = {'images/favicon.ico': {'path': 'favicon.ico'},}
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = u'it'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Anarcho-Transhuman', 'http://anarchotranshuman.org/'),
         ('Boing Boing: Cory Doctorow', 'https://boingboing.net/author/cory_doctorow_1'),
         ('Charlie\'s Diary', 'http://www.antipope.org/charlie/blog-static/'),
         ('Cuore, cervello e altre frattaglie', 'http://dottornomade.com/'),
         ('Drugs and Wires', 'http://www.drugsandwires.fail/'),
         ('Human Iterations', 'http://humaniterations.net/'),
         ('Linux Journal', 'https://www.linuxjournal.com/'),
         ('Neon Dystopia', 'https://www.neondystopia.com/'),
         ('pouët.net', 'https://www.pouet.net/'),
         ('Some1elsenotme', 'https://some1elsenotme.wordpress.com/'),
         ('The 8bit Guy', 'https://www.youtube.com/channel/UC8uT9cgJorJPWu7ITLGo9Ww'),)

# Social widget
SOCIAL = (('GitLab', 'https://gitlab.com/neurone'),
          ('GitHub', 'https://github.com/Tichy'),)


DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
