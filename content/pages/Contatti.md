title: Contatti
date: 2013-10-27 12:21

Potete scrivetemi all'indirizzo <tichy@_toglimi_cryptolab.net>, togliendo il
*\_toglimi\_*.

Se volete scrivermi una mail criptata, potete usare
[la mia chiave pubblica]({filename}/misc/tichy.asc).

Ho anche un contatto Jabber e mi si puo` trovare su IRC. Chiedetemeli per email.
