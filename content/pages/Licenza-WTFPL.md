title: Licenza WTFPL
date: 2015-07-02 18:26

L'autore di questo blog rinuncia a qualsiasi diritto di copyright su questo
lavoro, rilasciandolo con licenza WTFPL. Potete utilizzarlo, modificarlo,
storpiarlo e diffonderlo in qualunque modo senza dover attribuire o comunicare
nulla all'autore. Se diffondete una versione modificata, cambiate il nome
dell'autore.

I lavori altrui pubblicati su questo blog, dove possibile, riporteranno il nome
dell'autore originale e non vanno considerati in pubblico dominio o sotto WTFPL,
a meno che non sia specificato diversamente. Chi si sentisse offeso dalla
ripubblicazione dei propri lavori, può contattare l'autore del blog per
discuterne. Nel caso venisse decisa la cancellazione, sul blog verrà segnalato
che è stata fatta su richiesta dell'autore specificato.

```
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
```
