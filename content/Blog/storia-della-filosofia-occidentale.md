title: Storia della filosofia occidentale
date: 2015-04-24 19:24:21 +0200
tags: recensioni, libri, filosofia
summary: In questo classico della storia della filosofia, Bertrand Russell riesce a spiegare con chiarezza, discorsività e, talvolta, ironia, l’intero sviluppo della filosofia occidentale e le relazioni che essa ha avuto con la politica e la società delle varie epoche.

In questo classico della storia della filosofia, Bertrand Russell riesce a spiegare con chiarezza, discorsività e, talvolta, ironia, l’intero sviluppo della filosofia occidentale e le relazioni che essa ha avuto con la politica e la società delle varie epoche.

Con limpidezza, si riescono a capire i motivi
delle svolte storiche e delle rivoluzioni culturali avvenute anche grazie allo
sviluppo del pensiero filosofico e, viceversa, quando il pensiero filosofico è
stato a sua volta influenzato dal periodo storico.

Russell introduce, abbraccia e spiega il pensiero dei vari filosofi più
influenti, per poi distaccarsene ed osservarlo da un punto di vista non più
coinvolto, criticandolo e trovandovi le eventuali falle ed incongruenze. Si fa
così seguire dal lettore in questo percorso d’affezione, conoscenza ed
allontanamento, che è il metodo più corretto per comprendere al meglio le idee
altrui.

Il libro contiene parti più entusiasmanti ed altre meno. Questo dipenderà molto
dai gusti e da ciò che il lettore cerca. Personalmente, ho trovato molto
avvincenti i capitoli sui greci, in particolare i presocratici con la loro
spudorata e fantasiosa genialità. Molto interessanti, anche se talvolta lenti,
quelli sugli scolastici ed il periodo medievale. Sempre più coinvolgenti i
successivi capitoli del periodo rinascimentale, fino ad arrivare ai filosofi
moderni. Negli ultimi capitoli, è stato particolarmente interessante trovare i
germogli delle idee filosofiche successivamente sviluppate dai filosofi
contemporanei.

Consiglio la lettura a tutti gli appassionati di filosofia e di storia,
specialmente a chi intende allargare le proprie conoscenze filosofiche, ma non
sa di preciso da dove iniziare. Grazie a questo libro, si riesce ad avere una
panoramica abbastanza dettagliata, che permette di poter scegliere con più
chiarezza quale pensatore si vuole approfondire.