title:  Utilità di un drago
date:   2015-07-15 20:40
tags:   fantascienza, società, politica, racconti
summary: Il seguente racconto è stato scritto da Stanislaw Lem nel 1983 come metafora e parodia della politica sovietica del periodo. Potrebbe sorprendere il trovarvi moltissime analogie anche con il sistema capitalistico in cui viviamo tutt'oggi.

Il seguente racconto è stato scritto da [Stanislaw Lem][1] nel 1983 come metafora e parodia della politica sovietica del periodo. Potrebbe sorprendere il trovarvi moltissime analogie anche con il sistema capitalistico in cui viviamo tutt'oggi.



Fino a questo momento ho passato sotto silenzio la mia spedizione sul pianeta
Abrasione, nella costellazione della Balena. L’economia locale si basa
interamente sul drago. La mia ignoranza nelle materie economiche mi ha
purtroppo impedito di capire fino in fondo questa peculiarità locale,
nonostante gli Abrasiani si fossero fatti in quattro per fornirmi
delucidazioni. Può darsi che un vero intenditore della natura dei draghi riesca
a comprendere la cosa meglio di me.

Il radiotelescopio di Arecibo già da un po’ di tempo captava dei segnali che
nessuno sapeva decifrare. Ci riuscì finalmente il professore Katzenfaenger.
Senza farsi scoraggiare dal tremendo raffreddore che lo affliggeva, il
professore si dannò l’anima per risolvere il mistero. Ma fu proprio il suo
naso, tappato e colante, che gli era d’intralcio nel lavoro, a indurli in modo
del tutto inatteso a ipotizzare che gli abitanti del pianeta non fossero
dotati, come noi, di una percezione sensoriale visiva, bensì olfattiva. In
effetti, di lì a poco si scoprì che i loro segnali non venivano trasmessi con
le lettere dell’alfabeto, bensì in un codice composto dai simboli degli odori.
A dire il vero, il materiale decodificato da Katzenfaenger conteneva parecchi
passaggi oscuri. Quasi quasi se ne poteva dedeurre che il pianeta fosse
abitato, oltre che da esseri pensanti, anche da una creatura più grande di una
montagna, straordinariamente ingorda e taciturna. Tuttavia, non era tanto quel
bizzarro fenomeno della zoologia cosmica in sé ad attirare la curiosità degli
scienziati, quanto piuttosto il suo insaziabile appetito che, a quanto pareva,
portava dei vantaggi addirittura eccezionali alla collettività locale. Che il
mostro abrasiano fosse un essere pericoloso, c’era da aspettarselo, nondimeno
sembrava che quanto più si faceva minaccioso, tanti più benefici ne venissero
tratti. Sarà che ho un debole per i casi misteriosi, ma, incuriosito da quelle
notizie, mi precipitai su Abrasione senza pensarci due volte. Sul posto evvi
modo di appurare che gli Abrasiani hanno fattezze praticamente antropomorfe.
Solo che i loro nasi spuntano là dove noi abbiamo le orecchie, e viceversa.
Anche loro discendono dalle scimmie, con la differenza che da noi c’erano le
scimmie con il setto nasale largo o stretto, mentre le loro proscimmie erano
dotate di un naso singolo o doppio. Quelle con un unico naso si estinsero per
denutrizione. Attorno al pianeta girano molte lune che spesso causano
prolungate eclissi solari. Di colpo tutto sprofonda in un buio pesto. Di
conseguenza, chi doveva trovare il cibo avendo a disposizione solo la vista non
cavava un ragno dal buco. Chi invece si orientava tramite l’olfatto si trovava
in una situazione leggermente più favorevole. Ma i più fortunati in assoluto
erano quelli con due nasi ben distanziati, perché potevano farsi guidare
dall’olfatto in modo stereoscopico, proprio come facciamo noi grazie ai due
occhi e l’udito stereofonico.

In seguito gli Abrasiano inventarono la luce artificiale e la binasalità cessò
di essere indispensabile per la sopravvivenza; ereditarono tuttavia dai loro
progenitori questa bizzarria anatomica. Durante le stagioni fredde portano
berretti dotati di speciali risvolti per riparare i nasi dal congelamento. Ho
avuto come l’impressione ch enon fossero proprio entusiasti di quei nasi che,
dopotutto, sono il retaggio di un passato duro, che preferirebbero dimenticare.
Il sessodebile mimetizza i nasi con i più stravaganti monili, talvolta grandi
quanto un piatto. Ma cercavo di non farci troppo caso.

L’esperienza nel campo dell’astronautica mi ha insegnato che le differenze
morfologiche sono davvero trascurabili. Il vero osso duro si annida solitamente
più in profondità. Inutile precisare che il punto debole degli Abrasiani sia il
loro drago.

Sul pianeta c’è un unico grande continente suddiviso in qualcosa come ottanta
paesi. L’intero litorale del continente è bagnato dalle acque oceaniche. Il
drago si estende nel profondo nord. Confinano con lui tre stati: Klaustria,
Lelipia e Lualia. Dopo aver osservato a lungo il drago sulle fotografie
scattate dai satelliti artificiali e sui plastici realizzati in scala
1:1.000.000, constatai che si trattava di una brutta bestia. Fra l’altro non
somiglia per niente ai draghi che popolano le saghe e le leggende terrestri. Il
loro drago non ha sette teste, anzi di teste non ne ha affatto, e, a quanto
pare, neppure il cervello. Non avendo le ali, non vola.  Quanto alle gambe, non
è del tutto chiaro, ma si può ipotizzare che sia completamente privo di arti.
Ha l’aspetto di un dorso montuoso spalmato di una viscida sostanza gelatinosa.
In realtà bisogna armarsi di molta pazienza per accertare che sia
effettivamente una creatura vivente. Si sposta con un’eccezionale lentezza,
compiendo movimenti peristaltici, ciononostante infrange spesso i confini di
Klaustria e Lelipia. Il bestione ingurgita qualcosa come ottantamila tonnellate
di prodotti commestibili al giorno. Predilige il granoturco e quello saraceno,
ma accetta di buon grado anche altri cereali. Non è però vegetariano. I
rifornimenti di cibo gli vengono assicurati dai paesi facenti parte dell’Unione
di Cooperazione Economica. Un’imponente mole di prodotti viene trasportata in
treno fino alle apposite stazioni di scarico, mentre le zuppe e gli sciroppi
vengono convogliati attraverso un complesso sistema di acquedotti, e nei mesi
invernali, per ovviare al calo vitaminico, le adeguate razioni di ricostituenti
vengono lanciate da enormi aerei. Per quanto concerne la sua cavità orale,
tanto vale risparmiarsi la fatica di cercarla: il mostro è capace di ingozzarsi
con qualsiasi parte del suo corpaccione.

Quando arrivai in Klaustria, la domanda che mi premeva di più era: perché tutto
quel fervore e quegli sforzi per sfamare l’orribile mostro, quando si potrebbe
semplicemente abbandonarlo a se stesso e aspettare che crepi di fame? Mi morsi
però la lingua, quando mi giunsero voci sul cosiddetto attentato al drago. In
pratica, un Lelipiano che voleva conquistarsi la fama di salvatore dell’intera
Abrasione, aveva fondato un’organizzazione clandestina per far fuori
l’insaziabile colosso. L’idea era quella di infettare gli integratori
vitaminici con delle sostanze chimiche in grado di indurre un’arsura talmente
incontenibile che per placarla il mostro si sarebbe messo a trangugiare le
acque dell’oceano fino a scoppiare. Questo mi ricordò una nota leggenda
terrestre che narra le vicende di un eroe intrepido che sconfisse il drago
insediandosi nella sua patria, il cui cibo preferito erano le giovani vergini,
servendogli furtivamente pelle di pecora imbottita di zolfo. Ma le similitudini
tra la fiaba terrestre e la traltà abrasiana si esauriscono qui. Il drago
locale è infatti tutelato dalle leggi internazionali. A parte questo, le
ingenti forniture di cibi appetitosi gli vengono garantite dai trattati di
cooperazione con il drago, firmati da quarantanove paesi. Il traduttore
elettronico, dal quale non mi separavo mai, mi permise di esaminare
minuziosamente la stampa locale. L’attentato al drago, seppur sventato, aveva
destato grande scalpore nell’opinione pubblica. La gente reclamava con forza
pene severe per gli attentatori. Tutto ciò mi era parso molto strano,
specialmente perché del drago, in quanto tale, nessuno parlava con simpatia.
Tanto i giornalisti, quanto gli autori delle lettere spedite ai giornali,
nessuno escluso, lo trattavano come un mostro ripugnante oltre ogni
immaginazione. Sulle prime ho pensato che lo considerassero un idolo malvagio,
una specie di castigo mandato dal cielo, e attribuivo a una pura e semplice
usanza locale il fatto che definissero le offerte a lui profuse esportazioni.
Anche del diavolo si può parlare male, senza per questo tenerlo in scarsa
considerazione. Un diavolo è notoriamente in grado di indurre in tentazione, e
chi gli vende la propria anima può aspettarsi in cambio un bel po’ di godimento
e di piaceri terreni, mentre il drago – per quanto avevo potuto osservare – non
prometteva un bel nulla e non tentava nessuno con le benché minime lusinghe.
Tutt’altro. Di tanto in tanto si gonfiava e allargava le zone di confine con i
resti degli alimenti rigettati, e quanto faceva brutto tempo il suo fetore
infestava l’aria nel raggio di mille chilometri. Malgrado ciò, gli Abrasiani
erano convinti che bisognasse prendersene cura perché la puzza era solo sintomo
di cattiva digestione, e occorreva procurargli i farmaci in grado di regolare
il suo metabolismo. Per quanto cocerneva invece l’attentato, se,
malauguratamente, fosse andato a buon fine, le sue conseguenze sarebbero state
a dir poco catastrofiche. Leggevo e rileggevo tutti i giornali, eppure non
riuscivo a capire in che cosa esattamente sarebbe dovuta consistere questa
catastrofe. In preda alla confusione mi misi a girare per le biblioteche
locali, a spulciare diverse enciclopedie e la Storia universale di Abrasione,
feci persino un salto all’Associazione per l’Amicizia con il Drago. Tutto
inutile. Eccetto qualche piccolo funzionario, non trovai nessuno. Questi
provarono addirittura a convincermi a prendere il tesserino e a versare
anticipatamente tutte le tasse annue, ma non era certo quello lo scopo della
mia visita.

Dal momento che gli stati confinanti con il drago sono delle democrazie
liberali, chiunque è libero di dire quel che gli pare e piace. Dopo snervanti
ricerche riuscii finalmente a scovare le pubblicazioni contestatarie. Ma anche
gli autori di questi scritti sostenevano che i rapporti con il drago dovrebbero
fondarsi su ragionevoli compromessi. Secondo loro l’uso della forza o dei
trabocchetti potrebbe avere conseguenze fatali. Inutile dire che che gli
attentatori mancati venivano nel frattempo tenuti agli arresti. Non si
dichiararono colpevoli, benché ammettessero l’intenzione di ammazzare il drago.
Negli organi di stampa del governo venivano definiti come delle innocue teste
calde con un cervello di gallina. La «Gazzetta illustrata di Klaustria» li
etichettò invece come un manipolo di provocatori, insinuando peraltro che uno
dei governi limitrofi, insoddisfatto delle quote di esportazioni assegnate al
suo paese dall’Unione di Cooperazione Economica, avesse fomentato la
provocazione nella speranza di creare le condizione favorevoli alla revisione
delle quote di esportazione e a una loro ripartizione più equa.

Un reporter che si presentò per intervistarmi venne sommerso dalle mie domande
sul drago. Come mai gli attentatori erano stati messi sul banco degli imputati,
invece di concedere loro la chance di farla finita con il mostro una volta per
tutte?

Il giornalista affermò che sarebbe stato un delitto atroce. In fondo, mi disse,
il drago è un bonaccione, solo che le rigide condizioni di vita nel perenne
gelo artico gli impediscono di manifestare la sua innata docilità. Tutti quelli
che soffrono di fame cronica, poco importa se draghi o non draghi, prima o poi
si incattiviscono. Dobbiamo nutrirlo, solo così smetterà di spingersi verso sud
e diventerà più mansueto.

«Da dove nasce questa sua certezza?» domandai. «Negli ultimi giorni ho raccolto
molti ritagli del vostri giornali. Eccole una manciata di titoli: ‘Le regioni
nordiche di Lelipia e di Klaustria si stanno spopolando. L’esodo di massa
continua’. Oppure quest’altro: ‘Il drago ingoia un altro gruppo di turisti. Per
quanto ancora le irrespondabili agenzie continueranno a organizzare viaggi
sciagurati?’ Senta questa: ‘Nel corso degli ultimi dodici mesi il drago ha
allargato il suo territorio di ben 900.000 ettari’. Be’? Che mi dice?»

«È solo la conferma di quello che le ho appena detto» replicò. «Non lo stiamo
nutrendo ancora abbastanza! Quanto ai turisti, non cercherò di negarlo, è
capitato qualche incidente, senza dubbio spiacevole, ma è anche vero che non
bisogna infastidire il drago. Non sopporta i turisti, specialmente quelli che
fanno le foto. È allergico ai lampi del flash. Che c’è da stupirsi: non vive
forse al buio per tre quarti dell’anno? D’altro canto non dobbiamo dimenticare
che solo l’industria che produce i ricostituenti ipercalorici per il drago ci
assicura centroquarantaseimila posti di lavoro. D’accordo, un po’ di turisti
sono scomparsi, ma consideri quante persone morirebbero di fame se dovessero
perdere il lavoro».

«Aspetti un momento» ribattei. «Il cibo che fornite al drago ha un suo costo di
produzione. Chi lo paga?»

«I nostri parlamentari stanziano per questo gli appositi crediti di
esportazione»

«In pratica il drago grava sulle tasche dei vostri contribuenti?»

«In un certo senso sì, ma è un investimento vantaggioso»

«Mi chiedo se non sarebbe ancora più conveniente sopprimerlo…»

«Lei parla a sproposito. Negli ultimi trent’anni abbiamo investito nei vari
settori dell’industria di cibo per il drago suppergiù quattrocento miliardi…»

«E non sarebbe meglio utilizzare questo denaro a vostro esclusivo vantaggio?»

«Ma questi sono, parola per parola, gli argomenti dei nostri conservatori più
reazionari!» Il reporter alzò la voce infastidito. «È una combutta di
seviziatori! Tutto quello che vogliono è fare piazza pulita e trasformare il
drago in scatolette! Ma la vita è una cosa sacra. È vietato uccidere».

Visto che questa discussione non mi portava da nessuna parte, mi congedai dal
giornalista. Dopo una breve riflessione mi diressi verso gli Archivi della
Stampa e dei Documenti Antichi, con il fermo proposito di compulsare intere
annate di periodici per scoprire da dove, perdio, era sbucato quel drago.
Faticai parecchio, ma riuscii a sconvare una notizia piuttosto curiosa.

Soltanto mezzo secolo prima, quando il territorio occupato dal drago era di
appena due milioni di ettari, non lo prendeva sul serio nessuno. Mi ero
imbattuto in numerosi articoli, in cui si suggeriva di dissodare il terreno o
di inondare il drago con acqua, fatta confluire per mezzo di un sistema di
canali appositamente congegnato, per farlo morire di congelamento. Ma gli
economisti di allora obiettavano che un’operazione del genere sarebbe stata
troppo dispendiosa. Tuttavia quando il drago, che a quei tempo si nutriva solo
di muschio e di licheni, raddoppiò la sua mole e gli abitanti delle regioni
limitrofe presero a lamentarsi del suo lezzo che ammorbava l’aria, specialmente
quando spiravano i tiepidi venti primaverili o estivi, le associazioni di mutuo
soccorso si adoperavano a profumare il drago e, visto che i risultati non
arrivavano, iniziarono la raccolta del pane. All’inizio le loro azioni venivano
sbeffeggiate, ma con il passare del tempo cominciarono a prosperare. Curiosando
negli annali successivi non trovai più una sola parola sulla liquidazione del
drago. Venivano piuttosto sottolineati i vantaggi che si sarebbero potuti
ricavare offrendogli aiuto ed assistenza. Le informazioni che avevo raccolto
erano troppo cavillose, e siccome volevo andare al fondo della questione,
decisi di fare una visita all’università, e per la precisione alla facoltà di
dracologia generale e applicata. Il decano della facoltà mi ricevette con
grande cordialità.

«Le questioni che lei pone sono del tutto anacronistiche» disse con un
sorrisetto indulgente, dopo avermi ascoltato con attenzione. «Il drago
costituisce ormai un aspetto oggettivo della nostra realtà, per certi versi è
il suo elemento portante, integrante, e per quetso motivo dobbiamo occuparci di
lui come di un affare di grande rilievo internazionale».

«Se non le dispiace, passerei ai fatti concreti» gli risposi. «Da dove diavolo
viene quel drago?»

«E chi lo sa» rispose con flemma il dracologo. «L’archeologia, la predracologia
e la genetica dei draghi esulano dall’ambito delle mie competenze. Io non mi
occupo di dracogenesi. Finché era piccolo, non creava nessun problema. Come
tutte le cose di questo mondo, mio egregio straniero».

«Ho sentito dire che è nato dalla mutazione genetica delle lumache».

«Ma figuriamoci. A ogni modo cosa ci può importare ora da dove viene, oggi come
oggi non ci rimane che prendere semplicemente atto che esiste, in tutta la sua
evidenza. La sua improvvisa scomparsa, dio ce ne scampi, sarebbe una
catastrofe. Temo che non riusciremmo mai a risollevarci da una sciagura di tale
portata».

«Dice sul serio? In fin dei conti perché?»

«Come conseguenza dell’automazione generale il tasso di disoccupazione aveva
cominciato a crescere inesorabilmente. Senza risparmiare le professioni
accademiche».

«E allora? Il drago ha forse risolto il problema?»

«Altroché. Avevamo un enorme surplus di generi alimentari, montagne di pasta, i
pozzi pieni di olio commestibile, una sovrapproduzione di dolciumi. Attualmente
esportiamo tutte le nostre eccedenze al nord, naturalmente dopo averle
adeguatamente trasformate. Lui non si accontenta certo di una cosa qualunque».

«Il drago?»

«E già. Solo per elaborare il regime alimentare più adatto a lui ci è toccato
fondare tutta una serie di centri di ricerca. Per esempio l’Istituto centrale
d’Ingrasso del drago, o la stessa Accademia d’Igiene del Drago, e poi ogni
università possiede ora almeno una cattedra di dracologia. Abbiamo stabilimenti
gastronomici che producono nuovi tipi di vivande e di ricostituenti. I
Ministeri della propaganda hanno predisposto delle apposite reti d’informazione
per spiegare alla popolazione i vantaggi derivanti dallo scambio commerciale
con il drago».

«Uno scambio? Allora da lui si può ricavare qualcosa? Non l’avrei mai detto!»

«Ma è ovvio. In primo luogo la cosiddetta draconina, cioè la sua secrezione
cutanea»

«Sarebbe quella specie di di muco lucido che lo ricopre? Si vede nelle foto. Mi
chiedo che utilità possa avere».

«In forma condensata ci server per produrre la plastilina usata dai bambini
negli asili, che se devo dire che i problemi, in effetti non mancano. L’odore è
quasi insopportabile».

«Puzza?»

«Direi proprio di sì, e anche parecchio. Per rimediare bisogna aggiungere
speciali sostanze deodoranti. E per il momento la plastilina di drago costa
otto volte quella normale»

«E cosa mi dice, professore, dell’attentato al drago?»

Lo studioso si grattò l’orecchio che gli si afflosciò sulle labbra.

«Se quell’attentato fosse stato messo a segno, saremmo stati colpiti da
epidemie e pestilenze, tanto per iniziare. Provi solo a immaginarsi le
esalazioni che si sprigionerebbero dalla sua carogna! In secondo luogo, un
inevitabile crac bancario. Il collasso dell’intero sistema monetario. Tutto
questo ci porterebbe allo sfacelo generale, caro il mio forestiero. A un
terribile disastro».

«Ma se la sua sola presenza nuoce anche all’aria… Parliamoci chiaramente: è una
bella seccatura».

«Una seccatura perché?» disse il dracologo con una calma imperturbabile.
«Un’eventuale crisi postdraconica sarebbe molto più seccante! Cerchi di capire
che noi non provvediamo solamente al suo nutrimento, ma che la nostra
assistenza va molto al di là dei meri adempimenti gastronomici. Cerchiamo di
rabbonirlo e di tenerlo con la corda al colo. Tali iniziative rientrano nel
programma del cosiddetto addomesticamento e della pacificazione del drago.
Negli ultimi tempi gli vengono somministrate grandi quantità di dolciumi. È
molto goloso, gli piace rimpinzarsi di leccornie».

«Dubito che questo lo possa addolcire» mormorai.

«Comunque sia, le esportazioni dell’industria dolciaria sono quadruplicate.
Bisogna anche tener conto dell’attività dell’URSS».

«Che cosa sarebbe?»

«L’ufficio di Regolamentazione dei Servizi di Supporto, preposto a compensare
l’impatto socioambientale del drago. Aiuta i neolaureati a inserirsi nel
mercato del lavoro. Il drago deve essere esaminato, studiato, di tanto in tanto
persino curato e medicato: prima avevamo eccedenza di manodopera nel settore
dei servizi sanitari, ora invece ogni giovane medico ha un posto di lavoro
assicurato».

«Capisco» dissi senza troppa convinzione. «Se però guardiamo in faccia ai
fatti, questo non è altro che esportazione di beneficenza. Che cosa vi
impedisce piuttosto di diventare benefattori dei vostri stessi cittadini?»

«Come sarebbe a dire?»

«Be’… È fin troppo evidente che stanziate un mucchio di soldi per il drago!»

«E che male c’è? Dovremmo forse fare omaggi in denaro a tutti i cittadini? Ma
non si rende conto che questo sarebbe palesemente in contrasto con i più
elementari principi dell’economia? Si vede subito che lei se ne intende ben
poco dei fondamenti del sistema produttivo. Il finanziamento dei crediti
all’esportazione rinvigorisce la congiuntura economica. Incrementa i
fatturati».

«Ma la tempo stesso incrementa anche le dimensioni del drago» lo interruppi.
«Quanto più intensamente lo alimentate, tanto più lui si ingrandisce, e più
s’ingrossa, più aumenta il suo appetito. Che razza di ragionamenti fate? Non vi
rendete conto che di questo passo vi divorerà tutti?!»

«Sciocchezze» si sdegnò il professore. «I crediti vengono registrati dalle
banche come saldi attivi!»

«In che senso? Come se fossero dei semplici prestiti restituibili? Bella
questa! Come dire che il drago salderà i suoi debiti con la plastilina?»

«Non mi metta le parole in bocca, per piacere. Se non avessimo il drago, per
chi potremmo fabbricare gli acquedotti che convogliano le sue pappe? Faccia
almeno uno sforzo per capire: gli acquedotti fanno lavorare sia le fonderie che
le acciaierie, sia i reparti di laminazione ch ei macchinari per la saldatura,
e via di seguito. I fabbisogni del drago sono reali. Vuol metterselòo in testa
una buona volta? I bisogni economici non nascono mica dal nulla! Col cavolo che
i produttori si metterebbero a fabbricare le merci sapendo di doverle buttare a
mare. Il consumatore concreto è un altro paio di maniche. Il drago costituisce
un mercato esterno enorme, ad altissimo potenziale di assorbimento,
caratterizzato da una domanda colossale…»

«Non lo metto in dubbio» tagliai corto, sapendo già che anche questa
conversazione non mi avrebbe portato da nessuna parte.

«Così l’ho convinta?»

«No».

«È perché lei proviene da una civiltà troppo diversa dalla nostra. D’altronde
il drago ha smesso da tempo di essere un semplice importatore dei nostri
prodotti»

«E cosa sarebbe diventato adesso?»

«Un’idea, mio caro. Una necessità storica e la nostra ragione di stato. Un
fattore determinante che conferisce senso unitario ai nostri sforzi collettivi.
Cerchi di vedere la cosa da questo punto di vista e sono sicuro che prima o poi
dovrà riconoscere anche lei la fondamentale importanza delle implicazioni che
racchiude in sé una bestiaccia pe certi versi abominevole, quando raggiunge
dimensioni planetarie»

*Settembre 1983*

P.S. Corre voce che il drago si sia smembrato in una moltitudine di
dragoncelli, ma il loro appetito non si è affatto placato.

*[Stanislaw Lem][1]*

[1]:https://it.wikipedia.org/wiki/Stanis%C5%82aw_Lem
