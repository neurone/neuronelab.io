title: Cosa pensa chi vuol diventare soldato?
date: 2015-05-07 20:12:23 +0200
tags: psicologia, politica, etica, violenza
summary: Riporto questo articolo del mio vecchio blog. È datato 25 luglio 2010 e fa riferimento a fatti di quel periodo. Riporto anche la discussione nata nei commenti, dato che, in qualche modo, era diventata "mitica" fra chi seguiva il blog per via di un commento particolarmente goffo fatto da un più-o-meno noto scrittore.

Riporto questo articolo del mio vecchio blog. È datato 25 luglio 2010 e fa riferimento a fatti di quel periodo. Riporto anche la discussione nata nei commenti, dato che, in qualche modo, era diventata "mitica" fra chi seguiva il blog per via di un commento particolarmente goffo fatto da un più-o-meno noto scrittore.

Mi piacerebbe riuscire a capire che genere di meccanismi di rimozione possano
scattare nella mente di una persona, nel momento in cui decide di diventare un
soldato di carriera.

Perché è esattamente quello, credo, il momento in cui hai ancora l’ultima
possibilità di farti delle domande e magari renderti conto che prendendo quella
strada, implicitamente accetti di poterti ritrovare nella situazione di dover
uccidere anche persone inermi e bambini, di avvelenare il loro territorio e
distruggere la vita della generazione attuale di sopravvissuti e di quelle
future.

Magari ti illudi che potrebbe non accadere, ma com’è possibile non essere in
grado di pensare che quella possibilità esista concretamente e che tu potresti
diventarne complice o artefice diretto? Quando decidi per il “sì”, decidi anche
che quell’eventualità, che forse ti disturba e ti mette a disagio, è meno
importante di altre.

È l’ultima possibilità, perché una volta accettato, grazie all’indottrinamento e
all’addestramento, che ti rendono sempre più specializzato ed ef**f**iciente nello
seguire le regole del gioco e di chi lo dirige, la tua volontà individuale viene
inevitabilmente meno. Quello che accade poi, penso sia ampiamente spiegato dagli
esprimenti [Milgram][1] e della [prigione di Stanford][2].
<!--more-->

Non sto cercando delle colpe: per una persona, è necessario diventare in qualche
modo “schizofrenico” per poter partecipare ad u**n**a cosa disumanizzante come la
guerra. Tutto ciò che concerne l’addestramento alla violenza ed al dominio, va a
creare un equilibrio troppo precario perché delle persone possano riuscire a
reggerlo. In questo, chi decide la carriera militare, viene ingannato o
s’inganna da sé, vittima di illusioni.
È necessario sviluppare una doppia moralità e mentire alla pr**o**pria coscienza per
poter reggere ciò che si ha scelto e ci si sente poi obbligati a fare… obbligati
in nome di qualcosa che si ritiene superiore… che la si chiami “patria”,
“democrazia”, “dio”, “stato” o quant’altro.

Se si fosse davvero lucidi e completamente razionali, in che modo si potrebbe
pensare di fa**r**e del bene, imparando ad ammazzare, distruggere ed esercitare il
dominio ed il comando sugli altri? Dopo aver letto articoli di cronaca come
questi: [“Raptus di follia”, Afghanistan, versioni discordanti sulla strage: i 17 civili uccisi “da più soldati americani”][3],
[Follia di soldati americani, due donne violentate e uccise][4], come si può
essere certi di non finire nelle stesse condizioni mentali di quel, o quei,
soldati?

Ed a quel punto, gli uomini più forti, con più forza di volontà e coraggio,
p**o**trebbero non essere più quelli con il record di medaglie sulla divisa, ma i
[disertori][5].

Questo post è nato appena dopo aver letto questi **d**ue articoli.

- [Il massacro americano di Falluja][6] ha avuto conseguenza peggiori anche di
  Hiroshima
- [Le menzogne sull’uranio da Hiroshima a Falluja][7]

Per non dire dei produttori di armi, fra cui l’Italia. [L’Onu proibisce le bombe a grappolo ma l’Italia continuerà a venderle][8]

> Organo e funzione sono termini inseparabili. Levate ad un organo la sua
> funzione o l’organo muore o la funzione si ricostituisce.  Mettete un
> esercito in un paese in cui non ci siano né ragioni né paure di guerra
> interna o esterna, ed esso provocherà la guerra, o, se non ci riesce, si
> disfarà. Una polizia dove non ci siano delitti da scoprire e delinquenti da
> arrestare, inventerà i delitti e delinquenti, o cesserà di esistere.
**Errico Malatesta**

Le Déserteur di [Boris Vian][8] è una delle più famose canzoni contro la guerra.
Ne sono state fatte molte versioni e traduzioni, una è [quella di Luigi Tenco][9].
Per saperne di più su questo brano e su tutte le sue versioni, vi consiglio il
sito [Canzoni contro la guerra (www.antiwarsongs.org)][10]. Canzoni Contro la
guerra: [Le Dèserteur][11]

> Padroni della Terra,/
> vi scrivo queste righe/
> che forse leggerete/
> se tempo avrete mai./
> Ho qui davanti a me/
> il foglio di richiamo:/
> io devo ritornare/
> in caserma lunedì./
> Padroni della Terra,/
> non lo voglio più fare,/
> non posso più ammazzare/
> la gente come me./
> Non è per farvi torto/
> ma è tempo che vi dica:/
> la guerra è un’idiozia,/
> non ne possiamo più./
> Da quando sono nato/
> dei figli son partiti,/
> dei padri son caduti/
> davanti agli occhi miei./
> Ho visto mille madri/
> che han perso tutto quanto/
> ed ancora vanno avanti/
> senza saper perché./
> Al prigioniero poi/
> han rubato la vita,/
> han rubato la casa/
> e tutto quel che ha./
> Domani alla mia porta/
> verranno due gendarmi,/
> verranno ad arrestarmi,/
> ma io non ci sarò./
> Lontano me ne andrò;/
> sul mare e sulla terra,/
> per dire no alla guerra/
> a quelli che vedrò./
> E li convincerò/
> che c’è un nemico solo:/
> la fame che nel mondo/
> ha gente come noi./
> Se c’è da versar sangue/
> versate solo il vostro;/
> signori, ecco il mio posto:/
> io non vi seguo più./
> E se mi troverete,/
> con me non porto armi:/
> coraggio, su, gendarmi,/
> sparate su di me.
**Luigi Tenco, Padroni della Terra - Le dèserteur (Boris Vian)**

Commenti
--------
**[Gianluca Bartalucci][12]**
*on 26 luglio 2010 at 08:20*

ieri sera riguardavo per l’ennesima volta “Amore e guerra” di W. Allen e mi
venivano a mente alcune di queste cose. Penso che quel film – in maniera leggera
– dica diverse su pacifismo et similia.
https://www.youtube.com/watch?v=o1RX_Ikoljw

**Masque**
*on 26 luglio 2010 at 08:54*

bello. non l’avevo mai visto :-)
lo guarderò. stavo anche cercando Orizzonti di gloria, di Kubrik

**[Gianluca Bartalucci][12]**
*on 26 luglio 2010 at 09:18*

bello bello, “orizzonti di gloria”. ma va be’, il numero di prodotti di
letteratura e cinema sul rifiuto della guerra e sugli orrori psicologici della
stessa sono, come sai, sconfinati. legandomi al tuo titolo (cosa scatta nella
mente di chi vuole diventare un soldato?) ho subito pensato, per esempio, a “One
hour by the concrete lake” dei POS e a “Nella valle di Elah”
http://www.youtube.com/watch?v=fvnRkAlvgCk

**Masque**
*on 26 luglio 2010 at 09:33*

Sembra bello quel film. Me lo procurerò.
Non avevo pensato, invece, a “concrete lake”. Forse perché parla di un operaio
di una fabbrica di armi. Ma in effetti, anche lì, il discorso non cambia. E la
situazione è ancora più subdola, probabilmente. Ed allora mi chiedo anche cosa
può pensare l’impiegato di una banca quando scopre che essa investe anche
nell’industria bellica (le cosiddette banche armate
http://www.banchearmate.it/home.htm ). Come può l’impiegato di una banca, BNL o
Unicredit ad esempio, magari ritenendosi anche pacifista, continuare a lavorare
lì quando scopre cosa va a finanziare il suo lavoro?

**Masque**
*on 27 luglio 2010 at 13:07*

Ora ho visto tutti i tre i film. Tutti e tre stupendi. Amore e guerra è il primo
film di W.Allen che vedo. Adorabile!
Orizzonti di gloria e Nella valle di Elah sono entrambi bellissimi (il figlio
della poliziotta, con quelle sue poche e semplici domande sulla storia di David
e Goliah, si meriterebbe il nobel). Ma con questi, penso di aver raggiunto il
limite massimo di film sulla guerra che riesco a reggere per questo periodo. :-)

**[Gianluca Bartalucci][12]**
*on 27 luglio 2010 at 15:34*

:)

Amore e guerra penso di conoscerlo a mente. So che è pieno di citazioni di
letteratura russa (una passione di Allen poi sfociata – tra le altre cose – in
“Match Point”), tra cui il dialogo dostoevskijano:

« Padre: Ricordi quel bravo ragazzo nostro vicino, Raskolnikov?  

Boris: Si.  
Padre: Ha ucciso due signore.  
Boris: Ma che brutta storia.  
Padre: A me lo ha detto Bobik. Lo ha saputo da uno dei fratelli Karamazov.  
Boris: Atti inconsulti di ossessi.  
Padre: Non aveva nè guerra nè pace.  
Boris: O forse era solo un idiota.  
Padre: Aveva una faccia da umiliato e offeso.  
Boris: Io sapevo che era un giocatore.  
Padre: Potrebbe essere il tuo sosia!  
Boris: La realtà romanzesca! »  

“Domattina alle sei verrò giustiziato per un crimine che non ho commesso. Dovevo
essere giustiziato alle cinque. Ma ho un avvocato in gamba.”  

**Masque**
*on 28 luglio 2010 at 11:57*

Quando l’avevo visto, non mi ero accorto dei riferimenti a Dostoevskij :-D
Bisognerebbe guardarlo più volte, ed anche al rallentatore, perché i dialoghi
filosofici fra la cugina acquisita e Boris o gli altri amanti si riescono a
seguire per un po’, e sono sensatissimi, ma poi sfuggono perché troppo veloci e
“tecnici”. :-)

**PM**
*on 21 agosto 2010 at 21:59*

Credo che anche questa lettera possa esprimere molto sull’idea del pacifismo o
della non-violenza…:
http://piemssite.wordpress.com/2010/06/02/se-non-potremo-salvare-lumanita-ci-salveremo-almeno-lanima/
Ciao, e grazie di essere passato :-)

**Masque**
*on 22 agosto 2010 at 01:15*

Grazie a te :-)
Leggerò con piacere domani la lettera di Don Milani (che conosco praticamente
solo per nome… sentito nominare molto spesso, ma mai letto. la mia ignoranza mi
mette spesso in imbarazzo :-) ), ora non riuscirei ad essere abbastanza sveglio.

**PM**
*on 22 agosto 2010 at 23:14*

Ah, non preoccuparti, quanto a ignoranza non manca nulla neppure a me… Ho
scoperto questa lettera di recente pure io, attraverso persone che combattono
attivamente la violenza (anche quella che usa, per copertura, la difesa della
patria e così via) e l’ho fatta mia.

**Masque**
*on 23 agosto 2010 at 18:56*

Sì. C’è una frase famosa, non ricordo di chi, che dice qualcosa del tipo “quando
lo stato si prepara per ammazzare, si fa chiamare patria”.
La stessa cosa si può dire di molti altri casi in cui il potere propaganda il
proprio mantenimento e la propria espansione, richiamando ideali “superiori”,
autorità divine e via dicendo. In questo modo, il senso critico della gente si
affievolisce e ciò permette loro di essere crudeli come non lo sarebbero in
nessun’altra situazione.
Io penso che la maggior parte delle persone non siano crudeli e non vogliano
esserlo, ma spesso ci si ritrovino ad esserlo a causa della poca conoscenza che
hanno di loro stessi, del loro – per così dire – funzionamento. Ci si trova
vittime di illusioni che ci portano a conclusioni affrettate, non si comprende
la facilità con cui si può commettere degli errori e quindi ci si sopravvaluta.
Si danno per scontate moltissime cose, evitando così di metterle in dubbio.
Semplicemente non le si nota. Ed è normale che accada: è un meccanismo innato
che serve a ridurre il carico cognitivo della nostra mente. Ma per evitare di
rimanervi intrappolati, è necessario accorgerci che un simile meccanismo esiste.

**Mastercaution**
*on 27 agosto 2010 at 08:03*

Ciao. Mi chiamo Luca, e nel provare a rispondere alla tua domanda desidererei
premettere che tendenzialmente simpatizzo con le tue argomentazioni. Dire
“simpatizzo” non significa però condividerle in toto; per dirla con De André “se
non del tutto giusto, quasi niente sbagliato”.
A mio avviso è necessario operare una distinzione netta nella figura di ciò che
tu definisci “soldato”, raffigurando così un unico calderone monolitico,
omogeneo, impenetrabile, unidirezionale. Tale distinzione si basa sulle
motivazioni che spingono (ho detto “spingono”, non “invitano”) ad intraprendere
la carriera militare. E quindi troveremo chi si arruola per semplice bisogno di
mangiare (e le Forze Armate in questo hanno sempre servito egregiamente da
serbatoio occupazionale), per fanatismo militarista (e costoro ultimamente hanno
subìto un duro tracollo psicologico nel prendere atto che le loro precedenti
aspettative e aspirazioni sono state principalmente indotte e plasmate
dall’immaginario hollywoodiano, che nulla ha a che vedere con la realtà), chi
invece crede nelle Forze Armate come un settore della società moderna, previsto
e governato dalla Costituzione Italiana (quest’ultima esempio di modernità
democratica invidiato da più parti nel mondo), da leggi e regolamenti (pertanto
sottratto all’arbitrio), e a fondamento delle quali risiede lo spirito di
servizio, la fedeltà nei confronti del popolo Italiano (e non semplicisticamente
nei governanti o nei politici), l’intento di fare concretamente qualcosa per il
proprio Paese e la sua popolazione, in contrapposizione all’imperante dominio
dell’interesse particolare di combriccole di affaristi dallo stomaco peloso e
senza fondo. Io appartengo a quest’ultima categoria, e ho l’onore di servire
come Ufficiale in Aeronautica Militare. L’orgoglio – ahimé ultimamente un po’
malconcio – con cui indosso la mia uniforme non mi esime però, leggendo il tuo
blog, dal riflettere sul senso profondo della mia scelta e delle mie
motivazioni, cogliendo tutta una serie di contrasti, paradossi e
disallineamenti, fondamentalmente basati sulla constatazione che le Forze
Armate, in quanto puro e semplice strumento di una volontà politica (sia essa
espressione di una democrazia o meno), possono essere utilizzate bene o male, a
favore o contro, per uno scopo o per il suo contrario.

PRIMO PARADOSSO:
Cito dal tuo articolo:
“prendendo quella strada, implicitamente accetti di poterti ritrovare nella
situazione di dover uccidere anche persone inermi e bambini, di avvelenare il
loro territorio e distruggere la vita della generazione attuale di sopravvissuti
e di quelle future. Magari ti illudi che potrebbe non accadere, ma com’è
possibile non essere in grado di pensare che quella possibilità esista
concretamente e che tu potresti diventarne complice o artefice diretto? Quando
decidi per il “sì”, decidi anche che quell’eventualità, che forse ti disturba e
ti mette a disagio, è meno importante di altre”.
Eccetto che per l’azione di avvalenare deliberatamente qualcuno, quanto tu
descrivi è letteralmente applicabile anche alle seguenti situazioni:
– chi guida un’automobile in modo scriteriato;
– chi irrora di insetticida con poca attenzione un campo seminato al limitare di
un centro abitato
– chi accende un barbecue al limitare di una pineta secolare, méta di turismo di
massa.

In pratica è applicabile a quasi tutte le azioni umane.

Domanda 1: quando una persona impugna un’arma e fa fuoco contro un’altra
persona, ad uccidere o ferire è l’arma oppure è la volontà che contrae il dito
sul grilletto? Allo stesso modo, quando una Forza Armata spiega le proprie forze
in campo causando perdite, quelle perdite le causano i militari oppure i
dirigenti dello Stato che ne hanno ordinato l’impiego?
Domanda 2: lo “stato eteronomico” definito a seguito dell’esperimento Milgram
(da te citato nell’articolo) è applicabile solo ad un militare che aziona
un’arma durante un’operazione, oppure anche allo stesso militare che salva una
famiglia alluvionata calandosi da un elicottero nel bel mezzo di una tempesta
monsonica, in entrambi i casi in obbedienza ad un preciso ordine impartito
dall’alto?
Domanda 3: perché nessuno ha rilevato che l’esperimento carcerario di Stanford
non è applicabile alla realtà militare, laddove agli studenti-carcerieri viene
data AMPIA DISCREZIONE SULLA SCELTA DEI METODI PER MANTENERE L’ORDINE (nella
realtà avviene il contrario, per mantenere l’ordine tu, guardia, ti devi
attenere ad una tale messe di regole da impiegare mesi di studio), mentre se gli
stessi eventi violenti – come descritti da Wikipedia – fossero successi a
militari veri questi sarebbero finiti in galera per trent’anni?
Domanda 4: perché nessuno ha rilevato che l’esperimento Milgram e quello
carcerario di Stanford non sono accaduti in un ambiente militare, ma
universitario e di gente priva di uniforme?
Domanda 5: ferme restando le innegabili nefandezze commesse dal personale
(militare) in servizio nel carcere di Abu-Ghraib, perché nessuno si è sentito in
dovere di precisare che le dure condanne – sul piano etico e morale, sociale,
penale – inflitte a tali criminali sono anche conseguenza della VIOLAZIONE DELLE
REGOLE DEL CARCERE, CHE VIETAVANO ESPRESSAMENTE TALI COMPORTAMENTI DA PARTE
DELLE GUARDIE?

ALCUNE CONSIDERAZIONI IN ORDINE SPARSO:

– le Forze Armate, come ho detto sopra, non sono un organismo monolitico,
impenetrabile, avulso dalla Società di cui sono espressione. Soffrono della
stessa umana fallacia della medesima società per la quale operano.
– per quanto ti sia difficile da accettare (e infatti nessuno è più tenuto a
prestare obbligatoriamente servizio nelle Forze Armate) il tipico sistema di
regole in vigore nel mondo militare, che si sostanzia nel rispetto di una
disciplina e nella gerarchia, è fondamentalmente una misura atta a governare e
limitare l’indiscriminato uso della violenza associato alla padronanza delle
armi. Esattamente in questo risiede l’imprescindibile differenza tra un’Unità
militare ed una banda di predoni, oppure tra un gruppo di militari ben
addestrati ed un gruppo di universitari ingaggiati per un esperimento sulla vita
carceraria, che si tramuta in soli cinque giorni in un’accolita di sadici e
violenti fuori da ogni controllo. Ogni recente episodio passato alla storia per
la violenza perpetrata ai danni della popolazione civile da parte di unità
militari si contraddistingue per una precisa CARENZA DI AUTORITA’ AL COMANDO,
che altrimenti avrebbe impedito tali atrocità. Altra misura volta a limitare
l’uso indiscriminato della forza conseguente alla padronanza delle armi è
l’addestramento, che contrariamente a quanto da te affermato non è funzionale al
plagio delle menti e delle coscienze, ma semplicemente evita che un’arma
provochi danni/feriti/vittime in misura superiore al necessario e sufficiente
(sebbene si potrebbe disquisire per settimane sulla natura e liceità morale di
tale “sufficiente necessità”), ottenendo un risultato con efficacia (efficacia =
il rapporto tra i risultati ottenuti e l’obiettivo voluto: per espugnare un
fabbricato, in passato si radeva al suolo un intero quartiere, oggi invece è
possibile attaccare la singola finestra di un singolo caseggiato). In aggiunta a
ciò, è stato creato tutto il sistema del Diritto Internazionale dei Conflitti
Armati, non esente da difetti e dubbi e storture interpretative, ma comunque
assai preciso nel definire cosa sia un’azione legittima e cosa non lo sia. In
quest’ultimo caso è competente il Tribunale Penale Internazionale dell’Aia.
– spero di fare cosa gradita nell’informarti che lo “stato eteronomico” nella
compagine militare, perlomeno in Italia, è stato ampiamente disinnescato e
neutralizzato dalla normativa sulla Disciplina Militare introdotta con Decreto
del Presidente della Repubblica del 1978 (quindi successiva agli esperimenti da
te citati), che introduce il concetto di “ordine legittimo”: se qualcuno mi
ordina di compiere un’azione manifestamente contraria alla legge, ho il DOVERE
di non eseguire l’ordine e di informare i superiori. In caso contrario sono
reputato complice in un reato, senza la possibilità di addurre l’ordine stesso
quale scusante a mia discolpa.
– volendo focalizzare l’attenzione sulle parole di Errico Malatesta, non mi
risultano esempi degni di nota, presenti o passati, di lotta alla criminalità
mediante abolizione della Polizia. Allo stesso tempo è vero che la semplice
presenza di un Corpo di Polizia non è sufficiente a far sparire la criminalità.
Quest’ultima, infatti è un fenomeno sociale, mentre la Polizia è un’espressione
della volontà di uno Stato. Il principio della fine avviene quando la seconda
viene contaminata dal primo.

Spero, con il mio piccolo pensiero, di avere contribuito ad un dibattito che
vedo comunque con favore (il dibattito ed il confronto sono sempre stati al
centro di ogni umano progresso). Si può essere a favore o contro le tue parole,
ma di sicuro non indifferenti.

Un saluto.

**Masque**
*on 27 agosto 2010 at 10:57*

Mi piace il tuo intervento, perché è coraggioso e ben argomentato. Chiaramente
il mio punto di vista è parziale: non ho mai fatto parte dell’esercito, sono
stato obiettore quando ancora la leva era l’opzione di “default” e quindi non so
cosa proverei ad essere un militare. Inoltre, seguo con convinzione un’ideologia
di tipo nonviolento, quindi come puoi immaginare, difficilmente riusciremo a
trovare totale accordo su questi argomenti.

Hai scritto molte cose e dovrei farmi un appunto per poter rispondere punto per
punto. Intanto vorrei spiegare più chiaramente alcune cose.
Quando ho scritto di avvelenare il territorio, non intendevo necessariamente
un’azione volontaria e, nel caso comunque di azione volontaria (un bombardamento
con napalm, armi nucleari o bombe al fosforo bianco non possono non essere
volontarie), non posso negare che gli effetti collaterali (volendo supporre che
non fosse quella l’intenzione principale) siano anche di contaminazione del
territorio. In questo includo anche le porzioni di territorio nelle quali
vengono dimenticate mine o parti di ordigni a frammentazione come quelli citati
in uno degli articoli che avevo collegato.
Io vivo in un territorio che ha subito molti bombardamenti durante le due guerre
e capita saltuariamente che sia necessario l’intervento degli artificieri per
poter rimuovere quegli ordigni che potrebbero, tutt’oggi causare danni, feriti o
morti. Questo mi ha fatto molto pensare. Armi sganciate da nazioni che in un
breve periodo ci erano nemiche, rimangono sul territorio. Il conflitto cessa,
noi non siamo più nemici di quelle nazioni, ma alleati. Tuttavia, quelle armi
che ci avevano sganciato contro, rischiano ancora di causare sofferenza. Come se
fossimo ancora nemici loro.

Il paradosso che hai scritto, secondo me non calza. Nell’imparare ad utilizzare
l’automobile, non c’è nessuno che ti insegni come investire persone nel modo più
efficace, o come causare il maggior numero di danni ad altri veicoli limitando i
morti accidentali. Impari a guidare l’automobile perché ti fa comodo saperti
spostare con un veicolo e lo scopo dell’insegnamento si limita a questo ed al
modo migliore di interagire con gli altri utenti della strada, mediante
l’apprendimento di regole più o meno sensate che riducano la possibilità di
errore. L’addestramento militare, è altra cosa. Si può dire che insegnino a
distruggere ed uccidere con criterio, magari limitandosi al massimo. Ma lo scopo
finale rimane quello. Un fucile viene costruito per causare danni e morte,
un’automobile per spostarsi velocemente e comodamente. Inoltre, la distinzione
fra oggetto utilizzato ed intenzione è molto importante, ma non bisogna
dimenticare che la nostra mente agisce in modo diverso a seconda delle
caratteristiche ambientali, che comprendono il corpo, gli oggetti adiacenti e di
uso più frequente e via sfumando fino a ciò che è più distante. Si tende a
modificare il proprio comportamento in base a ciò si cui si estende la nostra
mente. C’è una frase che mi piace molto che dice “Per un bambino con in mano un
martello, ogni cosa si trasforma in un chiodo”. Tornando ad esempi
automobilistici, come si può notare nella quotidianità, chi guida automobili più
potenti tende a voler sfruttare quella potenza, chi guida automobili più robuste
o che danno maggior senso di sicurezza, tende a sottovalutare i rischi del
proprio comportamento al volante, chi usa motociclette sportive (per prendere un
esempio a me vicino), tende a voler guidare troppo velocemente, e via dicendo.
La presenza costante di armi e strumenti costruiti per distruggere,
inevitabilmente va a modificare i nostri processi mentali, anche se
razionalmente ci costringiamo con una fortissima autodisciplina ed obbedienza.
La facilità e l'”umanità” del commettere errori è una cosa che non va mai
sottovalutata. Quanto più una persona ha potere (e le armi sono strumenti che
danno molto potere), tanto più saranno gravi le conseguenze dei propri
inevitabili errori.
Per motivi analoghi, anche gli altri esempi simili non mi sembrano calzanti.

Intanto questo… Appena possibile cercherò di rispondere anche al resto.

**mastercaution**
*on 27 agosto 2010 at 11:31*

Ciao, e grazie per la tua risposta, seria e pacata ogni oltre mia più rosea
speranza. Dico così perché in passato a pù riprese mi è capitato di affrontare
l’argomento, che mette a durissima prova le mie convinzioni progressiste, con
persone che denotavano una rigidissima chiusura ad ogni – benché minima – forma
di dialogo, addirittura inquadrandomi nella categoria degli “sbirri”. Non dico
argomentazione, dico proprio il semplice e civile dialogo. Attendo pertanto il
prosieguo del tuo pensiero, che suscita già parecchio interesse. Grazie e alla
prossima. Luca

**Masque**
*on 27 agosto 2010 at 18:13*

Provo a continuare. Le domande 1 e 2 sono legate. Lo stato eteronomico, è
applicabile ad ogni situazione in cui si può identificare un’autorità incarnata
o meno, oppure un ideale che si ritiene superiore. In tutti questi casi, la
propria individualità ed il senso critico vengono messi in grossa difficoltà.
Questo non significa che in uno stato eteronomico si possano esclusivamente
commettere azioni atroci, ma che si modifica il proprio comportamento in modo da
agire secondo ciò che l’ideale, l’autorità, il dio, vorrebbe che si agisse. Ci
sono state molte persone religiose, di qualsiasi religione (anche quelle che
consideriamo più “crudeli”), che diventando “agenti incarnati” della divinità,
hanno fatto cose molto buone per la gente, ed altre che hanno causato grande
sofferenza (pensiamo alle guerre sante, alle crociate, e via dicendo). Quindi,
l’entrare in uno stato eteronomico non causa, in sé, situazioni in cui si è
portati a infliggere dolore in modo distaccato, ma semplicemente ti pone in una
configurazione mentale che ti rende più difficile notare gli errori
dell’autorità o quelli insiti nella legge, o nel dogma a cui ci si sta dando. Si
rischia quindi di diventare veicoli amplificatori di questi errori. (Il discorso
sugli errori è quello che avevo scritto prima).
È un po’ lo stesso ragionamento che avevo fatto qua:
https://neuroneproteso.wordpress.com/2009/11/14/autorita-e-senso-comune-nella-morale-il-ruolo-della-religione/
Ma che allora non avevo ancora focalizzato così chiaramente.
Nelle successive domane, mi metti in evidenza il fatto che per evitare
situazioni perverse, sono stati creati dei precisi regolamenti.
Non posso negare che questi regolamenti spesso funzionino (anche se si nota più
spesso quando questi vengono applicati a scopo riparatore, più che di
prevenzione), e che in certi ambienti, la loro mancanza causerebbe disastri
terribili.
Io credo che siano proprio quegli ambienti (addestramenti all’uso di strumenti
mortali e distruttivi, addestramenti all’uso dell’autorità per soggiogare le
persone), ad essere talmente delle situazioni limite e difficili da gestire
emotivamente, per le persone, da dover richiedere la creazione di autorità e
regole sempre più forti. In questo modo si riesce a rimanere in bilico fra la
propria capacità di distruzione e di imporre la propria volontà sull’altro, e la
disciplina che ti impedisce di abusarne. Si pensa, credo, in questo modo, di
avere un potenziale utilizzabile in caso di necessità, e di riuscire a tenerlo a
freno quando esso non è necessario o causerebbe sofferenza ingiusta. Ma rimane
sempre una situazione “borderline”, che non tutti sono in grado di gestire. Ed
appunto, mi sorprendevo di come fosse possibile che una persona, scegliendo la
carriera militare, non potesse accorgersi di finire in una situazione simile,
dove il rischio di fare del male è talmente vicino da non riuscire spesso a
vederlo. Ci si deve fidare ciecamente di troppe variabili incerte, come il
proprio autocontrollo, la giustizia dell’autorità che ci da i comandi, la nostra
capacità di accorgerci degli eventuali errori di essa, il coraggio e la capacità
di uscire dallo stato eteronomico per poterla mettere in discussione (“e cosa
accade se invece sono io a sbagliarmi?”). Tutte cose che difficilmente si
possono prevedere fino a quando non le si sperimenta.
Per questo, io mi chiedo se una persona in procinto di iniziare una carriera da
militare, si faccia questo tipo di domande e se sì, secondo quale ragionamento
riesca alla fine a decidere di correre un rischio così grande.
Perché, se si vuole essere d’aiuto alla gente che abita sul proprio territorio,
si deve decidere di entrare a far parte di un corpo armato, e non invece in
qualche altra associazione o corpo di aiuto o che venga utilizzato in casi di
estremo pericolo civile (vigili del fuoco, ad esempio)?
So bene che le società attuali sono costruite in modo da rifiutare la violenza
generalizzata, delegandone però l’uso a poche persone scelte che, teoricamente,
dovrebbero essere addestrate per farne un uso saggio. Io sono convinto però, che
non esista un uso saggio della violenza, anche se storicamente, essa è stata
talvolta funzionale nel fermare pericoli e possibili violenze peggiori. Ma i
pericoli e le possibili violenze peggiori, li possiamo distinguere solo
guardando il passato. Per quanto riguarda l’ora ed il futuro, non possiamo far
altro che stimare, indovinare… e seppur delle volte possa essere facile questa
stima, moltissime altre no. E rischieremo di illuderci, di credere che la nostra
previsione sia corretta anche in caso contrario, e la porteremo avanti con la
stessa convinzione che abbiamo ogni volta che siamo certi di una nostra
previsione.
Forse ora si riesce a capire meglio il mio rifiuto della violenza e dei corpi
armati. Sono, per me, troppo pericolosi, e troppo alto vedo il rischio di
perdita di controllo.
Penso che tutti vorremmo arrivare ad una civiltà senza violenza e senza
sofferenza inflitta dagli uomini sugli uomini e sulle altre specie, ma penso che
non ci arriveremo mai se continueremo a pensare che delle volte la violenza
possa essere giustificata. Perché anche se animati di buoni propositi, di fatto,
continueremo a perpetrane l’idea.

Mi sono un po’ dilungato. Poi vedrò se riesco a scrivere qualcosa anche sul
resto del tuo commento.

**Masque**
*on 28 agosto 2010 at 18:37*

Proseguo. Al resto del commento, ho in parte già risposto nel messaggio
precedente. Anche sull’uso dell’autorità per tenere a freno il pericolo di abuso
di potere.
Mi fa piacere sapere che un militare possa rifiutarsi di eseguire un ordine se
illegittimo. Quello di cui non sono sicuro però, è quante possibilità abbia un
“esecutore”, di comprendere l’illegittimità di un ordine (escludendo cose palesi
come: bombarda l’ambulanza, lancia una granata sul civili, e via dicendo) e
quanto questo pensiero critico venga incoraggiato o, al contrario, osteggiato.
Mi spiego meglio. Che possibilità avrei, da soldato di basso grado, di conoscere
abbastanza dei piani ai quali appartengono gli ordini che mi sono stati dati, in
modo da farmi un precisa idea dello scopo e delle ripercussioni che avranno le
mie azioni? Che possibilità ho di sapere se una persona qualsiasi nella catena
di comando sta mentendo o nascondendo qualcosa in modo da far eseguire comunque
degli ordini che, se accompagnati dalla verità, potrebbero risultare
inaccettabili? Ho questa possibilità, oppure devo avere fede nei miei superiori?
Quanto vengo incoraggiato o scoraggiato ad agire di testa mia? Nel caso mi
rifiutassi di eseguire un ordine e denunciassi chi me l’ha dato, se nel processo
che seguirà (suppongo che ci sarà un processo o qualcosa di simile) risultasse
che sono stato io invece a sbagliare, cosa rischierei? Cosa rischierei invece se
comunque avessi avuto ragione? Siamo sicuri che l’ambiente non sia tale da
inibire questo genere di azioni individuali, anche se formalmente permesse?
È un po’ come il discorso sulla stampa che spesso si fa nei riguardi delle
testate giornalistiche che appartengono a Berlusconi, suo fratello o qualche suo
associato (questo discorso lo espongo come dato di fatto, perché credo che sia
abbastanza lampante anche per chi è seguace suo): è molto difficile che un
giornalista, in un ambiente come quello che abbiamo in questo paese, scriva
qualcosa di veramente critico sul proprio padrone, non perché abbia ricevuto
ordini precisi di non farlo, ma per un misto di inconscio timore e senso di
appartenenza e gratitudine verso chi gli da da lavorare.
Volendo prendere una situazione diametralmente opposta, parlerei del metodo
scientifico. Nelle scienze, la confutabilità di una teoria è una cosa richiesta
e fondamentale. Gli scienziati vengono incoraggiati a trovare falle nelle teorie
e gli errori scoperti sono di estrema utilità per il progresso. Questo perché lo
scopo della ricerca scientifica, è di avvicinarsi il più possibile alla verità.
Lo scopo dell’esercito non è di avvicinarsi alla verità, ma di eseguire nel modo
più efficiente operazioni delicate (che possono essere di violenza, distruzione
dell’avversario, ma anche di salvataggio). L’esercito credo che non si possa
permettere di lasciare troppa libertà confutatoria ai suoi appartenenti (in caso
contrario, non esisterebbero nemmeno delle ferree gerarchie), perché questo
scardinerebbe l’efficienza. Il prezzo da pagare è l’essere pronti a commettere
un certo numero di errori disastrosi, ma che in qualche modo siano accettabili
per via delle molte altre operazioni ben riuscite. Il problema nasce quando
questi errori causano danni e vittime fra le persone civili. Si rende necessario
parlare di numeri, ma ogni persona è in individuo con legami affettivi ed una
storia personale ed unica.
Potrei fare un paragone con il metodo induttivo. L’induzione è un meccanismo che
da tanti casi singoli, tenta di costruire una regola generale. Può essere una
frequente causa di illusioni e falsi, ma la selezione naturale “ce l’ha
lasciata” perché in certe situazioni di sopravvivenza, era più conveniente per
l’uomo avere dei falsi positivi, che il contrario. Per l’uomo primitivo era più
conveniente indurre che il movimento di un cespuglio fosse causato da una belva
e quindi decidere di fuggire, che non perdere tempo per indagare e quindi
rischiare di venire sbranato. Questo anche se altre volte il movimento non era
causato da belve e l’uomo fuggiva inutilmente (magari lasciando per terra tutto
il cibo che aveva fino a quel momento raccolto). È una situazione delicata e
critica, dove viene privilegiata l’efficienza alla ricerca della verità.
Ma… http://it.wikipedia.org/wiki/Tacchino_induttivista

La frase si Malatesta, secondo me, andrebbe letta alla luce di tutto ciò che ho
scritto in questi commenti, specialmente in quello dove ho scritto dell’uso
degli oggetti e di come si modifica la mente in base all’ambiente e quando ho
scritto dell’uso della violenza per prevenire violenze peggiori (che di fatto
perpetua l’idea che la violenza possa essere giustificata).
Di certo, in QUESTA società, togliere improvvisamente le forze di polizia
causerebbe disastri. Tutta la società è costruita su schemi competitivi,
conflittuali ed opportunistici che quindi, necessitano purtroppo, di qualcuno
che gestisca la violenza.
Per poter eliminare la violenza e la necessità di qualcuno che la debba gestire,
è prima necessario modificare la società dal basso, passano da uno schema
competitivo ad uno di mutua collaborazione, di parità di diritti e poteri e via
dicendo. È necessario che ognuno educhi se stesso, che si renda esempio vivente
della società futura, in questo modo, forse, c’è la possibilità che poco alla
volta emergano piccole chiazze di cambiamento. Le rivoluzioni, l’abbiamo
imparato dalla storia, o non durano, oppure si trasformano in altre dittature.
Malatesta, ovviamente, parla con in mente l’ideale anarchico, nel quale la
perfetta parità, l’eliminazione di ogni forma di sfruttamento e l’aumentata
responsabilità individuale (necessaria per saper gestire la maggiore libertà),
_dovrebbero_ eliminare alla radice le cause della criminalità e della violenza.

**Masque**
*on 28 agosto 2010 at 18:46*

Un’aggiunta: se nell’esperimento carcerario potrebbe aver senso ciò che hai
scritto (cioè che con una gestione più consapevole ed autoritaria del potere, si
sarebbe evitato il disastro… ma mi chiedo però, fino a che punto si può
aumentare il controllo autoritario, diminuendo l’autonomia degli esecutori. il
discorso è sempre quello… in queste situazioni, è inevitabile che si sia sempre
in un pericolosissimo bilico), la stessa cosa non si può dire per l’esperimento
Milgram.
http://it.wikipedia.org/wiki/Esperimento_Milgram

**Marco Carminati**
*on 3 settembre 2010 at 14:31*

Bello il titolo del vostro portale! E molto indovinato.
Appunto “il (l’unico) neurone” che vi gironzola nella zucca …vuota. Forse più
che “proteso” lo chiamrei però “preteso”, perchè vanamente pretenderebbe di
avere dignità di cellula cerebrale pensante…
Il fatto è che decerebrati, disertori di ogni valore morale e civile, canaglia
codarda, possono tranquillamente continuare a infestare la terra come il muschio
le piante sane e svettanti, perchè al mondo ci sono quegli imbecilli di miltari
che ci lasciano la vita per darvi la possibilità di dire cazzate a ruota libera.

**Masque**
*on 6 settembre 2010 at 18:55*

È possibile che tu abbia scritto questo commento affrettatamente. Forse non hai
letto i commenti precedenti, in particolare quello di Mastercaution e le mie
risposte.

Sei libero di pensarla come vuoi. Il mio non era un post per cercare di
convincere i lettori, ma un tentativo di esprimere la mia sorpresa su come fosse
possibile che chi decide di iniziare una carriera militare non si ponga certi
dubbi (che, per quanto mi riguarda, sono importanti) e se se li pone, grazie a
quale meccanismo li riesce a scansare.

Come ho scritto nella prima risposta a Mastercaution, so di avere una mia idea
a-priori (com’è normale che sia) e sono consapevole del mio punto di vista
parziale, di persona convintamente pacifista e nonviolenta. Sono anche
consapevole della mutevolezza del mio pensiero.
La natura “aleatoria” e mutevole delle idee che esprimo nei miei post, è ancora
di più accentuata dal fatto che scrivo sotto pseudonimo e che non rivendico
alcuna paternità o merito sui contenuti originali del blog (come si può leggere
nel riquadro in alto della homepage).

Libero di pensare che io scriva cazzate a ruota libera e che la mia intelligenza
sia subnormale, data dalla presenza di un solo neurone (senza alcuna connessione
con altri, non può essere “cellula pensante”, dato che l’intelligenza e la
coscienza nascono dalle relazioni fra le varie parti e non dalle singole
separate. per questo, il poveretto si protende nel tentativo di raggiungere con
il suo dendrite la sinapsi di un altro suo simile). Ma se a dirlo sono io, dico
una cosa quasi ovvia, ammettendo la mia fallibilità (naturale ed inevitabile),
se invece a farlo è qualcun altro, appena arrivato, che irrompe in modo
aggressivo, questa persona deve aspettarsi di venire giudicata negativamente dal
lettore occasionale.

**[Gianluca Bartalucci][12]**
*on 7 settembre 2010 at 12:29*

“Il fatto è che decerebrati, disertori di ogni valore morale e civile, canaglia
codarda, possono tranquillamente continuare a infestare la terra come il muschio
le piante sane e svettanti, perchè al mondo ci sono quegli imbecilli di miltari
che ci lasciano la vita per darvi la possibilità di dire cazzate a ruota
libera.”

Ci manca un “W il Duce”. Ah no, anche lui da giovane fu arrestato per
diserzione.

**Masque**
*on 7 settembre 2010 at 13:42*

Quel “canaglia codarda” aveva fatto pensare subito anche a me, al modo tipico di
esprimersi di quel periodo e che spesso viene riutilizzato dalle persone che
giudicano positivamente il fascismo italiano. Forse l’uso di quelle espressioni
viene fatto per il fascino estetico del linguaggio, o anche come modo di
riconoscersi fra loro senza dover esplicitare chiaramente (e rischiosamente) la
propria ideologia.

Vabbeh. Il tipo (che tra l’altro ho scoperto essere uno scrittore) non sembrava
comunque interessato alla discussione.

**[RobertoG][13]**
*on 16 febbraio 2013 at 19:03*

Molte persone si arruolano per il posto fisso e preferirebbero stare lontano da
possibili guai, ricordo che poter uccidere significa anche poter essere uccisi,
sono quindi soprattutto motivazioni economiche a spingerli. Ci sono quelli che
lo fanno perchè desiderano una vita operativa e questi lo fanno o perchè
vogliono misurarsi con qualcosa di forte, essere attivi, o perchè credono in
valori come la patria, la bandiera, il gioco di squadra ecc… in genere sono
sempre valori positivi a spingere queste persone.

**Masque**
*on 16 febbraio 2013 at 21:27*

Ma se la priorità di una persona è quella di avere un posto fisso e sceglie di
arruolarsi, significa che ritiene meno importante o sottovaluta il rischio di
dover uccidere delle persone. È vero che rischia di farsi uccidere, ma data la
scelta fatta, probabilmente ritiene poco probabile che accada, altrimenti perché
rischiare tanto? Non so cosa intendi per “vita operativa”, è un modo di dire che
non ho mai sentito, tuttavia se scelgono di “misurarsi con qualcosa di forte”,
pur sapendo che questo tipo di misura implica l’uccisione, significa che danno
maggiore priorità alla propria individuale emozione, rispetto alla vita altrui,
oppure che ritengono la possibilità di dover uccidere talmente remota da non
considerarla sufficiente (ma in questo caso, non si percepirebbe il “qualcosa di
forte” con cui misurarsi). Eppure, i dati sui morti nei conflitti sono molto
chiari, così come le testimonianze registrate. Mai sentito parlare di suicidi di
soldati, ormai tornati nel proprio paese, causati da disturbo post traumatico?
https://corrieredellacollera.com/2012/11/27/u-s-a-i-suicidi-nelle-ffaa-sono-fuori-controllo-il-pentagono-non-provvede-di-antonio-de-martini/
Il lavoro del soldato implica la possibilità di uccidere e di venire ucciso, è
inutile e falso girarci intorno o fingere che non sia così.
Chi si vuole arruolare deve essere sincero con se stesso, e capire se lo sta
facendo perché intende rendersi strumento di un ideale, o provare forti
emozioni, o guadagnare un ottimo stipendio e che non importi chi e quanti
debbano morire. Oppure ritiene di poter fare il soldato senza correre mai il
rischio di uccidere. Perché nel secondo caso, sta sottovalutando la realtà,
mentendo a se stesso, oppure illudendosi. Sono sicuro che una sincera
introspezione riguardo a questo, potrebbe portare ad discreto shock morale. In
entrambi i casi, la possibilità di dover uccidere sarà sempre presente,
indipendentemente da ciò che si può credere.

Quanto a patria e bandiera, si potrebbe discutere e divagare ma preferisco non
farlo. Suppongo però che quando una persona dice di voler difendere la patria,
lo faccia perché con questo intende dire che vuole difendere le persone che gli
stanno vicino e che gli sono care. In questo caso, mi verrebbe da pensare che
questa persona si sta illudendo. Sono ormai alcuni decenni che gli eserciti dei
paesi ricchi non vengono utilizzati a scopo di difesa del proprio paese, ma al
contrario, come forza di occupazione in paesi esteri o come forza repressiva
all’interno dei propri confini.
Non dubito che una persona pensi di se stessa di essere buona e di seguire
valori positivi. Quello su cui voglio mettere il dubbio, invece, è: sicuri che
non vi stiate illudendo? Non stiamo parlando nemmeno di arruolarsi in un periodo
di guerra in cui il territorio in cui vive ed in cui vivono le persone care è
minacciato da un esercito avversario. Quello è accaduto in passato e,
probabilmente, è proprio quest’idea romantica della difesa eroica dei propri
cari da un avversario agguerrito, ad attirare molte persone idealiste… Ma in
questo periodo storico, la realtà è molto diversa.

Consiglio la lettura di questo articolo, che si presenta come un parallelo molto
interessante a questo discorso dal punto di vista psicologico e morale:
https://neuroneproteso.wordpress.com/2012/03/10/lavorando-in-incognito-in-un-macello-un-intervista-con-timothy-pachirat/

**elteo**
*on 13 giugno 2013 at 00:03*

penso che una persona quando decide di fare il soldato non pensi al fatto che
dovrà uccidere, certo sono situazioni che possono accadere, ma presumo che ogni
persona che si accinga a fare questo lavoro abbia l’idea di mettersi al servizio
della sua patria nel caso in cui operi sul suolo del suo paese o di altre
persone nel caso operi su suolo straniero. Penso che un soldato che operi ad
esempio in Afghanistan si preoccupi tutti i giorni di quanto fa per i civili, di
quanti civili vengono a farsi curare negli ospedali delle varie basi militari,
di quanti bambini riesce a salvare anche regalandogli solo un sorriso. Penso che
la professione del soldato durante le missioni di pace sia molto simile a quella
del medico, chiamatemi ipocrita ma penso proprio che quello che muove un
aspirante militare a diventare un militare sia tutto meno il pensiero di
uccidere.

**Masque**
*on 13 giugno 2013 at 07:20*

Il discorso che faccio e’ molto di parte. E’ chiaro che sono pacifista ed
antimilitarista. Tuttavia mi interessa capire. Da quello che mi scrivi
sembrerebbe che una persona decida di far il soldato per fare cio’ che fanno
quelli di Emergency, che e’ una associazione che esiste solamente perche’, in
primo luogo, esistono gli eserciti.
Questo non fa che confermare la mia idea che chi decide di fare il soldato, stia
seguendo delle illusioni.
Se hai visto dei notiziari o letto degli articoli di gente, non militare, che e’
stata in Afghanistan, in Iraq, in Libia, o in uno dei molti altri stati un cui
la Nato sta facendo guerre, avrai visto che la popolazione civile, questi
“liberatori”, non li vuole, ha paura di loro o li odia. La popolazione civile
viene ammazzata, anche, da questi liberatori, talvolta come “danno collaterale”
altre volte, come s’e’ visto in alcuni scandalosi video, di proposito. (
https://www.youtube.com/watch?v=to3Ymw8L6ZI )
Ora si potrebbe fare il discorso sulle “mele marce”… il fatto e’ che se metti
delle mele buone in un cesto di mele marce, e’ molto probabile che marciscano
anche quelle buone. Soprattutto considerando che l’esercito e’ un’organizzazione
il cui mezzo e’ l’utilizzo della forza, della violenza e delle armi, ed i cui
scopi variano a seconda delle necessita’. Si potrebbe dire che e’ proprio il
cestino, ad essere il primo diffusore del contagio. Ovvero, tornando alla
realta’, se insegni un metodo violento, non ti puoi aspettare che chi s’e’
addestrato, rendendo, quindi, abitudine gli insegnamenti acquisiti, quando
agisce, ottenga risultati che non implichino la violenza.
Sarebbe una grandissima sorpresa per la didattica, scoprire che, se insegni per
anni ad una persona ad utilizzare le armi ed obbedire alle autorita’, questa ne
esce, poi, come una persona dal pensiero indipendente e pacifico, col desiderio
di curarsi del prossimo.
Come dire che ad andare a studiare dai preti, ne esci un libero pensatore ateo.
Talvolta capita, ma non per merito dell’insegnamento, ma per il rifiuto dello
stesso.
Piu’ che una sorpresa, implicherebbe di dover rivedere completamente intere
teorie psicologiche, comprese scoperte recenti come quella dei neuroni specchio,
e tutto quello che si sa sull’apprendimento.

**Baker**
_on 11 novembre 2015 at 07:36_

Salve, penso tu abbia le idee un po’ confuse riguardo al lavoro del militare, ed in generale delle forze armate, che siano esse marina, esercito, aeronautica o carabinieri.
Come giustamente hai fatto notare si è militari per scelta, e non è mai una scelta facile, ma non perché sappiamo che prima o poi uccideremo persone inermi!
È una strada difficile perché richiede passione e dedizione costante, perché richiede sacrificio, perché si è lontani da casa per molto tempo, e perché, in generale è una vita dura.
La gente poi, spesso disegna il militare come un soldatino senza volontà propria, al contrario! Il militare italiano ha una coscienza ed un cervello che funzionano più che bene, ha facoltà di non eseguire un ordine che vada contro i principi etici e morali propri della nostra società o che possa nuocere alla vita sua, dei suoi colleghi o di eventuali terzi, e l’addestramento che svolge, non rende macchine per uccidere che eseguono ordini, ma forma uomini e donne a quelle che sono le necessità di un eventuale luogo di operazione, sia in patria, sia all’estero, non insegnano solo a sparare, ma anche a prestare primo soccorso, a mettere atto posti di blocco, a scortare e proteggere persone e cose ad organizzare un centro recupero rifugiati in zone di guerra e così via dicendo, e da qui puoi ben capire che il lavoro del soldato non è soltanto portare morte e distruzione, certo, siamo comunque soldati, per cui il rischio di dovere usare un’arma c’è, ma credimi, nessuno darà mai ordine di far saltare in aria villaggi o sparare addosso alle persone, forse ti sarai fatto influenzare da qualche video su internet, ma credimi l’Esercito Italiano non lavora così.
Le motivazioni che spingono ad arruolarsi non sono un inganno, perché nessuno le ha imposte, ma sono proprie del soldato. Mi sono arruolato per aiutare chi è in difficoltà, dovunque esso sia, indipendentemente dal credo che professa o dal colore della pelle, per proteggere chi non può difendersi da chiunque lo minacci, per difendere non l’Italia, bensì gli Italiani: la mia famiglia, i miei amici, TE, tutti noi.
Nessun trucco, nessun inganno, quel che mi spinge è una motivazione personale, non è venuto nessuno a dirmi “guarda che servire lo Stato è importante”.
Prima di essere soldato, sono un uomo con un mio pensiero, una mia coscienza e miei principi etici e morali.
Ah e per ultima cosa, l’Italia non è in guerra con nessuno, partecipa a missioni (due cose completamente differenti) e sono tutte missioni umanitarie, cioè di supporto alle popolazioni e che ci si vada coi fucili a farle è perché qualcuno minaccia l’incolumità di queste persone, e che purtroppo non possono essere fermate con le parole o i buoni propositi, ma si cerca sempre e comunque di limitare i danni.
Bè detto questo, ho finito, spero di aver chiarito un po’ di cose che spesso, grazie ai film, ai libri od altro vengono troppo romanzate e portate all’esagerazione (e ricordo a tutti che non siamo più nel 1915-1945, gli errori e le atrocità che sono stati commessi in quel periodo non ci appartengono più, e non si ripeteranno mai più)
Comunque sia quella che ho fatto non è assolutamente una critica o rimprovero nei tuoi confronti o in quelli di altri, ripeto, ho voluto semplicemente togliere quel velo di inconsapevolezza che è presente quando qualcuno che non è militare o non conosce militari ha, e mitezza questa figura.
Grazie dell’attenzione

**Masque**
_on 11 novembre 2015 at 13:56_

Perdonami se taglio corto, ma la domanda fondamentale è: “Accetti di uccidere qualcuno per conseguire un obiettivo che tu ritieni più ‘alto’?”
Non prendiamoci in giro: nell’addestramento vi è anche questo, ciò significa che si deve essere pronti a farlo. Quindi, la questione diventa, come dicevo: o si sottovaluta questa eventualità, oppure la si ritiene meno importante di altre.

Ricordo inoltre che patologie come la sindrome da stress post traumatico, che colpisce frequentemente persone che lavorano nell’esercito in seguito a situazioni estremamente scioccanti nelle quali si sono loro malgrado o volontariamente trovati nello svolgimento di missioni, non me la sono inventata io. Gli studi di psicologia sociale sull’influenza dell’autorità nella propria capacità di prendere decisioni e di giustificare le proprie azioni, non me li sono inventati io.

Quanto alle missioni in corso, sul sito del ministero della difesa sono elencate, ma le descrizioni sono assai lacunose.
http://www.difesa.it/OperazioniMilitari/Pagine/OperazioniMilitari.aspx

Mi dispiace, ma non penso che potrò mai venire a compromessi su questa questione. Per me uccidere e causare sofferenza sono azioni imperdonabili e metterle in atto razionalmente, allenarsi per poterle svolgere più efficientemente, è quanto di più disumano possa immaginare.

Quando si uccide qualcuno, non si toglie solo di mezzo una persona o qualcuno che magari avrebbe potuto causare a sua volta sofferenza e dolore, ma si toglie dall’esistente un essere profondamente radicato.
Ogni singola persona è un universo di complessità, di storie e di profondità. Quella persona che normalmente classificheremo come “il tizio che ha gettato la cartaccia a terra” è il proprio centro di una rete di relazioni con altre persone e con l’ambiente ed il mondo e racchiude diversi scaffali di libri di storie personali. In sostanza è molto più profondo e radicato nel mondo, di quanto non ci appaia a prima vista. Forse è proprio questo che della morte sconvolge: quanto viene tolto dall’esistente nel momento in cui una persone smette di vivere.

Nel soggettivo, siamo abituati ad avere relazioni con molti esseri viventi, i nostri pensieri e le azioni si estendono su di loro e, come in un feedback, i nostri stessi pensieri ed azioni sono conseguenze di ciò che sono loro, di cosa fanno o del semplice essere presenti e percepibili. Diventano come un’estensione di noi stessi e nel momento in cui vengono a mancare, sentiamo una sensazione come di amputazione. Fino a quando l’abitudine della loro presenza è forte, ci capiterà spesso di agire e pensare come se essi ci fossero, per scoprire poi, con sconforto, che l’azione ed il pensiero che facciamo non arriva alle conseguenze che ci siamo abituati ad aspettare. Si ha quindi una sensazione che potrebbe essere simile a quella dell'”arto fantasma”, nei casi di persone amputate.

Quando qualcuno muore, non muore solo lui, ma si sradica tutta una rete di relazioni, ed il dolore che lui non può provare più, essendo morto, lo provano tutti coloro che con lui avevano una relazione, tutti coloro che hanno visto la sua morte ed empatizzato con la sua situazione.

**gerardo**
_on 14 gennaio 2016 at 17:51_	

allora proviamo a rispondere alla moltitudine di scemenze che hai scritto..
Hai minimamente idea di cosa sia un addestramento militare? suppongo che la tua conoscenxa provenga da filmati sul web e tanti film, il soldato non è addestrato ad uccidere (come qualsiasi membro delle forze armate) anche in missione si va per rispondere a degli obiettivi ben precisi .

**Masque**
_on 14 gennaio 2016 at 18:37_

La risposta alla moltitudine di scemenze dove sarebbe?

Quindi le armi che ci si porta appresso, per cosa si è addestrati ad usarle? Vorrei far notare che nessuno ancora ha risposto ad una semplice domanda: “Accetti che vi sia la possibilità che tu debba uccidere qualcuno nello svolgimento delle tue mansioni, che sia accidentalmente o che la tua mansione/obiettivo/ordine lo preveda esplicitamente?”
A seconda della risposta si possono dedurre due cose:
1) Hai accettato di intraprendere una carriera che, di fatto prevede questa possibilità, anche se non vuoi uccidere, illudendoti che non ti sarebbe mai capitato di doverlo fare.
2) Accetti che uccidere una persona possa essere meno importante di conseguire un obiettivo.

No, l’idea che mi sono fatto non deriva solo del web e dai film. Conosco almeno una persona che è stata in servizio nella guerra in ex Jugoslavia e che si è trovata ad uccidere. Gli studi che ho citato, come pure la ben nota sindrome da stress post traumatico non sono invenzioni cinematografiche, ma al contrario sono state utilizzate come soggetti per alcuni film. Meglio non mescolare cause ed effetti.

[1]:https://it.wikipedia.org/wiki/Esperimento_Milgram
[2]:https://it.wikipedia.org/wiki/Esperimento_carcerario_di_Stanford
[3]:http://www.ilfattoquotidiano.it/2012/03/11/afghanistan-soldato-americano-strage-civili-morti-raptus-follia/196593/
[4]:http://frontierenews.it/2012/03/afghanistan-follia-di-soldati-americani-due-donne-violentate-e-uccise/
[5]:http://liberthalia.wordpress.com/2008/11/04/grazie-ragazzi/
[6]:http://www.ilfattoquotidiano.it/2010/07/24/il-massacro-di-falluja-ha-avuto-conseguenza-peggiori-anche-di-hiroshima/43745/
[7]:http://www.ilfattoquotidiano.it/2010/07/25/le-menzogne-sulluranio-da-hiroshima-a-falluja/43933/
[8]:https://it.wikipedia.org/wiki/Boris_Vian
[9]:https://www.youtube.com/watch?v=kAndnhWomEE
[10]:http://www.antiwarsongs.org/index.php?lang=it
[11]:http://www.antiwarsongs.org/canzone.php?lang=it&id=1
[12]:https://some1elsenotme.wordpress.com/
[13]:http://www.informaweblog.com/
