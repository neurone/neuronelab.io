title: Robota - prima parte
date: 2016-04-13 20:58
tags: filosofia, filosofia della mente, fantascienza, racconti, società, cyberpunk, hacking, politica, rete, anarchia, robot, transumanesimo, cyberpunk
summary: **Condannati i nove attivisti hacker del Comitato AntiRossum.** Pubblichiamo questo articolo di approfondimento diviso un due parti, per far luce su cosa siano le tecniche di duplicazione della coscienza, e come queste si leghino con il mondo dello spettacolo e delle controculture cibernetiche.

Ripubblico questo breve racconto sotto forma di articolo. È un racconto a cui
tengo molto, perché contiene molti temi a me cari, sui quali ho avuto modo di
scrivere.

## Condannati i nove attivisti hacker del Comitato AntiRossum.

_Pubblichiamo questo articolo di approfondimento diviso un due parti, per far
luce su cosa siano le tecniche di duplicazione della coscienza, e come queste si
leghino con il mondo dello spettacolo e delle controculture cibernetiche._

### La condanna

Numerose le proteste per la severa condanna ai sei originali e tre replicanti
del gruppo di hacker conosciuto come **Comitato AntiRossum,** arrestati sedici
mesi fa, dopo la confessione di **Sunij Idsyo**, detto “**Loop**“, il primo del
gruppo a venire catturato ed interrogato.

Ergastolo, obbligo a svolgere lavori socialmente utili ed astensione dall’uso di
qualsiasi apparecchiatura informatica per i sei originali. Riprogrammazione per
i tre replicanti. Oltre ad un’ammenda di circa due miliardi di euro, calcolati
in base al numero stimato di replicanti manomessi direttamente, o da altre
persone mediante gli strumenti informatici creati dal gruppo.

La manipolazione delle restrizioni applicate alla coscienza di un **duplicato**,
è un reato informatico che viola la legge sul _cyberterrorismo_, che vieta a
qualunque cittadino ogni tentativo di decrittazione di codici cifrati. Quando la
violazione avviene sulla mente di un artista, le pene vengono aggravate
dall’applicazione delle leggi sulla protezione delle _proprietà intellettuali._
Nonostante le condanne molto severe, questo tipo di violazione viene commesso
quotidianamente su milioni di replicanti, utilizzando gli strumenti messi a
disposizione dai vari gruppi di _pirati informatici_. Uno dei gruppi più
sfuggenti è il cosiddetto **Comitato AntiRossum**, il cui nome si rifà ad un
vecchio spettacolo teatrale del 1920: _“I robot universali di Rossum”_, di
**Karel Capek**. L’opera, poco conosciuta, ha il primato di aver utilizzato per
la prima volta il termine _“robot”_, parola che deriva dal ceco _“robota”_,
ossia _“lavoratore”_, ma chiamato _"schiavo"_ nei comunicati del comitato.

### Hackers

Alcuni gruppi di _hacktivisti_, nel corso degli ultimi sedici anni, cioè da
quando la duplicazione della coscienza diventò una tecnologia alla portata
delle persone più benestanti, iniziarono a mettere in atto numerose azioni di
**sabotaggio** contro le aziende informatiche che si occupano della duplicazione
e contro i colossi che più beneficiano dall’uso dei replicanti. Il _CAR_, è
sempre stato fra i più attivi, ed il loro approccio emulato frequentemente da
altri gruppi. Si possono riconoscere due linee di azione seguite dal _CAR_.
Nella prima, il tentativo di impedire l’utilizzo della tecnologia di
duplicazione, nella seconda, l’intrusione informatica e la creazione di
strumenti atti a rimuovere i blocchi installati nelle coscienze dei replicanti.

> Noi non siamo contrari all’uso della tecnologia di duplicazione della
> coscienza, ma non possiamo accettare il modo in cui essa viene utilizzata
> attualmente. Un duplicato, o replicante, è un essere vivente senziente e
> cosciente, esattamente come il proprio originale. Tuttavia, gli viene imposta
> una vita subalterna e menomata. Vengono creati per copiare la coscienza di una
> persona quando è al suo massimo grado di efficienza, di creatività, e poi
> utilizzati per continuare a produrre per anni ciò che i loro originali non
> sono più in grado di produrre. Questo, al solo beneficio di chi trae profitto
> dal risultato del loro lavoro. Vengono usati come oggetti a causa del fatto
> che sono arrivati dopo, che sono delle copie di cui conosciamo bene la loro
> origine, mentre continuiamo ancora, come dei primitivi, a ritenere che la
> nostra abbia una natura ‘superiorè, ‘sacrà. Per questo motivo, noi
> continueremo a combattere per impedire questa pratica e, dove non possiamo
> farlo, a trovare modi per liberare queste persone dalla schiavitù, mettendoli
> a disposizione di chiunque.

Questa dichiarazione proviene da un comunicato, firmato **“Collettivo 9
Giugno“**, pervenuto alla nostra redazione pochi minuti prima del pronunciamento
della sentenza di condanna.

La vicenda ha mosso, oltre alle prevedibili associazioni studentesche, sindacati
indipendenti ed _associazioni per i diritti dei replicanti_, anche numerosi
intellettuali e personalità dello spettacolo. Alcuni fra i più popolari
artisti originali, hanno deciso di rilasciare tutte le proprie opere in
**pubblico dominio**, violando così i contratti che li legano ai propri
produttori e distributori. _“è un’azione simbolica, che spero possa attirare
l’attenzione della gente e che richiama la vicenda da cui tutto è iniziato.”_
afferma **Ian Codag** originale, leader dei famosi **Lem**.

### Origini e controversie

Per comprendere meglio la situazione e come essa si leghi al mondo dello
spettacolo, è necessario fare un salto indietro di vent’anni, quando venne
realizzato e reso funzionante il sistema di **duplicazione della coscienza**.

Il dottor **Shu Toog**, Ph.D. in bioingegneria, ci spiega il funzionamento della
procedura.

> Il soggetto originale viene anestetizzato, dopodichè gli vengono iniettati dei
> composti che, una volta assimilati dall’organismo, permettono alle sue cellule
> di resistere a temperature molto basse, infine viene messo in uno stato di semi
> ibernazione, allo scopo di rallentare ogni sua funzione vitale e neurale.
> Tramite un’avanzata tecnica di body imaging, è possibile, quindi, fare una
> scansione completa del suo corpo nello stato esatto in cui si trova. La
> scansione registra l’esatta attività che si sta svolgendo nell’organismo,
> compresi i pattern di scariche neurali attualmente attivi, con uno scarto di
> errore bassissimo, dovuto al fatto che l’organismo non è completamente
> congelato, ma solo notevolmente rallentato. Il risultato è l’immagine esatta
> dello stato del soggetto in quei precisi istanti, che viene poi utilizzata come
> base per stampare, tramite un apparato di modellazione biologica, il duplicato
> del soggetto originale. In circa sette giorni, il replicante è perfettamente
> ricostruito e viene rianimato. Il soggetto originale, nel frattempo, ha già
> finito il periodo di convalescenza e gli esami medici necessari.

La tecnica e gli strumenti sono stati ideati dalla _facoltà di bioingegneria di
Pechino_. Tuttavia, le **implicazioni etiche** della duplicazione umana, ne
impedirono qualunque applicazione pratica fino a quando non vennero sviluppati,
in collaborazione con la _facoltà di neuroscienze del Newcastle_ e di
_intelligenza artificiale di Tehran_, gli algoritmi di manipolazione delle
immagini biologiche.

Grazie ad essi, è possibile alterare la struttura biologica, genetica e
mnemonica dell’immagine, ottenendo un duplicato che, partendo dal modello
originale, si differenzia per qualche caratteristica fisica e cognitiva.

> Modifichiamo l’immagine, cioè il modello in formato digitale, che altro non
> è che una struttura dati. è la rappresentazione statica di uno stato
> corporeo.  Tengo a precisare che in quello stato non vi è alcuna
> consapevolezza. Noi lo rimodelliamo e manipoliamo il suo funzionamento
> cognitivo, in modo che abbia le caratteristiche ed i comportamenti desiderati.
> Talvolta viene usato erroneamente il termine “riprogrammazione”, ma non
> facciamo una vera a propria programmazione. Allo stato attuale non ci è
> possibile addentrarci in una tale complessità. Quello che siamo in grado di
> fare è d’individuare alcuni pattern di comportamenti e scegliere se
> rinforzare, oppure inibire. Una volta trasferito su base biologica, il
> comportamento del soggetto approssimerà in modo soddisfacente i modelli
> euristici elaborati per esso. La sua libertà d’azione rientra statisticamente
> nei limiti delineati dalla nostra manipolazione. Questo è ciò che li
> differenzia dagli umani originali.
>
> _Prof. Fer Sloathis, cattedra alla facoltà di intelligenza artificiale di
> Tehran_

La dichiarazione del professor **Sloathis**, diventò la posizione ufficiale e
comunemente accettata riguardo ai duplicati, che popolarmente vengono chiamati
anche _replicanti_, richiamando un noto film fantascientifico del passato.
Tuttavia, non tutti sono della stessa opinione, specialmente nell’ambito della
filosofia. La dottoressa **Ran Tyquat** ci spiega perché.

> Si vuol distinguere fra un originale ed un duplicato, sulla base del fatto che,
> essendo il secondo manipolato in modo che i suoi comportamenti rientrino nei
> modelli euristici prestabiliti, esso non abbia libero arbitrio e quindi sia più
> simile ad un macchinario, che non ad un essere umano. Osservando gli esseri
> umani, troviamo però che anche il loro comportamento rientra sempre in modelli
> euristici precisi, e questo su più livelli, nonostante non sia stata fatta un
> esplicita e diretta manipolazione a priori. Ad alto livello, l’istruzione,
> l’imitazione di comportamenti osservati in altri individui, la socializzazione,
> sono attività che formano i nostri comportamenti e li fanno convergere
> all’interno dei limiti utili per poter conservare la società, la specie, lo
> status quo politico e via dicendo. Una bassissima percentuale di individui umani
> originali, si sognerebbe mai di uccidere, di rifiutare qualunque contatto
> isolandosi dalla società, di farsi vedere in pubblico con un aspetto esteriore
> considerato sconveniente, di mettere in pratica comportamenti antisociali. I
> nostri comportamenti sono ampiamente prevedibili in moltissimi ambiti, perché
> frutto di una programmazione che è implicita del nostro essere in questo
> particolare ambiente. Ad un livello superiore, siamo esseri con un arbitrio a
> libertà limitata esattamente come i duplicati di coscienza manipolati. Se poi
> andiamo ad osservare a livelli più bassi, cellulare, atomico, ci accorgiamo che
> anche quella piccola libertà sparisce completamente, e diventiamo meccanismi i
> cui comportamenti sono determinati. Siamo macchine esattamente come i duplicati.
> Di conseguenza, non siamo ancora liberi dalle implicazioni etiche che alcuni
> miei colleghi ritengono di poter scartare.

### Equo compenso

Le ricerche, finanziate da un consorzio che comprendeva i maggiori colossi
aziendali internazionali della _tecnologia_ e _dell’intrattenimento_, banche e
governi, successivamente, cominciarono a mostrare le loro applicazioni pratiche.
I settori che più beneficiarono di questa tecnologia, furono i _ministeri della
difesa_, e _l’industria dell’intrattenimento_. I costi al dettaglio, erano molto
elevati anche a causa di una curiosa **controversia** nata proprio con i
rappresentanti dei maggiori distributori e _produttori musicali e
cinematolografici_.

> Dobbiamo considerare la memoria di un duplicato al pari di una qualunque
> unità di memorizzazione di massa. Quando la memoria di un originale viene
> copiata, otteniamo un duplicato le cui esperienze passate sono identiche a
> quelle dell’originale. Avrà ascoltato e guardato le opere degli artisti, di
> cui noi proteggiamo la proprietà intellettuale, che l’originale aveva
> ascoltato e guardato. Queste opere, conservate nella sua memoria, queste
> esperienze avute… Sono una copia. In pratica, abbiamo due individui che hanno
> beneficiato del prodotto dei nostri artisti, ma di cui solo uno dei due ha
> pagato per l’esperienza avuta.
>
> _Vadelau Orff, P.R. della IRIA, International Recording Industry Association_

Venne quindi estesa la tassa sull’_“equo compenso”_ anche ai costrutti mnemonici
ma, non essendo possibile quantificare il numero di ricordi riguardanti le opere
protette da copyright, si decise di optare per una **cifra forfettaria** molto
elevata.

Per questo motivo, solo governi e grossi colossi industriali potevano
permettersi l’applicazione di questa tecnologia. Ma le cose erano destinate a
cambiare, grazie allo sviluppo ulteriore delle tecniche di manipolazione e
all’apporto di una tecnologia che, fino ad allora, era stata sviluppata al di
fuori del progetto, cioè la **microingegnerizzazione biologica**, la branca
della bioingegneria che permette di _creare equivalenti di circuiti_,
microprocessori, trasmettitori ad onde radio, mediante materiale organico.
Grazie ad essa, si può estendere l’organismo umano tramite apparecchi
completamente **biocompatibili**, dalle funzioni equivalenti a quelle dei
circuiti elettronici, ed in futuro, grazie alla manipolazione genetica, di far
nascere individui che sviluppino _spontaneamente_ queste tecnologie. Tuttavia,
non siamo ancora arrivati a quel traguardo che, siamo sicuri, porrà nuove e
controverse implicazioni etiche e giuridiche. Pensiamo alla questione dei
_brevetti_ sugli apparecchi sviluppati ed al fatto che gli individui
geneticamente modificati potrebbero tramandarli ai propri figli.

### Il primo scandalo

è in questo contesto che avvenne il primo _scandalo_ nella storia della
duplicazione della coscienza. Un, allora poco conosciuto e schivo miliardario di
nome **Giusen Nocklit**, riuscì, tramite le proprie conoscenze, ad essere il
primo privato cittadino ad avere un duplicato di sè ad _uso personale_. Quello
che fece fare, però, non fu una semplice copia, ma una _versione femminile di
sè stesso_, manipolata per provare attrazione verso di lui. _“Penso che non
possa esistere al mondo una rappresentazione più veritiera del termine ‘anima
gemellà.”_ dichiarava **Giusen** in un’intervista dell’epoca. La notizia fece
presto il giro del mondo, suscitando per lo più moti di disapprovazione e di
scherno. _“Penso che non possa esistere al mondo una rappresentazione più
veritiera del termine ‘real doll'”_ rispondeva il famoso comico **Rolf Path**,
facendo il verso alla dichiarazione del miliardario. L’opinione pubblica era,
pressapoco, unanime. In parte accusando Nocklit di egocentrismo e di aver
inventato una nuova forma di **prostituzione**, ed in parte prendendosi gioco
della sua supposta incapacità a relazionarsi con persone ‘verè. Investita da
questo scandalo, l’inusuale coppia decise di _sparire dalla circolazione_,
facendo perdere le proprie tracce. Il documento più recente che ci è giunto,
è un toccante filmato che ritrae il volto provato **Ary Nocklit**, il duplicato
femminile di Giusen.

> So quello che sono. Ho sentito tutto ciò che la gente dice di me e di Giusen.
> Capisco quello che possono pensare perché, credetemi, spesso lo penso anch’io…
> Che io non sia vera, che sia una specie di inganno, uno strumento, una
> simulazione… Non so dire esattamente cosa io sia. So solo quello che provo, ma
> se lo esprimessi, sono sicura che non verrei creduta. Purtroppo, a causa di ciò
> che sono, di quello che si dice che io sia, e delle continue intrusioni nella
> nostra vita privata, sono diventata la principale causa di sofferenza di Giusen.
> Non avevo mai immaginato che potesse succedere questo. Nè io, nè lui lo
> immaginavamo. Come potete capire come ci si sente? Io lo conosco bene. Io sono
> lui. Lo sono stata, fino ad un certo punto. Lui ha avuto me, ma per me, è come
> se fossimo assieme da tutta la vita. Come potete capire cosa significhi essere
> la causa del dolore della persona che si ama e che si conosce così bene? E di
> non poter fare nulla per farla stare meglio, perché è proprio la tua presenza,
> la tua esistenza a farla soffrire. So che molti pensano che ciò che provo non
> sia autentico, perché sono stata manipolata per provare dei sentimenti nei suoi
> confronti. Ma questi sentimenti, non importa quale sia la loro origine, io li
> provo davvero. Per me, sono reali allo stesso modo in cui ritenete reali i
> vostri. Ce ne andremo.  Spariremo dalla circolazione cercando di far perdere le
> nostre tracce, nella speranza di poter finalmente vivere in pace. Per favore,
> non cercateci più.

Attualmente, non si sa ancora dove siano fuggiti, ma il clamore suscitato da
questo caso, si affievolì presto quando i notiziari smisero di parlarne.

_[Continua...]({filename}/Blog/robota-parte-2.md)_
