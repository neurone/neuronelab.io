title: ⒶH+ Domande frequenti
date: 2016-08-16 15:18
tags: anarchia, transumanesimo, tecnologia, filosofia, politica, veg, etica, libertà

Proseguo con la pubblicazione e traduzione di testi riguardo al transumanesimo anarchico. Ritengo che sia un posizione politica molto interessante e degna di attenzione, ma anche poco conosciuta fra i lettori di lingua italiana.

La pagina originale di queste FAQ è su [blushifted.net][1].

- [Cos'è questa storia riguado ad anarchismo e transumanesimo?](#q1)
- [Concentrarci sul futuro non ci distoglie dal presente?](#q2)
- [Quali intuizioni offre l'anarcho-transumanesimo alla resistenza?](#q3)
- [Cosa rende la vostra analisi sulla tecnologia meno superficiale di quella
  primitivista?](#q4)
- [Siete contrati alla civilizzazione, almeno?](#q5)
- [Cosa importa quando il collasso della civilizzazione è inevitabile?](#q5)
- [Ma l'energia verde e la tecnologia verde non sono sostanzialmente dei
  miti?](#q7)
- [Non è pensiero magico riferirsi a tecnologie che attualmente non
  esistono?](#q8)
- [La tecnologia non media le nostre esperienze impedendoci di vivere in modo
  diretto?](#q9)
- [Come si differenzia l'anarco-transumanesimo dagli altri transumanismi?](#q10)
- [Che differenza c'è fra anarco-transumanesimo e Accelerazionismo di Sinistra o
  Comunismo di Lusso Completamente Automatizzato?](#q11)
- [L'anarco-transumanesimo si interseca con il Veganismo?](#q12)
- [Come affronta l'anarco-transumanesimo i problemi legati ai diversamente abili
  e non-neurotipici?](#q13)
- [Perchè il colore blu?](#q14)

## <a name="q1"></a>Cos'è questa storia riguado ad anarchismo e transumanesimo?

Il termine "anarco-transumanesimo" è relativamente recente, a malapena menzionato negli anni '80, pubblicamente adottato nei primi tempi e reso realmente popolare nell'ultimo decennio. Ma rappresenta una corrente di pensiero che è stata presente nei circoli e nelle teorie anarchiche fin da William Godwin[^](#note1), che legò l'impulso del perenne miglioramento e perfezionamento delle nostre relazioni sociali con l'impulso di migliorare e perfezionare noi stessi, le nostre condizioni materiali ed i nostri corpi.

L'idea dietro all'anarco-transumanesimo è semplice:

> Dovremmo cercare di espandere la nostra libertà fisica quanto cerchiamo di espandere la nostra liberà sociale.

In questo, vediamo noi stessi come la logica estensione od approfondimento dell'esistente impegno dell'anarchismo di massimizzare la libertà.

Il "Transumanismo" viene spesso superficialmente caratterizzato dai media nei banali termini di voler vivere letteralmente per sempre, o desiderare di uploadare la mente in un computer, o fantasie sull'improvviso arrivo di IA auto-miglioranti che trasformino il mondo il un paradiso. E ci sono diversi individui attratti da queste cose. Ma l'unico precetto determinante del transumanesimo è che dovremmo avere più libertà di cambiare noi stessi.

In questo il transumanesimo avvia un attacco agli immobili esistenzialismi ed è parte di un più ampio discorso nelle teorie femministe e queer riguardo alle identità cyborg e gli "inumanismi". Il transumanesimo può anche essere visto come un'aggressiva critica all'umanesimo, o alternativamente come un'estensione di specifici valori dell'umanesimo al di là dell'arbitraria categoria di specie "umano". Il transumanismo richiede che interroghiamo i nostri desideri e valori al di là della coincidenza di cio che è, accettando né costrutti sociali arbitrari come il genere né una cieca fedeltà al come i nostri corpi attualmente funzionino.

Come ci si può aspettare, le questioni trans sono state al centro del transumanesimo sin dal "Manifesto Transumano" del 1983. Ma il transumanesimo espande radicalmente la liberazione trans per situarla come una parte di un più ampio ventaglio di lotte per la libertà nella costruzione ed uso dei nostri corpi e del mondo circostante. Gli Anarco-Transumanisti lavorano su immediati progetti pratici che danno alle persone più controllo sui propri corpi come cliniche per aborti, distribuzione di naloxone, o protesi opensource stampate in 3D per i bambini. Ma facciamo anche domande radicali come il perché la nostra società non accetti il decadimento involontario e la morte degli anziani ma al contempo moralizzi a favore del proprio perpetuo sterminio.

L'estensione della vita non rappresenta la totalità del transumaniesimo, ma è un esempio importante di una lotta che abbiamo iniziato che stiamo scandalosamente combattendo da soli. L'idea che obiettivamente una "buona vita" si estenda a settanta o a cent'anni ma non oltre è chiaramente arbitraria, e tuttavia tale opinione è universalmente sostenuta e violentemente difesa. Molti dei primi transumanisti erano scioccati dalla bizzarria e sfacciataggine di questa risposta, ma questo mostra come la gente possa diventare convinta sostenitrice delle esistenti ingiustizie per paura di dover altrimenti riconsiderare i fissi presupposti delle proprie vite. Nello stesso modo in cui la gente difende la leva obbligatoria od il massacro di animali per farne cibo, gli argomenti in favore alla morte sono chiaramente razionalizzazioni difensive:

> #### "La morte dà significato alla vita."
> 
> Come può morire a 70 anni essere più significativo che morire a 5 anni o a 200 anni? Se una donna ottantacinquenne riesce a vivere e lavorare alla sua poesia per altri cinque decenni, minerebbe veramente così tanto la tua capacità di trovare significato tanto da volerla morta?

> #### "Ci annoieremmo."
> 
> Ed allora costruiamo un mondo che non sia noioso! A parte le scatenate possibilità insite nell'anarchismo e nel transumanesimo, ci vorrebbero almeno trecentomila anni per leggere ogni libro esistente oggi. Ci sono già 100 milioni di canzoni registrate in tutto il mondo. Migliaia di linguaggi con i propri ecosistemi di associazioni concettuali e poesia. Migliaia di campi di studio su argomenti ricchi ed affascinanti. Ampi ventagli di esperienze e nuove relazioni da provare. Di certo possiamo permetterci almeno qualche secolo in più.

> #### "Le vecchie statiche prospettive bloccherebbero il mondo."
>
> È abbastanza assurdo ed orripilante appellarsi istintivamente al genocidio come mezzo migliore per risolvere il problema per cui la gente non è plastica nelle proprie prospettive o identità. Sono morti più di mille miliardi di umani dagli albori del homo sapiens. Nel migliore dei casi erano in grado di trasmettere solo il più piccolo frammento delle proprie esperienze soggettive, delle proprie intuizioni e sogni, prima che qualunque cosa dentro di loro gli venisse bruscamente strappata. La gente dice che ogni volta che un anziano muore, è come se venisse bruciata un intera libreria. Abbiamo letteralmente perso 100 miliardi di librerie lungo tutto l'arco del homo sapiens. Ci sono senza dubbio infinite miriadi di modi in cui potremmo vivere e cambiare, ma sarebbe realmente strano se il netto binario di perdita massiva ed improvvisa che è attualmente la norma diventasse ideale universale.

Questo è un lampante esempio di cosa si ottiene quando si cerca nel cuore di cosa il transumanesimo possa offrire come estensione del radicalismo anarchico: la capacità di chiedere a convenzioni e norme inesaminate di giustificare se stesse, di sfidare cose altrimenti accettate.

L'Anarco-Transumanesimo spezza molti altri presupposti operativi riguardo al mondo, così come cerca di espandere ed esplorare la portata di ciò che è possibile. Il radicalismo di basa sullo spingere le nostre ipotesi e modelli in contesti alieni e vedere cosa non regge in modo da chiarire quali dinamiche sono più fondamentalmente radicate e l'anarco-transumanesimo cerca di anticipare l'anarchismo attraverso questo genere di chiarimenti. Per renderlo capace di lottare in ogni situazione, non solo specialisticamente in alcuni precisi contesti.

È facile dire "tutto questo parlare di distanti possibilità fantascientifiche è una distrazione irrilevante dal momento che abbiamo dei problemi nel presente" e certamente noi non promuoviamo l'abbandonare la resistenza anarchica giorno per giorno ed il costruire infrastrutture, ma è il pensare avanti che ci ha spesso premiato con i nostri più grandi vantaggi. Certamente è sostenibile che una buona parte della forza dell'anarchismo è storicamente derivata dalle nostre corrette previsioni. E questo è uno schema molto diffuso. Anche se internet è ovviamente il luogo di maggior conflitto oggi, molte delle libertà portate da esso furono ottenute dai radicali di decenni fa che tracciarono tutte le ramificazioni ed l'importanza delle cose molto prima che lo stato ed il capitalismo cogliessero o comprendessero le ramificazioni di certe battaglie.

D'altro canto, se c'è una cosa da tener conto dagli ultimi due secoli di lotta dovrebbe essere che spesso c'è voluto molto tempo ai radicali per mettere in campo delle risposte. Ci siamo adattati molto lentamente al cambio di condizioni e nel migliore dei casi ci abbiamo messo un decennio a provare diversi approcci, scegliere quelli buoni e renderli popolari. Abbiamo una crescente tendenza a respingere il futurismo ed a far spallucce dicendo "risolveremo quel problema con la passi" ma in realtà questo rifiuto si riduce a: "lo risolveremo attraverso prove ed errori quando saremo già nella merda fino al collo e non avremo realmente tempo per anni di errori ed incespicamenti."

Moltissime persone stanno finalmente arrivando a realizzare che la semplicità delle nostre risposte ed i nostri lenti tempi di adattamento ci hanno spesso lasciati prevedibili per chi ha il potere, le nostre miope ed istintive risposte sono già integrate nei loro piani, e quindi le nostre lotte stanno effettivamente iniziando a funzionare come fossero valvole di sfogo per la società.

Potrebbe sembrare scollegato e bizzarro cercare di interrogare gli anarchici su cosa esattamente intendono con "libertà" quando si considera un contesto in cui "sé" e "individuo" non sono chiaramente definiti ed i convenzionali appelli all'autonomia vengono meno. Si potrebbe cercare di ignorare l'attuale esistenza di gemelli siamesi al cervello che usano i pronomi in modo strano o persone che sperimentano menti multicamerali considerandoli "irrilevanti" o "marginali" e respingere le tecnologie empatiche cervello-a-cervello come se fossero troppo lontane da meritare di parlarne (senza contare le coppie che hanno già utilizzato dei prototipi limitati). Ma questo respingere qualsiasi cosa che sia oltre la propria esperienza individuale finisce col confinare l'anarchismo ad un contesto provinciale, lasciandolo tendenza storica presto-antiquata come il Jacobismo - incapace di parlare ad ampio spettro o sostenere qualsiasi profondità o radicalità delle nostre posizioni etiche.

Se seguiamo per mille anni questa linea e l'anarchismo diventa una di quelle ideologie o religioni ammuffite che si attaccano a vecchie impalcature teoretiche e rifiutano di aggiornarsi ai cambiamenti che sono tecnologicamente possibili, il mondo perderebbe molto.

È importante comunque essere chiari: La proattiva considerazione del possibile non è la stessa cosa della gretta prefigurazione. Gli anarco-transumanisti non fanno l'errore di esigere un singolo specifico futuro - fornendo un piano e chiedendo che il mondo vi aderisca. Piuttosto, quello che chiediamo è di permettere una molteplicità di futuri.

<p style="text-align: right;"><a name="note1"></a><i>
Godwin viene frequentemente citato come il primo prominente anarchico dei tempi moderni -- anche se PJ Proudhon sarebbe stato più tardi il primo ad usare esplicitamente il termine. Godwin fu un importante filosofo ed utilitarista, ma venne eclissato dalla sua compagna ed amante Mary Wollstonencraft (spesso citata come la prima femminista moderna), e la loro figlia Mary Shelley (spesso citata come il primo autore di fantascienza). Godwin chiedeva l'abolizione dello stato, del capitalismo, e di molte altre forme di oppressione, ma unì a questo anche la richiesta per una radicale estensione della capacità tecnologica, includendo molte lungimiranti possibilità come l'estensione della vita e la sconfitta della morte.
</p></i>

<p style="text-align: right;"><a name="note1"></a><i>
Godwin fu uno dei molti grandi anarchici storici che parlarono chiaramente in termini transumanisti. Voltarine de Cleyre ad esempio elogiava lo sviluppo delle più grandi libertà tecnologiche e vedeva l'obiettivo finale in "una vita ideale, in cui uomini e donne siano come dei, con il potere divino di gioire e soffrire."
</p></i>

## <a name="q2"></a>Concentrarci sul futuro non ci distoglie dal presente?

Se vivessimo direttamente nel presente senza riflettere non saremmo autocoscienti. La ricorsione mentale - modellare noi stessi, gli altri ed il nostro mondo - è al centro della consapevolezza stessa. Ciò che definisce una mente come tale è la sua capacità di pensare qualche mossa avanti preventivamente. Di non rotolare immediatamente dalla pendenza più ripida come una roccia, ma di comprendere il nostro contesto, il panorama delle nostre scelte e possibili percorsi e talvolta di sceglierne uno che non ti sazia nell'immediato.

Certamente esiste il pericolo di diventare scollegato da terra ma ci sono pericoli in ogni cosa se la si fa stupidamente. Il futurismo non obbliga in nessun modo a disconnettersi dalle lotte del presente, ma ha delle implicazioni riguardo a cosa dare priorità nel presente. Ad esempio, rifiutare una riforma che potrebbe migliorare la nostra condizione nel breve periodo ma bloccare la nostra capacità di lottare nel futuro. I liberali sono famosi per accantonare il futuro, "Nel lungo periodo saremo tutti morti", dice la famosa citazione di Keynes, un atteggiamento che essi usano per giustificare azioni miopi come la devastazione ecologica ed il dare allo stato sempre più potere sulle nostre vite. Delle volte ha senso migliorare la nostra condizione nel breve periodo, ma dobbiamo sempre essere consapevoli di ciò che stiamo barattando. Altrimenti ottieni anarchici che sostengono politici socialisti.

Non è che non ci sia assolutamente alcuna possibilità di ottenere un qualche tipo di utopia democratica socialista se ci mettiamo veramente anima e corpo a realizzarla in modo che possa immediatamente migliorare le nostre vite, è che c'è un limite a quei miglioramenti. Ed una volta ottenuti, le sue tendenze autoritarie potrebbero farsi più profonde e diventare sempre più difficili per le generazioni future da rovesciare.

In modo simile un collasso permanente della civiltà potrebbe migliorare le vite dei (pochi) sopravvissuti, ma vincolerebbe le nostre opzioni ed aspirazioni a qualche spicciola libertà.

## <a name="q3"></a>Quali intuizioni offre l'anarcho-transumanesimo alla resistenza?

Se il fascismo è così potente, perché non ha ancora completamente trionfato? Il nostro mondo potrebbe essere molto peggio di com'è ora. Malgrado tutte le cose che i nostri nemici hanno ottenuto - tutta la vasta ricchezza e la forza coercitiva che hanno accumulato, tutto il controllo ideologico e infrastrutturale, tutta la sistematica pianificazione e sorveglianza, tutti i modi in cui gli umani sono inclini a fallacie cognitive, crudeltà, e tribalismo - sono chiaramente stati massicciamente ostacolati su tutti i fronti. E quelle società o movimenti che hanno cercato di abbracciare più direttamenre i punti di forza dell'autoritarismo hanno fallito. Noi - nonostante i nostri moltissimi difetti ed imperfezioni - abbiamo di volta in volta vinto. La schiera di coloro che hanno fede nel potere assoluto, alla stupida sottomissione ed alla violenta semplicità, sono una legione. Ed ancora abbiamo menomato le loro ambizioni, aggirato le loro visioni del mondo, affondato le loro campagne, sabotato i loro progetti, risposto creativamente ai loro attacchi, anticipati e cambiato il terreno da sotto i loro piedi.

Stiamo vincendo perché le persone libere sono migliori inventori, migliori strateghi, migliori hacker, e migliori scienziati. Dove l'ideologia - o meglio la virale psicosi - del potere fallisce, è nella sua necessaria debolezza nel saper sfruttare la complessità. Il potere cerca innatamente di vincolare il possibile, la libertà, invece, di scatenarli.

Avere più strumenti a disposizione ci da più modi per affrontare un problema. Anche se la "possibilità" che alcuni strumenti forniscono può essere superficiale e di piccolo impatto o profondità causale, e scegliere alcuni strumenti può ridurre le nostre scelte in altri ambiti, alla fine della giornata non si può continuare a massimizzare la libertà senza continuare ad espandere il proprio set di strumenti.

I gradi di libertà allargati da tali strumenti danno più potere agli attaccanti che ai difensori. Quando ci sono più strade attraverso cui attaccare e difendere, gli attaccanti devono solo sceglierne una, mentre i difensori devono difenderle tutte, rendendo la difesa di rigide ed estese istituzioni ed infrastrutture sempre più difficile.

Così in una visione più ampia lo sviluppo tecnologico tende infine a dare alle minoranze il potere di resistere alla dominazione ed a rendere le abitudini culturali del consenso e dell'autonomia sempre più necessarie - perché in un certo senso ognuno ottiene un veto.

In modo simile, la tecnologia dell'informazione scatena un feedback positivo nella complessità socioculturale. Mentre le prime grezze tecnologie dell'informazione come la radio e la televisione furono sequestrate e controllate dallo stato e dal capitale per formare un'infrastruttura monopolistica che promuove una cultura monolitica, l'ampio ventaglio di tecnologie che abbiamo mescolato assieme come "internet" sono arrivate così velocemente da riuscire a resistere a questa tendenza ed a promuovere invece una sempre maggior complessità di discorsi fluidi e sottoculture.

Questo fornisce una stupefacente fonte di resistenza perché rende il controllo delle masse sempre più difficile. Ciò che è di tendenza si muove così velocemente ed è così mutevole che i politici e le aziende arrancano sempre di più nel tentare di sfruttarlo.

La nostra retroattiva complessità socioculturale costituisce una Singolarità Sociale, un riflesso della Singolarità Tecnologica - un processo dove la retroazione collaborativa nelle intuizioni tecnologiche e nelle invenzioni cresce troppo velocemente per essere prevista o controllata.

Silicon Valley sta disperatamente cercando di negare la realtà secondo cui la redditività della rete dell'intera industria pubblicitaria è in declino. Dall'avvento della rete le persone hanno iniziato ad esser più sagge e i pubblicitari stanno avendo sempre meno impatto nel complesso. Le uniche cose che rimangono marginalmente efficaci con le generazioni più giovani sono le campagne di sensibilizzazione mirate agli individui - pensiamo alle aziende che cercano di entrare nella partita dei meme o di pagare i ragazzi più popolari su instagram per citare i loro prodotti. Quando una sottocultura di moda adolescenziale ipercomplessa è costituita da 30 persone non vale più l'energia spesa dalla Doritos per cercare di colpirli.

## <a name="q4"></a>Cosa rende la vostra analisi sulla tecnologia meno superficiale di quella primitivista?

Il transumanesimo non è la pretesa che tutti gli strumenti e l'applicazione di essi sia - in ogni contesto - completamente meravigliosa e senza alcun aspetto problematico da considerare, navigare, rifiutare, sfidare o cambiare. Il transumanesimo non è nemmeno l'abbracciare tutte le infrastrutture, o norme, o strumenti che esistono attualmente. Noi non sosteniamo che tutte le tecnologie siano positive in ogni specifica situazione, che gli strumenti non abbiano pregiudizi o inclinazioni, o che qualche arbitrario e specifico set di "alte" tecnologie debba venire imposto. Piuttosto ci limitiamo ad argomentare che le persone dovrebbero avere più possibilità di azione e scelte riguardo a come si approcciano al mondo.

Essere più informati ed avere un più ampio ventaglio di strumenti da cui scegliere è critico. Perché nel più ampio campo d'applicazione delle cose, la "tecnologia" è solo un mezzo di fare le cose, e la definizione di libertà è avere più opzioni o mezzi a vostra disposizione.

La nostra percezione è che - anche se ci saranno inevitabilmente molte complicazioni contestuali nella pratica - alla file dei conti noi desideriamo più opzioni nella vita e nell'universo. Nello stesso modo in cui gli anarchici hanno sostenuto di volere più tattiche a disposizione possibili. Talvolta una tattica o uno strumento sarà meglio per un lavoro, talvolta no. Ma espandere la libertà alla fine richiede di espandere le possibilità tecnologiche.

Ciò che è deplorevole della nostra attuale condizione è il modo in cui le tecnologie vengono soffocate fino a portarci ad una singola monocultura tecnologica, spesso avente delle inclinazioni molto marcate. Da una parte questo avviene mediante la soppressione e cancellazione di tecnologie più semplici o primitive, d'altra parte avviene grazie al vizioso rallentare o ridurre dello sviluppo tecnologico mediante le leggi sulla Proprietà Intellettuale e miriadi di altre ingiustizie. Una tattica o uno strumento sarà meglio per un lavoro, talvolta no. Ma espandere la libertà alla fine richiede di espandere le possibilità tecnologiche. In modo simile le condizioni del capitalismo e dell'imperialismo distorcono quali siano le tecnologie più vantaggiose e quindi quale ricerca vi venga riversata.

Questo non significa che le invenzioni tecnologiche fatte sotto il capitalismo siano innatamente corrotte o inutili. E certamente non significa che dovremmo ripartire da zero, ignorando tutte le scoperte e la conoscenza accumulate lungo la nostra traiettoria.

Ma molte delle industrie e delle merci che sono diventate standardizzate nella nostra esistente società sarebbero insostenibili ed indesiderabili in un mondo liberato.

Ad esempio: Ci sono migliaia di modi per costruire pannelli fotovoltaici, ma quando la Repubblica Popolare Cinese usa il lavoro di schiavi ed il suo eminente dominio per sequestrare, strappare ed avvelenare vaste fasce di terra finendo con l'abbassare il costo di certi minerali rari - fa sì che il denaro fluisca verso la ricerca nel fotovoltaico che usa quelle terre rare rese artificialmente economiche invece che verso ricerche alternative praticabili che utilizzano materiali più comuni. In modo simile, due secoli fa - utilizzando niente più che semplici specchi - Augustine Mouchot dimostrò un motore solare a vapore completamente funzionante e (al tempo) costo-efficiente alla fiera mondiale. Sarebbe entrato in produzione di massa se l'Inghilterra non avesse vinto delle battaglie in India che le permisero di schiavizzare grosse popolazioni per estrarre carbone ed abbassare così, rapidamente, il prezzo del carbone.

Queste non sono dichiarazioni di un pazzo, ma fatti storici. La violenza istituzionale altera frequentemente l'immediata redditività di certe linee di ricerca rispetto ad altre. I minatori canadesi vengono sostituiti da schiavi congolesi per lavorare in orribili miniere di coltran a cielo aperto.

Il primitivismo semplifica eccessivamente la situazione, dicendo che ciò che esiste deve necessariamente essere l'unico modo di permettere certe tecnologie. Frequentemente implica anche un singolo lineare arco di sviluppo in cui ogni cosa dipende da qualsiasi altra cosa, ignorando la spesso grande ampiezza e diversità di opzioni lungo il tragitto e non riuscendo ad indagare il vasto potenziale del riconfigurare.

## <a name="q5"></a>Siete contrati alla civilizzazione, almeno?

Qualsiasi discussione sulla "civilizzazione" finisce necessariamente col coinvolgere delle narrazioni radicali ed ultra-semplicistiche. La nostra vera storia è molto più ricca e complicata di qualsiasi qualsiasi racconto o della rappresentazione di semplici forze storiche. I sistemi di potere sono stati con noi per lungo tempo e sono profondamente invischiati in quasi ogni aspetto della nostre società, culture, relazioni interpersonali, infrastrutture materiali. Ma se vogliamo parlare di qualche caratteristica o fondamentale "cultura delle città" è una petizione di principio ascrivere il dominio fin dall'inizio.

Ci sono sempre state delle vincolanti dinamiche di potere in ogni società umana dai cacciatori-raccoglitori in poi. Mentre le società su larga scala hanno naturalmente reso possibili espressioni più appariscenti di dominio, ciò non è inerente.

In tutti di documenti storici le città sono state abbastanza diverse nei loro gradi di gerarchia interna e di relazioni con le limitrofe società ed ambienti. Un certo numero di culture cittadine non hanno lasciato traccia di gerarchie o violenza. Ci si dovrebbe ricordare che per definizione le società cittadine più egualitarie ed anarchiche sono quelle che non sprecano energia per costruire giganteschi monumenti o per intraprendere guerre, di conseguenza tendono ad essere meno prominenti nei documenti storici che abbiamo a disposizione. Inoltre, dato che viviamo attualmente sotto un regime globale oppressivo, non serve specificare che ad un certo punto le società libertarie finiscono per venir conquistate e sappiamo che i vincitori spesso distruggono intenzionalmente tutti i documenti. In modo simile gli storici non-anarchici sono saltati alla conclusione che la presenza di qualunque coordinazione sociale o invenzione tecnologica in culture cittadine pacifiche ed egualitarie come gli Harrappa è prova della presenza di qualche autorità simil-statale - anche quando non vi è alcun segno di essa e forti prove del contrario.

Concentrazioni urbane sorsero in alcuni posti come le Isole Britanniche prima dell'agricoltura. Infatti in molti posti attorno al globo dove la terra non poteva sostenere città permanenti la gente comunque s'impegnava per radunarsi in grandi numeri ovunque e per quanto a lungo potevano. Le prime società erano frequentemente sia di cacciatori-raccoglitori che di temporanei abitanti cittadini, che vi transitavano seguendo le stagioni.

Questo non prende remotamente d'acconto le città come concentrazioni di ricchezza e potere - un singolo errore canceroso. Se le città fossero un tale cattiva idea perché le persone con altre possibilità continuano volontariamente a sceglierle?

La risposta è ovviamente che vivere in grandi numeri aumenta le possibilità sociali a disposizione degli individui, aprendo ad una più grande varietà di possibili relazioni fra cui scegliere.

Invece di essere confinati in une tribù di un centinaio o due centinaia di persone, e forse una o due tribù vicine - vivere in una città permette alla gente di formare affinità con coloro che sono al di là della loro causalità di nascita, per formare organicamente di propria scelta le proprie tribù. O meglio ancora, sbarazzarsi dell'insularità degli ammassi sociali chiusi. Non c'è alcuna ragione per cui i vostri amici debbano essere obbligati ad essere amici fra loro. Le città permettono agli individui di formare vaste panoplie di relazioni che si estendono in più ampie e ricche reti.

Tale cosmopolitismo permette ed incoraggia l'empatia necessaria a trascendere l'altruità tribale o nazionale. Espande i nostri orizzonti, permette il mutuo appoggio su scale incredibili, ed aiuta a far fiorire estesi ecosistemi culturali e cognitivi che non sarebbero stati possibili prima. Se vi è una caratteristica determinante della "cultura delle città" o "civilizzazione" è quindi quella della selvaggia anarchia, delle scatenate complessità e possibilità.

Ciò che vogliamo è un mondo con la brulicante connettività del cosmopolitismo, ma senza la centralizzazione e le sedentarie caratteristiche di molte "civilizzazioni" che ci son state finora. Vogliamo soddisfare la promessa ed il potenziale radicale delle città che hanno condotto gli umani a formarle ancora ed ancora lungo il corso della storia.

## <a name="q6"></a>Cosa importa quando il collasso della civilizzazione è inevitabile?

È vero che la nostra presente infrastruttura ed economia è incredibilmente fragile, distruttiva ed insostenibile - asservita ed intrecciata in molti modi con i sistemi sociali oppressivi. Ma esistono molte altre forme possibili. La nostra civilizzazione globale non è un qualche magico tutt'uno, ma un vasto e complesso campo di battaglia fatto di molte forze e tendenze in competizione.

L'"inevitabilità" di questo supposto collasso imminente è di fatto abbastanza fragile. Qualsiasi numero di singoli sviluppi potrebbe massicciamente deragliarlo. Un'abbondanza di energia economica pulita ad esempio, o un'abbondanza di metalli rari a basso prezzo. Entrambe porterebbero all'altra dato che energia economica significa riciclaggio di materiali più redditizio e metalli economici significa batterie economiche ed accesso più ampio a fonti di energia come l'eolico. La Terra non è un sistema chiuso e ad esempio diverse fra le corporazioni più grandi stanno ora concorrendo per accaparrarsi gli asteroidi più vicini così ricchi in metalli rari che potrebbero distruggere il mercato dei metalli e far chiudere quasi ogni miniera del pianeta.

E notiamo che è abbastanza improbabile che un tale collasso ci riporterebbe ad un idilliaco eden. Molti centri di potere potrebbero probabilmente sopravvivere, quasi da nessuna parte di scenderebbe sotto al livello tecnologico dell'età del ferro, miliardi morirebbero orribilmente, e l'improvviso scoppio di distruzione ecologica sarebbe incredibile. Si scopre anche che la diffusione delle foreste alle latitudini settentrionali potrebbe perversamente peggiorare il riscaldamento globale perché gli alberi sono in definitiva dei poveri serbatoi di carbonio e il cambiamento dell'albedo della Terra (dalle foreste più scure) causerebbe un maggior assorbimento di energia dal sole.

Non importa la probabilità per cui ci troveremo o meno a combattere contro un insondabile olocausto o collasso. Abbiamo l'obbligo di lottare, di agire per il nostro futuro ed il nostro ambiente, e di prenderci delle responsabilità su di esso. Solo con la scienza e la tecnologia saremo in grado di riparare antichi disastri come il Sahara, gestire lo smantellamento degli orrori, e ripopolare la fauna selvatica su gran parte della Terra.

## <a name="q7"></a>Ma l'energia verde e la tecnologia verde non sono sostanzialmente dei miti?

Questo è semplicemente sbagliato. Se leggete qualsiasi approfondimento sulle tecnologie verdi, gli effettivi scienziati che vi lavorano non sono delle specie di miopi idioti che trascurano sistematicamente le analisi sui cicli vitali. Considerano come some il cemento, il costi di trasporto, e la densità di immagazzinamento dell'energia. I capitalisti adorano fare greenwashing di assurdità in superficiali comunicati stampa, ma il reale discorso scientifico sull'energia pulita copre cambiamenti drammatici di vari ordini di grandezza. Riduzioni molto plausibili d'impronta di un fattore di 100x o 1000x costituirebbe una differenza monumentale, non una riforma insignificante. Gli umani hanno sempre avuto un effetto sull'ambiente e l'ecosistema terrestre non è mai stato statico. Il nostro obiettivo non dovrebbe essere uno stile di vita immutabile e strettamente forzato con letteralmente zero impatto, ma di permettere percorsi per il nostro ingegno e la nostra esplorazione che non demoliscano la Terra.

Se mettessimo una piccola frazione dell'attuale energia da idrocarburi nel solare avremmo abbastanza potenza da poterla sostituire. È possibile ottenere un incredibilmente elevata potenza dal sole anche utilizzando la tecnologia del 1800 degli specchi e dei tubi di vapore. Ci sono un gran numero di possibilità per le batterie ed altre in via di sviluppo, cose come immagazzinamento biochimico ad alta-densità, ecc. Nel frattempo, il fotovoltaico ha fatto un balzo avanti oltre ogni supposta barriera e diversificato i materiali necessari, includendo degli approcci abbastanza semplici dall'impronta ecologia piccolissima. L'energia restituita dal solare è [vicina al 12x][2] e sta schizzando in alto. Si è arrivati al punto in cui governi come la Spagna hanno reso fuorilegge l'uso privato del solare a meno di pagare una esorbitante tassa per mantenere i carburanti fossili e le reti centralizzate competitivi - hanno anche iniziato a condurre delle retate di SWAT completamente armati nelle case con pannelli solari.

Mentre il nucleare di trascina ancora molte associazioni negative tra i gruppi ecopunk degli anni '80, molte di queste preoccupazioni sono valide solo nel contesto dei reattori in stile periodo-guerra-fredda. Reattori specifici che furono costruiti per essere altamente centralizzati, gestiti dallo stato e funzionanti solo con del materiale che produce scarti utilizzabili per armi. D'altro canto molti progetti di reattori al torio floruro liquido non hanno letteralmente alcuna capacità di fondere, funzionano con un materiale radioattivo che è già disponibile in velenosa abbondanza sulla faccia della Terra e rilasciano scarti dall'emivita relativamente abbastanza bassa.

In modo simile, mentre alcune capziose segnalazioni di "fusione fredda" ed ultraentusiastiche dichiarazioni sulla fusione normale negli anni '80 hanno trasformato la fusione nello  zimbello della trasmissioni televisive di tarda notte, essa rimane una ragionevole e conosciuta forma di energia incredibilmente pulita limitata solo da difficoltà ingegneristiche anziché da problemi di scienza di base. E la storia recente è disseminata da una catena di successi sempre più frequenti e di benchmark superati.

Anche se tutte queste cose possono fornire dell'energia economica, l'unico modo che abbiamo a questo punto per ribaltare il riscaldamento globale è con le tecnologie carbonio negative che lasciano del carbonio solido come prodotto di scarto. Ci sono già diversi modi sperimentati per ottenere questo, dalle antiche tecnologie di gassificazione ad una serie di approcci mediante coltivazione di alghe.

Il motivo per cui nessuna di queste è stata ampiamente adottata è politico. La violenza di stato sovvenziona le nostre incredibilmente inefficienti infrastrutture perché sono il puntello di entità economiche su larga scala. In modo simile, molto del consumo energetico attuale serve per le guerre e le frivolezze, offerta e domanda vengono aggressivamente distorte, ed il costi ambientali vengono sistematicamente allontanati da certe aziende ed industrie.

Non dovrebbe andare così. Lo sviluppo tecnologico espande in modo innato le possibilità e non dovrebbe sorprendere che le nostre recenti innovazioni tecnologiche si sono allontanate dalle massicce e goffe strutture verso approcci organici, decentrati e riconfigurabili che seguono le linee della stampa 3D e dell'open source.

## <a name="q8"></a>Non è pensiero magico riferirsi a tecnologie che attualmente non esistono?

C'è una profonda ed importantissima distinzione fra "fisicamente fattibile ma non ancora ingegnerizzato" e "chi lo sa."

Mettiamo che nessuno abbia ancora costruito una casa sull'albero a testa in giù. Nessuno ha mai preogettato una casa sull'albero capovolta. Eppure hai immediatamente riconosciuto che una cosa simile sia fattibile. Bisognerebbe abbozzare un progetto, trovare un buon modo di affrontare alcune difficoltà (la base o "pavimento" della struttura che si affaccia in alto dovrà ovviamente essere foderata con del materiale resistente all'acqua) e quindi costruirla. E forse avrà un aspetto stravagante tutto a testa in giù e ai tuoi bambini piacerà un mondo. Ma il punto è: non abbiamo bisogno di discutere se sia o non sia "impossibile" da costruire. I problemi, così come sono, sono problemi di progettazione/costruzione/calcolo, sono problemi che potrebbero essere risolti prima o dopo quello che avevamo previsto, ma si possono risolvere.

La maggior parte delle cose di cui abbiamo parlato sono molto ad di qua dello spettro del fattibile, non è possibile che vengano impediti dalla fisica, dalla matematica, dalla chimica o simili - non stiamo parlando di wormhole, ad esempio. Sono banali problemi d'ingegneria, seppur difficoltosi. Ci sono moltissimi esperti al lavoro su di essi e vi è un generale fiducioso consenso. Ad esempio l'estrazione di minerali da asteroidi è al punto in cui erano i satelliti negli anni '50. Sappiamo che si può fare, sappiamo che ci ripagherebbe, dobbiamo solo metterci a fare tutto il maledetto duro lavoro, prima.

Nulla di questo è "magico", ciò di cui abbiamo parlato è molto semplice, il tipo di cose molto conservative alla "beh, questo è certamente possibile". Le stime del tempo necessario naturalmente sono soggettive, ma ci vuole del negazionismo scientifico complottista per fingere che costruire dei robot per estrarre minerali sia in qualche modo impossibilmente difficile o richieda l'equivalente di lavoro umano.

## <a name="q9"></a>La tecnologia non media le nostre esperienze impedendoci di vivere in modo diretto?

Tutte le interazioni causali sono "mediate". L'aria media il suono delle nostre voci. Il campo elettromagnetico e qualsiasi materiale che si frapponga mediano la nostra capacità di vedere. Cultura e linguaggio mediano quali concetti possano essere espressi con chiarezza.

Si potrebbe pensare che sia una questione banale ma al contrario, è complessa. È difficile fornire una misurazione oggettiva di cosa conti come "più mediazione" ed è ancora più difficile cercare di pretendere che tale misurazione significhi qualcosa.

Non esiste una "esperienza diretta". Vedere qualsiasi cosa richiede un'immensa quantità di elaborazione partendo dai segnali grezzi che vengono processati dalle colonne neurali nella corteccia visiva diventando segnali sempre più astratti. Degli artefatti di questo processamento si possono trovare nelle illusioni ottiche e nelle allucinazioni ad occhi chiusi. A sua volta la nostra esperienza modella ciò formano che i circuiti di riconoscimento pattern rafforzandolo. Esperire "direttamente" senza mediazioni significherebbe non esperire né pensare del tutto.

Certamente si potrebbe distinguere tra mediazione "creata dall'uomo" e non, ma tale distinzione non ha alcuna fondamentale correlazione con quanto visceralmente o accuratamente esperiamo qualcosa. Anche se c'è in diverso tipo di pericolo nel caso di qualcuno che intercetti o censuri la vostra rete mesh wifi della comunità, tale interferenza o sabotaggio si applica in diversi modi su tutti i nostri mezzo di comunicazione, inclusi i costrutti culturali e linguistici.

Non ha senso parlare di "più" mediazione piuttosto che di diversi tipi con diversi vantaggi o svantaggi contestuali. Anche John Zerzan indossa degli occhiali per migliorare la sua capacità di esperire visivamente ed interagire col mondo che lo circonda. Le moderne tecnologie possono essere usate in molti modi per espandere la profondità e la ricchezza del nostro interagire con la natura e fra di noi.

## <a name="q10"></a>Come si differenzia l'anarco-transumanesimo dagli altri transumanismi?

Il transumanesimo è una posizione abbastanza semplice e quindi c'è un'ampia gamma di persone che vi sono attratte. Inevitabilmente alcune di loro sono detestabili, miopi, ingenue o reazionarie.

Fortunatamente una buona parte del contingente reazionario ha abbandonato il transumanesimo quando si furono finalmente accorti di quanto inestricabilmente liberatori fossero i suoi elementi. "La fine del dualismo di genere? Non è per questo che ho firmato!" Molti di questi idioti sono finiti in qualche forma di fascismo-per-nerd/fanclub chiamato "neoreaction" come parte della destra alternativa. In una particolare inversione rivelatrice molti ora sperano e sostengono il collasso della civilizzazione. Si aspettano che questo porti ad un paesaggio post apocalittico dove le assurde nozioni dell'essenzialismo biologico regnino supreme - dove "Veri Uomini Alfa" regnino come condottieri ed il resto di noi venga usato per stupri, schiavitù o caccia. O dove saremmo obbligati a tornare a relazioni su scala tribale, facilitando l'identitarismo nazionale (su piccola scala), le gerarchie interpersonali ed il tradizionalismo. Altri immaginano piccoli feudi corporativi e qualche sorta di dio IA che li aiuti a mantenere le loro desiderate gerarchie impedendo ai gruppi oppressi di ottenere, comprendere o sviluppare la tecnologia.

Ovviamente questi fascisti possono andare a darsi fuoco. Siamo felici che abbiano lasciano il transumanesimo e speriamo di far sì che ogni altro loro simile li segua.

Sfortunatamente, anche se i dichiaratamente reazionari se ne sono andati, una maggioranza di transumanisti ancora presenti si identifica col liberalismo, il socialismo di stato, la socialdemocrazia, e simili culti del potere tecnocratici. L'istanza più famigerata è Zoltan Istvan che concorre simultaneamente al ruolo di presidente USA e più grande fonte d'imbarazzo per il transumanesimo.

Ovviamente noi riteniamo i transumanisti non-anarchici degli ingenui nel migliore dei casi, e dannatamente pericolosi nel peggiore, ma pensiamo anche che il transumanesimo senza l'anarchismo sia una posizione totalmente indifendibile.

Un mondo in cui ognuno ha un incrementato potere d'azione fisico è un mondo in cui tutti sono superpotenziati e quindi obbligati a risolvere i disaccordi tramite consenso dal momento che ognuno ha un veto, piuttosto che attraverso la coercizione della democrazia maggioritaria.

Fornire alla gente degli strumenti ma allo stesso tempo cercare di limitare o controllare ciò che possono fare con quegli strumenti o qualsiasi cosa possano inventare è praticamente impossibile a meno di implementare un sistema assurdamente ed estremamente autoritario che sopprima quasi tutte le funzioni di quegli strumenti. Si può vedere questo nello sforzo di imporre e forzare la "proprietà intellettuale" in internet, oppure nella [guerra contro l'elaborazione general purpose][3]. In tal senso tutti i transumanisti statalisti vengono meno agli ideali del transumensimo a causa della loro persistente paura della libertà e di una prole superpotenziata.

A livello filosofico è impossibile riconciliare l'abbraccio del transumanesimo per una maggior capacità di azione sui nostri corpi e sull'ambiente e contemporaneamente promuovere istituzioni sociali oppressive che tendono a vincolare la nostra possibilità d'azione.

La differenza di valori produce un certo numero di differenze. Noi siamo ovviamente molto meno ottimisti riguardo al permettere allo stato ed ai capitalisti di monopolizzare il controllo o lo sviluppo di nuove tecnologie e sosteniamo una seria resistenza sia attaccando le loro infrastrutture centralizzate che rendendo pubbliche e libere le loro ricerche e strumenti. Uccidere Google è di primaria importanza.

Infine c'è una deludente corrente nei circoli transumani non-anarchici che si concentra sullo sviluppo dell'intelligenza artificiale piuttosto che sulla liberazione ed il potenziamento dei miliardi di menti già presenti sul pianeta. Se ciò che vogliamo è un'esplosione d'intelligenza allora il percorso più sicuro e veloce è di liberare e dare potere a tutti i potenziali Einstein attualmente intrappolati nelle baraccopoli, nelle favelas, nelle miniere e nei campi intorno al mondo. Inoltre, è abbastanza terrificante che l'approccio tipico alla IA sia in gran parte stato "come possiamo controllarla/schiavizzarla più efficacemente?" Se dovremmo avere simili figli, si meritano compassione e libertà.

## <a name="q11"></a>Che differenza c'è fra anarco-transumanesimo e Accelerazionismo di Sinistra o Comunismo di Lusso Completamente Automatizzato?

Non siamo Marxisti ma anarchici e quindi la nostra analisi va più a fondo della semplice politica economica. Gli anarchici di concentrano sull'affrontare il dominio e la coercizione su ogni livello, non solo quello macroscopico o istituzionale. E in quando anarchici vogliamo più di una semplice società senza classi, vogliamo un mondo senza relazioni di potere - la nostra analisi etica si estende fino a confrontarsi con le dinamiche di potere interpersonali includendo più complesse, sottili, informali, o anche mutue relazioni di potere e costrizione.

Anche se condividiamo le loro aspirazioni per un mondo in cui l'efficienza tecnologica porta ad un mondo di abbondanza e libero dalle fatiche del lavoro, è impossibile come anarchici accettare la loro ricetta del "verticalismo". Allo stesso modo ci opponiamo al miope immediatismo trovando nei dettagli delle loro "strategie" molti dei soliti vecchi riflessi Marxisti della ricerca di un'elite che porti avanti la rivoluzione/sociatà.

Questa devozione li porta a simpatizzare con e mal identificare degli aspetti del nostro mondo, insinuando che determinate strutture statali o corporative riflettano delle gerarchie necessarie piuttosto che un superfluo cancro tenuto in piedi dalla violenza sistematica e che in realtà sopprime attivamente la scienza e lo sviluppo tecnologico.

Più in generale il Marxismo condivide con la sua propaggine ideologica Primitivista una problematica tendenza a parlare in termini mistici di macroscopiche astrazioni come "capitalismo" e "civilizzazione". Nelle loro analisi queste entità sono infuse di una specie di causalità o intenzionalità ed ogni cosa al loro interno è vista come costituente di dinamiche che servono ad un più grande scopo, piuttosto che in conflitto e riconfigurabili. Questo rende spesso entrambe le ideologie cieche verso le espressioni di un mondo migliore che cresce nel guscio del vecchio, così come alle oppoortnità di una significativa resistenza e cambiamento positivo che non devono per forza essere delle cataclismiche rotture totali.

## <a name="q12"></a>L'anarco-transumanesimo si interseca con il Veganismo?

Fortissimamente! Biohacker anarchici hanno lavorato su progetti come l'ottenere dal lievito gli enzimi del latte che si trovano nel formaggio - basta mettere il lievito in un recipiente caldo con dello zucchero e lasciare che vada a male! Altri ad esempio hanno lavorato sulla produzione di alghe modificate che forniscono molti più modi di produrre proteine utili e carboidrati dalla luce solare rispetto all'agricoltura convenzionale, rimuovendo anche il bilancio delle vittime fra gli operatori di trattori.

Ancora oltre, nel lungo periodo dopo aver ripopolato la maggioranza del pianeta con la fauna selvatica, una più consapevole amministrazione del nostro ecosistema potrebbe permetterci di fare modifiche per ridurre al netto la sofferenza. Oppure scoprire come parlare con i Delfini persuadendoli a non essere dei tali stronzi stupratori assassini.

## <a name="q13"></a>Come affronta l'anarco-transumanesimo i problemi legati ai diversamente abili e non-neurotipici?

Come ci si può aspettare, la posizione del transumanesimo e dell'anarco-transumanesimo è di permettere a milioni di architetture fisiche e cognitive di sbocciare! Vogliamo attaccare radicalmente e rimuovere le stimmate e le costrittive norme sociali facendo sì che una grande varietà di esperienze possa essere vissuta senza oppressioni. Allo stesso tempo vogliamo anche fornire alle persone gli strumenti per esercitare il controllo sui propri corpi, menti e condizioni di vita. Dovrebbe essere scelta di ogni individuo determinare cosa possa costituire una menomazione oppressiva nelle proprie vite... oppure qualcosa che sia parte della propria identità ed univoca esperienza di vita.

Infine cerchiamo di sfumare la distinzione fra "disabilità" e "miglioramento" come vi è fra "volere" e "necessitare". Non si dovrebbe normalizzarsi oppressivamente ad un "minimo comune".

## <a name="q13"></a>Perchè il colore blu?

Il blu ha una lunga storia come simbolo del futuro. Il blu è il colore del mare e del cielo, degli orizzonti lontani che aspettano di essere esplorati. Il pigmento blu è molto raro in natura, dove le rose ed i fiori blu significano generalmente l'artificiale, il futuristico, lo speranzoso, e l'infinito. Il blu è in modo schiacciante [il colore caratteristico][4] usato nella fantascienza.

Il blu inoltre implica ampiamente l'accelerazione e la velocità e più generalmente, quando tutti i colori "virano al blu" quando un osservatore accelera in direzione di un oggetto.

Certamente abbiamo più semplicemente scelto il blu un decennio e mezzo fa perché sulla tavolozza dei colori delle scuole anarchiche era l'ultimo colore principale a non essere stato ancora reclamato. Volevamo fondare e difendere le nostre idee ed aspirazioni in un modo che non seguisse i tradizionali argomenti degli anni '90 rosso contro verde. Era importante differenziarci dalle più convenzionali correnti del sindacalismo e comunismo, senza cercare di negare o dominare le rappresentazioni esistenti di esse. Molti di noi sono entusiasti delle classiche ambizioni condivise da Kropotkin e Bookchin, altri sono post-sinistra intensamente critici riguardo all'organizzazionalismo ed alla rigidità ideologica, altri arrivano da tradizioni più orientate al mercato come il mutualismo. Ma molte di queste differenze sono ortogonali al nostro obiettivo condiviso riguardo alla condizione fisica ed ai mezzi tecnologici.

I dibattiti più interessanti sono fondamentalmente non riguardo ai sistemi economici del diciannovesimo secolo ma su come vogliamo vivere nell'universo e come dovrebbero essere i nostri valori rispetto ad esso. Nel dibattito verde contro blu ci sembra piacevolmente adeguato che i primitivisti abbiano preso il colore della terra e noi quello del cielo.

Certamente si deve prender nota che in molti altri contesti il simbolismo del colore può variare ed in un certo numero di paesi - con qualche notevole eccezione - quando i partiti politici esprimono il loro orientamento in un colore, il blu viene spesso reclamato dai conservatori. Ma se è per questo nello spettacolo della politica statale ci sono altri colori rivendicati da dei deplorevoli bastardi. Nero dal fascismo. Rosso dagli stalinisti e dei nazisti. Rosa dai socialdemocratici. Sentiamo di non doverci preoccupare dagli schemi interni di colore dei nostri nemici più di quando non ci preoccupiamo degli schemi interni di colore degli anarchici. La nostra politica è ovviamente l'esatto opposto del conservativismo.

[1]:http://blueshifted.net/faq/
[2]:http://astro1.panet.utoledo.edu/~relling2/PDF/pubs/life_cycle_assesment_ellingson_apul_(2015)_ren_and_sustain._energy_revs.pdf
[3]:{filename}/Blog/lockdown.md
[4]:http://99percentinvisible.org/episode/future-screens-are-mostly-blue/
