title: I "Costi Impazziti" sono solo un sintomo
date: 2017-07-07 19:23
tags: società, politica, economia

Ho trovato un bell'articolo scritto in modo molto chiaro che spiega alcune cose sulle quali ho sempre avuto dei dubbi. Premetto che sono abbastanza ignorante riguardo all'economia, quindi qualsiasi commento a riguardo è molto gradito.

Ho trovato l'articolo sul sito [Center for a Stateless Society][1]

Di *Kevin Carson*. Originale pubblicato il 24 giugno 2017 con il titolo _The “Cost Disease” is Really Just a Symptom_. Traduzione di [Enrico Sanna][2].

Su *Slate Star Codex* Scott Alexander ha scritto un lungo, lungo, LUNGO articolo (“*Considerations on Cost Disease*,” 9 febbraio) che indaga sulle cause possibili dei “costi impazziti”, ovvero l’aumento continuo di costi e prezzi in certi settori economici in rapporto al prodotto finale. Dice:

> “ATTENTI, IL NOSTRO PROBLEMA PRINCIPALE È CHE TUTTE LE COSE PIÙ IMPORTANTI COSTANO IL DECUPLO DI PRIMA SENZA ALCUNA RAGIONE, MENTRE LA QUALITÀ SCADE E NESSUNO SA PERCHÉ, E NOI CI AGITIAMO DISPERATI ALLA RICERCA DI SOLUZIONI.”

Dal 1970 ad oggi la spesa per studente è più che raddoppiata in termini reali, e i risultati sono invariati. Le rette universitarie sono mediamente decuplicate dal 1980: giudicate voi se la qualità dell’insegnamento è migliorata di conseguenza. I costi della sanità sono quintuplicati dagli anni Settanta, e il costo annuale di un’assicurazione sanitaria è passato da dieci a sessanta giorni di salario. E se è vero che l’aspettativa di vita è cresciuta, è vero anche che è in linea con quella di paesi in cui la sanità costa un quarto della nostra (degli Stati Uniti, *ndt*). Ed è alle stelle il costo per miglio di infrastrutture come le metropolitane, e la casa.

All’origine non c’è il costo della manodopera, nota Alexander, perché in tutti questi settori le paghe medie sono rimaste piatte o invariate, o sono cresciute grossomodo seguendo la media salariale (che non è cresciuta un granché negli ultimi decenni). Alexander prende in considerazione varie possibili cause, alcune più plausibili di altre.

La più plausibile, se non sicura, è che “i mercati non funzionano [o non riescono a funzionare]” (ovvero, il meccanismo della concorrenza, dato per scontato dagli economisti neoclassici, per qualche ragione non funziona affatto). Alexander scarta l’ipotesi di “pura speculazione sui prezzi” perché i profitti non sono aumentati proporzionalmente, ragionamento che presume che le aziende servano principalmente a fare profitti per gli azionisti e non per pagare compensi gonfiati ai dirigenti.

In ogni caso, i “costi impazziti” furono notati a suo tempo da critici del capitalismo come Paul Goodman e Ivan Illich. Illich nota che i monopoli estremi soffocano la concorrenza delle più convenienti alternative locali, rendono impossibile una povertà dignitosa, e aumentano del 300-400% oltre il necessario i costi di realizzazione di ogni cosa. Goodman spiega come la mentalità organizzativa di una gerarchia burocratica gonfia sistematicamente i costi operativi:

> In definitiva, ciò che gonfia i costi delle attività operanti in un sistema sociale centralizzato e intricato, che si tratti di attività economiche, governative, o istituzionali no-profit, è l’insieme dei fattori organizzativi, procedurali e motivazionali che non dipendono direttamente dalla funzione o dal desiderio di adempiere alla funzione stessa. Parlo di brevetti e rendite, prezzi drogati, tariffe sindacali, vantaggi economici, benefici aggiuntivi, compensi legati allo status, rimborsi spese, uffici inutili, burocrazia, costi generali, immagine e relazioni pubbliche, spreco di tempo e capacità tramite la divisione in dipartimenti, ruoli ad acta, mentalità burocratica che risparmia sulle matite e scialacqua in jet, procedure ingessate e calendarizzazioni che esagerano prospettive e straordinari.

In realtà, i “costi impazziti” non sono una malattia. Sono il sintomo di un fenomeno più profondo, il risultato di forze strutturali al cuore del capitalismo.

Primo, il fatto che le grosse aziende oligopolistiche operino da posizioni di falsa abbondanza. Dato che ogni ramo industriale è dominato da una manciata di aziende che utilizzano i prezzi come guida, la competizione sui prezzi viene sostituita dall’addomesticamento dei prezzi stessi, cosa che permette di scaricare i costi sul consumatore. Questa possibilità di impostare i prezzi ad un livello pari ai costi più il markup (spese gestionali più profitto) dà come risultato lo stesso incentivo a massimizzare i costi che, come nota Seymour Melman in *The Permanent War Economy*, è diffuso nel complesso militare-industriale e nei servizi regolamentati.

Dato il regime oligopolistico dei prezzi e la scarsa concorrenza, le grandi aziende riescono a finanziare i nuovi investimenti interamente con le riserve contabili; non solo, ma riescono anche ad avere riserve contabili che eccedono di molto un livello ragionevole di spesa. È così che le aziende trattano le spese per il miglioramento del capitale come “soldi gratis” e si avventurano in progetti che una vera azienda competitiva considererebbe uno spreco di denaro.

A causa di questa abbondanza artificiale di capitali da reinvestire, e della possibilità di scaricare sui consumatori, sotto forma di markup, i costi degli sprechi, le aziende sono incentivate a perseguire una strategia di sostituzione del capitale al fine di disciplinare meglio i lavoratori e ridurne il potere contrattuale a livelli altrimenti svantaggiosi.

Nelle grandi aziende, lo spreco risultante da queste spese interne ricorda le previsioni di Friedrich Hayek sul genere di investimento prevalente in un’economia pianificata in cui i prezzi non agiscono da segnali.

> [Dobbiamo aspettarci] lo sviluppo eccessivo di alcune linee di produzione a spese di altre e l’utilizzo di metodologie inadatte alle circostanze. In alcune industrie ci sarebbe un sovraimpiego a costi non giustificati dall’importanza dell’aumento produttivo, mentre verrebbe frustrata l’ambizione di tecnici che vorrebbero applicare gli ultimi ritrovati altrove senza pensare se sono economicamente adeguati alla situazione. In molti casi, l’utilizzo di modi di produzione più evoluti, che non verrebbero adottati in assenza di pianificazione centrale, sarebbe il sintomo di un uso errato delle risorse e non la prova del successo.

Da questa falsa abbondanza, risultato dell’addomesticamento dei prezzi e delle enormi riserve contabili, segue tutto il resto. La tendenza cronica del capitalismo monopolistico all’eccesso di capitale investito spinge strutturalmente allo spreco. Ed è incoraggiato, tra l’altro, da meccanismi di rendiconto che trattano il consumo di risorse in ingresso, in quanto tale, come qualcosa che crea valore.

È il caso delle regole standard di rendicontazione, che trattano la manodopera, e solo quella, come unica fonte di costi diretti. Il risultato è la proliferazione di esperti in efficienza che cercano ossessivamente di tagliare ogni minuto di lavoro, spesso col risultato di abbassare drasticamente l’efficienza vuotando il capitale umano, le relazioni umane e la conoscenza distribuita, tutte fonti di gran parte della produttività dell’organizzazione. E mentre i manager fanno le capriole per ridurre il costo della manodopera, sperperano grosse somme in paghe amministrative e progetti.

Secondo queste regole di rendicontazione, i compensi dei dirigenti e le spese capitali sono considerati costi indiretti, facenti parte dei costi generali. Questi costi sono liquidati con un trucchetto contabile chiamato “assorbimento dei costi generali”, così che queste spese sono assorbite dal prezzo di trasferimento interno dei beni “venduti” all’inventario. E dato che l’inventario è un bene, più crescono i costi generali e il markup di questi prezzi di trasferimento artificiali, e più cresce il valore dei beni in magazzino.

Da un lato le aziende tagliano la manodopera e accelerano la produzione per “risparmiare denaro”, e dall’altro danno paghe incredibili ai dirigenti e buttano soldi in spese irrazionali che gareggiano con l’irrazionalità della vecchia economia pianificata sovietica.

Lo stesso principio si usa per calcolare il prodotto interno lordo: qualunque somma spesa per produrre beni o servizi va ad aggiungersi al pil, come se fosse valore prodotto. Più è inefficiente la produzione, più risorse si sprecano per produrre beni e servizi, più è alto il tasso di risorse naturali che passano dalla nostra casa alla discarica a causa dell’obsolescenza programmata, e più grande appare la nostra “ricchezza” nazionale.

Secondo Alexander, il fenomeno dei costi impazziti getta una nuova luce sul modo tradizionale d’intendere molte questioni politiche. Gran parte delle diatribe su temi come l’assistenza sanitaria universale, il college gratuito o la riforma scolastica proposta dalla destra che considera “i sindacati degli insegnanti” il nemico principale, sono il risultato di una lotta su come “distribuire le nostre perdite” a somma zero.

> Esempio: qualcuno propone il college gratuito per tutti ricordando i tempi in cui la classe media poteva permettersi di pagare il college. Altri sono contrari e ricordano i tempi in cui le persone non dipendevano dalle regalie dello stato. Entrambi hanno ragione! Mio zio pagava la retta di un ottimo college facendo lavoretti in estate: niente di impossibile in tempi in cui il college costava un decimo di oggi. Favorevoli e contrari al college gratuito sono però in contrasto su come distribuire le nostre perdite. Ai vecchi tempi, potevamo combinare tasse basse e ampie scelte in materia di istruzione. Oggi no, e per questo dobbiamo decidere quale dei due valori sacrificare.
> 
> Oppure: alcuni ce l’hanno con i sindacati degli insegnanti, dicono che con l’aumento dei costi tolgono “dinamismo” all’istruzione. Altri li difendono strenuamente, e dicono che gli insegnanti lavorano troppo e sono sottopagati. Ancora una volta, nel contesto dei costi impazziti, entrambi hanno chiaramente ragione. I contribuenti cercano di difendere il loro diritto ad un’istruzione a basso costo come in passato. Gli insegnanti cercano di difendere il loro diritto a guadagnare tanto quanto in passato. Contribuenti e sindacati degli insegnanti sono in conflitto su come distribuire le perdite. Qualcuno deve fare un passo indietro rispetto ad una generazione fa. Chi?

Il tipico approccio progressista consiste nel garantire che un prodotto come l’istruzione o la sanità sia un diritto, il che in termini pratici significa lasciare intatta tutta la cultura burocratica ad elevato costo fisso e spendere abbastanza da poter comprare il prodotto per tutti ad un esorbitante prezzo di monopolio.

> Nel contesto dei costi impazziti, il risultato è che i fornitori di questi servizi raddoppiano, triplicano, decuplicano i prezzi e lo stato semplicemente dice “va bene,” e aumenta le tasse per poter pagare la cifra richiesta.
> 
> Se offriamo il college gratis a tutti, risolviamo un grosso problema sociale. Ma ne blocchiamo il prezzo ad un livello dieci volte il necessario senza alcuna ragione.

Tutto ciò non solo è fiscalmente insostenibile, ma fa passare per “scroccone” chi, senza tutti quegli sprechi, senza l’escalation dei costi causata da burocrati irresponsabili, non avrebbe bisogno di assistenza.

Le tecnologie dell’abbondanza sono per loro natura molto deflazionarie. La loro tendenza ultima è la riduzione dei costi marginali di beni e servizi, nonché del lavoro necessario a produrli, sempre più verso zero. Solo in un sistema economico fortemente drogato le tecnologie che creano abbondanza, e la conseguente riduzione del lavoro, possono essere considerate una catastrofe per i lavoratori. E però sono catastrofiche; e la ragione sta nel fatto che il risparmio nei costi dovuto all’aumento della produttività, invece di andare a beneficio dei lavoratori dando loro la possibilità di mantenere lo stesso stile di vita lavorando meno, sono assorbiti dalle classi abbienti sotto forma di rendita generata da una scarsità artificiale.

E la risposta dei “progressisti” è sostanzialmente hamiltoniana: primo, tenere alto il prezzo di beni e servizi con inutili cariche dirigenziali e costi generali, e rendite derivanti da copyright e brevetti, così da generare un flusso di incassi sufficiente a pagare i lavoratori; secondo, fare in modo che la produzione richieda molta manodopera così da offrire un numero di “posti di lavoro” sufficiente a garantire i salari con cui pagare beni e servizi a prezzi gonfiati dall’inefficienza. Il risultato ricorda una di quelle macchine progettate da Rube Goldberg.

Bisognerebbe, invece:

1. Distruggere tutto lo spreco in input, la produzione superflua, l’obsolescenza programmata, la manodopera superflua, al fine di ridurre le ore lavorative allo stretto necessario e i costi di produzione ai minimi assoluti; e allo stesso tempo…

2. Abolire i privilegi e i monopoli che permettono alle classi abbienti di intascare, come rendita, i guadagni in produttività dati dai miglioramenti tecnologici, e…

3. Usare mezzi di produzione snelli e di piccola scala per togliere il più possibile la produzione dalla sfera del lavoro salariato e dirigerla verso la sfera sociale; così che…

4. Tutti i costi che l’aumentata efficienza permette di risparmiare vadano al pubblico sotto forma di meno ore lavorative e prezzi più bassi, mentre le rimanenti ore di lavoro pagato vengono distribuite, così da poter ripagare il valore pieno di tutto ciò che si produce.

[1]:https://c4ss.org/
[2]:https://c4ss.org/content/author/enrico
