title: Regno a venire (un frammento)
date: 2015-09-12 16:12
tags: libri, società, racconti
summary: di James Graham Ballard, da "Regno a venire"

di _James Graham Ballard_, da "Regno a venire"

..."Mio padre e l’incubo consumistico? Credo che ci sia un legame. Un sacco di
gente sta impazzendo senza rendersene conto."

"Tutti questi centri commerciali, la cultura degli aeroporti e delle
autostrade. È una nuova forma di inferno..." Sangster si alzò e si portò le
manone alle guance, come se cercasse di sgonfiarsele. "Questa è la prospettiva
di Hampstead, il punto di vista della Tavistock Clinic. L’ombra della statua di
Freud che si staglia sulla terra e funge da *agente arancio* dell’anima. Mi
creda, qui lecose sono diverse. Dobbiamo preparare i nostri ragazzi a un nuovo
tipo di società. Non ha senso parlare loro della democrazia parlamentare, della
chiesa e della monarchia. I vecchi ideali di educazione civica che erano alla
base della nosrta istruzione sono concetti alquanto egoistici. Tutta
quell’enfasi sui diritti dell’individuo, sull’*habeas corpus*, sulla libertà
del singolo contrapposto alla massa..."

"E la libertà di parola, il diritto alla privacy?"

"Che senso ha avere libertà di parola se non si ha nulla da dire? Ammettiamolo:
la maggior parte delle persone non ha proprionnulla da dire, e lo sanno anche
loro. E la privacy che senso ha se è soltanto una prigione personalizzata? Il
consumismo è un’impresa collettiva. Le persone hanno voglia di condividere,di
celebrare, vogliono sentirsi unite. Quando andiamo a fare shopping partecipiamo
ad una cerimonia collettiva di affermazione."

"Quindi essere moderni al giorno d’oggi significa essere passivi?"

Sangster diede una manata sulla scrivania, facendo cadere il portapenne. Si
sporse verso di me, e l’enorme soprabito lo avvolse in tutta la sua grandezza.

"Lasci stare la modernità. Si rassegni, Richard. La missione a favore della
modernità è sempre stata profondamente controversa. I fautori della modernità
ci hanno insegnato a non fidarci di noi stessi e a non amarci. Tutte quelle
storie sulla coscienza individuale, sul dolore solitario. La modernità si
basava sulla nevrosi e sull’alienazione. Basta guardare l’arte, l’architettura
che hanno espresso. Hanno qualcosa di molto freddo."

"E il consumismo invece?"

"Celebra la possibilità di consumare insieme. I sogni e i valori sono
condivisi, come le speranze e i piaceri. Il consumismo è un atteggiamento
ottimista e lungimirante. Naturalmente ci chiede di imparare a rispettare la
regola del più forte. Il consumismo è una nuova forma di politica di massa. È
qualcosa di molto teatrale, ma in fondo ci piace. È spinto dalle emozioni, ma
le sue promesse sono raggiungibili, e non si tratta solo di ampollosa retorica.
Una macchina nuova, un nuovo lettore cd."

"E la razionalità? Non c’è posto per la razionalità, immagino."

"La ragione, be’..." Sangster tornò dietro la scrivania, portandosi sulle labbra
le dita con le unghie rosicchiate. "È parente stretta della matematica. E la
maggior parte delle persone se la cava male in aritmetica e, comunque, in
generale il mio consiglio è quello di stare alla larga dalla razionalità. Il
consumismo celebra il lato positivo dell’equazione. Quando compriamo qualcosa
inconsciamente crediamo che ci sia stato fatto un regalo."

"E la politica richiede che ci sia un costante flusso di regali? Un altro
ospedale, un’altra scuola, un’autostrada?..."

"Proprio così. E sappiamo cosa succede ai bambini che non ricevono mai
giocattoli. Oggi siamo tutti come bambini. Che ci piaccia o no, soltanto il
consumismo può tenere unita la società moderna perché muove le guiste corde
emotive."

"Ma allora... il liberalismo, la libertà, la ragione?"

"Hanno fallito! La gente non vuole più che le si parli in nome della
razionalità." Sangster si piegò in avanti e fece scivolare il bicchiere di
sherry sulla scrivania, come se aspettasse che si potesse alzare da solo. " Il
liberalismo e l’umanitarismo sono dei grossi freni per la società. Fanno leva
sul senso di colpa e sulla paura. Le società sono più felici quando la gente
può spendere e non risparmiare. Adesso abbiamo bisogno di un consumismo
delirante, quel genere di comportamento che si vede in occasione dei motorshow.
Spettacoli visivamente entusiasmanti, una specie di eterna campagna elettorale.
Il consumismo riempie quel vuoto che è alla base delle società secolari. La
gente ha un enorme bisogno di autorità che soltanto il consumismo può
soddisfare."

"Compra un nuovo profumo, un nuovo paio di scarpe e sarai una persona migliore,
più felice? E come riesce a comunicare tutto questo ai suoi adolescenti?"

"Non ce n’è bisogno. È nell’aria che respirano. Non lo dimentichi mai, Richard:
il consumismo è un’ideologia di redenzione. Quando funziona cerca di
estetizzare la violenza, anche se spesso non ci riesce..."...

Intervista
----------
Aggiungo un'intervista fatta da Valerio Evangelisti a J.G. Ballard per XL nel
novembre 2006.

> "La società inglese si sta ritribalizzando, è svuotata di ogni ideale, di
> ogni spinta sociale. Non resta che il consumismo. Ci stanno drogando con i
> beni di consumo e dobbiamo svegliarci".
>
> **J.G. Ballard su Regno a venire, il suo nuovo romanzo.**

> "Vedo periferie che si diffondono per il pianeta, la suburbanizzazione
> dell’anima, vite senza senso, noia assoluta. Una specie di mondo della tv
> pomeridiana, quando sei mezzo addormentato... E poi, di tanto in tanto, bum!
> Un evento di una violenza assoluta, del tutto imprevedibile: qualcosa come un
> pazzo che spara in un supermercato, una bomba che esplode. È pericoloso".

L'intera intervista:

L’incubo è già là fuori, accanto a un’autostrada dove il Metro center, uno
sterminato supermarket, ha usurpato le funzioni che un tempo aveva la chiesa.
Un cambiamento di sistema che ha la forza assoluta di uno tsunami. di fronte al
quale nessuna resistenza sembra possibile. Ecco Regno a venire, il nuovo
romanzo di James Ballard. Se nel suo Crash (1973), diventato uno scioccante
film di Cronenberg, le auto dominavano anche il nostro eroe, qui
elettrodomestici, computer e tv sono oggetti di culto. E nelle strade marciano,
razzisti e volgari, i tifosi avvolti nell’Union Jack quali moderne SS in cerca
di un leader. "Penso che, in Inghilterra la classe operaia bianca si stia
‘ritribalizzando’", spiega Ballard al telefono. "Sì, dopo la caduta delle
ideologie rivendica la sua identità tribale, la tribù inglese. E lo sport è il
mezzo con cui lo fa. E può essere aggressiva, violenta. Abbiamo visto tutto
questo durante la Coppa del Mondo. Oggi viviamo nella cultura dei consumi, non
c’è nulla. nient’altro. In particolar modo in Inghilterra, non c’è nient’altro.
Nessuno più crede nelle ideologie politiche. In generale nessuno crede. Quella
inglese è una società secolare, non andiamo più in chiesa. Le chiese sono così
brutte. Se fossi italiano mi farei immediatamente cattolico e bacerei la terra
dove cammina il Papa perché le chiese sono belle. Qui le chiese sono brutte. Le
religioni sono morte, la monarchia non è rispettabile. Ha ucciso Lady D e il
popolo britannico non la perdonerà mai. Non siamo nemmeno più orgogliosi delle
nostre forze armate. E la politica ovviamente è svuotata di ogni autorità o
rispetto. Il Primo Ministro britannico vive di fantasie. Lo sanno tutti che
vive in un mondo di sogni. Quindi non ci resta che il consumismo: andiamo a
fare shopping".

**Ma cosa pensi esattamente di Tony Blair?**  
È un caso triste. Come ho detto, vive di sole fantasie. Crede alle proprie
illusioni, è pazzesco, Ha portato questo paese in guerra contro l’Iraq sulla
base di falsità. Ha sostenuto che Saddam aveva armi di distruzione di massa:
mentre ovviamente non ne aveva. Blair ci ha portati in guerra a suon di bugie.
E stato un danno enorme per la pace e la stabilità del mondo e noi ne pagheremo
le conseguenze. Con il sangue, e a lungo. Il fatto che abbiamo rieletto Blair è
la dimostrazione che la politica non viene presa sul serio. ci sono Legami tra
le periferie di Londra di Regno a venire e L’Inghilterra di Blair? "Legami
stretti, strettissimi. Le periferie di Londra sono ai margini, vicino ai grandi
sistemi autostradali. Nuove città, parchi commerciali, nuovi stabilimenti
industriali, aeroporti: è questa la vera Inghilterra. La vera Inghilterra non è
Westminster o Buckingham Palace: questo è solo show business per i turisti.
L’Inghilterra vera è qui, dove vivo io, accanto alla M25 o alla M3. Dove hanno
votato Thatcher, e poi Blair".

**Nei tuoi libri c’è una società apparentemente piacevole e moderna che nasconde
un tasso di violenza crescente. È un pericolo vero? E, se si, in Inghilterra o
in tutto l’Occidente?**
Sì, penso che sia una minaccia. Ricordo che qualche anno fa qualcuno mi chiese:
come definirebbe il futuro? Io risposi: è facile, il futuro sarà noioso. Saremo
tutti annoiati e quando la gente è annoiata, come i bambini che si annoiano,
comincia a rompere i giocattoli. Vedo periferie che si diffondono per il
pianeta, la suburbanizzazione dell’anima, vite senza senso, noia assoluta. Una
specie di mondo della tv pomeridiana, quando sei mezzo addormentato... E poi,
di tanto in tanto, bum! Un evento di una violenza assoluta, del tutto
imprevedibile: qualcosa come un pazzo che spara in un supermercato, una bomba
che esplode. È pericoloso.

**La suburbanizzazione significa che stiamo perdendo il centro delle cose o
invece che il centro si è fatto televisivo? La violenza è nei media, quindi
essere violenti è come essere al centro di qualcosa?**  
Proprio così! Io penso che la violenza abbia un ruolo molto particolare, oggi.
Il futuro ci riserverà psicopatologie. La gente è disposta a tollerare livelli
di psicopatologia sempre più elevati nella vita moderna. Livelli impensabili 50
anni fa. Come questa specie di apertura verso la pornografia, il che, tra
l’altro, è un bene.  Mi piace. La pornografia è bene, è controcultura. Il
capitalismo ha una grande inventiva, una capacità di trasformarsi con
brevissimo preavviso. Se qualcosa non va e tu non vuoi comprarla, non fa
niente! Inventeremo qualcosa di nuovo, riempiremo i negozi con qualche novità.
Ecco, io temo che la gente — annoiata per la maggior parte del tempo e senza
nulla per cui vivere, specie in Inghilterra — si lascerà andare alle
psicopatologia perché sono divertenti, sono esaltanti Siamo tutti un po’ folli
e ci possiamo divertire facendo matti.  E li che si annida il pericolo, una
specie di nuovo fascismo che sorge.

**Pensi che oggi ci sia veramente la minaccia dal fascismo? Dal libro
sembrerebbe di sì.**  
Non penso che il tipo di fascismo che sta per arrivare sia quello anni 30. Non
ci saranno stivali militari, Fuhrer che strepitano, niente Sturmtruppen. Non
sarà quel tipo di fascismo. Sarà un fascismo da tv, molto light, se è chiaro
cosa voglio intendere. Il nostro Fùhrer non sarà come Hitler, sarà più come uno
show pomeridiano. Mi pare che voi in talia abbiate tentato di avvicinarvi un po
a questo modello con Berlusconi.

**Sì. Cosa pensi di Berlusconi?**  
Molti commentatori hanno detto: quando Berlusconi era primo ministro c’era un
nuovo tipo di fascismo all’orizzonte, Controlla tutte quelle emittenti
televisive, tutti quei quotidiani, ecc, lo non so se tutto questo sia vero, ma
forse qui c’era l’inizio di qualcosa. L’uso dei mezzi di comunicazione di massa
per un nuovo tipo di politica emotiva. Perché questa è la chiave di tutto: le
emozioni. Blair lo ha dimostrato. Le emozioni sono sempre con noi, Non pensate
mai: è un errore pensare. Usate solo le emozioni. La gente è così. Oggi i
giovani uomini sono molto emotivi. E per questo che sono pericolosi. Sono
pericolosi al volante, quando girano in bande, quando si ubriacano... Sono
pericolosi quando la loro ragazza esce con un altro".

**Non sanno controllare le emozioni?**  
Esattamente.

**Hai scritto libri di ogni genere, saggi di sociologia e politica, hai un ruolo
importante nella Letteratura moderna, ma c’è chi ti definisce un semplice
scrittore di fantascienza, ovviamente uno dei migliori. Ti disturba?**  
No, in verità no. Molti anni fa scrivevo fantascienza. Ma non ho scritto
fantascienza per trent’anni o forse più. Non mi vedo più come uno scrittore di
fantascienza, però lo ero, e quindi la cosa non mi preoccupa.

**I tuoi primi lavori come Deserto d’acqua o Il vento dal nulla trattavano della
società che ci circondava. Ma erano ambientati in un’epoca molto futura. Il
Metro center invece è a pochi passi dal nostro tempo. Vediamo meno Lontano?**  
Sì. Non abbiamo più una visione del futuro. Molti pensano che il futuro sarà
esattamente come il presente. come oggi. Da giovane, negli anni 30, mentre
crescevo, tutti così come alla fine degli anni 40 e negli anni 50 — tutti
avevano grande consapevolezza del futuro perché ogni cosa cambiava cosi
rapidamente: gli aerei erano più veloci, le macchine erano più veloci, e poi
dopo la guerra sono arrivati gli antibiotici, le armi nucleari... I Jet
facevano il giro del mondo.  Il cambiamento arrivava a una velocità tale che
oggi, al confronto, non c’è più alcun cambiamento. Tutto è fatto per
raccogliere applausi, per così dire.

**Solo microcambiamenti?**  
Sì, piccolissimi cambiamenti. Non sono apprezzabili. E molto strano: ci sono
grandi cambiamenti, come Internet ad esempio, ma in realtà la vita nel suo
assieme non è molto diversa da come lo era 10 anni fa.  Non è cambiata
drasticamente. Quindi penso che ci sia il rischio che a morire sia la stessa
idea di futuro.

**Cosa pensi della fantascienza di oggi? Può essere sovversiva come in
passato?**  
La fantascienza è morta il giorno in cui Armstrong ha messo piede sulla Luna,
nel 1969. Penso che allora si sia messa la parola fine. Da allora molti dei
sogni della fantascienza si sono avverati. I trapianti, la manipolazione
genetica... Vuoi che tua figlia somigli alla Lollobrigida? Oggi è possibile.

**Dobbiamo accettare la realtà di Regno a venire, o è possibile reagire? Dacci
una speranza, anche piccola.**  
Bisogna aprire gli occhi. In Occidente stiamo correndo il rischio di marciare
come sonnambuli verso un incubo, Ne abbiamo avuto un assaggio con l’11
settembre a New York. In un certo senso, l’11 settembre è stata una specie di
sveglia: "Svegliati, America", io penso che si siano svegliati, ma che siano
scesi dalla parte sbagliata del letto. Hanno invaso l’Iraq, sbaglio enorme. Ma
quella era una sveglia. E la guerra contro il terrorismo islamico è molto vera.
In tutto il mondo, tra tante cose dobbiamo farne soprattutto una: dobbiamo
svegliarci. Noi occidentali stiamo molto comodi. Viviamo in belle case, non
abbiamo fame e, se ci ammaliamo, qualcuno si prende cura dì noi. Molto comodo.
Dobbiamo svegliarci. Ci stanno drogando con i beni di consumo. Non siamo più in
grado di badare a noi stessi e invece dobbiamo cominciare a badare a noi
stessi.

**Ma in pratica cosa possiamo fare, smettere di consumare?**  
Ah, che domanda. La mia generazione ha già tentato di rispondere: ora
rispondete voi, io sono troppo vecchio.
