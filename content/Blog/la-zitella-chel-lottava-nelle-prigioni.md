title: La "zitella" che lottava nelle prigioni
date: 2015-10-26 22:22
tags: storia, etica, società, veg, carceri, diritto
summary: Riporto dalla pagina Facebook di Aida Vittoria Eltanin, autrice dei libri *La dieta di Eva* e *La salute di Eva*.

Riporto dalla pagina Facebook di Aida Vittoria Eltanin, autrice dei libri *La dieta di Eva* e *La salute di Eva*.

Lady Constance Lytton  (1869-1923)
----------------------------------

Vorrei raccontarvi la storia di un'incredibile "zitella" vegana, dimenticata
dalla storia (come tante)...

![Lady Constance Lytton]({filename}/images/la-zitella-che-lottava-nelle-prigioni.jpg){: style="float:right"}
Constance Lytton era nata in una famiglia inglese aristocratica che definire
potente è dire poco: suo papà era il vice-re in India, sua madre era una
Contessa e suo fratello lavorava in Parlamento.

Soffriva gravemente di cuore fin da piccola ed era di salute molto
cagionevole... Per fortuna, verso i 30 anni, cambiò la sua alimentazione,
grazie alla zia.

Nel suo libro scrisse così a riguardo:

> “La stimolante compagnia di mia zia ebbe una grande influenza sulla mia
> mente. Grazie alle sue investigazioni in teorie di dieta, divenni vegana. La
> mia salute ne guadagno’ in tutte le direzioni e gradualmente mi liberai dai
> reumatismi cosiddetti “costituzionali” dei quali avevo sofferto fin
> dall’infanzia. Mi resi anche conto che in tutti quegli anni avevo causato
> una sofferenza indicibile allo scopo che mi venisse dato da mangiare e
> determinai che in futuro la morte innaturale degli animali non avrebbe dovuto
> essere piu’ necessaria per me. La mia vitalita’ miglioro’.”

Cambiare dieta a quell’eta’ non basto’ per farle recuperare per intero la sua
cagionevole salute, ma perlomeno le fece guadagnare anni preziosi e  nuova
forza.

Questa forza la utilizzò per dedicarsi alla causa femminista, non tanto per
ottenere il diritto di voto, ma per aiutare a migliorare le condizioni di vita
nelle prigioni del tempo. Quasi casualmente, verso i 38 anni, Constance aveva
scoperto le terribili condizioni e le torture che subivano le attiviste
femministe del tempo in carcere.

Anche se all'inizio non era affatto d'accordo sull'uso della violenza, si era
unita ad un gruppo di femministe militanti con l’obiettivo proprio di aiutare
quelle che finivano in prigione.

All'inizio si trattava di poca cosa. Lei stessa lo definiva "un hobby".
Doveva solo utilizzare le sue alte conoscenze e scrivere lettere, intercedere,
chiedere favori ecc.

Poi questo hobby le prese completamente la mano, il cuore e lo spirito. Mise
in questo lavoro tutto il cuore di una madre e tutta la sua creatività e
coraggio.

Questo è quello che scrisse alla madre in una lettera,  la notte prima di
essere arrestata lei stessa per la prima volta...

> “Mio angelo di madre…
>
> Le prigioni, come tu sai, sono state il mio hobby.
>
> La maternità che da qualche parte serpeggia in me è stata gradualmente
> risvegliata in questi anni dal destino dei prigionieri, il male crudele e
> deliberato che gli viene fatto all’anima e al corpo, lo spreco ignorante ed
> esasperato di buone opportunità, finchè adesso il pensiero di loro, la voglia
> di stare con loro, si muove in me e mi tira verso sè in modo altrettanto
> vitale e irreprimibile come mai un bambino in carne ed ossa può chiamare sua
> madre.”

A 40 anni, come vi dicevo, fu arrestata anche lei  a Londra, insieme a molte
altre compagne attiviste. Non avevano fatto nulla di particolare, tranne
partecipare a una dimostrazione davanti alla casa del Primo Ministro.

“Niente della gentilezza con cui era stata allevata”, scrive la sua biografa,
“l’aveva preparata ad affrontare un assalto ostile da un’ orda di  poliziotti.
Viene spinta, tirata e cade a terra più volte…” finchè venne portata in
prigione con tutte le altre.

Fu la prima, ma non ultima volta, che finì in carcere.

Le sarebbe stato facile uscire utilizzando il suo nome e le sue conoscenze, ma
lei non ha mai approfittato dei privilegi della sua casta.  Al contrario.
Voleva proprio essere trattata come le altre, per poter poi mostrare al mondo
(come fece) quanto fosse ingiusto quel sistema.

Vi basti questo aneddoto incredibile, per capire che forza di carattere aveva.

A quel tempo molte femministe  avevano deciso - come vera e propria tattica -
di fare lo sciopero della fame in prigione, in caso fossero state arrestate per
i loro “atti vandalici”. Il Governo però aveva emanato una legge che
costringeva ad ingozzare a forza le detenute ribelli (un po’ come si fa con le
famose oche da fois-gras).

Una volta arrestata, anche Constance rifiutò il cibo, come da programma. Le
cose però andarono storte.

Appena i poliziotti si resero conto di chi fosse figlia, non solo non la
forzarono a mangiare ma la rilasciarono. Le altre sue compagne invece non
furono così fortunate. Lei non se lo perdonò.

Mesi dopo si fece arrestare nuovamente.

Anche questa volta venne rilasciata dopo soli due giorni di sciopero della fame
perché, sapendo il suo nome, avevano scoperto che soffriva molto di cuore e non
volevano rischiare che morisse in carcere. Temevano che se fosse successo, le
femministe si sarebbero potute pregiare di una martire di grande fama.

Nuovamente Costance non si diede per vinta. L’anno dopo, questa volta a
Liverpool, si fece arrestare ancora una volta. Non pensate per chissà quali
crimini. Non fece mai male a una fatidica mosca.

Per essere sicura che la polizia non le riservasse un trattamento
differenziale, questa volta decise di non farsi riconoscere.


Non solo cambiò il suo nome, ma si tagliò corti i suoi bellissimi capelli
lunghi, si comprò dei vestiti da donna povera, degli occhialetti orribili di
quelli che poggiano sul naso e in generale si agghindò in modo tale da
risultare più brutta e vecchia che potesse.

Aveva infatti notato, nelle sue tante visite alle prigioni, che le donne più
brutte erano sempre quelle che subivano i trattamenti peggiori...

Se quindi vedrete una fotografia di lei come [questa a destra][1], magari con
sotto la scritta “zitella” o "femminista", sappiate che non state guardando il
suo vero ritratto!

La sera prima di mettere in atto il suo piano scrisse che la gentile padrona
della casa in cui era ospite “aveva saputo che ero vegetariana, e mi aveva
fatto preparare un piatto molto appetitoso di pere cotte.”

Il giorno dopo, quando venne arrestata e la polizia le chiese i dati
anagrafici, ebbe il coraggio di darne di falsi. Immaginatevi la scena:

> “Nome?”  
> “Jane Warton”
>
> “Professione?”  
> “Sarta”

Ed ecco che senza protezione maschile ed economica fu condannata ai lavori
forzati per 14 giorni. Cominciò lo sciopero della fame.

Qui nessuno sapeva di lei e la visita del medico fu molto meno accurata.
Nessuno si accorse che aveva seri problemi di cuore, quindi anche a lei toccò
"la tortura delle oche".

Il quarto giorno, emaciata, esausta ed in condizioni terribili, fu forzata a
mangiare per la prima volta. Se siete vegani, e sapete cosa succede alle oche
da fois-gras, consiglio di saltare la sua descrizione. Sapete già com'è.

Se invece non lo siete ancora, spero non toccherete più il fois-gras in vita
vostra dopo questa lettura. Vi avviso: io non sono riuscita ad andare fino in
fondo: Ne ho tradotto solo una parte …

> “Avevo resistito così tanto con i denti che il dottore mi disse che
> avrebbe dovuto darmi da mangiare attraverso il naso. Il dolore era così
> intenso che alla fine devo aver ceduto perché il dottore riuscì ad
> inserire una specie di imbuto tra i denti e lo girò molto più del
> necessario finchè le mie mandibole erano aperte e bloccate, più di
> quanto non potrebbero fare in modo naturale.
> 
> Poi mi mise giù
> per la gola un tubo che mi sembrò esageratamente grande, qualcosa come
> un metro di lunghezza. L’irritazione del tubo era eccessiva.
> Mi stavo strozzando nel momento che mi ha toccato la gola fino ad andar giù.
> 
> 
> Poi è stato versato velocemente il cibo; mi è venuto il vomito pochi
> secondi dopo che era sceso, e l’azione del vomito mi fece muovere le
> gambe,  ma la guardiana immediatamente mi tirò indietro la testa e il
> dottore si piegò sulle mie ginocchia.
> 
> L’orrore che è stato è più di quanto possa descrivere…”

(Ho anche scoperto cosa la forzarono a deglutire: “una brodaglia di latte, uova
e brandy”. Per forza vomitò subito, lei che era vegana…)

Subirà questa tortura otto volte in totale, finchè interverrà la sorella per
salvarla, facendo saltare la sua copertura.

Quando uscì di prigione, anche se era debolissima, decise di raccontare tutto
quello che le era successo. Visti i suoi agganci riuscì a far pubblicare il suo
articolo addirittura sul Times.

Ovviamente la reazione popolare di sdegno verso il Governo non si fece
attendere. Oltre a quell’articolo, Constance cominciò a parlare un po’ ovunque
in pubblico della sua esperienza.

La sua storia e i suoi agganci fecero sì che di lì a poco la pratica ignobile
dell’ingozzatura forzata delle prigioniere venisse cancellata e da allora le
suffragette vennero trattate come prigioniere politiche, e non come criminali
qualsiasi, con tutti i vantaggi del caso.

Quando entrò nuovamente in prigione, due anni dopo, scrisse che il posto era
irriconoscibile. Tutto era cambiato e “la cortesia era ovunque”.

Delle sue tante esperienze in carcere, sia come prigioniera che visitatrice,
scrisse un libro: “Prigioni e Prigionieri”, nel 1914.

Visse il resto della vita con la madre, che la curò e aiutò fino alla morte a
scrivere le sue memorie, nonostante non fosse d'accordo con lei.

Vi lascio con il suo epitaffio:

> “Dotata di un celestiale senso dell’umorismo, infinita compassione e
> raro talento musicale, ha dedicato gli ultimi anni della sua vita alla
> liberazione politica delle donne e sacrificato la sua salute e i suoi
> talenti per aiutare a portare vittoria a questa causa.”

Così finisce la storia di un’altra incredibile donna senza figli che ha fatto
la storia.

[1]:http://christinacroft.blogspot.it/2009/11/lady-constance-lytton.html
