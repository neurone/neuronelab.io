title: Loro sono peggio di noi
date: 2016-5-7 16:30
tags: veg, etica
summary: Quante parole si stanno spendendo in questi giorni per la decisione di una catena di supermercati, nemmeno la prima, di vendere della carne macellata secondo il metodo *Halal*... Leggo di animalisti stracciarsi le vesti per questa scelta irrispettosa per gli animali, minacciare il boicottaggio, per poi tornare a più miti consigli nel momento in cui il rappresentante della catena decide di tornare sui propri passi proseguendo a vendere non più la carne di animali ammazzati secondo la procedura *Halal*, bensì solo uccisi tramite i nostri misericordiosi metodi tradizionali. Forse questi animalisti dimenticano che anche gli animali uccisi dai nostri macellai, non sempre vengono storditi prima dello sgozzamento.

Quante parole si stanno spendendo in questi giorni per la decisione di una catena di supermercati, nemmeno la prima, di vendere della carne macellata secondo il metodo [Halal][1]... Leggo di animalisti stracciarsi le vesti per questa scelta irrispettosa per gli animali, minacciare il boicottaggio, per poi tornare a più miti consigli nel momento in cui il rappresentante della catena decide di tornare sui propri passi proseguendo a vendere non più la carne di animali ammazzati secondo la procedura [Halal][1], bensì solo uccisi tramite i nostri misericordiosi metodi tradizionali. Forse questi animalisti dimenticano che anche gli animali uccisi dai nostri macellai, non sempre vengono storditi prima dello sgozzamento. Talvolta, il [metodo per stordire o anestetizzare][2] l'animale [non riesce][3], ma l'animale prosegue comunque presso la fredda "catena di smontaggio" in stato semi-incosciente. Che differenza c'è quindi? Mi risulta inoltre, che non sia ancora possibile uccidere un animale senza ammazzarlo... Sono sicuro che se fossi un serial killer e venissi arrestato e processato, nessun tribunale mi annullerebbe la condanna se affermassi che per far soffrire meno le mie vittime, prima di sgozzarle, le tramortivo. Perché tanto sgomento verso il metodo Halal? Forse perché è rassicurante pensare che ci sia qualcuno peggio di noi? Attacchiamo l'altro per distogliere l'attenzione da noi? Mi sembra come se si volesse istituire una definizione di omicidio speciale, nel caso in cui esso venga fatto con un'arma particolare da una persona di lingua particolare. Come a dire che se è un arabo col coltello ad ammazzare qualcuno, allora quella morte, è più **morte** delle altre. Ho l'impressione che sotto sotto ci sia la solita strisciante xenofobia ed il senso di superiorità [etnocentrico][4] contro chi non si conosce e non parla la nostra lingua. Comportamento che risale sin dall'epoca in cui è stata inventata la parola "[barbaro][5]". La liberazione animale non può mai essere divisa dalla liberazione umana e dalla lotta contro tutte le discriminazioni e prevaricazioni, contro la violenza e le gerarchie. Non può essere scissa dal discorso politico. La politica è ciò che facciamo e che coinvolge il resto della popolazione. La politica siano noi (non "lo Stato siamo noi", come la frase fatta che alcuni ingenui ripetono allo scopo di chiudere il dialogo verso chi rifiuta di identificarsi con lo Stato). È necessario essere alleati delle persone che hanno una tradizione come quella della macellazione Halal e, con loro, discutere sul perché riteniamo che non sia una pratica moralmente accettabile, assieme agli altri abusi verso esseri viventi, non solo dal punto di vista etico, ma anche da quello delle necessità.

Consiglio la lettura di due articoli precedentemente pubblicati e riporto il post che scrissi nel 2010 quando una identica proposta era stata fatta da Coop.

- [In incognito in un macello][6]
- [Allevatori Saturniani][7]

## La Coop e la macellazione halal

Non è il solito discorso che troverete in rete. Ho deciso di scriverlo proprio perché mi sono accorto che nessuno dei discorsi che ho trovato, assomigliavano a quello che avevo in mente.

Da qualche giorno è stata resa nota la notizia che la catena di supermercati Coop sta iniziando a vendere nei banchi della carne, anche degli animali macellati secondo il metodo “halal“, cioè il metodo rituale utilizzato nelle religioni islamiche ed ebraiche affinché la carne sia considerata commestibile.

Si tratta di un metodo doloroso per l’animale, che viene sgozzato e lasciato dissanguare, quando ancora cosciente. In alcuni casi e secondo precise regole, può venire permesso di stordire l’animale.

Come animalista e vegano, considero questo metodo estremamente crudele e, dal mio punto di vista di non credente, irrazionale ed ingiustificato. Ma la questione non è così banale, ed il rischio di arrivare a deduzioni e conclusioni scorrette, è alto. Infatti, nello stesso problema, sono contenuti due temi diversi, quello del rispetto verso la vita e le condizioni degli animali e quello dell’incontro, l’accoglienza ed il rispetto verso civiltà diverse. Se non si affrontano questi temi separatamente, per poi arrivare ad una soluzione, si rischia di finire erroneamente in una situazione nella quale la verità di un’ipotesi, implica la falsità dell’altra e viceversa, impedendoci di accorgerci di eventuali soluzioni migliori che le soddisfino entrambe.

Nei forum animalisti, le reazioni sono state molto forti. Sono state mandare varie lettere di protesta alla direzione della Coop, la quale ha risposto con questa missiva.

> Gentile Socio/Consumatore
> 
> La preoccupazione per limitare al massimo le sofferenze degli animali in fase di macellazione è anche nostra.
> Infatti la condizione imprescindibile che abbiamo posto per rispondere alle richieste di nostri clienti di fede musulmana è riuscire a conciliare le loro tradizioni con i nostri impegni di maggior rispetto degli animali; in caso contrario non avremmo proceduto con l’inserimento di questi prodotti nei nostri negozi.
> 
> L’Imam che presiede alla macellazione ha dichiarato conforme al rito islamico la procedura di stordimento preventivo che abbiamo posto come requisito Coop.
> Va detto che siamo i primi ad aver richiesto questa procedura anche per i grandi animali e siamo costantemente alla ricerca di nuove procedure, in collaborazione con le principali associazioni animaliste, che siano ancora più rispettose del benessere animale.
> L’intera operazione viene supervisionata anche dal servizio veterinario competente, al fine di verificare il rispetto delle normative europee relative alla protezione degli animali durante la macellazione.
> 
> Crediamo in questo modo di rispondere ad una esigenza sempre più diffusa nelle nostre realtà, di creare una alternativa alla forma più cruenta di macellazione tradizionale e ancor più alla macellazione clandestina e crediamo , al contempo, di proseguire in un impegno di tutela del benessere animale che non ha paragoni in Italia e che si dispiega su vari fronti.
> 
> A solo titolo di esempio: abbiamo deciso di vendere uova di galline che non fossero allevate in gabbia, fin dagli anni 90 abbiamo inserito regole volontarie (e quindi per noi onerose) sugli spazi e le modalità di alimentazione, allevamento e trasporto dei vitelli, o ancora , primi e unici , abbiamo deciso di aderire con tutti i nostri prodotti della cosmesi alla campagna stop ai test su animali e ancor a siamo stati i primi ad aver deciso la completa esclusione delle pellicce naturali e il passaggio a materiali sintetici.
> 
> Inoltre Coop da anni, col supporto del mondo delle Ricerca Pubblica e delle principali associazioni animaliste, sta inserendo regole sul benessere animale nei propri capitolati di produzione, tramite disciplinari, (primi in Italia) valutati e validati dal Centro Nazionale di Referenza per il Benessere degli Animali da reddito.
> 
> Per quanto riguarda i soci e i clienti che hanno deciso di non consumare carni, questi trovano in Coop prodotti provenienti da filiere produttive controllate fin dal campo, il più grande assortimento di prodotti biologici della grande distribuzione italiana, prodotti che sono pensati per avere il minimo impatto ambientale, un impegno alla valorizzazione dei prodotti agricoli tradizionali e un impegno più complessivo per la salvaguardia della biodiversità.
> 
> E siamo sempre disponibili a considerare le richieste di estensione dell’assortimento di prodotti che soddisfino le esigenze di integrazione alimentare dei vegetariani, come già è successo per altre fasce di consumatori con bisogni specifici (come i celiaci).
> 
> Sono questi temi per noi molto importanti e sui quali continueremo a lavorare come abbiamo fin qui fatto.
> 
> Servizio Assistenza Clienti
> 
> Coop Italia

Apprezzo molto la buona volontà dell’azienda, ma non posso evitare di notare una certa incoerenza: parlare di benessere dell’animale nello stesso discorso in cui si cerca di giustificare il fatto che venga ammazzato per dissanguamento, è una cosa che stona parecchio. Non credo che l’incongruenza sia voluta, ma penso che sia causata dall’abitudine e dalle tradizioni, dal troppo “dare per scontato” che, per ridurre il carico cognitivo, finisce con il nascondere alla mente certi dettagli.

L’animale viene stordito, ed in teoria dovrebbe soffrire di meno. Tuttavia, mi risulta che non sia comunque ancora possibile uccidere un animale senza ammazzarlo… Sono sicuro che se fossi un serial killer e venissi arrestato e processato, nessun tribunale mi annullerebbe la condanna, se affermassi che per far soffrire meno le mie vittime, prima di sgozzarle, le tramortivo. Sono certo che la stessa cosa succederebbe se le vittime fossero animali d’affezione che, come si sa, al contrario degli altri, sono protetti dalla legge.

Come ho scritto all’inizio dell’articolo, per evitare di fare confusione, dividerò la questione in due temi.

Il primo riguarda l’uccisione degli animali, e non è per nulla diverso da quello che si farebbe e che si fa sempre, quando si parla degli allevamenti “nostrani”, intensivi e non, e di uccisione degli animali. I vegetariani ed i vegani, il discorso, lo conoscono bene. Ne ho già scritto in abbondanza in un mio precedente articolo, al quale vi rimando.

Il secondo invece, riguarda l’essere ospitali con persone dalle tradizioni diverse da quelle della nazione in cui vivono (l’Italia in questo caso). Io sono convinto che sia corretto fare tutto il possibile perché chiunque venga a vivere su di un territorio, sia rispettato al pari di chi ha avuto generazioni di parenti sullo stesso territorio. Siano essi “regolari”, “irregolari”, rom, sinti, ed in qualunque religione credano o non credano.

A me non interessa cosa dicano le leggi sull’immigrazione, non mi interessa chi è su un territorio da più tempo di altri. Sono le persone ad interessarmi.

È prevedibile che popoli diversi abbiano tradizioni diverse e che siano legati in modo più o meno forte a queste tradizioni. Questo non implica che dovremmo impedire a queste persone di seguire le proprie tradizioni, ma neppure che non dovremmo discuterne se le riteniamo crudeli. Il confronto ed il dialogo devono sempre essere possibili ed incoraggiati.

Si spera quindi – chi conosce un po’ di antropologia, e di storia, sa che questa speranza è tutt’altro che infondata – che il confronto fra varie civiltà, porti come risultato ad un meticciato culturale che trasformi tutte le parti in causa. Il rifiuto di queste pratiche crudeli non può venire imposto, ma col dialogo e con l’esempio, le stesse persone che le praticavano potrebbero non desiderarle più. Esattamente come succede spesso anche nella nostra società. Qualsiasi cambiamento non può venire imposto, ma deve emergere dall’interno. Avevo fatto un discorso simile, qualche mese fa, riguardo ad una proposta di legge per impedire l’uso del Burqa.

Io penso che la soluzione più corretta, sia di informare, di mostrare il nostro esempio, far conoscere il nostro modo di vivere e la nostra filosofia, sia agli italiani, come già facciamo, sia a chi viene a vivere in Italia. Più il nostro esempio viene conosciuto, più diventiamo numerosi. Più siamo numerosi, più il messaggio entra a far parte del senso comune e più viene accettato. Le aziende commerciali, come la Coop, che vivono con le vendite dei prodotti, non potranno ignorare le esigenze dei loro “soci/consumatori” (come ci definiscono).

Fra le persone comuni, gli esempi positivi, funzionano molto meglio di quelli negativi. Il denunciare e mostrare le crudeltà non basta, se non viene affiancato da qualcosa di propositivo, perché in caso contrario, chi dovesse ricevere il messaggio, rimarrebbe spaesato, senza alternativa valida, ed in breve tempo (anche se a malincuore perché saprebbe di fare qualcosa di sbagliato), tornerebbe alle vecchie abitudini.

La forza, le azioni violente ed eclatanti, con le persone comuni non funzionano, perché le spingono in una situazione emotiva che costringe alla difesa. Scatterebbe il rifiuto categorico ed irrazionale, che le renderebbe impermeabili ad ogni messaggio, per quanto corretto, di buon senso, o autorevole possa essere.

[1]: https://it.wikipedia.org/wiki/Halal

[2]: https://it.wikipedia.org/wiki/Macellazione#Stordimento

[3]: http://www.laverabestia.org/play.php?vid=1096

[4]: https://it.wikipedia.org/wiki/Etnocentrismo

[5]: https://it.wikipedia.org/wiki/Barbaro

[6]: {filename}/Blog/in-incognito-in-un-macello.md

[7]: {filename}/Blog/allevatori-saturniani.md
