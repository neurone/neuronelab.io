title: Lila
date: 2015-06-23 12:03:06 +0200
tags: libri, filosofia, recensioni
summary: Pirsig approfondisce la sua metafisica della Qualità, che aveva “scoperto” nel precedente Lo Zen e l’arte della manutenzione della motocicletta.

[Pirsig][1] approfondisce la sua [metafisica della Qualità][2], che aveva “scoperto” nel precedente Lo Zen e l’arte della manutenzione della motocicletta.

La sua è una metafisica molto solida e convincente, che abbandona il classico
paradigma soggetto/oggetto, per utilizzarne uno in cui la divisione è fra
staticità e dinamismo. Qualità statica e Qualità dinamica.

Se [Lo Zen e l’arte della manutenzione della motocicletta][3], era fresco ed
illuminante, per via di molte idee geniali – alla portata di mano ma invisibili
a causa del buio delle normali convinzioni e della comune cultura – e ti
lasciava entusiasta per il nuovo dinamico punto di vista scoperto, in Lila è più
frequente la logica e composta approvazione di concetti sensati e razionali,
scaturiti dall’approfondimento di quell’idea di Qualità che nel precedente libro
era stata accennata. Solo in alcuni passi si riesce a provare lo stesso brivido
di entusiasmo ed illuminazione. Ma anche questo, è correttamente spiegato e
previsto dalla metafisica della Qualità. Come ammette lui stesso, l’atto di
definire una metafisica la rende statica. Un altro punto per [Robert][1]!

Purtroppo, la sua debolezza è nell’esposizione: ci sono frequenti ripetizioni di
concetti già spiegati – quasi tutto il libro consiste in confronti fra la realtà
ed il suo schema metafisico – ed una sottotrama narrativa meno convincente ed
appassionante di quella del precedente.

Ho trovato molto interessanti le parti riguardo alla follia e quelle sui nativi
americani e l’antropologia.

È un buonissimo libro e le idee contenute meritano di essere lette e conosciute,
ma l’esposizione poteva essere molto migliore.

[1]:https://it.wikipedia.org/wiki/Robert_M._Pirsig
[2]:http://www.moq.org/italia/
[3]:https://gruppodilettura.wordpress.com/2004/07/13/zen-la-manutenzione-della-motocicletta-pirsig-e-un-articolo-dimenticato/
