title: Anarchia, antispecismo e dipendenza
date: 2014-09-03 20:34:08 +0200
tags: anarchia, antispecismo, veg, filosofia
summary: Mesi fa, sulla rivista *"A rivista anarchica"* ci fu un lungo dibattito riguardo ad anarchia ed antispecismo. La questione era se l'anarchismo dovesse o meno includere fra i suoi assiomi, assieme al rifiuto di sessismo, militarismo, razzismo, anche il rifiuto dello specismo, ovvero *"l'attribuzione di un diverso valore e status morale agli individui a seconda della loro specie di appartenenza"*, in una morale che rifiuti coerentemente ogni forma di dominio e sfruttamento.

Mesi fa, sulla rivista *"A rivista anarchica"* ci fu un lungo dibattito riguardo ad anarchia ed [antispecismo][1]. La questione era se l'anarchismo dovesse o meno includere fra i suoi assiomi, assieme al rifiuto di sessismo, militarismo, razzismo, anche il rifiuto dello [specismo][1], ovvero *"l'attribuzione di un diverso valore e status morale agli individui a seconda della loro specie di appartenenza"*, in una morale che rifiuti coerentemente ogni forma di dominio e sfruttamento.

La conclusione di alcuni, è stata che, tradizionalmente, l'anarchismo è una
ideologia [umanista][3] e che non è "obbligato" ad occuparsi delle altre specie.
Dal mio punto di vista, è una motivazione insoddisfacente. Innanzitutto, ritengo
che per l'anarchismo le tradizioni non siano vincolanti, anche se comprendo che
situazioni simili siano tipiche delle controculture: sul lungo periodo, esse
tendono a cristallizzarsi come descritto anche da [Max Stirner][4] in *L'Uno e
la sua proprietà*.

Tuttavia, il problema di fondo, rimane sulla coerenza dell'idea di rifiutare
ogni tipo di sfruttamento.

Al di là dell'aspetto scientifico e filosofico - parlando di filosofia della
mente, uno dei filosofi più influenti degli ultimi quarant'anni, [Douglas
Hofstadter][5], scrittore di [Gödel, Escher, Bach][6], è vegano - vi è l'aspetto
morale, cioè: non posso permettermi di cedere all'agire in modo sopraffatorio,
con nessun essere senziente che mi si presenti. È una questione morale
individuale.

In conclusione, vorrei cercare di andare un po' oltre all'idea di dominio, dato
che non penso che esso sia la *causa prima* di tutti i mali, ma che emerga da
elementi diversi quali la violenza e la *dipendenza*.
Se la violenza è facilmente identificabile e comprensibile, il rapporto fra
dominio e dipendenza è meno esplicito. Nessuno dovrebbe essere messo in
condizione di dipendere da altri. Chi dipende si espone alla sopraffazione ed al
dominio, in quanto subordinato alla persona o all'entità da cui dipende. Per
questo motivo, ad esempio, affidarsi a delle istituzioni per chiedere loro di
fare qualcosa per noi (sia con le buone che con la forza), non è un agire
anarchico.

*"...chiedere al potere di riformare il potere... che ingenuità!"*
recitava [Gian Maria Volontè][7] nei panni di [Giordano Bruno][8].

- [Antispecismo su A][1]
- [Specismo][2]

[1]:http://arivista.org/index.php?option=com_content&view=article&id=4&Itemid=33&key=antispecismo
[2]:https://it.wikipedia.org/wiki/Specismo
[3]:https://it.wikipedia.org/wiki/Umanismo
[4]:http://ita.anarchopedia.org/Max_Stirner
[5]:https://it.wikipedia.org/wiki/Douglas_Hofstadter
[6]:https://it.wikipedia.org/wiki/G%C3%B6del,_Escher,_Bach_-_Un%27eterna_ghirlanda_brillante
[7]:https://it.wikipedia.org/wiki/Gian_Maria_Volont%C3%A9
[8]:https://it.wikipedia.org/wiki/Giordano_Bruno
