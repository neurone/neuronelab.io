title: Solaris
date: 2014-06-10 20:44:45 +0200
tags: fantascienza, recensioni, libri
summary: Il romanzo di Stanislaw Lem è considerato un classico della letteratura fantascientifica, ed una piccola eccezione nello stile dell'autore, noto per essere molto preciso e razionale.

Il romanzo di [Stanislaw Lem][1] è considerato un classico della letteratura fantascientifica, ed una piccola eccezione nello stile dell'autore, noto per essere molto preciso e razionale.

Per sua stessa ammissione, [Solaris][2] è una storia *irrazionale, enigmatica*,
che è nata e cresciuta come avesse vita propria. Il mistero del pianeta vivente
si presta a molte interpretazioni, parecchie delle quali vengono illustrate nel
libro. Il pianeta Solaris è un tale enigma che è stata creata un'intrea area di
studi, la solaristica, per studiarlo. Molti lo considerano un essere vivente, ma
come spiegare i fenomeni che accadono vicino ad esso?
Come interpretare la sua mente, sempre ammesso che possa essere considerato una
mente e non solo una macchina? Come stabilire un contatto con una entità, così
diversa da noi? In che modo comunicare? In che modo spiegare i movimenti del suo
oceano, senza cadere nella trappola di antropomorfizzare ciò che si vede e
ridurlo ad un equivalente umano che non ha senso di essere? Ha senso parlare di
psiche ed utilizzare la comune psicologia umana? Un tema questo, del contatto
con l'alieno, condiviso da un altro romanzo Pic-Nic sul ciglio della strada dei
fratelli Strugatzki, anch'esso adattato alla pellicola da Andrej Tarkowskij in
Stalker.

![Solaris]({filename}/images/solaris.jpg "Solaris"){: style="float:right"}
Il libro offre tantissimi spunti anche sulle classiche questioni di filosofia
della mente, cioè cosa sia una mente, dove risieda la coscienza, se un clone
contenente i ricordi di una persona possa essere considerato vivo, cosciente,
quella stessa persona, oppure solo una simulazione di essa. Cosa proverebbe,
potendo percepire la continuità fra i ricordi ed il qui e ora. Se sia da
considerare una copia dell'originale, oppure una persona a sé stante, se la
base organica da cui emerge una mente sia sostituibile da altre. L'autore
stesso, ritiene che la fantascienza debba essere come un esperimento mentale,
nel quale si pongono delle ipotesi e si ragione sul “cosa sarebbe se…?”

Solaris è anche una decisa critica all'[antropocentrismo][3], cioè alla tendenza a
misurare ogni caratteristica dell'universo sull'uomo.

> Noi partiamo per lo spazio preparati a tutto, cioè pronti al sacrificio, alla
> solitudine, alla lotta, alla morte. Per modestia, non lo diciamo ad alta
> voce, ma lo pensiamo dentro di noi di tanto in tanto; pensiamo di essere
> eccezionali.  Intanto, però, non è tutto, il nostro zelo si rivela una posa.
> Non abbiamo nessuna voglia di conquistare il cosmo, noi vogliamo soltanto
> allargare fino ai suoi ultimi confini le frontiere della Terra. Certi pianeti
> devono essere deserti come il Sahara, altri freddi e ghiacciati come il Polo
> o tropicali come la giungla del Brasile. Siamo umanitari e nobili, non
> abbiamo intenzione di conquistare altre razze, vogliamo solo trasmettere i
> nostri valori e in cambio impadronirci del loro patrimonio. Ci crediamo
> cavalieri dell’ordine del Santo Contatto. Questa è una bugia. Noi cerchiamo
> solo l’uomo. Non abbiamo bisogno di altri mondi, abbiamo bisogno di specchi.
> Non sappiamo che cosa farcene degli altri mondi. Uno ci basta, quello in cui
> sguazziamo. Vogliamo trovare il ritratto idealizzato del nostro mondo!
> Cerchiamo dei pianeti con una civiltà migliore della nostra… ma che sia
> l’immagine evoluta di quel prototipo che è il nostro passato primordiale.
> Dall’altro lato, c’è in noi qualcosa che non accettiamo, contro cui lottiamo;
> ma che comunque resta, perché dalla Terra non abbiamo portato un distillato
> di virtù o una statua alata dell’uomo! Siamo arrivati qua così come siamo
> realmente, e quando l’altra faccia, cioè la parte che manteniamo segreta, si
> mostra com’è veramente… non riusciamo ad andarci d’accordo!
**Solaris, pag 80: Snaut a Chris**

[1]: https://en.wikipedia.org/wiki/Stanislaw_Lem
[2]: https://en.wikipedia.org/wiki/Solaris_(novel)
[3]: https://it.wikipedia.org/wiki/Antropocentrismo
