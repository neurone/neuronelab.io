title: La biblioteca di Babele
date: 2015-01-27 11:01:45 +0100
tags: racconti
summary: L'universo (che altri chiama la Biblioteca) si compone d'un numero indefinito, e forse infinito, di gallerie esagonali, con vasti pozzi di ventilazione nel mezzo, bordati di basse ringhiere. Da qualsiasi esagono si vedono i piani superiori e inferiori, interminabilmente. La distribuzione degli oggetti nelle gallerie è invariabile.

di _Jorge Luis Borges_ (1941)

L'universo (che altri chiama la Biblioteca) si compone d'un numero indefinito, e
forse infinito, di gallerie esagonali, con vasti pozzi di ventilazione nel
mezzo, bordati di basse ringhiere. Da qualsiasi esagono si vedono i piani
superiori e inferiori, interminabilmente. La distribuzione degli oggetti nelle
gallerie è invariabile. Venticinque vasti scaffali, in ragione di cinque per
lato, coprono tutti i lati meno uno; la loro altezza, che è quella stessa di
ciascun piano, non supera di molto quella d'una biblioteca normale. Il lato
libero dà su un angusto corridoio che porta a un'altra galleria, identica alla
prima e a tutte. A destra e a sinistra del corridoio vi sono due gabinetti
minuscoli. Uno permette di dormire in piedi; l'altro di soddisfare le necessità
fecali. Di qui passa la scala spirale, che s'inabissa e s'innalza nel remoto.
Nel corridoio è uno specchio, che fedelmente duplica le apparenze. Gli uomini
sogliono inferire da questo specchio che la Biblioteca non è infinita (se
realmente fosse tale, perché questa duplicazione illusoria?) io preferisco
sognare che queste superfici argentate figurino e promettano l'infinito... La
luce procede da frutti sferici che hanno il nome di lampade. Ve ne sono due per
esagono, su una traversa. La luce che emettono è insufficiente, incessante.

Come tutti gli uomini della Biblioteca, in gioventù io ho viaggiato; ho
peregrinato in cerca di un libro, forse del catalogo dei cataloghi; ora che i
miei occhi quasi non possono decifrare ciò che scrivo, mi preparo a morire a
poche leghe dall'esagono in cui nacqui. Morto, non mancheranno mani pietose che
mi gettino fuori della ringhiera; mia sepoltura sarà l'aria insondabile; il mio
corpo affonderà lungamente e si corromperà e si dissolverà nel vento generato
dalla caduta, che è infinita. Io affermo che la Biblioteca è interminabile. Gli
idealisti argomentano che le sale esagonali sono una forma necessaria dello
spazio assoluto o, per lo meno, della nostra intuizione dello spazio. Ragionano
che è inconcepibile una sala triangolare o pentagonale. (I mistici pretendono di
avere, nell'estasi, la rivelazione d’una camera circolare con un gran libro
circolare dalla costola continua, che fa il giro completo delle pareti; ma la
loro testimonianza è sospetta; le loro parole, oscure. Questo libro ciclico è
Dio.) Mi basti, per ora, "ripetere la sentenza classica: «La Biblioteca è una
sfera il cui centro esatto è qualsiasi esagono, e la cui circonferenza è
inaccessibile».

<!--more-->
A ciascuna parete di ciascun esagono corrispondono cinque scaffali; ciascuno
scaffale contiene trentadue libri di formato uniforme; ciascun libro è di
quattrocentodieci pagine; ciascuna pagina, di quaranta righe; ciascuna una riga,
di quaranta lettere di colore nero. Vi sono anche delle lettere sulla costola di
ciascun libro; non, però, che indichino o prefigurino ciò che diranno le pagine.
So che questa incoerenza, un tempo, parve misteriosa. Prima d'accennare alla
soluzione (la cui scoperta, a prescindere dalle sue tragiche proiezioni, è forse
il fatto capitale della storia) voglio rammentare alcuni assiomi.

Primo: La Biblioteca esiste ab aeterno. Di questa verità, il cui corollario
immediato è l'eternità futura del mondo, nessuna mente ragionevole può dubitare.
L'uomo, questo imperfetto bibliotecario, può essere opera caso o di demiurghi
malevoli; l'universo, con la sua elegante dotazione di scaffali, di tomi
enigmatici, di infaticabili scale per il viaggiatore e di latrine per il
bibliotecario seduto, non può essere che l'opera di un dio. Per avvertire la
distanza che c'è tra il divino e l’umano, basta paragonare questi rozzi, tremuli
simboli che La mia fallibile mano sgorbia sulla copertina d’un libro, con le
lettere organiche dell'interno: puntuali, delicate, nerissime, inimitabilmente
simmetriche.

Secondo: Il numero dei simboli ortografici è di venticinque (1). Questa
constatazione permise, or sono tre secoli, di formulare una teoria generale
della Biblioteca e di risolvere soddisfacentemente il problema che nessuna
congettura aveva permesso di decifrare: la natura informe e caotica di quasi
tutti i libri. Uno di questi, che mio padre vide in un esagono del circuito
quindici novantaquattro, constava delle lettere M C V, perversamente ripetute
dalla prima all'ultima riga. Un altro (molto consultato in questa zona) è un
mero labirinto di lettere, ma l’ultima pagina dice Oh tempo le tue piramidi. E
ormai risaputo: per una riga ragionevole, per una notizia corretta, vi sono
leghe di insensate cacofonie, di farragini verbali e di incoerenze. (So d’una
regione barbarica i cui bibliotecari ripudiano la superstiziosa e vana abitudine
di cercare un senso nei libri, e la paragonano a quella di cercare un senso nei
sogni o nelle linee caotiche della mano... Ammettono che gli inventori della
scrittura imitarono i venticinque simboli naturali, ma sostengono che questa
applicazione è casuale, e che i libri non significano nulla di per sé.  Questa
affermazione, lo vedremo, non è del tutto erronea.)

Per molto tempo si credette che questi libri impenetrabili corrispondessero a
lingue preterite o remote. Ora, è vero che gli uomini più antichi, i primi
bibliotecari, parlavano una lingua molto diversa da quella che noi parliamo
oggi; è vero che poche miglia a destra la lingua è già dialettale, e novanta
piani più sopra è incomprensibile. Tutto questo, lo ripeto, è vero, ma
quattrocentodieci pagine di inalterabili M C V non possono corrispondere ad
alcun idioma, per dialettale o rudimentale che sia. Alcuni insinuarono che ogni
lettera poteva influire sulla seguente, e che il valore di M C V nella terza
riga della pagina 71 non era lo stesso di quello che la medesima serie poteva
avere in altra riga di altra pagina; ma questa vaga tesi non prosperò. Altri
pensarono a una crittografia; quest'ipotesi h stata universalmente accettata, ma
non nel senso in cui la formularono i suoi inventori.

Cinquecento anni fa, il capo d’un esagono superiore (2) trovò un libro tanto
confuso come gli altri, ma in cui v’erano quasi due pagine di scrittura
omogenea, verosimilmente leggibile. Mostrò la sua scoperta a un decifratore
ambulante, e questo gli disse che erano scritte in portoghese; altri gli dissero
che erano scritte in yiddish. Poté infine stabilirsi, dopo ricerche che durarono
quasi un secolo, che si trattava d’un dialetto samoiedo-lituano del guaranì, con
inflessioni di arabo classico. Si decifrò anche il contenuto: nozioni di analisi
combinatoria, illustrate con esempi di permutazioni a ripetizione illimitata.
Questi esempi permisero a un bibliotecario di genio di scoprire la legge
fondamentale della Biblioteca. Questo pensatore osservò che tutti i libri, per
diversi che fossero, constavano di elementi eguali: lo spazio, il punto, la
virgola, le ventidue lettere dell'alfabeto. Stabilì, inoltre, un fatto che tutti
i viaggiatori hanno confermato: non vi sono, nella vasta Biblioteca, due soli
libri identici. Da queste premesse incontrovertibili dedusse che Ia Biblioteca è
totale, e che i suoi scaffali registrano tutte le possibili combinazioni del
venticinque simboli ortografici (numero, anche se vastissimo, non infinito) cioè
tutto cioè ch'è dato di esprimere, in tutte le lingue. Tutto: la storia
minuziosa dell'avvenire, le autobiografie degli arcangeli, il catalogo fedele
della Biblioteca, migliaia e migliaia di cataloghi falsi, la dimostrazione della
falsità di questi cataloghi, la dimostrazione della falsità del catalogo
autentico, l'evangelo gnostico di Basilide, il commento di questo evangelo, il
commento del commento di questo evangelo, il resoconto veridico della tua morte,
Ia traduzione di ogni libro in tutte le lingue, le interpolazioni di ogni libro
in tutti i libri.

Quando si proclamò che la Biblioteca comprendeva tutti i libri, la prima
impressione fu di straordinaria felicità. Tutti gli uomini si sentirono padroni
di un tesoro intatto e segreto. Non v’era problema personale o mondiale la cui
eloquente soluzione non esistesse: in un qualche esagono. L’universo era
giustificato, l’universo attingeva bruscamente le dimensioni illimitate della
speranza. A quel tempo si parlò molto delle Vendicazioni: libri di apologia e di
profezia che giustificavano per sempre gli atti di ciascun uomo dell'universo e
serbavano arcani prodigiosi per il sue futuro. Migliaia di ambiziosi
abbandonarono il dolce esagono natale e si lanciarono su per le scale, spinti
dal vano proposito di trovare la propria Vendicazione. Questi pellegrini
s’accapigliavano negli stretti corridoi, proferivano oscure minacce, si
strangolavano per le scale divine, scagliavano I libri ingannevoli nei pozzi
senza fondo, vi morivano essi Stessi, precipitativi dagli uomini di regioni
remote. Molti impazzirono... Le Vendicazioni esistono (io ne ho viste due, che
si riferiscono a persone da venire, e forse non immaginarie), ma quei
ricercatori dimenticavano che la possibilità che un uomo trovi la sua, o qualche
perfida variante della sua, è sostanzialmente zero.

Anche si sperò, a quel tempo, nella spiegazione dei misteri fondamentali
dell'umanità: l’origine della Biblioteca e del tempo. E’ verosimile che di
questi gravi misteri possa darsi una spiegazione in parole: se il linguaggio del
filosofi non basta, la multiforme Biblioteca avrà prodotto essa stessa
l’inaudito idioma necessario, e i vocabolari e la grammatica di questa lingua.
Già da quattro secoli gli uomini affaticano gli esagoni.. Vi sono cercatori
ufficiali, inquisitori. Li ho visti nell'esercizio della loro funzione:
arrivano sempre scoraggiati parlano di scale senza un gradino, dove per poco non
s’ammazzarono; parlano di scale e di gallerie con il bibliotecario; ogni tanto,
prendono il libro più vicino e lo sfogliano, in cerca di parole infami. Nessuno,
visibilmente, s’aspetta di trovare nulla.

Alla speranza smodata, com'è naturale, successe una eccessiva depressione. La
certezza che un qualche scaffale d’un qualche esagono celava libri preziosi e
che questi libri preziosi erano inaccessibili, parve quasi intollerabile. Una
setta blasfema suggerì che s’interrompessero le ricerche e che tutti gli uomini
si dessero a mescolare lettere e simboli, fine a costruire, per un improbabile
dono del caso, questi libri canonici. Le autorità si videro obbligate, a
promulgare ordinanze severe. La setta sparì, ma nella mia fanciullezza ho visto
vecchi uomini che lungamente s’occultavano nelle latrine, con dischetti di
metallo in un bossolo proibito, e debolmente rimediavano al divino disordine.

Altri, per contro, credettero che l’importante fosse di sbarazzarsi delle opere
inutili. Invadevano gli esagoni, esibivano credenziali non sempre false,
sfogliavane stizzosamente un volume e condannavano scaffali interi: al loro
furore igienico, ascetico, si deve l’insensata distruzione di milioni di libri.
Il loro nome è esecrato, ma chi si dispera per i "tesori" che la frenesia di
coloro distrusse, trascura due fatti evidenti. Primo: la Biblioteca è cosi
enorme che ogni riduzione d’origine umana risulta infinitesima. Secondo: ogni
esemplare è unico, insostituibile, ma (poiché Ia Biblioteca è totale) restano
sempre varie centinaia di migliaia di facsimili imperfetti, cioè di opere che
non differiscono che per una lettera o per una virgola. Contrariamente
all'opinione generale, credo dunque che le conseguenze delle depredazioni
commesse dai Purificatori siano state esagerate a causa dell'orrore che quei
fanatici ispirarono. Li sospingeva l’idea delirante di conquistare i libri
dell'Esagono Cremisi: libri di formato minore dei normali; onnipotenti,
illustrati e magici.

Sappiamo anche d’un’altra superstizione di quel tempo: quella dell'Uomo del
Libro. In un certo scaffale d’un certo esagono (ragionarono gli uomini) deve
esistere un libro che sia la chiave e il compendio perfetto di tutti gli altri:
un bibliotecario l’ha letto, ed è simile a un dio. Nel linguaggio di questa zona
si conservano alcune tracce del culto di quel funzionario remoto. Molti
peregrinarono in cerca di Lui, si spinsero invano nelle più lontane gallerie.
Come localizzare il venerando esagono segreto che l’ospitava? Qualcuno propose
un metodo regressivo: per localizzare il libro A, consultare previamente il
libro B; per localizzare il libro B, consultare previamente il libro C; e cosi
all'infinito… In avventure come queste ho prodigato e consumato i miei anni.

Non mi sembra inverosimile che in un certo scaffale dell'universo esista un
libro totale (3); prego gli del ignoti che un uomo — uno solo, e sia pure da
migliaia d’anni! — l’abbia trovato e l’abbia letto. Se l’onore e la sapienza e
la felicità non sono per me, che siano per altri. Che il cielo esista, anche se
il mio posto è all'inferno. Ch'io sia oltraggiato e annientato, ma che per un
istante, in un essere, la Tua enorme Biblioteca si giustifichi.

Affermano gli empi che il nonsenso è normale nella Biblioteca, e che il
ragionevole (come anche l’umile e semplice coerenza) è una quasi una miracolosa
eccezione. Parlano (lo so) della "Biblioteca febbrile, i cui casuali volumi
corrono il rischio incessante di mutarsi in altri, e tutto affermano, negano e
confondono come una divinità in delirio". Queste parole, che non solo denunciano
il disordine, ma lo illustrano, testimoniano generalmente del pessimo gusto e
della disperata ignoranza di chi le pronuncia. In realtà, la Biblioteca include
tutte le strutture verbali, tutte le variazioni permesse dai venticinque simboli
ortografici, ma non un solo nonsenso assoluto. Inutile osservarmi che il miglior
volume dei molti esagoni che amministro s’intitola Tuono pettinato, un altro Il
crampo di gesso e un altro Axaxaxas mlö. Queste proposizioni, a prima vista
incoerenti, sono indubbiamente suscettibili d’una giustificazione crittografica
o allegorica; questa giustificazione e verbale, e però, ex hypothesi, già figura
nella Biblioteca. Non posso immaginare alcuna combinazione di caratteri

->dhcmrlchtj<-

che la divina Biblioteca non abbia previsto, e che in alcuna delle sue lingue
segrete non racchiuda un terribile significato. Nessuno può articolare una
sillaba che non sia piena di tenerezze e di terrori; che non sia, in alcuno di
quei linguaggi, il nome poderoso di un dio. Parlare è incorrere in tautologie.
Questa epistola inutile e verbosa già esiste in uno del trenta volumi del cinque
scaffali di uno degli innumerabili esagoni e cosi pure la sua confutazione. (Un
numero n di lingue possibili usa lo stesso vocabolario; in alcune, il simbolo
biblioteca ammette la definizione corretta di sistema duraturo e ubiquitario di
gallerie esagonali, ma biblioteca sta qui per pane, o per piramide, o per
qualsiasi altra cosa, e per altre cose stanno le sette parole che la
definiscono. Tu, che mi leggi, sei sicuro d’intendere la mia lingua?)

Lo scrivere metodico mi distrae dalla presente condizione degli uomini, cui la
certezza di ciò, che tutto sta scritto, annienta o istupidisce. So di distretti
in cui i giovani si prosternano dinanzi ai libri e ne baciano con barbarie le
pagine, ma non sanno decifrare non sola lettera. Le epidemie, le discordie
eretiche, le peregrinazioni che inevitabilmente degenerano in banditismo, hanno
decimato la popolazione. Credo di aver già accennato ai suicidi, ogni anno più
frequenti. M’inganneranno, forse, la vecchiezza e il timore, ma sospetto che la
specie umana — l’unica — stia per estinguersi, e che ha Biblioteca perdurerà:
illuminata, solitaria, infinita, perfettamente immobile, armata di volumi
preziosi, inutile, incorruttibile, segreta.  Aggiungo: infinita. Non introduco
quest'aggettivo per un’abitudine retorica; dico che non è illogico pensare che
il mondo sia infinito. Chi lo giudica limitato, suppone che in qualche luogo
remoto i corridoi e le scale e gli esagoni possano inconcepibilmente cessare;
ciò che è assurdo. Chi lo immagina senza limiti, dimentica che e limitato il
numero possibile dei libri. Io m’arrischio a insinuare questa soluzione: La
Biblioteca è illimitata e periodica. Se un eterno viaggiatore la traversasse in
una direzione qualsiasi, constaterebbe alla fine dei secoli che gli stessi
volumi si ripetono nello stesso disordine (che, ripetuto, sarebbe un ordine:
l’Ordine). Questa elegante speranza rallegra la mia solitudine (4).

1941, Mar del Plata

1. Il manoscritto originale non contiene cifre né maiuscole. La punteggiatura è
   limitata alla virgola e al punto. Questi due segni, lo spazio, e le ventidue
   lettere dell'alfabeto, sono i venticinque simboli sufficienti che enumera lo
   sconosciuto. [Nota dell'editore.

2. Prima, per ogni tre esagoni c’era un uomo. Il suicidio e le malattie
   polmonari hanno distrutto questa proporzione. Fatto indicibilmente
   malinconico: a volte ho viaggiano molte notti per corridoi e scale pulite
   senza trovare un solo bibliotecario.

3. Ripeto: perché un libro esista, basta che sia, possibile. Solo l’impossibile
   è escluso. Per esempio: nessun libro è anche una scala, sebbene esistano
   sicuramente dei libri che discutono, che negano, che dimostrano questa
   possibilità, e altri la cui struttura corrisponde a quella d’una scala.

4. Letizia Alvarez de Toledo ha osservato che la vasta Biblioteca e inutile; a
   rigore, basterebbe un solo volume, di formato comune stampato in corpo nove
   o in corpo dieci, e composto d’un numero infinito di fogli infinitamente
   sottili.  (Cavalieri, al principio del secolo xvii, affermò che ogni corpo
   solido è la sovrapposizione d’un numero infinite di piani.) Il maneggio di
   questo serico vademecum non sarebbe comodo: ogni foglio apparente si
   sdoppierebbe in altri simili; L’inconcepibile foglio centrale non avrebbe
   rovescio.

Racconto contenuto nel volume ["Finzioni"][1].

[1]:https://it.wikipedia.org/wiki/Finzioni
