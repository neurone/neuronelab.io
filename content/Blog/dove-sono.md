title: Dove sono?
date: 2013-10-29 20:52
tags: filosofia, filosofia della mente, scienza, fantascienza
summary: *di Daniel C. Dennett*, dalla raccolta "L'io della mente"

*di Daniel C. Dennett*, dalla raccolta "L'io della mente"

Ora che ho vinto la mia causa in base alla Legge per la Libertà d’Informazione,
sono libero di rivelare per la prima volta un curioso episodio della mia vita
che può presentare un certo interesse non solo per chi studia la filosofia
della mente, l’intelligenza artificiale e la neurobiologia, ma anche per un
pubblico più vasto.  
Parecchi anni fa venni avvicinato da alcuni funzionari del Pentagono, che mi
chiesero di offrirmi volontario per una missione segreta e pericolosissima. In
collaborazione con la NASA e con Howard Huges, il Dipartimento della Difesa
stava spendendo miliardi per mettere a punto un Dispositivo Scavatore
Sotterraneo Supersonico, o DSSS. Esso avrebbe dovuto scavare a grandissima
velocità una galleria attraverso il nucleo della Terra e infilare una testata
atomica all’uopo progettata “su per i silos dei missili, a quei Rossi”, come si
espresse uno dei papaveri del Pentagono.
<!--more-->

Il problema era che durante una delle prime prove erano riusciti a cacciare una
testata alla profondità di un chilometro e mezzo sotto Tulsa, in Oklahoma, e
volevano che io gliela recuperassi. “Perché proprio io?” domandai. Be’, la
missione contemplava certe applicazioni rivoluzionarie delle ricerche correnti
sul cervello, e loro avevano sentito parlare del mio interesse per il cervello,
e naturalmente della mia curiosità faustiana, del mio grande ardimento e così
via… Insomma, come avrei potuto rifiutare? La difficoltà che aveva spinto il
Pentagono a interpellarmi era che il dispositivo che mi chiedevano di
recuperare sprigionava una forte radioattività, ma di genere nuovo. Secondo gli
strumenti di controllo, qualcosa nella natura del dispositivo, in seguito alle
complesse interazioni con le sacche di minerale incontrate a grande profondità,
aveva prodotto una radiazione che poteva causare gravi anomalie in certi
tessuti del cervello. Non si era trovato alcun mezzo per schermare il cervello
da questi raggi mortali, che a quanto pareva erano invece innocui per gli altri
organi e tessuti del corpo. Era stato perciò deciso che la persona inviata a
recuperare il dispositivo *avrebbe lasciato a casa il proprio cervello.* Esso
sarebbe stato custodito in un luogo sicuro, da dove avrebbe potuto esplicare le
proprie normali funzioni di controllo grazie a complessi collegamenti via
radio. Ero disposto a subire un’operazione chirurgica che mi avrebbe asportato
tutto il cervello, il quale sarebbe poi stato mantenuto in vita in un apposito
recipiente presso il Centro per il volo spaziale umano di Houston? Ciascuna via
d’ingresso e di uscita, appena recisa, sarebbe stata rimessa in funzione
mediante una coppia di ricetrasmettitori microminiaturizzati, uno collegato
appunto al cervello e l’altro alle radici nervose del cranio svuotato. Non ci
sarebbe stata alcuna perdita d’informazione, tutto il complesso delle
connessioni sarebbe stato perfettamente conservato. All’inizio ero un po’
riluttante: avrebbe funzionato davvero? I neurochirurgi di Houston
m’incoraggiavano: “Lo consideri” dicevano “come un semplice *allungamento* dei
nervi. Se il suo cervello fosse spostato di appena due o tre *centimetri*
all’interno del cranio, ciò non potrebbe alterare o menomare la sua mente. Noi
non faremo altro che rendere i nervi indefinitamente elastici, collegandoli via
radio”.

Mi portarono a fare un giro nel laboratorio di Houston specializzato nel
mantenimento in vita di organi e tessuti e mi mostrarono la vasca nuova di
zecca in cui sarebbe stato collocato il mio cervello, se avessi accettato.
incontrai la folta squadra di assistenza, composta da brillanti neurologi,
ematologi, biofisici e ingegneri elettronici e, dopo parecchi giorni di
discussioni e dimostrazioni, accettai di correre il rischio. Fui sottoposto a
una quantità di esami del sangue, di analisi del cervello, di esperimenti,
colloqui e via dicendo. Registrarono nei minimi particolari la mia biografia,
fecero noiosissimi elenchi delle mie convinzioni, speranze, paure e gusti.
Fecero addirittura un elenco dei miei dischi stereo preferiti e mi sottoposero
ad un trattamento psicoanalitico accelerato.

Giunse alla fin il giorno dell’operazione; naturalmente mi fecero l’anestesia e
dell’operazione in sé non ricordo nulla. Quando ripresi i sensi, aprii gli
occhi, mi guardai intorno e feci l’inevitabile, la tradizionale, banalissima
domanda postoperatoria: “Dove sono?”. L’infermiera mi sorrise e disse: “A
Houston”, e io pensai che, in un modo o nell’altro, ciò aveva ancora una buona
probabilità di essere vero. Mi porse uno specchio. E difatti, ecco le minuscole
antenne che si rizzavano dalle loro basi di titanio saldate al mio cranio.  
“Immagino che l’operazione sia riuscita” dissi. “Voglio andare a vedere il mio
cervello”. Mi condussero (ero un po’ frastornato e incerto sulle gambe) per un
lungo corridoio e mi fecero entrare nel laboratorio di mantenimento in vita. La
squadra di assistenza al completo mi accolse con un’ovazione e io risposi con
quello che speravo fosse un gaio saluto. Poiché avevo ancora il capogiro, mi
aiutarono a raggiungere il recipiente. Guardai attraverso il vetro: dentro,
sospeso in un liquido che pareva birra, c’era senz’ombra di dubbio un cervello
umano, benché quasi tutti coperto di piastrine con circuiti stampati, tubicini
di plastica, elettrodi e altri ammennicoli. “È il mio?” chiesi. “Giri
l’interruttore del trasmettitore in uscita, lì sul fianco della vasca, e se ne
accorgerà” rispose il direttore del progetto. Misi l’interruttore sulla
posizione di spento e di colpo, intontito e in preda alla nausea, mi accasciai
fra le braccia dei tecnici, uno dei quali rimise gentilmente l’interruttore
sulla posizione di acceso. Mentre andavo recuperando l’equilibrio e la
padronanza di me stesso, così riflettevo: “Be’, eccomi qui, seduto su uno
sgabello, a contemplare il mio cervello attraverso una lastra di vetro… Un
momento, ” mi dissi “non avrei forse dovuto pensare ‘Eccomi qui, sospeso in un
liquido pieno di bollicine, contemplato dai miei occhi?’”. Cercai di pensare
quest’ultimo pensiero. Cercai di proiettarlo nella vasca, offrendolo speranzoso
al mio cervello, ma non riuscii a eseguire l’esercizio con convinzione. Tentai
di nuovo: “Eccomi qui, io, Daniel Dennett, sospeso in un liquido pieno di
bollicine, contemplato dai miei occhi”. No, non ci riuscivo. Davvero
sconcertante e imbarazzante. Essendo un filosofo di salda fede fisicalista,
credevo fermamente che i miei pensieri trovassero un supporto in un qualche
luogo del mio cervello: eppure, quando pensavo “Eccomi qui”, il luogo in cui
questo pensiero si presentava era qui, fuori dalla vasca, dove io, Dennett, me
ne stavo a contemplare il mio cervello.

Tentai e ritentai di pensarmi dentro la vasca, ma senza alcun risultato. Cercai
di allenarmi all’impresa facendo degli esercizi mentali. Pensai trame per
cinque volte in rapida successione: “Il sole brilla *laggiù*” e ogni volta mi
rappresentai nella mente un posto diverso; nell’ordine: l’angolo del
laboratorio illuminato dal sole, il prato che vedevo di fronte all’ospedale,
Houston, Marte e Giove. Scoprii di non avere nessuna difficoltà a far
rimbalzare i miei “laggiù” per tutta la mappa celeste con i punti di
riferimento giusti. In un istante riuscivo a scagliare un “laggiù” attraverso
le distese più lontane dello spazio e poi a collocare il “laggiù” successivo,
con la precisione di una punta di spillo, nel quadrante superiore sinistro di
una lentiggine del mio braccio. Perché avevo tanta difficoltà con il “qui”?
“Qui a Houston” funzionava abbastanza bene, e così pure “qui nel laboratorio” e
anche “qui in questa zona del laboratorio”; ma “qui nella vasca” appariva
sempre come un vuoto boccheggiamento mentale involontario. Provai a chiudere
gli occhi mentre lo pensavo. Ci fu, mi parve, un certo miglioramento, ma non il
pieno successo, se non forse per un istante fuggevole. Non ne ero sicuro. Anche
la scoperta che non riuscivo a esserne sicuro era sconvolgente. Come facevo a
sapere dove intendevo per “qui” quando pensavo “qui”? Che *credessi* di
intendere un posto quando in realtà ne intendevo un altro? Non capivo come si
potesse ammettere ciò senza sciogliere i pochi legami d’intimità fra una
persona e la sua vita mentale che erano sopravvissuti al furibondo attacco
degli scienziati e dei filosofi del cervello, dei fisicalisti e dei
comportamentisti. Forse ero incorreggibilmente in errore in ciò che *intendevo*
quando dicevo “qui”. Tuttavia, nelle circostanze in cui mi trovavo, pareva
proprio o che io fossi condannato alla pura e semplice forza dell’abitudine
mentale a formulare pensieri indexicali sistematicamente falsi, oppure che il
luogo in cui una persona si trova (e quindi il luogo in cui i suoi pensieri
trovano un supporto ai fini di un’analisi semantica) non sia necessariamente il
luogo in cui è il suo cervello, la sede fisica della sua anima. Irritato da
questa confusione, cercai di orientarmi ricorrendo a uno degli stratagemmi
prediletti dei filosofi: cominciai ad assegnare un nome alle cose.

“Yorick,” dissi ad alta voce al mio cervello “tu sei il mio cervello. Al resto
del mio corpo, seduto su questo sgabello, darò il nome di ‘Amleto’”. Eccoci
dunque tutti qui: Yorick è il mio cervello, Amleto è il mio corpo e io sono
Dennett. *Allora*, dove sono io? E quando penso “Dove sono io?”, dove trova
supporto questo pensiero? Lo trova nel mio cervello, a mollo lì nella vasca,
oppure qui tra le mie orecchie dove *sembra* trovare un supporto? O da nessuna
parte? Le sue coordinate *temporali* non mi provocano alcuna difficoltà; ma non
deve possedere anche delle coordinate spaziali? Cominciai a fare un elenco
delle varie possibilità.

- *Dove va Amleto va anche Dennett*. Mi fu agevole respingere questo principio
  ricorrendo agli esperimenti ideali di trapianto cerebrale tanto cari ai
  filosofi. Se Tizio e Caio si scambiano i cervelli, Tizio è quello che ha il
  corpo che prima era di Caio; chiedendoglielo: sosterrà di essere Tizio e vi
  rivelerà i particolari più intimi della vita di Tizio. Era chiaro, dunque, che
  il mio corpo attuale e io potevamo separarci ma non era verosimile che io
  potessi essere separato dal mio cervello. La regola empirica che emergeva
  chiaramente dagli esperimenti ideali era che, in un’operazione di trapianto
  cerebrale, si vuole essere il *donatore*, non il beneficiario. In realtà è più
  giusto chiamare questa operazione un trapianto di *corpo*. Quindi, forse, la
  verità è che:

- *Dove va Yorick va anche Dennett*. Questo tuttavia non mi andava molto. Come
  facevo a essere nella vasca e non invece pronto ad andarmene in giro, quando
  era così evidente che mi trovavo fuori della vasca, guardavo dentro di essa e
  cominciavo addirittura a progettare tra me e me una ritirata nella mia stanza
  per fare un sostanzioso spuntino? Era una petizione di principio, me ne rendevo
  conto, eppure sembrava che portasse a qualcosa di importante. Mentre cercavo un
  qualche sostegno per la mia intuizione, mi venne in mente un argomento di
  natura giuridica che sarebbe forse piaciuto a Locke.

Supponiamo, ragionai tra me, che io ora andassi in California, rapinassi una
banca e fossi catturato. In quale Stato sarei processato: in California, dov’è
avvenuta la rapina, o nel Texas, dove si trovava il cervello della banda? Sarei
un criminale californiano con un cervello fuori dello Stato o un criminale
texano che controlla da lontano un complice, se così si può dire, che agisce in
California? Probabilmente potrei evitare la condanna proprio per
l’indecidibilità di questo conflitto di competenze territoriali, oppure lo
giudicherebbero un crimine interstatale e quindi federale. Supponiamo comunque
che io venissi condannato. È plausibile che la California si contenterebbe di
gettare in galera Amleto, sapendo che Yorick se la passa e fa la cura delle
acque nel Texas? Oppure il Texas metterebbe dentro Yorick, lasciando Amleto
libero di prendere la prima nave per Rio? Quest’alternativa, mi sembrò
interessante. Escludendo la pena capitale o altre punizioni insolite e crudeli,
lo Stato sarebbe obbligato a conservare il dispositivo di mantenimento in vita
di Yorick, magari trasferendolo a Houston al penitenziario di Leavenworth, e
io, a parte l’inconveniente del disonore, non troverei la cosa affatto seccante
e in quelle circostanze mi considererei un uomo libero. Se lo stato ha
interesse a rinchiudere una persona in un istituto di pena, rinchiudendovi
Yorick non rinchiuderebbe certo *me*. Se tutto ciò fosse vero, ne deriverebbe
una terza alternativa.

- *Dennett si trova ovunque pensi di trovarsi*. Se la si generalizza, questa
  asserzione diventa: In qualsiasi istante una persona ha un *punto di vista* e
  l’ubicazione del punto di vista (che è determinato interamente dal contenuto
  del punto di vista) è anche l’ubicazione della persona.

Questa proposizione può anche suscitare qualche perplessità; a me tuttavia
sembrò un passo nella direzione giusta. L’unico problema era che pareva
mettermi, riguardo all’ubicazione, in una improbabile situazione di
infallibilità, come dire: testa, vinco io/croce, perdi tu. Non mi era successo
molte volte di sbagliare credendo di essere in un dato posto, e altrettante
volte di non esserne sicuro? Non è forse possibile perdersi? Certo, ma perdersi
*geograficamente* non è l’unico modo di perdersi. Se ci si perde in un bosco, si
può almeno cercare di rassicurarsi con il consolante pensiero che se non altro
si sa dove si è: proprio lì, nei dintorni familiari del proprio corpo. Forse in
questo caso la scoperta di tale certezza non sarebbe di grandissimo aiuto, ma
si possono immaginare situazioni anche peggiori; e io non ero certo che la mia
in quel momento non fosse una di esse.

Era chiaro che il punto di vista aveva qualcosa a che fare con l’ubicazione della
persona, ma in sé era una nozione poco chiara. È evidente che il contenuto del
punto di vista di un individuo non coincide col contenuto delle sue convinzioni
o dei suoi pensieri né è da esso determinato. Per esempio, che dovremmo dire del
punto di vista di uno spettatore di un film in cinerama che grida e si contorce
sulla poltrona allorché il documentario sulle montagne russe travolge il suo
senso psichico della distanza? Ha forse dimenticato di essere la cinema, seduto
in luogo sicuro? In questo caso ero incline a dire che costui sta sperimentando
un cambiamento illusorio del proprio punto di vista. In altri casi, la mia
propensione a definire illusori questi cambiamenti era molto meno forte. I
tecnici dei laboratori o degli impianti che manipolano materiali pericolosi
mediante braccia e mani meccaniche controllate con dispositivi a retroazione,
subiscono un cambiamento del punto di vista che è più deciso e pronunciato di
qualunque effetto provocato dal cinerama. Con le loro dita metalliche possono
sentire il peso e la scivolosità dei contenitori che manipolano. Essi sanno
benissimo dove si trovano e non si lasciano indurre in false convinzioni da
quell’esperienza; eppure è come se fossero dentro la camera ermetica che
guardano. Con uno sforzo mentale essi riescono a spostare avanti e indietro il
loro punto di vista, un po’ come quando si guarda un cubo trasparente di Neckar
o un disegno di Escher e si riesce a vederlo orientato ora in un modo ora
nell’altro. Pare proprio assurdo supporre che nell’eseguire questo esercizio di
ginnastica mentale essi trasportino avanti e *indietro se stessi*.

Comunque questo esempio mi diede qualche speranza. Se io ero davvero nella vasca
a dispetto delle mie intuizioni, sarei forse riuscito, allenandomi, a rendermi
quel punto di vista addirittura abituale. Dovevo soffermarmi su immagini di me
stesso comodamente immerso nella vasca, occupato a inviare ordini a quel corpo
familiare *là fuori*. Riflettei che la facilità o la difficoltà di
quest’operazione erano presumibilmente indipendenti dalla vera ubicazione del
cervello. Se mi fossi esercitato prima dell’operazione, forse ora questa sarebbe
stata per me una seconda natura. Voi stessi potete tentare un simile
*trompe-œil*: supponete di avere scritto una lettera sediziosa che è stata
pubblicata sul “New York Times” e che di conseguenza il governo abbia deciso di
rinchiudere il vostro cervello per un periodo di osservazione di tre anni nella
Clinica dei Cervelli Pericolosi a Bethesda, nel Maryland. Naturalmente il vostro
corpo è lasciato libero di guadagnarsi uno stipendio e continuare così la sua
funzione di accumulare reddito imponibile. In questo momento, tuttavia, il
vostro corpo è seduto in una sala ed ascolta lo stranissimo resoconto che Daniel
Dennett fa di una sua esperienza analoga. Provate. Pensatevi a Bethesda e poi
ripensate con nostalgia al vostro corpo così lontano, che pure sembra così
vicino. È solo con una repressione a grande distanza (vostra? del governo?) che
riuscite a controllare l’impulso di battere quelle mani in un cortese applauso
prima di muovere il vostro vecchio corpo verso il bagno e poi verso un ben
meritato aperitivo al bar. Immaginare ciò è certamente difficile, ma se
raggiungete questo scopo il risultato potrebbe essere consolante.

Fatto sta che io ero a Houston, perso per così dire nei miei pensieri. Ma ciò
non durò a lungo: le mie speculazioni furono interrotte ben presto dai dottori
di Houston i quali, prima di inviarmi nella mia rischiosa missione, desideravano
verificare il mio nuovo sistema nervoso protesico. Come ho detto prima,
all’inizio ero un po’ stordito, comprensibilmente, ma mi abituai subito alle mie
nuove condizioni (che in fin dei conti erano quasi indistinguibili dalle mie
vecchie condizioni). Il mio adattamento però non era perfetto e ancor oggi
continuo a soffrire di lievi difficoltà di coordinamento. La velocità della luce
è elevata ma finita e, via via che il mio corpo e il mio cervello si
allontanavano l’uno dall’altro, la delicata interazione fra i miei sistemi di
retroazione è gettata nello scompiglio dai ritardi temporali. Proprio come di
diventa quasi incapaci di parlare quando si sente la propria voce soggetta a un
ritardo o rimandata dall’eco, così io sono praticamente incapace, per esempio,
di seguire con gli occhi un oggetto in movimento quando il mio cervello è
lontano dal corpo di qualche chilometro. Nella maggior parte dei casi la mia
menomazione è quasi impercettibile, benché io non sia più in grado di colpire
una palla a effetto con la padronanza di un tempo. Naturalmente vi sono certe
compensazioni. I liquori mi piacciono sempre e mi riscaldano la gola
corrodendomi il fegato, ma ora ne posso bere a volontà senza ubriacarmi
minimamente: fatto curioso, che alcuni miei amici più intimi hanno forse notato
(benché, di tanto in tanto io abbia *finto* di essere ubriaco per non attirare
la loro attenzione sulle mie condizioni insolite). Per ragioni analoghe, se mi
slogo un polso prendo l’aspirina per bocca, ma se il dolore continua chiedo che
mi venga somministrata della codeina *in vitro* a Houston. Nei periodi in cui
sono indisposto mi possono arrivare bollette del telefono da far rizzare i
capelli.

Ma torniamo alla mia avventura. Alla fine, i dottori e io eravamo persuasi che
fossi ormai pronto a intraprendere la missione sotterranea. Lasciai quindi il
mio cervello a Houston e mi diressi in elicottero a Tulsa. O per lo meno, questa
fu la mia sensazione, così mi esprimerei senza stare a lambiccarmi il cervello,
per così dire. Durante il viaggio ricominciai a riflettere sulle preoccupazioni
iniziali e capii che le mie prime meditazioni postoperatorie avevano una
sfumatura di panico. La faccenda non era affatto così strana o metafisica come
avevo supposto. Dove mi trovavo? In due luoghi, evidentemente: dentro la vasca e
fuori. Così come uno può stare con un piede nel Connecticut e l’altro nel Rhode
Island, io mi trovavo in due luoghi contemporaneamente. Ero diventato uno di
quegli individui “diffusi” di cui si sentiva tanto parlare. Più meditavo su
questa risposta, più essa acquistava i tratti di un’ovvia verità. Eppure, strano
a dirsi, quanto più essa appariva vera, tanto meno sembrava importante la
domanda dalla quale poteva costituire la vera risposta. Destino triste ma non
nuovo per un problema filosofico. Naturalmente questa risposta non mi
soddisfaceva del tutto. Restava una domanda alla quale mi sarebbe piaciuto
trovare risposta, e non era né “Dove sono tutte le mie parti?” né “Qual è il mio
punto di vista attuale?”. O almeno mi pareva che ci fosse una domanda del
genere; perché sembrava proprio innegabile che in un certo senso io, e non solo
la *maggior parte di me*, stessi per scendere sotto terra a Tulsa in cerca di
una testata atomica.

Quando trovai la testata fui proprio contento di non aver portato il cervello,
poiché l’indice dello speciale contatore di Geiger che avevo con me era andato a
fondo scala. Chiamai Houston con la mia radio normale e riferii al centro di
controllo operativo la mia posizione e i miei progressi. In risposta essi mi
fornirono le istruzioni per smantellare il veicolo in base alle osservazioni
fatte sul posto. Mi ero appena messo al lavoro con cannello ossidrico, quando
all’improvviso accadde una cosa terribile: diventai completamente sordo. Sulle
prime pensai che fosse solo un guasto alla mia cuffia radio, ma poi diedi uno o
die colpi con le nocche sull’elmetto e non sentii nulla. Evidentemente i
ricetrasmettitori audio erano andati a pallino. Non potevo più sentire né
Houston né la mia voce, ma potevo parlare e cominciai quindi a spiegare che
cos’era accaduto. A metà di una frase capii che si era guastato qualcos’altro:
il mio apparato vocale era rimasto paralizzato. Poi la mia mano destra si
afflosciò… un altro ricetrasmettitore partito. mi trovavo davvero in guai
grossi. Ma il peggio doveva ancora venire. Dopo qualche minuto diventai cieco.
Maledissi la mia sorte e poi maledissi gli scienziati che mi avevano cacciato in
quel gravissimo frangente. Ero lì, sordo, muto e cieco, in un buco radioattivo,
a un chilometro e mezzo sotto Tulsa. Poi l’ultimo dei miei collegamenti radio
col mio cervello s’interruppe e all’improvviso mi trovai di fronte a un nuovo
problema, ancora più sconvolgente: mentre un istante prima mi trovavo sepolto
vivo nell’Oklahoma, ora mi trovavo privo di corpo a Houston. Non mi resi conto
immediatamente del mio nuovo stato. Mi ci vollero parecchi angosciosi minuti per
capire che il mio corpo giaceva a parecchie centinaia di chilometri, col cuore
che pulsava e i polmoni che respiravano, ma peraltro morto come il corpo di un
donatore in un trapianto di cuore, e col cranio zeppo di aggeggi elettronici
guasti e inutili. Il cambiamento di prospettiva che prima avevo trovato quasi
impossibile ora mi parve del tutto naturale. Benché riuscissi a ritrasportarmi
col pensiero nel mio corpo dentro la galleria sotto Tulsa, mi dovevo sforzare
alquanto per mantenere l’illusione. Perché era certo un’illusione supporre che
fossi ancora nell’Oklahoma: con quel corpo avevo perduto ogni contatto.

Poi, in una di quelle intuizioni rivelatrici di cui si dovrebbe diffidare, mi
venne in mente che mi ero imbattuto in una straordinaria dimostrazione
dell’immaterialità dell’anima basata su premesse e princìpi fisicalisti.
Infatti, non appena si era estinto l’ultimo segnale radio fra Tulsa e Houston,
io non avevo forse cambiato ubicazione da Tulsa a Houston alla velocità della
luce? E non avevo forse compiuto ciò senz’alcun aumento della massa? Ciò che si
era spostato da A a B a quella velocità ero certamente io, o comunque la mia
anima o la mia mente, il centro privo di massa del mio essere e la dimora della
mia coscienza. Il mio *punto di vista* era rimasto un po’ indietro, ma avevo già
notato che esiste un rapporto indiretto tra il punto di vista e l’ubicazione
della persona. Non riuscivo a vedere come un filosofo fisicalista potesse
trovare da obiettare alcunché, a meno che non imboccasse la strada tremenda e
anti-induttiva dove è bandito ogni discorso sulle persone. E tuttavia la nozione
di personalità era talmente radicata nella visione del mondo di tutti, o almeno
così mi pareva, che la sua negazione sarebbe stata tanto stranamente poco
convincente, tanto sistematicamente in malafede, quanto la negazione cartesiana
“non sum”.

La gioia della scoperta filosofica mi aiutò così a superare alcuni dolorosissimi
minuti, o forse ore, in cui lo sconforto e la disperazione della mia situazione
mi si fecero via via più evidenti. Mi invasero ondate di panico e addirittura di
nausea, rese ancora più orribili dall’assenza della loro fenomenologia normale
collegata al corpo. Né scariche di adrenalina, né formicolio alle braccia, né
batticuore, né salivazione premonitrice. A un certo punto sentii una
spiacevolissima sensazione come di un crampo allo stomaco e per un momento
m’illuse la falsa speranza che stessi subendo il processo inverso di quello che
mi aveva ridotto in quello stato: una graduale riacquisizione del corpo. Ma si
era trattato di una sensazione unica e isolata, e mi convinse ben presto che si
trattava semplicemente dei prodromi di un supplizio che con ogni probabilità mi
stava colpendo: l’allucinazione di avere un corpo fantasma, ben nota a chiunque
abbia subito un’amputazione.

Il mio stato d’animo era dunque assai confuso e contraddittorio. Da una parte
ero esaltato dalla mia scoperta filosofica e mi arrovellavo il cervello (una
delle poche cose familiari che fossi ancora in grado di fare) per trovare il
modo di comunicarla ai giornali; dall’altra ero amareggiato, solo, pieno di
terrore e d’incertezza. Per fortuna tutto ciò non durò a lungo, poiché la
squadra di assistenza tecnica con un sedativo mi procurò un sonno senza sogni,
dal quale riemersi udendo con meravigliosa fedeltà i primi, ben noti accordi del
mio prediletto *Trio per archi e pianoforte* di Brahms. Ecco dunque perché
avevano voluto un elenco dei miei dischi preferiti! Non mi ci volle molto per
capire che sentivo la musica senza orecchi: Ciò che usciva dalla puntina
stereofonica, dopo essere passato attraverso chissà quale raffinato circuito di
raddrizzamento, veniva riversato direttamente nel mio nervo acustico. Stavo
ascoltando Brahms in presa diretta, un’esperienza indimenticabile per tutti i
patiti della stereofonia. Quando il disco finì, non fui sorpreso nell’udire la
voce rassicurante del direttore del progetto che parlava nel microfono che ora
costituiva la mia protesi acustica. Egli confermò l’analisi da me fatta sul
guasto che si era verificato e mi assicurò che si stavano prendendo i
provvedimenti del caso per reincorporarmi. Non fu più specifico e dopo qualche
altro disco cominciai a scivolare nel sonno. Come appresi in seguito, il mio
sonno durò per quasi un anno e quando mi svegliai mi ritrovai del tutto
reintegrato nei miei sensi. Tuttavia, quando mi guardai allo specchio, fui un
po’ stupito nel vedere una faccia che non conoscevo. Barbuta e un po’ più larga,
con un’indubbia rassomiglianza con la mia faccia precedente e la stessa
espressione di vivace intelligenza e risolutezza, ma certamente una faccia
diversa. Altre esplorazioni di natura intima mi tolsero ogni dubbio che si
trattasse di un corpo nuovo, e il direttore del progetto confermò le mie
conclusioni. Non si dilungò sulla storia precedente del mio nuovo corpo e io
(saggiamente, penso ora) decisi di non indagare. Come hanno teorizzato di
recente molto filosofi ignari delle mie traversie, l’acquisizione di un corpo
nuovo lascia intatta la *persona.* E dopo un certo periodo di adattamento a una
nuova voce, a un diverso vigore o debolezza dei muscoli e così via, *la
personalità* è anch’essa grosso modo conservata. Cambiamenti di personalità più
appariscenti sono stati comunemente osservati in persone che abbiano subìto
importanti operazioni di chirurgia plastica, per non parlare dei cambiamenti
chirurgici di sesso, e penso che nessuno voglia negare che in quei casi la
persona sopravvive. Comunque io mi adattai presto al mio nuovo corpo, al punto
di non riuscire a richiamare alla coscienza, o addirittura alla memoria, nessuna
delle novità che esso aveva rappresentato all’inizio. Ciò che vedevo nello
specchio mi diventò presto del tutto familiare. Tra l’altro vedevo ancora le
antenne e perciò non fui sorpreso quando m’informarono che il mio cervello non
era stato tolto dalla sua dimora nel laboratorio per il mantenimento in vita.

Decisi che il mio vecchio amico Yorick meritava una visita. Io e il mio nuovo
corpo, che potremmo anche chiamare Fortebraccio, entrammo nel solito
laboratorio, dove fummo accolti da un’altra ovazione dei tecnici, i quali
ovviamente non si congratulavano con me ma con se stessi. Ancora una volta
sostai davanti alla vasca e contemplai il povero Yorick e mi venne ancora una
volta il capriccio di spegnere il trasmettitore di uscita. Immaginatevi la mia
sorpresa quando non accadde nulla d’insolito. Nessuno svenimento, nessun’ondata
di nausea, nessun mutamento percettibile. un tecnico si affrettò a girare di
nuovo l’interruttore, ma neppure questa volta sentii nulla. Domandai spiegazioni
e il direttore del progetto me le fornì subito. A quanto pareva, ancor prima di
operarmi la prima volta, avevano costruito al calcolatore un duplicato del mio
cervello, che traduceva in un gigantesco programma sia la struttura completa per
l’elaborazione dell’informazione sia la velocità di calcolo del mio cervello.
Dopo l’operazione, ma prima di inviarmi in missione nell’Oklahoma, avevano fatto
funzionare l’uno accanto all’altro questo sistema da calcolatore e Yorick. I
segnali provenienti da Amleto venivano inviati simultaneamente ai
ricetrasmettitori di Yorick e al fascio degli ingressi del calcolatore. E le
uscite di Yorick, non solo venivano ritrasmesse ad Amleto, il mio corpo:
venivano anche registrate e confrontate con le uscite simultanee del programma
che, per ragioni a me oscure, era stato chiamato “Uberto”. Per giorni e
addirittura per settimane le uscite furono identiche e sincrone, il che
naturalmente non *dimostrava* che fossero riusciti a copiare la struttura
funzionale del cervello; tuttavia questa conferma empirica era molto
incoraggiante.

Nei giorni in cui ero rimasto privo di corpo, l’ingresso di Uberto, e quindi
anche la sua attività, erano stati mantenuti paralleli a quelli di Yorick. E
ora, per dimostrarlo, essi avevano commutato l’interruttore principale affidando
così per la prima volta a Uberto il controllo in linea del mio corpo: non di
Amleto, naturalmente, bensì di Fortebraccio. Appresi che Amleto non era stato
più estratto dalla sua tomba sotterranea e si poteva presumere che a quel punto
fosse ormai ritornato polvere. Sulla mia tomba giace ancora la massa imponente
del congegno abbandonato, con la parola DSSS disegnata a grandi lettere sul
fianco (circostanza che potrà ispirare agli archeologi del secolo venturo idee
curiose sui riti di sepoltura dei loro antenati).  
A questo punto i tecnici del laboratorio mi mostrarono l’interruttore
principale, che aveva due posizioni, contrassegnate con una C per cervello (non
sapevano che il mio cervello si chiamava Yorick), e una U per Uberto.
L’interruttore era appunto sulla U e mi spiegarono che, se volevo, potevo
ricommutarlo su C. Col cuore in gola (e col cervello in vasca) eseguii. Non
accadde nulla. Solo un clic. Per verificare la loro asserzione, e con
l’interruttore principale ora su C, feci scattare l’interruttore d’uscita di
Yorick collocato sulla vasca: e difatti cominciai a venir meno. Una volta che
l’interruttore di uscita fu rimesso nella posizione di acceso e che io ebbi
recuperato i sensi, per così dire, continuai a giocare con l’interruttore
principale, facendolo scattare avanti e indietro. Mi convinsi che, a parte il
clic della commutazione, non riuscivo a scoprire la minima differenza. Potevo
commutare a metà di una frase, e la frase che avevo cominciato sotto il
controllo di Yorick veniva completata, senza pausa o stacco alcuno, sotto il
controllo di Uberto. Avevo un cervello di scorta, una sorta di protesi che un
giorno avrebbe potuto essermi di grande aiuto se fosse capitato qualche guaio a
Yorick. Oppure potevo tenere Yorick di riserva e usare Uberto. Sembrava che la
scelta fosse del tutto indifferente, poiché il logorio e l’affaticamento del mio
corpo non avevano alcun effetto debilitante su nessuno dei due cervelli, sia che
stessero, l’uno o l’altro, effettivamente generando i movimenti del mio corpo
sia che stessero buttando la loro uscita al vento.  
L’unico aspetto veramente preoccupante di questo nuovo sviluppo era la
prospettiva, che non tardò molto a farsi luce il me, che qualcuno staccasse il
ricambio – Uberto o Yorick, secondo il caso – da Fortebraccio e lo ricollegasse
a un altro corpo ancora, a un qualche Rosencrantz o Guildenstern dell’ultim’ora.
A quel punto (se non prima) ci sarebbero state *due* persone, questo era chiaro.
Una sarei stata io e l’altra sarebbe stata una sorta di fratello supergemello.
Se ci fossero stati due corpi, uno controllato da Uberto e l’altro da Yorick,
quale sarebbe stato riconosciuto dal mondo come il vero Dennett? E qualunque
fosse stata la decisione del resto del mondo, quale dei due sarebbe stato *me*?
Sarei stato quello col cervello Yorick, in virtù della priorità causale di
Yorick e della sua precedente relazione intima con Amleto, il corpo originale di
Dennett? Questo argomento sembrava un po’ troppo legalitario, sapeva un po’
troppo dell’arbitrarietà della consanguineità e del possesso legale per essere
convincente a livello metafisico. Supponiamo infatti che, prima della comparsa
sulla scena del secondo corpo, avessi tenuto Yorick di riserva per anni e avessi
fatto guidare per tutto il tempo il mio corpo – cioè Fortebraccio – dall’uscita
di Uberto. Allora la coppia Uberto-Fortebraccio avrebbe costituito per
usucapione (tanto per contrapporre un concetto giuridico a un altro) il vero
Dennett e l’erede legittimo di tutto ciò che era appartenuto a Dennett. Era un
problema interessante, certo, ma sicuramente non così pressante come un altro
problema che mi tormentava. Intuivo lucidamente che in tale eventualità io sarei
vissuto fintantoché *una delle due* coppie cervello-corpo fosse rimasta intatta,
ma non ero molto incerto se desiderare o no che vivessero entrambe.  
Discussi le mie preoccupazioni con i tecnici e col direttore del progetto. La
prospettiva di due Dennett mi ripugnava, spiegai, soprattutto per ragioni
sociali: non volevo rivaleggiare con me stesso per l’affetto di mia moglie, né
mi allettava la prospettiva di due Dennett che si spartissero il mio modesto
stipendio di professore. Ancora più sgomentante e disgustosa, tuttavia, era
l’idea di sapere *tante cose* di un’altra persona, che a sua volta la sapeva
altrettanto lunga su di me. Come avremmo mai potuto guardarci negli occhi? I
miei colleghi del laboratorio sostenevano che stavo dimenticando il lati
positivi della faccenda. Non c’erano forse molte cose che avrei potuto fare ma
che, essendo una sola persona, non ero mai riuscito a fare? Ora un Dennett
poteva restare a casa a fare il professore e il padre di famiglia, mentre
l’altro poteva darsi a una vita di viaggi e di avventure: gli sarebbe mancata la
famiglia, è naturale, ma sarebbe stato felice di sapere che l’altro Dennett
badava al focolare domestico. Avrei potuto essere fedele e allo stesso tempo
adultero; avrei perfino potuto farmi le corna da me… per non parlare di altre
possibilità più lubriche che i miei colleghi facevano a gara per presentare alla
mia immaginazione sovraffaticata. Ma le mia peripezia nell’Oklahoma (o a
Houston?) avevano ridimensionato il mio gusto per l’avventura, e declinai
l’opportunità che mi veniva offerta (anche se naturalmente non fui mai del tutto
sicuro che venisse offerta a *me* per primo).  
C’era un’altra prospettiva ancora più spiacevole: che il ricambio, fosse Uberto
o Yorick, venisse staccato da qualunque ingresso proveniente da Fortebraccio e
lasciato così, staccato. Allora, come nell’altro caso, ci sarebbero stati due
Dennett, o almeno due pretendenti al mio nome e ai miei beni: uno nel corpo di
Fortebraccio e l’altro, misero e triste, privo di corpo. Tanto l’egoismo quanto
l’altruismo mi spingevano a far sì che ciò non accadesse. Chiesi dunque che
venissero prese delle precauzioni affinché nessuno potesse mai manomettere i
collegamenti del ricetrasmettitore o l’interruttore principale senza che io
(noi? no, *io*) lo sapessi e vi consentissi. Poiché non volevo certo passare
tutta la vita a Houston a far la guardia alle apparecchiature, fu deciso di
comune accordo che tutti i collegamenti elettronici del laboratorio fossero
messi sotto chiave. Tanto quelli che governavano il sistema per il mantenimento
in vita di Yorick quanto quelli che governavano l’alimentazione di energia per
Uberto sarebbero stati protetti da apparecchiature a prova di guasto e io avrei
portato con me, dovunque andassi, l’unico interruttore principale, attrezzato
per il controllo a distanza via radio. Lo porto legato intorno alla vita e… un
momento…  *eccolo qui*. Ogni due o tre mesi faccio una verifica della situazione
e cambio i canali. Naturalmente lo faccio solo alla presenza di amici, perché se
l’altro canale, Dio non voglia, fosse morto o altrimenti occupato, dev’essere
presente qualcuno a cui stiano a cuore i miei interessi, per ricommutarlo e
farmi così tornare dal nulla. Infatti, pur essendo in grado di sentire, vedere,
udire e percepire qualunque cosa succedesse al mio corpo dopo questa
commutazione, non sarei tuttavia in grado di governarlo. Tra l’altro le due
posizioni dell’interruttore non sono contrassegnate di proposito, e quindi non
ho la più pallida idea se lo sto commutando da Uberto a Yorick o viceversa.
(Alcuni di voi potranno pensare che in tal caso io non so in realtà *chi* sono,
per non parlare di dove sono. Ma queste riflessioni ormai non intaccano la mia
fondamentale Dennettità, il senso che ho della mia identità principale. Se è
vero che in un certo senso non so chi sono, questa è un’altra delle vostre
verità filosofiche di portata insignificante).
Comunque ogni volta che ho girato l’interruttore finora non è accaduto nulla.
*Proviamo, dunque…*

“DIO SIA LODATO! PENSAVO CHE NON AVRESTI PIÙ GIRATO QUELL’INTERRUTTORE! Non puoi
immaginare quanto siano state tremende queste due settimane… ma ora lo sai, il
purgatorio adesso tocca a te. Come ho desiderato questo momento! Vedi, circa due
settimane fa – scusatemi, signore e signori, ma devo spiegare tutto ciò a mio…
ehm, fratello, direi, tutto sommato; lui vi ha appena narrato i fatti, quindi
capirete – circa due settimane fa i nostri due cervelli hanno perso un po’ del
loro sincronismo. Ignoro quanto te se il *mio* cervello adesso sia Uberto o
Yorick, ma in ogni caso i due cervelli si sono separati e, una volta iniziato,
il processo di differenziazione se è messo a galoppare, perché io ero in una
stato di ricezione leggermente diverso rispetto all’ingresso che ricevevamo
entrambi, e ben presto questa differenza è cresciuta. In men che non si dica
l’illusione che io avessi il controllo del mio corpo, cioè del nostro corpo, è
scomparsa del tutto. Non potevo fa nulla, non avevo alcuna possibilità di
chiamarti. TU NON SAPEVI NEPPURE CHE IO ESISTESSI! Era come essere portati in
giro in una gabbia o, meglio, come essere posseduti: udivo la mia voce
pronunciare cose che non volevo dire, guardavo con rabbia impotente le mie mani
che compivano gesti che non volevo compiere. Quando avevamo il prurito tu
grattavi ma non come l’avrei fatto io, e con tutto il tuo voltarti e rivoltarti
non mi facevi dormire. Ero spossato, ero sull’orlo di un esaurimento nervoso,
venivo portato qua e là senza potermi difendere nel tuo giro frenetico di
attività, sostenuto soltanto dalla consapevolezza che un giorno avresti girato
l’interruttore.

“Ora tocca a te, ma tu almeno avrai la consolazione di sapere che *io so* che tu
sei lì dentro. Come una madre in attesa, io ora mangio – o almeno sento odori e
sapori e vedo – per *due* e cercherò di facilitarti le cose. Non preoccuparti.
Non appena questo convegno sarà terminato, tu e io prenderemo il primo aereo per
Houston e vedremo che cosa di più fare per dare ad ognuno di noi un altro corpo.
Potrai avere un corpo di donna, potrai scegliere il colore che preferisci… Ma
pensiamoci su. Ecco, senti qua… per fare le cose onestamente, se entrambi
volessimo questo corpo, ti prometto che chiederò al direttore di tirare una
moneta per stabilire chi dei due lo potrà tenere e chi invece dovrà scegliersene
uno nuovo. Con questo dovrebbe essere assicurata la giustizia, no? Comunque, ti
prometto che avrò cura di te. Le persone qui presenti me ne sono testimoni.

“Signore e signori, la conferenza che abbiamo appena ascoltato non è esattamente
quella che avrei fatto io, ma vi assicuro che tutto ciò che lui ha detto è
assolutamente vero. E ora, col vostro permesso, tornerò al mio… torneremo al
nostro… posto”.


Riflessioni
-----------

La storia che avete appena letto non solo non è vera (caso mai ne dubitaste), ma
non potrebbe neppure esserlo. Le meraviglie tecnologiche che vi sono descritte
sono impossibili oggi e alcune rimarranno forse per sempre irraggiungibili; ma
non è questo il punto. Ciò che importa è sapere se vi sia qualcosa d’impossibile
in linea di principio – qualcosa d’incoerente – nell’insieme della storia.
Quando le fantasie filosofiche diventano troppo bizzarre (tirando in ballo per
esempio macchine del tempo o universi duplicati o demoni ingannatori
infinitamente potenti) è forse buona norma evitare di trarne *qualsiasi*
conclusione. Il nostro esser persuasi di capire i problemi trattati potrebbe
essere ingannevole, un’illusione provocata dal vivido realismo del racconto
fantastico.

L’operazione chirurgica e le microradio descritte in questo racconto superano di
gran lunga le possibilità della tecnologia sia attuale si di un futuro
chiaramente prevedibile, ma è fantascienza certo “innocente”. Meno chiaro è se
rimanga entro i limiti l’invenzione di Uberto, la copia di Yorick, il cervello
di Dennett, fatta al calcolatore. (Noi venditori di fantasia possiamo
naturalmente inventare le regole via via che procediamo, ma col rischio di
raccontare una storia priva d’interesse teorico). Uberto, dice il racconto,
funziona per anni in *sincronia perfetta* con Yorick, senza che fra i due vi sia
alcun legame interattivo di correzione. Ciò non sarebbe soltanto un grande
trionfo tecnologico: sfiorerebbe addirittura il miracolo. In primo luogo, per
potersi avvicinare alla velocità del cervello umano nel trattare milioni di
canali d’ingresso e d’uscita paralleli, il calcolatore dovrebbe possedere una
struttura fondamentale del tutto diversa da quella dei calcolatori esistenti; ma
non basta: anche se possedessimo un calcolatore così simile al cervello, la sua
stessa mole e la sua complessità renderebbero praticamente impossibile la
prospettiva di un comportamento sincrono *indipendente*. Senza questo
funzionamento sincrono e identico dei due sistemi, dovremmo rinunciare a un
aspetto essenziale del racconto.

Perché? Perché l’assunto che vi sia una sola persona con due cervelli (uno di
riserva) dipende proprio da esso. Si consideri ciò che dice Ronald de Sousa in
“Rational Homunculi” a proposito di un caso analogo:  
La trasformazione del dottor Jekill nel signor Hyde è una cosa strana e
misteriosa. Sono due persone che si avvicendano in un solo corpo? Esiste
tuttavia una cosa ancora più strana: anche il dottor Juggle e il dottor Boggle
si avvicendano in un unico corpo. *Ma essi si somigliano come gemelli identici!*
Si obietterà: allora perché dire che si sono trasformati l’uno nell’altro? E
perché no? Se il dottor Jekill si può trasformare in un uomo così diverso da lui
come Hyde, dev’essere certo molto *più facile* per Juggle trasformarsi in Boggle,
che è esattamente uguale a lui

È necessario un conflitto o una forte differenza per far vacillare l’assunto
naturale che a un corpo corrisponde al massimo un agente.

Poiché parecchi degli aspetti più notevoli di “Dove sono?” dipendono
dall’ipotesi di un funzionamento sincrono e indipendente di Yorick e di Uberto,
è importante rilevare che questa supposizione è veramente eccessiva: è dello
stesso calibro della supposizione che da qualche parte esista un altro pianeta
esattamente identico alla Terra contenente un duplicato, atomo per atomo, di
ognuno di noi, del nostro ambiente e di tutti i nostri amici (Come nel famoso
esperimento ideale della “Terra gemella”, di Hilary Putnam. Si veda il cap.
“Letture ulteriori”.), o della supposizione che l’età dell’universo sia di soli
cinque giorni (esso ci appare più antico perché Dio, quando lo fece, cinque
giorni fa, creò un sacco di adulti pronti carichi di “ricordi”, biblioteche
colme di libri dall’aspetto antico, montagne piene di fossili nuovi di zecca e
così via).

La possibilità di un cervello protesico come Uberto è quindi soltanto una
possibilità di principio, per quanto la costruzione di pezzetti meno
straordinari di sistema nervoso artificiale possa essere imminente. Esistono già
vari occhi artificiali televisivi, un po’ rudimentali, per i ciechi; alcuni
inviano gli ingressi direttamente a certe porzioni della corteccia visiva del
cervello, altri fanno a meno di questi virtuosismi chirurgici e trasmettono le
loro informazioni attraverso altri organi di senso esterni, ad esempio i
ricettori tattili del polpastrelli, o addirittura le traducono in formicolii
distribuiti secondo certe configurazioni sulla fronte, sull’addome o sul dorso
del soggetto.

*D.C.D. (1978)*

*Nota:* Al contrario del resto dei contenuti di questo blog, questo racconto non
è in pubblico dominio, ma sotto copyright e pubblicato in Italia dalla
[Adelphi][1] sui libri [L'io della mente][2] e [Brainstorms][3].
La versione in lingua originale è leggibile su siti di [varie facoltà][4] di
scienze cognitive. 

[1]: http://www.adelphi.it/ 
[2]: http://www.adelphi.it/libro/9788845907913
[3]: http://www.adelphi.it/libro/9788845907708
[4]: http://instruct.westvalley.edu/lafave/where_am_i.html
