title: Macchine e uomini
date: 2015-07-13 20:11
tags: filosofia
summary: Un brano che mi ha fatto pensare, fra le altre cose, all'esperimento della stanza cinese, di John Searle. Searle sostiene che un'intelligenza artificiale potrà essere considerata una mente solo quando avrà capacità di comprensione.

Un brano che mi ha fatto pensare, fra le altre cose, all'esperimento della [stanza cinese][1], di [John Searle][2]. Searle sostiene che un'intelligenza artificiale potrà essere considerata una mente solo quando avrà capacità di comprensione.

Secondo Searle, perché questo accada, è necessario che l'intelligenza in questione abbia tali "poteri causali", che solo la mente umana ha. Con essi, si intende la capacità di essere una causa, il principio di un'azione. Si può essere il principio  se si possiede volontà, in caso contrario, si è reazione meccanica di altre cause. Searle da per scontato che la mente umana possieda sempre questi poteri causali, sia quindi dotata di volontà, di arbitrio. Ma che prove abbiamo a riguardo?

Tratto da [Frammenti di un insegnamento sconosciuto][3] di [P.D. Ouspensky][4].
Pag 23.

A Mosca, un giorno parlavo a [Gurdjieff][5] di Londra, dove avevo soggiornato
brevemente qualche tempo prima, e della spaventosa meccanizzazione che stava
invadendo le grandi città europee, senza la quale era probabilmente impossibile
vivere e lavorare nel vortice di quegli enormi ‘giocattoli meccanici'.

"Le persone stanno trasformandosi in macchine, dicevo, e non dubito che un
giorno diventeranno macchine perfette. Ma sono ancora capaci di pensare? Non lo
credo. Se tentassero di pensare, non sarebbero delle così belle macchine".

"Sì, rispose G., è vero, ma solo in parte. La vera questione è questa: di
*quale pensiero* si servono nel loro lavoro? Se si servono del pensiero
appropriato, potranno persino pensare meglio, nella loro vita attiva in mezzo
alle macchine.  Ma ancora una volta a condizione che si servano del pensiero
appropriato".

Non comprendevo ciò che G. intendeva per ‘pensiero appropriato' e lo compresi
solo molto più tardi.

"In secondo luogo, la meccanizzazione di cui voi parlate non è affatto
pericolosa. Un uomo può essere un *uomo* - ed egli accentuò questa parola - pur
lavorando con le macchine. Vi è un'altra specie di meccanizzazione molto più
pericolosa: essere noi stessi una macchina. Non avete mai pensato che tutti gli
uomini sono *essi stessi* delle macchine?".

"Sì, da un punto di vista strettamente scientifico, tutti gli uomini sono
macchine guidate da influenze esteriori. Ma la questione è: può il punto di
vista scientifico essere interamente accettato?".

"Scientifico o non scientifico, per me è lo stesso, disse G. Voglio farvi
comprendere ciò che dico. Guardate! Tutte quelle persone che voi vedete - e
indicava la strada - sono semplicemente macchine, niente di più".

"Credo di Capire quello che voi intendete. Ho spesso pensato come nel mondo
siano pochi coloro che possano resistere a questa forma di meccanizzazione e
scegliere la propria via".

"È proprio questo il vostro più grave errore! disse G.. Voi pensate che
qualcosa possa scegliere la propria via, qualcosa che possa resistere alla
meccanizzazione; voi pensate che tutto non sia egualmente meccanico".

"Ma come! esclamai. Certamente no! L'arte, la poesia e il pensiero sono
fenomeni di tutt'altro ordine".

"Esattamente dello stesso ordine. Queste attività sono meccaniche esattamente
come tutte le altre. Gli uomini sono macchine e da parte di macchine non ci si
può aspettare altro che azioni meccaniche".

"Benissimo, gli dissi, ma non vi sono persone che non siano macchine?".

"Può darsi che ce ne siano, disse G.; soltanto, non sono quelle che voi vedete.
Non le conoscete. È proprio questo che voglio farvi capire".

Mi pareva piuttosto strano che egli insistesse tanto su questo punto. Ciò che
diceva mi sembrava ovvio e incontestabile. Allo stesso tempo, non mi erano mai
piaciute le metafore che in quattro parole pretendono di dire tutto. Esse
omettono le distinzioni. Io del resto avevo sempre sostenuto che le distinzioni
sono ciò che vi è di più importante e che, per comprendere le cose, bisogna
prima di tutto considerare i punti in cui esse differiscono. Mi sembrava
strano, di conseguenza, che G. insistesse tanto su un'idea che mi appariva
innegabile, a condizione tuttavia di non farne un assoluto e di ammettere delle
eccezioni.

"Le persone si assomigliano talmente poco, dissi. Ritengo impossibile metterle
tutto nello stesso sacco. Vi sono selvaggi, vi sono persone meccanizzate, vi
sono intellettuali, vi sono dei genii".

"Assolutamente giusto, disse G. Le persone sono molto differenti, ma la reale
differenza tra le persone voi non la conoscete e non potete vederla. Le
differenze di cui voi parlate, semplicemente non esistono. Questo deve essere
compreso. Tutte le persone che voi vedete, che conoscete, che vi può capitare
di conoscere, sono macchine, vere e proprie macchine che lavorano soltanto
sotto la pressione di influenze esterne, come voi stesso avete detto. Macchine
sono nate e macchine moriranno. Che c'entrano i selvaggi e gli intellettuali?
Anche ora, in questo preciso istante, mentre parliamo, parecchi milioni di
macchine cercano di annientarsi a vicenda. In che cosa differiscono, quindi?
Dove sono i selvaggi e dove gli intellettuali? Sono tutti uguali…

"Ma vi è una possibilità di cessare di essere una macchina. È a questo che noi
dobbiamo pensare e non certo ai diversi tipi di macchine esistenti. È vero che
le macchine differiscono le une dalle altre; un'automobile è una macchina, un
grammofono è una macchina e un fucile è una macchina. Ma questo che cosa
cambia? È la stessa cosa, si tratta sempre di macchine".

Ricordo un'altra conversazione che si può collegare a questa.

"Che cosa pensate della moderna psicologia?", domandai un giorno a G. con
l'intenzione di sollevare la questione della psicoanalisi, della quale avevo
diffidato fin dal primo giorno.

Ma G. non mi permise di andare così lontano.

"Prima di parlare di psicologia, disse, dobbiamo capire chiaramente di che cosa
tratta o di che cosa non tratta questa scienza. L'oggetto proprio alla
*psicologia* sono gli uomini, gli esseri umani. Di quale *psicologia*, ed egli
accentuò questa parola, si può parlare quando non si tratta che di macchine? È
la meccanica che è necessaria per lo studio delle macchine e non la psicologia.
Ecco perché noi cominciamo con la meccanica. Siamo ancora molto lontani dalla
psicologia".

Domandai: "Può un uomo smettere di essere una macchina?".

"Ah! È proprio questo il problema. Se voi aveste fatto più spesso simili
domande, forse le nostre conversazioni avrebbero potuto condurre a qualche
cosa. Sì, è possibile smettere di essere una macchina, ma, per questo, è
necessario prima di tutto *conoscere la macchina*. Una macchina, una vera
macchina, non conosce se stessa e non può conoscersi. Quando una macchina
conosce se stessa, da quell'istante ha cessato di essere una macchina; per lo
meno non è più la stessa macchina di prima. Comincia già ad essere responsabile
delle proprie azioni".

"Questo significa, secondo voi, che un uomo non è responsabile delle proprie
azioni?".

"Un *uomo* - ed egli sottolineò questa parola - è responsabile. Una *macchina*
no".

Un'altra volta domandai a G.: "Qual è, secondo voi, la migliore preparazione
per lo studio del vostro metodo? Per esempio, è utile studiare la cosiddetta
letteratura ‘occulta' o ‘mistica'?".

Dicendo questo, pensavo in modo particolare ai ‘Tarocchi' e a tutta la
letteratura riguardante i ‘Tarocchi'.

"Sì, disse G., attraverso la lettura si può trovare molto. Per esempio,
considerate il vostro caso: voi potreste conoscere già molte cose *se foste
capace di leggere*. Mi spiego: se voi aveste *compreso* tutto quello che avete
letto nella vostra vita, avreste già la conoscenza di ciò che ora cercate. Se
aveste capito tutto quanto è scritto nel vostro libro, qual è il suo titolo? -
e invece delle parole Tertium Organum' * pronunciò qualcosa di assolutamente
impossibile - toccherebbe a me venire da voi, inchinarmi e pregarvi di
insegnarmi. Ma *voi non comprendete* né quello che leggete, né quello che
scrivete. Non capite neppure quel che significa la parola *comprendere*. La
comprensione è tuttavia la cosa essenziale, e la lettura può essere utile solo
a condizione che si comprenda ciò che si legge. Ma naturalmente nessun libro
può dare una preparazione reale. È quindi impossibile dire quali siano i libri
migliori. Ciò che un uomo conosce *bene* - e accentuò la parola ‘bene' - questa
può essere la sua preparazione. Se un uomo sa bene come si prepara il caffè, o
come si fanno bene le scarpe, allora è già possibile parlare con lui. Il guaio
è che nessuno conosce bene qualcosa. Tutto è conosciuto alla meglio, in modo
superficiale".

Si trattava ancora di una svolta inaspettata che G. dava alle sue spiegazioni.
Le sue parole, oltre al loro senso ordinario, ne contenevano sempre un altro
completamente diverso. Ma intravedevo già che per arrivare a questo senso
nascosto, bisognava cominciare dal loro senso usuale e semplice. Le parole di
G., prese nel senso più semplice, erano sempre piene di significato, ma esse ne
avevano anche altri. Il significato più ampio e più profondo, rimaneva velato
per molto tempo.

Un'altra conversazione è rimasta nella mia memoria. Domandavo a G. che cosa un
uomo dovesse fare per assimilare il suo insegnamento.

"Cosa deve *fare*?, esclamò come se la domanda lo sorprendesse. Ma egli è
incapace di *fare* qualcosa. Deve prima di tutto *comprendere* certe cose. Ha
migliaia d'idee false e di concezioni false soprattutto su di sé e deve
cominciare con il liberarsi perlomeno da alcune di esse, se vuole acquistare
qualcosa di nuovo. Altrimenti, il nuovo sarebbe edificato su una base falsa ed
il risultato sarebbe ancora peggiore".

"Come ci si può liberare dalle idee false? domandai. Noi dipendiamo dalle forme
della nostra percezione. Le idee sono prodotte dalle forme della nostra
percezione".

G. scosse la testa:

"Di nuovo voi parlate di un'altra cosa. Voi parlate degli errori derivanti
dalle percezioni, ma non si tratta di questo. Nei limiti di date percezioni si
può sbagliare più o meno. Come vi ho già detto, la suprema illusione dell'uomo
è la sua convinzione di poter *fare*. Tutti pensano di poter fare, vogliono
fare, e la loro prima domanda riguarda sempre ciò che dovranno fare. Ma a dire
il vero, nessuno fa qualcosa e nessuno può fare qualcosa. Questa è la prima
cosa che bisogna capire. *Tutto accade.* Tutto ciò che sopravviene nella vita
di un uomo, tutto ciò che si fa attraverso di lui, tutto ciò che viene da lui -
*tutto questo accade*. E questo capita allo stesso modo come la pioggia cade
perché la temperatura si è modificata nelle regioni superiori dell'atmosfera,
come la neve fonde sotto i raggi del sole, come la polvere si solleva con il
vento.

"L'uomo è una macchina. Tutto quello che fa, tutte le sue azioni, le sue
parole, pensieri, sentimenti, convinzioni, opinioni, abitudini, sono i
risultati di influenze esteriori, di impressioni esteriori. Di per se stesso un
uomo non può produrre un solo pensiero, una sola azione. Tutto quello che dice,
fa, pensa, sente - accade. L'uomo non può scoprire nulla, non può inventare
nulla. Tutto questo accade.

"Ma per stabilire questo fatto, per comprenderlo, per convincersi della sua
verità, bisogna liberarsi da mille illusioni sull'uomo, sul suo potere
creativo, sulla sua capacità di organizzare coscientemente la sua propria vita,
e co sì via. Tutto questo in realtà non esiste. Tutto accade movimenti
popolari, guerre, rivoluzioni, cambiamenti di governi, tutto accade. E capita
esattamente nello stesso modo in cui tutto accade nella vita dell'uomo preso
individualmente. L'uomo nasce, vive, muore, costruisce case, scrive libri, non
come lo desidera, ma come capita. Tutto accade. L'uomo non ama, non desidera,
non odia - tutto accade.

"Nessuno vi crederà se gli dite che non può fare nulla. Questa è la cosa più
offensiva e spiacevole che si possa dire alla gente. Ed è particolarmente
spiacevole e offensiva perché è la verità e nessuno vuol conoscere la verità.

"Se capite questo, ci sarà più facile parlare. Ma una cosa è capire con
l'intelletto che l'uomo non può far nulla ed un'altra sentirlo vivamente ‘con
tutta la propria massa'; essere realmente convinti che è così e mai
dimenticarlo.

"Questa questione del *fare* (G. accentuava tutte le volte questa parola) si
collega del resto a un'altra. Alla gente sembra sempre che gli altri non
facciano nulla come si dovrebbe, che gli altri facciano tutto sbagliato.
Invariabilmente ognuno pensa che lui potrebbe fare meglio. Nessuno comprende né
vuol comprendere che ciò che viene fatto attualmente in un certo modo - e
soprattutto ciò che *è stato già fatto* - non poteva essere fatto altrimenti.
Avete notato come parlano tutti della guerra? Ognuno ha il proprio piano, la
propria teoria. Ognuno è del parere che niente viene fatto come si dovrebbe. In
verità però, tutto viene fatto nell'unico modo possibile. Se *una sola cosa*
potesse essere fatta diversamente, tutto potrebbe diventare diverso. E allora
forse non ci sarebbe stata la guerra.

"Cercate di, capire quel che dico: tutto dipende da tutto, tutte le cose sono
collegate, non vi è niente di separato. Tutti gli avvenimenti seguono dunque il
solo cammino che possono prendere. Se le persone potessero cambiare, tutto
potrebbe cambiare. Ma esse sono quelle che sono, e di conseguenza le cose,
anche esse sono quelle che sono".

Era molto difficile da mandar giù. "Non vi è nulla, assolutamente nulla, che
possa essere fatto?", domandai.

"Assolutamente nulla".

"E nessuno può fare nulla?".

"È un'altra questione. Per *fare*, bisogna *essere*. E bisogna per prima cosa
comprendere cosa significa *essere*. Se continueremo queste conversazioni,
vedrete che ci serviremo di un linguaggio speciale e che per essere in grado di
parlare con noi, bisogna imparare questo linguaggio. Non vale la pena di
parlare nel linguaggio ordinario, perché, in questa lingua è impossibile
comprenderci. Questo vi stupisce. Ma è la verità. Per riuscire a comprendere è
necessario imparare un'altra lingua. Nella lingua che parla la gente non ci si
può capire. Vedrete più tardi perché è così.

"E poi bisogna imparare a dire la verità. Anche questo vi sembra strano. Non vi
rendete conto che si debba imparare a dire la verità. Vi sembra che basti
desiderare o decidere di dirla. E io vi dico che è relativamente raro che le
persone dicano una bugia deliberatamente. Nella maggior parte dei casi pensano
di dire la verità. Mentono continuamente, sia a se stessi che agli altri. Di
conseguenza nessuno comprende gli altri, né se stesso. Pensateci: potrebbero
esserci tante discordie, profondi malintesi e tanto odio verso il punto di
vista o l'opinione altrui, se le persone fossero capaci di comprendersi l'un
l'altro? Ma non possono comprendersi perché non possono non mentire. Dire la
verità è la cosa più difficile del mondo; si deve studiare molto, e per molto
tempo, per poter un giorno dire la verità. Il desiderio solo non basta. *Per
dire la verità, bisogna essere diventati capaci di conoscere cosa è la verità e
cos'è una menzogna; e prima di tutto in se stessi.* E questo nessuno lo vuol
conoscere".

* Titolo di un'opera di Ouspensky (Ed. inglese 1922).

[1]:https://it.wikipedia.org/wiki/Stanza_cinese
[2]:https://it.wikipedia.org/wiki/John_Searle
[3]:https://it.wikipedia.org/wiki/Frammenti_di_un_insegnamento_sconosciuto
[4]:https://it.wikipedia.org/wiki/P%C3%ABtr_Dem%27janovi%C4%8D_Uspenskij
[5]:https://it.wikipedia.org/wiki/Georges_Ivanovi%C4%8D_Gurdjieff
