title: Norm, un corto (non) sullo stupro
date: 2017-06-28 18:01
tags: etica, cortometraggi

Consiglio la visione di questo cortometraggio della **Vidara Films**.

["Norm" - A Short Film][1]

È un'analogia molto azzeccata, non vi dico su cosa perché è abbastanza facile da capire.
Chi vuole scaricare il video usando programmi come [youtube-dl][2], può copiare i sottotitoli italiani nella stessa directory del video: [Norm - sottotitoli italiani][3]
Oppure scaricarli direttamente dal video col comando:
```
youtube-dl --write-sub --sub-format srt --sub-lang it https://www.youtube.com/watch?v=poxl0K9UrP0
```

È disponibile anche il testo sotto forma di copione. [Norm - copione PDF][4] - [ODT][5]

[1]:https://www.youtube.com/watch?v=poxl0K9UrP0
[2]:https://rg3.github.io/youtube-dl/
[3]:{filename}/misc/Norm_ita_subs.zip
[4]:{filename}/pdfs/Norm.pdf
[5]:{filename}/misc/Norm.odt
