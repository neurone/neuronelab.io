title: Commenti
date: 2016-09-04 10:12
tags: blog

Ho abilitato la possibilità di commentare sul blog tramite _Disqus_. Purtroppo,
la piattaforma traccia gli utenti, quindi chi si sentisse infastidito dalla
cosa, dovrebbe bloccarlo tramite un addon tipo [uBlock Origin][1],
[RequestPolicy][2], oppure disabilitare del tutto gli script.

Ho scelto questo perché m'è sembrato il compromesso più pratico. Molto
probabilmente, buona parte degli utenti già usano disqus su altri siti, e chi
invece è interessato a preservare al massimo la propria privacy, conosce i modi
tramite cui bloccare questi contenuti.

Sarà possibile commentare come _ospite_ ed i commenti verranno pubblicati
immediatamente, senza dover aspettare la moderazione, a meno che il commento
includa dei link: in quel caso, per evitare lo spam, il commento verrà
pubblicato solo dopo la moderazione.

Chi volesse bloccare Disqus ma comunque commentare, può mandarmi un email
all'indirizzo indicato nei contatti. Il commento verrà pubblicato manualmente
nel post relativo.

[1]:https://github.com/gorhill/uBlock
[2]:https://addons.mozilla.org/en-US/firefox/addon/requestpolicy-continued/
