title: Le ragioni dei vegani - Massimo Filippi
date: 2015-04-15 19:15:55 +0200
tags: veg, filosofia, psicologia, etica
summary: Anche l'alimentazione può essere oggetto di considerazioni morali? Perché la filosofia non se ne è mai occupata? Quali sono gli argomenti a favore del rifiuto della carne? L'etica del “Né l'uovo oggi né la gallina domani”.

Anche l'alimentazione può essere oggetto di considerazioni morali? Perché la filosofia non se ne è mai occupata? Quali sono gli argomenti a favore del rifiuto della carne? L'etica del “Né l'uovo oggi né la gallina domani”.

I sistemi filosofici sembrano considerare l'atto del mangiare come
fondamentalmente extra-morale.  
Questa dimenticanza appare ancora più sospetta quando si consideri che tutta la
vita activa è caduta sotto le lenti dell'etica, dalla sessualità ai problemi
connessi alle fasi iniziali e finali della vita, dal turismo al consumo delle
risorse, dalle pratiche aziendali a quelle bancarie. L'unica spiegazione
plausibile per questa “svista” è che l'etica è stata sempre declinata, sia in
termini di agente che di paziente morale, in maniera rigorosamente
antropocentrica. In effetti, se ci si pensa, tutti i sistemi filosofici, pur
nella loro estrema varietà, concordano sempre su un fatto: l'esclusione
dell.animale.  
Se l'unico soggetto delle considerazioni etiche è l'uomo, l'alimentazione
diventa un aspetto da lasciare alle categorie della clinica: l'alimentazione può
essere salubre o nociva, ma mai morale o immorale.

Antipasto
---------
Negli ultimi anni, però, altri soggetti si sono affacciati sulla scena,
reclamando l'accesso al club esclusivo della morale: gli altri animali. Ecco le
parole con cui, più di 100 anni fa, Henry S. Salt inaugurava il libro intitolato
Animals' Rights: “Gli animali hanno diritti? Indubbiamente sì, se li accordiamo
agli uomini”. Questo è anche quanto sostengono i vegani che, rifiutando ogni
forma di sfruttamento animale, escludono dalla loro dieta non solo carne e
pesce, come fanno i vegetariani, ma anche uova, latte e derivati, non vestono
capi in pelle o di lana e si oppongono, tra le altre cose, a circhi, zoo e
sperimentazione animale.
<!--more-->

Tagliere
--------
Nessuna specie animale in nessuna fase della sua vita sfugge al nostro tagliere:
l'uomo è un animale onnivoro nel senso più completo del termine. Questo,
unitamente all'abnorme crescita demografica della nostra specie, fa sì che non
sia più possibile contare quanti animali vengano uccisi nel mondo per la nostra
alimentazione. C'è chi dice 50 miliardi ogni anno. Ma poi si affretta ad
aggiungere: “Senza contare pesci, conigli e, in genere, animali di piccola
taglia che vengono venduti a tonnellaggio”. Questa impossibilità di contare
l'enormità del massacro è, però, un'impossibilità più ontologica che pratica:
contare è già aver ridotto l'incommensurabilità del mondo che ogni essere
senziente porta con sé a una sorta di denominatore comune, in questo caso la
commestibilità delle sue carni, è già aver trasformato l'esistente in un immenso
tagliere. Si conta quando quello che viene contato non conta più. Ma, in fondo,
la dimensione dell'olocausto è di scarsa importanza rispetto alla perversità che
lo precede.  
Come scrive Yukio Mishima: “La gente insensibile è sconvolta solamente quando
vede scorrere il sangue; eppure, quando il sangue è versato, la tragedia è ormai
compiuta”. E prima che scorra il sangue, la tragedia è quella delle galline
ovaiole e dei vitelli da carne bianca costretti per i pochi mesi della loro
misera vita alla più assoluta immobilità, dei pulcini maschi stritolati vivi
appena nati perché inservibili alla produzione delle uova, degli uccelli
accecati per servire da richiamo ai cacciatori per ucciderne altri, dei maiali
bloccati in minuscole gabbie di contenzione per allattare i loro piccoli, dei
pesci che prima di spirare nel silenzio assoluto dell'asfissia hanno circolato
insensatamente nelle vasche di coltura, delle mucche da latte continuamente
ingravidate e private della loro prole, di tutti quegli esseri a cui vengono,
senza anestesia e compassione, strappati denti, becchi, code, testicoli per far
sì che non si amputino da soli nell'inferno della loro desolazione e che vengono
riempiti di antibiotici e steroidi perché ingrassino rapidamente e non muoiano
prima del tempo stabilito dalla catena di smontaggio, di tutti quei condannati a
morte che, senza capirne il motivo, vengono caricati a botte sui camion che
percorrono le nostre autostrade diretti al mattatoio.

[Thomas Nagel][1] ci ha invitato a immaginare che [cosa si provi][2] a essere un
pipistrello; forse, sarebbe opportuno arricchire questo esperimento mentale
chiedendosi che effetto farebbe essere una gallina ovaiola, una mucca da latte,
un uccello da richiamo, un maiale d'allevamento intensivo e, più in generale, un
cosiddetto animale da reddito. Pochi secondi in questo incubo dovrebbero
convincerci che, in effetti, sedersi a tavola non è una questione extra-morale e
che la tragedia che precede il versamento del sangue è già tutta racchiusa nella
situazione di contenzione dove l'animale è alienato fino all'estremo, per cui il
suo stesso corpo diventa il suo peggior nemico.

Primi
-----
Il giardino dell'Eden è esistito. C'è stato, infatti, un tempo in cui non ci
consideravamo i primi, ma in cui vivevamo, da raccoglitori, in armonia con il
resto del vivente, un tempo in cui non ci eravamo ancora tagliati fuori dal
corpo della natura inaugurando quella serie ininterrotta di tagli che ci ha
portato qui dove siamo, a ballare incuranti e spensierati sull'orlo di un abisso
senza precedenti, un abisso onnivoro quanto la nostra specie, pronto a ingoiare
l'intero esistente in una crisi ecologica, sociale e morale senza precedenti.
C'è stato un tempo in cui la nostra occupazione principale non era quella di
uccidere e di questo ne dà inconfutabile testimonianza il nostro organismo.
Tutto, dalle dimensioni dell'intestino alla morfologia dei denti, dagli enzimi
contenuti nella saliva all'anatomia e ai movimenti della mandibola, indica che
l'umano è più simile agli animali erbivori che a quelli carnivori. Anche la
dieta di alcuni nostri antenati ominidi e dei primati non umani a noi più
vicini: i gorilla sono vegani, gli scimpanzé e gli oranghi essenzialmente
frugivori, testimonia il fatto che l'onnivorismo umano, contrariamente a quanto
comunemente si pensa, è un'acquisizione recente: per milioni di anni nel corso
della nostra evoluzione siamo stati vegani.  
La domanda su come sia potuta accadere la tragica metamorfosi alimentare in cui
ancora viviamo è la stessa che due millenni fa si poneva Plutarco: “Io mi
domando con stupore in quale circostanza e con quale disposizione spirituale
l'uomo toccò per la prima volta con la bocca il sangue e sfiorò con le labbra la
carne di un animale morto; e imbandendo mense di corpi morti e corrotti, diede
altresì il nome di manicaretti e di delicatezze a quelle membra che poco prima
muggivano, si muovevano e vivevano. Come poté la vista tollerare il sangue di
creature sgozzate, scorticate, smembrate, come riuscì l'olfatto a sopportarne il
fetore? Come mai quella lordura non stornò il senso del gusto che veniva a
contatto con le piaghe di altre creature e che sorbiva gli umori e i sieri
essudati da ferite mortali?”.  
Se la risposta a questo angoscioso e accorato “come?” si perde nelle notte dei
tempi, è invece certo che l'atto stesso di cibarsi di altri corpi, in tutto e
per tutto simili al nostro, è un atto di inconcepibile violenza con il quale
abbiamo scelto di vivere sopra e al di fuori di quel “reticolo” di relazioni
intramondane che, estendendo il pensiero di Merleau-Ponty fino a comprendere
anche gli altri animali, potremmo chiamare carne-del-mondo, quel gesto con cui
abbiamo scelto una dubbia sopravvivenza (supra-vivenza) alla ricchezza del
vivere-intra, della vita tra gli altri e con l'altro. In una sorta di colossale
cortocircuito, il mangiarci la carne del mondo è al contempo la causa e il
risultato dell'esserci messi in cima alla scala degli esseri, che a questo punto
coincide con la catena alimentare, dell'esserci trasformati in puro spirito, di
aver negato la nostra stessa carne con la sua finitezza e mortalità, a favore
dell'immedesimazione con un Dio anch'esso supra-vivente e onnivoro: per il Dio
della nostra tradizione, comunque lo si declini, la carne del mondo è infatti
perennemente sacrificabile sugli altari dell'altro mondo.  
Il precedente richiamo a Plutarco ha anche il valore di ricordarci che la
caratteristica più sorprendente del vegetarianesimo è la lunghezza della sua
storia. Per rimanere nell'ambito della nostra tradizione culturale, oltre a
Plutarco si pensi, solo per fare qualche esempio, all'orfismo e allo
zoroastrismo e poi a Pitagora, Empedocle, Epicuro, Plotino, Porfirio e… Diogene!
Si pensi che era vegetariano Teofrasto, l'allievo che sostituirà Aristotele alla
direzione del Liceo; si pensi che norme per il benessere animale sono contenute
nel Levitico – e, come ci ricorda Milan Kundera, la Genesi (come il Levitico) “è
stata redatta da un uomo, non da un cavallo”. E per fare capolino al di là della
cultura “europea”, non dimentichiamoci la precocissima comparsa del
vegetarianesimo in India, dove, nel terzo secolo prima di Cristo, l'imperatore
Asoka, convertitosi al buddhismo, divenne uno strenuo propugnatore del
vegetarianesimo e rese illegali una quantità di pratiche che coinvolgevano
animali, tra cui i sacrifici rituali. In India, tuttora patria del
vegetarianesimo, prosperano religioni “vegetariane”, quali l'induismo e il
giainismo. In poche parole, il vegetarianesimo non è una moda recente destinata
a passare.  

Secondi di carne
----------------
Günther Anders sostiene che il modello di consumo della nostra epoca è quello
del mangiare, cioè di quel consumare che rende “il più piccolo possibile
l'intervallo che si estende tra la produzione e la liquidazione del prodotto”.
Il mangiar carne rientra nella sfera della morale anche in quanto emblema
dell'epoca del consumatore onnivoro che divora tutto ciò che gli sta intorno:
gli altri animali, il pianeta (di cui la dieta carnea favorisce la
desertificazione e l'inquinamento), lo strato di ozono (gli allevamenti
contribuiscono significativamente alla produzione di gas serra), le riserve
idriche ed energetiche (che vengono dissipate in notevole quantità nella filiera
della carne), gli abitanti del cosiddetto Terzo Mondo (che coltiva le proprie
terre per alimentare gli animali d'allevamento del Primo Mondo) e il proprio
corpo (che la dieta carnea espone a un incrementato rischio di contrarre varie
malattie, quali quelle neoplastiche e cardiovascolari).

Secondi di pesce
----------------
“Pesce grande mangia pesce piccolo” è uno dei modi in cui spesso ci si sente
obiettare circa la presunta irragionevolezza della dieta vegana. Il che
significa che partendo da una assunzione sbagliata, “è sempre stato così”, si
arriva a giustificare la peggiore delle leggi che abbiamo potuto partorire: “la
legge del più forte”. Risiede in questo la caratteristica principale della dieta
onnivora: essa, interrompendo quel “reticolo” di rapporti che ci lega al resto
del vivente, innesca una sorta di inarrestabile reazione a catena (alimentare)
che tutto ingloba e digerisce. Il mangiar carne è la quintessenza della banalità
del male di arendtiana memoria. Stiamo soffocando asfissiati nella malvagità del
banale senza emettere un suono, esattamente come fanno i pesci che uccidiamo a
tonnellate.  
La sola differenza è che loro sono consapevoli della propria morte, mentre noi,
nell'aberrante ripetizione del refrain del “pesce grosso mangia pesce piccolo”,
assolutamente no. Forse, potremmo cominciare a riprendere contatto col mondo che
ci circonda non solo quando rigetteremo “la legge del più forte” nell'ambito dei
rapporti intraumani, ma quando riconosceremo che “l'idea che la vita
subrazionale sia destinata a rimanere per sempre un universo del genere [di
sofferenza, violenza e distruzione] non è né filosofica né scientifica”
(Marcuse), trasformando la natura da oggetto di dominio a soggetto di
liberazione.

Contorni
--------
“Ma anche i vegetali soffrono!” è l'altra obiezione “fondamentale” mossa ai
vegani. A parte la disonestà di chi non si preoccupa affatto del benessere dei
vegetali, ma solo di mettersi facilmente in pace la coscienza rilevando delle
presunte contraddizioni nel comportamento dell'interlocutore, a parte la
contro-obiezione razionale secondo cui chi mangia animali ingrassati con
vegetali di fatto si ciba di entrambi, esiste una considerazione ulteriore già
sviluppata da Leonardo nel Manoscritto H: “Poiché la natura ha dotato di
sensibilità al dolore quegli organismi viventi che hanno capacità di muoversi –
al fine, per mezzo di ciò, di salvaguardare quei membri che per tale capacità
sono esposti al danno e alla distruzione – gli organismi che non possiedono la
capacità di movimento non possono imbattersi in oggetti pericolosi e,
conseguentemente, le piante non necessitano di possedere una sensibilità al
dolore. Per questo motivo, discende che se spezzi una pianta, le sue membra non
provano dolore come succede agli animali. Leonardo, anche in questo anticipando
di secoli gli sviluppi delle neuroscienze, sostiene cioè che sensibilità e
motricità sono inestricabilmente associate e che solo una mente addestrata al
taglio può pensarle come separate.

L'unità animale di capacità motoria, percezione e sentimento trova riscontro
anche nella recente scoperta dei “neuroni specchio”, neuroni che hanno la
peculiarità di attivarsi sia quando si compie un'azione, sia quando si osserva
un'azione compiuta da un altro. Tali neuroni permettono di creare un copia
motoria della percezione visiva dell'azione altrui, permettendo così a soggetti
interagenti di costruire una rappresentazione comune delle azioni proprie e di
quelle degli altri, rappresentazione che è alla base dell'interazione sociale e
dell'empatia.  
Dietro alla morale si nasconderebbe perciò una “simulazione incarnata”
(Rizzolatti e Sinigaglia), che la rende qualcosa di meno spirituale di quanto la
nostra tradizione ci ha indotto a credere, qualcosa di più gestuale e di più
corporeo. L'etica non è solo un modo di sentire, ma anche un comportamento, un
modo di muoversi, un galateo. Parte di questo galateo è il modo in cui ci si
siede a tavola e cosa ci si appresta a mangiare; galateo che non prevede l'uso
del coltello, che non separa uomo da animale e sensibilità da movimento.

Dolce
-----
Conosciamo il salato – il conto che stiamo per pagare per l'indecoroso pranzo
che da millenni portiamo avanti ai danni del vivente –, conosciamo l'amaro –
della morte e del dolore che abbiamo incrementato a dismisura –, ma abbiamo poca
dimestichezza con il dolce. Creare un buon dolce è difficile perché richiede una
complessa e variabile dosatura di gradevolezza (uguaglianza) e amabilità
(libertà), che la società del taglio e del tagliar corto ci ha insegnato a tener
separate. L'uguaglianza ha così assunto il sapore agro della giustizia cieca –
la “disuguaglianza tra uguali” di [Murray Bookchin][4] –, mentre la libertà ha spesso
il sapore insipido del laissez faire del neoliberismo. La dieta vegana, invece,
con la sua messa in scacco delle logiche identitarie racchiuse nella formula
“noi/loro”, è parte di quel difficile equilibrio di eguaglianza e libertà di cui
siamo incessantemente alla ricerca, preconizzando un mondo dove sia possibile
l'uguaglianza tra disuguali.  
La dieta vegana è parte della dolcezza a venire perché cambia il pronome
dell'etica, perché introduce l'etica in prima persona – “Sii il cambiamento che
vuoi vedere avvenire nel mondo” (Gandhi)-, sostituendola a quella in terza
persona, quella che prevede che la responsabilità del male sia sempre di altri.  
È ciò che intendeva il premio Nobel per la letteratura Isaac Bashevis Singer,
laddove scriveva: “Questa è la mia protesta contro la condotta del mondo. Essere
vegetariani significa dissentire, dissentire con il corso degli eventi attuali.
Energia nucleare, carestie, crudeltà, dobbiamo prendere posizione contro queste
cose. Il vegetarianesimo è la mia presa di posizione. E penso che sia una presa
di posizione consistente”. Un'etica in prima persona che, naturalmente e
immediatamente, passa dall'io a un nuovo “noi”: il veganesimo, la dieta che non
prevede né l'uovo oggi né la gallina domani, è un altro modo di dire quanto
sostenuto da Albert Camus: “Mi rivolto, dunque siamo”.

*[Massimo Filippi][3]*

[1]:https://it.wikipedia.org/wiki/Thomas_Nagel
[2]:https://tichy.github.io/blog/2014/08/18/che-cosa-si-prova-a-essere-un-pipistrello/
[3]:http://www.hsr.it/research/organization/divisions-centers/division-of-neuroscience/massimo-filippi/2/
[4]:http://ita.anarchopedia.org/Murray_Bookchin
