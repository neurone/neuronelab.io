title: Tirare a indovinare
date: 2014-10-13 22:28:51 +0200
tags: libri, politica, scienza
summary: Il brano che segue è preso dal libro Un uomo senza patria (2005) di Kurt Vonnegut.

Il brano che segue è preso dal libro [Un uomo senza patria][1] (2005) di [Kurt Vonnegut][2].

Per l’ultimo milione di anni o giù di lì, gli esseri umani hanno dovuto tirare a
indovinare su quasi tutto. I personaggi principali dei libri di storia non sono
altro che quelli di noi che hanno tirato a indovinare nella maniera più
affascinante, e a volte più spaventosa.

Ne posso nominare due?

Aristotele e Hitler.

Uno ci ha azzeccato, e l’altro ha sbagliato.

E nel corso dei secoli le masse umane, sentendo di avere dei mezzi di giudizio
inadeguati, proprio come noi oggi, e a ragione, si sono viste praticamente
costrette a credere di volta in volta a quelli che tiravano a indovinare.

I russi che non erano d’accordo con le congetture di Ivan il Terribile, per
esempio, rischiavano di ritrovarsi il cappello inchiodato alla testa.
<!--more-->
Dobbiamo comunque riconoscere che i più persuasivi fra quelli che tiravano a
indovinare – perfino Ivan il Terribile, il quale oggi nell’ex Unione Sovietica è
un eroe – talvolta ci hanno dato il coraggio di sopportare immani sofferenze che
non eravamo in grado di comprendere. Carestie, pestilenze, eruzioni vulcaniche,
bambini nati morti: spesso quegli individui ci hanno dato l’illusione che la
buona e la cattiva sorte fossero comprensibili e a volte potessero essere
affrontate in maniera intelligente ed efficace. Senza questa illusione, forse il
genere umano si sarebbe arreso molto tempo fa.

Ma quelli che tiravano a indovinare, di fatto, non ne sapevano più della gente
comune, anzi a volte ne sapevano anche di meno, perfino quando – o specialmente
quando – ci hanno dato l’illusione di avere il controllo sul nostro destino.

Tirare a indovinare in maniera persuasiva è un ingrediente fondamentale della
capacità di leadership da così tanto tempo – anzi, lo è stato per tutto il corso
dell’esperienza umana – che non c’è affatto da sorprendersi se ancora oggi gran
parte dei leader del pianeta, nonostante tutte le informazioni di cui
improvvisamente possiamo disporre, vogliono che il meccanismo continui. Adesso è
il loro turno di tirare a indovinare, tirare a indovinare e avere intorno chi
gli dà retta. Un posto dove questo oggi si fa nella maniera più becera, tronfia
e ignorante è Washington. I nostri leader sono stufi marci delle tonnellate di
informazioni valide che sono state riversate sul genere umano dalla ricerca,
dallo studio e dal giornalismo investigativo. Pensano che ne sia stufa la
nazione intera, e potrebbero anche aver ragione. Non è al sistema aureo che
vogliono riportarci. Vogliono scendere a un livello ancora più elementare.
Vogliono riportarci al sistema degli elisir dei ciarlatani.

Le pistole cariche sono un bene per tutti tranne per chi è chiuso in galera o al
manicomio.

Esatto.

I milioni di dollari spesi per la sanità pubblica fanno crescere l’inflazione.

Esatto.

I miliardi di dollari spesi per le armi fanno scendere l’inflazione.

Esatto.

Le dittature di destra sono più vicine agli ideali americani rispetto alle
dittature di sinistra.

Esatto.

Più testate nucleari abbiamo, pronte a essere lanciate da un momento all’altro,
più l’umanità è al sicuro e migliore sarà il mondo che erediteranno i nostri
nipoti.

Esatto.

Le scorie industriali, specie quelle radioattive, non fanno male quasi a
nessuno, perciò la gente dovrebbe smettere di parlarne tanto.

Esatto.

Le industrie dovrebbero essere autorizzate a fare quello che gli pare: versare
mazzette, distruggere un pochino l’ambiente, gonfiare i prezzi, fregare i
clienti stupidi, annullare la concorrenza e svaligiare le casse del Tesoro
quando vanno in bancarotta.

Esatto.

Questa è la libera impresa.

Esatto anche questo.

I poveri hanno fatto qualche grosso errore, altrimenti non sarebbero poveri,
perciò i figli ne devono pagare le conseguenze.

Esatto.

Non ci si può aspettare che gli Stati Uniti d’America sappiano badare al loro
stesso popolo.

Esatto.

Quello è compito del libero mercato.

Esatto.

Il libero mercato è un sistema automatico di giustizia.

Esatto.

Sto scherzando.

E se siete davvero persone istruite capaci di pensare con la vostra testa, a
Washington non sareste visti di buon occhio. Conosco addirittura un paio di
ragazzini svegli delle medie che a Washington non sarebbero visti di buon
occhio. Vi ricordate quei dottori che qualche mese fa si riunirono per
annunciare che era un dato di fatto chiaro e lampante, scientificamente provato,
che l’umanità non sarebbe potuta sopravvivere neanche a un attacco lieve di
bombe H? Ecco, quelli a Washington non erano visti di buon occhio.

Anche se sparassimo noi la prima raffica di bombe atomiche e il nemico non
rispondesse mai al fuoco, i veleni prodotti probabilmente annienterebbero seduta
stante l’intero pianeta.

Qual è la risposta di Washington? Loro tirano a indovinare, e dicono che non
sarà così. A che serve l’istruzione? A governarci sono ancora questi sfrenati
amanti delle congetture – e nemici delle informazioni. Ed è quasi tutta gente
molto istruita. Pensateci un attimo. Gente che ha dovuto buttare via la propria
istruzione, perfino la laurea a Harvard o a Yale.
Se non l’avessero fatto, la loro incontenibile smania di tirare a indovinare non
potrebbe durare così tanto. Voi, per favore, non seguite il loro esempio. Ma
sappiate che, se farete uso della vasta miniera di conoscenze che oggi è a
disposizione delle persone istruite, vi ritroverete soli come cani. Il rapporto
numerico fra quelli che tirano a indovinare e voi è – adesso sono io che tiro a
indovinare – più o meno dieci a uno.

In caso non l’aveste notato, in seguito a delle elezioni sfacciatamente truccate
in Florida, nelle quali migliaia di afroamericani sono stati privati in maniera
arbitraria dei loro diritti, adesso gli Stati Uniti si presentano al resto del
mondo come una massa di spietati guerrafondai dalla mascella quadrata superbi e
ghignanti, dotati di un arsenale militare mostruosamente potente e privi di
oppositori.

In caso non l’aveste notato, oggi noi americani siamo temuti ed odiati in tutto
il mondo proprio come lo erano un tempo i nazisti.

E a ragione.

In caso non l’aveste notato, i nostri leader irregolarmente eletti hanno privato
della dignità umana milioni e milioni di persone solo a causa della loro fede e
della loro razza. Li feriamo, li uccidiamo, li torturiamo e li imprigioniamo
come e quando ci pare.

Una passeggiata.

In caso non l’aveste notato, priviamo della dignità umana anche i nostri
soldati, non a causa della loro fede o della loro razza, ma per via della loro
estrazione sociale.

Mandateli in qualunque posto. Fategli fare qualunque cosa.

Una passeggiata.

Perciò io sono un uomo senza patria, fatta eccezione per i bibliotecari e un
giornale di Chicago che si chiama *In These Times*.

Prima che attaccassimo l’Iraq, l’autorevolissimo *New York Times* ci aveva
garantito che vi erano nascoste delle armi di distruzione di massa.

Verso la fine della loro vita sia Albert Einstein che Mark Twain avevano perso
ogni speranza nella razza umana, anche se Twain non aveva nemmeno assistito alla
prima guerra mondiale. Ora la guerra è una forma di intrattenimento televisivo.
e quello che ha reso la prima guerra mondiale così emozionante sono state due
invenzioni americane: il filo spinato e la mitragliatrice.

Lo sharpnel prende il nome dall’ufficiale inglese che lo ha inventato. Non
piacerebbe anche a voi che dessero il vostro nome a qualcosa?

Come i miei illustri predecessori Einstein e Twain, anche io adesso abbandono
ogni speranza nell’umanità. Ho combattuto nella seconda guerra mondiale e devo
dire che questa non è la prima volta che soccombo a una spietata macchina da
guerra.

Le mie ultime parole? «La vita è un pessimo trattamento da infliggere a un
animale, fosse anche un topo».
Il napalm è stato creato ad Harvard. *Ve lo giuro!*

Il nostro presidente è cristiano? Lo era anche Adolf Hitler.

Che cosa possiamo dire ai nostri giovani, ora che delle personalità
psicopatiche, ossia individui privi di coscienza, privi di pietà e di vergogna,
hanno tolto tutto il denaro dalle casse del nostro governo e dalle nostre
aziende, e se lo sono preso per sé?

E il massimo che vi posso dare a cui aggrapparvi è ben poca cosa, in effetti.
Non è molto meglio di niente, e anzi forse è anche peggio di niente. È l’idea di
un vero eroe moderno. È la storia, per sommi capi, della vita di Ignaz
Semmelweis, un mio eroe.

Ignaz Semmelweis nacque a Budapest nel 1818. Cronologicamente, la sua vita ha in
parte coinciso con quella di mio nonno e con quella dei vostri nonni, e ci può
sembrare tanto tempo fa, ma in realtà era solo ieri.

Diventò un ostetrico, cosa che già basterebbe a fare di lui un eroe moderno.
Dedicò la vita alla salute dei neonati e delle loro madri. Ce ne servirebbero di
più, di eroi così. Al giorno d’oggi, man mano che diveniamo una nazione sempre
più industrializzata e militarizzata sotto il controllo di gente che tira a
indovinare, non ci si prende più cura quasi per niente delle madri, dei neonati,
dei vecchi e di tutti coloro che sono fisicamente o economicamente deboli.

Ho già detto prima che tutta la messe di informazioni di cui disponiamo oggi è
recentissima. È così recente che l’idea che molte malattie siano causate dai
germi risale solo a centoquaranta anni fa. La mia casa di villeggiatura a
Sagaponack (long Island) di anni ne ha il doppio. E non so come hanno fatto gli
operai a sopravvivere tanto a lungo da riuscire a finire di costruirla. Insomma,
la teoria dei germi è davvero recente. Quando mio padre era bambino, Louis
Pasteur era ancora vivo e al centro di mille polemiche. Al potere c’erano ancora
tanti di quelli che tiravano a indovinare, furiosi dell’idea che la gente desse
retta a lui invece che a loro.

Ecco, e anche Ignaz Semmelweis, appunto, era convinto che i germi portassero le
malattie. Quando andò a lavorare in un ospedale pediatrico di Vienna, rimase
inorridito nello scoprire che una madre su dieci moriva di febbre puerperale.

Si trattava di donne povere: quelle ricche partorivano ancora a casa. Semmelweis
studiò le procedure dell’ospedale e cominciò a sospettare che fossero i medici
stessi a provocare le infezioni alle pazienti. Notò che spesso passavano
direttamente dall’autopsia dei cadaveri nell’obitorio alle visite nel reparto
maternità. In via sperimentale, propose che i dottori si lavassero le mani prima
di toccare le pazienti.

Poteva mai esserci affronto peggiore? Come si permetteva di dare un consiglio
simile a gente di estrazione sociale superiore? Semmelweis si rese conto che non
era nessuno. Veniva da fuori, non aveva amici né protettori fra i nobili
austriaci. Ma le puerpere continuavano a morire, e Semmelweis, che non aveva le
stesse doti di diplomazia nei rapporti interpersonali, continuava a chiedere ai
suoi colleghi di lavarsi le mani.

E loro alla fine gli obbedirono, per beffa, per divertimento, per disprezzo.
Quanto si saranno insaponati, strofinati a dovere e puliti fin sotto le unghie…

E a quel punto, le pazienti smisero di morire: pensate un po’! Smisero di
morire. Semmelweis aveva salvato tutte quelle vite.
Di conseguenza, si può dire che abbia salvato milioni di vite, fra cui, con ogni
probabilità, anche la vostra e la mia. Che ringraziamento ricevette Semmelweis
dai più alti esponenti della sua professione nella società viennese – tutta
gente specializzata nel tirare a indovinare? Lo cacciarono dall’ospedale e
dall’Austria stessa, a cui aveva reso un servizio tanto grande. Concluse la sua
carriera in un ospedale di provincia in Ungheria. Fu lì che perse ogni speranza
nell’umanità – che poi saremmo noi e le nostre conoscenze dell’era informatica –
e in se stesso.

Un giorno, nella sala delle autopsie, prese la lama del bisturi con cui aveva
appena sezionato un cadavere e se la piantò di proposito nel palmo della mano.
Di lì a poco morì, come sapeva benissimo che sarebbe successo, di setticemia.
Quelli che tiravano a indovinare avevano il potere assoluto. Avevano vinto
ancora un volta. Eccoli, i veri germi. Ma in quell’occasione avevano in luce
anche un altro loro aspetto, di cui oggi dovremmo prendere attentamente nota.
Non gli interessava davvero salvare vite delle umane. Gli importa solo che la
gente gli dia retta – mentre loro, nella più totale ignoranza, continuano a
tirare a indovinare. Se c’è una cosa che odiano, è un individuo saggio.

Voi, però, siatelo. Salvateci la vita, e salvate anche la vostra. Siate persone
oneste.

[Un uomo senza patria][1]: [Kurt Vonnegut][2]

[1]:http://www.ibs.it/code/9788875210908/vonnegut-kurt/uomo-senza-patria.html
[2]:https://it.wikipedia.org/wiki/Kurt_Vonnegut
