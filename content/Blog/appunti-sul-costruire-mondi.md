title: Appunti sul costruire mondi
date: 2016-06-10 13:56
tags: letteratura, fantascienza
summary: Riporto tradotti degli appunti da un intervento di Charles Stross al _Edinburgh International Science Festival_ sul costruire mondi nella letteratura fantascientifica e fantasy.

Riporto tradotti degli appunti da un intervento di [Charles Stross][1] al
Edinburgh International Science Festival sul costruire mondi nella letteratura
fantascientifica e fantasy. Utile per scrittori e giocatori di ruolo.

Il post originale è qua: [Charlie's Diary: Some notes on world building][2]. La
traduzione viene pubblicata col consenso dell'autore.

### Costruzione di mondi, passo passo - note di un autore di fantascienza

* Perché costruiamo mondi?
    - Fa tutto parte del narrare.
    - Molte storie sono ambientate nel nostro stesso mondo, utilizzando la
      realtà già disponibile, che ci circonda.
    - Ancora più storie sono ambientate nel passato, usando ambientazioni molto
      diverse dalla nostra, ma che possono essere studiate dalla documentazione
      storica disponibile: chiamiamo questi lavori "romanzi storici", ed
      anch'esse sono già disponibili.
    * La fantascienza ed il fantasy sono abitualmente ambientati in un mondo
      immaginario secondario, a-storico.
        - Queste ambientazioni permettono storie impossibili nel nostro mondo,
          sia nel presente che in un'ambientazione storica.
        - Non sei obbligato ad inventare un mondo secondario per scrivere F/FS,
          ma se non lo fai, ti troverai con un repertorio limitato.
    * Scopi dell'usare un mondo secondario:
        * Alienazione del lettore dalla propia esperienza vissuta
            - Obbligare il lettore a riesaminare i preopri presupposti
            - Permettere all'autore di "piegare le regole", sia scientifiche che
              culturali, per scopi artistici o concettuali
            - Ci dona un viaggio in un mondo che non abbiamo mai visto prima
            * La fiction è scritta per un pubblico, ed il pubblico (qui ed oggi)
              è _WEIRD_
                * Siamo prodotti di ciò che gli psicologi sociali definiscono
                  una società _WEIRD_ - "Western, educated, industrialized, rich
                  and democratic" (Occidentale, istruito, industrializzato,
                  ricco e democratico) - e questa è un'anomalia.
                    - La democrazia non è la norma storica sotto la quale la
                      maggior parte delle persone hanno vissuto, compresi i
                      secoli passati
                    - Gossomodo il 95% del benessere/accumulazione del capitale
                      è avvenuto negli ultimi 200 anni
                    - L'industrializzazione è un fenomeno del mondo post-1712
                      (primi motori atmosferici)
                    - L'istruzione era abitualmente per gli aristocratici ed i
                      sacerdoti
                - Chiamiamo la nostra cultura prevalente "occidentale" ed il
                  nostro presupposto inespresso è che essa sia la "norma"
            * Notare che anche la finzione storica piò fungere allo scopo di
              alienare - le persone vedevano il mondo diversamente nel passato,
              ed agivano di conseguenza
                * Talvolta gli autori prendono un periodo storico e ne "limano
                  via i numeri di serie" - il percorso di resistena minima
                    - Esempio: I libri di _Videssos_ di _Harry Turtledove_.
                      Ambientati in un mondo secondario immaginario modellato in
                      modo molto simile all'impero Bizantino (ha un dottorato di
                      ricerca in storia Bizantina), con i nomi cambiati ed un
                      po' di magia in più.
                * Alcuni periodi storici ci sono comunque incredibilmente
                  alieni: non è necessario cambiare nomi, basta prenderli per
                  come sono
                    - Esempio: l'impero Azteco (fornisce l'ambientazione per la
                      trilogia "Obsidian and Blood" di _Aliette de Bodard_
                      (storie di investigatori Aztechi nelle quali gli dei
                      Aztechi e la loro magia è reale ...)
            * Talvolta otteniamo alienazione proiettando la nostra cultura
              _WEIRD_ in posti dove non siamo mai stati
                - Esempio: _Star Trek_ - nel quale i presupposti culturali dei
                  liberali Americani vanno fino a dove nessun Americano liberale
                  è mai giunto prima
                - Usato per esaminale la dissonante l'intersezione fra il nostro
                  modo di pensare e quello di altre culture immaginarie, OPPURE
                - Esaminare come la nostra cultura _WEIRD_ può affrontare le
                  nuove tecnologie o gli sviluppi sociali - sviluppando il "se
                  questa cosa continuasse così..."
            * Occasionalmente estrapoliamo culture non-_WEIRD_ all'interno del
              nostro contesto
                - Esempi: "Alba Rossa", "Arslan", racconti su
                  invasioni/conquiste aliene
                - (Questo era un terreno particolarmente fertile durante i
                  giorni paranoici della Guerra Fredda, ora in declino)
    * Esame al microscopio della condizione umana
        * Le quattro principali fonti di conflitto nelle narrazioni:
            * Persona contro persona
            * es. quasi in ogni narrazione di supereroi
                - (Le storie di supereroi si basano su classici archetipi
                  mitologici)
            * Persona contro Natura
                - es. "Il vecchio e il mare" (classici)
                * eg. "The Martian" (FS)
                    * Nella FS, la "Natura" non deve per forza essere naturale,
                      ma può essere un ambiente/situazione non umana in cui la
                      sopravvivenza è messa in dubbio:
                        - "2001: Odissea nello Spazio"
                        - "Ringworld"
            * Persona contro Società
                - es. "1984", "Noi", "Hunger Games", qualsiasi distopia che
                  possiate nominare
            * Persona contro Sé
                - "Morire dentro" (Robert Silverberg), "Glasshouse" (Charles
                  Stross)
                - Nella FS, Persona contro Sé può essere una lotta metaforica
                  (come nella letteratura classica) od una vera lotta mediata da
                  potenziamenti immaginari (es. telepatia, impianti cerebrali)
        * I conflitti ci permettono di testare la resistenza della nostra
          umanità (attraverso protagonisti fittizzi)
            * Possiamo giocare con protagonisti eroici
                - Per appagamento dei desideri o fuga
            - Possiamo giocare con protagonisti di dimensioni umane in
              ambientazioni di dimensioni non umane
        - I mondi immaginati ci forniscono nuove arene in cui testare la
          resistenza
        - Mi concentrerò sui mondi piuttosto che i personaggi in questa
          presentazione, perché l'argomento è la costruzione di mondi - ma non
          dimenticate che in una narrazione i vostri personaggi riflettono e
          rifrangono il mondo che il circonda.
    * Il mondo è un personaggio nella FS/F
        * Talvolta la storia è sul mondo tanto quanto lo è sui personaggi
            - (Questa è una variante della storia "Persona contro Natura")
            - Esempio: "Il Signore degli Anelli" come diario di viaggio
    * Raramente: Storie in cui l'umanità non appare mai (o è esplicitamente
      estinta)
        - Molto difficile da scrivere - i lettori hanno bisogno di un punto di
          vista con cui empatizzare
        * È difficile per un autore instillare emaptia per un punto di vista
          non-umano
            - Più facile con gli animali pelosi (es. "La collina dei conigli)
* Come costruire un mondo
    * Prima decidi di cosa hai bisogno dal tuo mondo immaginato
        * Puoi raccontare la tua storia nel mondo odierno? Se sì, hai davvero
          bisogno di inventare un mondo per essa?
            - Una storia di una rapina in banca su una stazione spaziale che non
              fa alcun uso dell'ambientazione o degli effetti di essa sui
              personaggi potrebbe benissimo essere ambientata sulla Terra. Se si
              basta sul tradizionale scassinare usando strumenti contemporanei,
              non ha nemmeno bisogno di essere FS, e l'ambientazione FS diventa
              in realtà una distrazione dalla storia.
            - La cerca di un cavaliere in un mondo medievale non ha bisogno di
              magia, mostri, e nomi conl'apostrofo nel mezzo per essere una
              storia soddisfacente - se inquadrata in un contesto storico.
        - I mondi immaginati dovrebbero aggiungere qualcosa alla storia;
          qualcosa senza la quale la storia non potrebbe funzionare.
    * Che elementi di variazione della storia richiede un mondo immaginato?
        * Cambiamenti alla condizione umana
            * Una storia che esamina li razzismo/sessismo ponendo che tutti gli
              umani condividano colore della pelle/simili caratteristiche oppure
              modificando delle variabili
                - es. "La falce dei cieli" (Ursula LeGuin)
                - es. "Ammonite" (Nicola Griffith)
            * Una storia in cui l'aspettativa di vita umana è enormemente lunga
                - es. "Back to Methuselah" (George Bernard Shaw)
            * Una storia in cui l'abilità di provare emozioni è stata eliminata
              dalla psiche umana
                - es. "War Games" si Brian Stableford
            * Una storia in cui gli umani sono organismi eusociali, menti di
              gruppo, o comunque subiscono modifiche nella loro identità
                - "Hothouse" (Brian Aldiss)
                - "L'alveare di Hellstrom" (Frank Herbert)
                - "Ancillary Justice" (Ann Leckie)
            - Qualsiasi cambiamento biologico o cognitivo che modifichi il modo
              in cui il protagonista vede le cose
        * Cambiamenti all'ambiente umano
            * Una storia ambientata in un mondo sovrappopolato
                - es. "Tutti a Zanzibar" di John Brunner
                - es. "A Torrent of Faces" di James Blish
            * Una storia ambientata in un mondo post-disastro
                - es. "Il serpente dell'oblio" di Vonda McIntyre (e troppi altri per poter
                  essere contati)
            * Una storia ambientata su una colonia spaziale, stazione spaziale o
              "Grande Oggetto Muto" (Bug Dumb Object)
                - es. molti dei lavori di Iain M. Banks
            * Una storia ambientata in una società con politiche radicalmente
              diverse
                - es. "Il piano clandestino" (Ken MacLeod)
            * Una storia ambientata in un mondo dove l'intelligenza artificiale
              interagisce con gli umani per generare complessi risultati
              emergenti
                - es. "The Red" (Linda Nagata)
                - es. "Neuromante" (William Gibson)
            - Qualsiasi ambientazioni che cambi il mondo in cui il protagonista
              può agire
        * Cambiamenti in ambienti non-umani
            * Una storia ambientata su un altro pianeta che a malapena supporta
              la vita
                - es. "Cyteen" (C. J. Cherryh)
        - Quando necessario, alcune di queste categorie possono sovrapporsi;
          puoi averle tutte in una singola storia, e certamente alcune di esse
          si alimentano a vicenda: un ambiente modificato solitamente porta a
          cambiamenti nel modo in cui la gente ci vive.
    * La finzione narrativa ha richieste diverse - restrittive - da quella
      visiva o di altri media (es. musica, giochi)
        - Non puoi mostrate informazioni visive, testuali o somatiche senza
          descriverle
        - Questo può interferire con il ritmo di una narrazione
        * Come scorciatoia per lo scrittore, è meglio affidarsi al "mostra, non
          raccontare" - evitare gli infodump (scarico di informazioni)
            * Un infodump interrompe lo scorrere della narrazione per sganciare
              un indigeribile boccone di spiegazioni nella storia.
                - (Se hai letto "Dune", quelle voci da "Enciclopedia Galattica"
                  all'inizio di ogni capitolo sono classici infodump. Lo stesso
                  per ogni episodio della serie classica di Star Trek che inizia
                  con il "Diario del capitano" per introdurre la puntata.)
                - Un parente stretto degli infodump nudi e crudi sono i "Mi
                  dica, Professore": come in, "mi dica, professore, come
                  funziona l'ecosistema di questo meraviglioso nuovo
                  pianeta...?" Il quale è solitamente seguito da un monologo di
                  un ora, che si differenzia da un infodump solo per le
                  virgolette che lo racchiudono.
            * "Mostrare, non raccontare" porta le informazioni nella narrazione
              con il contagocce, ad es. inserendo pezzi di colore di sottofondo
              tramite aggettivi o discorsivamente. Quando fatto bene è senza
              soluzione di continuità. O può venire astutamente cammuffato in un
              mi-dica-professore al contrario.
                - Un classico è incipit di "La spiaggia d'acciaio" di John
                  Varley (vinse il premio nebula nel 1991): "Entro due anni il
                  pene sarà obsoleto, disse il commerciante". Questo fornisce
                  l'implicazione secondo cui la storia sia ambientata in una
                  società con un'avanzata e facile ingegneria biologica ed un
                  approccio molto diverso alla sessualità; prima ancora di aver
                  letto per intero la prima frase!
    * L'autore deve conoscere il proprio mondo più del lettore
        - Gli osservatori sono sensibili alla cornice che circonda il quadro
        - In modo simile, un lettore otterrà un senso di complesità e profondità
          se del mondo c'è più di quanto l'autore gli mostra - la prosa
          dell'autore implicherà profondità
        * Parte del lavoro del lettore nell'asismiliare un testo FS o Fantasy è
          decodificare le regole base sulle quali la storia si fonda
            - Come in un giallo: ma gli indizi riguardano la natura del mondo
              invece dell'identità dell'assassino
            - Il mondo è un personaggio in sé
            - Quando scrivi narrativa devi generalmente sapere dei personaggi
              più di quanto racconti al lettore: i mondi immaginari non sono
              diversi
            - Se non conosci gli antefatti della famiglia del protagonista, la
              tua descrizione delle loro reazioni quando incontrano altre
              persone potrebbe sembrare contradditoria o arbitraria ai lettori.
            - Devi sviluppare una teoria della mente per i tuoi personaggi.
              "Cosa farebbe X?" è più facile da capire se sai che genere di cose
              normalmente fa X.
            - Analogamente, devi sviluppare una teoria della mente per il tuo
              mondo immaginario: come accoglie gli sviluppi della tua storia?
    * Quali sono le mie regole per costruire un mondo:
        - C'è più di un modo per farlo - questo è quello che funziona per me
        * Dall'alto verso il basso
            * Quando sai che genere di storia vuoi raccontare, avrai qualche
              idea sulla dimensione del mondo che vorrai costruire
                - Alcune storie richiedono solo una città (es. "Perdido Street
                  Station" di China Mieville)
                - Alcune storie richiedono un singolo stato delle dimensioni del
                  Regno Unito
                - Alcune storie richiedono interi continenti con diverse nazioni
                  e varie regioni geografiche e condizioni climatiche
                - Alcune storie richiedono interi pianeti
                - Alcune storie richiedono molti mondi
            - Concentra gli sforzi sulle parti più importanti - quelle che i
              tuoi personaggi vedranno di più - ma sìì consapevole di come sfuma
              lo sfondo dietro a loro
        * La consistenza interna è vitale
            - Questo non significa uniformità; come ha sottolineato William
              Gibson, "Il futuro è già qui, solo che è irregolarmente
              distribuito".
            - Un viaggiatore del tempo del presente, andando negli anni 40 non
              troverà segni di computer digitali, motori jet, missili balistici
              o programmi d'armi nucleari, ma tutte queste cose esistevano o
              erano in frenetico sviluppo militare e sarebbero stati lontani
              dalla coscienza del pubblico per un altro decennio o due.
            * Piuttosto, significa che se andrai a modificare le regole, dovrai
              cambiarle in modo consistente.
                - Ad esempio, scrivendo una space opera, probabilmente vorrai
                  avere i viaggi a velocità super-luce. Ma sarà d'aiuto annotare
                  qualche numero sul retro di una busta: quanto più veloce della
                  luce può viaggiare un'astronave, quanto sono distanti le
                  stelle attorno a cui orbitano i pianeti che i protagonisti
                  intendono visitare, quanto è il tempo necessario per viaggiare
                  tra di essi.
                - Una volta che hai questi parametri, applicali: se sono
                  necessarie sette ore per viaggiare da Heathrow a New York con
                  un jet, allora se il tuo protagonista avrà bisogno di tornare
                  indietro in tre ore soltanto, dovrai introdurre il Concorde
                  come eccezione speciale, e giustificarlo.
        * A meno che tu non scriva deliberatamente surrealismo, la mancanza di
          consistenza interna infastidirà i tuoi lettori.
            - Dopotutto, hai dato loro un puzzle (il mondo) da risolvere: una
              storia senza consistenza interna è come delle parole crociate
              senza soluzione.
        * La complessità aggiunge profondità
            - Il "piccolo pianeta agricolo" è un cliché nella FS, ed è falso:
              quanti biomi può sopportare la Terra? Quante industrie?
            - Gli ecosistemi evoluti sono complessi. Non hai mai solo una specie
              di erbivori ed una di carnivori... a meno che non sia arrivato
              qualcosa a sfrondare radicalmente l'ecosistema uccidendo tutto ciò
              che c'era prima. Come una battuta di caccia.
            - Le economie sono complesse. Prendi l'industria metallurgica a Port
              Talbot. La cifra nominale di posti di lavoro persi se l'industria
              metallurgica chiudesse è 4000, ma questi 4000 redditi primari
              vanno a sostenere una serie di imprese locali - il numero stimato
              di posti di lavoro persi se l'impianto chiudesse è vicino ai
              30.000, che vanno dai negozi di quartiere alle ditte di taxi. Non
              c'è qualcosa come una singola industria in una regione, a meno che
              non si tratti di contadini autosufficienti dell'età del ferro che
              non commerciano a lunga distanza e non usano denaro. (Chiamiamo
              questo genere di geografia politica "i secolu bui" per una
              ragione.)
        - "La storia è l'arma segreta dell'autore di fantascienza" - Ken MacLeod
        * "La storia si ripete, prima come tragedia, poi come farsa" - Karl Marx
          (famoso MacLeodista)
            - Se vuoi costruire un mondo credibile, prendi in considerazione di
              rubare da due incidenti storici e rimescolarli per aggiungere
              complessità
            - Il mondo reale è incline ad episodi surreali. Feste sontuose in
              cui circola cocaina su vassoi d'argento trasportati sulla testa da
              nani, dittatori che cambiano il nome dei giorni della settimana
              con il proprio (tranne uno, a cui danno quello della propria
              suocera), il Paraguay che si prepara ad una guerra di conquista
              planetaria invadendo contemporaneamente Brasile, Argentina e
              Uruguai - la storia e gli affari umani non sono obbligati ad avere
              un senso.
        * La condizione umana non è costante ed universale; il progresso non ha
          una direzione e non è irreversibile
            - Come indicato prima, noi che siamo di questo pubblico, siamo
              _WEIRD_; le nostre esperienze sono incompresibilmente aliene dalla
              prospettiva di qualcuno del 1716.
            - Questi cambiamenti non sono invariabilmente progressivi: considera
              la resistenza agli antibiotici come freno per gli impianti
              cerebrali così popolari nella narrativa cyberpunk (chi fra voi
              vuol essere il primo ad installare nella propria testa una
              meningite resistente agli antibiotici solo per poter fare un
              aggiornamento dell'interfaccia utente?)
            - Ancora, considera com'è essere una donna che vive in Siria o Iraq
              oggi rispetto all'oriente di tre decenni fa (in stati mono-partito
              nominalmente secolari).
- Questo è stato un giro veloce su alcuni dei punti salienti del costruire mondi
  scrivendo fantascienza, ed abbiamo appena scalfito la superficie
  dell'argomento. Ora il tempo è finiro; grazie.

[1]: https://it.wikipedia.org/wiki/Charles_Stross
[2]: http://www.antipope.org/charlie/blog-static/2016/04/some-notes-on-world-building.html
