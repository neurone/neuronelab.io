title: Platone, gli animali e la guerra
date: 2015-06-18 18:50:35 +0200
tags: filosofia, etica
summary: Da "I filosofi e gli animali"

Da "[I filosofi e gli animali][1]"

In [Platone][2], la gerarchia dei viventi non è una scala, ma una immensa ruota
[empedoclea][3] con azione reciproca basso-alto / alto-basso: le anime compiono
percorsi di caduta e di ascesa e la loro liberazione può avvenire solo
attraverso l’esercizio della giustizia in questo mondo. Con l’uccisione degli
animali sono penetrate nella vita dell’uomo ingiustizia e guerra (analoga tesi
in [Pitagora][4], [Teofrasto][5] e [Porfirio][6]).

*La guerra è una forma di caccia, ma il cacciato è l’uomo. La caccia è
propedeutica alla guerra, serve ad abituare al sangue, allo sforzo necessario
per uccidere. Tutte le tecniche della caccia sviluppano doppiezza e spirito
d’inganno, aspetti che sono agli antipodi delle virtù che la Polis della
Giustizia esige dai suoi cittadini.  
Nell’ordine politico dell’ingiustizia e della sopraffazione, i sacrifici
di sangue, il mattatoio, la violenza fisica e quella del denaro erette a
leggi sono iscritte in un unico ordine degenerativo.*

[1]:http://www.agireoraedizioni.org/libri/animali/filosofi-animali/
[2]:https://it.wikipedia.org/wiki/Platone
[3]:https://it.wikipedia.org/wiki/Empedocle
[4]:https://it.wikipedia.org/wiki/Pitagora
[5]:https://it.wikipedia.org/wiki/Teofrasto
[6]:https://it.wikipedia.org/wiki/Porfirio
