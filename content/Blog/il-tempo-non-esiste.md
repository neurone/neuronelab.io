title: Il tempo non esiste
date: 2014-09-02 19:36:18 +0200
tags: filosofia, scienza, fisica
summary: Usando un approccio riduzionista, si potrebbe dire che il tempo non esiste e che quello che noi chiamiamo tempo, non è che la percezione sequenziale di semplici variazioni della materia nello spazio.

Usando un approccio [riduzionista][1], si potrebbe dire che il tempo non esiste e che quello che noi chiamiamo tempo, non è che la percezione sequenziale di semplici variazioni della materia nello spazio.

Movimenti, mutazioni, spostamenti di oggetti, anche il classico muoversi della
lancetta dell’orologio non è altro che movimento di materia causato da altri
movimenti di materia, che siano le oscillazioni di un quarzo o il muoversi di un
meccanismo a molla. Il battito del cuore, il cadere di una goccia, fino al
fiorire causato dalle variazioni nella materia che compone la pianta. Ciò che
include e che sta fra una variazione e l’altra, lo consideriamo come un continuo
e lo chiamiamo tempo. La nostra misurazione del tempo consiste nel trovare quali
cambiamenti della materia avvengono simultaneamente ad altri che noi abbiamo
deciso di usare come riferimento.

Ipotizziamo che, simultaneamente, tutte le variazioni della materia
dell’universo, accelerino o rallentino. La nostra percezione del tempo,
rimarrebbe, rispetto ad esso, invariata e noi non noteremmo nulla. Per usare
un'analogia, se chiedessi a qualsiasi persona se i buchi esistono, questa non
avrebbe alcuna esitazione nel dirmi che sì, esistono. Ma cos’è un buco? Un buco
è la mancanza di materia delimitata dalla presenza di materia. Ed essi sono
definiti proprio dall’assenza di essa, ma non hanno un’esistenza propria ed
indipendente. Il concetto di buco, quindi, è un’idea, o un’intuizione istintiva,
che definisce un qualcosa che, oggettivamente, non esiste. Possono esistere gli
istanti, configurazioni particolari della materia che a noi paiono salienti, ma
può non esistere il tempo come assoluto, perché il tempo non è necessario al
funzionamento dell’universo. Tutto quello che c’è fra una variazione percepita e
l’altra, potrebbe essere considerato alla stregua di un buco: un qualcosa di
mentale, un’astrazione che esiste solo in relazione a qualcos’altro. Togli
questo, ed ottieni che rimangono solamente variazioni di configurazione della
materia.

Nel 2012, al *festival delle scienze* di Roma, il tema principale era stato
proprio il tempo. Sul [sito dell'evento][2] sono state pubblicate le lezioni di
vari scienziati e teorici, scaricabili in formato MP3.

Trascrivo l'audio della lezione del fisico teorico [Carlo Rovelli][3].
Consiglio anche l’ascolto del dialogo con [Julian Barbour][4] e Giuseppe
Giorello (attenzione che, sulla pagina, il file con l’audio originale e quello
con la voce doppiata dall’interprete, sono invertiti).
<!--more-->

Il tempo non esiste
-------------------

Grazie di essere venuti. Buongiono. Buonasera. Parliamo di tempo, e in questa
chiacchierata cercherò di raccontarvi, prima quello che io, nel mio mestiere di
fisico, credo di capire sul tempo, su cos’è e sulla sua struttura, e poi
soprattutto anche quello che so di non capire ancora sul tempo.

La chiacchierata si articola in quattro capitoli e l’ultimo sarà molto breve.
Cominciamo dal capitolo uno. Questa è una linea, una lunga linea dritta, e
questo è come noi immaginiamo il tempo. Il tempo è una successione di momenti,
di eventi, di giorni, di anni, di minuti. Qui siamo ad adesso, ad esempio, qui è
dieci minuti fa quando siete entrati, qui è ieri quando io sono arrivato a Roma
con l’aereo, l’anno scorso, quando sono nato, l’inizio della terra, l’inizio
dell’universo… Qua c’è il futuro: domani, eccetera, eccetera, fino all’infinito.
Ogni punto rappresenta un momento del tempo. Ad esempio, fissiamone uno, poi lo
confronto con gli altri: io alzo il braccio sinistro, e diciamo che eravamo qua.
Ecco, questo è il momento in cui ho alzato il braccio sinistro. Passa del tempo…
un evento che dura del tempo, ad esempio questa lezione, copre un pezzo di
questo tempo che passa. Ad esempio, fra mezz’ora, quando avrò finito di parlare,
più o meno, saremo quà. Questo è l’intervallo di tempo fra qui e qui. Il tempo
lo misuriamo con degli orologi: “Ho iniziato alle cinque, quando avrò finito di
parlare saranno più o meno le cinque e mezza.” Fatemi prendere un orologio.
Questo segna le cinque. Credetemi anche se non lo vedete. Qui, nel momento in
cui inizio a parlare, sono le cinque. Il motivo di tutto questo è che, pian
piano, qualcosa cambierà in quest’immagine. Quando finirò di parlare saranno le
cinque e mezza: questo orologio segnerà le cinque e mezza.

Noi viviamo dentro questo tempo. Adesso non siamo più qui, è passato un po’ di
tempo da quando ho alzato il braccio. Noi ci muoviamo lungo questo tempo. Il
temo scorre per noi, ed il tempo è cruciale per noi. Noi siamo il tempo, in
qualche maniera, abitiamo dentro il tempo. È nella nostra esperienza immediata.
Talmente tanto che possiamo facilmente immaginare un mondo senza degli oggetti,
senza le cose, addirittura senza lo spazio, se ci pensiamo sono i nostri
pensieri, ma è molto difficile pensarlo senza il tempo, perché la nostra
diretta, immediata, esperienza del mondo, è il tempo che passa. Per poter
pensare il mondo, dobbiamo pensare che i pensieri avvengono nel tempo, uno dopo
l’altro. Per cui il tempo è la nostra casa ed è organizzato in questa lunga
linea. Nell’induismo indiano, quest’idea è espressa da Shiva che danza. Shiva,
nella sua danza, fa vivere l’universo e da il ritmo lungo cui avviene
l’universo. Questo ritmo, questa successione, questa lunga linea che va dal
passato verso il futuro, è il tempo. Questo è il capitolo primo.

Capitolo due. Se il tempo è fatto così, abbiamo capito tutto. Abbiamo capito che
struttura ha, come ci siamo dentro e com’è fatto, sostanzialmente. Bene: questa
descrizione del tempo che ho appena dato, non è giusta. È sbagliata. Non è
sbagliata per qualche considerazione filosofica sottile o per complicate
elaborazioni teoriche. È proprio sbagliata, punto. Sbagliata come se io dicessi:
“La terra su cui siamo poggiati è ferma”. È una immediata percezione che abbiamo
dalla realtà. Molto semplice, molto diretta, molto immediata: la terra è ferma.
Sappiamo che non è ferma, che si muove. “Eppur si muove”, diceva Galileo, perché
gira, gira intorno al Sole, gira su se stessa, eccetera. Ed io posso misurare la
terra che gira. Ad esempio col pendolo di Focault. Appendo un grosso pendolo che
continua a pendolare a lungo su un piano, e poi, lentamente, questo piano gira.
Prima pendolerà di così, poi così e poi così… molto lentamente. Perché gira?
Beh, non è che gira, è la Terra che gli gira sotto, sostanzialmente. Il pendolo
sta sempre sullo stesso piano. Quindi c’è un piccolo effetto che ci mostra che
la nostra idea che la Terra stia ferma è falsa. Poi, gli astronauti vanno sulla
Luna, guardano la Terra e la vedono girare, proprio. Non c’è dubbio che la Terra
giri, e non c’è dubbio che le nostre percezioni sull’immobilità della Terra son
sbagliate, anche se sono così dirette, immediate. Quello che io credo
soprattutto interessante nella scienza è proprio il fatto che corregge le nostre
immediate percezioni del mondo, o la nostra struttura concettuale per pensare il
mondo e ci racconta che il mondo è fatto un po’ diverso.

Un altro esempio, che forse è collegato, ma più vicino. Se io faccio cadere un
oggetto, cade e traccia una linea… se io faccio cadere un altro oggetto qui,
cade e traccia un’altra linea… due rette. Queste linee sono parallele? Voi
direte: “Sì, tutte le cose cascano.” Invece, evidentemente no, perché la Terra è
una sfera, ed ogni cosa che casca deve cascare ciascuna ad un angolo, rispetto a
quella vicina. Voi direte: “Ma fra qui e qui, l’angolo è piccolissimo.” Certo, è
piccolissimo, infatti è molto difficile da misurare. Se io faccio cadere due
cose, io le vedo cadere perfettamente parallele, ma questa piccolissima
discrepanza fra le parallele e come cadono i due oggetti è proprio quella che ci
dice che la Terra è tonda. Se veramente cascassero parallele, vorrebbe dire che
la Terra è piatta, all’infinito, e quindi avrei un’immagine del mondo che è
completamente sbagliata. Quindi, piccoli dettagli, ci possono cambiare in
maniera radicale l’immagine generale del mondo.

Torniamo al tempo. Perché è sbagliata quest’immagine? Perché io ho detto: “Qui
erano le cinque, qui erano le cinque e mezza. Quest’orologio senza le cinque,e
quest’orologio qua senza le cinque e mezza.” Che vuol dire, che se io prendo
quest’orologio e lo tengo fermo per mezz’ora, dopo mezz’ora segna mezz’ora dopo.
Ora, è un fatto, misurabile, anche se piccolo, che se io prendo un altro
orologio e lo metto un metro più in basso, aspetto mezz’ora e poi lo guardo, non
segna le cinque e mezza. Segna le cinque e mezza meno qualche cosa. Il tempo, è
un fatto misurabile, non passa alla stessa velocità un po’ più in alto o un po’
più in basso. La differenza è piccola, dell’ordine di un milionesimo di
miliardesimo di secondo, però è misurabile. Oggi abbiamo orologi molto più
precisi di questo mio cipollone meccanico. Orologi atomici, orologi al cesio…
orologi estremamente precisi, e si misura con relativa facilità, in un
laboratorio di fisica, che se io ne prendo due uguali, ne metto uno più alto ed
un più basso, aspetto un po’, li guardo e quello più alto è andato più avanti.
Quello più basso è andato più indietro. Quest’effetto è vero, reale e
misurabile, ed è tanto importante che nei GPS che abbiamo nelle automobili, che
funzionano con degli orologi precisi che stanno sui satelliti, è necessario
tenere quest’effetto da conto, altrimenti il GPS non funzionerebbe, perché lassù
dove ci sono i satelliti con i loro orologi, il tempo corre più veloce. Il tempo
va più lento quando mi avvicino alla Terra. Oppure, voi sapere che se prendo un
orologio e lo muovo molto velocemente, poi torno ad incontrarmi con un orologio
che, invece, è rimasto fermo, l’orologio che si è mosso resta indietro rispetto
all’orologio fermo. Per cui se io ho un compagno di scuola, un coetaneo con cui
ero insieme alle elementari, eravamo tutti e due di dieci anni, e lui va a fare
un viaggio, lontano, veloce, e torna indietro, io ho 55 anni e lui ne ha 32. Noi
non ci accorgiamo di queste grosse differenze, perché, più o meno, viaggiamo
tutti alla stessa velocità, perché nessuno viaggia molto veloce, e perché quindi
le differenze sono piccole. Quindi, il mio compagno di classe che fa il pilota
d’aereo, che viaggia veloce, ha vissuto meno di me quando ci incontriamo, ma la
differenza è di un milionesimo di secondo e non ce ne accorgiamo, guardandoci in
faccia. Però questo ci dice, che quest’idea che il tempo sia una linea, che
scorre uniforme, e tutto sta ordinato lungo questo tempo, e qui è una certa ora,
è sbagliata. In qualche maniera un orologio che si abbassa, scende, a un certo
momento, che segue un altro percorso, più in basso, e poi si riunisce a
quest’orologio qui, misura un tempo diverso. Questo vuol dire che l’immagine del
singolo tempo che passa, non va bene. È proprio sbagliata, non c’entra niente
con la realtà. È una descrizione della realtà che vale sono entro un certo
livello di approssimazione molto imprecisa. Ogni percorso nello spazio, si porta
il suo tempo. Non c’è un tempo, ci sono tanti tempi. Come faccio a tener conto
del tempo tessuto da tutti questi percorsi tessuti che passano attraverso lo
spazio? La soluzione, che è quella che i fisici usano oggi, è pensare che tutti
questi percorsi tessano una superficie che si chiama lo spaziotempo.

Ora, voi mi direte: “Professore, era proprio necessario portare un lenzuolo da
casa per mostrare questa cosa? E, professore, se anche avesse dovuto portare un
lenzuolo, per lo meno poteva portare la parte di sopra del lenzuolo e non la
parte di sotto, che è tutta storta? Che non si riesce neanche a mettere, che fa
le curve, fa i buchi…” La risposta è che ho voluto portare la parte di sotto del
lenzuolo perché lo spaziotempo non è una superficie piana, ma una superficie
curva. Quello che abbiamo imparato, ormai da un secolo, dal 1915, da quando
Einstein, per primo, ha scritto le equazioni che descrivono questo spaziotempo,
è che questa superficie non la posso pensare come una tela piana, ma è una tela
di montagne, di buchi, in cui gli oggetti stessi curvano lo spazio e ne fanno
delle tasche, delle curve, esattamente come un lenzuolo di sotto, che è curvo.
Non riesco a metterlo dritto. Ne avanza sempre un pezzo se cerco di tenderlo. E
così lo spaziotempo è curvo. Ci sono dei passaggi che sono delle scorciatoie,
dei passaggi più lunghi come la superficie delle montagne. Quello che Einstein è
riuscito a fare è scrivere le equazioni che descrivono lo spaziotempo, come si
curva, e come la presenza di oggetti, un orologio, una massa, il sole, la Terra,
curvano lo spazio. La Terra curva lo spaziotempo, ed è per questo che un
orologio più vicino alla Terra, rallenta. Fa come un rigonfiamento nello
spaziotempo, ed è per questo che rallenta il tempo.

Questa curva, questi buchi, questi avvallamenti, possono essere talmente grandi
che, secondo l’equazione di Einstein, si può addirittura bucare. Questi buchi,
che si chiamano buchi neri, di cui avete certamente sentito parlare, quando io
ero studente i buchi neri si studiavano come soluzioni all’equazione di
Einstein, e c’era scritto sui libri e lo dicevano i miei professori che nessuno
si immagina che esistano davvero, sono solo oggetti matematici che saltano fuori
da quest’equazione. Oggi sono passati solo 25 anni dai miei corsi, o qualcosa di
simile, e i buchi neri, nel cielo li vediamo. Questa è un’immagine artificiale:
cosa vedrebbe uno, se guardasse attraverso un buco nero. Questa è una vera
immagine, anche qua non si vede molto bene, ma se cercate in internet ne vedrete
molti. Ci sono dei buchi neri nel cielo, che abbiamo visto e stiamo studiando.
Ce n’è uno enorme nel centro della galassia. E sono reali. Quindi, lo
spaziotempo effettivamente è una cosa tutta curva, con i buchi, e questa è la
struttura dello spazio e del tempo. E questo, chiude il capitolo due.

Capitolo tre. Ancora una volta, beh, abbiamo capito tutto: il tempo non è una
cosa lineare, è una cosa più complicata, bisogna andare a scuola,
all’università, bisogna imparare un po’ di matematica ma, in fondo, tutte le
università italiane hanno un corso di relatività generale, ci sono i libri,
basta un po’ di coraggio, mettersi lì, uno studia e capisce tutto sullo
spaziotempo. Non devo pensare ad una linea, è una superficie, si curva, si
piega, ha i buchi, ci sono le equazioni di Einstein che lo descrivono… È tutto
chiaro. No. Non è tutto chiaro. Perché? Perché come Einstein stesso, dopo aver
fatto la teoria della relatività, nel 1915, novantasei anni fa, due anni dopo ha
scritto un articolo, lui per primo, dicendo “ovviamente questa teoria non può
coprire tutta quella è che la realtà dello spazio e del tempo, perché trascura
qualcosa di fondamentale – che si stava scoprendo proprio in quegli anni – che
sono le proprietà quantistiche- come si dice adesso – dello spazio e del tempo,
i quanti”. Nel ventesimo secolo, la fisica, ha fatto delle grandi scoperte. Una
è proprio la struttura tutta curve e storta dello spaziotempo, l’altra è la
meccanica quantistica. E cos’è questa scoperta? È la scoperta che tutti gli
oggetti – gli oggetti fisici, ma anche i campo elettrico, il campo magnetico, e
anche lo spaziotempo – visti a piccola scala, hanno una struttura granulare e un
comportamento random, casuale. Si muovono come da leggi guidate alla
probabilità, invece che da leggi precise. Non voglio parlavi della meccanica
quantistica, è un grande argomento complesso. Il punto è che questa immagine
dello spaziotempo, è un’immagine approssimata. Pensare allo spaziotempo come una
cosa continua, con le sue curve e i suoi buchi, è un’immagine che sappiamo che
non va bene se la guardiamo a piccola scala. Che succede a piccola scala?
Succede che anche lo spaziotempo deve avere una struttura granulare e una
struttura fluttuante. Immaginate: questo è lo spazio, lo guardo un po’ più da
vicino, prendo una piccola regione, la ingrandisco, la ingrandisco ancora, la
ingrandisco ancora, continuo a guardarla sempre più nel piccolo e quello che
penso che vedrei è tutta una struttura complicata. Questa è un’altra immagine.
Immaginate che questo è il lenzuolo spaziotempo, se lo guado più da vicino vedo
che è rugoso, ed ancora più da vicino è tutto fatto di bolle e bitorzoli. Quindi
quello in realtà è lo spaziotempo visto a piccola scala. In realtà, se ci
pensate, non è poi così strano, perché anche questo lenzuolo ha una struttura.
Sono dei fili, sono tutti dei fili intrecciati. Se lo guardo anora nel più
piccolo, è fatto di atomi. Bene, lo spaziotempo è questa cosa, continua se vista
su grande scala, ma su piccola scala è fatta di una struttura fine, minuta e
soprattutto tutta fluttuante. Ma allora, se lo spaziotempo lo devo pensare così,
il tempo qua come cammina? Sale per così… poi fa un giro, torna indietro…
scende… poi entra in un buco, gira intorno… Chiaro che l’idea di un tempo che
passa diventa molto complicata da pensare se non ho né una linea, né una
superficie.

E quindi cosa fanno i fisici, oggi? Cercano di descrivere le equazioni di questo
spaziotempo quantistico, con la sua struttura piccola, rinunciando del tutto
all’idea del tempo. Dimentichiamoci quest’ordine delle cose lungo il tempo, sia
lineare, sia sulla superficie, e scriviamo delle equazioni senza tempo. Che
significa? Qui viene, un po’, il discorso centrale di questa chiacchierata. Che
vuol dire, fare la fisica senza tempo? Cerchiamo di tornare all’osso della
questione. Com’è possibile, e che significa, parlare del mondo, pensare al
mondo, senza pensare al tempo che corre. Per prima cosa vi voglio far vedere
che, in realtà, è molto più semplice di quello che suona, a prima vista, quando
uno dice “faccio la scienza senza tempo”.

Torniamo all’osservazione fondamentale. Cosa vuol dire quando pensiamo al tempo
che passa. Prendiamo due eventi. Alle cinque ho alzato la mano sinistra, alle
cinque e mezza alzo di nuovo la mano sinistra. Fra questo evento, le cinque, e
questo evento, le cinque e mezza, è passata mezz’ora. Che vuol dire? Vuol dire,
per esempio, che l’orologio, qui, misurava le cinque e che l’orologio, qui,
misurava le cinque e mezza. Ma come faccio ad essere sicuro che quest’orologio è
giusto? Il vecchio cipollone meccanico. Beh, lo guardo con orologio più preciso,
elettronico, che costa molto meno e va molto meglio, e vedo che è giusto. Ma
come faccio ad essere sicuro che questo è giusto? Lo controllo col segnale
orario. Come faccio ad essere sicuro che il segnale orario è giusto? So che è
controllato con un orologio atomico, precisissimo, al cesio, super-preciso,
eccetera, eccetera… Ma qui stiamo parlando di tempi minutissimi, molto più
precisi dell’orologio d’oro. Ma come faccio a sapere che quell’orologio atomico
è giusto? Lo confronto con un altro orologio ancora più preciso.

Chiaro che c’è un problema… Qual’è il problema? Il problema è che io posso solo
vedere… posso costruire degli orologi, che sono tutti dei pendolini, alla fine.
Posso controllare un orologio con un altro orologio. Posso vedere se un insieme
di orologi è coerente nel definire il tempo. Ma come faccio se veramente misura
il tempo o qualcos’altro? Ma che significa misurare il tempo senza un orologio?

La chiarezza su questo punto, la si trova nel libro di Newton, che per primo ha
messo tutto in ordine. E Newton dice, in maniera chiarissima: “Vedete, il tempo,
noi, mica lo vediamo. Non lo misuriamo. Noi misuriamo posizioni di lancette di
orologio, posizione del sole nel cielo, posizione del braccio che si alza, si
abbassa… posizione di un pendolo, l’angolo. Tutte queste variabili cambiano e
quando una cambia, l’altra cambia. Per mettere ordine e descrivere tutte queste
variabili, è comodo introdurre una cosa che chiamiamo la variabile tempo e
riferire tutto al tempo.” Per cui noi diciamo “Alle cinque il braccio si è
alzato e la lancetta stava là. Alle cinque e mezza il braccio s’è alzato e la
lancetta stava là.” Ma in realtà, “le cinque” non c’è. L’unica cosa che vediamo
è che quando la lancetta stava là, il braccio stava qui e quando la lancetta
stava la, il braccio stava qui. Quindi, quando noi osserviamo il mondo, vediamo
solo cose, posizioni di oggetti, valori di variabili, posizioni del braccio,
posizioni di lancette, posizione del sole, eccetera, come cambiano le une
rispetto alle altre. Ed è tutto. Il tempo lo aggiungiamo noi.

Allora, è chiaro che in linea di principio io posso fare a meno di mettere il
tempo e scrivere delle equazioni che ci dicono solo come cambiano le cose, una
rispetto all’altra, senza il tempo. Ed è chiaro che se questa cosa è possibile
nella fisica newtoniana del tempo lineare, questa cosa diventa necessaria quando
il tempo è fatto così, quando lo spaziotempo è fatto così, in cui cercare di
mettere ordine, di mettere una variabile lì, non serve, diventa troppo
complicato. È molto più semplice pensare al mondo, rinunciando del tutto
all’idea di un tempo che passa, e i mondo lo pensiamo senza tempo, con delle
variabili collegate una all’altra. E quindi, in qualche maniera, questa è
un’immagine dello spaziotempo… sono tutti disegni, ovviamente… della struttura
minuta, e non possiamo pensare un tempo che passa lungo questi fili, perché è
troppo aggrovigliato. Quindi, invece di pensare a tanti tempi che scorrono,
pensiamo – questa è una metafora – “Sono sparite tutte le lancette. Non ci sono
più i tempi”.

Quindi, riassumendo. È possibile pensare al mondo senza tempo semplicemente
descrivendo quello che vediamo, senza parlare di tempo, e dicendo: c’è
concomitanza fra le lancette dell’orologio e questa posizione del braccio e le
lancette dell’orologio e questa posizione del braccio. È necessario fare questo
quando descriviamo quello che succede su scala piccolissima, dove cercare di
mettere ordine con il tempo non funziona più. È come se invece di Shiva che
danza e tiene tutto quanto in ordine, la visione della realtà che ci da la
fisica di oggi si frantuma in una miriade di danze particolare in cui ognuno
danza col vicino a piccolissima scala e non c’è più un ordine globale in cui…
non c’è più Shiva che danza. Adesso, spero che Shiva non lo prenda a male… Ma
c’è una danza globale in cui non c’è più un tempo in cui tutto questo è
ordinato.

La conclusione è che, nella fisica contemporanea, molti gruppi di ricerca stanno
cercando di scrivere le equazioni fondamentali del moto senza alcun riferimento
alla nozione di tempo. E questo è il capitolo tre.

E poi c’è il capitolo quattro, che sarà molto breve. Che però, forse, alla fine,
è quello in cui ho le idee meno chiare, ma anche quello più interessante di
tutti. Perché, se rinunciamo al tempo, cioè il tempo non è più né questa linea
che scorre, né la tela, resta una descrizione del mondo in cui le cose
avvengono, ma non avvengono nel tempo. In cui c’è il cambiamento relativo delle
cose, le une rispetto alle altre, ma non c’è il cambiamento nel tempo. Quindi
non c’è più il tempo che scorre.

Io scrivo delle equazioni, immaginiamo che ci sono riuscito, il cui il tempo è
completamente sparito… Resta un problema: perché noi percepiamo il tempo? Io fra
un po’ devo smettere perché mi è stata data mezz’ora di tempo. Il tempo, per
noi, c’è. Non c’è una contraddizione frail fatto che il tempo non ci sia a
livello fondamentale, e noi che percepiamo il tempo. Pensate all’alto e il
basso. Noi abbiamo una percezione fondamentale del mondo dove c’è il su e il
giù. Sappiamo da Newton, che nell’universo non c’è l’alto e il basso. Gli
astronauti che volano fuori dalla Terra, non hanno un alto e un basso. Per loro,
tutte le direzioni sono uguali. Quindi, noi sappiamo che a livello fondamentale
del mondo, non c’è l’ alto e il basso, ma per noi c’è l’alto e il basso. Però
sappiamo come nasce, come emerge l’alto e il basso. Sappiamo che noi siamo sulla
Terra, c’è la forza di gravità che ci schiaccia, quindi qui, sulla superficietta
sopra la terra, è ben definito l’alto e il basso. Altrove, no. Quindi sappiamo
come emerge e in che situazione particolare emerge il concetto di alto e basso,
che ci serve nella nostra vita quotidiana. Domanda… e questo è il problema del
capitolo quattro… ammettiamo che, al livello fondamentale, il tempo non c’è,
l’universo non ha tempo. Domanda: “Come emerge il tempo per noi? Che cos’è
questa cosa che noi percepiamo, che è il tempo?” Bene, su questo stanno
lavorando alcuni gruppi di ricerca nel mondo, ci sono delle idee, vi voglio
parlare dell’idea che, a me, sembra più interessante. Che è questa. Noi abbiamo
una percezione del mondo che non è il dettaglio minuto dello spaziotempo, la
scala piccolissima in cui vale la gravità quantistica, la cosiddetta scala di
Planck. Non è quella schiuma di spaziotempo. Noi abbiamo un’immagine molto
grossolana del mondo. La maggior parte dei dettagli microscopici della realtà
non li vediamo. Noi vediamo solo qualcosa. Noi non vediamo il mondo nella sua
complessità, lo vediamo molto semplificato. È solo in questa semplificazione, in
questa visione approssimativa statistica, che nasce l’ordine dello spaziotempo
e, in particolare, nasce l’ordine del tempo lineare. Io credo che questa nascita
riguardi, non tanto il mondo in sé, ma il modo in cui noi vediamo il mondo, il
modo in cui noi organizziamo il mondo. Questo vuol dire, quindi, che non è un
unico Shiva che danza e che tiene su il mondo, ma siamo noi, nella nostra
percezione del mondo che creiamo il tempo che passa. Io penso, alla fine, anche
se non ho idee chiare di come, in che modo, che il tempo siamo noi. Grazie.

[1]:https://it.wikipedia.org/wiki/Riduzionismo_(filosofia)
[2]:http://www.auditorium.com/eventi/podcast?id_podcast=5224388
[3]:https://en.wikipedia.org/wiki/Carlo_Rovelli
[4]:http://en.wikipedia.org/wiki/Julian_Barbour
