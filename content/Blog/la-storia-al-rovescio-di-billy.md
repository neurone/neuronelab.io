title: La storia al rovescio di Billy
date: 2015-02-04 18:55:29 +0100
tags: libri
summary: Vista a rovescio da Billy, la storia era questa: gli aerei americani, pieni di fori e di feriti e di cadaveri decollavano all’indietro da un campo di aviazione in Inghilterra. Quando furono sopra la Francia, alcuni caccia tedeschi li raggiunsero, sempre volando all’indietro, e succhiarono proiettili e schegge da alcuni degli aerei e degli aviatori.

Da Mattatoio 5 di Kurt Vonnegut.

Vista a rovescio da Billy, la storia era questa: gli aerei americani, pieni di
fori e di feriti e di cadaveri decollavano all’indietro da un campo di aviazione
in Inghilterra. Quando furono sopra la Francia, alcuni caccia tedeschi li
raggiunsero, sempre volando all’indietro, e succhiarono proiettili e schegge da
alcuni degli aerei e degli aviatori. Fecero lo stesso con alcuni bombardieri
americani distrutti, che erano a terra e poi decollarono all’indietro, per
unirsi alla formazione. Lo stormo, volando all’indietro, sorvolò una città
tedesca in fiamme. I bombardieri aprirono i portelli del vano bombe,
esercitarono un miracoloso magnetismo che ridusse gli incendi e li raccolse in
recipienti cilindrici di acciaio, e sollevarono questi recipienti fino a farli
sparire nel ventre degli aerei. I contenitori furono sistemati ordinatamente su
alcune rastrelliere. Anche i tedeschi, là sotto, avevano degli strumenti
portentosi, costituiti da lunghi tubi di acciaio. Li usavano per succhiare altri
frammenti dagli aviatori e dagli aerei. Ma c’erano ancora degli americani
feriti, e qualche bombardiere era gravemente danneggiato. Sopra la Francia,
però, i caccia tedeschi tornarono ad alzarsi e rimisero tutti e tutto a nuovo.
Quando i bombardieri tornarono alla base, i cilindri di acciaio furono tolti
dalle rastrelliere e rimandati negli Stati Uniti, devo c’erano degli
stabilimenti impegnati giorno e notte a smantellarli, e separarne il pericoloso
contenuto e a riportarlo allo stato di minerale. Cosa commovente, erano
soprattutto le donne a fare questo lavoro. I minerali venivano poi spediti a
specialisti in zone remote. Là dovevano rimetterli nel terreno e nasconderli per
bene in modo che non potessero più fare male a nessuno.

[1]:https://it.wikipedia.org/wiki/Mattatoio_n._5
[2]:https://it.wikipedia.org/wiki/Kurt_Vonnegut
