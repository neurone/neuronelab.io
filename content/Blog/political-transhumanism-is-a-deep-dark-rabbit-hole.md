title: Political Transhumanism is a Deep, Dark Rabbit Hole
date: 2016-07-28 19:41
tags: politica,filosofia,tecnologia,transumanesimo
summary: Transhumanism has been going through a rough patch lately; or rather, certain interpretations of it. In 2015, Zoltan Istvan announced that he would be running for President of the United States under the American Transhumanist Party – of which he founded a year prior. Yes, he knew that running was one long exercise in futility; that's why he ensured his tour bus looked like a coffin before setting out with his campaign. No, this was not a joke; the 2016 United States Presidential Election has been markedly different in that interest in third party candidates is at an all time high. Zoltan gaining anything resembling a following isn't mere coincidence. Yet there seems to be an unspoken ideological rift between what Zoltan Istvan thinks Transhumanism is, and what Transhumanism is in the eyes of its global constituency.

Da [ensorcel.org][credit]

![Political Transhumanism]({filename}/images/political-transhumanism1.jpg)
Transhumanism has been going through a rough patch lately; or rather, certain
interpretations of it. In 2015, Zoltan Istvan announced that he would be running
for President of the United States under the [American Transhumanist Party][1] –
of which he founded a year prior. Yes, he knew that running was one long
exercise in futility; that's why he ensured his tour bus looked like a
[coffin][2] before setting out with his campaign. No, this was not a joke; the
2016 United States Presidential Election has been markedly different in that
interest in third party candidates is at an [all time high][3]. Zoltan gaining
anything resembling a following isn't mere coincidence. Yet there seems to be an
unspoken ideological rift between what Zoltan Istvan thinks Transhumanism is,
and what Transhumanism is in the eyes of its global constituency.

## But what is Transhumanism?

Transhumanists simply ask what it means to be human and suggest that the answer
is much more nuanced than one might think; it all comes down to liberating
ourselves from the limitations of the human body. It's unsurprising to
subsequently find that Transhumanism encompasses a rather broad spectrum of
proposed methodologies. From the immortalists seeking natural life extension to
the singularitarians seeking to upload their minds elsewhere in an effort to
form a collective consciousness, wanting to be “more human than human” can be an
apt descriptor of what most transhumanists desire. But when it comes to the
political motives in obtaining said desires.. then the emergence of divergent
modes of thought become more apparent. On the other hand, some would rather not
think at all to begin with.

![Political Transhumanism]({filename}/images/political-transhumanism2.png)
But I digress.

Imagine what a self-proclaimed transhumanist might be doing three years ago
before Zoltan Istvan published his first novel, _[The Transhumanist Wager][4]_.
Well, they'd likely be doing fuck-all since most of the technologies that would
have necessitated any strand of Transhumanism are still too speculative.
Needless to say, I'd be lying if I said I wasn't interested; Zoltan's novel was
hyped up as this [revolutionary][5] piece of literature that would fundamentally
change the contemporary transhumanist landscape and all we got was a steaming
pile of shit much akin to Ayn Rand's _Atlas Shrugged_.

One thing is certain, however: Transhumanism has been grounded in Libertarianism
for quite some time. Richard Barbrook's 1995 essay, _[The Californian
Ideology][6]_ lamented this when the fact was still new. Although back then, it
was the predominantly neoliberal [Extropian Institute][7] that deserved his
criticism rather than organizations of a more libertarian manifestation.
Regardless, in the United States at least, transhumanists of libertarian
political alignments have stuck around; their time wouldn't come until not long
after the Extropian Institute's demise. The World Transhumanist Association
(known today as Humanity+) was formed at the start of the new millennium and
everything was alright for a time. James Hughes outlines this occurrence in his
article, _The Politics of Transhumanism and the Techno-Millennial Imagination,
1626-2030._ He reminds us all that after the Great Recession, the unity with
which democratic transhumanists and libertarian transhumanists shared since the
creation of the World Transhumanist Association had since fallen by the wayside.

> In 2009 the libertarians and Singularitarians launched a campaign to take over
> the World Transhumanist Association Board of Directors, pushing out the Left
> in favor of allies like Milton Friedman's grandson and Seasteader leader Patri
> Friedman. Since then the libertarians and Singularitarians, backed by (Peter)
> Thiel's philanthropy, have secured extensive hegemony in the transhumanist
> community. As the global capitalist system spiraled into the crisis in which
> it remains, partly created by the speculation of hedge fund managers like
> Thiel, the left-leaning majority of transhumanists around the world have
> increasingly seen the contradiction between the millennialist escapism of the
> Singularitarians and practical concerns of ensuring that technological
> innovation is safe and its benefits universally enjoyed. p.13

_The Transhumanist Wager_ has been praised as a sort of go-to introductory
course for someone not well-versed in transhumanist dialectics; I'm not alone in
my disapproval of this. Some have gone so far to to sidestep Godwin's Law by
[comparing the novel][8] to Adolf Hitler's _Mein Kampf_. What's interesting
about Zoltan's novel however is that it tries to propose a philosophical
framework for a libertarian transhumanist agenda - one that can be equally
applied in meatspace. It's called "Teleological Egocentric Functionalism."

> Teleological—because it is every advanced individual's inherent design and
> desired destiny to evolve. Egocentric—because it is based on each of our
> selfish individual desires, which are of the foremost importance.
> Functional—because it will only be rational and consequential. p.66

Zoltan has claimed that his novel is purely an artistic representation of
transhumanist thought. Yet when he extrapolates on these same concepts in
reaction to the United States' contemporary political landscape, why does he
become so stingy when people start prying for answers? His "[Three Laws of
Transhumanism][9]" inspire little sympathy as it's only a reworded form of (I'm
tired of saying this) Teleological Egocentric Functionalism. Despite this he
continues to [insist][10] that the novel isn't indicative of his personal
philosophy.  There is nothing radical about being so vehemently individualistic.
Zoltan tries to reassure everyone that he's far more democratic and humanitarian
than he's led people to believe, but I've seen little evidence to suggest
otherwise. If true, that would only make him a [Trot][11] where he's
antagonizing for the sake of it. His libertarian tendencies could be no more
than a ruse hiding his true intentions - not like that makes his illegitimate
campaign any less appealing; more on that later. Zoltan Istvan's politics have
been utterly shallow given how much attention he's brought to himself within the
past year alone so I doubt there's anything else to be found past the
tongue-twister that is his personal philosophy. It's a pity, really as
mainstream understandings of Transhumanism in the United States haven't been
helped by the man's publicity stunts. As Rick Searle [points out][12]:

> In fusing the so-called militancy and cultural illiteracy of the New Atheism
> with a religiously infused interpretation of transhumanism, Knights, or
> Istvan, has merely created a new form of fundamentalism

It's utterly terrifying that anybody could actually think this way; Zoltan has
gone to great lengths in advocating against the dystopian, hollywood-driven
image of a transhumanist future and yet his politics suggest an embrace of those
same negative aspects.

![Political Transhumanism](/images/political-transhumanism3.jpg)

Across the Atlantic, democratic transhumanists have been regrouping under the
United Kingdom's Transhumanist Party. The formation of the UKTP is a pretty
recent phenomenon as Amon Twyman, it's founder and current leader among others
have been speculating methods of organizing since as far back as 2012 with their
ideas of a "[Zero State][13]" and "[Wavism][14]"; of which, are far too esoteric
for me to delve into right now (if ever). Both have attempted to become the
embodiment of Amon's even more recent notions of "Social Futurism" in an effort
to intersect with the libertarian majority. I keep seeing these aforementioned
predecessors lumped together under the "[Institute for Social Futurism][15]" but
it appears to still largely be an amalgamation of occult sensibilities
masquerading as something new. When it comes to the concept itself, Amon
originally [defined][16] it as:

> a worldview which combines social justice concerns with the radical
> transformative potential of modern technology.

Okay, cool; but what's the catch? Amon has since drawn parallels between Social
Futurism and infamous "Third Way" politics that had more or less defined the
policies of the United Kingdom's Labour Party for decades. He emphasizes a
desire to return to a reinterpretation of what were once social democratic norms
and the flawed sense of unity found in the centrism therein. This is very
reminiscent of Transhumanism's political landscape before the Great Recession; a
sort of nostalgia for what essentially amounted to nothing. Amon believes that
this centrism is not only radical, but ultimately synonymous with
Technoprogressivism even though that couldn't be further from the truth.
Although he is sympathetic to more socialist rhetoric, Amon and subsequently the
rest of the United Kingdom's Transhumanist Party seemingly remain on the fence
regarding where they truly stand. That is probably the most disconcerting thing
about this whole ordeal; democratic transhumanists want to respond to the man
who evidently enough wishes to become the face of Political Transhumanism
(Zoltan Istvan), but still have little to show for themselves in retrospect. In
October 2015, Amon Tyman posted an open letter on the UKTP's official blog
addressing Zoltan's persistence in speaking on behalf for the global
transhumanist community, [stating][17]:

> ..while his efforts gives him a unique opportunity to “brand” Transhumanism
> for a wider audience as he personally sees fit, he does not have any moral
> authority to do so. His implicit claim to moral authority comes from his claim
> to be founder of the Transhumanist Party, but the fact is that he is no such
> thing. He created and popularised the idea, to be sure, but he deliberately
> chose not to build a real party.

That's right; Zoltan created the original "Transhumanist Party" but the party
itself was no more than a media group aimed at perpetuating images of himself
and the ideas showcased in his shitty book. Zoltan's former secretary, Hank
Pellissier, had [resigned][18] from his position due to cited lack of
transparency within the organization. However much I disagree with the policies
of the United Kingdom's Transhumanist Party, at least they have some semblance
of legitimacy.  In the United States, the [Transhumanist National Committee][19]
was formed [in response][20] to this growing schism; a PAC dedicated to more or
less the same policies with which the United Kingdom's Transhumanist Party
adheres by albeit in a much less esoteric manner. Soon thereafter the latter has
since come out [in support][21] of the former. It's hard to say how well this
will pan out, yet at the same I'm reminded of Rachel Pick's [speculation][22] on
_Motherboard_ last year:

> Transhumanism may be too big and too multifaceted of a concept to be embodied
> by a central organization.

Amon Twyman and the United Kingdom's Transhumanist Party have been presenting
Social Futurism as a "true" alternative to the predominant right-libertarian
sensibilities shared by most transhumanists. A paradigm shift in thinking is
needed especially for what democratic transhumanists feel every time people like
Zoltan gain access to a keyboard, a puppy dies. But it's almost as if Amon is
asking libertarian transhumanists to meet him halfway (even though they have no
intention in doing so); it's just not working. Him making much ado about
decentralized, hipster organization models like "[Holacracy][23]" do little to
persuade those who are looking beyond tomorrow - beyond superfluous speculation.
A hierarchical system is still hierarchical even when its decentralized; there's
no need to beat around the bush here. No, a true "alternative" would be
something far more anarchistic.

## But what is Anarcho-Transhumanism?

Before you scoff and motion to close this tab whilst thinking the same tired
arguments against anarchism, I suggest reading Peter Gelderloos's _[Anarchy
Works][24]_ before reading on. Anarcho-Transhumanism is the idea that
transhumanist technologies and philosophies can facilitate the creation and
maintenance of anarchist societies. In short, Transhumanism implies Anarchism;
it makes Anarchism possible. H+Pedia [defines][25] it as:

> ..a political philosophy that rejects the requirements of the state as in
> Social Futurism or the economic model of contemporary western capitalist
> society.

William Gillis (also known as rechelon) is said to have coined the term in 2012,
but anarchist sensibilities within the transhumanist movement can be traced even
further to certain facets of the transhumanist blogosphere from [the early
oughts][26]; ones like _[Blue Shift][27]_ being the most notable. In his
article, _[The Incoherence and Unsurvivability of Non-Anarchist
Transhumanism][28]_, Gillis outlines why anarchism is the solution to the
conundrum that is "Political Transhumanism". He believes that the notion that
Social Democracy and Transhumanism are reconcilable is just plain absurd;
justifiably so:

> What does a world look like in which we _have the capacity_ to stop people
> from printing AR-15s? Forget the fuzzy-wuzzy associations of “democracy”, even
> “direct democracy”. Ask yourself what actually needs to be done to control
> gene therapy? Single facilities of government overseen use of high
> technologies? Massive backdoors in everyone’s devices that aggressively
> monitor and limit use? Totalitarian control of every communication on the
> planet? Aggressive raids against all hackers and tinkerers? Systematic
> accounting of every fabrication machinery in existence? Constant surveillance
> of anyone with knowledge of how these things work? Complete control of all
> resource allocation on the planet?

Gillis goes on to conclude that authoritarianism is the only outcome of social democracy when it's applied to transhumanist aspirations.

Ah, **Capitalism!**~ 

<iframe src='https://gfycat.com/ifr/PaleIndelibleKillifish' frameborder='0' scrolling='no' width='640' height='' allowfullscreen></iframe>

There is no such thing as "[responsible capitalism][29]" as Amon Twyman likes to
emphasize so much. I'm not alone when I say I find very little merit in the
longwinded, "ethical" justifications for the exploitation perpetuated from
presiding terminal capitalist systems. As technology becomes more and more
invasive, any justification is simply a distraction. [We should seek to expand
our physical freedom just as we seek to expand our social freedom][30]; so from
a transhumanist perspective Anarchism is a finality, not merely an alternative.
From my understanding, this sentiment is echoed by most democratic
transhumanists in all but name. This is made evident through the policies with
which their budding representatives adhere by; only instead they view these
policies through a rather narrow lens. From notions of "Post-Scarcity" that
posit a society without the coercive nature embodied by (oh so spooky)
capitalism, I'd wager that any self-proclaimed transhumanist that isn't actively
working towards an anarchist society doesn't embody this movement as much as
they'd like to believe. Transhumanism intersects with Futurology quite a bit; we
can spend all day discussing what the future entails, but these discussions are
usually grounded in a degree of certainty that the State will always exist
because that is how it has always been. This is also typically grounded in the
frankly outdated marxist notion that economic liberty is equal to social
liberty; if this logic is ever applied to transhumanist aspirations, then its
opponents will only decry the metaphorical horseshoe. We often lament how
opponents of Transhumanism can't imagine a world where mortality is a thing of
past - and that is only one facet of the kind of world with which we're
advocating for. Yet at the same time we continue to believe in the supposed
necessity of an [ideological state apparatus][31]; a need for centralized
governance and its dominance over local communities.

Today's transhumanists are clearly not ready to approach this. How long will
this dissonance continue? Will it finally be reprimanded by the time we actually
start eating meat that can be genetically identified [as ourselves][32]? How
about whenever we start losing limbs and replacing them with ones printed in
[someone else's garage?][33] Will we embrace our anarchistic tendencies when we
come to understand that our labour [is utterly meaningless][34] no matter how
many hours we clock in? The longer we wait the more likely getting implants at
some unsanitary, back alley vendor will be the only feasible option for enabling
[morphological freedom][35]; trust in one's community often supersedes broad,
utilitarian constraints and I find that fact most preferable. These are all
things we'll soon experience one way or another. Anarchism has provided a
plethora of witticisms in reaction to authoritarian fuck-ups and I don't see why
it should stop now.

Political Transhumanism is no longer something we can ignore. Its proposed
methodologies have been developing for quite some time; the sooner they come
close to realization, the more their political motivations will butt heads.. and
the effects will be monumental. 

[credit]:https://ensorcel.org/political-transhumanism-is-a-deep-dark-rabbit-hole/
[1]:http://www.transhumanistparty.org/About.html
[2]:http://www.huffingtonpost.com/zoltan-istvan/transhumanist-immortality_b_8241604.html
[3]:http://www.dailydot.com/layer8/donald-trump-hillary-clinton-poll-record-unpopularity-third-party-searches/
[4]:https://en.wikipedia.org/wiki/The_Transhumanist_Wager
[5]:https://web.archive.org/web/20160305210331/http://immortallife.info/articles/entry/transhumanist-revolution-now-...-an-excerpt-from-the-transhumanist-wager-by
[6]:https://en.wikipedia.org/wiki/The_Californian_Ideology
[7]:https://en.wikipedia.org/wiki/Extropianism#The_Extropy_Institute
[8]:https://web.archive.org/web/20160331162511/http://www.thehappydrone.com/2013/08/transhumanisms-mein-kampf/
[9]:http://www.huffingtonpost.com/zoltan-istvan/the-three-laws-of-transhu_b_5853596.html
[10]:https://web.archive.org/web/20160707011927/https://www.reddit.com/r/politics/comments/4php95/wild_transhumanist_campaign_tech_well_see_in/d4nli4e?context=3
[11]:http://www.antipope.org/charlie/blog-static/2013/11/trotskyite-singularitarians-fo.html
[12]:https://web.archive.org/web/20160619170011/http://ieet.org/index.php/IEET/more/searle20130916
[13]:https://hpluspedia.org/wiki/Zero_State
[14]:https://hpluspedia.org/wiki/Wave
[15]:https://hpluspedia.org/wiki/Institute_for_Social_Futurism
[16]:http://socialfuturism.net/
[17]:https://web.archive.org/web/20160429133816/https://transhumanistparty.wordpress.com/2015/10/12/zoltan-istvan-does-not-speak-for-the-transhumanist-party/
[18]:http://transhumanity.net/i-quite-the-usa-transhumanist-pary-why-zoltans-non-inclusive-leadership/
[19]:http://www.transhumanpolitics.com/
[20]:http://transhumanity.net/15-question-zoltan-istvan-is-avoiding-why-what-are-the-answers-opinion/
[21]:https://web.archive.org/web/20160429025153/https://transhumanistparty.wordpress.com/author/transhumanistparty/
[22]:https://motherboard.vice.com/read/the-transhumanist-movement-is-having-an-identity-crisis
[23]:https://web.archive.org/web/20160528014350/http://ieet.org/index.php/IEET/more/twyman20150602
[24]:http://theanarchistlibrary.org/library/peter-gelderloos-anarchy-works
[25]:https://hpluspedia.org/wiki/Anarcho-Transhumanism
[26]:https://twitter.com/rechelon/status/672170016821809152
[27]:https://web.archive.org/web/20110812175820/http://blueanarchy.wordpress.com/
[28]:http://ieet.org/index.php/IEET/more/gillis20151029
[29]:http://ieet.org/index.php/IEET/more/twyman20140427
[30]:https://web.archive.org/web/20160719211129/https://www.reddit.com/r/DebateAnarchism/comments/3o95oo/
[31]:https://theanarchistlibrary.org/library/saul-newman-anarchism-marxism-and-the-bonapartist-state#toc5
[32]:https://newrepublic.com/article/116947/celebrity-meat-science-behind-making-james-franco-furter
[33]:https://3dprint.com/2438/50-prosthetic-3d-printed-hand/
[34]:http://www.zerohedge.com/news/2015-05-29/europe-has-solution-unemployment-problem-fake-jobs
[35]:http://anarchotranshuman.org/post/117749304562/morphological-freedom-why-we
