title: L'illusione di essere liberi
date: 2015-09-23 18:19
tags: scienza, filosofia, filosofia della mente, psicologia, neuroscienze
summary: Sul blog Spaziomente, ho trovato un articolo molto ben scritto sull’esperimento di Libet, di cui Gianluca aveva pubblicato il video tempo fa. Quello che l’esperimento cercava di provare, è se siamo in grado di fare delle scelte coscienti. Cioè, se esiste una volontà, oppure, al contratio, se quello che percepiamo come atto di volontà, non sia altro che un’osservazione in retrospettiva di reazioni inconscie.

Sul [blog Spaziomente][1], ho trovato un [articolo][2] molto ben scritto sull’esperimento di [Libet][3], di cui Gianluca aveva [pubblicato il video][4] tempo fa. Quello che l’esperimento cercava di provare, è se siamo in grado di fare delle scelte coscienti. Cioè, se esiste una volontà, oppure, al contratio, se quello che percepiamo come atto di volontà, non sia altro che un’osservazione in retrospettiva di reazioni inconscie.

L’illusione di essere liberi
----------------------------

di Luca Bertolotti

Il tema del libero arbitrio è da sempre un punto dolente all’interno di
qualsiasi corpus filosofico, scientifico o religioso. Potrebbe essere quindi
utile offrire uno spunto di riflessione richiamando gli studi effettuati nel
1985 dal neurofisiologo Benjamin Libet, i quali tendono a dimostrare che
l’esperienza soggettiva della volontà è successiva ad una scelta già compiuta a
livello precosciente o inconscio (*La dimensione interpersonale della
coscienza*, 1998).

Prima che in noi nasca, consapevole, il desiderio di compiere, liberamente e
spontaneamente, un movimento qualsiasi, il nostro cervello ha già attivato i
processi cerebrali necessari al compimento del movimento stesso, quindi
inconsciamente.

Nei suoi esperimenti i partecipanti ricevevano l’indicazione di compiere un
semplice atto motorio di quando in quando, non appena decidevano liberamente e
spontaneamente di farlo, segnando nel contempo l’istante in cui prendevano la
decisione.

Durante il protrarsi dell’esperimento venivano registrati i potenziali
corticali, che le indagini neuropsicologiche attuali consentono di identificare
come indicatori della prontezza all’azione motoria, constatando che tali
indicatori precedevano sistematicamente di circa 0,5 secondi l’esperienza
cosciente.

Secondo le conclusioni che Libet trae dai risultati di questi esperimenti,
l’inizio neurale inconscio dell’azione, si mantiene tale per 0,35-0,4 secondi
circa, mentre il soggetto umano diviene consapevole della sua intenzione di
agire solo 0,15-0,2 secondi prima che l’atto motorio abbia luogo.

Ciò ha portato alla conclusione, avvalorata da molti altri esperimenti
successivi condotti da Dennet e Hilgard, che l’Io cosciente non possa realmente
decidere di compiere un movimento in un dato momento; piuttosto l’azione viene
decisa ad un altro livello non cosciente di attività cerebrale, e l’Io ne può
solo prendere atto in un secondo momento illudendosi di aver scelto.

Tutti gli esperimenti iniziano con un dialogo e una decisione di collaborazione
che prende forma tra sperimentatore e il soggetto dell’esperimento. Tale
rappresentazione di sé con l’altro persiste nella memoria del soggetto durante
il resto dell’esperimento al di fuori della sua coscienza, divenendo parte dei
processi mentali inconsci.

A questo livello inconscio tutto si prepara e prende forma l’inizio
dell’azione; solo dopo compare nell’individuo l’illusione di aver deciso
coscientemente, proprio in quell’attimo, di compiere il gesto voluto. Solo se
si considera l’individuo isolato, dunque, appare illusoria la sua esperienza di
essere l’autore cosciente delle proprie azioni, ma tale concetto è
un’astrazione che occorre superare.

Le conclusioni dell’esperimento di Libet si possono riassumere in 3 punti:

- Anche i movimenti volontari che riteniamo più spontanei hanno inizio da
  un’attività neurale inconscia.
- La consapevolezza dell’intenzione o del desiderio di agire è successiva a
  questo inizio neurale inconscio di 0,34 – 0,4 secondi.
- Il libero arbitrio, da sempre considerato promotore delle nostre libere
  azioni, va ripensato nel ruolo di semplice supervisore dell’azione.

La conclusione implicita è che anche le azioni volontarie, che abbiamo sempre
ritenuto di iniziare spontaneamente, in realtà, sarebbero il frutto di
un’attività cerebrale inconscia che ha inizio ben prima dell’intenzione
cosciente del soggetto di compiere l.azione stessa.

La proposta di Libet è di pensare al libero arbitrio in una forma del tutto
nuova, che non ha possibilità di dare inizio ad una azione, eppure ne diviene
responsabile ultimo del compimento dell’azione stessa. Solo dopo essere
divenuto cosciente di voler agire, un uomo può esercitare il suo libero
arbitrio decidendo di compiere o meno l’azione.

Nei 0,2 secondi in cui si diviene cosciente di voler agire, l’uomo può
esercitare il suo libero arbitrio, decidendo se compiere effettivamente
l’azione o se vietarla. In tali millisecondi è dunque possibile:

- Prendere atto del desiderio o della più generica intenzione di agire.
- Prendere atto di voler compiere l’azione con una certa urgenza.
- Decidere consapevolmente se portare a compimento oppure no l’azione.
- Dare il via, in caso di decisione favorevole, all’esecuzione motoria.

Libet ha recentemente proposto una nuova teoria in cui tenta di dare soluzione
al problema dell’unità della coscienza e della relazione tra la coscienza ed il
cervello. Secondo tale teoria l’esperienza cosciente avrebbe le caratteristiche
di un campo mentale, chiamato *Conscious Mental Field* (campo mentale
cosciente).

Affine per certi aspetti ad un campo magnetico e tuttavia non riducibile ad
alcun processo fisico noto, questo campo mentale avrebbe le medesime qualità
che sembrano caratterizzare l’esperienza cosciente: l’unità e la capacità di
influenzare o modificare l’attività neuronale.

Sebbene innegabilmente originato da una molteplice, ma adeguata attività
neuronale (che comunque contribuisce ad alterarne in parte i processi), il
fenomeno del *Mental Field* appartiene secondo Libet ad una categoria
fenomenologicamente indipendente, poiché non si tratta di un evento fisicamente
descrivibile.

Il campo mentale cosciente non è riducibile ad un semplice processo neuronale,
anche perché non vi è isomorfismo tra eventi neurali ed eventi mentali. Può
essere invece considerato una proprietà emergente del sistema cerebrale, da
cui, però, non può essere diviso come se fosse un’entità a se stante.

Il modo in cui la volontà cosciente sembra agire, con la sua capacità di
intervenire su processi cerebrali già attivati, può essere considerato un
esempio del funzionamento di questo campo mentale. Anche paragonata ad un
particolare campo di forze, di natura mentale anziché fisica, la coscienza
rimane comunque un fenomeno soggettivo ed in quanto tale accessibile solo al
singolo individuo.

Ancora una volta, quindi, il modo migliore, l’unico attraverso il quale sia
possibile indagare sulla coscienza e sui suoi prodotti, è chiedere direttamente
ai soggetti un resoconto introspettivo, quindi tramite l’introspezione.

Ad ogni modo, ciascuno di noi è “libero” di trarre le proprie conclusioni in
merito.

[1]:https://spaziomente.wordpress.com/
[2]:https://spaziomente.wordpress.com/2010/07/27/l%E2%80%99illusione-di-essere-liberi/
[3]:https://it.wikipedia.org/wiki/Benjamin_Libet
[4]:https://some1elsenotme.wordpress.com/2010/06/27/esperimento-di-libet/
