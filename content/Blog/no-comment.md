title: No Comment
date: 2018-06-02 16:26
tags: blog

Ho deciso di disabilitare il sistema di commenti tramite Disqus, essendo rimasto quasi del tutto inutilizzato, tornando al sistema precedente. Anch'esso non mi aveva portato alcun commento, ma almeno aveva il pregio di non affidarsi a soluzioni terze e potenzialmente lesive per al privacy.

Chi volesse commentare un articolo, può scrivere un'email usando indirizzo indicato nei contatti. Il commento verrà incluso manualmente in coda all'articolo.

È di oggi la notizia che [Microsoft sta valutando di acquisire GitHub][1]. Nel caso l'acquisizione vada in porto, questo blog forse verrà spostato su [GitLab][2] e dovrò decidere se mantenere o meno il mio account su GitHub, che attualmente uso anche per contribuire ad alcuni progetti Open Source tramite traduzioni e debugging.

[1]:https://www.businessinsider.com/2-billion-startup-github-could-be-for-sale-microsoft-2018-5?IR=T
[2]:https://about.gitlab.com/
