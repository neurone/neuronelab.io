title: Che cosa si prova a essere un pipistrello?
date: 2014-08-18 13:14:21 +0200
tags: filosofia, filosofia della mente
summary: Proseguo con la trascrizione di alcuni testi sul tema della filosofia della mente. Il seguente è un classico, scritto da Thomas Nagel.

Proseguo con la trascrizione di alcuni testi sul tema della filosofia della
mente. Il seguente è un classico, scritto da [Thomas Nagel][1].

Un altro testo molto influente è [L'esperimento della Stanza cinese][2] di [John Searle][3]. Si possono trovare entrambi pubblicati in italiano sulla raccolta L'io della mente,
da [Adelphi][4]. Ottimo libro, che consiglio come introduzione alla filosofia
della mente.

Il seguente articolo analizza il rapporto fra mente e cervello, toccando il
classico tema della soggettività e della difficoltà a comprendere le [altre menti][5],
specialmente se completamente aliene alla nostra.
Tema affrontato anche in due dei romanzi qua recensiti: Solaris di [Stanislaw Lem][6
e Picnic sul ciglio della strada di [Akardi e Boris Strugatzki][7].
A seguito, le riflessioni di Douglad Hofstadter.

Che cosa si prova a essere un pipistrello?
------------------------------------------
*di Thomas Nagel*

La coscienza è ciò che rende veramente ostico il problema del rapporto fra la
mente e il corpo. Forse è per questo che quando oggi si discute di questo
problema si presta scarsa attenzione alla coscienza o la si affronta in modo
palesemente sbagliato. La recente ondata di euforia riduzionista ha dato luogo a
parecchie analisi dei fenomeni mentali e dei concetti della mente, mirati a
spiegare la possibilità di certe forme di materialismo, di identificazione
psicofisica o di riduzione. Ma i problemi affrontati sono quelli comuni a questo
e ad altri tipi di riduzione, mentre viene ignorato ciò che rende il problema
mente-corpo unico e diverso dal problema acqua–H2O, o dal problema macchina di
Turing–macchina IBM, o dal problema fulmine–scarica elettrica, o dal problema
gene–DNA, o dal problema quercia–idrocarburo.
<!--more-->

Ciascun riduzionista ha la sua analogia preferita nella scienza moderna. È assai
improbabile che qualcuno di questi esempi incorrelati di riduzione ben riuscita
possa far luce sul rapporto fra mente e cervello. Ma i filosofi, come gli altri
uomini, hanno la debolezza di voler spiegare ciò che è incomprensibile in
termini che vanno bene per ciò che è familiare e ben compreso, benché totalmente
diverso. Ciò ha portato ad accettare descrizioni nient’affatto plausibili del
mentale, sostanzialmente perché esse consentono riduzioni di genere consueto.
Cercherò di spiegare perché gli esempi adotti comunemente non ci aiutano a
capire il rapporto tra mente e corpo; perché, anzi, a tutt’oggi non abbiamo la
minima idea di come potrebbe essere una spiegazione della natura fisica di un
fenomeno mentale. Senza la coscienza il problema mente–corpo sarebbe molto meno
interessante; con la coscienza esso appare senza speranza di soluzione.
L’aspetto più importante e caratteristico dei fenomeni mentali coscienti è
pochissimo compreso; le teorie riduzioniste per lo più non cercano nemmeno di
spiegarlo e un esame accurato dimostrerà che nessuno dei concetti di riduzione
attualmente disponibili è applicabile ad esso. Forse a questo scopo si può
escogitare una nuova forma teorica di riduzione, ma questa soluzione, se esiste,
si trova in un futuro intellettuale ancora lontano.

L’esperienza cosciente è un fenomeno ampiamente diffuso: è presente a molti
livelli della vita animale, anche se non possiamo essere certi della sua
presenza negli organismi più semplici ed è molto difficile in generale dire che
cosa ne dimostri l’esistenza. (Alcuni estremisti sono giunti a negarla perfino
nei mammiferi diversi dall’uomo). Essa si manifesta certo in innumerevoli forme,
per noi del tutto inimmaginabili, su altri pianeti di altri sistemi solari
nell’universo. Ma comunque possa variarne la forma, il fatto che un organismo
abbia un’esperienza cosciente significa, fondamentalmente, che a essere
quell’organismo si prova qualcosa. Vi possono essere altre implicazioni
riguardanti la forma dell’esperienza; vi possono forse anche essere (benché io
ne dubiti) implicazioni riguardanti il comportamento dell’organismo; ma
fondamentalmente un organismo possiede stati mentali coscienti se e solo se si
prova qualcosa a *essere* quell’organismo: se l’*organismo* prova qualcosa a
essere quello che è.

Possiamo parlare a questo proposito di carattere soggettivo dell’esperienza.
Nessuna delle analisi riduttive del mentale recenti e più conosciute ne dà
conto, perché esse sono tutte logicamente compatibili con la sua assenza. Il
carattere soggettivo dell’esperienza non è analizzabile nei termini di alcun
sistema esplicativo di stati funzionali o di stati intenzionali, poiché questi
stati potrebbero essere attribuiti a robot o ad automi che si comportassero come
persone anche senza avere alcuna esperienza soggettiva.(1) Esso non è analizzabile
in termini del ruolo causale dell’esperienza soggettiva in relazione al
comportamento umano tipico, e ciò per ragioni analoghe.(2) Non nego che gli stati
e gli eventi mentali coscienti causino il comportamento o che di essi si possa
dare una caratterizzazione funzionale; nego soltanto che non l’aver stabilito
una cosa del genere la loro analisi debba considerarsi conclusa. Qualsiasi
programma riduzionista deve essere basato su un’analisi di ciò che si deve
ridurre. Se l’analisi lascia fuori qualcosa, il problema è posto in modo falso.
È inutile basare la difesa del materialismo su un’analisi dei fenomeni mentali
che non tenga conto esplicitamente del loro carattere soggettivo, poiché non vi
è alcuna ragione per supporre che una riduzione che paia plausibile quando non
si faccia alcun tentativo per spiegare la coscienza possa essere estesa fino ad
includere la coscienza. Pertanto, se non si possiede alcuna idea di che cosa sia
il carattere soggettivo dell’esperienza, non si può sapere che cosa si debba
richiedere a una teoria fisicalista.

Benché una descrizione delle basi fisiche della mente debba spiegare molte cose,
questa sembra essere la più difficile. È impossibile escludere da una riduzione
gli aspetti fenomenologici dell’esperienza allo stesso modo in cui si escludono
gli aspetti fenomenologici di una sostanza ordinaria da una sua riduzione fisica
o chimica, cioè spiegandoli come effetti sulla mente degli osservatori umani
(cfr. Rorty, 1965). Se vogliano difendere il fisicalismo, dobbiamo trovare una
spiegazione fisica anche per gli aspetti fenomenologici. Tuttavia, quando si
esamina il loro carattere soggettivo sembra che sia impossibile riuscirci. La
ragione è che ogni fenomeno soggettivo è sostanzialmente legato a un singolo
punto di vista e pare inevitabile che una teoria oggettiva e fisica debba
abbandonare quel punto di vista.

Voglio prima di tutto cercare di enunciare il problema in modo alquanto più
preciso e completo di quanto si possa fare riferendosi semplicemente al rapporto
fra il soggettivo e l’oggettivo, o fra il *pour soi* e l’*en soi*. Ciò non è
affatto facile. I fatti relativi a ciò che si prova a essere un dato *X* sono
molto peculiari, tanto peculiari che alcuni possono essere inclini a dubitare
della loro realtà o a chiedersi se abbia senso sostenere qualche tesi su di
essi. Per illustrare il legame tra la soggettività e un particolare punto di
vista e per mettere in luce l’importanza degli aspetti soggettivi, sarà utile
indagare sulla questione riferendoci a un esempio che mette chiaramente in
risalto la divergenza tra i due tipi di concezione, quella soggettiva e quella
oggettiva.

Do per scontato che tutti siamo convinti che i pipistrelli abbiano esperienze
soggettive: in fin dei conti sono mammiferi, e il fatto che abbiano esperienze
soggettive non è più dubbio del fatto che le abbiano i topi, i piccioni o le
balene. Ho scelto i pipistrelli anziché le vespe o le sogliole perché via via
che si scende lungo l’albero filogenetico si è sempre meno disposti a credere
che siano possibili esperienze soggettive. Benché siano più affini a noi che
alle altre specie sopra ricordate, i pipistrelli presentano tuttavia una gamma
di attività e organi di senso così diversi dai nostri che il problema che voglio
impostare ne risulta illuminato vividamente (per quanto naturalmente lo si possa
porre anche per le altre specie). Anche senza il beneficio della riflessione
filosofica, chiunque sia stato per qualche tempo in uno spazio chiuso in
compagnia di un pipistrello innervosito sa che cosa voglia dire imbattersi in
una forma di vita fondamentalmente *aliena*.

Ho detto che la convinzione che i pipistrelli abbiano un’esperienza soggettiva
consiste essenzialmente nel credere che a essere un pipistrello si prova
qualcosa. Ora, noi sappiamo che la maggior parte dei pipistrelli (i
microchirotteri, per la precisione) percepisce il mondo esterno principalmente
mediante il sonar, o ecorilevamento: essi percepiscono le riflessione delle
proprie strida rapide, finemente modulate e ad alta frequenza (ultrasuoni)
rimandate dagli oggetti situati entro un certo raggio. Il loro cervello è
strutturato in modo da correlare gli impulsi uscenti con gli echi che ne
risultano, e l’informazione così acquisita permette loro di valutare le
distanze, le dimensioni, le forme, i movimenti e le strutture con la precisione
paragonabile a quella che noi raggiungiamo con la vista. Ma il sonar del
pipistrello, benché sia evidentemente una forma di percezione, non assomiglia
nel modo di funzionare a nessuno dei nostri sensi e non vi è alcun motivo per
supporre che esso sia oggettivamente simile a qualcosa che noi possiamo
sperimentare o immaginare. Ciò, a quanto pare, rende difficile capire che cosa
si provi a essere un pipistrello. Dobbiamo vedere se esiste qualche metodo che
ci permetta di estrapolare la vita interiore del pipistrello a partire dalla
nostra situazione(3) e, in caso contrario, quali metodi alternativi vi siano per
raggiungere il nostro scopo.

È la nostra esperienza che fornisce il materiale di base alla nostra
immaginazione, la quale è perciò limitata. Non serve cercare di immaginare di
avere sulla braccia un’ampia membrana che ci consente di svolazzare qua e là
all’alba e al tramonto per acchiappare insetti con la bocca; di avere una vista
molto debole e di percepire il mondo circostante mediante un sistema di segnali
sonori ad alta frequenza riflessi dalle cose; e di passare la giornata appesi
per i piedi, a testa in giù, in una soffitta. Se anche riesco a immaginarmi
tutto ciò (e non mi è molto facile), ne ricavo solo che cosa proverei *io* a
comportarmi come un pipistrello. Ma non è questo il problema: io voglio sapere
che cosa prova *un pipistrello* a essere un pipistrello. Ma se cerco di
figurarmelo, mi trovo ingabbiato entro le risorse della mia mente, e queste
risorse non sono all’altezza dell’impresa. Non riesco a uscire né immaginando di
aggiungere qualcosa alla mia esperienza attuale, né immaginando di sottrarle via
via dei segmenti, né immaginando di compiere una qualche combinazione di
aggiunte, sottrazioni e modifiche.

Anche se mi fosse possibile avere l’aspetto e il comportamento di una vespa o di
un pipistrello, senza però mutare la mia struttura fondamentale, anche in questo
caso le mie esperienze non sarebbero affatto simili alle esperienze di questi
animali. D’altra parte, non ha probabilmente senso supporre che io possa
arrivare a possedere la costituzione neurofisiologica interna di un pipistrello.
Anche se potessi trasformarmi gradualmente in un pipistrello, nulla della mia
costituzione attuale mi consente di immaginare quali sarebbero le esperienze di
questo mio stato futuro dopo la metamorfosi. Le indicazioni migliori verrebbero
dalle esperienze dei pipistrelli, se solo sapessimo come sono.

Se quindi per farsi un’idea di che cosa si provi a essere un pipistrello ci si
basa su un’estrapolazione della nostra situazione, questa estrapolazione è
destinata a restare incompleta. Possiamo costruirci tuttalpiù una concezione
schematica di che cosa si *prova*; per esempio, possiamo ascrivere *tipi*
generali di esperienza soggettiva sulla base della struttura e del comportamento
animale.

Descriviamo così il sonar dei pipistrelli come una forma di percezione
tridimensionale in avanti; crediamo che i pipistrelli sentano una qualche forma
di dolore, paura, fame e concupiscenza e che, oltre al sonar, posseggano altri
tipi di percezione a noi più familiari. Tuttavia siamo anche convinti che queste
esperienze hanno in ciascun caso un carattere soggettivo specifico e che
concepirlo supera le nostre capacità. E se altrove nell’universo esiste vita
cosciente, è probabile che in certi casi essa non sia descrivibile neppure nei
più generali termini esperenziali a nostra disposizione.(4) (Il problema,
tuttavia, non è limitato ai casi estremi: esso esiste anche fra una persona e
l’altra: il carattere soggettivo dell’esperienza di una persona sorda e cieca
dalla nascita, per esempio, non mi è accessibile, così come presumibilmente a
lei non è accessibile il carattere soggettivo della mia esperienza. Questo non
impedisce a ciascuno di noi di credere che l’esperienza dell’altro possegga
questo carattere soggettivo).

Chi fosse incline a negare che si possa credere nell’esistenza di fatto come
questo, la cui natura esatta non abbiamo modo di concepire, rifletta che
nell’osservare i pipistrelli noi ci troviamo in una posizione quasi identica a
quella in cui si troverebbero un pipistrello intelligente o un marziano(5) che
tentassero di farsi un’idea di che cosa si provi a essere noi. La struttura
della loro mente potrebbe impedir loro di riuscirci, ma noi sappiamo che
avrebbero torto a concludere che non si prova nulla di preciso ad essere noi,
che a noi possono essere ascritti solo certi tipi generali di stati mentali
(forse la percezione e l’appetito sarebbero concetti comuni a noi e a loro; o
forse no). Sappiamo che avrebbero torto a trarre una conclusione così scettica,
perché noi sappiamo che cosa si prova a essere noi. E sappiamo che, per quanto
ciò comprenda una varietà e una complessità grandissime e per quanto noi non
possediamo la terminologia capace di darne una descrizione sufficiente, il suo
carattere soggettivo è altamente specifico e, sotto certi aspetti, è
descrivibile in termini che possono essere capiti solo da creature come noi. Il
fatto che non possiamo sperare di riuscire mai a fornire col nostro linguaggio
una descrizione particolareggiata della fenomenologia dei marziani o dei
pipistrelli non dovrebbe indurci a considerare priva di sensi l’ipotesi che i
pipistrelli e i marziani abbiano esperienze affatto paragonabili alle nostre per
ricchezza di particolari. Sarebbe bello se qualcuno riuscisse a elaborare un
insieme di concetti e una teoria che ci consentissero di riflettere su queste
cose; ma i limiti della nostra natura ci impediscono, forse per sempre, una tale
comprensione. E negare la realtà o la portata logica di ciò che non potremmo mai
descrivere o comprendere è la forma più rozza di dissonanza cognitiva.

Questo ci porta a sfiorare un argomento che richiede una discussione molto più
ampia di quella che mi è consentita qui: cioè il rapporto tra i fatti da una
parte e gli schemi concettuali o i sistemi di rappresentazione dall’altra. La
mia posizione realistica nei confronti del dominio della soggettività in tutte
le sue forme implica che io credo nell’esistenza di fatti che travalicano la
portata dei concetti umani. È certamente possibile per un essere umano credere
che vi siano dei fatti per rappresentare o comprendere i quali gli uomini non
*possederanno mai* i concetti necessari. Sarebbe anzi assurdo dubitarne, vista
la finitezza delle aspettazioni umane. In fin dei conti, i numeri transfiniti
sarebbero esistiti lo stesso anche se tutti gli uomini fossero stati tolti di
mezzo dalla peste bubbonica prima della scoperta di Cantor. Ma si può anche
credere che vi siano dei fatti che non potrebbero mai essere rappresentati o
compresi dagli esseri umani, anche se la nostra specie durasse per sempre,
semplicemente perché la nostra struttura non ci permette di operare con i
concetti del tipo necessario. Questa impossibilità potrebbe essere addirittura
osservata da altri esseri, ma non è detto che l’esistenza di tali esseri, o la
possibilità della loro esistenza, sia una condizione affinché l’ipotesi che vi
siano fatti inaccessibili agli uomini abbia senso. (Dopotutto, la natura di
esseri aventi accesso a fatti inaccessibili agli uomini è presumibilmente
anch’essa un fatto inaccessibile agli uomini). Riflettendo su ciò che si prova a
essere un pipistrello si arriva dunque, a quanto pare, alla conclusione che
esistono fatti che non consistono nella verità di preposizioni esprimibili con
linguaggio umano. Possiamo essere costretti a riconoscere l’esistenza di tali
fatti senza essere in grado di enunciarli o di comprenderli.

Tuttavia interromperò qui la discussione di questo argomento. La sua rilevanza
per il problema che ci sta di fronte (cioè per il problema mente-corpo) sta nel
fatto che esso ci consente di fare un’osservazione generale sul carattere
soggettivo dell’esperienza. Qualunque sia la natura dei fatti relativi a ciò che
si prova a essere un uomo o un pipistrello o un marziano, questi fatti
esprimono, a quanto pare, uno specifico punto di vista.

Non mi riferisco qui alla supposta privatezza dell’esperienza per chi la compie;
il punto di vista in questione non è un punto di vista accessibile a un unico
individuo: è piuttosto un *tipo*. È spesso possibile assumere un punto di vista
diverso dal proprio, sicché la comprensione di tali fatti non è limitata al
proprio caso particolare. Vi è un senso in cui i fatti fenomenologici sono
perfettamente oggettivi: una persona può sapere o dire quale sia la qualità
dell’esperienza di un’altra persona. Essi sono soggettivi, tuttavia, nel senso
che anche questa ascrizione oggettiva dell’esperienza è possibile solo a
qualcuno che sia abbastanza simile all’oggetto dell’ascrizione da essere in
grado di adottare il suo punto di vista, cioè di comprendere l’ascrizione in
prima persona, per così dire, oltre che in terza persona. Quanto più l’altro, il
soggetto dell’esperienza, è diverso da noi, tanto più difficile sarà,
presumibilmente, riuscire in questa impresa. Nel caso di noi stessi, noi
occupiamo il punto di vista in questione, ma se ci accostassimo alla nostra
esperienza da un altro punto di vista, incontreremmo, per comprenderla nel modo
giusto, la stessa difficoltà che incontreremmo se tentassimo di comprendere
l’esperienza di un’altra specie senza adottare il *suo* punto di vista.(6) Ciò
tocca direttamente il problema mente-corpo, poiché se i fatti dell’esperienza
soggettiva – i fatti riguardanti il provare ciò che prova l’organismo *che ha*
l’esperienza – sono accessibili da un unico punto di vista, allora è un mistero
come il vero carattere fisico delle esperienze soggettive può essere rivelato
nel funzionamento fisico di quell’organismo. Quest’ultimo è un campo di fatti
oggettivi per eccellenza, fatti che possono essere osservati e capiti da molti
punti di vista e da individui dotati di sistemi di percezione differenti. Non
esistono barriere immaginative analoghe che si oppongano all’acquisizione di
conoscenze sulla neurofisiologia dei pipistrelli da parte di scienziati umani, e
viceversa pipistrelli o marziani intelligenti potrebbero imparare sul cervello
umano più di quanto potremo mai imparare noi.

Questo non è di per se stesso un argomento contro la riduzione. Uno scienziato
marziano che non capisce la percezione visiva, potrebbe capire l’arcobaleno o il
fulmine o le nubi come fenomeni fisici, anche se non sarebbe mai in grado di
capire i concetti umani dell’arcobaleno, del fulmine o della nube, o il posto
che queste cose occupano nel nostro mondo fenomenico. La natura oggettiva delle
cose espresse da questi concetti potrebbe essere da lui colta perché, mentre i
concetti sono legati a un punto di vista particolare e a una particolare
fenomenologia visiva, le cose colte da quel punto di vista non lo sono: esse
sono osservabili da quel punto di vista, ma sono esterne a esso; possono quindi
essere capite anche da punti di vista diversi, sia da parte degli stessi
organismi sia da parte di altri. Il fulmine ha un carattere oggettivo che non si
esaurisce nella sua manifestazione visiva, e può essere studiato da un marziano
privo della vista. Per essere precisi: esso ha un carattere *più* oggettivo di
quanto non si riveli nella sua manifestazione visiva. Parlando del passaggio
dalla caratterizzazione soggettiva a quella oggettiva, desidero non pronunciarmi
sull’esistenza o meno di un punto terminale, di una natura intrinseca
compiutamente oggettiva della cosa, raggiungibile o no. Forse è più corretto
concepire l’oggettività come una direzione in cui può viaggiare il comprendere.
E per comprendere un fenomeno come il fulmine è legittimo allontanarsi quanto
più possibile da un punto di vista strettamente umano.(7)

Nel caso dell’esperienza soggettiva, viceversa, il legame con un punto di vista
particolare sembra molto più stretto. È difficile capire che cosa si potrebbe
intendere per carattere oggettivo di un’esperienza soggettiva, a parte il modo
in cui la coglie, dal suo particolare punto di vista, il soggetto che la coglie.
Dopotutto, che cosa resterebbe di ciò che si prova ad essere un pipistrello se
si eliminasse il punto di vista del pipistrello? Ma se l’esperienza soggettiva
non ha, in aggiunta al proprio carattere soggettivo, una natura oggettiva che
possa essere colta da molti punti di vista diversi, come si può supporre che un
marziano investighi il mio cervello possa osservare dei processi fisici che sono
i miei processi mentali (così come potrebbe osservare dei processi fisici che
sono i fulmini), ma da un punto di vista diverso? E come, anzi, potrebbe
osservarli da un altro punto di vista un fisiologo umano?(8)

A quanto pare ci troviamo di fronte a una difficoltà di carattere generale a
proposito della riduzione psicofisica. In altri campi il processo di riduzione
porta nella direzione di una maggiore oggettività, porta verso una visione più
precisa della reale natura delle cose. Ciò viene ottenuto mediante la riduzione
della nostra dipendenza da punti di vista specifici dell’individuo o della
specie nei confronti dell’oggetto d’indagine: noi lo descriviamo non nei termini
delle impressioni che esso procura ai nostri sensi, bensì nei termini dei suoi
effetti più generali e a proprietà rilevabili con mezzi diversi dai sensi
dell’uomo. Quanto meno la nostra descrizione dipende da un punto di vista
specificamente umano, tanto più essa è oggettiva.

È possibile seguire questa via poiché, sebbene i concetti e le idee da noi
impiegati nel riflettere sul mondo esterno provengano all’inizio da un punto di
vista che coinvolge il nostro apparato percettivo, essi vengono da noi usati per
riferirci a cose che stanno al di là di essi e nei confronti delle quali noi
*possediamo* un punto di vista fenomenico. Possiamo perciò abbandonare un punto
di vista in favore di un altro, pur continuando a riflettere sulle stesse cose.

L’esperienza soggettiva, tuttavia, non sembra rientrare in questo schema. Con
essa l’idea di muovere dalle apparenze alla realtà non sembra avere senso. Che
cosa corrisponde in questo senso alla ricerca di una comprensione più oggettiva
degli stessi fenomeni, abbandonando il punto di vista soggettivo inizialmente
adottato nei loro confronti in favore di un altro più oggettivo ma che riguarda
la stessa cosa? Certamente *appare* improbabile che possiamo avvicinarci alla
natura reale dell’esperienza umana abbandonando la particolarità del nostro
punto di vista umano e sforzandoci di giungere a una descrizione accessibile a
esseri incapaci di immaginare che cosa si provi a essere noi. Se il carattere
soggettivo dell’esperienza si può comprendere compiutamente da un solo punto di
vista, allora nessuno spostamento verso una maggiore oggettività, cioè nessun
distacco da un punto di vista specifico, ci porterà più vicini alla natura reale
del fenomeno: anzi ce ne allontanerà.  
In un certo senso i germi di questa obiezioni alla riducibilità dell’esperienza
si possono già riscontrare in certi casi riusciti di riduzione; infatti nello
scoprire che il suono è in realtà un fenomeno ondulatorio che avviene nell’aria
o in altri mezzi, noi abbandoniamo un punto di vista per assumerne un altro, e
il punto di vista uditivo, umano o animale, che abbandoniamo non viene ridotto.
Due individui appartenenti a specie radicalmente diverse possono capire entrambi
gli stessi eventi fisici in termini oggettivi, senza per questo dover capire le
forme fenomeniche sotto le quali quegli eventi appaiono ai sensi degli
appartenenti all’altra specie. Perciò il loro riferirsi a una realtà comune ha
come condizione che i loro punti di vista più particolari non facciano parte
della realtà comune che entrambi colgono. La riduzione può riuscire solo se il
punto di vista proprio della specie viene eliminato da ciò che si deve ridurre.

Tuttavia, mentre è giusto mettere da parte questo punto di vista quando si
ricerca una comprensione più piena del mondo esterno, non lo si può ignorare in
modo permanente, dato che esso costituisce l’essenza del mondo interiore e non
semplicemente un punto di vista su di esso. In massima parte il
neocomportamentismo della recente psicologia filosofica deriva dallo sforzo di
sostituire alla mente reale un concetto oggettivo di mente, allo scopo di non
lasciare nulla che non possa essere ridotto. Se riconosciamo che una teoria
fisica della mente deve spiegare il carattere soggettivo dell’esperienza,
dobbiamo ammettere che nessuna delle concezioni attuali ci dà un’indicazione su
come si possa ottenere una tale spiegazione. Il problema è unico. Se i processi
mentali sono davvero processi fisici, allora si prova, intrinsecamente(9),
qualcosa nel subire certi processi fisici. Che cosa poi ciò sia, resta un
mistero.

Che morale si può ricavare da queste riflessioni, e quale deve essere il passo
successivo? Sarebbe un errore concludere che il fisicalismo è necessariamente
falso. L’inadeguatezza delle ipotesi fisicaliste che postulano un’analisi
falsamente oggettiva della mente non dimostra nulla. Sarebbe più giusto dire che
il fisicalismo è una posizione che non possiamo capire perché ora non abbiamo
alcuna concezione di come esso potrebbe essere vero. Ma forse si giudicherà
irragionevole considerare il processo di una concezione del genere come una
condizione per la comprensione. Dopotutto, si potrebbe dire, il significato del
fisicalismo è chiaro: gli stati della mente sono stati del corpo; gli eventi
mentali sono eventi fisici. Non sappiamo *quali* stati e *quali* eventi fisici
essi siano, ma questo non dovrebbe impedirci di comprendere l’ipotesi. Che cosa
ci potrebbe essere di più chiaro delle parole “é” e “sono”?

Ma io credo che proprio questa chiarezza che attribuiamo alla parola “é” sia
ingannevole. Di solito, quando ci dicono che *X* è *Y*, noi sappiamo come si
intende che ciò sia vero, ma questo dipende da uno sfondo concettuale o teorico
che non è trasmissibile dal solo “è”. Di *“X”* e *“Y”* sappiamo come si
riferiscono alle cose e il genere di cose cui si riferiscono, e abbiamo un’idea
più o meno precisa di come i due percorsi referenziali potrebbero convergere su
un’unica cosa, sia essa un oggetto, una persona, un processo, un evento o altro.
Ma quando i due termini dell’identificazione sono molto disparati, può non
essere altrettanto chiaro come ciò possa essere vero. Potremmo non avere neppure
un’idea approssimata di come i due percorsi referenziali potrebbero convergere o
su che genere di cose essi potrebbero convergere; e per farci comprendere ciò,
può darsi che sia necessaria un’impalcatura teorica. Senza questa impalcatura,
l’identificazione sarebbe circondata da un alone di misticismo.

Questo spiega il sapore magico delle presentazioni divulgative delle scoperte
scientifiche fondamentali, che vengono proclamate come proposizioni da accettare
senza veramente capirle. Oggi per esempio, fin da bambini si sente dire che
tutta la materia in realtà è energia. Ma nonostante tutti sappiano che cosa
significa “è”, la maggior parte della gente non si farà un idea di che cosa
rende vera questa affermazione, perché non possiede un preparazione teorica.

La situazione odierna del fisicalismo è simile a quella in cui si sarebbe
trovata l’ipotesi che la materia è energia se fosse stata formulata da un
filosofo presocratico. Non abbiamo la più pallida concezione di come il
fisicalismo potrebbe essere vero. Per poter capire l’ipotesi che un evento
mentale è un evento fisico abbiamo bisogno di capire qualcosa di più della
parola “è”. non abbiamo alcuna idea di come un termine mentale ed un termine
fisico potrebbero riferirsi alla stessa cosa, e le solite analogie con le
identificazioni teoriche che osserviamo in altri campi non riescono a darcela.
Non ci riescono perché, se interpretiamo il riferimento dei termini mentali agli
eventi fisici secondo i modello solito, otteniamo o una ricomparsa degli eventi
soggettivi separati come effetti attraverso i quali è assicurato il riferimento
mentale agli eventi fisici, oppure una spiegazione falsa di come i termini
mentali si riferiscono alle cose (ad esempio una spiegazione comportamentista
causale).

Può sembrare strano, ma è possibile avere una prova della verità di qualcosa che
non riusciamo a comprendere realmente. Supponiamo che un tale all’oscuro della
metamorfosi degli insetti rinchiuda un bruco in un recipiente sterilizzato e
che, riaprendo il recipiente dopo qualche settimana, vi trovi una farfalla. Se
questo tale è sicuro che il recipiente è sempre stato chiuso, ha ragione di
credere che la farfalla sia, o sia stata in passato, il bruco, pur senza
minimamente sapere in che senso ciò sia vero. (Una possibilità è per esempio che
il bruco contenesse un minuscolo parassita alato che lo abbia divorato e sia
quindi cresciuto fino a diventare la farfalla).

È concepibile che nei confronti del fisicalismo ci troviamo in una situazione
analoga.

Donald Davidson ha sostenuto che gli eventi mentali, se hanno cause ed effetti
fisici, devono possedere una descrizione fisica. Egli ritiene che abbiamo motivo
di crederlo anche se non possediamo – anzi, anche se non *potessimo* possedere –
una teoria psicofisica generale.(10) Il suo ragionamento è riferito agli eventi
mentali intenzionali, ma io penso che abbiamo anche motivo di credere che le
sensazioni sono processi fisici, pur senza essere in grado di capire come. La
posizione di Davidson è che certi eventi fisici hanno proprietà mentali
irriducibili, e forse una concezione che si possa formulare in questi termini è
giusta. Ma nulla di cui oggi ci possiamo formare un concetto corrisponde a essa;
e non abbiamo neppure alcuna idea di come sarebbe una reoria che ci sonsentisse
di concepire una cosa del genere.(11)

Pochissimi sforzi sono stati dedicati al problema fondamentale (a proposito del
quale non è assolutamente necessario parlare di cervello) se si possa attribuire
significato all’ipotesi che le esperienze soggettive abbiano un qualche
carattere oggettivo. In altre parole, ha senso che io mi chieda come sono
*realmente* le mie esperienze, rispetto a come mi appaiono? Non ci è possibile
avere una comprensione autentica dell’ipotesi che la loro natura possa essere
rispecchiata in una descrizione fisica, se non comprendiamo l’idea più
fondamentale che esse *hanno* una natura oggettiva (o che i processi oggettivi
possono avere una natura soggettiva).(12)

Vorrei concludere con una proposta speculativa. Può darsi che ci si possa
accostare al divario tra soggettivo e oggettivo da un’altra direzione. Mettendo
da parte per il momento il rapporto tra mente e cervello, possiamo cercare di
raggiungere una comprensione più oggettiva del mentale di per sé. Al momento non
abbiamo alcuno strumento per riflettere sul carattere soggettivo dell’esperienza
senza ricorrere all’immaginazione, cioè senza assumere il punto di vista del
soggetto dell’esperienza. Questo ci dovrebbe spingere a costruire concetti nuovi
e a inventare un metodo nuovo, una fenomenologia oggettiva che non dipendesse
dall’empatia o dall’immaginazione. Anche se presumibilmente essa non potrebbe
dar conto di tutto, il suo scopo sarebbe quello di descrivere, almeno in parte,
il carattere soggettivo delle esperienze in una forma che fosse comprensibile a
essere incapaci di avere quelle esperienze.

Dovremmo elaborare una fenomenologia siffatta per descrivere le esperienze sonar
dei pipistrelli, ma si potrebbe anche cominciare dagli uomini: si potrebbe, per
esempio, cercare di foggiare concetti che servano a spiegare a un cieco nato che
cosa si prova a vedere. Prima o poi ci si troverebbe di fronte a un muro, ma
dovrebbe essere possibile escogitare un metodo per esprimere in termini
oggettivi molto più di quanto non possiamo esprimere oggi, e con una precisione
molto maggiore. Le vaghe analogie intermodali (per esempio: “il rosso è come uno
squillo di tromba”) che pullulano nelle discussioni su questo argomento servono
a poco. Ciò dovrebbe essere chiaro a chiunque abbia udito una tromba e visto il
rosso. Ma gli aspetti strutturali della percezione potrebbero essere più
accessibili a una descrizione oggettiva, anche se qualche cosa ne verrebbe
lasciato fuori. E concetti diversi da quelli che noi apprendiamo in prima
persona ci possono consentire di arrivare a un tipo di comprensione anche della
nostra stessa esperienza che ci è impedito proprio da quella facilità di
descrizione e da quell’assenza di distanza che consentono i concetti soggettivi.

A parte il suo interesse intrinseco, una fenomenologia che fosse oggettiva in
questo senso consentirebbe di dare una forma più intelligibile alle domande a
proposito della base fisica(13) dell’esperienza. Gli aspetti dell’esperienza
soggettiva che ammettessero questo genere di descrizione oggettiva potrebbero
prestarsi meglio di altri a fornire spiegazioni oggettive di tipo più concreto.
Ma indipendentemente dal fatto che questa congettura sia giusta o no, sembra
improbabile che si possa formulare una qualunque teoria fisica della mente
finché non si sarà riflettuto più a fondo sul problema generale della
soggettività e dell’oggettività. Altrimenti non si potrà neppure porre il
problema mente-corpo senza con ciò stesso eluderlo.

Riflessioni
-----------

*He does all the things that you  
[would never do;  
He loves me, too –  
His love is true.  
Why can’t he be you?(14)*  
Hank Cochran, ca. 1955

*Twinkle, twinkle, little bat,  
How I wonder what you’re at,  
Up above the world you fly,  
Like a tea-tray in the sky.(15)*  
Lewis Carrol, ca. 1865

C’è un famoso rompicapo che viene posto nei corsi di matematica e di fisica:
“Perché lo specchio scambia la destra e la sinistra, ma non l’alto e il basso?”.
Esso costringe molti a una pausa di riflessione; chi non vuole sentirsi dire
subito la risposta, salti i due capoversi che seguono.

La risposta è imperniata su quello che noi consideriamo un modo giusto di
proiettare noi stessi sulle nostre immagini riflesse. La nostra prima
impressione è che avanzando di qualche passo e poi girandoci sui tacchi,
potremmo metterci al posto di “quella persona” là dentro lo specchio,
dimenticandoci però che il cuore, l’appendice, eccetera, di “quella persona”
sono dalla parte sbagliata. L’emisfero cerebrale che presiede al linguaggio non
è, con ogni probabilità, dalla parte dove sta di solito. Da un punto di vista
anatomico generale, tale immagine è in realtà una non-persona; a livello
microscopico poi la situazione è ancora peggiore: le eliche delle molecole di
DNA girano alla rovescia e la “persona” dello specchio non potrebbe accoppiarsi
con una persona normale più di quanto potrebbe farlo un’anosrep!

Un momento, però: possiamo tenere il cuore dal lato giusto se, invece di
girarci, ci mettiamo a testa in giù (per esempio, ruotando su una sbarra
orizzontale posta all’altezza della vita). Ora il nostro cuore è dalla stessa
parte di quello della persona dello specchio, ma i piedi e la testa sono nella
posizione sbagliata, e lo stomaco, benché più o meno all’altezza giusta, è
capovolto. Pare dunque che si possa considerare lo specchio come un dispositivo
che scambia l’alto e il basso purché noi siamo disposti e proiettarci su una
creatura che ha i piedi in alto e la testa in basso. Tutto dipende da come ci si
vuole proiettare su un’altra entità. Si può scegliere tra una piroetta intorno a
una sbarra orizzontale e una piroetta intorno a una sbarra verticale, tra avere
il cuore nella posizione giusta e la testa e i piedi scambiati, e avere a posto
la testa e i piedi ma non il cuore. Il fatto è semplicemente che, a causa della
simmetria verticale esterna del corpo umano, una piroetta intorno a una sbarra
verticale fornisce una corrispondenza fra noi e l’immagine in apparenza più
plausibile. Ma agli specchi in realtà non importa in che modo noi interpretiamo
ciò che essi fanno. E in realtà ciò che essi scambiano sono solo il davanti e il
di dietro!

C’è qualcosa di molto ingannevole in questo concetto di proiezione,
corrispondenza, identificazione, empatia o comunque lo si voglia chiamare. È un
tratto umano fondamentale, al quale in pratica non si può resistere, eppure esso
ci può condurre per sentieri concettuali molto strani. Il rompicapo appena visto
ci mostra i pericoli di un’autoproiezione troppo facile, e il ritornello della
canzonetta citato in epigrafe ci ricorda con maggior forza che è vano prendere
troppo sul serio questa proiezione. Eppure non possiamo farne a meno, andiamo
fino in fondo e abbandoniamoci a un’orgia di stravaganti variazioni sul tema
proposto da Nagel col suo titolo.

Che cosa si prova a lavorare da McDonald? Ad avere trentotto anni? A essere a
Londra oggi?

Che cosa si prova a scalare l’Everest? A vincere la medaglia d’oro della
ginnastica alle Olimpiadi?

Che cosa si proverebbe a essere un buon musicista? A saper improvvisare fughe? A
essere J.S. Bach? A essere J.S. Bach mentre scrive l’ultimo movimento del
*Concerto italiano*?

Che cosa si prova a credere che la Terra sia piatta?

Che cosa si prova a essere una persona incommensurabilmente più intelligente di
noi? O incommensurabilmente meno intelligente?

Che cosa si prova a detestare il cioccolato (o la cosa che ci piace di più)?

Che cosa si prova sentir parlare italiano (o la propria madrelingua) senza
capire niente?

Che cosa si prova ad appartenere all’altro sesso? (Si veda sopra “Un problema di
rigetto”).

Che cosa si proverebbe a essere la nostra immagine allo specchio? (Si veda il
film *Journey to the Far Side of the Sun*).

Che cosa si proverebbe a essere il fratello di Chopin (che non ne aveva)? O
l’attuale re di Francia?

Che cosa si prova a essere una persona sognata? A essere una persona sognata nel
momento in cui suona la sveglia? A essere Holden Caulfield? A essere il
sottosistema del cervello di J.D. Salinger che rappresenta il personaggio di
Holden Caufield?

Che cosa si prova a essere una molecola? Un insieme di molecole? Un microbo? Una
zanzara? Una formica? Un formicaio? Un’arnia? La Cina? Gli Stati Uniti? Detroit?
La General motors? Il pubblico di un concerto? Una squadra di baseball? Una
coppia sposata? Una mucca con due teste? Due fratelli siamesi? Una persona
commissurotomizzata? Metà di una persona commissurotomizzata? La testa di un
ghigliottinato? O il suo corpo? La corteccia visiva di Picasso?

Il centro del piacere di un ratto? La gamba spasmodica di una rana sezionata?
L’occhio di un’ape? Una cellula della retina di Picasso? Una molecola di DNA di
Picasso?

Che cosa si prova a essere sotto anestesia totale? A essere uccisi da una
scarica elettrica? A essere un maestro Zen che abbia raggiunto uno stato di
satori in cui non esiste più il soggeto (l’“io”, l’ego, il sé)?

Che cosa si prova a essere un ciottolo? Un carillon a vento? Un corpo umano? La
rocca di Gibilterra? La galassia di Andromeda? Dio?

L’immagine evocata dalla frase “Che cosa si prova a essere X?” ècosì seducente e
tentatrice… E la nostra mente è flessibile, pronta ad accettare questo concetto,
questa idea che “si prova qualcosa a essere un pipistrello”. Inoltre siamo
prontissimi a far nostra l’idea che vi siano cose a “essere” le quali “si prova
qualcosa” – “cose esseribili”, come i pipistrelli, le mucche, le persone; e
altre cose per le quali ciò non vale, come le palle, le bistecche, le galassie
(benché una galassia possa contenere innumerevoli cose esseribili). Qual è il
criterio dell’esseribilità?

Nella letteratura filosofica sono state usate molte espressioni per tentare di
evocare le impressioni giuste di ciò che è veramente l’essere senzienti (una di
queste espressioni è appunto “essere senzienti””. Un termine di uso antico è
“anima”; di questi tempi una parola alla moda è “intenzionalità”; si può anche
ricorrere alla stagionata “coscienza”. Poi ci sono “essere un soggetto”, “avere
un punto di vista”, avere una “ubicazione percettiva” o una “personalità” o un
“sé” o il “libero arbitrio”. Per certe persone le immagini giuste sono suggerite
da “avere una mente”, “essere intelligenti” o il comune e onesto “pensare”.
Nell’articolo di Searle veniva tracciata la distinzione tra “forma” (vuota e
meccanica) e “contenuto” (vivo e intenzionale); per caratterizzare queste
distinzione venivano usate anche le parole “sintattico” e “semantico” (o “privo
di significato” e “significativo”). Tutti i termini di questo vasto assortimento
sono quasi sinonimi: tutti hanno a che fare col problema emotivo se abbia o no
senso proiettare noi stessi sull’oggetto in questione: “Quest’oggetto è
esseribile o no?”. Ma esiste realmente qualche cosa a cui questi termini si
riferiscono?

Nagel dice chiaramente che la “cosa” che lui cerca è un distillato di ciò che è
comune alle esperienze soggettive di tutti i pipistrelli; non è l’insieme delle
esperienze di un pipistrello particolare. Searle potrebbe quindi dire che Nagel
è un “dualista”, dal momento che crede in una qualche astrazione operata a
partire dalle esperienze di tutti quegli individui.

Qualche lume su queste complesse faccende ci viene dalla grammatica delle frasi
che invitano il lettore a eseguire una proiezione mentale. Si consideri per
esempio la differenza fra queste due domande: “Che cosa *si proverebbe* a essere
Indira Gandhi?” e “Che cosa *si prova* a essere Indira Gandhi?”. La domanda al
condizionale ci spinge a proiettarci nella “pelle”, per così dire, di un’altra
persona, mentre la domanda all’indicativo sembra chiedere che cosa prova Indira
Gandhi a essere Indira Gandhi. Si potrebbe sempre chiedere: “Nei termini di chi
bisogna descriverlo?”. Se a cercare di spiegarci che cosa si prova a essere
Indira Gandhi fosse Indira Gandhi, essa potrebbe cercare di spiegarci i problemi
della vita politica indiana riferendosi a elementi della nostra esperienza da
lei considerati vagamente analoghi. Se protestassimo: “No, non traduca dei
*miei* termini! Lo dica nei suoi termini! Mi dica che cosa prova Indira Gandhi,
in quanto Indira Gandhi, a essere Indira Gandhi!”, in tal caso, naturalmente,
tanto varrebbe che essa rispondesse in hindi, lasciandoci la briga d’imparare
questa lingua. Ma anche dopo averla imparata saremmo nella stessa posizione di
milioni di persone di lingua hindi che non hanno idea di che cosa si proverebbe
a essere Indria Gandhi, per non dire di che cosa provi Indria Gandhi a essere
Indria Gandhi…

C’è qualcosa che ha l’aria di essere molto sbagliato in tutto ciò. Nagel dice e
ripete di volere che il suo verbo “essere” sia in effetti senza soggetto. Non
“Che cosa proverei *io* a essere X?”, bensì “Che cosa si prova *oggettivamente*
a essere X?”. Abbiamo qui un “essuto” senza “essente”, per così dire. Forse sarà
meglio tornare alla versione condizionale: “Che cosa si proverebbe ad essere
Indria Gandhi?”. Ma per me o per lei? E dove va la povera Indria, mentre io sono
lei? Oppure, se rovesciamo la situazione “poiché l’identità è una relazione
simmetrica), otteniamo, “Che cosa proverebbe Indria Gandhi ad essere me?”. E
anche qui, dove andrei io se essa fosse me? Ci scambieremmo i posti? Oppure
fonderemmo temporaneamente due “anime” separate in una sola?

Si noti che tendiamo a dire: “Se essa fosse me” e non “Se essa fosse io”. Molte
lingue europee sono piuttosto capricciose con equazioni di questo tipo. Suona
strano usare il nominativo tanto per il soggetto quanto per il complemento
oggetto e si preferisce usare “essere” con l’accusativo come se si trattasse in
qualche modo di un verbo transitivo! “Essere” non è un verbo transitivo, bensì
un verbo simmetrico, eppure la lingua ci allontana da questa visione simmetrica.

Possiamo vederlo nel tedesco, che presenta alternative interessanti per
costruire tali proposizioni assertrici d’identità. Ecco due esempi, tratti con
qualche modifica della traduzione tedesca di un dialogo di Stanislaw Lem, dove
sta per essere costruita una replica esatta, molecola per molecola, di una
persona che sta per morire. Nello stesso spirito, daremo la replica (quasi)
esatta, parola per parola, in inglese e in italiano dell’originale tedesco:

1. *Ob die Kopie wirklich du bist, dafür muss der Beweis noch erbracht werden.*  
   As-to-whether the copy really you are, thereof must the proof still provided be.
   Se la copia davvero tu sei, di-ciò deve la prova ancora fornita essere.

2. *Die Kopie wird behaupten, daß sie du ist.*  
   The copy will claim that it you is.
   La copia sosterrà che essa tu è.

Si osservi che in entrambe le proposizioni dove si asserisce l’identità “la
copia” (o “essa”) viene per prima, poi viene il “tu” e poi il verbo. Ma si noti:
nella prima il verbo è “sei”, il che implica retroattivamente che “tu” era il
soggetto e che “la copia” era il complemento, mentre nella seconda il verbo è
“è”, il che implica retroattivamente che il soggetto era “essa” e il complemento
era “tu”. La posizione finale del verbo dà a queste proposizioni un certo sapore
di conclusione a sorpresa. In inglese e in italiano non è facile ottenere lo
stesso effetto, ma possiamo chiederci quale sia la differenza di sfumatura di
significato delle frasi “La copia è realmente te?” e “Tu sei realmente la
copia?”. Queste due domande “scivolano” nella nostra mente lungo dimensioni
diverse. La prima scivola in quest’altra domanda: “Oppure la copia è in realtà
qualcun altro, o magari non è nessuno?”. La seconda scivola in: “Oppure tu sei
altrove, oppure non sei in nessun posto?”. Il titolo del nostro libro, fra
l’altro, può essere interpretato non sole come un possessivo, ma altrettanto
legittimamente come una breve risposta completa alle due domande “Chi sono io?”
e “Chi è me”.(16) Si noti come l’uso transitivo – che a rigore è un uso
sgrammaticato di “essere” – dia alla seconda domanda un “sapore” affatto diverso
dalla prima.

[D.C.D. a D.R.H.: Se io fossi te, ricorderei anche come sarebbe curioso
permettere a un consiglio “Se tu fossi me, io…”, ma se tu fossi me, io ti
suggerirei di ricordarlo?

Tutti questi esempi dimostrano quanto siamo suggestionabili. Accettiamo senza
batter ciglio l’idea che lì dentro ci sia un’”anima”, una sorta di fiammella che
può accendersi o spegnersi o addirittura essere trasferita da un corpo all’altro
come la fiamma da una candela a un’altra. Se una candela si spegne e viene
riaccesa, è ancora “la stessa fiamma”?. Oppure, se rimane accesa, è sempre “la
stessa fiamma” i ogni momento? La fiaccola olimpica viene mantenuta
costantemente accesa quando, ogni quattro anni, viene trasportata di corsa per
migliaia di chilometri da Atene alla sua destinazione. Vi è un simbolismo molto
forte nell’idea che si tratti “proprio della fiamma che è stata accesa ad
Atene”. La benché minima interruzione della catena distruggerebbe il simbolismo
per chi lo sapesse. Naturalmente per chi non lo sapesse, non ci sarebbe alcun
male! Che importanza potrà mai avere? Eppure da un punto di vista emotivo pare
che ne abbia. Non sarà estinta facilmente, questa idea dell’“anima-fiamma”:
eppure ci trascina in un mare di problemi.

Intuiamo certamente che possono scivolare l’una nell’altra solo cose che abbiano
più o meno “anime delle stesse dimensioni”. Il racconto di fantascienza *Fiori
per Algernon* di Daniel Keyes, narra di un giovanotto ritardato che, in seguito a
una cura medica miracolosa, diventa a poco a poco sempre più intelligente fino a
diventare un grande genio; ma poi si scopre che gli effetti della cura non sono
durevoli, ed “egli” assiste al proprio regresso mentale fino allo stato di
deficienza originario. Questo racconto di fantasia ha un corrispettivo nella
vera tragedia di coloro che, dopo essere cresciuti da uno stato di intelligenza
nulla a uno di normale intelligenza adulta, assistono al proprio decadimento
mentale; o nella tragedia di coloro che subiscono gravi danno al cervello. E
tuttavia sono in grado di rispondere alla domanda “Che cosa si prova quando
l’anima ti sfugge via?” meglio di quanto possa farlo una persona dotata di una
vivida immaginazione?

La *metamorfosi* di Franz Kafka è la storia di un giovane che una mattina si
sveglia trasformato in un gigantesco insetto il quale tuttavia pensa come una
persona. Sarebbe interessante combinare l’idea di *Fiori per Algernon* con quella
della *Metamorfosi* e immaginare le esperienze di un insetto la cui intelligenza
crescesse fino a essere quella di un uomo di genio (e già che ci siamo, perché
non di un superuomo?) e poi ridiscendesse al livello d’insetto. Questa,
tuttavia, è una cosa che ci è praticamente impossibile concepire. Prendendo in
prestito il gergo degli ingegneri elettrotecnici, diremo che l’“adattamento
d’impedenza” delle menti interessate è troppo cattivo. In effetti, l’adattamento
d’impedenza può ben essere il criterio principale per saggiare la plausibilità
di domande del tipo di quelle posta da Nagel. Che cosa è più facile immaginare
di essere: il personaggio del tutto fittizio Holden Caulfield, oppure un qualche
pipistrello reale? Naturalmente è molto più facile proiettarsi su un uomo
fittizio che non su un pipistrello reale: molto più facile, molto più *reale*. Ciò
è un po’ sorprendente. Sembra che il verbo “essere” di Nagel qualche volta si
comporti in un modo molto strano. Forse, come si suggeriva nel dialogo sul test
di Turing, il verbo “essere” viene esteso; forse viene addirittura stirato oltre
i propri limiti!

In tutta questa faccenda c’è qualcosa di molto sospetto. Com’è possibile che una
cosa *sia* qualcosa che *non è*? E come diventerebbe più plausibile questo fatto
quando le due cose potessero “avere esperienze soggettive”? Quasi non ha senso
per noi chiederci cose come: “Che cosa proverebbe quel ragno nero laggiù a
essere quella zanzara prigioniera nella sua ragnatela?”; o peggio ancora: “Che
cosa proverebbe il mio violino a essere la mia chitarra?”; oppure: “Come sarebbe
questa frase se fosse un ippopotamo?”. Ma *per chi*? Per i vari soggetti
interessato, siano essi senzienti o no? Per noi che percepiamo? O, ancora,
“oggettivamente”?

Questo è il punto dove s’incaglia l’articolo di Nagel. Egli vuol sapere se sia
possibile dare, per usare le sue parole, “una descrizione [della natura reale
dell’esperienza umana] in termini accessibili a esseri incapaci di immaginarsi
che cosa si proverebbe a essere noi”. Detta così, nuda e cruda, la cosa suona
come una vistosa contraddizione: e in effetti è questo che Nagel vuol dire. Non
vuole sapere che cosa provi lui a essere un pipistrello: vuole sapere
*oggettivamente* che cosa si provi *soggettivamente*. Non gli basterebbe aver
vissuto l’esperienza di indossare un “casco pipistrellatore”, un casco dotato di
elettrodi che gli stimolassero il cervello procurandogli esperienze da
pipistrello: di aver sperimentato la “pipistrellità”. Questo, in fin dei conti,
sarebbe solo quello che proverebbe Nagel a essere un pipistrello. Che cosa
dunque lo soddisferebbe? Egli non è certo che qualcosa possa soddisfarlo, ed è
questo che lo tormenta. Egli teme che questa idea di “avere delle esperienze
soggettive” si trovi fuori dall’ambito dell’oggettività.

Ora, tra i vari sinonimi di esseribilità elencati sopra, quello che suona forse
più oggettivo è “avere un punto di vista”. In fin dei conti, anche il più
dogmatico degli scettici sull’intelligenza delle macchine probabilmente
attribuirebbe, anche se brontolando, un “punto di vista” a u programma di
calcolatore che rappresentasse alcuni fatti riguardanti il mondo e il proprio
rapporto col mondo. È un fatto indiscutibile che un calcolatore può essere
programmato in modo da descrivere il mondo circostante in termini di un sistema
di riferimento incentrato sulla macchina stessa, come in questa frase: “Tre
minuti fa l’orsacchiotto di pezza era a trentacinque leghe a oriente di qui”. Un
tale sistema di riferimento incentrato intorno al “qui e ora” costituisce un
rudimentale punto di vista “egocentrico”. “Essere qui e ora” è un esperienza
fondamentale per qualunque “io”; d’altra parte, come si può definire il “qui” e
l’“ora” senza far riferimento a un qualche “io”? È inevitabile questa
circolarità?

Riflettiamo un momento sul legame tra “io” e “ora”. Che cosa si proverebbe a
essere una persona cresciuta normalmente, e quindi dotata di capacità percettive
e linguistiche normali, la quale poi, in seguito a una lesione al cervello, non
fosse più in grado di convertire i circuiti neuronici riverberanti della memoria
a breve termine in ricordi a lungo termine? Il senso di esistenza di questa
persona si estenderebbe solo di pochi secondi al di qua e al di là dell’“ora”.
Non avrebbe alcun senso della continuità del sé su ampia scala; nessuna visione
interna di una catena di sé che si estende in entrambe le direzioni del tempo
per costituire un’unica persona coerente.
Quando si subisce una commozione cerebrale, gli istanti immediatamente
precedenti vengono cancellati dalla mente, ed è come se non si fosse mai stati
coscienti di essi. Se qualcuno mi colpisse in testa in questo momento, nel mio
cervello non resterebbe alcuna traccia permanente dell’aver scritto queste
ultime frase. Chi, dunque, ne ha avuto esperienza? Un’esperienza diventa parte
di *noi* solo quando sia stata affidata alla memoria a lungo termine? Chi ha
sognato tutti quei sogni di cui non ricordiamo neppure un frammento?

Proprio come “io” e “ora” sono termini strettamente legati, così lo sono anche
“io” e “qui”. Supponiamo di fare adesso l’esperienza della morte in un modo
piuttosto curioso. Quelli di noi che in questo momento non sono a Parigi sanno
che cosa si provi a essere *morti a Parigi*: niente luci, niente suoni, niente
di niente. Lo stesso vale per Timbuctu. In effetti noi siamo morti
*dappertutto*… tranne che in una piccola zona. Si pensi quanto poco ci manca per
essere morti dappertutto! E siamo anche morti in tutti gli altri momenti che non
siano questo *preciso momento*. Il piccolo frammento di spazio-tempo in cui
siamo vivi non si trova *per caso* dove si trova ora il nostro corpo: esso è
definito dal nostro corpo e dal concetto di “ora”. In tutte le lingue esistono
parole che contengono un ricco insieme di associazioni con “qui” e “ora”, in
parole come “io”, “me” e così via.

Oggi capita spesso di programmare il calcolatore e impiegare parola come “io” e
“me” e “mio” per descrivere i propri rapporti col mondo. Naturalmente dietro
queste parole non deve necessariamente esserci un concetto del sé molto
elaborato; però può esserci. In sostanza, qualsiasi sistema rappresentazionale
fisico, del tipo definito nelle Riflessioni su “Preludio e… mirmecofuga”, è
l’espressione di un qualche punto di vista, per quanto modesto. Questo legame
esplicito tra “avere un punto di vista” e “essere un sistema rappresentazionale”
ci consente ora di fare un passo avanti nella riflessione sull’esseribilità,
poiché se potremo identificare gli esseribili con quei sistemi
rappresentazionali fisici il cui repertorio di categorie sia abbastanza ricco e
i cui ricordi delle linee d’universo siano abbastanza ben rubricati, avremo resa
oggettiva almeno una parte della soggettività.

Si deve osservare che quello che c’è di strano nell’idea di “essere un
pipistrello” non è che i pipistrelli percepiscono il mondo esterno in modo
stravagante: è che i pipistrelli hanno manifestamente un insieme di categorie
concettuali e percettive ridottissimo rispetto a quello di noi uomini. In un
certo senso le modalità sensoriali sono sorprendentemente intercambiabili ed
equivalenti. Per esempio è possibile provocare esperienze visive tanto nei
ciechi quanto nei vedenti servendosi del senso del tatto. Contro la schiena del
soggetto viene appoggiata una griglia di oltre un migliaio di stimolatori
pilotati da una telecamera; le sensazioni vengono inviate al cervello, dove la
loro elaborazione può provocare esperienze visive. Una donna vedente così
riferisce la propria esperienza di visione protesica.

Ero seduta sulla sedia, bendata; sentivo contro la schiena il freddo dei coni
del convertitore tattovisivo. Dapprima sentii solo ondate di sensazioni: Collins
mi disse che stava soltanto muovendo la mano davanti a me, per abituarmi alla
sensazione. Tutt’a un tratto sentii, o vidi, non so quale dei due, un triangolo
nero nell’angolo in basso a sinistra di un quadrato. Mi era difficile
localizzare esattamente la sensazione. Sentivo delle vibrazioni sulla schiena,
ma il triangolo appariva in una cornice quadrata dentro la mia testa. (Nancy
Hechinger, “Seeing Without Eyes”, *Science* 81, 1981, p. 43).

È ben nota la possibilità di trascendere in modo analogo le modalità degli
ingressi sensoriali. Come si è fatto notare in un brano precedente, una persona
che porti occhiali prismici che capovolgono tutto può abituarsi benissimo, dopo
due o tre settimane, a vedere il mondo in questa maniera. E, su un piano più
astratto, una persona che impari una lingua straniera continua ad avere
praticamente la stessa esperienza del mondo delle idee.

Quindi, in realtà, non è né il modo in cui gli stimoli vengono trasdotti in
percezioni né la natura del mezzo che costituisce il supporto del pensiero ciò
che rende la “*Weltanschauung* del pipistrello” diversa dalla nostra: è
l’insieme limitatissimo delle categorie, unitamente al diverso accento posto su
ciò che nella vita è importante e ciò che non lo è. È il fatto che i pipistrelli
non sono in grado di formarsi concetti tipo “la Weltanschauung umana” e di farci
sopra dell’umorismo, perché sono troppo indaffarati, essendo sempre nell’ambito
della pura sopravvivenza.

La domanda di Nagel ci costringe a riflettere – e a riflettere molto seriamente
– sulla questione di come si possa mettere in corrispondenza la nostra *mente*
con quella di un pipistrello. Che genere di sistema rappresentazionale è la
mente di un pipistrello? Possiamo immedesimarci con un pipistrello? In questa
prospettiva, la domanda di Nagel appare strettamente collegata al modo in cui un
sistema rappresentazionale ne emula un altro, secondo quanto si è visto nelle
Riflessioni su “Menti, cervelli e programmi”. Apprendiamo qualcosa se domandiamo
a un Sigma-5: “Che cosa si prova a essere un DEC?”. No, sarebbe una domanda
sciocca, e per il motivo seguente: un calcolatore non programmato non è un
sistema rappresentazionale. Anche quando un calcolatore ha un programma che gli
consente di emularne un altro, ciò non gli conferisce il potere
rappresentazionale di affrontare i concetti contenuti in questa domanda. Per
farlo gli occorrerebbe un programma di IA molto avanzato, un programma che, fra
l’altro, potesse usare il verbo “essere” in tutti i modi in cui lo usiamo noi
(compreso il senso esteso di Nagel). La domanda da porre, piuttosto, sarebbe
questa: “Che cosa provi tu, come programma di IA che capisse se stesso, a
emulare un altro programma dello stesso tipo?”. Ma questa domanda comincia a
somigliare molto a quest’altra: “Che cosa prova una persona a immedesimarsi
profondamente in un’altra?”.

Come abbiamo sottolineato in precedenza, gli esseri umani non hanno la pazienza
o le doti di precisione per emulare anche per breve tempo un calcolatore. Quanto
tentano di mettersi nei panni di altri esseribili, gli uomini tendono a
immedesimarsi, non a emulare. Esso “sovvertono” il proprio sistema di simboli
interno adottando volontariamente un insieme di inclinazioni che modificano le
concatenazioni dell’attività simbolica del loro cervello. Non è esattamente come
prendere l’LSD, benché anche questo provochi cambiamento radicali nel modo in
cui i neuroni comunicano tra di loro. L’LSD lo fa con effetti imprevedibili che
dipendono da come essi si diffonde all’interno del cervello, senza che ciò abbia
alcun rapporto con il significato dei simboli. L’LSD agisce sul pensiero più o
meno come farebbe una pallottola sparata nel cervello: nessuno di questi due
corpi estranei tiene nel minimo conto il potere simbolico della materia
cerebrale.

Ma un’inclinazione messa in atto mediante i canali *simbolici* – “Fammi un po’
pensare a che cosa si proverebbe a essere un pipistrello” – instaura un contesto
mentale. Tradotto in termini meno mentalistici e più fisici, il tentativo di
proiettare noi stessi nel punto di vista di un pipistrello attiva certi simboli
del nostro cervello. Questi simboli, finché rimangono attivati, contribuiscono
alle strutture di attivazioni di tutti gli altri simboli che vengono attivati. E
il cervello è abbastanza complesso da riuscire a trattare certe attivazioni come
stabili – cioè come *contesto* – con la successiva attivazione di altri simboli in
maniera subordinata. Così, cerchiamo di “pensare da pipistrello”, sovvertiamo il
nostro cervello, stabilendo contesti neuronici che incanalano i nostri pensieri
lungo percorsi diversi da quelli che seguono d’ordinario. (E tanto peggio se non
riusciamo a “pensare da Einstein” quando vogliamo!).

Tutta questa ricchezza, tuttavia, non riesce a portarci fino alla pipistrellità.
Il simbolo del sé di ciascuna persona – il “nucleo personale” o la “gemma” della
personetica di Lem – è diventato, nel corso della sua vita, così vasto,
complicato e idiosincratico che non è più in grado di assumere, come un
camaleonte, l’identità di un’altra persona o di un altro essere. La sua storia
individuale è troppo avvinta in quel minuscolo “nodo” che è il simbolo del sé.

È interessante considerare due sistemo così simili da avere simboli del sé
isomorfi o identici: per esempio una donna e la sua replica atomo per atomo. Se
la donna pensa a se stessa, pensa anche alla sua replica? C’è chi fantastica che
da qualche parte lassù in cielo vi sia un’altra persona identica a lui; quando
pensa a se stesso, costui pensa, senza rendersene conto, anche a questa persona?
A chi sta pensando la persona lassù in questo momento? Che cosa si proverebbe a
essere quella persona? È lui quella persona? Se dovesse scegliere, chi
lascerebbe uccidere, quella persona o se stesso?
L’unica cosa che Nagel nel suo articolo sembra non aver riconosciuto è che il
linguaggio (fra le altre cose) è un ponte che ci consente di penetrare in un
territorio che non è il nostro. I pipistrelli non hanno alcuna idea di “che cosa
si provi a essere un altro pipistrello” e nemmeno si pongono il problema. E la
ragione è che i pipistrelli non hanno una moneta universale per lo scambio delle
idee, che a noi invece è fornita dal linguaggio, dai film, dalla musica, dai
gesti e via dicendo. Questi mezzi ci aiutano nella nostra proiezione, ci aiutano
ad assorbire punti di vista estranei. Mediante una moneta universale, i punti di
vista diventano più *modulari*, più trasferibili, meno personali e
idiosincratici.

La conoscenza è una curiosa mescolanza di oggettivo e di soggettivo. La
conoscenza verbalizzabile può essere trasferita ad altri e condivisa nella
misura in cui le parole realmente “significano la stessa cosa” per persone
diverse. Due persone parlano mai la stessa lingua? Ciò che intendiamo dicendo
“parlano la stessa lingua” è una questione spinosa. Noi accettiamo e diamo per
scontato che le sfumature nascoste e sotterranee del significato non vengono
condivise. Sappiamo, più o meno, che cosa si mantiene e che cosa si perde nelle
transazioni linguistiche. Il linguaggio è un mezzo pubblico per lo scambio delle
esperienze più private. Ogni parola è circondata, in ogni mente, da un ricco e
inimitabile alone di concetti e sappiamo che, per quanto ci sforziamo di
portarlo in superficie, ne perdiamo sempre una parte. Possiamo al massimo darne
un’idea approssimata. (Per una discussione approfondita di tutto ciò, si
veda *After Babel*, di George Steiner).
Grazie ai mezzi per lo scambio dei memi (si veda sopra “Geni egoisti e memi
egoisti”), come il linguaggio e i gesti, *possiamo* sperimentare (talvolta in
modo vicariante) che cosa si provi a essere o a fare *X*. Non è mai una cosa
autentica, ma che cos’è poi una conoscenza autentica di ciò che si prova a
essere *X*? Non sappiamo neppur bene che cosa si provava a essere noi dieci anni
fa: lo possiamo dire solo rileggendo il nostro diario, e anche così, mediante
una proiezione! È sempre un modo vicariante. Peggio ancora, spesso non sappiamo
neppure come abbiamo potuto fare ciò che abbiamo fatto ieri. E tutto sommato, a
pensarci bene, non è neppure tanto chiaro che cosa si provi a essere me in
questo momento.

È il linguaggio che ci caccia in questo problema (permettendoci di vedere la
questione) e che ci aiuta anche a uscirne (i quanto è un mezzo universale di
scambio di pensieri, che consente di rendere condivisibili e più oggettive le
esperienze). Tuttavia esso non può farci arrivare fino in fondo.

In un certo senso, il teorema di Gödel è un corrispettivo matematico del fatto
che io non riesco a capire che cosa si provi a non trovare buono il cioccolato o
a essere un pipistrello, se non attraverso una successione infinita di processi
di simulazione sempre più precisi, che convergono verso l’emulazione senza
tuttavia mai raggiungerla. Sono prigioniero dentro di me e quindi non posso
vedere come sono gli altri sistemi. Il teorema di Gödel deriva da un conseguenza
di questo fatto generale: sono prigioniero dentro di me e perciò non posso
vedere come gli altri sistemi mi vedono. Quindi i dilemmi concernenti
soggettività e oggettività posti da Nagel con tanta acutezza sono in qualche
modo collegati ai problemi epistemologici tanto della logica matematica quanto,
come abbiamo visto in precedenza, dei fondamenti della fisica. Queste idee sono
esposte con maggiore ampiezza nell’ultimo capitolo di *Gödel, Escher, Bach*, di
Hofstadter.

**D.R.H.**

1. Può darsi che robot siffatti non possano esistere. Forse qualunque cosa
   abbastanza complessa da comportarsi come una persona avrebbe esperienze
   soggettive. Ma se ciò fosse vero, non potremmo scoprirlo mediante la sola
   analisi del concetto di esperienza soggettiva.
2. Esso non è equivalente a ciò in cui siamo incorreggibili, sia perché non
   siamo incorreggibili per quanto riguarda l’esperienza soggettiva sia perché
   l’esperienza soggettiva è presente in animali privi di linguaggio e di
   pensiero, che non hanno credenze o opinioni sulle loro esperienze.
3. Quando dico “la nostra situazione” non intendo semplicemente “la mia
   situazione”, ma piuttosto quelle idee mentalistiche che noi applichiamo senza
   porci troppi problemi a noi stessi e agli altri esseri umani.
4. Quindi la forma analogica dell’espressione inglese *“what is like”* [qui e
   altrove tradotta “che cosa si prova”, ma alla lettera: “a che cosa è simile”
   è fuorviante, perché ciò che ci chiediamo non è: “a che cosa*somiglia* (nella
   nostra esperienza)”, bensì “com’è per il soggetto stesso”.
5. Qualunque extraterrestre intelligente del tutto diverso da noi.
6. Superare le barriere interspecifiche con l’ausilio dell’immaginazione è forse
   più facile di quanto non si creda. Per esempio, i ciechi sono capaci di
   rivelare oggetti vicini mediante una specie di sonar, schioccando la lingua o
   battendo un bastone. Forse, sapendo che cosa si prova in questi casi, si
   potrebbe per estensione immaginare grossomodo che cosa si proverebbe a usare
   il sonar tanto più raffinato di un pipistrello. La distanza fra un individuo
   e le altre persone o le altre specie può cadere in un punto qualunque di un
   continuo. Anche nel caso di altre persone la comprensione di che cosa si
   prova a essere loro è solo parziale e quando si passa a specie molto diverse
   da noi ci può essere ancora un comprensione parziale, sia pure minore.
   L’immaginazione è assai flessibile. Ciò che voglio dire, tuttavia, non è che
   noi non possiamo *sapere* che cosa si provi a essere un pipistrello. Non sto
   sollevando questo problema epistemologico: ciò che voglio dire è che anche
   solo per formarsi un idea di ciò che si prova a essere un pipistrello (e a
   fortiori per sapere che cosa si prova a essere un pipistrello) si deve
   assumere il punto di vista del pipistrello. Se si riesce ad assumerlo in modo
   approssimativo o parziale, anche l’idea conseguente sarà approssimativa o
   parziale. Almeno così sembra nello stato in cui ora comprendiamo questo
   problema.
7. Il problema che sto per sollevare può quindi essere posto anche se la
   distinzione tra descrizioni o punti di vista più soggettivi o più oggettivi
   può essere fatta a sua volta solo entro un più ampio punto di vista umano. Io
   non accetto questo genere di relativismo concettuale, ma non è necessario
   respingerlo per giungere alla conclusione che la riduzione psicofisica non
   può trovar luogo nell’ambito del modello dal-soggettivo-all’oggettivo che ci
   è familiare da altri casi.
8. Il problema non è solo che quando guardo *La Gioconda* la mia esperienza
   visiva ha una certa qualità della quale nessuna traccia potrà essere
   trovata da chi guardi dentro il mio cervello. Infatti, anche se costui vi
   vedesse una figuretta della *Gioconda*, non avrebbe alcun motivo per
   identificarla con la mia esperienza.
9. Ci sarebbe dunque un rapporto non contingente, come quello tra una causa e il
   suo effetto distinto: sarebbe necessariamente vero che un certo stato fisico
   produce certe sensazione. Kripke (1972) sostiene che il comportamento causale
   e le analisi del mentale a esso collegate falliscono perché interpretano, per
   esempio, “dolore” come un nome puramente contingente dei dolori. Il carattere
   soggettivo di un’esperienza (“la sua qualità fenomenologica immediata”, la
   chiama Kripke [p. 380]) è la proprietà essenziale che queste analisi
   escludono ed è quella in virtù della quale l’esperienza è, necessariamente,
   quella che è. La mia opinione è molto vicina a quella di Kripke: come lui,
   anch’io ritengo che l’ipotesi che un certo stato cerebrale debba
   *necessariamente* avere un certo carattere soggettivo non è comprensibile se
   non si trovano ulteriori spiegazioni. Ma nessuna spiegazione adeguata può
   emergere dalle teorie che considerano contingente il rapporto mente-cervello:
   forse però ci sono altre alternative, ancora da scoprire.
   Una teoria che spiegazze in che modo il rapporto mente-cervello è necessario
   non risolverebbe ancora il problema di Kripke, che è quello di spiegare
   perché esso appaia nondimeno contingente. A me sembra che si possa superare
   questa difficoltà nel modo seguente. Noi possiamo immaginare una qualche cosa
   rappresentandocela in modo o percettivo, o simpatetico, o simbolico. Non
   tenterò di spiegare come funziona l’immaginazione simbolica, ma dirò in parte
   ciò che accade negli altri due casi. Per immaginare una cosa in modo
   percettivo, ci poniamo in uno stato di coscienza che assomiglia allo stato in
   cui ci troveremmo se la percepissimo. Per immaginare una cosa in modo
   simpatetico ci poniamo in uno stato di coscienza che assomiglia alla cosa
   stessa. (Questo metodo può essere adottato solo per immaginare eventi e stati
   mentali, nostri o altrui). Quanto tentiamo di immaginare uno stato mentale
   che si presenti senza lo stato cerebrale ad esso associato, dapprima
   immaginiamo simpateticamente il presentarsi dello stato mentale: cioè ci
   mettiamo in uno stato che gli assomigli mentalmente. Allo stesso tempo
   tentiamo di immaginare per via percettiva il non presentarsi dello stato
   fisico associato mettendoci in un altro stato non legato al primo, uno stato
   che somigli a quello in cui ci troveremmo se percepissimo i non presentarsi
   dello stato fisico. Dove l’immaginazione degli aspetti fisici è percettiva e
   l’immaginazione degli stati mentali è simpatetica, a noi pare di poter
   immaginare qualunque esperienza che si presenti senza il suo stato mentale
   associato, e viceversa. Il rapporto fra di essi apparirà contingente ancorché
   necessario, a causa dell’indipendenza dei due diversi tipi di immaginazione.
   (Per inciso, se si attribuisce erroneamente all’immaginazione simpatetica il
   funzionamento che è proprio dell’immaginazione percettiva, il risultato è il
   solipsismo: in tal caso infatti, appare impossibile immaginare qualsiasi
   esperienza che non sia la propria).
10. Si veda Davidson (1970); io tuttavia non comprendo l’argomento contro le
    leggi psicofisiche.
11. Osservazioni analoghe valgono per Nagel (1965).
12. Tale questione è anche al centro del problema delle altre menti, il cui
    stretto legame con il problema mente-corpo viene spesso trascurato. Se
    riuscissimo a capire come l’esperienza soggettiva possa avere una natura
    oggettiva, capiremmo anche l’esistenza di soggetti diversi da noi.
13. Non ho definito il termine “fisico”. È chiaro che esso non si riferisce solo
    a ciò che può essere descritto dai concetti della fisica contemporanea,
    poiché ci attendiamo sviluppi ulteriori. Alcuni possono ritenere che non ci
    sia nulla che impedisca di riconoscere prima o poi una natura fisica
    indipendente ai fenomeni mentali. Ma il fisico, qualunque altra cosa si
    possa dire su di esso deve comunque rimanere oggettivo. Quindi, se la nostra
    concezione di fisico si estenderà un giorno fino a comprendere i fenomeni
    mentali, dovrà ascrivere loro un carattere oggettivo, indipendentemente dal
    fatto che ciò avvenga o no mediante una loro analisi in termini di altri
    fenomeni già considerati come fisici. A me tuttavia sembra più probabile che
    i rapporti tra mentale e fisico finiranno per essere espressi da una teoria
    i cui termini fondamentali non potranno essere situati nettamente in nessuna
    delle due categorie.
14. “Lui fa tutto ciò che tu non faresti mai; / e poi mi ama… / Il suo amore è
    vero. / Perché lui non può essere te?”.
15. “Brilla, brilla pipistrello, / chissà mai che fai di bello, / sopra il mondo
    voli voli, / come un piatto in mezzo ai cieli”.
16. Il titolo inglese del libro (*The Mind’s I*) significa infatti “L’io della
    mente”, ma suona anche come se fosse “L’occhio della mente” (The Mind’s
    Eye); potrebbe però anche essere la forma contratta di *“The Mind Is I”*,
    cioè “La mente è io” oppure “La mente è me”, che sono appunto le risposte a
    quelle due domande [*N.d.T.*].

[1]:https://en.wikipedia.org/wiki/Thomas_Nagel
[2]:https://it.wikipedia.org/wiki/Stanza_cinese
[3]:https://it.wikipedia.org/wiki/John_Searle
[4]:http://www.adelphi.it/libro/9788845906329
[5]:https://it.wikipedia.org/wiki/Problema_delle_altre_menti
[6]:https://it.wikipedia.org/wiki/Stanis%C5%82aw_Lem
[7]:https://en.wikipedia.org/wiki/Arkady_and_Boris_Strugatsky
