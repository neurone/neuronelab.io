title: Dialogo dal futuro
date: 2015-12-09 18:58
tags: racconti, fantascienza, filosofia, società
summary: Il seguente frammento è stato ritrovato nel computer di un'astronave generazionale abbandonata, orbitante attorno alla stella Epsilon Eridani.

_Ma se ci sbagliassimo?_

In cosa?

_A fare una società collettiva, in cui tutto è collegato._

Noi non abbiamo mai scelto di creare una società così. Essa è nata grazie alle condizioni in cui ci siamo sviluppati. Solo in epoche recenti, abbiamo potuto constatare come funzioniamo ed in che relazione siamo con tutto ciò che è esterno all’individuo e, quindi, capire ciò che siamo.

_Quindi, non c’è stata la volontà di fare una società come la nostra?_

È emersa spontaneamente. Poi, ci accorgemmo che tutto ciò con cui ogni elemento si relazionava, andava a formare una rete di collegamenti nella quale ogni individuo diventava il risultato dell’interazione fra molti elementi. Non abbiamo scelto: abbiamo compreso il funzionamento, e mentre lo comprendevamo, spariva l’idea di poter commettere qualsiasi genere di violenza e di dominio sugli altri, creando così una società completamente paritaria. Perché la conseguenza diretta di comprendere di essere il risultato di una rete di relazioni, è che qualsiasi azione compiuta sugli altri, si riflette anche su chi l’ha compiuta, cambiandolo, e che l’individuo può essere libero, solo se anche tutti gli altri lo sono. La comprensione della natura e l’organizzazione della società sono andati di pari passo, influenzandosi a vicenda. Non potevamo essere diversi, senza mentire a noi stessi.

_E se qualcuno non volesse essere collegato a tutto?_

Fintanto che un individuo è in relazione con altri e con l’ambiente, per quanto possa sforzarsi, non può non essere collegato. Ogni sua azione influenza ciò che lo circonda, come accade anche per ogni azione che decide di inibire. Lui stesso è influenzato da ciò che lo circonda. Può cercare di convincersi di essere isolato, ma in quel caso, non farebbe che mentire a se stesso o illudersi.

_Se volesse essere isolato, dovrebbe quindi allontanarsi da tutti, eliminare ogni tipo di interazione._

Sì. Ed in quel caso, per via dell’isolamento, diventerebbe un individuo molto diverso da quello che è stato fino a quel momento. La sua mente si chiuderebbe su se stessa, come un serpente che si morde la coda. Una mente, non è un qualcosa che origina i pensieri e plasma l’individuo ma, al contrario, è essa stessa il risultato dell’interazione di numerosi pensieri, i quali nascono dalle percezioni del proprio organismo, che scaturiscono dall’interazione con ciò che è esterno ad esso. Anticamente, esisteva una parola, “io”, che veniva usata dall’individuo per indicare se stesso. Era frutto di una concezione ingenua ed intuitiva della coscienza, quando ancora si riteneva che ogni individuo avesse al suo interno una specie di “fuoco magico” e che esso fosse l’origine di ogni cosa che lo riguardava, pensieri, emozioni, volontà. Ora, quella parola viene utilizzata solo da studiosi della lingua e della storia… e talvolta anche da chi non condivide l’idea, comunemente accettata, che facciamo tutti parte di un sistema connesso e che le coscienze individuali emergono da esso.

_E se avessero ragione loro? Oppure ci fosse qualche altro modo migliore di spiegare la natura e l’organizzazione?_

È possibile. Può darsi che tutto ciò che crediamo di aver scoperto riguardo a questo, sia sbagliato, un’illusione. Al momento, queste sono le teorie più accreditate e che, fino ad ora, non sono ancora state smentite efficacemente. Ma sappiamo dalla storia, che moltissime teorie affermate e comunemente accettate, si sono poi rivelate inesatte e sono state sostituite da altre, o corrette.

_Se fossero sbagliate, la nostra società finirebbe…_

Non sa prevederlo. Forse non sarebbe più la stessa. Cambierebbe. Non sa dire se in modo più o meno efficiente o lungimirante. Proprio per questo motivo, diamo molta importanza a chi la pensa diversamente, agli individualisti e tutte quelle persone che, nell’antichità venivano invece isolate o eliminate. Perché, non importa da quanto tempo esista e quanto sia solida e diffusa l’idea che abbiamo ora. Può sempre rivelarsi inadeguata, irrealistica. Ed è solo grazie a chi non si conforma, che abbiamo la possibilità di accorgercene e di cambiare.

_Ma sarebbe triste se la nostra società pacifica finisse._

È naturale che noi la pensiamo così, perché è da essa che siamo nati ed in essa siamo cresciuti. Ma nell’universo, ogni cosa cambia in continuazione, adattandosi a ciò che lo circonda o, per meglio dire, tutto si adatta continuamente a tutto.

_Quello che hai appena detto, si rifà sempre alla teoria sulla quale si basa la nostra società… Quindi, quella teoria non garantisce la nostra sopravvivenza?_

No, infatti. Di quella teoria, noi siamo solo una manifestazione particolare. Anche se sparissimo, il nostro svanire sarebbe comunque compreso e spiegabile da essa, o dall’eventuale teoria futura che la sostituirà.