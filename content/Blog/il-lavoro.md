title: Il lavoro
date: 2015-04-24 19:44:25 +0200
tags: lavoro, società, anarchia
summary: Ci chiediamo mai in cosa traduciamo “lavoro”? Fin da quando siamo piccoli, veniamo inconsapevolmente indottrinati con delle idee sullo scopo dell'imparare, sul lavoro e sul guadagno. Le diamo per scontate. Diventa difficile, poi riuscire a distaccarsi da esse per osservarle con occhio disincantato.

Ci chiediamo mai in cosa traduciamo “lavoro”? Fin da quando siamo piccoli, veniamo inconsapevolmente indottrinati con delle idee sullo scopo dell'imparare, sul lavoro e sul guadagno. Le diamo per scontate. Diventa difficile, poi riuscire a distaccarsi da esse per osservarle con occhio disincantato.

Studiamo per specializzarci in un compito, ma rimanendo semi-incapaci di fare
moltissime altre cose. Diamo per scontato che poi dovremmo fare uno specifico e
specialistico lavoro che ci servirà per avere il denaro da spendere per
acquistare il prodotto dei lavori di altre persone specializzate in altri
compiti. Abbiamo bisogno di fare questo perché la specializzazione tende a
renderci dei semi-incapaci in tutto il resto. Non siamo in grado di svolgere
altri compiti, perché non siamo mai incoraggiati a farlo e difficilmente ci
viene lasciato il tempo di impararli. Una società come la nostra, che si basa
sullo sviluppo continuo, rende obbligatoria la specializzazione e la
suddivisione dei compiti, purtroppo fino al punto in cui moltissimi lavori
basilari devono venire delegati ad altri.
<!--more-->

Il cosiddetto *libero mercato* è una dittatura, perché esso ci governa senza che
noi si abbia la possibilità di poterlo scegliere o quantomeno di poter deciderne
le modalità ed il funzionamento. Chi critica giustamente il pedo-battesimo,
dovrebbe analogamente rabbrividire pensando a come il sistema del mercato e del
capitale ci vengono imposti. [James G. Ballard][1], in un suo romanzo (non ricordo se
*Regno a venire* o *Millenium people*), affermava qualcosa come "l'atto più
politico che possiamo fare, è scegliere se acquistare o no una lavatrice. Ogni
volta che compriamo, votiamo". Questo è un dominio ancora più insidioso di
quello dei governi e delle religioni istituzionalizzate. Il capitale ed il
mercato governano il lavoro, come lo stato fa con la libertà individuale e la
religione istituzionalizzata con lo spirito. Negli ultimi due casi, le persone
hanno la possibilità di accorgersi che ci potrebbero essere delle alternative.
Nel caso del mercato e dell'attuale gestione del denaro, si da ancora troppo per
scontato che sia l'unico e migliore modo possibile. Complice di questo, è anche
il mito dell'autoregolazione. Nella realtà dei fatti, c'è un [bias][2] causato
dagli individui che hanno maggior potere. In questo, secondo me, i sindacati
sono ancora miopi: non riescono a vedere il quadro d'insieme che permetterebbe
loro di capire come il disagio dei lavoratori dipenda solo sintomaticamente dai
singoli “padroncini” e che l'origine di esso sia congenita al sistema del
mercato e del capitale. Il risultato finale, è che proteggono il lavoratore
dalle vessazioni del padrone più vicino, come se questo fosse un pastore cattivo
ed il lavoratore una pecorella, per poi rimetterla nello stesso recinto, oppure
in quello di un padrone un po' migliore. Così, la pecorella rimane comunque
all'interno del sistema che sfrutta gli animali come lei, ed essa non avrà mai
la possibilità di una vita al di fuori dei recinti. Il sistema che ha fatto
emergere queste situazioni locali di vessazione e sfruttamento, invece, non solo
viene a malapena sfiorato, ma anche costantemente confermato. Vengono curati i
sintomi, ma al tempo stesso, si mantene ben funzionante tutto il sistema che
questi sintomi causa. Neppure la politica e gli stati hanno più la capacità di
influire in modo determinante sul meccanismo, perché le persone che li formano
sono esse stesse inconsapevolmente, pecore e pastori.

È difficile riuscire a ragionare su questo meccanismo e ad uscirne, proprio
perché il modo in cui è costruito tende ad impedirci di farlo, portandosi via la
maggior parte del tempo della nostra vita da svegli. È un sistema che tende ad
auto-preservarsi, nello stesso modo in cui fanno le società, ma anche tutti i
sistemi di potere e dominio. Mi vengono in mente [Momo][3] e [Fahrenheit 451][4].
In quest'ultimo, l'intrattenimento istituzionalizzato ed il lavoro erano
funzionali proprio al non lasciare tempo alle persone di ragionare sulla propria
condizione.

Nel 1932, [Bertrand Russel][5] in *In praise of Idleness* scrisse:
> Vorrei dire, in tutta serietà, che il credere nella virtù del LAVORO causa
> grande danno nel mondo moderno, e che la strada verso la felicità e la
> prosperità consiste in una diminuzione del lavoro programmata. […
> 
> Dall'alba della civiltà fino alla rivoluzione industriale, un uomo poteva, di
> norma, produrre con lavori di fatica poco più di quanto era richiesto per
> sostenere se stesso e la sua famiglia, nonostante la moglie lavorasse
> duramente almento quanto lui, e i suoi figli aggiungessero il loro contributo
> non appena diventavano abbastanza grandi per farlo. Quel piccolo surplus
> oltre il necessario non rimaneva a chi aveva lavorato per produrlo, ma veniva
> espropriato da preti e guerrieri. […
> 
> Un sistema che è durato così tanto ed è terminato tanto recentemente (questo
> saggio è del 1932, NdParkaDude) ha ovviamente lasciato tracce profonde nelle
> opinioni e nel pensiero degli uomini. Molto di quel che prendiamo come un
> dato di fatto circa il desiderio di lavorare è dovuto a questo sistema che,
> essendo pre-industriale, è inadatto al mondo moderno. La tecnologia ha reso
> possibile che il godersi la vita, entro certi limiti, non sia solo
> prerogativa di una piccola classe di privilegiati, ma un diritto equamente
> distribuito nella comunità. La morale del lavoro è una morale da schiavi, e
> il mondo moderno non ha bisogno di schiavitù. […
> 
> Il concetto di dovere, storicamente parlando, è stato usato da chi deteneva
> il potere per indurre gli altri a vivere per l'interesse dei propri padroni
> piuttosto che per il proprio. […
> 
> Il tempo libero è necessario alla civiltà, e in tempi passati  lo si
> garantiva a pochissimi grazie al lavoro dei tanti. Ma gli sforzi dei tanti
> erano preziosi, non perché il lavoro è bene, ma perché il tempo libero è
> bene. E con la tecnologia moderna sarebbe possibile distribuire il tempo
> libero in maniera equa e senza danno per la civiltà.

[1]:https://it.wikipedia.org/wiki/James_Graham_Ballard
[2]:https://it.wikipedia.org/wiki/Bias
[3]:https://it.wikipedia.org/wiki/Momo_(romanzo)
[4]:https://it.wikipedia.org/wiki/Fahrenheit_451
[5]:https://it.wikipedia.org/wiki/Bertrand_Russell
