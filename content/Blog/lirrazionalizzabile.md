title: L'Irrazionalizzabile
date: 2015-01-15 19:57:18 +0100
tags: filosofia
summary: Ultimamente, sembra che moltissime persone si stiano interessando alle questioni religiose, dal punto di vista sociale e filosofico. Vorrei dare un mio contributo alla questione filosofica, partendo da un punto di vista poco discusso.

Ultimamente, sembra che moltissime persone si stiano interessando alle questioni religiose, dal punto di vista sociale e filosofico. Vorrei dare un mio contributo alla questione filosofica, partendo da un punto di vista poco discusso.

Molto spesso le discussioni si fermano sulla possibilità o meno dell'esistenza
di un Dio, limitandosi al solito scontro fra teisti ed ateisti limitata al
commento di qualche testo religioso. Ritenendo poco probabile l'esistenza di un
Dio come quello descritto dai maggiori testi sacri, il discorso che andrò a
fare, sarà più astratto.

Mi si potrebbe dire *"Può esistere un Dio, non concepibile dall’umana ragione".*
Questa affermazione mi pone in una situazione senza via d'uscita. Non mi è
possibile negarla o confermarla, perché l'unico mio mezzo per fare ciò, passa
proprio attraverso la ragione. È un analogo teologico del [primo teorema di Gödel][1].
L'unica cosa che posso fare con una simile affermazione è non rispondere, o
rispondere ["mu"][2], come fosse un [*koan*][3] zen.

La domanda stessa, nelle sue premesse, esclude qualsiasi possibilità di
risposta, in quanto toglie alla persona che deve rispondere l'unico mezzo
a sua disposizione per poter trovare una qualunque risposta, o di immaginare un
tale oggetto.

La stessa idea che abbiamo di *Dio* è frutto della ragione, così come
dell'*esistere*. Perché, quindi, chiamare questo oggetto "Dio", piuttosto che
Squandli, Mapoppi o Tef? "Può esistere un Mapoppi, non concepibile dall'umana
ragione".

Se ciò che cerchiamo è il mistico, non ci è necessario cercarlo in un Dio già
razionalmente codificato da numerosi testi religiosi. La sola idea di un oggetto
non concepibile dall'umana ragione, la ricorsione che essa genera, può suscitare
ammirazione e contemplazione pari e forse maggiore dell'idea di un Dio
codificato ed antropizzato. Allo stesso modo, una simile mistica può essere
trovata in tutto ciò che è infinitamente grande, piccolo, complesso o, appunto,
incomprensibile. La scala delle distanze cosmiche, della densità atomica nella
materia visibile e del rapporto di distanza fra nucleo ed elettroni atomici,
della complessità della vita che emerge da essi tramite numerose intermedie
complessità fra cui quelle cellulari, dell'incomprensibilità di come da della
materia che pare non contenere in sé alcuna informazione riguardo alla mente,
essa vi possa emergere, dell'idea stessa di *emergenza*. L'idea di una mente che
osserva se stessa nell'atto di osservarsi, come due specchi rivolti l'uno verso
l'altro nei quali non si può vedere il centro dell'immagine, ma solo l'infinito
riflettersi del suo confine.  
Il pensiero che qualsiasi persona incontrata racchiuda e sia tutto questo, tutta
la complessità, la sua unicità al pari della tua e che di esse, di queste
complessissime macchine di atomi, da cui emergono molecole, organizzatissime
cellule il cui contenuto e funzionamento si auto organizza senza governo
centrale o programma condiviso, DNA, organi, cervello interfaccia fra corpo,
ambiente e mente, che influenzano e vengono influenzate dal tutto, sia qui ed a
portata di mano, e di essa ce ne siano molte, tutte a loro modo uniche e
racchiudenti tutta questa complessità ed una quantità di informazioni
astronomica.

Ce n'è a sufficienza da restare al tempo stesso atterriti ed affascinati.

![Pale Blue Dot]({filename}/images/pale_blue_dot.png "Pale Blue Dot")

"Da questo distante punto di osservazione, la Terra può non sembrare di
particolare interesse. Ma per noi, è diverso. Guardate ancora quel puntino. E’
qui. E’ casa. E’ noi. Su di esso, tutti coloro che amate, tutti coloro che
conoscete, tutti coloro di cui avete mai sentito parlare, ogni essere umano che
sia mai esistito, hanno vissuto la propria vita. L’insieme delle nostre gioie e
dolori, migliaia di religioni, ideologie e dottrine economiche, così sicure di
sé, ogni cacciatore e raccoglitore, ogni eroe e codardo, ogni creatore e
distruttore di civiltà, ogni re e plebeo, ogni giovane coppia innamorata, ogni
madre e padre, figlio speranzoso, inventore ed esploratore, ogni predicatore di
moralità, ogni politico corrotto, ogni ‘superstar’, ogni ‘comandante supremo’,
ogni santo e peccatore nella storia della nostra specie è vissuto là, su un
minuscolo granello di polvere sospeso in un raggio di sole. La Terra è un
piccolissimo palco in una vasta arena cosmica. Pensate ai fiumi di sangue
versati da tutti quei generali e imperatori affinché, nella gloria e nel
trionfo, potessero diventare i signori momentanei di una frazione di un puntino.

Pensate alle crudeltà senza fine inflitte dagli abitanti di un angolo di questo
pixel agli abitanti scarsamente distinguibili di qualche altro angolo, quanto
frequenti le incomprensioni, quanto smaniosi di uccidersi a vicenda, quanto
fervente il loro odio. Le nostre ostentazioni, la nostra immaginaria autostima,
l’illusione che abbiamo una qualche posizione privilegiata nell’Universo, sono
messe in discussione da questo punto di luce pallida. Il nostro pianeta è un
granellino solitario nel grande, avvolgente buio cosmico. Nella nostra oscurità,
in tutta questa vastità, non c’è alcuna indicazione che possa giungere aiuto da
qualche altra parte per salvarci da noi stessi. La Terra è l’unico mondo
conosciuto che possa ospitare la vita. Non c’è altro posto, per lo meno nel
futuro prossimo, dove la nostra specie possa migrare. Visitare, sì. Colonizzare,
non ancora. Che vi piaccia o meno, per il momento la Terra è dove ci giochiamo
le nostre carte. E’ stato detto che l’astronomia è un’esperienza di umiltà e che
forma il carattere. Non c’è forse migliore dimostrazione della follia delle
vanità umane che questa distante immagine del nostro minuscolo mondo.

Per me, sottolinea la nostra responsabilità di occuparci più gentilmente l’uno
dell’altro, e di preservare e proteggere il pallido punto blu, l’unica casa che
abbiamo mai conosciuto.

**Carl Sagan**

*La “Pale Blue Dot” è una fotografia del pianeta Terra scattata nel 1990 dalla
sonda Voyager 1, quando si trovava a sei miliardi di chilometri di distanza.
L’idea di scattare una foto della Terra dai confini del sistema solare è stata
dell’astronomo e divulgatore scientifico Carl Sagan (New York, 9 novembre 1934 –
Seattle, 20 dicembre 1996).*

[1]:https://it.wikipedia.org/wiki/Teoremi_di_incompletezza_di_Gödel
[2]:https://it.wikipedia.org/wiki/Mu_(buddhismo_Zen)
[3]:https://it.wikipedia.org/wiki/Kōan
