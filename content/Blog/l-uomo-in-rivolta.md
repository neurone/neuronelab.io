title: L'uomo in rivolta
date: 2016-08-30 20:48
tags: recensioni, libri, filosofia, anarchia

[L'uomo in rivolta][1], saggio filosofico di [Albert Camus][2] su rivolta e
rivoluzione.

Ritengo che chiunque si interessi di politica e società non possa assolutamente
trascurare di includere il pensiero anarchico fra le proprie conoscenze. Pena,
l'avere una visione parziale e limitata della società, dell'umanità e delle sue
possibilità.

In questo saggio, Camus analizza e critica in modo estremamente razionale le
grandi ideologie politiche del suo tempo, debitrici della corrente filosofica
nichilista, due delle quali furono rivoluzioni che sfociarono, come sappiamo,
nel comunismo autoritario sovietico e nel nazionalsocialismo e fascismo.

La lettura di questo saggio riesce a dare una chiave di decifratura della
politica, anche odierna, e per via della sua analisi nella complessità
dell'argomento, riporta le solite banali dispute "rossi vs neri" alla dimensione
ingenua che meglio le rappresenta.

Una caratteristica secondo me negativa, la prefazione: scritta con una prosa
che riassume in modo secondo me arzigogolato ciò che l'autore riesce al
contrario a spiegare con limpidezza e linearità.

[1]:https://it.wikipedia.org/wiki/L%27uomo_in_rivolta
[2]:https://it.wikipedia.org/wiki/Albert_Camus
