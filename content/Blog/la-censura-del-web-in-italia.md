title: La censura del web in Italia
date: 2014-02-25 21:12:15 +0100
tags: tecnologia, censura, rete, libertà
summary: Guardando in retrospettiva ciò che mostra il sito Osservatorio censura, di Marco d'Itri, si può affermare che il 2013 è stato, almeno fino ad ora, l'anno in cui la censura del web in Italia ha avuto la maggiore espansione.

Guardando in retrospettiva ciò che mostra il sito [Osservatorio censura][1], di Marco d'Itri, si può affermare che il 2013 è stato, almeno fino ad ora, l'anno in cui la censura del web in Italia ha avuto la maggiore espansione.

![Grafico censura web in Italia]({filename}/images/grafico_censura_it.png "Grafico
censura web in Italia")

Dal grafico si nota chiaramente come si sia passati da circa ottanta richieste
di censure accolte dal 2008 a fine 2013, a circa trecentotrenta a fine 2013.
Duecentocinquanta richieste in un solo anno: circa *il triplo di quanto era
stato fatto nel totale dei quattro anni precedenti*.
<!--more-->
Secondo quanto riporta l'articolo [La censura su internet in Italia: un problema
sottovalutato][2] del gruppo [Autistici/Inventati][3], *"Ad oggi ci sono più di
cinquemila siti che dalla rete italiana sono irraggiungibili senza che la
decisione derivi da una sentenza penale. In sostanza, ai provider italiani viene
chiesto di impedire ai propri clienti di raggiungere una porzione di internet,
ed è ormai pratica comune utilizzare lo strumento censorio per bloccare siti di
filesharing o anche di ecommerce, oltre ai siti di scommesse e di
pedopornografia. [...] Stupisce che non ci sia alcuna consapevolezza diffusa su
questo tema:  a livello tecnico la censura di internet in Italia è equiparabile
a quella in vigore in regimi totalitari, eppure nessuno sembra particolarmente
preoccupato. Ancora più paradossale è il fatto che questa censura agisca spesso
sottotraccia, e  l’indice dei siti proibiti non esista in alcuna forma pubblica
consultabile dai cittadini."*

Il sito *Osservatorio censura* sopra riportato è un primo tentativo di rendere
pubblica la situazione e permettere alle persone di poter consultare degli
elenchi di siti censurati e le eventuali motivazioni che hanno portato
all'oscuramento.

Come viene operata la censura e come aggirarla
----------------------------------------------

Esistono diversi sistemi per aggirare la censura, a seconda del modo in cui è
stata implementata. Attualmente, i metodi utilizzati sono due.

1. **Falsificazione del dominio**  
   I fornitori di connettività configurano i server [DNS][4], che si occupano di
   trasformare il nome pubblico di un sito o servizio, nel corrispondente
   indirizzo IP numerico, in modo che *rispondano falsamente che il sito non
   esiste, oppure redirigano la richiesta verso specifici server web contenenti
   una pagina di errore*. Una sorta di [hijacking][5] di stato.
2. **Intercettazione del traffico verso l'IP**  
   I fornitori di connettività agiscono sulla propria rete in modo che il
   traffico diretto verso l'[indirizzo IP][6], o la classe di indirizzi IP che
   ospitano il sito censurato *non siano inoltrati a destinazione*.

Entrambi i sistemi *sono aggirabili dall'utente*. La manomissione
dei server DNS, può essere scavalcata impostando manualmente dei name server
diversi da quelli forniti dal provider. Alcuni esempi sono i [DNS pubblici di Google][7] ed [OpenDNS][8].

Aggirare l'intercettazione del traffico verso gli IP è leggermente più
laborioso. È necessario passare attraverso una [VPN][9], oppure un sistema di
routing come [TOR][10], che permetta di uscire sulla rete pubblica da dei *nodi
dislocati in paesi diversi da quelli in cui è attivo il blocco*.
Alcuni fornitori di VPN sono i già citati [Autistici/Inventati][11],
[SwissVPN][12] e [IPredator][13]. Questi ultimi due, a pagamento.

Attenzione: Alcuni provider potrebbero *forzare una redirezione* del traffico
DNS verso i propri, impedendo agli utenti di utilizzare dei DNS diversi. Mi è
stato riferito che Vodafone è uno di questi. Di altri provider non ho, al
momento, notizie.  
Per aggirare questa manomissione, è possibile utilizzare TOR o una VPN, per
*dirottarvi le richieste di risoluzione dei nomi* le quali, passando attraverso
un tunnel cifrato, non possono venire intercettate e manipolate dal provider.

Prossimamente ripubblicherò una guida su come configurare TOR, un proxy locale
per il filtraggio delle pagine web, un proxy locale per la cache, ed un DNS di
cache locale che si agganci a TOR, su una distribuzione [Arch Linux][14], ma
adattabile a qualsiasi altra distribuzione.

Considerzioni
-------------

Ritengo che la liberazione dalla censura non possa essere chiesta alle
istituzioni, le stesse a cui viene delegato il potere di darla o toglierla, ma
vada presa individualmente o in associazione fra pari e mantenuta. Non penso che
la libertà possa essere una concessione di terzi, ma piuttosto uno stato di
coscienza, la possibilità di essere padroni di se stessi, che dev'essere
ottenuto individualmente ed autonomamente.  
Non una rivoluzione, ma una ribellione, come suggeriva [Max Stirner][15].

Ben vengano tutti gli strumenti atti ad aggirare la censura. Che siano resi
accessibili ed utilizzabili da chiunque. Che si formino reti sotterranee per la
comunicazione libera.

Vorrei che nessuno si sentisse impotente e circondato da forze e complotti
impenetrabili ed invulnerabili. Abbattere la morale delle persone è anch'esso
uno stratagemma per ammansirle... Vorrei che tutti i maniaci dei complotti lo
capissero: nel loro tentativi di "risvegliare" la conoscenza e l’interesse delle
persone, a causa dei metodi e del modo illogico e poco onesto di comunicare,
finiscono spesso con l’ottenere l’effetto opposto.  
Essi dovrebbero capire che affermazioni eclatanti, nel contenuto e nei toni,
richiedono dimostrazioni e prove altrettanto eclatanti. Quando si è dalla
parte della verità, dovrebbe essere sufficiente esprimersi in modo onesto.
Molte persone sono sufficientemente mature ed intelligenti da poter valutare
correttamente dati veritieri e dimostrazioni logiche e sensate, quando vengono
mostrate in modo sufficientemente corretto ed esauriente.  
Se si vuole che una persona si comporti in modo intelligente, per prima cosa va
trattata come tale.

[1]: https://censura.bofh.it/
[2]: https://cavallette.noblogs.org/2013/01/8143
[3]: https://www.autistici.org/
[4]: https://it.wikipedia.org/wiki/Domain_Name_System
[5]: https://it.wikipedia.org/wiki/Hijacking
[6]: https://it.wikipedia.org/wiki/Indirizzo_IP
[7]: https://developers.google.com/speed/public-dns/docs/using
[8]: https://store.opendns.com/setup/computer/
[9]: https://it.wikipedia.org/wiki/Virtual_Private_Network
[10]: https://it.wikipedia.org/wiki/Tor_(software)
[11]: https://vpn.autistici.org/
[12]: http://www.swissvpn.net/
[13]: https://www.ipredator.se/
[14]: https://www.archlinux.org/
[15]: http://www.ita.anarchopedia.org/Max_Stirner
