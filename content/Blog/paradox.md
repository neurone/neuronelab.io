title: Paradox
date: 2017-01-13 19:43
tags: fantascienza, recensioni, libri

Avrei iniziato il post su questo freschissimo romanzo di [Massimo Spiga][16]
dicendo che sembra il figlio ibrido de [La trilogia degli Illuminati][1] di
[Robert Shea][2] e [Robert Anton Wilson][3], [Hyperion][4] di [Dan Simmons][5]
oppure [La fine dell'Eternità][6] di [Isaac Asimov][7], e la fantascienza
satirico mistica di [Moebius][15], ma non avendo trovato questi autori tra le
fonti dirette d'ispirazione in coda al libro (indirettamente però, il citato
[Hakim Bey][8] di [T.A.Z.][9] si definisce discordiano, come i protagonisti
della trilogia degli Illuminati), credo che le similitudini siano una
coincidenza utile per consigliare il libro a chi ha già apprezzato tutte queste
opere.

Abbiamo _D_, un viaggiatore del tempo, [discordiano][10] e partigiano che combatte
per scardinare una dittatura galattica che si estende retroattivamente a tutte
le ere. Creatura umanoide postumana proveniente da un lontanissimo futuro,
potenziata dalla tecnologia ed in simbiosi con una IA il cui avatar sembra
uscito dritto da un videogioco. Nella sua lotta contro l'avversario che gli sta
dando la caccia, il _Giallo_, si schianta in una zona povera di Roma, incrociando
la sua vicenda con la vita della ragazzina _Perla_ e del vagabondo mistico e
[altermondialista][11] _Tao_.

Una cosa molto interessante è il modo in cui vengono trattati i viaggi nel
tempo. Spessissimo, questo è un tema che tende a far emergere storie e
sceneggiature incoerenti, oppure che necessitano di una rigida struttura per
poter restare in piedi (vedi [La fine dell'Eternità][6]). Nel caso di _Paradox_,
come ci si può immaginare dal titolo, l'autore decide di sporcarsi le mani con i
paradossi e l'alterazione della realtà, narrando d'interventi sull'orlo della
rottura dei principi di causalità, ma nonostante questo, la storia rimane in
piedi, anche per merito della sospensione d'incredulità che riesce a far
mantenere fino alla fine del romanzo.

Fin dalle prime pagine ti rendi conto che il tono è decisamente _punk_, nelle
situazioni, nelle idee che esprime, e nel linguaggio. L'arrivo di D, lo porta
ancor di più nella direzione di un anarchismo cosmico che abbraccia gli ideali
di liberazione quadrimensionalmente. Il ritmo della narrazione è sempre
abbastanza sostenuto e ricorda [Snow Crash][12] di [Neal Stephenson][13], tanto
da farmi pensare che anche Paradox sia stato ideato col mezzo del fumetto in
mente.  La storia, nonostante i piccoli spoiler che questo post e la descrizione
sul sito della [Acheron Books][14], vi stanno rifilando, è ricca di sorprese ed
idee interessanti.

Consiglio la lettura ad appassionati di fantascienza, aderenti alla religione
discordiana, anarchici, transumanisti e punk.

> Roma, oggi. La vita di borgata di Perla, adolescente randagia, si trascina
> fra le rovine di una famiglia disastrata e una suburra popolata da spacciatori,
> tossici e criminali di piccolo cabotaggio. L'unica nota di colore in questa
> periferia cronica e in bianco e nero è il barbone Tao – un mistico che vede
> cose che la feccia del quartiere non può capire. Per esempio il Cubo, un
> fenomeno alieno che appare di tanto in tanto nel cielo livido della città. Ma
> quell'impossibile geometria è l'avanguardia di qualcosa d'altro. Il Cubo
> deflagra contro la borgata, devastandola, e libera le due entità intrappolate
> al suo interno: il feroce e inarrestabile Giallo, e D, il Gatto dei Portali. La
> loro lotta, intrappolata in un loop temporale infinito, è solo la propaggine di
> una guerra universale fra potenze cosmiche impossibili anche solo da
> immaginare. Perla, imprigionata nel cronoclisma scatenato dal Cubo, si troverà
> a combattere per la salvezza della sua famiglia, del suo quartiere e, forse,
> dell'intero universo...
> 
> Massimo Spiga, figlio ibrido e pazzo dei fumetti lisergici di Grant
> Morrison e dei saggi acidpunk di Hakim Bey e di Marco Philopat, disgrega la
> fantascienza e la ricompone in un mutante narrativo autodivorante, ambientando
> un calibratissimo delirio burroughsiano in una borgata dei Ragazzi di Vita di
> Pier Paolo Pasolini.

Link a: [Paradox su Acheronbooks][14]

[1]: https://it.wikipedia.org/wiki/Trilogia_degli_Illuminati
[2]: https://it.wikipedia.org/wiki/Robert_Shea
[3]: https://it.wikipedia.org/wiki/Robert_Anton_Wilson
[4]: https://it.wikipedia.org/wiki/Hyperion_(romanzo)
[5]: https://it.wikipedia.org/wiki/Dan_Simmons
[6]: https://it.wikipedia.org/wiki/La_fine_dell%27eternit%C3%A0
[7]: https://it.wikipedia.org/wiki/Isaac_Asimov
[8]: http://ita.anarchopedia.org/Hakim_Bey
[9]: http://www.livingeuropa.org/hakim-bey-taz.pdf
[10]: https://it.wikipedia.org/wiki/Discordianesimo
[11]: http://ita.anarchopedia.org/altermondialismo
[12]: https://it.wikipedia.org/wiki/Snow_Crash
[13]: https://it.wikipedia.org/wiki/Neal_Stephenson
[14]: https://www.acheronbooks.com/index.php?id_product=17&controller=product
[15]: https://en.wikipedia.org/wiki/Jean_Giraud
[16]: http://www.massimospiga.it/
