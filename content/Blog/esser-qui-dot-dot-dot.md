title: Esser qui...
date: 2013-11-21 19:31
tags: film, recensioni, filosofia, filosofia della mente
summary: Oltre il giardino, *Being There*, è una commedia molto intelligente ed al tempo stesso, delicata. Il protagonista della vicenda, *Chance*, interpretato da Peter Sellers, è un uomo molto particolare: fin dalla nascita, è vissuto nella villa di un vecchio signore benestante, senza mai uscire, occupandosi solamente della cura del giardino. La sua identità anagrafica è inesistente, non essendo mai stato registrato e non possedendo alcun documento. Oltre a questo, sembra completamente incapace di comprendere ciò che gli accade intorno. Come fosse staccato dalla realtà e la sua mente sia sprovvista di identità propria, priva del sé.

[Oltre il giardino][1], *Being There*, è una commedia molto intelligente ed al tempo stesso, delicata. Il protagonista della vicenda, *Chance*, interpretato da [Peter Sellers][2], è un uomo molto particolare: fin dalla nascita, è vissuto nella villa di un vecchio signore benestante, senza mai uscire, occupandosi solamente della cura del giardino. La sua identità anagrafica è inesistente, non essendo mai stato registrato e non possedendo alcun documento. Oltre a questo, sembra completamente incapace di comprendere ciò che gli accade intorno. Come fosse staccato dalla realtà e la sua mente sia sprovvista di identità propria, priva del sé.

Tutto ciò che Chance conosce proviene dalla televisione. Il vecchio gliene
regalò alcune: tramite quelle, apprese ciò che poteva riguardo alle relazioni
con gli altri.

Questo fece sì che, come nell’esperimento mentale della [Stanza Cinese][3] di
[John Searle][4], Chance fosse in grado di rispondere in maniera corretta alle
domande poste, anche senza comprenderne il significato.

![Being There]({filename}/images/being_there.jpg){: style="float:right"}
A causa della morte del vecchio proprietario, Chance si trova costretto a
lasciare la casa. Una persona con tali difficoltà, non potrebbe sopravvivere da
sola, se non entrassero in gioco dei fattori molto interessanti: le aspettative
della gente e la psicologia del senso comune. Noi tutti *auto-completiamo le
informazioni parziali che costantemente ci giungono, cercando, spesso
inconsciamente, di dare loro un significato: aggiungiamo e correggiamo ciò
che viene omesso, nel tentativo di ricavarne qualcosa di coerente.* La
psicologia del senso comune, sostenuta da [Jerry Fodor][5] e criticata proprio
da [Searle][4] e da [Paul Churchland][6], è un modo di ragionare che risulta
molto naturale ed intuitivo: le azioni altrui vengono spiegate attribuendo loro
atteggiamenti proposizionali. Ovvero: nell’osservare un’azione, si attribuisce
ad essa una credenza. Se vedo Tizio uscire di casa portandosi l’ombrello, lo
spiego attribuendogli la credenza che fuori piova o stia per piovere. La
spontaneità di questo è contrapposta alla facilità di commettere errori di
valutazione, inoltre, fenomeni come le malattie mentali, l’immaginazione
creativa, la natura del sonno, le allucinazioni e l’apprendimento, non possono
venire spiegate tramite la psicologia del senso comune.

Il comportamento di Chance è molto pacato ed educato: appare come una persona
gentile, sensibile ed ingenua. Le sue pause nel rispondere, vengono interpretate
come caratteristiche di una persona molto saggia. Quando esce a sproposito con
dei discorsi riguardo al giardinaggio, vengono interpretati come profonde
metafore, anche se Chance non è in grado di comprendere le metafore: quando
qualcuno utilizza con lui una metafora, una forma allegorica, oppure un doppio
senso, lui le interpreta alla lettera, rispondendo in modo bizzarro.
Solitamente, la persona che gliel’ha rivolta, considera la risposta data come un
frutto del suo senso dell’umorismo. A tal proposito, sono emblematiche le scene
in ascensore in cui Chance ed un maggiordomo dialogano, ognuno dando un senso
diverso a ciò che dice l’altro. E tuttavia, la risposta altrui appare sempre
corretta e sensata, secondo la propria interpretazione.

Chance è un personaggio molto complesso ed ambiguo. Suscita una grande simpatia,
ma anche una malinconica tenerezza, specialmente quando ci si chiede se sia
veramente in grado di comprendere i sentimenti che le altre persone provano per
lui. È tutto un inseguirsi di illusioni ed incomprensioni. Per tutto il film,
non si riesce a capire se quello che Chance mostra, sia frutto di un sé, oppure
sia una risposta “automatica” ad degli stimoli esterni… quanto sia distaccato e
quanto invece sia veramente lì.
*Non è possibile capire se Chance abbia una vera e propria esperienza cosciente,
oppure sia come una "macchina" che reagisce agli stimoli in modi automatici od
appresi.* Potrebbe essere considerato uno [Zombie filosofico][7].

[1]:https://it.wikipedia.org/wiki/Oltre_il_giardino
[2]:https://en.wikipedia.org/wiki/Peter_Sellers
[3]:https://it.wikipedia.org/wiki/Stanza_Cinese
[4]:https://it.wikipedia.org/wiki/John_Searle
[5]:https://en.wikipedia.org/wiki/Jerry_Fodor
[6]:https://en.wikipedia.org/wiki/Paul_Churchland
[7]:https://en.wikipedia.org/wiki/Philosophical_zombie
