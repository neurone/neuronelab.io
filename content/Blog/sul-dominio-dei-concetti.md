title: Sul dominio dei concetti
date: 2016-04-05 18:25
tags: filosofia, società, anarchia
summary: Soltanto la filosofia moderna, da Cartesio in poi, si è data seriamente a condurre il Cristianesimo verso un effetto sicuro, proclamando la “coscienza scientifica” quale unicamente vera e fornita di valore. Perciò essa col dubbio assoluto, col dubitare, dà principio alla “contrizione” della coscienza comune, allontanandola da tutto ciò che non sia legittimato dallo spirito, dal pensare.  Nulla conta per lei la natura, nulla l’opinione degli uomini e le “istituzioni umane”; ed essa non ha tregua sino a tanto che non abbia tutto rischiarato col lume della ragione sì da poter dire: “il reale è il ragionevole, e soltanto ciò che è ragionevole e reale”. Con ciò essa ha finalmente guidato alla vittoria lo spirito, la ragione: ormai tutto è spirito, poi che tutto è ragionevole, così la natura come le più bizzarre opinioni degli uomini; poiché ogni cosa deve servire pel suo meglio , cioè al trionfo della ragione.

Di [Max Stirner][1], da [L'unico e la sua proprietà][2].

Soltanto la filosofia moderna, da **Cartesio** in poi, si è data seriamente a
condurre il **Cristianesimo** verso un effetto sicuro, proclamando la “coscienza
scientifica” quale unicamente vera e fornita di valore. Perciò essa col
**dubbio** assoluto, col **dubitare**, dà principio alla “contrizione” della
coscienza comune, allontanandola da tutto ciò che non sia legittimato dallo
spirito, dal pensare. Nulla conta per lei la **natura**, nulla l’opinione degli
uomini e le “istituzioni umane”; ed essa non ha tregua sino a tanto che non
abbia tutto rischiarato col lume della ragione sì da poter dire: “il reale è il
ragionevole, e soltanto ciò che è ragionevole e reale”. Con ciò essa ha
finalmente guidato alla vittoria lo spirito, la ragione: ormai tutto è spirito,
poi che tutto è ragionevole, così la natura come le più bizzarre opinioni degli
uomini; poiché ogni cosa deve servire pel suo meglio , cioè al trionfo della
ragione.

Il **dubitare** del Cartesio contiene l’affermazione recisa, che il **cogitare**
soltanto, soltanto il pensare sia lo spirito. E ripudiata dunque la coscienza
“comune” che assegnava una realtà alle cose “irragionevoli”! Soltanto il
ragionevole esiste, solo lo spirito esiste! Questo è il principio, nella sua
essenza cristiana, della moderna filosofia. Già **Cartesio** distingueva
rigorosamente il corpo dallo spirito. E il **Goethe** dice che “lo spirito è
quello che si edifica il corpo”.

Ma anche questa filosofia, la cristiana, non sa come liberarsi dal ragionevole e
grida perciò contro quel che è “puramente subbiettivo”, contro le “idee
improvvise, le accidentalità, gli arbitrii” ecc. Non chiede essa forse che il
“divino” si manifesti in ogni cosa, e che ogni coscienza diventi una scienza del
divino, e che l’uomo veda Dio in ogni dove? ma Dio non si trova mai scompagnato
dal **diavolo**.

Per ciò non può dirsi filosofo chi ha bensì gli occhi aperti alle cose del
mondo, uno sguardo chiaro e non velato, un giudizio sereno intorno al mondo, ma
nel mondo non vede che il mondo e negli oggetti i puri oggetti; bensì filosofo è
soltanto colui che nel mondo scorge il cielo, nelle cose terrestri il
soprannaturale, nel mondano il **divino**; e sa dimostrarlo e provarlo. Quegli
che, sia pur dotato dell’intelletto più acuto, proclama la massima: “Ciò che non
vede l’intelletto dell’uomo intelligente, nella sua semplicità lo mette in opera
l’intelletto del bambino, animo infantile occorre per essere riconosciuti
filosofi”, costui non possiede che la coscienza “comune”; invece chi conosce e
sa proclamare il “divino”, ha una coscienza scientifica. Per questa ragione,
**Bacone** fu cacciato dal regno dei filosofi.

Del resto, la filosofia cosiddetta inglese non ha saputo produrre nulla di
meglio delle scoperte dei cosiddetti “spiriti aperti”, **Bacane** e **Hume**.
Gli inglesi non seppero elevare ad un’importanza filosofica “l’animo infantile”,
non conobbero l’arte di creare dagli “animi infantili” dei filosofi.

Ciò vuol dire: la loro filosofia non seppe diventar “teologica”. Eppure soltanto
quale teologia essa può svilupparsi e perfezionarsi interamente. Nella teologia
essa deve contorcersi in disperata agonia. **Bacone** non si curava delle
questioni teologiche e dei punti cardinali.

La vita è invece l’oggetto della conoscenza del pensiero tedesco, poi che
questo, meglio d’ogni altro, sa discendere ai principi ed alle fonti
dell’esistenza, e solo nella conoscenza vede la vita. Il cartesiano “cogito,
ergo sum” significa: “Si vive solo quando si pensa”. Vita di pensiero vuol dire:
“vita spirituale”! Lo spirito solo vive, la vita sua è la vera vita. E così
nella natura le “leggi eterne” (lo spirito) rappresentano la vera vita. Solo il
pensiero, negli uomini come nella natura, vive; tutto il resto e morto ! A
codesta astrazione, alla vita delle generalità o delle cose apparentemente
inanimate si deve giungere facendo la storia dello spirito. Dio, che è spirito,
vive lui solo. Nulla vive all’infuori del fantasma.

Come si può affermare a proposito della filosofia o della civiltà moderna,
ch’esse abbiano conquistato la libertà se esse non ci hanno liberato dal dominio
dell’oggettività? O sono io forse libero, di fronte al despota, se io, pur non
dimostrando timore di lui personalmente, tremo tuttavia di contravvenire alla
venerazione che io credo dovergli essere da me tributata? La stessa cosa è della
civiltà moderna. Essa non fece che mutare gli oggetti “esistenti”, quelli che in
realtà si onoravano, in oggetti **rappresentati**, vale a dire in “concetti”, di
fronte ai quali l’antico rispetto non pure non si dileguò ma anzi s’accrebbe. Se
si prese un po’ in burla Dio ed il diavolo per la rozza materialità con cui
venivano anticamente rappresentati, si prestò tanta maggior attenzione al
concetto ch’era in essi. “Si sono liberati dai malvagi, ma il male è restato”. A
cuor leggero si sconvolse lo Stato, si mutarono le leggi, senza pensarvi più che
tanto, poiché s’era deciso di non sottrarsi all’impero di ciò che realmente
esisteva e si poteva toccare con mano: ma peccare contro il **concetto** dello
Stato, ma ribellarsi al **concetto** della legge, chi mai l’avrebbe osato? In
tal modo si rimase “cittadini dello Stato” uomini “legali” ossequienti alle
leggi: anzi si creddette di dover dimostrare maggior ossequio alle leggi, dopo
aver abolite quelle che apparivano difettose; e lo si fece col rendere omaggio
allo “spirito della legge”. In tutto ciò gli oggetti, solo trasformati, avevano
conservato la loro supremazia; in breve, si era ancora in preda, all’obbedienza
ed all’ossessione, si viveva nella “riflessione” e si aveva un oggetto per la
propria riflessione, oggetto che si rispettava, si venerava, si temeva. Non si
era fatto altro che mutare le cose in **rappresentazioni**, in pensieri cioè e
in concetti, rendendone cosi più intima e indissolubile la **dipendenza**. Cosi,
per esempio, non riesce difficile emanciparsi dai comandamenti dei genitori, o
sottrarsi alle ammonizioni dello zio e della zia, alle preghiere del fratello e
della sorella; ma della negata obbedienza si prova poi subito rimorso, e, quanto
meno noi ci arrendiamo a singole pretese che la nostra ragione ci dice essere
irragionevoli, tanto più teniamo alto il culto della pietà, dell’amore della
famiglia, restii a perdonare a noi stessi l’infrazione del **concetto** che si
ha dell’amor di famiglia e degli obblighi della pietà figliale. Redenti dalla
dipendenza della famiglia esistente, si cade nella dipendenza ancor più
tirannica del concetto della famiglia: si è dominati dallo spirito della
famiglia. Quella famiglia che si componeva di Gianni e Ghita, ecc., la cui
padronanza è divenuta impotente, continua ad esistere mutata nel **concetto**
astratto della famiglia cui si applica l’antico precetto: bisogna obbedire prima
a Dio che agli uomini; ciò che nel nostro caso significherebbe: Io non posso
assoggettarmi alle vostre insensate pretese; ma quale mia “famiglia” voi
continuate ad esser l’oggetto del mio amore e de’ miei pensieri: imperocché la
“famiglia” è un concetto santo, che non è permesso d’offendere.

E questa famiglia che ebbe vita nel mio interno, questa famiglia immateriale
sarà per me quind’innanzi la cosa “santa”, il cui dispotismo sarà le mille volte
più insopportabile, perchè strepiterà senza tregua nella mia coscienza. Questo
dispotismo non può essere infranto, che quando anche il concetto astratto della
famiglia si dissolva nel **nulla**. Le parole del Vangelo; “Donna, che cosa ho
io di comune con te?” _[(1) GIOV., 2, 4.]_; “Io sono venuto a suscitare l’uomo
contro il proprio padre e la figlia contro la madre” _[(2) MATT., 10, 35]_ ed
altre simili, vengono poste in correlazione con la famiglia celeste, con la
**vera** famiglia, e non significano altro fuor che la pretesa dello Stato, per
la quale in caso di conflitto tra esso e la famiglia, è obbligo di obbedire allo
Stato.

Come della famiglia, così è della morale. Molti si staccano dalla morale ma
restano servi della moralità. La moralità è l’idea della morale, è la sua
potenza spirituale, la sua potenza sulle coscienze; mentre la morale è troppo
materiale, per poter dominare lo spirito, e non può assoggettare un uomo
“spirituale”, un cosiddetto “indipendente”, un “libero pensatore”.

Il protestante può dire ciò che vuole; ma “santa” è per lui la “Sacra
Scrittura”, la “parola di Dio”. Chi cessa dal ritenerla “santa” cessa d’essere
protestante. Ma per ciò stesso gli è “sacro” ciò che in lei è “prescritto”:
l’autorità posta da Dio, ecc.

Tutto ciò per lui dev’essere indissolubile, intangibile, “superiore ad ogni
dubbio”, e siccome il “dubbio” è la cosa più naturale all’uomo, tutte quelle
cose vengono riguardate come superiori all’uomo. Chi non sa **liberarsene avrà
la fede**: poiché **credere** significa esser vincolato a qualche cosa. Poiché
nel **protestantesimo** la **fede** si è fatta più pura, anche il servaggio è
divenuto più intimo: tutte quelle cose “sacre”, son divenute parte dell’essere
stesso, “questioni di coscienza”, “sacrosanti obblighi”. Per ciò al protestante
é sacra quella tal cosa dalla quale non sa liberare la sua coscienza, e la
“**coscienziosità**” è la virtù che più di tutte lo distingue dagli altri.

Il protestantesimo ha ridotto l’umanità in uno stato affatto simile alla
“polizia segreta”. La spia continuamente origliante della “coscienza” vigila
ogni moto dello spirito: ogni azione e ogni pensiero, è per lei “questione di
coscienza”. In questo antagonismo tra l’ “istinto naturale” e la “coscienza”
(plebe e polizia interiore) vive il protestante. La ragione della Bibbia (al
posto della cattolica ragion della Chiesa), è tenuta in conto di sacra, e il
sentimento che la parola della Bibbia è sacra si chiama coscienza.

Con ciò si fa **entrare per forza** la santità nella coscienza dell’uomo. Chi
non sa liberarsi dalla coscienza, della cosa sacra, potrà, è vero, agire contro
coscienza, ma giammai indipendentemente dalla coscienza.

Il cattolico si sente soddisfatto, quando ha eseguito un ordine; il protestante
opera secondo la sua “miglior scienza e coscienza”. Il cattolico non è che un
laico, il protestante è sempre “sacerdote”.

Questo perfezionarsi dello spirituale è il progresso segnato dalla **Riforma**
sul Medio Evo, ma ne è anche la maledizione.

Che altro era la morale gesuitica fuorché una continuazione del commercio delle
indulgenze, con questa sola differenza che ormai quegli che otteneva l’indulto
dei peccati, poteva prendere in esame l’indulto che otteneva a persuadersi in
qual modo gli veniva tolto il peccato? Poiché in certi casi determinati (così
dicono i casuisti) non era affatto peccato ciò ch’egli aveva commesso.

Il commercio delle indulgenze s’estendeva a tutti i peccati e a tutte le
contravvenzioni ed aveva fatto tacere tutti gli scrupoli della coscienza. Tutta
la sensualità poteva espandersi a sua posta purché si fosse conquistata a suon
di denari la licenza della Chiesa. Questo favoreggiamento della sensualità fu
continuato dai **Gesuiti**, mentre i protestanti puritani, tetri, fanatici,
smaniosi di penitenze, avidi di mortificazioni e di preghiere, nella lor qualità
di restauratori del **Cristianesimo** null’altro volevano ammettere fuor che
l’uomo spirituale e religioso.

Il cattolicesimo e particolarmente i **Gesuiti** favorirono con ciò l’egoismo e
trovarono persino tra i protestanti un seguito involontario ed incosciente
riuscendo così a salvarsi dalla degenerazione e dalla morte **dei sensi**.

Malgrado tutto lo spirito protestante estende sempre più il suo dominio, e il
gesuitismo (il quale per lui, che si tiene divino, non rappresenta che il
“diabolico” necessariamente inseparabile da tutto ciò che è divino), nonostante
tutti gli sforzi, non può sostenersi in nessuna parte colle proprie forze, e
deve assistere, come avviene in **Francia**, alla vittoria del protestantesimo
nell’ipocrisia borghese, che pone lo spirito al disopra d’ogni altra cosa.

Al protestantesimo vuolsi riconoscere il merito d’aver ricondotto in onore il
“temporale”, per esempio il matrimonio, lo Stato, ecc. Ma per esso il
**temporale** (come il profano) è molto più indifferente che non sia pel
cattolico, il quale permette al mondo profano di esistere, e ne partecipa spesso
ai godimenti, mentre il protestante, ragionevole e logico, s’appresta a
distruggere del tutto ogni cosa che sia mondana. Il che gli succede col
proclamarla semplicemente “sacra”.

Così al matrimonio è stato tolto il carattere naturale, col renderlo “sacro”,
non già nel senso di sacramento cattolico che lo presuppone cosa profana che
dalla Chiesa soltanto riceve la consacrazione, bensì nel senso ch’esso diventa
per sé stesso un non so che di sacro, un sacro legame. Così lo Stato, ecc. Una
volta era il papa che consacrava e benediceva lo Stato e i suoi principi; ora lo
Stato è santo in sé, e tale è pure la maestà senza aver bisogno della
benedizione sacerdotale.

In generale si consacrò l’ordine della natura, ovvero il diritto naturale, il
quale diventò l’ “ordine divino”. Perciò leggiamo, p. es., nella Confessione
d’Augusta, art. 11: “E così atteniamoci al decreto saggio e giusto dei
giureconsulti: che l’uomo e la donna stiano insieme, è diritto naturale. Se è un
diritto naturale, è anche un **ordinamento di Dio** che ha disposto che così
fosse, e per conseguenza è un **diritto divino**“. E che è mai **Feuerbach** se
non un protestante illuminato quando dimostra sacri i rapporti morali, non già
perchè ordinati da Dio, bensì per lo **spirito** che in essi alberga? Ma il
matrimonio, se veramente risulti da una libera unione d’amore, **è per sé stesso
sacro**, per la **natura** dell’unione che viene contratta. **Quel** matrimonio
soltanto è **religioso**, il quale è anche **vero** e corrisponde
all’**essenza** del matrimonio, all’amore.

E così è di tutti i rapporti **morali**. Essi non diventano e non sono morali, e
come tali non vengono tenuti in onore, che quando per **sé stessi sono
riguardati come religiosi**. Vera amicizia non v’é se non la dove i **limiti**
dell’amicizia vengono religiosamente osservati collo stesso fervore religioso
con cui il credente difende la dignità del suo Dio.

“Sacra” è, e dev’essere, per te l’amicizia, sacra la proprietà, sacro il
matrimonio, sacro il benessere d’ogni uomo, ma sacro in **sé, per sé stesso**
_[(1) Essenza del Cristianesimo, pag. 408]_ Questo è un momento molto
essenziale. Nel cattolicesimo le istituzioni mondane possono venir “consacrate”
ed anche “santificate”; ma, senza la consacrazione religiosa, non sono sacre;
mentre nel protestantesimo i rapporti mondani sono “sacri **per sé stessi**“,
sacri unicamente perchè sussistono.

Con la consacrazione che conferisce la santità s’accorda benissimo la massima
gesuitica: “lo scopo santifica i mezzi”.

Nessun mezzo è per sé stesso santo o non santo: bensì i suoi rapporti con la
Chiesa, l’utilità ch’esso ha per la Chiesa, lo rendono tale. Tra questi mezzi
c’è anche il regicidio; se esso era stato compiuto in prò della Chiesa, poteva
esser sicuro d’essere santificato, benché non apertamente.

Pel protestante la maestà è sacrosanta, pel cattolico non era tale che quella
consacrata dal pontefice, anche senza un atto speciale, una volta per tutte. Se
il papa revocasse la sua consacrazione, il re pel cattolico non differirebbe da
un altro uomo qualsiasi.

Se il protestante è intento a trovare anche nelle cose sensuali la “santità”, il
cattolico tende a porre tutto ciò che è sensuale in un luogo appartato, dove, al
pari del resto della natura, continua a conservare il suo valore.

La Chiesa cattolica sottrasse dal proprio Stato consacrato l’istituzione mondana
del matrimonio, e lo vietò ai sacerdoti; la Chiesa protestante, all’incontro,
dichiarò sacro il matrimonio e i legami coniugali, quindi non li giudicò
inadatti per religiosi.

Un gesuita, da buon cattolico, può santificar ogni cosa. Basta p. es. ch’egli si
dica: Io nella mia qualità di sacerdote sono necessario alla Chiesa; ma la servo
con maggior zelo, se posso soddisfare i miei desideri; per conseguenza voglio
sedurre quella ragazza, voglio far perire di veleno questo mio nemico, ecc. Il
mio fine è santo, perchè è il fine d’un sacerdote, quindi santifico i mezzi. In
fin dei conti tutto si risolve in maggior gloria della Chiesa. Perchè il prete
cattolico dovrebbe rifiutarsi ad offrire all’imperatore **Arrigo VII** l’ostia
avvelenata — per la maggior gloria della Chiesa?

I protestanti ortodossi levano alta la voce contro ogni “divertimento innocente”
sostenendo che solo le cose sacre, le spirituali possono essere innocenti. Tutto
ciò in cui non si può dimostrare la presenza dello spirito, deve essere
ripudiato: la danza, il teatro, le pompe (p. es. nelle chiese), ecc.

Di fronte a questo **Calvinismo** puritano il **Luteranesimo** procede di
preferenza sulla via religiosa, vale a dire sulla via spirituale; esso è più
radicale.

Il **Calvinismo** cioè esclude d’un tratto un gran numero di cose, perchè sensuali e
mondane, e **purifica** così la Chiesa; il luteranesimo invece cerca di
**spiritualizzare** quante più cose gli è possibile, e così di far riconoscere lo
spirito quale essenza d’ogni cosa per modo da render **sacro** tutto ciò che è
mondano. Perciò riuscì al luterano Hegel (in un passo d’una delle sue opere egli
dichiara di “voler restar luterano” ) l’attuazione compiuta del pensiero
mediante il tutto. In tutto v’è la ragione: o — in altri termini — “il reale è
ragionevole”. Il reale é, in verità, il tutto, poiché in ogni cosa, persino
nella menzogna, può venir scoperto il vero; non esiste una menzogna assoluta,
come non esiste il male assoluto, e così via.

Grandi opere dello spirito non furono create che dai protestanti, poiché essi
erano i veri discepoli e i veri zelatori dello **spirito**.

Quanto angusto è l’impero dell’uomo! Egli deve permettere che il sole segua il
suo corso, che il mare sollevi le sue onde, che i monti s’ergano verso il cielo.
E così egli si arresta impotente dinanzi all’**invincibile**.

Può egli schermirsi dall’impressione della propria impotenza di contro a questo
accordo colossale? Il mondo è la **legge** immutabile alla quale egli è costretto di
assoggettarsi; essa determina il suo **destino**.

A che cosa intendeva l’umanità precristiana? A rendersi libera dall’imperversare
dei destini, a non lasciarsene alterare. Gli stoici raggiunsero questo fine
coll’apatia durando **indifferenti** gli assalti della natura, senza mostrarsene
turbati. **Orazio** pronuncia il celebre “**Nil admirari**“, con cui egli
manifesta anche l’indifferenza **dell’altro**, del mondo; esso non deve aver
influenza su noi, non deve eccitare la nostra meraviglia. E il suo **impavidum
ferient ruinae** esprime la stessa incrollabilità, di cui parla il salmo 46, 3:
“Noi non temiamo, anche se dovesse crollare il mondo”. Tutto ciò apre la via
alla tesi cristiana che il mondo è vano, sgombra cioè il cammino al **disprezzo
del mondo** proprio dei cristiani.

Lo spirito “incrollabile” del “savio” con cui il mondo antico si adoperava alla
propria affermazione finale, ricevette un tale urto interiore dal quale non
seppe proteggerlo nessuna **atarassia**, e nemmeno il coraggio stoico.

Lo spirito, resosi sicuro contro ogni influenza del mondo, insensibile ai suoi
colpi, e **superiore** ai suoi assalti, deliberato a non ammirare cosa alcuna,
non poteva esser tratto dalla sua indifferenza nemmeno dal crollare del mondo; —
egli traboccava sempre. Imperocché nel suo interno si sviluppavano dei gas
(spiriti) e, cessati gli effetti dell’**urto meccanico** prodotto dal di fuori,
le **tensioni chimiche** eccitate nel suo seno diedero principio alla loro
attività meravigliosa.

Infatti la storia antica finisce il giorno in cui l’uomo acquista nel mondo la
sua proprietà.

“Tutte le cose mi furono consegnate da mio padre” (Matt. II, 27). Il mondo ha
cessato di esser per me ultrapossente, inconcepibile, sacro, divino, ecc.; esso
è “sdivinizzato” ed io lo tratto a mio piacimento, di modo che, s’io potessi far
miracoli, io vorrei esercitare su di esso tutta la mia forza, (cioè la forza
dello spirito), per spostare i monti, ordinare ai gelsi di strappar da sé stessi
le proprie radici dalla terra e di metter radice nel mare” (Luca, 17, 6);
atterrare, insomma, tutto ciò che può esser pensato. Tutte le cose sono
possibili per colui che crede _[(1)MARCO, 9, 23]_. Io sono il **padrone** del
mondo: la sovranità m’appartiene. Il mondo si è fatto prosaico, giacché ciò che
era divino è scomparso; esso è mia proprietà, della quale mi valgo a mio
piacere.

Poiché l’**Io** era assorto al dominio del mondo, l’egoismo aveva celebrato la
sua prima e compiuta vittoria; egli aveva superato il mondo, era divenuto senza
mondo, aveva chiuso sotto chiave le conquiste d’una lunga êra.

La prima proprietà, la prima signoria era stata conquistata!

Ma il signore del mondo non è per ciò ancora il signore dei propri pensieri, dei
suoi sentimenti, della sua volontà; egli non s’è reso ancora padrone e dominator
dello spirito, poiché lo spirito e ancor santo, è lo “spirito santo” e il
cristiano **senza mondo** non saprebbe essere il cristiano **senza Dio**. Se la
lotta antica era diretta contro il **mondo**, quella del Medio Evo cristiano era
combattuta dall’uomo contro **sé stesso** (lo spirito). La prima era una lotta
contro il mondo esteriore, questa fu un combattimento contro il mondo interiore.
L’uomo del Medio Evo è l’ uomo “raccolto in se stesso”, pensante, pensoso. Tutta
la pazienza degli antichi è sapienza **mondana**, cosmologia; quella dei moderni
è sapienza **divina**, teologia.

Del mondo i pagani (anche i giudei tra altri), seppero aver ragione: ma ormai si
trattava di venire a capo di se stessi, di finirla con lo **spirito**, di
diventare, in una parola senza spirito e senza Dio.

Sin da quasi duemila anni noi ci affatichiamo a soggiogare lo spirito santo, e
coll’andar del tempo abbiamo distrutta e calpestata buona parte di santità; ma
il poderoso avversario si risolleva dinanzi a noi perennemente diverso, sotto
forme mutate, sotto nomi ad ora ad ora differenti. Lo spirito non cessò ancora
d’essere divino, non fu ancora sconsacrato, fatto profano. Vero è ch’ei non
aleggia più sulle nostre teste in forma di colomba, non predilige più soltanto i
suoi santi, ma si lascia dar la caccia anche dai laici. Ma col nome di spirito
dell’umanità, di spirito umano, cioè di spirito **dell’uomo**, egli per me e per
te continua ad essere uno spirito straniero, ben lontano ancora dal diventare
nostro esclusivo **possesso**, del quale noi possiamo disporre a nostro piacere.
Tuttavia una cosa è avvenuta certamente, la quale ebbe azione efficace sulla
storia dei tempi che successero ai cristiani; la tendenza cioè ad **umanizzare**
lo spirito, ad avvicinarlo agli uomini, a trasformarlo in umano.

Da ciò seguì ch’esso poté venir riguardato come lo spirito dell’umanità e
rendersi così più simpatico, confidenziale ed accostevole coi nomi di umanità,
umanesimo, amore degli uomini ecc.

Dovremmo credere dunque che ognuno potesse ora possedere lo spirito santo,
accogliere in sé stesso l’idea dell’umanità, incarnata in sé stesso ?

No, lo spirito non è spogliato della sua santità e della sua inaccessibilità,
non è per noi raggiungibile, non è possesso nostro; poiché lo spirito
dell’umanità non è ancora il mio spirito. Può essere un mio **ideale** e come
tale io posso vagheggiarlo in pensiero: è in mio possesso, ed io lo dimostro a
sufficienza col rappresentarmelo come meglio mi piace, oggi così, domani
diversamente, nei modi ad ora ad ora più differenti. Ma, in pari tempo, esso è
un fedecommesso che non mi è lecito alienare, e da cui non posso liberarmi.

Per effetto di lente mutazioni lo spirito santo d’un tempo si trasforma
nell’**idea assoluta**, la quale, a sua volta, per opera di molteplici atti, si
scinde nelle idee di amore del prossimo, di ragionevolezza, di virtù civile,
ecc.

Ma posso io chiamar mia l’idea, se essa è l’idea dell’umanità? Posso io ritenere
d’aver superato lo spirito, se io sono obbligato a servirlo, a “sacrificarmi” a
lui? Gli antichi presero possesso del mondo solo quando n’ebbero infranta la
strapotenza e la “divinità”, e riconosciutane la impotenza e la vanità.

Così è dello spirito. Quando io sono giunto a considerarlo come un **fantasma**
e a vedere nel dominio ch’egli ha su di me un **ramo di follia** da parte mia,
allora esso cessa di esser sacro e divino, allora io mi servo di lui, come senza
scrupoli ed a mio talento mi servo della **natura**.

La “natura della cosa” il “concetto del rapporto” devono servirmi di norma
quand’io tratto quella cosa, quand’io formo quel rapporto. Come se un concetto
della cosa esistesse in sé e non invece dalla cosa derivasse il concetto! Come
se un rapporto, che s’inizia, non fosse unico per il fatto che unico son io che
lo penso! Come se dipendesse dal modo con cui le terze persone lo definiranno!
Ma alla stessa guisa, che si separa l’ “essenza” dell’uomo dall’uomo stesso, e
questo si giudica alla stregua di quella, così si distinguono dall’uomo le sue
azioni e le si apprezzano a seconda del loro “valore umano”. I **concetti**
devono decidere in ogni cosa, regolar l’esistenza, **dominare**.

Questo è il mondo religioso al quale **Hegel** dette un’espressione sistematica
coll’introdurre il metodo in una cosa priva di senso e col codificare i concetti
in modo da ottenerne una dogmatica serrata solidamente costrutta. Tutto in quel
sistema viene misurato alla stregua dei concetti, e l’uomo reale, vale a dire l’
“io”, è costretto a vivere secondo quei concetti. Può darsi una più tirannica
dominazione di leggi? e non ha forse confessato il **Cristianesimo** sin dal bel
principio, ch’esso intendeva stringere ancor maggiormente il freno delle leggi
mosaiche? (“Non una parola della legge deve andar perduta!”).

Il liberalismo non fece che incidere le tavole di altri concetti, umani invece
che divini, e sostituire il concetto dello Stato a quello della Chiesa, ai
religiosi gli **scientifici**, o, per dir meglio, ai “rozzi sistemi e alle
grossolane istituzioni i concetti reali e le leggi eterne”.

Ormai solo lo spirito impera nel mondo e un numero infinito di concetti affolla
i cervelli; ebbene che cosa fanno quelli che tendono a progredire? Essi negano
quei concetti per metterne altri in lor luogo! Essi dicono: voi vi siete formati
un falso concetto del diritto, dello stato, dell’uomo, della libertà,
dell’onore; il vero concetto del diritto, dello stato, dell’uomo, della libertà
dell’onore è quello che noi vi proponiamo. E di questo passo la confusione dei
pensieri s’accresce.

La storia universale ci ha trattati crudelmente e lo spirito ha raggiunto una
forza onnipotente.

Tu sei tenuto a rispettare le mie miserabili scarpe, che potrebbero proteggere i
tuoi piedi nudi; il mio sale, che potrebbe servire a condir le tue patate; e la
mia carrozza di gala, il cui possesso ti trarrebbe dall’indigenza; a tutto ciò
tu non devi tender la mano. Tutte queste ed altre cose senza numero l’uomo è
obbligato a riconoscerle indipendenti, inaccessibili ed intangibili, sottratte
al suo potere. Egli deve rispettarle; e s’egli tenda la mano bramosa verso di
esse, noi diremo subito di lui ch’egli ha le mani “lunghe”.

Quanto miserabilmente scarso è il numero delle cose di cui ci è rimasto il
possesso! Poco più di nulla! Ogni cosa è stata collocata fuor dalla nostra
portata; nessuna cosa possiamo ardire di toccare, se non ci fu **data**; noi non
viviamo che della **carità** del donatore. Tu non puoi raccoglier da terra
nemmeno un ago, se non hai ottenuto da te stesso licenza di poterlo fare. E da
chi deve venirti codesta licenza? Dal **rispetto**! Soltanto quand’esso te la
cede in tua proprietà; solo quando tu puoi **rispettarla** quale cosa tua
propria, tu hai licenza di prendertela.

E, d’altro canto, tu non puoi concepire alcun pensiero, né pronunciare sillaba,
né commettere un’azione, che non ti sian suggerite dalla moralità, dalla ragione
o dall’ umanità. Beata **ingenuità** dell’ uomo concupiscente! Senza
misericordia si tentò di immolarti sull’altare delle “prevenzioni”.

Ma intorno all’altare sorge una chiesa e le sue mura si allargano sempre più.
Ciò ch’esse racchiudono è sacro. A te ne è vietato l’accesso: tu non puoi più
toccare le cose che vi si racchiudono. Gettando grida di dolore a cui ti sforza
la fame tu t’aggiri intorno a quelle mura a raccogliere le poche briciole del
profano, e sempre più s’allarga la cerchia. In breve quella chiesa abbraccerà
tutta la terra, e tu ne sarai respinto al margine estremo; un passo ancora ed
**il mondo “sacro” avrà trionfato**; tu precipiterai nell’abisso. Sollecita
dunque te stesso, finché n’è tempo; non vagare più inutilmente sul terreno già
falciato del profano, spicca il salto e di un balzo entra nel santuario. Quando
avrai **consumato** ciò che è **santo**, tu l’avrai posto in tuo dominio!
Digerisci l’ostia; ne sarai liberato.

[1]:https://it.wikipedia.org/wiki/Max_Stirner
[2]:https://it.wikipedia.org/wiki/L%27Unico_e_la_sua_propriet%C3%A0
