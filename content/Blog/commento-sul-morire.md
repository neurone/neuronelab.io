title: Commento sul morire
date: 2015-10-05 18:49
tags: filosofia, psicologia
summary: Commento in risposta ad un bel post dell'Anacronista.

Commento in risposta ad un [bel post][1] dell'Anacronista.

Quando passeggio o aspetto il treno o l'autobus osservo le persone. Un pensiero
che mi sorprende ancora, è quando realizzo che ogni singola persona è un
universo di complessità, di storie e di profondità. Quella persona che
normalmente classificheremo come "il tizio che ha gettato la cartaccia a terra"
è il proprio centro di una rete di relazioni con altre persone e con l'ambiente
ed il mondo e racchiude diversi scaffali di libri di storie personali. In
sostanza è molto più profondo e radicato nel mondo, di quanto non ci appaia a
prima vista. Forse è proprio questo che della morte sconvolge: quanto viene
*tolto dall'esistente* nel momento in cui una persone smette di vivere.

Nel soggettivo, siamo abituati ad avere relazioni con molti esseri viventi, i
nostri pensieri e le azioni si estendono su di loro e, come in un feedback, i
nostri stessi pensieri ed azioni sono conseguenze di ciò che sono loro, di cosa
fanno o del semplice essere presenti e percepibili. Diventano come
un'estensione di noi stessi e nel momento in cui vengono a mancare, sentiamo
una *sensazione come di amputazione*. Fino a quando l'abitudine della loro
presenza è forte, ci capiterà spesso di agire e pensare come se essi ci
fossero, per scoprire poi, con sconforto, che l'azione ed il pensiero che
facciamo non arriva alle conseguenze che ci siamo abituati ad aspettare. Si ha
quindi una sensazione che potrebbe essere simile a quella dell'"arto fantasma",
nei casi di persone amputate.

[1]:http://taccuinoanacronistico.blogspot.it/2015/10/note-sul-morire.html
