title: La società è troppo complessa per avere un presidente, suggerisce la teoria della complessità
date: 2016-11-18 21:03
tags: società, matematica, scienza, anarchia, politica, transumanesimo, primitivismo

Articolo originale su [Motherboard][1]

Circa due-terzi degli americani [credono che il paese stia andando nella "direzione sbagliata"][2], e giovedì il paese voterà per due fra i [meno popolari candidati di tutti i tempi][3]. Sia la destra che la sinistra affermano che il governo degli Stati Uniti è inefficace.

Una potenziale ragione di ciò? La società umana è [semplicemente troppo complessa][4] perché la democrazia rappresentativa possa funzionare. Gli Stati Uniti probabilmente non dovrebbero neppure avere un presidente, stando ad un'analisi dei matematici del [New England Complex Systems Institute][5].

Il NECSI è un'organizzazione di ricerca che utilizza la stessa matematica degli studi sui sistemi fisici e chimici - seguitemi per un momento - ed i giganteschi insiemi di dati recentemente resi disponibili per spiegare come eventi in una parte del mondo possano influire su qualcosa di apparentemente scollegato in un'altra parte del mondo.

Notoriamente, il direttore dell'istituto, Yaneer Bar-Yam, [previde la Primavera Araba][6] diverse settimane prima che accadesse. Scoprì che delle apparentemente slegate linee di condotta - i sussidi sull'etanolo negli USA e la deregolamentazione dei mercati mondiali delle materie prime - portarono all'impennata dei prezzi del cibo nel 2008 e 2011. Si scoprì che esisteva una correlazione molto chiara tra l'indice dei prezzi del cibo delle Nazioni Unite e le rivolte nel mondo, che nessuno eccetto Bar-Yam ebbe colto.

![Food Price Index][7]  
_I paesi indicati sono dove occorsero rivolte legate al cibo. I numeri tra parentesi sono il numero di morti causati da violenza.  
Immagine: Yaneer Bar-Yam_

Quando consideriamo il nostro sistema di governo, il collegamento tra queste politiche e l'inaspettata violenza globale è illustrativo, ma non unico: Bar-Yam fu capace di descrivere nei dettagli queste relazioni causa-effetto perché osservava dei precisi specifici input e precisi specifici output. Stava zoomando su una specifica parte di quel "sistema" che è la civiltà umana nel tentativo di spiegare una piccola ma importante parte del mondo.

È assurdo, quindi, credere che la concentrazione di poteri in uno o più individui alla cima di una democrazia rappresentativa gerarchica possano essere in grado di operare delle decisioni ottimali su di un vasta gamma di questioni complesse e connesse che avranno certamente delle conseguenze radicali ed impreviste su altre parti della civiltà umana.

"C'è un naturale processo di aumento della complessità nel mondo," mi disse Bar-Yam. "E possiamo riconoscere ad un certo punto, che questo aumento di complessità dovrà essere affrontato dalla complessità dell'individuo. Ed a quel punto, le organizzazioni gerarchiche falliranno."

"Siamo stati cresciuti nella convinzione che la democrazia, ed anche la democrazia che abbiamo noi, sia un sistema che ha intrinsecamente del buono in esso," aggiunge. Ma non è solo la democrazia a fallire. "Le organizzazioni gerarchiche stanno fallendo in risposta alle sfide decisionali. E ciò è vero sia che si parli di dittature, o del comunismo che ebbe un sistema di controllo molto centralizzato, o delle democrazie rappresentative odierne. Le democrazie rappresentative concentrano ancora il potere in uno o pochi individui. E questa concentrazione di controllo e processi decisionali rende questi sistemi inefficaci."

La 'Complessità' della Società Umana
------------------------------------

![Civilization Complexity][8]  
_La società è aumentata in complessità sin dai principi della civilizzazione umana.   
Immagine: Yaneer Bar-Yam_

Quest'idea di una quantificabile, misurabile "complessità", si riferisce alla difficoltà nel descrivere cosa diavolo stia succedendo in un sistema. E Bar-Yam afferma che la società umana è come qualsiasi altro sistema.

Un individuo umano è fatto di atomi, che compongono cellule, che compongono organi, e così via. Descrivere il comportamento di ogni singolo atomo è incredibilmente difficile; descrivere il comportamento degli organi è meno difficile, ed è banale affermare che i miei organi stiano facendo qualcosa ora dentro di me per permettermi in questo momento di scrivere usando un computer. In altre parole, i comportamenti collettivi sono intrinsecamente più "semplici" di quelli individuali. Descrivere il comportamento degli atomi è più complesso che descrivere il comportamento collettivo dei molti atomi che formano un essere umano.

Quest'analogia si estende agli umani che vivono in società. Prevedere lo specifico comportamento di un operaio di una fabbrica automobilistica nella sua vita quotidiana è molto più difficile che prevedere che lui ed un collettivo di altre persone produrranno delle automobili nella fabbrica. "Nelle organizzazioni umane, si verifica il coordinamento perché gli individui influenzano i propri comportamenti l'un l'altro," scrive Bar-Yam in un documento spiegando questa ipotesi. "Una gerarchia di controllo è progettata per permettere ad un singolo individuo di controllare un comportamento collettivo."

Il governo, quindi, è un tentativo di organizzare il comportamento di molti umani individualmente complessi (come gli atomi di cui sopra) in qualcosa di più semplice e coerente.

"Al tempo degli antichi imperi, i sistemi umani su larga scala eseguivano dei comportamenti relativamente semplici, e gli individui svolgevano compiti individuali relativamente semplici che venivano ripetuti da molti altri individui nel tempo ottenendo un effetto su larga scala," aggiunge.

![Historical Progression][9]  
_C-individual è il punto in cui una persona non può prendere decisioni efficacemente. Le democrazie rappresentative non sono gerarchie strette, tuttavia continuano ad accentrare i poteri su pochi individui (il modello "ibrido"). Bar-Yam suggerisce che la società ha bisogno di spostarsi verso un processo decisionale con una mentalità più di squadra per poter affrontare la complessità della società umana._

In altre parole, la natura del mondo relativamente semplice di quel tempo permetteva ad una singola persona di padroneggiare tutti gli aspetti del governo. Il comportamento collettivo di un'intera città, paese, o impero nelle società degli inizi era facilmente descrivibile, perché tutti facevano più o meno le stesse cose.

Il problema qua, è che la pura dimensione ed interdipendenza della società è notevolmente aumentata rispetto ai giorni degli antichi imperi, aumentando la generale complessità della società. Per riportare questo all'analogia biologica, è come se la società si fosse evoluta dall'essere un organismo molto semplice, come un microbo a qualcosa di molto più complesso, come un umano (con ogni probabilità la società diventerà molto più complessa - forse un'analogia migliore sarebbe con qualcosa come una medusa attualmente). Questo è ciò che ci si aspetta - la teoria fisica suggerisce che tutti i sistemi aumentano di complessità col passare del tempo.

> "Siamo diventati fondamentalmente confusi riguardo a quali siano le decisioni e quali le loro conseguenze. E non riusciamo a fare una connessione tra esse."


L'avanzamento tecnologico della rivoluzione industriale ha permesso l'automazione di lavori umili e diversificato il numero di mansioni che gli esseri umani possono svolgere. La rivoluzione industriale ha portato ad avanzamenti nei trasporti e nelle consegne che hanno collegato disparate parti del mondo, ed internet, computer, smartphone, certamente, sono serviti a mescolare quasi ogni angolo del pianeta.

La "società umana" è ora un gigantesco, incredibilmente complesso sistema o organismo invece di esserne molti piccoli, isolati e semlici.

È così che si arriva alle politiche sull'etanolo firmate in America nei tardi anni '90, che portano ad una insurrezione globale decenni dopo. Ci sono, certamente, innumerevoli decisioni ed eventi che hanno un effetto indicibile e difficile da predire su disparate parti del mondo.

La complessità e la presidenza
------------------------------

![Environmental Demands][10]  
_La complessità dei nostri sistemi di governo deve rimanere sul lato buono della linea "sopravvivenza/fallimento"._

[Un quadro proposto][11] dal pioniere della cibernetica Ross Ashby negli anni '50 che servì come base di fondo per il lavoro di Bar-Yam suggerisce che le organizzazioni iniziano a fallire se le richieste poste sopra esse eccedono la complessità della struttura di governo di quell'organizzazione. Quando quella struttura di governo concentra i poteri in una o poche persone alla cima, significa che le richieste poste su quella struttura non possono essere più complesse di quelle che una persona possa gestire.

Nel caso di una democrazia rappresentativa, ci si aspetta che un presidente - aiutato da consiglieri e Congresso, ovviamente - prenda infine delle decisioni in un ambiente che è molto più complicato per qualsiasi singola persona. La democrazia come la conosciamo sta fallendo.

"Non possiamo aspettarci che un individuo sappia come rispondere alle sfide del mondo odierno," dice Bar-Yam. "Quindi che si parli di un candidato o dell'altro, dei Democratici o dei Repubblicani, Clinton contro Trump, la vera domanda è fondamentalmente se saremo capaci di cambiare il sistema."

"Siamo diventati essenzialmente confusi riguardo a quali siano le decisioni e quali le loro conseguenze. E non riusciamo a fare una connessione tra esse," aggiunge. "E ciò è vero per chiunque, così come per chi prende le decisioni e chi legifera. Non sanno quali saranno gli effetti delle decisioni che stanno prendendo."

Bar-Yam propone un sistema organizzato più _lateralmente_ nel quale moltissime piccole squadre si specializzano in certi procedimenti, e queste lavorano assieme per prendere infine le decisioni.

"La finiremo con le persone che affermeranno, 'Io farò questo e le cose andranno meglio.' ed un'altra persona che dirà, 'Io farò questo. E le cose andranno meglio.' e non lo potremo sapere," afferma. "Attualmente il pericolo è quello di scegliere delle strategie che causino realmente moltissima distruzione prima di riuscire a creare l'abilità di avere dei migliori processi decisionali."

Testo integrale della ricerca: [EOLSSComplexityRising.pdf][4]


Divagazione a margine del traduttore
------------------------------------

Da quello che ho avuto modo d'osservare, ciò che viene mostrato in questo articolo può evidenziare anche una delle molte differenze tra l'anarchismo primitivista ed il recente anarchismo transumano. Prendo queste due correnti come estremi dell'approccio verso la scienza e la complessità.

Si riconosce il problema della difficoltà di mantenere una capacità di agire e quindi di libertà in una società complessa ed interconnessa (vedere grafico "Historical Progression"), capacità che viene limitata dalla complessità del sistema che attualmente sembra favorire maggiormente le strutture di controllo statali e corporative, perché in controllo dei mezzi tecnologici che permettono parte di questa interconnessione.

Nell'approccio primitivista si pensa di risolvere il problema spingendo al ritorno verso una società meno complessa ed in cui il controllo su di essa sia più facilmente contrastabile.

In quello transumanista si accetta l'esistenza di questa complessità e del fatto che non sia possibile tornare indietro, quindi si cerca di strappare il controllo su di essa spingendo per una liberazione della conoscenza (abolizione della proprietà intellettuale, promozione del software libero, creazione di laboratori di "makers", decentramento delle reti, uso esteso della crittografia) e dei mezzi tecnologici. Perché, come spiega questo articolo, la complessità, per essere gestita, richiede un sempre più esponenziale sforzo se fatto da un apparato accentrato, mentre al contrario gioca a favore del decentramento e della distribuzione.

Personalmente, ritengo più realistico il secondo approccio. Inoltre, è decisamente più in linea con l'ideale anarchico. Molto spesso vengono citati gli anarchici storici dimenticando il contesto in cui essi vivevano e quindi ciò a cui potevano aspirare. L'anarchismo si è sempre distinto per essere un movimento in cui è caratteristica intrinseca discutersi e mutarsi, adattarsi alla realtà attuale, pensare in un'ottica futura basandosi su tutta la conoscenza disponibile in quel momento storico. Storicamente, l'anarchismo critica la rigidità ideologia, si pensi ad esempio al discorso che fa Max Stirner, poi ripreso anche da Albert Camus, riguardo a come le rivoluzioni finiscando per cristallizzare e diventare tradizionaliste ed al fatto che dentro di esse poi nascano nuovi moti di ribellione all'attuale stato di cose che s'è creato, ed è sorprendente, per me, incontrare così tanti anarchici che ragionano come dei tradizionalisti. L'utopia non è un obiettivo statico, ma in continua mutazione, emergente dal contesto storico, dalle conoscenze e dalle tecniche disponibili. Rivolgere il proprio sguardo al passato o ad una cristallizzazione del presente perché si temono peggioramenti autoritari, non può che essere controproducente ed incoerente. Puntare ad un'utopia statica ed immutabile, la rende distopia man mano che vi si avvicina.

Commenti
--------

**azza** _Giovedì 21 Dicembre 2017_

concordo in pieno, anche io rilevo questa crisi nella variopinta galassia anarchica. conosco diversi primitivisti francamente anche più estremi di quanto traspaia dall'altro tuo articolo "La posizione dei primitivisti verso la scienza".

tuttavia non posso fare a meno di temere il momento in cui saranno i governi, o le grandi corporations, ad ottenere il controllo di questa sorta di psicostoria asimoviana.

---

**Ticy** _Giovedì 21 Dicembre 2017_

ciao! grazie del commento! :)

in parte, questo sta già avvenendo (e mi dispiace per l'ottimismo di Hari Seldon, Giskard e Daneel :) ). il problema è che non lo si può fermare, quindi l'unica salvezza è di diventare più all'avanguardia e più esperti dei loro. è necessario che la conoscenza sia facilmente usufruibile da chiunque e, per questo motivo, secondo me, sono importantissime le lotte per l'abolizione della proprietà intellettuale e la diffusione libera della conoscenza, compreso il movimento per il software libero (cosa sempre più centrale nelle nostre vite, dato che gradualmente, anche non volendolo, ci stiamo dirigendo verso una sorta di algocrazia come è già avvenuto nella finanza).

non a caso, "il capitale" utilizza le leggi sulla protezione della proprietà intellettuale, copyright e brevetti, non per garantire una retribuzione a chi crea opere d'ingegno (scopo per cui si suppone fossero nate), ma per mantenere soggetto a scarsità (e quindi capitalizzabile) un bene, cioè la conoscenza, che per sua natura non può esserlo.

---

**azza** _Giovedì 21 Dicembre 2017_

> il problema è che non lo si può fermare, quindi l'unica salvezza è di diventare più all'avanguardia e più esperti dei loro.

è precisamente quello il punto che mi impedisce qualunque concessione ai primitivisti, nonostante io personalmente sia poco informatizzata e ami grandemente una vita più semplice possibile in un contesto naturale. non si può far finta che il problema non esista, o che sia risolvibile pigiando su "undo" fino al neolitico.

---

**Ticy** _Giovedì 21 Dicembre 2017_

Esatto. Alcune volte mi è capitato di spiegarlo così: "puoi anche ritirarti nella tua casetta in campagna e vivere come si faceva duecento anni fa, ma quando ti manderanno i carrarmati di Terminator, se non saprai come fare per fermarli e non avrai vicino nessuno che lo sappia fare, sarà molto dura per te. Non puoi aspettarti che quelli che chiami oppressori e da cui fuggi, per un qualche spirito di sportività nei tuoi confronti, decidano di non usare tutti i mezzi che hanno a propria disposizione".

Inoltre, se si ha a cuore anche il prossimo, si è anche in qualche modo responsabili verso di esso. C'è chi non si può permettere di rinunciare al lavoro ed agli strumenti tecnologici e non credo sia giusto abbandonarli nella nostra fuga verso un passato mitologico.

Comunque, il commento che avevo scritto in questo post e l'altro post che avevi letto, erano nati in seguito ad una giornata sulla "Liberazione della Terra" a cui partecipavano diversi attivisti radicali, ecologisti ed animalisti. Due dei temi su cui si dibatteva erano una loro "particolarissima" interpretazione del manifesto Cyber-femminista di Donna Haraway e le tecniche di fecondazione artificiale ed assistita.
Ovviamente erano praticamente tutti estremamente ostili a quei temi. Ho ascoltato tante di quelle imprecisioni e ed errori grossolani, che non ho potuto evitare di intervenire in continuazione. Mi avranno odiato.
Lì per lì, credevo che le critiche nascessero ad una spontanea ideologia primitivista, ma ho scoperto successivamente che erano un po' di "seconda mano" in quanto ripetevano quasi pari pari gli argomenti di Alexis Escudero ne "La riproduzione artificiale dell'umano", che ammicca al primitivismo. Non ho mai letto questo libro, e se lo facessi sarebbe per puro masochismo... considerando anche che su Umanità Nova era stato descritto come reazionario e sessista. ...ed in effetti mi ero chiesto più di una volta come mai mi sentivo come se stessi assistendo ad un congresso delle sentinelle in piedi, mentre credevo che mi sarei trovato in mezzo ad anarchici... :D

---

**azza** _Giovedì 21 Dicembre 2017_

ti posso confermare che quel libro è spazzatura, e francamente mi è scaduta anche ortica editrice che l'ha pubblicato.
per quanto mi riguarda, tutti i discorsi sulla fantomatica origine dello sfruttamento dell'uomo sull'uomo, o dell'uomo sull'animale, o dell'uomo sulla natura, sono di interesse puramente accademico. mi piace da matti occuparci oziose serate di chiacchiere con gli amici e una bottiglia di vino, ma non credo siano dati rilevanti per definire una strategia di azione.

a me interessa il presente ed il futuro, e già così sono grane :-P

---

**Ticy** _Venerdì 22 Dicembre 2017_

sì, sono molto d'accorto. idem con i discorsi sull'avere i canini (i nostri ridicoli canini con cui secondo alcuni saremmo paragonabili alle tigri) o meno, sull'essere stati in origine dei raccoglitori opportunisti o dei cacciatori, e via dicendo. utili fino ad un certo punto, considerando che il contesto è completamente diverso ed attualmente siamo raccoglitori di merci dagli scaffali.

i discorsi sull'antropologia, invece, secondo me, sono abbastanza utili, ad esempio per poter discutere di società organizzate diversamente. a volte ci si sorprende che possano esistere società che funzionano pur non essendo identiche alla nostra. tipo: https://pulgarias.wordpress.com/2017/12/15/societa-astatuali-in-africa/

guardare al futuro è una volontà che, mi sembra, si sia un po' persa, anche negli ambienti anarchici. vedo tanti atteggiamenti difensivi, per proteggere quello che si ha nel presente, ma poca immaginazione e voglia di pianificare il futuro. anzi, spesso con un forte rifiuto verso le opportunità offerte dalla scienza e dalla tecnologia. tanto cyberpunk, poco asimov. ma se c'è un autore che mi piacerebbe far leggere a molti anarchici, è Iain Banks col suo Ciclo della Cultura.

---

***Dottor Nomade*** _Mercoledì 6 Settembre 2017_

Grande, ottima traduzione e ottimo argomento. Mi pare da una sbirciatina al manoscritto originale che lo studio sia una sorta di technical report, e presenti un modello qualitativo --voglio scoprire se ci sono altre pubblicazioni dove ha fatto un po' di conti, e sopratutto vedere bene quelle previsioni. Manca il tempo, ovviamente, ma è nella pipeline :)

---

***Tichy*** _Mercoledì 6 Settembre 2017_

Ciao! Sicuramente ci dovrebbe essere lo studio in cui avevano previsto la "primavera araba", ma bisogna cercarselo, perché il link nell'articolo porta a un 404... :)

[1]:https://motherboard.vice.com/read/society-is-too-complicated-to-have-a-president-complex-mathematics-suggest
[2]:http://www.realclearpolitics.com/epolls/other/direction_of_country-902.html
[3]:http://www.usatoday.com/story/news/politics/onpolitics/2016/08/31/poll-clinton-trump-most-unfavorable-candidates-ever/89644296/
[4]:{filename}/pdfs/EOLSSComplexityRising.pdf
[5]:http://necsi.edu/
[6]:http://motherboard.vice.com/blog/a-complex-systems-model-predicted-the-revolutions-sweeping-the-globe-right
[7]:{filename}/images/la-societa-e-troppo-complessa-per-avere-un-presidente1.png
[8]:{filename}/images/la-societa-e-troppo-complessa-per-avere-un-presidente2.png
[9]:{filename}/images/la-societa-e-troppo-complessa-per-avere-un-presidente3.png
[10]:{filename}/images/la-societa-e-troppo-complessa-per-avere-un-presidente4.png
[11]:http://repository.upenn.edu/cgi/viewcontent.cgi?article=1132&context=asc_papers
