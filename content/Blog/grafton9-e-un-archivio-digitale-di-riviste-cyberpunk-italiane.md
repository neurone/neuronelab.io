title: Grafton9 è un archivio digitale di riviste cyberpunk italiane
date: 2017-09-15 13:18
tags: fantascienza, cyberpunk, politica, tecnologia, hacking

Riporto un articolo molto interessante, [apparso qualche giorno fa su Motherboard][1]. Per restare in argomento, su questo blog potrete trovare anche lo storico [Dossier Cyberpunk][2], salvato dal vecchio sito di _Decoder_ e diversi altri articoli a tema _cyberpunk_ e _hacking_ seguendo i relativi tag.

Articolo di **Antonella Di Biase**

**Un'edicola digitale in cui sfogliare rarissime autoproduzioni ribelli degli anni novanta, legate al mondo dell'hacking e non solo.**

I curatori dell'[Archivio Grafton9][3] stanno mettendo in pratica _un esperimento_: dare nuova vita e diffusione ad alcune pubblicazioni del circuito dei centri sociali italiani appartenenti a un periodo che va dalla prima metà degli anni novanta agli anni duemila. Tutto materiale abbastanza introvabile, ormai fuori stampa, che difficilmente si trova in commercio se non in qualche mercatino domenicale o fiera dell'editoria indipendente/vintage. Di solito, è gelosamente custodito nelle librerie di chi quel periodo lo ha vissuto e basta.

La riuscita dell'esperimento dipende da quanto queste riviste rare, dalla grafica esplosiva e dai contenuti di un incazzato di cui si sono perse da tempo le tracce, riescano ad attirare l'attenzione di chi naviga su internet. Sul sito di grafton9, infatti, è possibile trovare le versioni digitalizzate e perfettamente leggibili di autoproduzioni cyberpunk legate al mondo dell'hacking e non solo, con nomi come _Ufologia Radicale, Fikafutura, Stati di Agitazione, MicrosMegma_.

L'iniziativa è di un gruppo di attivisti di base a Bologna che le stanno digitalizzando pagina per pagina, un po' perché il sapere va sempre condiviso — _collettivizzazione_ — un po' perché quei materiali sono oggettivamente molto interessanti sia a livello estetico che di contenuti, un po' perché forse sperano che se [gli spazi fisici della controcultura bolognese][4] stanno andando tutti a puttane, almeno quelli virtuali possono sopravvivere e diffondere dei messaggi ritenuti ancora significativi. Ne abbiamo parlato con uno di loro.

**Motherboard: Come mai 'grafton9'?**  
**Griffo:** Il nome grafton9 deriva da un laboratorio di editoria/libreria/spazio di aggregazione bolognese che ha avuto un'importanza fondamentale per le controculture della città fino al 2003 circa (il sito è ancora online, [qui][5]). Volevamo rendere merito a quell'esperienza che è stata molto importante per la nostra formazione. Gran parte dei testi che abbiamo oggi li abbiamo comprati lì. Era un laboratorio vicino al centro sociale [Livello 57][6] ed era un punto di riferimento per l'editoria indipendente bolognese e nazionale.

<iframe src='https://archive.org/stream/decoder-7?ui=embed#mode/2up' width='600px' height='430px' frameborder='0' ></iframe>

**Voi ufficialmente siete un collettivo, un'associazione, avete un nome?**  
Siamo un gruppo di amici che hanno vissuto quel periodo, quel movimento, e basta. Abbiamo partecipato in prima persona alla redazione di alcune autoproduzioni e ci interessava diffonderle. Ci siamo resi conto del fatto che queste cose erano spesso sconosciute, le nuove generazioni hanno perso un pezzo di questa storia perché i contenuti non erano mai stati pubblicati sul web. Il nostro obiettivo principale era prendere le nostre collezioni private e pubblicarle di modo che fossero leggibili gratuitamente e senza particolari tecnologie. Per sfogliare le digitalizzazioni è sufficiente un browser. Abbiamo usato Internet Archive perché vorremmo dare continuità al progetto quando finiremo di occuparcene: se il sito grafton9.net dovesse chiudere un giorno, tutti i testi rimarranno disponibili e scaricabili da lì.

**[Sul sito][3] ci tenete a specificare che siete archivisti amatoriali, perché?**  
Nel linguaggio comune la parola _archivio_ rende l'idea, ma gli archivi professionali sono dei dispositivi molto più complessi, non solo dei luoghi in cui si accumulano le cose: c'è bisogno di uno spazio fisico, di un collettivo di persone, di una visione politica, di regole ben definite di selezione e catalogazione, di una sostenibilità economica. Noi ci avviciniamo di più al concetto di edicola: per nostra scelta non abbiamo implementato nessuna tecnologia di ricerca nel sito, ci sono solo delle copertine in vista, cliccando si può sfogliare e leggere a schermo. A noi interessa che la gente legga le cose, e perda del tempo a sfogliare le pagine.

<iframe src='https://archive.org/stream/luther-blissett-rivista-di-guerra-psichica-3-1995?ui=embed#mode/2up' width='600px' height='430px' frameborder='0' ></iframe>

**Mi presenteresti le riviste che avete inserito nell'archivio? In base a quali criteri le avete scelte?**  
I criteri sono la rarità del documento, la facilità di digitalizzazione e l'interesse che ha suscitato in noi la lettura. I bollettini ECN Milano sono quelli conservati meglio e dai contenuti molto vari, abbracciavano essenzialmente le tematiche dei movimenti: anonimato, crittografia, antiproibizionismo, antimilitarismo, rifiuto del lavoro e via dicendo. Tra le collezioni più specifiche c'è la collezione Luther Blisset, il foglio di espressione di un movimento che prendeva piede dal situazionismo e la cui costola bolgnese è in parte confluita nei Wu Ming. Oggi, stando ai dati che abbiamo, è la collezione più ricercata.

Poi c'è la collezione Decoder, una rivista milanese uscita in 12 numeri, che era ufficialmente la rivista del cyberpunk italiano. È stata pubblicata da [Shake edizioni][7], l'editore che ha portato in Italia i grandi romanzi del cyberpunk, appunto. È la fanzine più direttamente legata ai temi del digitale, musica elettronica, rave, controcultura. Ed è la più richiesta dagli studenti per tesi, ricerche eccetera. Poi c'è una collezione varia di libri e riviste più generiche. Non guardiamo troppo al contenuto, cerchiamo di digitalizzare le cose più rare, introvabili e significative. Poi c'è anche un aspetto tecnico: spesso i software ce li siamo scritti noi, evitando le tecnologie proprietarie.

<iframe src='https://archive.org/stream/Fikafutura1/fikafutura-1?ui=embed#mode/2up' width='600px' height='430px' frameborder='0' ></iframe>

**Avete seguito una collocazione temporale rigorosa?**  
No, non così rigorosa. Alcuni testi risalgono alla fine degli anni Ottanta, ma essenzialmente il periodo è primi anni novanta/metà anni duemila. I contenuti di questo tipo sono stati i primi interamente prodotti al computer. I movimenti sociali hanno da sempre sperimentato per primi qualsiasi tecnologia per la stampa (a partire dal ciclostile fino alla fotocopiatrice, strumento fondamentale della cultura punk). Con i computer l'intero processo, dalla scrittura all' impaginazione diventava digitale. Noi adesso, paradossalmente, stiamo riportando in digitale delle opere nate in digitale ma poi distribuite su carta. Stiamo operando una digitalizzazione di ritorno.

**Come venivano distribuite in quegli anni, principalmente, nei circuiti?**  
Erano delle fanzine, dei fogli A4 che venivano piegati e distribuiti al pubblico. Nascevano in digitale e poi venivano diffuse su carta. Parliamo di un periodo in cui il web non c'era ancora in Italia, si usavano le BBS, e i centri sociali sono stati uno dei primi soggetti ad adottare queste tecnologie. Ne avevano capito la portata rivoluzionaria. La rete era usata come medium di distribuzione, i contenuti venivano diffusi poi stampati e distribuiti localmente.

<iframe src='https://archive.org/stream/BollettiniEcn/ecn-bollettino-0-gennaio-1995?ui=embed#mode/2up' width='600px' height='430px' frameborder='0' ></iframe>

**Quanto era diffusa, allora, la consapevolezza del fatto che internet fosse un'arma a doppio taglio? Cioè ok il potenziale rivoluzionario, ma il potenziale di controllo?**  
A noi era assolutamente chiaro già da allora. Leggendo i contenuti dei bollettini si capisce che ai nostri occhi internet non era un posto idilliaco, anzi. Si parlava già di sorveglianza, privacy, libertà di espressione, anonimato. Era il periodo del cyberpunk, che da movimento letterario è diventato un movimento politico di sensibilizzazione nei confronti dei problemi legati a queste nuove tecnologie, come per esempio l'esigenza di sviluppare i propri strumenti o di usare la crittografia. Periodo in cui sono nate esperienze come [ECN][8] e in seguito [autistici/inventati][9], ed ancora altre realtà collettive che fanno parte della galassia dell'hacking italiano legato all'attivismo sociale.

**E questi messaggi venivano recepiti?**  
In quegli anni non siamo stati del tutto capiti perché ci rivolgevamo a un pubblico non ancora avvezzo all'uso di certe tecnologie. Noi volevamo acquisire le conoscenze necessarie per sviluppare i nostri stessi strumenti, senza doverci affidare alle multinazionali del software. E le fanzine erano un modo per diffondere conoscenze, per mettere in guardia su certi rischi.

**Ti sembra che ci sia stato un crollo particolarmente tragico, oggi, di movimenti di questo tipo?**  
I centri sociali non sono dei ghetti della società, cambiano insieme a essa. È normale che alcune cose siano cambiate. Il fascino dei centri sociali negli anni novanta si è perso, non c'è stato un grande ricambio generazionale. La creatività che si viveva in quel periodo la si vede riflessa in questi documenti che ripubblichiamo: c'era una forte sperimentazione sui linguaggi, sulle immagini e sulla componente visiva (anche solo i manifesti che si attachinavano in strada erano delle opere da collezione). Oggi il digitale ci ha abituato a strumenti più facili da usare, ma che in un certo senso hanno appiattito la qualità.

[1]:https://motherboard.vice.com/it/article/5998nq/grafton9-e-un-archivio-digitale-di-riviste-cyberpunk-italiane
[2]:{filename}/Blog/dossier-cyberpunk.md
[3]:https://grafton9.net/
[4]:https://noisey.vice.com/it/article/59p8jb/xm24-bologna-gentrification-marnero-lleroy-cani-portici-stormo
[5]:http://www.ecn.org/archivio/grafton9/
[6]:http://www.ecn.org/livello57/html/storial57.html
[7]:http://www.shake.it/
[8]:http://www.ecn.org/xm24/
[9]:https://motherboard.vice.com/it/article/pg399b/autistici-inventati-intervista-collettivo-hacker
