title: L'anima dell'Animale Modello III
date: 2014-01-29 17:16:27 +0100
tags: filosofia, filosofia della mente, fantascienza
summary: “La posizione di Anatol è abbastanza chiara” disse Hunt. “Egli considera la vita biologica come un fenomeno meccanico complesso.”.

di _Terrel Miedaner_ dalla raccolta "L'io della mente"

“La posizione di Anatol è abbastanza chiara” disse Hunt. “Egli considera la vita
biologica come un fenomeno meccanico complesso.”.

Lei alzò le spalle, ma non con indifferenza. “Ammetto di essere affascinata
dall’uomo, ma non posso accettare questa filosofia.”.

“Prova a pensarci” le suggerì Hunt. “Tu sai che, secondo la teoria
neoevoluzionista, il corpo degli animali viene formato attraverso un processo
completamente meccanicistico. Ciascuna cellula è una macchina microscopica, un
minuscolo componente integrato in un congegno più grande e più complesso”.

Dirksen scosse la testa. “Ma il corpo degli animali e degli uomini è qualcosa di
più di una macchina. Già l’atto riproduttivo lo rende diverso”.
<!--more-->
“Perché poi” chiese Hunt “è così straordinario che una macchina biologica generi
un’altra macchina biologica? Concepire e partorire in un mammifero femmina non
richiede più pensiero creativo di quanto ne occorra a una fresatrice automatica
per vomitare blocchi motore”.

Gli occhi di Dirksen lampeggiarono. “Pensi che la fresatrice automatica senta
qualcosa quando partorisce?”, lo provocò.
“Il suo metallo subisce forti tensioni, e alla lunga la fresatrice si logora”.

“Non credo di intendere in questo modo il termine ‘sensazione’ ”.

“Nemmeno io” convenne Hunt. “Ma non è sempre facile sapere chi o che cosa abbia
delle sensazioni o dei sentimenti. Nella fattoria dove sono cresciuto avevamo
una scrofa da riproduzione che aveva l’infelice tendenza a schiacciare sotto il
suo peso la maggior parte dei suoi nati uccidendoli, puri incidenti, immagino.
Poi mangiava i cadaveri dei suoi piccoli. Secondo te aveva sentimenti materni?”.

“Non sto parlando dei maiali!”.

“Potremmo altrettanto parlar bene degli uomini. Che ne diresti di calcolare
quanti neonati vengono annegati nel gabinetto?”.

Dirksen era troppo sgomenta per parlare.

Dopo una pausa Hunt riprese: “Ciò che tu in Klane consideri una fissazione per
le macchine è soltanto una prospettiva diversa. Per lui le macchine sono
un’altra forma di vita, una forma che egli può creare da sé con plastica e
metallo. Ed è abbastanza onesto da considerare se stesso una macchina”.

“Una macchina che genera macchine” ribatté Dirksen sarcastica. “Tra un po’ dirai
che è una madre!”.

“No” disse Hunt. “È un ingegnere. E per quanto rozza sia una macchina costruita
da un ingegnere in confronto al corpo umano, essa rappresenta un atto più
elevato della semplice riproduzione biologica, perché almeno è il risultato di
un processo del pensiero”.

“Non dovrei mettermi a discutere con un avvocato” riconobbe lei, ancora turbata.
“Ma io non provo niente per le macchine! Dal punto di vista emotivo, tra il modo
in cui trattiamo gli animali e quello in cui trattiamo le macchine c’è una
differenza che sfida ogni spiegazione logica. Per esempio, posso benissimo
rompere una macchina senza scompormi ma non sono capace di uccidere un animale”.

“Hai mai provato?”.

“Più o meno” rammentò Dirksen. “L’appartamento che avevo all’università era
infestato dai topi e così ci misi una trappola. Ma quando alla fine ne catturai
uno non fui capace di toglierlo dalla trappola… povera cosina morta, con
quell’aria triste e innocente. Così seppellii trappola e tutto nel cortile e
decisi che vivere coi topi era molto meno sgradevole che ammazzarli”.

“Eppure tu mangi la carne” osservò Hunt. “Quindi non ti ripugna tanto l’atto di
uccidere in sé quanto il farlo tu stessa”.

“Senti” disse lei irritata. “Questo ragionamento non tiene conto di un punto
essenziale, cioè il rispetto fondamentale per la vita. Noi abbiamo qualcosa in
comune con gli animali: questo lo capisci, no?”.

“Klane ha una teoria che forse troverai interessante” insisté Hunt. “Direbbe che
la parentela biologica, reale o immaginaria, non ha nulla a che fare con il tuo
‘rispetto per la vita’. In realtà a te non piace uccidere solo perché l’animale
si oppone alla morte: urla, lotta o ha l’aria triste, insomma ti implora di non
distruggerlo. Ed è la tua mente, tra l’altro, non il tuo corpo biologico, che
sente le implorazioni dell’animale”.

Lei lo guardava poco convinta.

Hunte mise del denaro sul tavolo e spinse indietro la sedia. “Vieni con me”.

Mezz’ora più tardi Dirksen entrava nella casa di Klane in compagnia
dell’avvocato di costui. Il cancello d’entrata era scivolato automaticamente da
parte per far passare la macchina di Hunt, e al suo tocco un servomeccanismo
aveva spalancato la porta d’ingresso priva di chiave.

Hunt la condusse nel laboratorio sotterraneo dove si trovavano numerose vetrine;
ne aprì una e ne estrasse qualcosa che aveva l’aspetto di un grosso scarabeo di
alluminio, la cui superficie levigata era munita di piccoli segnalatori colorati
e di alcune protuberanze meccaniche. Hunt lo capovolse e Dirksen vide nella
parte inferiore tre ruote gomma. Sul fondo del piatto di metallo erano
stampigliate le parole ANIMALE MODELLO III.

Hunt appoggiò l’arnese sulle piastrelle del pavimento e allo stesso tempo azionò
un minuscolo interruttore posto sotto la pancia. Con un lieve ronzio il
giocattolo cominciò a muoversi qua e là sul pavimento come se cercasse qualcosa.
Si fermò un momento, poi si diresse verso una presa di corrente presso la base
di un grande telaio. Si fermò davanti all’attacco, fece uscire due sottili
sporgenze da un’apertura del suo corpo metallico, tastò la sorgente di energia e
vi si inserì. Sul corpo, alcuni segnalatori cominciarono a risplendere di luce
verde e dall’interno giunse una vibrazione sonora, come di un gatto che fa le
fusa.

Dirksen guardava il congegno con interesse. “Un animale meccanico. Grazioso… ma
perché tutto questo?”.
Hunt prese un martello che era sul banco lì vicino e glielo porte. “Ora, per
favore, uccidilo”.

“Ma che stai dicendo?” disse Dirksen un po’ allarmata. “Perché dovrei uccidere…
rompere quella… macchina?”. Arretrò, rifiutandosi di prendere l’arma.

“Solo a titolo di esperimento” rispose Hunt. “Anch’io ho provato qualche anno
fa, per ordine di Klane, e l’ho trovato istruttivo”.

“Che cos’hai imparato?”

“Qualcosa sul significato della vita e della morte”.

Dirksen fissò Hunt con sospetto.

“La ‘bestia’ non ha armi di difesa che ti possano far del male” la rassicurò
lui. “Sta’ solo attenta a non andare a sbattere contro qualcosa mentre la
insegui”. Le porse il martello.

Esitando, essa fece un passo in avanti, prese l’arma, guardò di sottecchi la
strana macchina che ronfava sonoramente mentre succhiava la corrente elettrica.
Le andò vicino, si chinò e sollevò il martello. “Ma… sta mangiando” disse
rivolgendosi verso Hunt.

Hunt scoppiò a ridere. Infuriata, lei afferrò il martello con entrambe le mani,
lo sollevò e lo calò con forza.

Con uno stridio acuto, simile a un grido di paura, al bestia ritirò le mandibole
dalla presa e arretrò di colpo. Il martello ricadde con violenza sul pavimento,
colpendo una piastrella che fino a un istante prima era stata nascosta dal corpo
della macchina. La piastrella era tutta scheggiata e incrinata.

Dirksen sollevò lo sguardo. Hunt rideva. La macchina si era allontanata di un
paio di metri, si era fermata, e la teneva d’occhio. No, si disse Dirksen, non
stava tenendola d’occhio. Irritata con se stessa, impugnò l’arma e avanzò con
cautela. La macchina indietreggiò, mentre due luci rosse sul davanti aumentavano
e diminuivano d’intensità con approssimativamente la stessa frequenza delle onde
alfa del cervello umano. Dirksen si slanciò in avanti, vibrò il martello e mancò
il colpo…

Dieci minuti dopo, rossa e ansimante, tornò da Hunt. Il corpo le doleva in
parecchi punti, perché aveva sbattuto più volte contro gli spigoli dei
macchinari; la testa le faceva male dove aveva preso una zuccata contro il piano
del bancone. “È come cercar di acchiappare un grosso topo! Quand’è che si
scaricano quelle stupide batterie?”.

Hunt consultò l’orologio. ”Direi che ne ha ancora per mezz’ora, purché tu la
faccia correre”. Puntò il dito sotto il bancone, dove la bestia aveva trovato
un’altra presa di corrente. “Ma c’è una maniera più facile per catturarla”.

“Quale?”.

“Metti giù il martello e prendila”.

“Prenderla… così?”.

“Sì. Riconosce solo i pericoli che possono derivarle da cose simili a lei, in
questo caso la testa d’acciaio del martello. È programmata in modo da fidarsi
del protoplasma disarmato”.

Dirksen posò il martello su un bancone e si diresse lentamente verso la
macchina. Questa non si mosse. Le fusa erano cessate; pallide luci arancione
brillavano dolcemente. Dirksen si chinò e la toccò cautamente; sentì un lieve
fremito. La raccolse guardinga con entrambe le mani. Le luci diventarono di un
verde limpido e attraverso il gradevole calore dell’epidermide metallica essa
sentì la vibrazione tranquilla del meccanismo interno.

“E adesso che devo farne di questa roba?” chiese con un certo nervosismo.

“Mettilo a pancia in su sopra il bancone. In quella posizione non potrà far
nulla e tu potrai colpirlo come ti pare”.

“Non ho bisogno di antropomorfismi” borbottò Dirksen e seguì il consiglio di
Hunt: era decisa ad arrivare fino in fondo.
Come capovolse la macchina e la posò, le luci ridiventarono rosse. Le ruote
girarono per un momento, poi si arrestarono. Dirksen afferrò di nuovo il
martello, lo sollevò in fretta e lo calò con forza, ma la mira era sbagliata: il
martello colpì la macchina indifesa di lato danneggiandone una ruota e facendola
balzare di nuovo a pancia in giù. Dalla ruota colpita uscì un cigolio metallico
e la bestia si mise a girare descrivendo un cerchio irregolare. Da sotto il
ventre si udì uno schianto: con un mesto ammiccare di luci la macchina si
arrestò.

Dirksen strinse forte le labbra e alzò il martello per il colpo di grazia. Ma
mentre cominciava a calarlo, dall’interno della bestia uscì un suono, un debole
gemito lacrimoso che si alzava e si abbassava come il piagnucolio di un bambino.

Dirksen lasciò cadere il martello e arretrò, gli occhi fissi sulla pozza rosso
sangue di lubrificante che si allargava sul tavolo sotto la creatura. Guardò
Hunt con orrore. “È… è…”.

“È solo una macchina” disse Hunt, di nuovo serio. “Come queste, che l’hanno
preceduta nella sua storia evolutiva”. Indicò con un gesto la schiera di
macchine del laboratorio, osservatori muti e minacciosi. “Ma questa, a
differenza delle altre, è in grado di accorgersi del proprio fato e di gridare
per chiedere aiuto”,

“Spegnila” disse lei seccamente.

Hunt andò al tavolo e tentò di far scattare il minuscolo interruttore. “Temo che
tu l’abbia inceppato”. Raccolse il martello che era caduto sul pavimento. “Vuoi
darle il colpo di grazia?”.

Lei arretrò scuotendo la testa; Hunt sollevò il martello. “Non potresti
aggiustare…”. Ci fu un breve scricchiolio metallico. Lei sussultò e girò il
capo. Il lamento era cessato: risalirono le scale in silenzio.

Riflessioni
-----------

Jason Hunt osserva: “Ma non è sempre facile sapere chi o che cosa abbia delle
sensazioni o dei sentimenti”. Questo è il punto cruciale del brano. All’inizio
Lee Dirksen individua l’essenza del vivente nella sua capacità di
autoriprodursi. Hunt le fa subito osservare che certi congegni inanimati sono in
grado di autoassemblarsi. E che dire dei microbi, o addirittura dei virus, che
portano in sé le istruzioni per la propria duplicazione? Hanno un’anima? C’è da
dubitarne!

Poi Dirksen vede la chiave nell’idea di sentimento, e per illustrare questa tesi
l’autore sfrutta tutte le armi emotive senza esclusione di colpi per convincerci
che possono esistere sentimenti meccanici e metallici – cosa che sembrerebbe una
vera contraddizione in termini. L’autore ricorre soprattutto ad appelli
subliminali a livello viscerale: usa espressioni come “scarabeo di alluminio”,
“sonoro ronfare”, “stridio acuto, simile a un grido di paura”, “la teneva
d’occhio”, “lieve fremito”, “il gradevole calore dell’epidermide metallica”,
“mesto ammiccare di luci”. Tutto questo è già molto vistoso… ma come superare
l’immagine seguente, quella della “pozza rosso sangue di lubrificante che si
allargava sul tavolo sotto la creatura”, dalla quale proviene “un debole gemito
lacrimoso che si alzava ed abbassava come il piagnucolio di un bambino”?
Andiamo, via!

Le immagini sono tanto suggestive che si resta invischiati. Ci si può sentire
manovrati, ma il senso di fastidio che può venirne non riesce a soffocare il
nostro istintivo sentimento di compassione. Com’è difficile per certe persone
affogare una formica nel lavandino aprendo il rubinetto! E com’è facile per
altre dar da mangiare ogni giorno ai piranha del loro acquario pesciolini rossi
vivi! A che punto dobbiamo tracciare la linea di demarcazione? Che cosa è
sacrosanto e che cosa è superfluo?

Pochi di noi sono vegetariani o anche solo considerano seriamente la possibilità
di esserlo nel corso della propria vita. È perché l’idea di macellare vitelli,
maiali e così via non ci disturba affatto? Certo no. Pochi amano sentirsi
ricordare che quando mangiano una bistecca hanno nel piatto un pezzo di animale
morto. Per lo più ci proteggiamo ricorrendo a un linguaggio allusivo e a un
insieme di complicate convenzioni che ci permettono si salvare capra e cavoli.
Della vera natura del mangiar carne, come della vera natura del sesso e
dell’evacuazione, possiamo parlare solo in modo implicito, dietro lo schermo di
allusioni e sinonimi eufemistici: “fettine”, “fare all’amore”, “andare in
bagno”. Abbiamo una certa qual sensazione che nei macelli avvenga un’uccisione
di anime, ma i nostri palati non vogliono sentircelo ricordare.
Che cosa è più facile distruggere: un Chess Challenger VII che sa darci una
bella partita a scacchi facendo lampeggiare allegramente le sue luci rosse
mentre “riflette” sulla mossa successiva, oppure il tenero orsacchiotto che ci
piaceva tanto quando avevamo tre anni? Perché l’orsacchiotto ci prende al cuore?
In qualche modo denota piccolezza, innocenza, vulnerabilità.

Siamo fortemente soggetti ai richiami emotivi, ma siamo anche capaci di essere
molto selettivi nell’attribuzione dell’anima. Come poterono i nazisti
convincersi che era giusto uccidere gli ebrei? Come facevano gli americani ad
avere tanta voglia di “sterminare i gialli” durante la guerra del Vietnam?
Sembra che le emozioni di un certo tipo (il patriottismo) possano fungere da
valvola di controllo di quelle altre emozioni che ci permettono di identificarci
con gli altri, di proiettarci in loro: di vedere la nostra vittima come (un
riflesso di) noi stessi.

In qualche misura siamo tutti animisti. Alcuni di noi attribuiscono una
“personalità” alla loro automobile, altri considerano “vivi” i loro giocattoli o
la loro macchina da scrivere, come se possedessero un’“anima”. È difficile
bruciare certe cose nel caminetto, perché è una parte di noi che se ne va in
fumo. È chiaro che l’“anima” che proiettiamo in questi oggetti è soltanto
un’immagine della nostra mente. Eppure, se le cose stanno così, perché la stessa
cosa non vale per l’anima che proiettiamo nei nostri amici e nei nostri
familiari?

Tutti noi abbiamo un deposito di empatia al quale è più o meno facile o
difficile attingere, secondo il nostro umore o gli stimoli. Talvolta semplici
parole o espressioni fugaci toccano una corda sensibile e ci inteneriamo; altre
volte restiamo duri e gelidi, irremovibili.

In questo brano, la lotta della bestiola contro la morte tocca il cuore di Lee
Dirksen e il nostro. Vediamo il piccolo scarabeo combattere per la propria vita
o, per usare le parole di Dylan Thomas, infuriare “contro il morir della luce”,
e rifiutarsi di “entrare docilmente in quella buona notte”. Questo supposto
riconoscimento del proprio fato è forse il tocco più convincente di tutti. Ci
ricorda gli sventurati animali messi in circolo, scelti a caso e scannati, pieni
di terrore davanti all’approssimarsi di un destino inesorabile.

Quand’è che un corpo contiene un’anima? In questo brano così commovente abbiamo
visto emergere l’“anima” non in funzione di un qualche stato interno chiaramente
definito, bensì in funzione della nostra capacità di proiezione. Strano a dirsi,
questa è l’impostazione più comportamentistica che si possa immaginare! Non ci
preoccupiamo di sapere come funzionano i meccanismi interni: al contrario, li
diamo per scontati, una volta osservato il comportamento. È una strana convalida
dell’impostazione data da Turing col suo test al problema della “rivelazione
dell’anima”.

*Douglas R. Hofstadter*

*Nota:* Al contrario del resto dei contenuti di questo blog, questo racconto non
è in pubblico dominio, ma sotto copyright e pubblicato in Italia dalla
[Adelphi][1] sul libro [L'io della mente][2].

[1]: http://www.adelphi.it/ 
[2]: http://www.adelphi.it/libro/9788845907913
