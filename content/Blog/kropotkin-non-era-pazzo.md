title:  Kropotkin non era pazzo
date:   2015-07-28 19:39
tags:   scienza, filosofia, società, anarchia, violenza
summary: L'articolo originale si intitola *Kropotkin Was No Crackpot* e fu scritto da Stephen Jay Gould nel 1997.

L'[articolo originale][1] si intitola *Kropotkin Was No Crackpot* e fu scritto da Stephen Jay Gould nel 1997.

NEL TARDO 1909, due grandi uomini corrispondevano attraverso oceani, religioni,
generazioni e razze. Leo Tolstoy, saggio della nonviolenza Cristiana nei
suoi tardi anni, scrisse al giovane Mohandas Gandhi, che lottava per i
diritti degli immigrati indiani nel Sudafrica.

> Dio aiuti i nostri cari fratelli e collaboratori nel Transvaal. La stessa lotta
> del gentile contro il duro, della mansuetudine e dell'amore contro la superbia e
> la violenza, si sta facendo sentire sempre più anche qui in mezzo a noi.

Un anno più tardi, stanco dei litigi domestici, ed incapace di sopportare la
contraddizione di una vita nella povertà Cristiana dentro una prosperosa tenuta
gestita con i guadagni dei suoi grandi romanzi (scritti prima della sua
conversione religiosa e pubblicati da sua moglie), Tolstoy fuggì in treno verso
luoghi sconosciuti ed una più semplice fine per i suoi ultimi giorni. Scrisse a
sua moglie:

> La mia partenza ti addolorerà. Mi scuso per questo, ma capisco e credo che non
> potrei fare altrimenti. Il mio posto in casa sta, o è diventato
> insopportabile. A parte tutto, non posso vivere più nella condizione di
> lusso nella quale ero, quindi sto facendo ciò che i vecchi della mia età
> comunemente fanno: lasciare questa vita mondana per trascorrere gli utlimi
> giorni della mia vita in pace e solitudine.

Ma l'ultimio viaggio di Tolstoy fu sia breve che infelice. Meno di un mese più
tardi, al freddo e spossato dai numerosi lunghi viaggi sui treni Russi mentre si
avvicinava l'inverno, contrasse la polmonite e morì all'età di ottantadue anni
nella casa del capostazione della fermata di Astapovo. Troppo debole per
scrivere, dettò la sua ultima lettera il giorno 1 Novembre 1910. Indirizzata ad
un figlio ed una moglie che non condividevano il suo punti di vista sulla
nonviolenza Cristiana, Tolstoy offrì un ultimo consiglio:

> Il punto di vista che avete acquisito su Darwinismo, evoluzione, e la lotta
> per la sopravvivenza non può spiegarvi il significato della vostra vita e non
> vi sarà da guida per le vostre azioni, ed una vita senza una spiegazione del
> suo significato ed importanza, e senza la guida incrollabile che ne deriva, è
> un esistenza pietosa. Pensateci. Lo dico, probabilmente alla vigilia della mia
> morte, perché vi amo.

La critica di Tolstoy è stata la più comune fra tutte le accuse volte contro
Darwin, dalla pubblicazione de *L'origine delle specie* nel 1859 ad oggi. Il
Darwinismo, l'accusa sostiene, compromette la moralità affermando che in natura
il successo può venire misurato solamente come vittoria in una lotta all'ultimo
sangue - la "lotta per la sopravvivenza" o "sopravvivenza del più adatto" per
citare i motti scelti dallo stesso Darwin. Se volessimo il trionfo della
"mansuetudine e dell'amore" sulla "superbia e la violenza" (come Tolstoy scrisse
a Gandhi), allora dovremmo ripudiare il punto di vista di Darwin sulla natura -
come Tolstoy dichiarò nell'appello finale ai suoi vigli erranti.

Quest'accusa contro Darwin è scorretta per due ragioni. Primo, la natura (non
importa quanto crudele in termini umani) non fornisce basi per i nostri valori
morali. (L'evoluzione potrebbe, al massimo, aiutare a spiegare perché abbiamo
sentimenti morali, ma la natura non può mai decidere al posto nostro se una
particolare azione è giusta o sbagliata.) Secondo, la "lotta per la
sopravvivenza" di Darwin è una metafora astratta, non una dichiarazione
esplicita riguardo alla lotta sanguinosa. Il successo riproduttivo, il criterio
della selezione naturale, funziona in diversi modi: la vittoria in battaglia può
essere una strada, ma cooperazione, simbiosi, e mutuo aiuto possono anch'essi
assicurare il successo in diversi tempi e contesti. In un famoso passaggio,
Darwin spiegò il suo concetto di lotta per l'evoluzione (*Origin of Species*,
1859, pp. 62-62):

> Utilizzo questo termine in modo largo e metaforico includendo la dipendenza di
> un essere da un altro, ed includendo (cosa più importante) non solo la vita
> dell'individuo, ma il successo nel lasciare progenie. Due animali canini, in
> tempo di scarsità, si può dire che debbano combattere fra loro per ottenere
> cibo e vivere. Ma di una pianta sul limitare del deserto si può dire che
> combatta per la sua vita contro la scarità... Come l'esistenza del vischio che
> viene disseminato dagli uccelli, dipende da essi; e si può metaforicamente
> dire che lotti con le altre piante da frutto, nel tentare gli uccelli a
> mangiare e quindi spargere i suoi semi invece di quelli di altre piante. In
> questi diversi sensi, che transitano l'uno nell'altro, io uso per motivi di
> convenienza l'espressione generica di lotta per la sopravvivenza.

Eppure, in un altro senso, la critica di Tolstoy non è completamente priva di
fondamento. Darwin presentò una definizione metaforica onnicomprensiva di lotta,
ma i suoi esempi concreti favorivano certamente l'idea di battaglia sanguinosa -
"La Natura, dai rossi denti ed artigli", da una frase di Tennyson così tanto
citata che divenne presto un cliché della sua visione sulla vita. Darwin basò la
sua teoria della selezione naturale sulla triste visione di Malthus secondo cui
la crescita demografica supererà l'approvvigionamento di cibo e porterà ad una
battaglia per le scarseggianti risorse. Inoltre, Darwin mantenne una visione
limitata ma controllata dell'ecologia come di un mondo zeppo di specie in
competizione - così in equilibrio e così affollato che una nuova forma potrebbe
entrare solo spingendo fuori un precedente abitante. Darwin espresse questo
punto di vista con una metafora ancora più centrale del concetto di lotta per la
sua visione generale, la metafora del cuneo. La Natura, scrive Darwin, è come
una superficie con 10.000 cunei piantati saldamente ad occupare tutto lo spazio
disponibile. Una nuova specie (rappresentata come un cuneo) può guadagnare
spazio in una comunità spingendosi in un piccolo spazio e scalzando fuori un
altro cuneo. Il successo, da questo punto di vista, può essere ottenuto solo
subentrando direttamente in aperta competizione.

Inoltre, il principale discepolo di Darwin, Thomas Henry Huxley avanzò questa
visione "gladiatoriale" della selezione naturale (parole sue) in una serie di
famosi saggi sull'etica. Huxley sosteneva che la predominanza della lotta
sanguinosa definiva il senso della natura come non morale (non esplicitamente
immorale, ma certamente inadatto ad offrire una guida al comportamento morale).

> Dal punto di vista del moralista il mondo animale è circa al livello di uno
> spettacolo di gladiatori. Le creature sono abbastanza ben trattate e pronte
> alla battaglia - laddove il più forte, il più veloce, ed il più scaltro
> sopravvive per combattere un altro giorno. Lo spettatore non ha bisogno di
> girare il pollice in basso, dato che la lotta è senza quartiere.

Ma Huxeley va oltre. Ogni società umana che segua questa linea della natura si
devolverà nell'anarchia e nella miseria - il mondo brutale di Hobbes del *bellum
omnium contra omnes* (dove bellum significa "guerra", non bellezza): la guerra
di tutti contro tutti. Pertanto, lo scopo principale della società deve
risiedere nella mitigazione della lotta che definisce il percorso della natura.
Studia la selezione naturale e fai il contrario nella società umana:

> Ma nella società civile, l'inevitabile risultato dell'obbedienza [alla legge
> della lotta sanguinosa] è il ristabilirsi, in tutta la sua intensità, di
> quella lotta per la sopravvivenza - la guerra di chiunque contro tutti - la
> cui mitigazione o abolizione fu lo scopo finale dell'organizzazione sociale.

Questa apparente discordanza fra il modo naturale ed ogni speranza
per una decenza sociale umana ha definito l'argomento principale di discussione
sull'etica e l'evoluzione sin da Darwin. La soluzione di Huxley ha guadagnato
molti sostenitori - la natura è cattiva non una guida morale ad eccezione,
forse, di usarla come indicatore di ciò da evitare nella società umana. La mia
preferenza si poggia su una diversa soluzione basata sul prendere seriamente il
punto di vista metaforico di Darwin sulla lotta (certamente a fronte delle
preferenze di Darwin per gli esempi sui gladiatori) - la natura è talvolta
cattiva, talvolta buona (in realtà nessuno dei due, data l'inappropriatezza dei
termini umani). Presentando esempi di tutti i comportamenti (alla metaforica
voce di "lotta"), la natura non ne favorisce alcuno e non offre linee guida. I
fatti della natura non possono fornire in alcun caso alcuna guida morale.

Ma una terza soluzione è stata sostenuta da alcuni pensatori che non desiderano
trovare una base per la moralità nella natura e nell'evoluzione. Dato che pochi
possono rilevare un gran conforto morale nell'interpretazione gladiatoriale,
questa terza posizione deve riformulare il modo naturale. Le parole di Darwin
sul carattere metaforico della lotta offrono un promettente punto di partenza.
Si potrebbe sostenere che gli esempi sui gladiatori sono stati venduti troppo
bene e travisati come predominanti. Forse la cooperazione ed il mutuo aiuto sono
i risultati più comuni della lotta per la sopravvivenza. Forse è la comunione,
piuttosto che il combattimento a portare un maggior successo riproduttivo nella
maggioranza dei casi.

La più famosa espressione di questa terza soluzione può essere trovata in *Il
mutuo appoggio*, pubblicato nel 1902 dal rivoluzionario anarchico Pëtr
Kropotkin. (Dobbiano sfatare il vecchio stereotipo degli anarchici come barbuti
bombaroli che si aggirano furtivamente per le strade della città nella notte.
Kropotkin era un uomo geniale, quasi un santo, stando ad alcuni, che promosse
la visione di piccole comunità auto organizzate mediante consenso per il
beneficio di tutti, pertanto eliminando la necessità della maggior parte delle
funzioni di un governo centrale.) Kropotkin, un nobiluomo russo, visse in esilio
in Inghilterra per ragioni politiche. Scrisse *Mutual Aid* (in Inglese) come
risposta diretta al saggio di Huxley sopracitato, "The Struggle for Existence in
Human Society", pubblicato su *The Ninteenth Century*, nel febbraio 1888.
Kropotkin rispose ad Huxley con una serie di articoli, anch'essi stampati su
*The Ninteenth Century* e successivamente raccolti nel libro *Il mutuo
appoggio*.

Come suggerisce il titolo, Kropotkin argomenta, nella sua premessa cardinale,
che la lotta per l'esistenza solitamente tende verso il mutuo aiuto piuttosto
che che la battaglia come principale criterio di successo evolutivo. La società
umana deve quindi svilupparsi sulle nostre inclinazioni naturali (non
invertirle, come sosteneva Huxley) formando un ordine morale che porterà pace e
prosperità alla nostra specie. In una serie di capitoli, Kropotkin cerca
d'illustrare una continuità fra la selezione naturale mediante mutuo appoggio
fra gli animali e le basi per il successo di una sempre progressiva
organizzazione sociale umana. I suoi cinque successivi capitoli riguardano il
mutuo aiuto fra gli animali, fra i selvaggi, i barbari, nelle città medievali, e
noi stessi.

Confesso di aver sempre visto Kropotkin come un folle eccentrico, anche se
innegabilmente benintenzionato. Viene sempre presentato così nei corsi standard
di biologia evolutiva - come uno di quei pensatori teneri e confusi che lasciano
che la speranza ed il sentimentalismo intralcino la robustezza analitica e la
disponibilità ad accettare la natura com'essa sia, nel bene e nel male.
Dopotutto era un uomo dalle strane idee politiche e dagli ideali irrealizzabili,
strappato dal contesto della sua giovinezza, uno straniero in terra straniera.
Per di più il suo ritratto di Darwin combaciava così tanto con le sue idee
sociali (il mutuo appoggio come prodotto di un'evoluzione senza la necessità di
un'autorità centrale) che sul suo conto, uno potrebbe solamente vedere della
speranza piuttosto che dell'accuratezza scientifica. Kropotkin è stato a lungo
sulla mia lista dei potenziali argomenti per un saggio (se non altro perché
volevo leggere il suo libro, e non ripetere la sua mera interpretazione del
libro di testo), ma non l'ho mai fatto perché non riuscivo a trovare un contesto
più ampio dell'uomo stesso. I pensatori eccentrici sono interessanti per farci
delle chiacchiere, magari sulla psicologia, ma la vera eccentricità fornisce la
peggior base possibile per le generalizzazioni.

Ma questa situazione cambiò in un lampo quando lessi un veramente ottimo
articolo di Daniel P. Todes nell'ultimo numero di *Isis* (la nostra principale rivista
specializzata in storia della scienza) intitolato "La Metafora Malthusiana di
Darwin ed il Pensiero Evoluzionistico Russo, 1859-1917." Ho imparato che il
provincialismo era mio nella mia ignoranza sul pensiero evoluzionistico Russo,
non di Kropotkin nel suo isolamento in Inghilterra. (So leggere il russo, ma
solo faticosamente, e con un dizionario - il che significa, per motivi pratici,
che non so leggere la lingua.) Seppi che Darwin diventò un eroe per
l'intellighenzia Russa ed influenzò la vita accademica in Russia forse più che
in qualsiasi altro paese. Ma praticamente nessuna di queste opere russe è mai
stata tradotta o quantomeno discussa nella letteratura inglese. Le idee di
quella scuola ci sono sconosciute; non sappiamo nemmeno riconoscere i normi dei
principali protagonisti. Conoscevo Kropotkin perché pubblicò in inglese e visse
in Inghilterra, ma non avevo mai capito che lui rappresentava una ben sviluppata
e comune critica russa a Darwin, basata su ragioni interessanti e coerenti
tradizioni nazionali. L'articolo di Todes non rende Kropotkin più corretto, ma
dispone i suoi scritti in un contesto generale che richiede il nostro rispetto e
produce notevole ispirazione. Kropotkin faceva parte di un grande fiume di
pensiero che scorreva in una direzione poco famigliare, non era un piccolo
isolato torrentello.

La scuola russa di critica a Darwin, argomenta Todes, basa la sua premessa
principale sul netto rifiuto dell'affermazione di Malthus che la competizione,
in stile gladiatoriale, debba dominare in un mondo sempre più affollato, dove la
popolazione, in crescita geometrica, inevitabilmente eccede le riserve di cibo
che possono solamente crescere aritmeticamente. Tolstoy, parlando per il
consenso dei suoi compatrioti, bollò Malthus come una "malefica mediocrità."

Todes trova un eterogeneo insieme di ragioni dietro l'ostilità russa verso
Malthus. Obiezioni politiche alla caratteristica cane-mangia-cane della
competizione industriale occidentale crebbero da entrambi gli estremi dello
spettro Russo. Todes scrive:

> I radicali, che speravano di costruire una società socialista, videro Malthus
> come una corrente reazionaria nell'economia politica borghese. I conservatori,
> che speravano di preservare le virtù comuni della Russia zarista, lo videro
> come un'espressione del "modello nazionale Britannico".

Ma Todes identifica una ragione più interessante nell'esperienza immediata della
terra e della storia naturale Russa. Abbiamo tutti la tendenza naturale a
derivare teorie universali partendo da un limitato dominio di circostanze
attinenti. Molti genetisti leggono l'intero mondo dell'evoluzione fra i confini
di una boccetta da laboratorio piena di moscerini della frutta. La mia crescente
dubbiosità riguardo agli adattamenti universali deriva in gran parte,
indubbiamente, dal fatto che io studi una peculiare lumaca che varia così
ampiamente e capricciosamente all'interno di un apparentemente invariante
ambiente, piuttosto che da un uccello in volo o qualche altra meraviglia della
natura.

La Russia è un paese immenso, sotto-popolato dal punto di vista del
diciannovesimo secolo riguardo al suo potenziale agricolo. La Russia è anche,
in gran parte della sua area, una terra aspra, dove la competizione pone
un organismo più facilmente contro l'ambiente (come nella metafora di Darwin
della pianta sul limitare del deserto) piuttosto che contro un organismo in
diretta e sanguinosa battaglia. Come può qualsiasi russo, con una profonda
sensibilità per il proprio paesaggio, vedere il principio di Malthus della
sovra-popolazione come fondamento di una teoria sull'evoluzione? Todes scrive:

> Era estraneo alla loro esperienza perché, molto semplicemente, l'enorme massa
> di terra della Russia sminuiva la propria sparsa popolazione. Per un russo
> immaginare un inesorabile incremento di popolazione inevitabilmente proteso
> verso le riserve di cibo e lo spazio richiedeva un bel salto d'immaginazione.

Se questi critici russi potrebbero onestamente legare il proprio scetticismo
alla visione del proprio cortile di casa, essi potrebbero anche riconoscere che
l'entusiasmo opposto di Darwin potrebbe ricordare la provincialità dei suoi
diversi circondari, piuttosto che un insieme di verità necessariamente
universali. Malthus è un miglior profeta in un paese industriale affollato che
professa un ideale di competizione aperta in un libero mercato. Inoltre è stato
spesso appuntato che sia Darwin che Alfred Russel Wallace svilupparono
indipendentemente la teoria della selezione naturale in seguito alle prime
esperienze con la storia naturale nei tropici. Entrambi dichiararono di
ispirarsi a Malthus, ancora indipendentemente; ma se la fortuna favorisce la
mente preparata, allora la loro esperienza tropicale probabilmente dispose
entrambi gli uomini a leggere Malthus con risonanza ed approvazione.
Nessun'altra zona della Terra è così ricca di specie, e quindi così piena di
competizione fra corpi. Un inglese che ha imparato le vie della natura ai
tropici era quasi del tutto obbligato a vedere l'evoluzione in modo diverso da
un russo alimentato da storie sulla steppa siberiana.

Ad esempio, N. I. Danielevsky, un esperto di pesca e dinamiche della popolazione,
pubblicò nel 1885 una grossa critica in due volumi sul darwinismo. Identificò la
lotta per il personale vantaggio come il credo di un distintamente britannico
"modello nazionale" al contrario dei vecchio valore slavo del collettivismo. Un
ragazzino inglese, scrive, "fa a botte uno contro uno, non in gruppo come a noi
russi piace fare." Denielevsky vide la competizione Darwiniana come "una pura
dottrina inglese" fondata su una linea di pensiero britannica che si estende da
Hobbes attraverso Adam Smith fino a Malthus. La selezione naturale, scrisse, ha
le radici nella "guerra di tutti contro tutti, ora chiamata lotta per
l'esistenza - la teoria politica di Hobbes sulla concorrenza - la teoria
economica di Adam Smith. ...Malthus applicò lo stesso identico principio al
problema della popolazione. ... Darwin estese sia la teoria parziale di Malthus
che la teoria generale degli economisti politici al mondo organico."
(Virgolettato dall'articolo di Todes.)

Quando ci rivolgiamo a *Il mutuo appoggio* di Kropotkin alla luce delle scoperte
di Todes sul pensiero evolutivo russo, dobbiamo invertire il punto di vista
tradizionale ed interpretare la sua opera come corrente principale della critica
russa, non una nevrosi personale. La logica centrale dell'argomento di Kropotkin
è semplice, lineare, ed ampiamente convincente.

Kropotkin inizia riconoscendo che la lotta gioca un ruolo centrale nelle vite
degli organismi e fornisce anche l'impulso principale per la loro evoluzione. Ma
Kropotkin afferma che la lotta non dev'essere vista come un fenomeno unitario.
Dev'essere divisa in due fondamentalmente diverse forme con significati
evolutivi diversi. Dobbiamo riconoscere, prima di tutto, la lotta dell'organismo
contro l'organismo per le risorse limitate - il tema che Malthus impartì a
Darwin e che Huxley descrisse come gladiatoriale. Questa forma di lotta diretta
conduce alla competizione per il vantaggio personale.

Ma una seconda forma di lotta - lo stile che Darwin chiamò mateforico - pone
l'organismo contro la durezza dell'ambiente fisico circostante, non contro altri
membri delle stesse specie. Gli organismi devono lottare per mantenersi al
caldo, per sopravvivere agli improvvisi ed imprevedibili pericoli del fuoco e
della tempesta, per sopravvivere attraverso duri periodi di siccità, neve, o
pestilenza. Queste forme di lotta tra organismo ed ambiente vengono meglio
combattute mediante cooperazione fra membri della stessa specie tramite mutuo
appoggio. Se la lotta per l'esistenza pone due leoni contro una zebra, allora
saremmo testimoni di una battaglia felina e di una carneficina equina. Ma se il
leoni lottano assieme contro la durezza di un ambiente inanimato, allora il
combattimento non rimuoverebbe il nemico comune - mentre la cooperazione
potrebbe superare il pericolo più di quanto potrebbe ogni singolo individuo.

Kropotkin quindi creò una dicotomia nella nozione generale di lotta - due forme
opposte: (1) organismo contro organismo della stessa specie per le risorse
limitate, che conduce alla competizione; e (2) organismo contro ambiente, che
conduce alla cooperazione.

> Nessun naturalista dubiterà che l'idea di lotta per la vita portata avanti
> attraverso la natura organica sia la più grande generalizzazione di questo
> secolo. La vita è lotta; ed in questa lotta il più adatto sopravvive. Ma le
> risposte alle domande "con che armi la lotta vien principalmente portata
> avanti?" e "chi sono i più adatti nella lotta?" varieranno ampiamente a
> seconda dell'importanza data ai due diversi aspetti della lotta: quello
> diretto, per il cibo ed il riparo tra individui separati, e la lotta che
> Darwin descrisse come "metaforica" - la lotta, molto spesso collettiva, contro
> le circostanze avverse.

Darwin riconobbe l'esistenza di entrambe le forme, ma la sua lealtà a Malthus e
la sua visione di una natura affollata di specie lo condusse ad enfatizzare
l'aspetto competitivo. I devoti di Darwin meno sofisticati quindi esaltarono la
visione competitiva ad una quasi esclusività, e vi poggiarono pure sopra un
significato sociale e morale.

> Arrivarono a concepire il mondo animale come un mondo di perenne lotta fra
> individui affamati che si abbeverano dell'altrui sangue. Fecero risuonare la
> letteratura moderna al grido di guerra di "guai ai vinti", come se fossero le
> ultime parole della moderna biologia. Per vantaggio personale innalzarono la
> lotta "spietata" alle altezze di un principio biologico che l'umano stesso
> dovesse sottoscrivere, sotto la minaccia di soccombere altrimenti in un mondo
> basato sullo sterminio recuproco.

Kropotkin non negò la forma competitiva di lotta, ma sostenne che lo stile
cooperativo era stato sottovalutato e dovesse invece bilanciare od anche
predominare sulla competizione se si considera la natura nel suo complesso.

> C'è un'immensa quantità di guerra e sterminio in corso fra diverse specie;
> c'è, al tempo stesso, altrettanto, o forse ancora più, mutuo supporto, mutuo
> aiuto, e mutua difesa... La socialità è una regola della natura altrettanto
> quanto la reciproca lotta.

Mentre Kropotkin trafficava con i suoi selezionati esempi e costruì il motore
per le proprie preferenze, divenne sempre più convinto che lo stile cooperativo,
che conduce al mutuo appoggio, non solo predominava in generale ma anche
caratterizzava le più avanzate creature di qualsiasi gruppo: formiche tra gli
insetti, mammiferi tra i vertebrati. Il mutuo appoggio quindi divenne un
principio più importante rispetto alla competizione ed alla carneficina:

> Se noi ... chiediamo alla Natura: "chi sono i più adatti: coloro che sono in
> continua guerra fra di loro, o coloro che si sostengono a vicenda?" noi ad un
> tratto vediamo che quegli animali che acquisiscono abitudini di mutuo aiuto
> sono senza dubbio di più adatti. Essi hanno più possibilità di sopravvivere, e
> raggiungono, nelle loro rispettive classi, il più elevato sviluppo
> intellettivo e di organizzazione corporea.

Se chiedessimo perché Kropotkin favorì la cooperazione mentre la maggior parte
dei sostenitori di Darwin del diciannovesimo secolo sostenevano la competizione
come risultato predominante della lotta in natura, emergerebbero due ragioni
principali. La prima appare meno interessante, com'è ovvio sotto il vagamente
cinico ma assolutamente realistico che i veri credenti tendono a leggere le loro
preferenze sociali nella natura. Kropotkin, l'anarchico che desiderava
sostituire le leggi del governo centrale con il consenso di comunità locali,
certamente sperava di localizzare una profonda preferenza per il mutuo appoggio
nel più profondo midollo evolutivo del nostro essere. Lasciate che il mutuo
appoggio pervada la natura e la cooperazione umana diventa una semplice istanza
della legge della vita.

Né lo schiacciante potere dello Stato centralizzato né gli insegnamenti di odio
reciproco e lotta spietata che arrivarono, abbelliti con gli attributi della
scienza, da obbligare filosofi e sociologi, potrebbero estirpare il sentimento
di umana solidarietà, profondamente alloggiato nell'umana ragione e nel cuore,
perché esso è stato alimentato da tutta la nostra precedente evoluzione.

Ma la seconda ragione è più illuminante, come un benvenuto input dall'esperienza
personale di Kropotkin come naturalista e conferma dell'intrigante tesi di Todes
secondo cui l'usuale fluire dall'ideologia all'interpretazione della natura
dovrebbe talvolta venire invertito, e far sì che il paesaggio colori la
preferenza sociale. Quando era ragazzo, molto prima della sua conversione al
radicalismo politico, Kropotkin trascorse cinque anni in Siberia (1862-1866)
poco dopo che Darwin pubblicò *L'origine delle specie*. Ci andò come ufficiale
militare, ma il suo mandato servì come comoda copertura al suo desiderio di
studiare la geologia, geografia e zoologia del vasto entroterra russo. Là, al
polo opposto rispetto alle esperienze tropicali di Darwin, dimorò nell'ambiente
meno favorevole al punto vista di Malthus. Osservò un mondo sparsamente
popolato, spazzato da frequenti catastrofi che minacciavano le poche specie in
grado di trovar posto in tale desolazione. In quando potenziale discepolo di
Darwin, cerco la competizione, ma raramente ne trovo alcuna. Al contrario,
continuò ad osservare i benefici del mutuo aiuto nel far fronte alla durezza
esteriore che minacciava tutti allo stesso modo e che non poteva esser
affrontata con degli analoghi della guerra e del combattimento.

Kropotkin, in breve, ebbe una personale ed empirica ragione per vedere
con favore la cooperazione quanto forza naturale. Egli scelse questo argomento
come paragrafo d'apertura per *Il mutuo appoggio*:

> Due aspetti della vita animale mi impressionarono maggiormente durante i
> viaggi che feci in gioventù nella Siberia orientale e nella Manciuria
> settentrionale. Uno di essi era l'estrema severità della lotta per l'esistenza
> che la maggior parte delle specie di animali dovevano portare avanti contro
> una Natura inclemente; l'enorme distruzione di vite che periodicamente
> risultava dagli agenti naturali; e la conseguente scarsità di vita sul
> territorio che cadde sotto la mia osservazione. E l'altra era, che anche in
> quei pochi punti dove la vita animale brulicava in abbondanza, non riuscii a
> trovare - nonostante la stessi avidamente cercando - quell'amara lotta per la
> sopravvivenza tra animali appartenenti alle stesse specie, che era considerata
> dalla maggior parte dei darwinisti (anche se non sempre da Darwin stesso) come
> la caratteristica dominante della lotta per la vita, ed il fattore principale
> dell'evoluzione.

Cosa possiamo farci oggi dell'argomento di Kropotkin, e di tutta la scuola russa
rappresentata da lui? Erano solo vittime di una speranza culturale e
conservatorismo intellettuale? Non penso. In realtà, vorrei sostenere che
l'argomento base di Kropotkin è corretto. La lotta occorre in diverse modalità,
ed alcune conducono alla cooperazione tra i membri di una specie come percorso
migliore per avvantaggiare gli individui. Se Kropotkin sopravvalutò il reciproco
aiuto, la maggior parte dei darwinisti dell'Europa occidentale esagerarono la
competizione altrettanto fortemente. Se Kropotkin trasse inappropriata speranza
per una riforma sociale dal suo concetto di natura, altri darwinisti errarono
altrettanto fermamente (e per motivi che la maggior parte di noi vorrebbe ora
biasimare) nel giustificare conquiste imperiali, razzismo, ed oppressione degli
operai come dura conseguenza nella selezione naturale nella modalità
competitiva.

Vorrei criticare Kropotkin in solo due modi - uno tecnico, e l'altro generale.
Fece un errore concettuale comune a non riconoscere che la selezione naturale è
un argomento sui vantaggi dei singoli organismi, in qualsiasi modo lottino. Il
risultato della lotta per l'esistenza può essere la cooperazione piuttosto che
la competizione, ma il mutuo appoggio deve beneficiare i singoli organismi nel
mondo della spiegazione di Darwin. Kropotkin talvolta parla di mutuo aiuto come
preferenza per il benessere di intere popolazioni o specie - un concetto
estraneo alla logica darwiniana classica (dove gli organismi lavorano, anche se
inconsciamente, per il proprio stesso benessere in termini di geni passati alle
future generazioni). Ma Kropotkin anche (e spesso) riconobbe che la preferenza
per il mutuo appoggio beneficia direttamente ogni individuo nella sua lotta per
il personale successo. Quindi, se Kropotkin con comprese tutte le implicazioni
dell'argomento base di Darwin, incluse la soluzione ortodossa come principale
giustificazione per il mutuo appoggio.

Più generalmente, mi piace applicare una qualsivoglia cinica regola empirica nel
giudicare gli argomenti sulla natura che hanno anche implicazioni sociali:
quando certe dichiarazioni permeano la natura con solo quelle proprietà che ci
fanno sentire bene o che alimentano i nostri pregiudizi, bisogne essere
doppiamente sospettosi. Sono particolarmente prudente con argomenti che trovano
gentilezza, reciprocità, sinergia, armonia - gli stessi elementi che cerchiamo
con forza, e così spesso infruttuosamente, di inserire nelle nostre vite -
intrinsecamente nella natura. Non vedo prove per la noosfera di Teilhard, per lo
stile olistico californiano di Capra, per la risonanza morfica di Sheldrake.
Gaia mi colpisce come una metafora, non come un meccanismo. (Le metafore possono
essere liberatorie ed illuminanti, ma nuove teorie scientifiche devono fornire
dichiarazioni riguardo la causalità. Gaia, a me, sembra solo riformulare, con
diversi temini, le conclusioni fondamentali da lungo conseguite dai classici
argomenti riduzionisti della teoria dei cicli biogeochimici.)

Non ci sono scorciatoie per l'illuminazione morale. La natura non è
intrinsecamente qualcosa che possa offrire conforto o ristoro in termini umani -
se non altro perché le nostre specie sono delle ritardatarie insignificanti in
un mondo non costruito per noi. Tanto meglio. Le risposte ai dilemmi morali non
giacciono là fuori, aspettando di essere scoperte. Esse risiedono, come il regno
di Dio, dentro noi - il punto più difficile ed inaccessibile per qualsiasi
scoperta o consenso.

[1]:https://www.marxists.org/subject/science/essays/kropotkin.htm
