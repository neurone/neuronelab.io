title: La peste
date: 2016-03-29 20:53
tags: libri, società, racconti, filosofia
summary: Riporto un brano da _La peste_ di **Albert Camus**. [...] La brezza recava odori di spezie e di pietra. Il silenzio era assoluto. "Si sta bene", disse Rieux sedendo, "è come se la peste non fosse mai salita qui". Tarrou, voltandogli le spalle, guardava il mare. "Sì", disse dopo un momento, "si sta bene".

Riporto un brano da [La peste][1] di [Albert Camus][2].

[...] La brezza recava odori di spezie e di pietra.  
Il silenzio era assoluto. "Si sta bene", disse Rieux sedendo, "è come se la
peste non fosse mai salita qui".  
Tarrou, voltandogli le spalle, guardava il mare.  
"Sì", disse dopo un momento, "si sta bene".  
Andò a sedere vicino al dottore e lo guardò attentamente. Tre volte il bagliore
ricomparve nel cielo. Uno strepito di stoviglie urtate salì sino a loro dalla
profondità della strada. Una porta sbatté nella casa.  
"Rieux", disse Tarrou con un tono molto naturale, "lei non ha mai cercato di
sapere chi sono io? Lei ha dell'amicizia per me?"  
"Sì", rispose il dottore, "ho dell'amicizia per lei; ma sinora ci è mancato il
tempo".  
"Bene, ciò mi tranquillizza. Vuole che sia questo il momento dell'amicizia?"  
Per tutta risposta, Rieux gli sorrise.  
"Ebbene, ecco..."  
Alcune strade più lontano, un'automobile sembrò scivolare lungamente sul
selciato umido; si allontanò, e poi confuse esclamazioni venute da lontano,
ruppero ancora il silenzio. E il silenzio ricadde sui due uomini con tutto il
suo peso di cielo e di stelle. Tarrou si era alzato per issarsi sul parapetto
della terrazza, di fronte a Rieux sempre affondato nella sua sedia. Parlò a
lungo, ed ecco, press'a poco, ricostruito, il suo discorso.

"Diciamo per semplificare, Rieux, che io soffrivo della peste molto prima di
conoscere questa città e questa malattia. Basti dire che io sono come tutti
quanti; ma ci sono persone che non lo sanno, o che si trovano bene in tale
stato, e persone che lo sanno e vorrebbero uscirne. Io, ho sempre voluto
uscirne.  
"Quand'ero giovane, vivevo con l'idea della mia innocenza, ossia con nessuna
idea, proprio. Non sono il tipo del tormentato, ho cominciato come si conveniva.
Tutto mi riusciva, ero a mio agio nell'intelligenza, mi andava per il meglio con
le donne, e se avevo qualche inquietudine, passava com'era venuta. Un giorno, ho
cominciato a riflettere. Intanto...  
"Bisogna dire che non ero proprio povero come lei; mio padre era accusatore
pubblico, ed è una posizione. Tuttavia non ne aveva l'aria, essendo d'indole
bonaria. Mia madre era semplice e discreta, e non ho mai cessato di amarla; ma
preferisco non parlarne. Lui si occupava di me con affetto, e credo persino che
cercasse di capirmi. Aveva delle avventure fuori casa, adesso ne sono sicuro, e
sono anche ben lontano dall'indignarmene. In tali cose si comportava come si
deve, senza urtare nessuno. Per dirla in breve, non era originalissimo, e oggi
che è morto mi rendo conto che se non è vissuto come un santo nemmeno è stato un
cattivo uomo. Si teneva nel mezzo, ecco tutto; ed è il tipo di uomo per cui si
sente un ragionevole affetto, quello che dura.  
"Aveva tuttavia una particolarità: l'Orario generale Chaix era il suo libro
prediletto. Non che viaggiasse, se non durante le vacanze, per andare in
Bretagna dove aveva una piccola proprietà; ma era in grado di dire esattamente
le ore di partenza e d'arrivo del Parigi-Berlino, le combinazioni d'orario
usufruibili per andare da Lione a Varsavia, la distanza chilometrica precisa da
una qualsiasi capitale all'altra. Lei mi sa dire come si va da Briançon a
Chamonix? Anche un capostazione si perderebbe; mio padre non si perdeva. Si
esercitava quasi tutte le sere ad arricchire le sue conoscenze in argomento, e
n'era piuttosto fiero. Questo mi divertiva molto, e io lo interrogavo sovente,
estasiato di controllare le sue risposte nello Chaix e di riconoscere che non si
era sbagliato. Questi esercizietti ci hanno molto legati l'uno all'altro: io gli
offrivo un uditorio di cui egli apprezzava la buona volontà. Quanto a me,
trovavo che questa superiorità relativa alle ferrovie ne valeva un'altra.  
"Ma io mi lascio andare, e corro il rischio di dare troppa importanza a questo
brav'uomo; infatti, per finirla, egli non ha avuto che un influsso indiretto
sulle mie decisioni. Al massimo, egli mi ha offerto un'occasione. Quando ebbi
diciassett'anni, mio padre m'invitò ad andarlo a sentire. Si trattava di una
causa importante, in Corte d'assise, e lui aveva pensato di figurarvi sotto la
luce migliore. Credo, inoltre, che contasse su tale cerimonia, atta a colpire le
fantasie giovanili, per spingermi ad entrare nella carriera che lui stesso si
era scelta. Avevo accettato, perché facesse piacere a mio padre, e perché,
anche, ero curioso di vederlo e di ascoltarlo in una parte diversa da quella che
recitava tra noi. Non pensavo a nient'altro. Quanto accadeva in tribunale mi era
sempre sembrato naturale e inevitabile come una rivista del 14 luglio o una
premiazione. Ne avevo un'idea molto astratta, che non mi disturbava.  
"Di quel giorno, tuttavia, non ho serbato che una sola immagine, quella del
colpevole. Credo che fosse colpevole davvero, importa poco di che; ma
quell'ometto di pel rosso e povero, d'una trentina d'anni, pareva sì deciso a
tutto ammettere, sì spaventato di quello che aveva fatto e che stavano per
fargli, che dopo alcuni minuti, io non ebbi occhi se non per lui. Aveva l'aria
d'un gufo intontito da una luce troppo viva; il nodo della cravatta non gli si
adattava con precisione al giro del collo; si rosicchiava le unghie di una sola
mano, la destra... In breve (non voglio insistere), lei ha capito ch'era un uomo
vivo.  
"Ma io me n'accorgevo improvvisamente, mentre, siano ad allora, non avevo
pensato a lui che traverso la comoda categoria dell'"imputato". Non posso dire
che dimenticassi allora mio padre, ma qualcosa, prendendomi allo stomaco, mi
toglieva ogni altra attenzione che non fosse rivolta al prevenuto. Non ascoltavo
quasi niente, e sentivo che si voleva uccidere quell'uomo vivo, e un istinto
formidabile come un'onda mi portava al suo fianco con una sorta di ostinato
accecamento. Non mi risvegliai veramente con la requisitoria di mio padre.  
"Trasformato dalla toga rossa, né bonario né affettuoso, la sua bocca
gorgogliava di frasi immense, che senza tregua ne uscivano come serpenti. E
capii che chiedeva la morte di quell'uomo in nome della società e che inoltre
chiedeva il taglio del suo collo. Diceva soltanto, è vero: 'Quella testa deve
cadere'; ma, insomma, la differenza non era grande. E venne a esser lo stesso,
infatti, in quanto egli ottenne quella testa. Semplicemente, non è lui che fece
allora il lavoro. E io che seguii poi la cosa sino alla conclusione, io ebbi con
quello sciagurato una intimità ben più vertiginosa di quella che ebbi con mio
padre. Questi doveva intanto, secondo il costume, assistere a quelli che si
chiamavano elegantemente gli ultimi momenti, ma che bisogna definire il più
abbietto degli assassinii.  
"Da quel momento in poi, non potei più guardare l'Orario Chaix che con un
abominevole disgusto. Da quel momento in poi, m'interessai con orrore alla
giustizia, alle condanne a morte, alle esecuzioni, e constatai, con una
impressione di vertigine, che mio padre aveva dovuto assistere parecchie volte
all'assassinio, e ch'era proprio nei giorni in cui si alzava prestissimo. Sì,
egli caricava la sveglia in quelle occasioni. Non osai parlare a mia madre, ma
la osservai meglio, allora, e capii che non c'era più nulla tra di loro e che
lei menava una vita di rinuncia. Questo mi aiutò a perdonarle, come dicevo
allora; in seguito seppi che non c'era nulla da perdonarle, in quanto essa era
stata povera tutta la vita, sino al matrimonio, e la povertà le aveva insegnato
la rassegnazione.  
"Di certo lei si aspetta che io le dica di essermene andato subito. No, sono
rimasto parecchi mesi, quasi un anno. Ma avevo la nausea. Una sera mio padre mi
domandò la sveglia: doveva alzarsi presto. La notte, non dormii; quando lui
tornò, il giorno dopo, ero partito. Diciamo subito che mio padre mi fece
cercare, che andai a trovarlo, che senza spiegar nulla gli dissi con calma che
mi sarei ucciso se mi avesse costretto a tornare. Egli finì con l'accettare, era
d'indole piuttosto mite; mi fece un discorso sulla stupidità che c'era nel voler
vivere la propria vita (in tal modo lui si spiegava il mio gesto, né io lo
dissuasi), mille raccomandazioni, e trattenne la sincere lacrime che gli
venivano. In seguito, ma molto tempo dopo, tornai regolarmente a trovare mio
padre, e allora lo incontrai. Tali rapporti gli bastarono, io credo. Per me, io
non avevo animosità contro di lui, soltanto un po' di tristezza al cuore. Quando
morì, presi mia madre con me, e ancora ci sarebbe, se non fosse morta anche lei.  
"Ho insistito molto su questo inizio perché fu, effettivamente, l'inizio di
tutto. Ho conosciuto la povertà a diciott'anni, uscendo dall'agiatezza; ho fatto
mille mestieri per guadagnarmi la vita, e non mi è riuscito troppo male. Ma
quello che m'interessava, era la condanna a morte; volevo regolare un conto col
gufo rosso. Di conseguenza, ho fatto della politica, come si dice. Non volevo
essere un appestato, insomma. Ho creduto che la società in cui vivevo fosse
fondata sulla condanna a morte e che, combattendola, avrei combattuto
l'assassinio. Io l'ho creduto, altri me l'hanno detto e, infine, è vero in gran
parte. Mi sono quindi messo con gli altri che amavo, e che non ho cessato di
amare. Ci sono rimasto a lungo e non vi è paese in Europa alle cui lotte non
abbia partecipato. Andiamo avanti.  
"Beninteso, sapevo che anche noi, all'occasione, pronunciamo condanne; ma mi
dicevano che quei pochi morti erano necessari per portare a un mondo in cui non
si sarebbe più ucciso nessuno. Era vero, in un certo modo, e, dopo tutto, forse
io non sono capace di fermarmi su questo genere di verità. Di ben sicuro, so che
esitavo. Ma pensavo al gufo, e potevo continuare. Sino al giorno in cui ho
veduto un'esecuzione (era in Ungheria) e la stessa vertigine che aveva colto il
ragazzo che io ero oscurò i miei occhi d'uomo.  
"Lei non ha mai veduto fucilare un uomo? No certo; sono cose che si fanno
generalmente su invito, e il pubblico è scelto prima. Il risultato è che lei è
rimasto alle stampe e ai libri: una benda, un palo e a distanza alcuni soldati.
Ebbene no! Lei sa che il plotone d'esecuzione si mette invece a un metro e mezzo
dal condannato? Sa che se il condannato facesse due passi avanti urterebbe col
petto nei fucili? Sa che a questa distanza gli sparatori concentrano il tiro
sulla regione del cuore e che tutti, coi grossi proiettili, fanno un buco dove
si potrebbe mettere un pugno? No, lei non lo sa: sono particolari di cui non si
parla. Il sonno degli uomini è più sacro che la vita per gli appestati; non si
deve impedire alla brava gente di dormire. Ci vorrebbe del cattivo gusto, e il
buon gusto consiste nel non insistere, è cosa che tutti sanno. Il cattivo gusto
mi è rimasto in bocca e io non ho cessato d'insistere, ossia di pensarvi.  
"Ho capito allora che io, almeno, non avevo finito di essere un appestato
durante i miei lunghi anni in cui, tuttavia, con tutta la mia anima, credevo
appunto di lottare contro la peste. Ho saputo di aver indirettamente firmato la
morte di migliaia di uomini, che avevo persino provocato tale morte, trovando
buoni i principi e le azioni che l'avevano fatalmente determinata. Gli altri non
ne sembravano urtati, o almeno non ne parlavano mai spontaneamente. Io avevo un
nodo alla gola. Quando mi capitava di esprimere i miei scrupoli, mi dicevano che
bisogna riflettere a quanto era in gioco e mi davano ragioni sovente
impressionanti, per farmi mandar giù quello che non riuscivo a inghiottire. Ma
io rispondevo che i grandi appestati, coloro che mettono le toghe rosse, anche
essi hanno eccellenti ragioni in quei casi, e che se ammettevo le cause di forza
maggiore e le necessità invocate dai piccoli appestati, non avrei potuto
respingere quelle dei grandi. Mi facevano notare che la maniera buona di dar
ragione alle toghe rosse era di lasciargli l'esclusività della condanna. Ma io
dicevo allora che se si cedeva una volta, non c'era ragione per fermarsi. Mi
sembra che la storia mi abbia dato ragione, oggi si fa a chi uccide di più. Sono
tutti nel furore del delitto, e non possono fare altrimenti.  
"La faccenda mia, in ogni caso, non era il ragionamento; era il gufo rosso,
quella sudicia avventura in cui sudice bocche appestate annunciavano a un uomo
in catene che doveva morire e regolavano tutte le cose per farlo morire,
infatti, dopo notti e notti d'agonia durante le quali egli si aspettava di
essere assassinato a occhi aperti. La faccenda mia era il buco nel petto. E mi
dicevo che aspettando, e almeno da parte mia, mi sarei rifiutato di dar mai una
sola ragione, una sola, lei capisce, a quella disgustosa macelleria. Sì, ho
scelto quest'accanimento ostinato in attesa di vederci più chiaro.  
"Poi, non ho cambiato. Da tanto tempo ho vergogna, vergogna da morirne, di esser
stato, sebbene da lontano, sebbene in buona fede, anch'io un assassino. Col
tempo, mi sono semplicemente accorto che anche i migliori d'altri non potevano,
oggi, fare a meno di uccidere o di lasciar uccidere: era nella logica in cui
vivevano, e noi non possiamo fare un gesto in questo mondo senza correre il
rischio di far morire. Sì, ho continuato ad aver vergogna, e ho capito questo,
che tutti eravamo nella peste; e ho perduto la pace. Ancor oggi la cerco,
tentando di capirli tutti e di non essere il nemico mortale di nessuno. So
soltanto che bisogna fare quello che occorre per non esser più un appestato, e
che questo soltanto ci può far sperare nella pace o, al suo posto, in una buona
morte. Questo può dar sollievo agli uomini e, se non salvarli, almeno fargli il
minor male possibile e persino, talvolta, un po' di bene. E per questo ho deciso
di rifiutare tutto quello che, da vicino o da lontano, per buone o per cattive
ragioni, faccia morire o giustifichi che si faccia morire.  
"Per questo, inolte, l'epidemia non m'insegna nulla, se non che bisogna
combatterla al suo fianco, Rieux. Io so di scienza certa (tutto so della vita,
lei lo vede bene) che ciascuno porta con sé, la peste, e che nessuno, no,
nessuno al mondo ne è immune. E che bisogna sorvegliarsi senza tregua per non
esser spinti, in un minuto di distrazione, a respirare sulla faccia di un altro
e a trasmettergli il contagio. Il microbo, è cosa naturale. Il resto, la salute,
l'integrità, la purezza, se lei vuole, sono un effetto della volontà e d'una
volontà che non si deve mai fermare. L'uomo onesto, colui che non infetta quasi
nessuno, è colui che ha distrazioni il meno possibile. E ce ne vuole di volontà
e di tensione per non essere mai distratti; sì, Rieux, essere appestati è molto
faticoso; ma è ancora più faticoso non volerlo essere. Per questo tutti appaiono
stanchi: tutti, oggi, si trovano un po' appestati. Ma per questo alcuni che
vogliono finire di esserlo, conoscono un culmine di stanchezza, di cui niente li
libererà se non la morte.  
"Di qui, so che io non valgo più nulla per questo mondo in se stesso, e che dal
momento in cui ho rinunciato a uccidere mi sono condannato a un definitivo
esilio. Saranno gli altri a fare la storia. So, inoltre, che non posso
apparentemente giudicare questi altri; mi manca una qualità per essere un
assassino ragionevole; non è quindi una superiorità. Ma ora, acconsento a essere
quello che sono, ho imparato la modestia. Dico soltanto che ci sono sulla terra
flagelli e vittime, e che bisogna, per quanto è possibile, rifiutarsi di essere
col flagello. Questo le sembrerà forse un po' semplice, e io non so se è
semplice, ma so che è vero. Ho sentito tanti tagionamenti da farmi girare la
testa, e che hanno fatto girare abbastanza altre teste da farle consentire
all'assassinio, che ho capito come tutte le disgrazie degli uomini derivino dal
non tenere un linguaggio chiaro, Allora ho presto il partito di agire
chiaramente, per mettermi sulla buona strada. Di conseguenza, ho detto che ci
sono flagelli e vittime, e nient'altro. Se, dicendo questo, divento flagello io
stesso, almeno non lo è col mio consenso. Cerco di essere un assassino
innocente; lei vede che non è una grande ambizione.  
"Bisognerebbe di certo che ci fosse una terza categoria, quella dei veri medici,
ma è un fatto che non si trova sovente, dev'essere difficile. Per questo ho
deciso di mettermi dalla parte delle vittime, in ogni occasione, per limitare il
male. In mezzo a loro, posso almeno cercare come si giunga alla terza categoria,
ossia alla pace".  
Terminando, Tarrou faceva oscillare una gamba, sì che il piede batteva piano
contro la terrazza. Dopo un silenzio, il dottore, sollevandosi un poco, domandò
se Tarrou avesse un idea della strada da prendere per arrivare alla pace.  
"Sì, la simpatia".  
Due campane d'ambulanza risuonarono lontano. Le esclamazioni, confuse poco
prima, si unirono ai confini della città, presso la collina rocciosa. Nello
stesso tempo si udì qualcosa che somigliava a una detonazione; poi tornò il
silenzio, Rieux contò due ammicchi del faro. La brezza sembrò rinvigorirsi, e
insieme un soffio venuto dal mare portò un odor di salso, ora si sentiva
distintamente la sorda respirazione edlle onde contro la scogliera.  
"Insomma", disse Tarrou con semplicità, "quello che m'interessa è sapere come si
diventa un santo".  
"Ma lei non crede in Dio".  
"Appunto: se si può essere un santo senza Dio, è il solo problema concreto che
io oggi conosca".  
Improvvisamente un grande bagliore nacque nella parte in cui erano venuti i
gridi, e risalendo il fuime del vento un oscuro clamore giunse sino ai due
uomini. Il bagliore subito si oscurò, e lontano, all'orlo delle terrazze, non
restò che una macchia rosseggiante. In una pausa del vento si sentirono
distintamente gridi d'uomini, poi lo strepito d'una scarica e il rumore di una
folla. Tarrou si era alzato e ascoltava; non si sentiva più nulla.  
"Si sono ancora scontrati alle porte".  
"Adesso è finito", disse Rieux.  
Tarrou mormorò che non era mai finito e che ci sarebbero state altre vittime:
era la regola.  
"Forse", rispose il dottore. "Ma lei sa, io mi sento più solidale coi vinti che
coi santi. Non ho inclinazione, credo, per l'eroismo e per la santità. Essere un
uomo, questo m'interessa".  
"Si, noi cerchiamo la stessa cosa, ma io sono meno ambizioso".  
Rieux pensò che Tarrou scherzasse, e lo guardò. Ma nel vago bagliore che veniva
dal cielo egli vide un volto triste e serio. Il vento si levava di nuovo, e
Rieux lo sentì tiepido sulla pelle. Tarrou si scosse:  
"Sa cosa dovremmo fare per l'amicizia?" disse.  
"Quello che lei vuole", disse Rieux.  
"Un bagno in mare; anche per un futuro santo, è un degno piacere".  
Rieux sorrideva.[...]

[1]:https://it.wikipedia.org/wiki/La_peste_(romanzo)
[2]:https://it.wikipedia.org/wiki/Albert_Camus
