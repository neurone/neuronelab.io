title: La religione come effetto collaterale del pensiero
date: 2015-04-01 19:03:16 +0200
tags: psicologia, religione, antropologia
summary: In un interessantissimo articolo, pubblicato su New Scientist il 4 febbraio 2009, ed intitolato *"Born believers: How your brain creates God"*, alcuni psicologi ed antropologi, grazie ai risultati di recenti esperimenti cognitivi, cercano di arrivare a delle teorie plausibili.

In un interessantissimo articolo, pubblicato su New Scientist il 4 febbraio 2009, ed intitolato *"Born believers: How your brain creates God"*, alcuni psicologi ed antropologi, grazie ai risultati di recenti esperimenti cognitivi, cercano di arrivare a delle teorie plausibili.

Effetto dell'evoluzione
-----------------------

L’ipotesi evoluzionistica sull'origine della religione, sostiene che esse si
siano sviluppate per questioni di adattamento all'ambiente. I fenomeni di
aggregazione sociale, secondo questa ipotesi, furono favoriti dallo sviluppo di
religioni comuni fra i membri dei gruppi. Questo avrebbe creato gruppi molto
uniti e competitivi, quindi con maggiori possibilità di riproduzione e
trasmissione dei geni.

Quest'idea non è completamente condivisa. Secondo **Scott Atran**, antropologo
dell'università del Michigan, l’avere credenze prive di riscontri nella realtà,
non darebbe alcun vantaggio in termini di adattamento. Ad esempio, la credenza
nell'aldilà, sarebbe addirittura controproducente alla sopravvivenza
dell'individuo nel qui-ed-ora. In maniera simile, qualsiasi credenza specifica,
non spiegherebbe l’origine della religiosità, ma solo il modo in cui si è
propagata.

Effetto collaterale del pensiero astratto
-----------------------------------------

Secondo Atran, sarebbe il pensiero astratto ad averci reso così efficaci
nell'adattamento e la religiosità nascerebbe come effetto collaterale di esso. È
ciò che permette il pensiero soprannaturale, ad essere in noi innato, ed il
pensiero soprannaturale rimane inconsciamente sempre presente.

**Paul Bloom**, psicologo di Yale, sostiene che ciò sia dovuto al fatto che
abbiamo due sistemi cognitivi separati, che entrano in gioco a seconda che si
abbia a che fare con oggetti viventi, quindi dotati di coscienza o almeno che
mostrino intenzionalità, oppure con oggetti inanimati.

Dagli esperimenti fatti con bambini dell'età inferiore ai cinque mesi, si ha
avuto la dimostrazione che già a quell'età, i due sistemi iniziano a
differenziarsi. Negli esperimenti, i bambini mostravano sorpresa nel vedere
muoversi un oggetto inanimato, ma nessuna reazione particolare quando l’oggetto
in questione era una persona. Per i bambini, gli oggetti inanimati seguono
sempre precise regole fisiche e sono quindi prevedibili, mentre solo le persone
e gli esseri viventi hanno la facoltà di potersi muovere a piacimento.
<!--more-->

Dualismo fra mente e materia
----------------------------

Questa separazione, ci porta a fare una distinzione fra menti e corpi. Ci è di
aiuto nell'immaginare le intenzioni degli oggetti viventi in mancanza di una
rappresentazione fisica del loro corpo. La stessa capacità ci facilita il
compito di mantenere relazioni sociali e gerarchie.

L’effetto collaterale, è la tendenza ad immaginare l’esistenza di menti senza
corpo e ci mette in difficoltà quando oggetti che dovrebbero essere inanimati,
si comportano in modo che non sappiamo prevedere. Nei bambini, è tipico credere
in amici immaginari, oppure di poter parlare con i parenti morti. Allo stesso
modo, è molto comune in tutte le popolazioni, la credenza di poter uscire dal
proprio corpo nei sogni, oppure nelle cosiddette proiezioni astrali. Ed
ovviamente, anche gli dei fanno parte di questo gruppo di esseri senza corpo ed
invisibili, ma esistenti. Questo porta con se anche la credenza nella vita dopo
la morte, lo spiritismo e l’animismo. Nel rappresentarsi questi spiriti o dei,
attribuiamo ad essi delle caratteristiche e dei modi di pensare tipicamente
umani.

In un altro esperimento con dei bambini, è stato mostrato loro uno spettacolo di
marionette nel quale si vedeva un serpente mangiare un topo. Dopo lo spettacolo,
sono state fatte ai bambini alcune domande. In quelle che riguardavano gli
aspetti fisici: se il topo potesse ancora ammalarsi, oppure se sentisse il
bisogno di mangiare, i bambini rispondevano di no. Mentre alle domande
riguardanti questioni più “spirituali”: se il topo pensasse o potesse sapere
certe cose, i bambini rispondevano di sì.

Causalità estremizzata
----------------------

Un’altra caratteristica tipica ed innata del pensiero umano, è la ricerca dei
rapporti di causa ed effetto. Tendiamo ad assumere che ogni evento debba avere
una causa scatenante. Ciò, nella maggiore parte dei casi porta ad una deduzione
corretta, ma le cose si complicano quando le cause diventano meno evidenti,
oppure gli effetti hanno ripercussioni emotive molto forti sui soggetti. In
questi casi, entrano facilmente in gioco questi esseri immaginari, latori in
intenzioni. La conseguenza di queste due cose, è il credere che gli eventi
accadano per una causa, generata da uno scopo.

Siamo abituati fin piccoli a pensare che ogni evento accada perché qualcuno l’ha
fatto accadere. Il giocattolo si è schiantato per terra perché io l’ho fatto
cadere dal seggiolone… La mamma mi sta allattando, perché l’ho chiamata
piangendo… La palla serve per giocare… Fino a cose più complesse. Quando gli
eventi da spiegare sono troppo complessi, la soluzione cognitivamente più
economica è pensare che qualche entità invisibile che li abbia causati.

Questo tipo di deduzione, seppur ingenua, è estremamente utile quando è in gioco
la sopravvivenza. Immaginiamo di essere dei primati e di veder muoversi un
cespuglio. È più conveniente arrivare istantaneamente alla conclusione che ci
sia qualcuno dietro al cespuglio, forse un predatore, e fuggire, che fermarsi ad
analizzare razionalmente l’evento. A costo di fuggire anche quando non sarebbe
necessario.

A livelli più astratti di pensiero, gli stessi meccanismi, avrebbero portato
alcuni notevoli pensatori, alla ricerca di una *"causa prima"*.

Ad un gruppo di ragazzini di sette-otto anni, **Deborah Kelemen**
dell'università dell'Arizona, facendo loro delle specifiche domande riguardo ad
oggetti inanimati ed animali, ha scoperto che la maggior parte dei bambini
credeva che gli oggetti e gli animali fossero stati creati per degli scopi
precisi. Una sorta di ingenuo [intelligent design][1]. Le rocce appuntite
esistono per permettere agli animali di grattarsi, gli uccelli per "fare belle
musiche", e via dicendo.  Chiedendo a bambini in età prescolare, chi pensavano
avesse creato certi particolari oggetti naturali, come piante o animali, la
probabilità che essi rispondessero "Dio" era sette volte maggiore della risposta
"uomo".

Per scoprire quanto l’influenza degli adulti condizioni il pensiero infantile
nella creazione di dèi, sarebbe necessario fare un esperimento nel quale dei
bambini vengono lasciati a se stessi, permettendo loro di sviluppare un proprio
linguaggio autonomo ed una propria cultura. Un esperimento simile, tuttavia, non
è realizzabile per ovvi motivi etici.

A differenza dei bambini, gli adulti si legano alla religione principalmente per
via degli eventuali insegnamenti morali.

L’ultima risorsa
----------------

Kelemen ha dimostrato che la tendenza al pensiero soprannaturale ed irrazionale,
permane anche negli adulti messi sotto pressione riguardo l’origine di certi
fenomeni. In alcuni test, essi sono arrivati a rispondere che gli alberi
producono ossigeno perché gli animali possano respirare, oppure che il sole
produca calore per sostenere la vita.

Anche in persone dichiaratamente non credenti, questi meccanismi si possono
manifestare in situazioni di forte stress e perdita di controllo sulla propria
vita.

Nel tentativo di spiegare a se stessi eventi molto traumatici, può emergere la
necessità di trovare a tutti i costi una soluzione per evitare di venire
sopraffatti dall’evento.

Atran sostiene che questo meccanismo sia una sorta di jolly, che ci fornisce il
cervello, per permetterci di uscire da situazione estremamente difficili.

In un esperimento (su *Science vol.322, p.115*), si è cercato di riprodurre il
meccanismo. Ad un gruppo di persone è stato chiesto di trovare uno schema in una
serie di immagini di punti, e nelle informazioni della borsa. Metà dei
partecipanti sono stati indotti a provare sensazioni di perdita di controllo,
dando loro del feedback non relativo all'esperimento, o facendo ricordare loro
delle situazioni nelle quali avevano perso il controllo.

I risultati sono stati tanto stupefacenti da sorprendere anche gli
sperimentatori. **I soggetti che avevano sperimentato la perdita di controllo,
erano molto più propensi a trovare schemi e significati dove non c’erano.**

Questo dimostrerebbe in che modo l’uomo tenda a cadere nel pensiero
superstizioso quando sente di aver perso il controllo, e spiegherebbe perché le
religioni tornano maggiormente in auge durante i periodi più duri dell'umanità.

La religione sarebbe una conseguenza del funzionamento del nostro cervello.
Ovviamente, i risultati non ci dicono nulla sull'esistenza o meno di dèi, ma in
queste condizioni, l’ateismo si "vende" meno facilmente della religione.

L’indottrinamento, grazie a questi meccanismi, ottiene un supporto maggiore e
rafforza a sua volta l’effetto collaterale della credenza.

[1]:https://it.wikipedia.org/wiki/Intelligent_design
