title: Riconosciamo un animale prima ancora di esserne coscienti
date: 2015-02-24 18:44:09 +0100
tags: scienza, psicologia, neuroscienze
summary: Uno studio condotto da ricercatori dell’Istituto di bioimmagini e fisiologia molecolari del Cnr di Milano e da Alice Mado Proverbio dell’Università Milano Bicocca, coordinati da Alberto Zani del Cnr, ha dimostrato che il cervello umano distingue gli esseri viventi dagli oggetti in modo rapidissimo e automatico, indipendentemente dalla nostra volontà.

Da [Le scienze][1], su segnalazione di Gianluca Bartalucci, di cui vi consiglio [blog][2] e [tesi][3].

*Comunicato stampa - Uno studio condotto da ricercatori dell’Istituto di
bioimmagini e fisiologia molecolari del Cnr di Milano e da Alice Mado Proverbio
dell’Università Milano Bicocca, coordinati da Alberto Zani del Cnr, ha
dimostrato che il cervello umano distingue gli esseri viventi dagli oggetti in
modo rapidissimo e automatico, indipendentemente dalla nostra volontà. La
ricerca è stata pubblicata su Biological Psychology*

Roma, 19 febbraio 2015 - Il cervello umano reagisce più rapidamente alla vista
di un animale che di un oggetto e la risposta cerebrale avviene in modo
automatico, in uno stadio cioè precedente a quello della coscienza. È quanto
emerge da uno studio pubblicato sulla rivista “Biological Psycology”  e condotto
da un team formato da ricercatori dell’Istituto di bioimmagini e fisiologia
molecolari del Consiglio nazionale delle ricerche (Ibfm-Cnr) di Milano e da
Alice Mado Proverbio, del Milan Center for Neuroscience dell’Università Milano
Bicocca. 

“Il cervello analizza e discrimina rapidamente gli animali dagli oggetti
inanimati, indipendentemente dalla volontà del soggetto o dalle circostanze”,
spiega Alberto Zani dell’Ibfm-Cnr, che coordina il gruppo. “Questo comportamento
riflette probabilmente meccanismi biologici radicati nel cervello ed evolutisi
per rispondere alla necessità di distinguere, in una frazione di secondo, un
animale nascosto in un ambiente visivamente complesso o un esemplare inoffensivo
da uno pericoloso per l’incolumità umana. Di solito nell’essere umano la vista è
guidata dall’attenzione e, se quest’ultima manca, le informazioni sfuggono alla
nostra consapevolezza, ma questo processo non riguarda gli animali, ai quali è
riservato un sistema di monitoraggio automatico”.

Lo studio ha registrato la velocità di risposta motoria e i potenziali
bioelettrici cerebrali (Erp) generati da coppie di immagini statiche di animali,
oggetti o di queste categorie assieme, sottoposte a un gruppo di
studenti a cui è stato chiesto di giudicare se esse appartenessero a una stessa
categoria o a una diversa. “I risultati evidenziano come gli animali producano
una risposta motoria del cervello più veloce (564 millisecondi per gli animali
contro 626 per gli oggetti), con un maggiore potenziale bioelettrico nelle aree
occipito-temporali e occipito-parietali dell’emisfero destro a partire dai
120-180 ms. Questa prima onda negativa generata nel cervello dei partecipanti
dalle diverse immagini, chiamata N1, indica l’intervallo temporale entro il
quale la risposta cerebrale è maggiore per le immagini degli animali che per gli
oggetti”, prosegue Zani.

Che il cervello distingua prioritariamente gli animali dagli oggetti era già
noto, ma sempre nell’ambito di compiti che richiedevano di discriminare
volontariamente i primi dai secondi e uno studio del gruppo Ibfm-Cnr e Bicocca
del 2012 aveva riconfermato questa realtà.”Ciò che non era noto è che, anche
quando non dobbiamo distinguere volontariamente esseri viventi e non viventi, il
nostro cervello lo fa in modo automatico e al di fuori della nostra volontà
cosciente, non si sapeva cioè dell’esistenza di regioni e processi specifici per
l’elaborazione prioritaria involontaria degli animali nella corteccia visiva”,
precisa il ricercatore dell’Ibfm-Cnr. “Molto probabilmente, ciò dipende dal
fatto che il cervello coglie le informazioni sensoriali più ‘omomorfe’,
caratterizzate da un volto con occhi e bocca e dagli arti, tipiche degli
animali, rispetto a quelle con fattezze più squadrate e lineari, quali gli
oggetti. Conseguentemente, l’informazione raccolta attraverso  la vista e i
sensi ha un accesso ‘facilitato’ alla rappresentazione mnemonica delle prime,
determinando risposte più veloci che per le seconde”. 

“La straordinaria precocità dell’effetto suggerisce come l’immagine degli
animali abbia uno status speciale per il nostro cervello che si manifesta fin
dalla più tenera età, come dimostra la particolare 
attrazione che i bambini manifestano per gli animali e per tutto ciò che vi
assomigli, da Peppa Pig a Topolino, fino ai peluche”, conclude Proverbio.

[1]:http://www.lescienze.it/lanci/2015/02/19/news/cnr_riconosciamo_un_animale_prima_ancora_di_esserne_coscienti-2492192/
[2]:https://some1elsenotme.wordpress.com
[3]:https://some1elsenotme.wordpress.com/2015/02/23/meccanismi-cerebrali-innati-di-rappresentazione-degli-animate-living-e-atteggiamento-intenzionale-di-d-c-dennett-una-connessione-tra-neuroscienze-e-filosofia-della-mente-2014/
