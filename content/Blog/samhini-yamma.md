title:  Samhini yamma. Chissà se la madre di Amir capirà
date:   2015-08-05 20:10
tags:   società, politica, etica
summary: Riporto questa storia da "Fortress Europe". Com’è scritto nel blog di Gabriele Del Grande, questa è la storia che studieranno i nostri figli, quando nei testi di scuola si leggerà che negli anni duemila morirono a migliaia nei mari d’Italia e a migliaia vennero arrestati e deportati dalle nostre città. Mentre tutti fingevano di non vedere.

Riporto [questa storia][1] da "Fortress Europe". Com’è scritto nel blog di Gabriele Del Grande, questa è la storia che studieranno i nostri figli, quando nei testi di scuola si leggerà che negli anni duemila morirono a migliaia nei mari d’Italia e a migliaia vennero arrestati e deportati dalle nostre città. Mentre tutti fingevano di non vedere.

Questa è una delle tante storie di persone che quotidianamente vengono imprigionate e torturate, qua in Italia, perché provenienti da altri paesi, come la nigeriana [Joy][2], che subì un tentativo di stupro dal capo della polizia *Vittorio Addesso*, nel [CIE][3] di Milano. Tutto il meccanismo poliziesco e burocratico, si mise in moto per mettere a tacere la storia e deportare lei ed i testimoni. Le notizie su quanto sta succedendo, sui canali ufficiali, arrivano sempre molto tardi ed hanno pochissima visibilità. Nell'agosto 2011, il governo aumentò il periodo di detenzione nei CIE, da sei a diciotto mesi.

Stazione di Torino Porta Nuova. Il treno regionale per Bologna delle 18:20 è in
partenza al binario 10. Dietro il finestrino, Mahmud si infila gli auricolari
dell’iPhone e schiaccia play.

[Samhini yamma][4] di Ashref. Forse ha sbagliato canzone. O forse è proprio
quella giusta per questo momento. Samhini yamma, perdonami mamma. Perdonami se
me ne sono andato, perdonami l’esilio, perdonami l’assenza. A salutarlo dal
marciapiede del binario c’è un ragazzo con gli occhi arrossati dalle lacrime. È
il suo migliore amico. Singhiozza.

Sono cresciuti insieme per le strade di Sfax, in Tunisia. Insieme hanno
lavorato per anni sui pescherecci di Kerkennah e insieme hanno fatto la
traversata per Lampedusa. Era il 24 gennaio. Sono passati sei mesi da allora. E
adesso è arrivato il momento più difficile del viaggio. Il momento di dirsi
addio. Mahmud va a Parma, Hasan a Parigi. Raggiungono i parenti. In tasca hanno
un foglio di via. Li hanno appena rilasciati dal centro di identificazione e
espulsione di Torino, insieme a un altro amico della comitiva di Sfax, Amir,
che ha fatto la traversata sulla loro stessa fluca (barca) insieme a altri sei
passeggeri. Per loro il viaggio ricomincia da qui. Dopo sei mesi di detenzione.
Con la stessa determinazione di riuscire, ma con molta più amarezza nel cuore.
Perché l’Europa che hanno sognato per anni, ha cessato di esistere nel loro
immaginario.

Per Mahmud l’immagine dell’Europa è sfocata pian piano, al crescere delle dosi
di psicofarmaci che prendeva dentro il Cie di Torino. Trenta gocce di Rivotril
al mattino, trenta al pomeriggio e trenta alla sera. Per spegnere la mente. E
dormire più a lungo possibile. Ancora non ha recuperato la vivacità dello
sguardo dei suoi 26 anni. Ma va già meglio di ieri, quando appena uscito dal
Cie metteva le sigarette in bocca intontito e poi si dimenticava di accenderle.

Per Hasan invece l’Europa dei sogni si è trasformata in una macchia viola,
scura come i lividi che gli hanno lasciato le manganellate della polizia sulla
schiena. Era l’inizio di febbraio. L’idea era stata di tutti e tre, lui, Mahmud
e Amir. Di nodi se ne intendevano bene perché hanno lavorato per anni come
marinai in Tunisia. A forza di intrecciare le lenzuola erano riusciti a
ottenere una corda lunga nove metri, con un nodo ogni mezzo metro per
arrampicarsi meglio. Il resto avvenne in un attimo. Lanciarono la corda al di
là della gabbia di ferro alta cinque metri. Mahmud e Amir la tenevano dal basso
e Hasan che era il più leggero si lanciò nell’arrampicata. Saltato giù dalla
rete corse verso l’uscita su corso Brunelleschi con tutta la forza che aveva
nelle gambe. Ma non fu abbastanza rapido.

I militari di guardia nelle garitte lo bloccarono a pochi metri dall’uscita.
Non gli torsero un capello davanti agli altri reclusi affacciati alle gabbie.
Si limitarono a prenderlo sotto braccio e a condurlo di buona lena verso gli
uffici della Croce Rossa. Fu lì che avvenne il pestaggio. Se ne occupò la
polizia. In due lo tenevano per le braccia. E un terzo da dietro gli sferrò due
calci nei calcagni per farlo sbilanciare e cadere, e poi giù di colpi con i
manganelli. Il solito pestaggio punitivo. Al Cie di Torino è la prassi. Ne sa
qualcosa anche il terzo ragazzo della comitiva di Sfax, Amir. Che di quelle
violenze porterà per sempre un segno indelebile sul braccio sinistro.

Vanno dal bicipite al polso. Parallele, una sotto l’altra, sottili e ancora
arrossate. Sono le cicatrici dei tagli. I tagli che si fece con un pezzo di
vetro di una finestra per farli smettere di picchiare. Quel giorno lo avevano
preso in sei. Non sa dire se fossero poliziotti o finanzieri. Sa solo che non
la smettevano di picchiarlo e che l’unica idea che gli venne in mente per
fermarli fu quella di riempirsi di sangue tagliandosi le vene.

Tutto era cominciato la mattina nell’area gialla dove era recluso. Aveva avuto
una brutta discussione con un ispettore. Gli aveva detto che il cibo era
immangiabile. E quando il poliziotto gli aveva risposto che se non gli piaceva
lo poteva pure buttare via, lui glielo aveva tirato addosso in un gesto di
sfida, contro la gabbia di ferro che li separava. Il pestaggio fu la lezione
che decisero di impartirgli per addomesticarlo. Un po’ come fanno certe persone
con i cani quando non obbediscono. Per essere poi sicuri che avesse imparato
una volta per tutte la lezione, dopo le bastonate lo spedirono all’isolamento.

Due mesi rinchiuso in una stanza. Da solo. Tutto il giorno, a parte l’ora
d’aria nel campetto di calcio per sgranchirsi le gambe. Il sogno dell’Europa di
Amir è finito per sempre là dentro. Un giorno di primavera, appeso al cappio
che si mise intorno al collo quando decise che era troppo. E che non valeva più
vivere, continuamente umiliati e trattati come animali. Se la morte non se lo
portò via, ironia della sorte, fu proprio grazie a un militare. Un alpino di
guardia alla garitta di fronte all’isolamento. Che appena vide la scena, corse
sul posto con un coltellino a tagliare la corda con cui Amir si era impiccato,
giusto in tempo per portarlo in infermeria senza che il tentato suicidio
potesse avere ripercussioni sulla sua salute. Dopodiché fu la volta
dell’incendio.

In quel periodo, i tunisini reclusi nel Cie non parlavano d’altro. Come
scappare e tornare in libertà, per poter finalmente proseguire il viaggio verso
la Francia come avevano fatto i primi arrivati a Lampedusa e lasciati fuggire
dai centri di accoglienza di Bari e Crotone e dalle tendopoli tipo quella di
Manduria. Constatato che l’evasione era impossibile, che gli psicofarmaci non
facevano che rovinargli la salute e che il suicidio era soltanto una follia,
qualcuno iniziò a pensare che non rimaneva che il fuoco. Se avessero reso
inagibile il centro, da qualche altra parte avrebbero pure dovuto trasferirli e
magari li avrebbero rilasciati.

All’area gialla parteciparono spontaneamente alla rivolta quattro camerate su
cinque. Era il 18 febbraio. I reclusi appiccarono il fuoco a materassi e
lenzuola. La sezione venne devastata dalle fiamme e dopo l’intervento dei
vigili del fuoco venne chiusa, per permettere i lavori di ristrutturazione. Dal
Cie però non venne fatto uscire nessuno. Al contrario, due tunisini finirono in
galera con pesanti accuse di incendio e devastazione. E gli altri si videro
negato in modo arbitrario il permesso umanitario di sei mesi che il governo
italiano aveva nel frattempo deciso di concedere a tutti i tunisini sbarcati in
Italia prima del 5 aprile.

Così i tre amici di Sfax hanno dovuto aspettare lo scadere dei sei mesi di
detenzione prima di tornare in libertà. Sono usciti il pomeriggio di mercoledì
scorso. E dopo un giorno, eccoli già in partenza. Mahmud e Hasan sono partiti
per primi. Amir ha aspettato due giorni in più. Il tempo per disintossicarsi
dagli psicofarmaci e per riambientarsi alla vista del cielo all’orizzonte e al
rumore delle automobili, ai colori dei negozi e alle risate dei passanti, alle
voci dei bambini e ai profumi del cibo buono.

Confuso tra la folla dei viaggiatori alla stazione di Torino Porta Nuova,
sembra un ragazzo qualunque in partenza per il mare. Pantaloncini corti,
maglietta, e alle spalle uno zainetto con un paio di cambi. La differenza è che
in tasca ha un foglio di via. Gli hanno dato sette giorni per lasciare
l’Italia. Gliene restano quattro. Dopodiché sarà di nuovo illegale. E potrà in
qualsiasi momento essere di nuovo fermato dalla polizia e riportato in un Cie
per altri 18 mesi.

Sa dei rischi. Sa che la vita sarà dura. Ma da buon marinaio è abituato a
navigare in cattive acque. E poi i rischi fanno parte dell’avventura.
L’importante è non perdere la rotta.

Per ora è salito sul treno per Civitanova, nelle Marche lo aspetta un
compaesano. In Tunisia non potrà tornare per lunghi anni, fino a quando non
avrà un documento di soggiorno. È la clandestinità. Samhini yamma. Chissà se la
madre di Amir capirà.

[1]:http://fortresseurope.blogspot.it/2011/08/samhini-yamma-chissa-se-la-mamma-di.html
[2]:http://vegananarchist.blogspot.com/search/label/joy
[3]:https://it.wikipedia.org/wiki/Centro_di_identificazione_ed_espulsione
[4]:https://www.youtube.com/watch?v=2WL_89W5Pmw
