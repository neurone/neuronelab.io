title: Ma come si può uccidere un bambino?
date: 2013-11-09 11:53
tags: film, orrore, recensioni
summary: Girato nel 1976 dal regista uruguaiano-spagnolo Narciso Ibáñez Serrador, "¿Quién puede matar a un niño?", tratto dal romanzo *El juego de los niños* di *Juan José Plans*, appartiene al filone satirico e di critica sociale del genere horror, ed è un film di culto abbastanza controverso. Alcuni paesi, fra cui Finlandia ed Islanda, l’hanno addirittura messo al bando. Nel 1977 vinse il *premio della critica* al *Festival internazionale del film fantastico di Avoraz*.

Girato nel 1976 dal regista uruguaiano-spagnolo [Narciso Ibáñez Serrador][1], ["¿Quién puede matar a un niño?"][2], tratto dal romanzo *El juego de los niños* di *Juan José Plans*, appartiene al filone satirico e di critica sociale del genere horror, ed è un film di culto abbastanza controverso. Alcuni paesi, fra cui Finlandia ed Islanda, l’hanno addirittura messo al bando. Nel 1977 vinse il *premio della critica* al *Festival internazionale del film fantastico di Avoraz*.

La pellicola inizia mostrando immagini di repertorio in bianco e nero, prese
dalle più recenti tragedie e guerre di quel periodo: l’olocausto nazista, le
guerre in Vietnam e Corea, le guerre civili africane. In questi primi otto
minuti, viene evidenziato come a fare le spese di tutte queste follie siano
sempre i bambini.

Sulle soleggiate spiagge della Spagna, una giovane coppia sposata, Tom e Evelyn,
quest’ultima, in attesa di un bimbo, sta trascorrendo la sua vacanza in attesa
di imbarcarsi verso l’isola di Almanzora, a pochi chilometri dalla costa. Il
paesaggio ed i colori sono luminosi e soleggiati, *l’opposto di quello che ci si
aspetterebbe da un film horror*. Arrivati all'isola, essa appare come se fosse
stata abbandonata in tutta fretta. Carretti dei gelati incustoditi, un pollo
carbonizzato dimenticato sul girarrosto ancora in funzione… Tutto è deserto.  
Il sole splendente ed il cielo limpido non bastano a trattenere il senso di
inquietudine provocato dallo scoprire, abitazione dopo abitazione, che tutta
l’isola sembra innaturalmente disabitata. *Gli unici abitanti dell’isola,
sembrano essere dei bambini*, che paiono guardare questa coppia di turisti con
curiosità e diffidenza.

![¿Quién puede matar a un niño?]({filename}/images/nino.jpg){: style="float:right"}
Non ci vorrà molto perché s'inizi a svelare l’orrore nascosto dietro a tutto
questo: i bambini dell’isola sembra siano stati presi da una strana *follia
omicida* contro tutte le persone adulte che incontrano. La cosa più
terrificante è l’innocenza con la quale commettono gli omicidi: sono
sorridenti, si comportano esattamente come se stessero giocando. La scoperta
avviene sotto gli occhi increduli di Tom: una bambina sottrae il bastone ad un
vecchio, ed inizia a picchiarlo violentemente fino a quando non smette di
muoversi. Al tentativo di Tom di fermarla e di chiederle, quasi in lacrime:
“Perché? Perché l’hai fatto?”, la bambina risponde con una divertita risatina e
scappa in un vicolo.  
Da questo momento in poi, la tensione sale sempre più. La coppia scopre che ci
sono alcuni superstiti e cerca di raggiungerli per aiutarli e fuggire assieme.
Uno di questi è un uomo in stato di shock, dal quale riescono a farsi
raccontare cosa sia successo. Ripensando alle immagini iniziali, sembra
quasi che i bambini si stiano prendendo una rivincita sugli adulti, che troppo
spesso li hanno messi da parte e li hanno resi vittime dei loro “giochi di
guerra”.
L’uomo racconta che i bambini si sono svegliati, una notte, e scesi in strada
tutti assieme, hanno iniziato ad andare casa per casa, cantando e festeggiando,
mentre aggredivano e massacravano gli adulti. Increduli, Tom e Evelyn chiedono
all’uomo come hanno fatto dei fanciulli a sopraffare degli adulti, ma questi
risponde con una domanda: *“Come si può uccidere un bambino?”*. Una domanda che
fa riflettere anche per via del *contrasto con ciò che è stato mostrato
nell’introduzione*. Poco dopo, arriverà lì una bambina che prenderà per mano
l’uomo, e se ne andrà. Tom cercherà di fermarlo, ma lui continuerà a seguire la
bambina. Si fermerà solo un attimo per dire, come per giustificarsi: “è mia
figlia”, poi sparirà, facendo la stessa fine degli altri.

Il film prosegue in un crescendo sempre più terrificante. La situazione
descritta crea un terrore doppio: la paura che i bambini uccidano i
protagonisti, e quella di vedere i protagonisti uccidere dei bambini, anche
solo per difendersi. È una situazione senza via di scampo.

- [Narciso Ibáñez Serrador][1]
- [¿Quién puede matar a un niño?][2]

[1]: http://www.imdb.com/name/nm0406654/
[2]: https://it.wikipedia.org/wiki/Ma_come_si_pu%C3%B2_uccidere_un_bambino%3F
