title: Lavabit's and Snowden's Solos
date: 2016-03-17 18:37
tags:  società, rete, libertà, privacy, hacking, sicurezza
summary: Il gestore dell'ex servizio di posta sicura Lavabit racconta di come l'F.B.I. ed i giudici gli impedirono di difendersi efficacemente in aula alle richieste di compromettere la sicurezza del proprio servizio per permettere loro di intercettare le comunicazioni di Edward Snowden e di come il suo caso differisca da quello che attualmente coinvolge Apple nel caso di San Bernardino.

Il gestore dell'ex servizio di posta sicura [Lavabit][1] racconta di come
l'F.B.I. ed i giudici gli impedirono di difendersi efficacemente in aula
alle richieste di compromettere la sicurezza del proprio servizio per permettere
loro di intercettare le comunicazioni di [Edward Snowden][2] e di come il suo caso
differisca da quello che attualmente coinvolge Apple nel [caso di San Bernardino][3].

Testo originale: [http://www.metzdowd.com/pipermail/cryptography/2016-March/028669.html][4]

## [Cryptography] Lavabit's and Snowden's Solos

As the sysop I feel qualified to clarify. I don't keep up with this list
as closely as I should, so Mr Young, thank you for pointing me to this
thread.

Ray, your right. The two cases are similar. They both seek to defeat the
encryption used to protect data at rest. They differ in that the Lavabit
case relied upon a surveillance order under the PRTT statue, and a
search warrant issued in accordance with the SCA. The Apple case relies
on a writ of assistance issued under the AWA. They differ
technologically in that the Lavabit case involved a MitM attack to
capture the password during login, while the Apple case involves
disabling login controls to facilitate a brute force attack.

Because both cases involve what I call "extraordinary assistance," I
co-authored an amicus brief in support of Apple. The lawyers changed the
definition of ordinary assistance at the last second, so I'm including
the correct one here:

We contend this request is extraordinary because it seeks to compel the
use, modification, or disclosure of confidential intellectual property,
such as source code or encryption keys, which belongs to an innocent
third party. In contrast, ordinary assistance requests seek access to,
or the surrender of data in possession of a third party that has been
created by the investigative target, or generated as a byproduct of the
target’s actions.


There is a short "background section" in the amicus brief which
discusses the Lavabit case, and how it differs. That said, with only 7
days to write it, we didn't include everything I wanted to say. Namely a
discussion of cyber weapons, non-repudiation, and compelled felonious
conduct. If this case continues onto a round two, I'm hoping to expand
upon what we said this round. If your interested in what we did write,
see here for a summary, plus a few pithy comments that were censored (by
the lawyers):

[https://www.facebook.com/KingLadar/posts/10156672763765038][5]

Or for the full brief:

[http://lavabit.com/files/Lavabit.Amicus.Brief.20160303.pdf][6]

[https://cryptome.org/2016/03/usg-apple-102-105.pdf][7]

All of that said, the government did try to cite the Lavabit case as a
precedent in their reply brief. See footnote #9, page 22 (or page 30 in
the PDF), which reads:

For the reasons discussed above, the FBI cannot itself modify the
software on Farook’s iPhone without access to the source code and
Apple’s private electronic
signature. The government did not seek to compel Apple to turn those
over because it believed such a request would be less palatable to
Apple. If Apple would prefer thatcourse, however, that may provide an
alternative that requires less labor by Apple programmers. See In re
Under Seal, 749 F.3d 276, 281-83 (4th Cir. 2014) (affirmingcontempt
sanctions imposed for failure to comply with order requiring the company
to assist law enforcement with effecting a pen register on encrypted
e-mail content whichincluded producing private SSL encryption key).

I found this citation so misleading that I decided to write a little
statement about it. Namely, the appellate court never addressed the
substantive question of whether the FBI can seize keys. The court ruled
that I waived my right to appeal (which is impossible, since a careful
reading of the facts would show I never had a chance to make the
objection they contend I should have). Because they ruled on the waiver
issue, they explicitly avoid any discussion of the lawfulness of
extraordinary assistance. If you want to read my full statement, see:

[https://www.facebook.com/KingLadar/posts/10156714933135038][8]

As you probably picked up on, what Ray wrote below, while it captures
the gist of things, isn't quite right on a few details. I made my
comments inline.


On 3/16/2016 3:02 PM, Ray Dillinger wrote:
> Lavabit received an order asking IIUI to enable the FBI to access a
> *single* account. The operator, fearing that their motive might be
> persecution and vengeance rather than prosecution and justice, took less
> than 48 hours to consult with an attorney. Consulting with an attorney,
> as a constitutionally guaranteed right, seems reasonable to me.

The FBI originally served the PRTT order around 5pm on June 28th, a
Friday. After a 2-3 hour discussion with the agents, the meeting ended
with me telling them I needed to consult with an attorney, as I was
"uncomfortable with providing the SSL/TLS private key, and didn't know
what my obligations under the law were." That very same night I received
an order to compel, issued by a magistrate judge, which ordered me to
provide all of the necessary "technical assistance" to "decrypt the
data." Since the order (and statue) made no mention of the SSL/TLS
private key, I emailed the FBI asking for them to provide a written
description of what "technical assistance" they required. They never
provided the description I asked for. I wouldn't get the written demand
for another 2 weeks, and it arrived in the form of a subpoena (which I
was later excused from, and as such didn't include in the description
above). While I waited for the FBI to get back to me I began my search
for a lawyer.

Since the following week included July 4th, it ended up taking be about
a week to find a qualified lawyer. She was referred to me by the EFF,
and such understood the relevant statues and case law. It wasn't until I
consulted with her that I understood the PRTT statue only provided the
authority to collect metadata, as the agents implied it provided the
authority to collect everything on the wire. I subsequently read the
statue, and it listed "signalling information" as one of the items they
could collect. Without a lawyer, and based on the FBI agent's
description,  I incorrectly assumed that meant the "signal to noise
ratio," where the signal represents all of the information. When it
became clear that actually was a reference to the DTMF tones sent over a
phone line, and they were only authorized to receive metadata, I did try
to work with them for a solution that would provide a technological
guarantee"they only collected the authorized metadata on the account(s)
authorized by the court.

It's worth noting my custom mail server was probably the only one on the
planet that didn't write out the required metadata to a log file. As
such I proffered several possible solutions, which I can discuss at
length later if anyone is interested. All were rejected. Amongst the
reasons the DoJ came back with were a) they didn't trust me, b) my
estimate for costs if I implemented the logging was unreasonable, and c)
my proposal didn't provide them with real time access. Note I also
discussed a situation whereby they provided the equipment and I
configured it (with there help) and we each held onto half of the
password. While I didn't like the idea because it would be hard to know
if there was a backdoor, it was quickly rejected as well.

Whether you believe the FBI really only wanted metadata probably hinges
on whether you believe a prosecutor would lie in court. It might be
worth noting there was also a search warrant issued for the source code,
the encrypted user data, and the encrypted user keys. Data that was
largely worthless without the user password. All of that said, I can
prove the (now), that the prosecutor made a significant and material
misrepresentation to the court which would have changed things had I
been able to prove it at the time. Since I haven't discussed this
publicly yet, I'll save it for another day. What I have discussed
publicly is how many immaterial misrepresentations the DoJ made about
me. Including my favorite, which insinuated I tried to avoid service by
fleeing out the backdoor of my 5th floor apartment. It was one of two
times I laughed during the whole ordeal. I really wish I could fly, or
crawl on walls, but alas I'm only human.

What Ray is probably referring to is the approximately 48 hours I had
between when I found out my first attorney couldn't represent me in
Virginia the following week (she was based in San Francisco). In that
gap I interviewed a dozen plus lawyers, but didn't find a good match
(cost, knowledge, strategy, etc) in time. I asked for more time, but was
denied. Hence I showed up pro se (if I hadn't they would have sought a
bench warrant for my arrest and dragged me to Virginia in handcuffs). I
was finally able to find an attorney the morning of my hearing, but not
in time for them to make it to court. I hired him that afternoon, and he
had a week to prepare a defense. Notably, he wasn't given transcripts of
my pro se appearance until our appeal was due.

> He may or may not have voluntarily provided access to that account -
> we'll never know, because by the time he had finished consulting an
> attorney the FBI had, apparently because of his less than instant
> compliance, made a completely intolerable demand instead for the keys to
> the entire site. That would have enabled covert, real-time access to the
> communications of subscribers who were specifically purchasing the
> service of privacy. Deprived of the ability to sell what his subscribers
> were buying, he shut the business down rather than engage in fraud or
> the provision of pretended services.

See above, but yes, my desire to consult an attorney led to an instant
order to compel. And yes, I was unwilling to compromise the integrity of
the system, and found it even more abrasive because I wouldn't have been
allowed to tell anyone what happened. Hence the decision to shutdown
after an abbreviated court battle.

> Then as now, the FBI made a demand for access so broad in scope,
> burdensome to provide, or contrary to the basic principles of the party
> from whom it was demanded, as to be offensive.
>
> The major distinction as far as I can tell is that Apple has the money
> to hire a herd of lawyers and fight it in court, and Lavabit didn't.

Largely true. In an interesting twist, one of the lawyers I consulted
during the 48 hour period is now representing Apple, but I couldn't
afford him, and because of the secrecy, could broadcast a call for help.
That meant even though I was aware of a couple cyber law mailing lists,
I could ask for help pro bono, or seek the money to cover the cost of a
proper defense. The fact that we were only given a week, and my lawyer
wasn't a specialist made things even harder.

Over the last 2.5 years I've certainly gained a _lot_ of knowledge about
the relevant laws which would have been helpful at the time, and will be
if/when I ever relaunch. It's also why I decided to submit the amicus.

> If Lavabit had fought the demand in court and lost (very likely given
> its meager legal budget and the infamy of the single account that the
> FBI originally demanded access to) it would have established a precedent
> which the FBI would now be trying to leverage in further cases. So the
> decision by Lavabit to shut down rather than have a court battle was
> probably the only available way to avoid the creation of a harmful
> precedent.  Leaving the arena means you don't win, but it also means
> denying victory to your opponent.

I did fight things out in court. It just occurred in secret, and while a
proper description would require the use of vulgarity, I'll summarize by
saying I was railroaded. The length of time between when I received the
PRTT order, and when I was found in contempt (ex parte) was about 5
weeks. For comparison the _median_ time between filing and disposition
of a civil contempt charge in federal court during 2013 was 6 _months_.

The contempt charge levied a fine of 5K a day for each day I didn't turn
over the keys. So I shutdown the system and turned over the keys.
Technically I complied after being held in contempt, but because the
system was shut down, prevented them from using (or at least minimized
the damage).

After raising money from the public I appealed the contempt charge. As I
mentioned above the appellate court ignored the question and claimed I
waived my right to the appeal because we didn't object to both of the
legal ground cited in the contempt charge. What they ignore is that the
order they are referring to was issued on a Friday, which read for "the
reasons stated in the governments brief," and I was found in contempt
the following Monday ex parte. Because the government included an
argument in their brief that was never discussed in court, and ran
contrary to comments the judge made in session, the appellate court
claimed we never objected to it. That said, ask yourself, did we even
have time to understand what had just happened, and make the objection?
Even then, my lawyer asked to make an oral objection to the contempt
charge, but was denied. Presto facto, a non-appealable contempt charge
is created.

Like I stated in the beginning, the ruling on waiver means no precedent
was set.

> In much the same way the FBI now seeks a precedent in the Apple case,
> and in much the same way, compelling compliance with their order seems
> more important to them than the data from the single instance that the
> case is ostensibly about.

Definitely true. There are a number of ways, albeit difficult, which
would allow them to extract the data if meant that much to the
investigation. What the government wants is a public piece of case law
they can use in the future (in secret) to compel a number of even
nastier things from Apple. Like an OTA update to an encrypted device
which steals the key for your favorite messaging app right out of the
device's memory.

> So I have to wonder if an attempt to replace the precedent they planned
> to get from a Lavabit case, which they failed to obtain because it never
> came to court, may be part of what motivates the FBI in its battle over
> the Apple case.

What they sought from me, legally, wouldn't have allowed them to compel
what they want from Apple. But it might have applied to the WhatsApp
case (I haven't read those briefs yet). That said, both cases flow from
the same sense of entitlement. As in, we are entitled to everyone's
plain text data, and if Congress won't give it to us, we'll use our 27.1
billion dollar budget, and army of 100K lawyers to take it in court.
Your tax dollars at work.

> Of course if they had that Lavabit precedent, they would certainly be
> using it against Apple right now.

They tried. See my statement linked to in the intro.

L~

P.S. I haven't run this email by my lawyers, but I think everything I
said is unsealed and public already.


[1]:https://en.wikipedia.org/wiki/Lavabit
[2]:https://en.wikipedia.org/wiki/Edward_Snowden
[3]:https://en.wikipedia.org/wiki/FBI%E2%80%93Apple_encryption_dispute
[4]:http://www.metzdowd.com/pipermail/cryptography/2016-March/028669.html
[5]:https://www.facebook.com/KingLadar/posts/10156672763765038
[6]:http://lavabit.com/files/Lavabit.Amicus.Brief.20160303.pdf
[7]:https://cryptome.org/2016/03/usg-apple-102-105.pdf
[8]:https://www.facebook.com/KingLadar/posts/10156714933135038
