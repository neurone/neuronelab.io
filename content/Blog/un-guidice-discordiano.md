title: Un guidice discordiano
date: 2015-11-26 14:28
tags: racconti, fantascienza, società, libri, anarchia
summary: Sono i romanzi che hanno inventato la religione satirica del discordianismo. I protagonisti discordiani sono i nemici diretti degli *Illuminati*, che controllano il mondo irregimentandolo mediante banche, governi, mafie e burocrazia, in un ordine statico e sterile.

Tratto da [La mela d'oro][1], secondo libro della [trilogia degli illuminati][2]: serie di romanzi fanta-satirici scritti da [Robert Shea][3] e [Robert Anton Wilson][4].

Vedi anche [discordianismo][5].

Sono i romanzi che hanno inventato la religione satirica del discordianismo. I protagonisti discordiani sono i nemici diretti degli *Illuminati*, che controllano il mondo irregimentandolo mediante banche, governi, mafie e burocrazia, in un ordine statico e sterile.

Come antefatto al brano che riporto, al giudice protagonista era stata somministrata una droga che trasforma le persone da neofobe (che temono il nuovo) a neofile.

Caligula Bushman, conosciuto come il giudice più duro di Chicago, stava proprio in quei giorni processando sei persone accusate d’aver assaltato un ufficio di leva, distrutto tutta la mobilia, rovinato gli archivi e gettato una carriola piena di merda di vacca sul pavimento. A sorpresa Bushman interruppe il processo a metà della presentazione del caso da parte del pubblico ministero, con l’annuncio che stava per richiedere una perizia psichiatrica. Tra la confusione di tutti, pose quindi una serie di domande piuttosto strane al procuratore distrettuale Milo A. Flanagan:

“Che penserebbe di un uomo che non solo tenesse un arsenale nella propria casa, che che stesse raccogliendo a prezzo di enormi sacrifici finanziari un secondo arsenale per proteggere il primo? Che cosa direbbe se quest’uomo spaventasse così tanto i suoi vicini che anch’essi, a loro volta, iniziassero ad armarsi per proteggersi da lui? E se quest’uomo spendesse per le proprie armi dieci volte tanto quel che spende per l’educazione dei propri figli? Che se uno dei propri figli criticasse il suo hobby, chiamasse quel figlio traditore, vagabondo e lo diseredasse? E prendesse un altro figlio che lo obbedisce fedelmente e armasse quel figlio e lo mandasse in giro ad aggredire i vicini? Che direbbe di un uomo che introducesse veleni nell’acqua che beve e nell’aria che respira? E se quest’uomo non si limitasse a litigare con i vicini nel suo isolato, ma s’immischiasse anche nei litigi di altri, in parti lontane della città e addirittura in periferia? Un tipo così, signor Flanagan, sarebbe chiaramente uno schizofrenico paranoico con tendenze omicide. Questo è l’uomo che dovrebbe essere processato, sebbene nel nostro moderno, illuminato sistema di giurisprudenza, cercheremmo di curarlo e riabilitarlo, invece di punirlo solamente.

“Parlando come giudice,” continuò, “dichiaro il non luogo a procedere contro gli imputati per diversi motivi. Lo stato è clinicamente pazzo come entità organizzata ed è assolutamente incapace di arrestare, processare e incarcerare coloro i quali sono in disaccordo con la sua politica. Ma dubito che questo giudizio, seppur ovvio per ogni persona di buon senso, si possa inserire nelle regole del gioco giuridico americano. Decreto, quindi, che il diritto a distruggere proprietà governativa è protetto dal Primo emendamento della Costituzione degli Stati Uniti, e perciò che il crimine di cui queste persone sono accusate, per la Costituzione non è un crimine. La proprietà governativa appartiene a tutto il popolo, e il diritto di tutto il popolo a esprimere disaccordo con il proprio governo, anche distruggendo la proprietà governativa, è prezioso e non verrà prevaricato.”

Questa dottrina era improvvisamente piovuta sul giudice Bushman mentre questi stava arringando senza toga. Era meravigliato, ma aveva notato che, quel pomeriggio, la sua mente stava lavorando meglio e più velocemente.

Continuò: ” Lo Stato non esiste come esistono le persone e le cose, ma è una costruzione legale immaginaria. Una costruzione immaginaria è una forma di comunicazione. Qualsiasi cosa sia inclusa in una forma di comunicazione dev’essere quindi anch’essa una forma di comunicazione. Il governo è una mappa, e i documenti del governo sono una mappa della mappa. Il medium è, in questo caso, decisamente il messaggio, come ogni studioso di semantica ben sa. Inoltre, ogni atto fisico diretto contro una comunicazione, è esso stesso una comunicazione, una mappa della mappa della mappa. Quindi la distruzione di proprietà governativa è protetta dal Primo emendamento. Emetterò un’opinione scritta più ampia al riguardo, ma penso che, a questo punto, gli imputati non debbano soffrire ulteriori impedimenti alla loro libertà personale. Il caso è chiuso”.

[1]:http://www.shake.it/index.php?27&backPID=27&swords=la%20mela&productID=3&pid_product=23&detail=
[2]:https://en.wikipedia.org/wiki/The_Illuminatus!_Trilogy
[3]:https://en.wikipedia.org/wiki/Robert_Shea
[4]:https://en.wikipedia.org/wiki/Robert_Anton_Wilson
[5]:https://it.wikipedia.org/wiki/Discordianesimo