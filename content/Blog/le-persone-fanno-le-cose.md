title: Le persone fanno le cose (Non le aziende e non lo stato)
date: 2017-11-26 10:49
tags: società, politica, libertà, economia, software libero, software libero

Articolo apparso su [C4SS][1] l'otto marzo 2016, di _Kevin Carson_, traduzione di _Enrico Sanna_.

Su Facebook Doug Henwood, autore di _Wall Street_ e direttore del _Left Business Observer_, parlando di aziende del “settore pubblico” che fanno meglio del “settore privato”, ha citato ([3 marzo alle 10:48][2]) l’esempio di un accumulatore ad alta capacità sviluppato dall’agenzia statale Arpa-E. “E poi,” aggiunge, “ c’è la prima videocamera stabilizzata, finanziata da VC, che è merito del settore pubblico.” Il successo in questione sarebbe il “Santo Graal” in fatto di tecnologie d’accumulo elettrico, che potrebbe rendere l’energia solare e le auto elettriche più convenienti, oltre a trasformare la rete di distribuzione elettrica (Suzanne Goldenberg, “[US agency reaches ‘holy grail’ of battery storage sought by Elon Musk and Gates][3]”, The Guardian, marzo). Il succo del discorso è che “Arpa-E è all’avanguardia rispetto a Gates e Musk nella corsa multimiliardaria alla [batteria di nuova generazione][4] utile alla produzione in centrale e a casa.”

Qualche tempo fa Arthur Chu molto correttamente ha notato su Twitter che non è stato il “capitalismo” a fare l’iPhone e tante altre cose. Nel corso della storia, sotto qualunque ismo, a produrre ogni cosa è stato il lavoratore. L’ismo serve solo a stabilire chi prende i soldi. L’intervento era in risposta ad alcune persone di destra che sbeffeggiavano chi protestava con #ResistCapitalism sul loro iPhone. In realtà, l’iPhone non è stato creato dalla Apple. È stata la conoscenza diffusa e lo spirito di cooperazione sociale dei suoi dipendenti a crearlo. La Apple ha semplicemente applicato la “proprietà intellettuale” all’hardware e al software al fine di mantenere conoscenze tecnologiche e cooperazione entro le mura aziendali. Sostanzialmente, la Apple fa da gabelliere tra lavoratori informatici e designer aziendali da una parte, e i laboratori che producono l’hardware in Cina dall’altra, esigendo tributi da entrambi.

E se il “capitalismo” e le “aziende” non creano nulla, lo stesso vale per le agenzie governative. Henwood che esulta per il “settore pubblico” è l’immagine speculare di chi a destra esulta per la Apple. Entrambi sono senza senso.

Io non credo che “settore pubblico” e “settore privato” significhino davvero qualcosa a queste condizioni. In entrambi i casi, il lavoro è svolto da esseri umani in carne ed ossa tramite quello che l’antropologo David Graeber, in _Debt_, chiama “comunismo quotidiano”, ovvero tramite la cooperazione e la condivisione delle informazioni. Tutte le conquiste attribuite a sistemi gerarchici istituzionali, che siano aziendali o statali, sono in realtà opera di gruppi paritetici formati da persone che cooperano tra loro, e che tengono in piedi l’attività nonostante le interferenze dall’alto e le scelte irrazionali dei manager ai vertici della gerarchia. La cosa più importante non è capire qual’è il sistema gerarchico che si intromette meno, ma eliminare il sistema gerarchico tout court.

Se talvolta le istituzioni gerarchiche statali o aziendali sono servite, è stato quando potevano essere giustificate dalle dimensioni degli investimenti e degli impianti da amministrare, o dai costi di transazione generati da operazioni di vasta scala che coinvolgevano molte migliaia di persone. Entrambe queste giustificazioni sono, o stanno diventando rapidamente, obsolete. Per quanto riguarda il design, assente la “proprietà intellettuale” della Apple, tutto ciò che si fa all’interno dell’azienda potrebbe essere fatto a livello paritario da comunità che producono software e hardware open-source. Quanto alla produzione, la Apple non fa che affidare la produzione manifatturiera di ciò che è stato progettato dai suoi dipendenti ad imprese indipendenti dall’altra parte dell’oceano.

Grazie al possesso della “proprietà intellettuale”, la Apple riesce a bloccare le relazioni orizzontali tra progettisti e produttori, e scremare un profitto. Può usare il suo diritto monopolistico di disporre del prodotto per vendere computer e smartphone, fatti interamente da altri, con un margine molto, molto al di sopra dei costi di produzione.

La Apple, l’azienda, è un parassita. Tutto quello che fa potrebbe essere fatto da produttori paritari che distribuiscano gratuitamente schemi hardware e software a chiunque voglia servirsene, con laboratori indipendenti, gestiti dagli stessi lavoratori, che producono l’hardware ovunque nel mondo ci sia richiesta.

Similmente, ci sono persone ignoranti che esaltano la creazione della dorsale di internet da parte dell’Arpa, citandola come un trionfo del “socialismo”, come se ogni attività posseduta o diretta dallo stato debba essere celebrata come esempio di “socialismo”. Certo è socialismo, ma non del genere che intendono loro. Le persone che hanno sviluppato Arpanet e internet condividevano una cultura interna, dal carattere cooperativo e paritario, fondamentalmente in contrasto con la mentalità dei burocrati del Pentagono per cui teoricamente lavoravano. Sono state le loro relazioni umane orizzontali, il loro “comunismo quotidiano”, a fare internet, e a porre le basi dell’ethos del World Wide Web.

Sbagliano entrambi, chi a destra prende in giro l’hashtag #ResistCapitalism e chi come Henwood si entusiasma per il “settore pubblico”, e sbagliano per ragioni molto simili. Bisogna riconoscere il ruolo centrale che ha la libertà e la capacità d’agire dell’uomo nell’edificare un mondo migliore, gettando nella spazzatura il nesso stato-aziende con tutte le loro istituzioni sfruttatrici.

Citazioni:

* Kevin Carson, [People make things: Not corporations, Not government][5], Augusta Free Press, 19-3-2016
* Kevin Carson, [People Make Things — Not Corporations, Not Government][6], P2P Foundation Blog, 24-3-2016
* Kevin Carson, [People Make Things — Not Corporations, Not Government][7], NewsLI, 21-3-2016


[1]:https://c4ss.org/content/50190
[2]:https://www.facebook.com/doug.henwood/posts/10153453333071475
[3]:http://www.theguardian.com/environment/2016/mar/03/us-agency-says-has-beaten-elon-musk-gates-to-holy-grail-battery-storage
[4]:http://www.bloomberg.com/news/articles/2015-04-14/gates-pritzkers-take-on-musk-in-5-billion-race-for-new-battery
[5]:http://augustafreepress.com/people-make-things-not-corporations-not-government/
[6]:https://blog.p2pfoundation.net/people-make-things-not-corporations-not-government/2016/03/24
[7]:https://www.newsli.com/2016/03/21/op-ed-people-make-things-not-corporations-not-government/
