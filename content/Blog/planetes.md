title: Planetes
date: 2014-04-14 18:58:40 +0200
tags: fantascienza, recensioni, anime
summary: Nel 2075 l’industria spaziale sarà molto sviluppata. Una fonte di energia, chiamata Elio-3 (He3) verrà usata come carburante al posto degli attuali derivati del petrolio. Questo gas viene principalmente estratto dal terreno lunare, sul quale è nata anche la prima città, chiamata Città del Mare della Tranquillità.

Nel 2075 l’industria spaziale sarà molto sviluppata. Una fonte di energia, chiamata Elio-3 (He3) verrà usata come carburante al posto degli attuali derivati del petrolio. Questo gas viene principalmente estratto dal terreno lunare, sul quale è nata anche la prima città, chiamata Città del Mare della Tranquillità.

La reazione fra [He3][1] e [deuterio][2] è ciò che produce la maggior
parte dell’energia necessaria ai viaggi spaziali. Buona parte dei voli
trans-oceanici avvengono al di sopra dell’atmosfera terrestre. Tutta questa
innovazione tuttavia, deve scontrarsi con un problema molto vecchio: i
[detriti][3] lasciati nello spazio. Questo problema, risollevato di
[recente][4], è aumentato notevolmente di proporzione nel 2075, tanto da
trasformasi in un pericolo per l’incolumità dei civili che vengono trasportati e
di chi nello spazio ci lavora. Un detrito grande anche solo come una vite,
viaggiando può scontrarsi a velocità relativa molto elevata con un aereo
spaziale, causando una falla nello scafo che ne produca la de-pressurizzazione e
la conseguente morte dell’equipaggio.
<!--more-->
![Planetes 1]({filename}/images/planetes1.jpg "Planetes 1")

Per questo, esistono i raccoglitori di detriti, di cui fanno parte i
protagonisti di questa serie animata.

La caratteristica che salta immediatamente all’occhio è l’accuratezza con cui
viene rappresentato lo spazio e la fisica, il comportamento in assenza di
gravità e di aria, ma anche *il rapporto fra l’uomo e lo spazio*, i rischi che
esso corre, sia fisici che psicologici. Nelle varie puntate della serie,
verranno affrontati molti argomenti di attualità, come l’ecologia, il
terrorismo, lo sfruttamento dei paesi poveri da parte delle nazioni ricche e la
guerra.

Lo spazio è molto insidioso, pur apparendo vuoto. Il vuoto stesso è un pericolo:
non essendoci punti di riferimento vicini, è molto semplice perdersi,
allontanarsi troppo e trovarsi a vagare naufrago nello spazio, fino a che l’aria
delle bombole non finisce. Un astronauta che si trova in una situazione del
genere, anche se viene recuperato prima che la mancanza di ossigeno inizi a
[danneggiare il tessuto nervoso][5] e successivamente lo uccida per
soffocamento, subisce un fortissimo trauma psicologico, che nell'anime viene
chiamato “sindrome da smarrimento spaziale”. Anche la velocità non va
considerata come si fa sulla terra. Quando si è sulla terra, si da per scontato
che la velocità sia relativa al terreno, tanto da non specificarlo mai. Nello
spazio la velocità è relativa ad ogni oggetto che si incontra. Potresti trovarti
ad una velocità relativa di qualche centinaio di Km/h fra te ed un detrito e
rendertene conto solo quanto lo stai per colpire. Le radiazioni sono presenti in
quantità estremamente maggiori che non sulla Terra (protetta dallo strato di
atmosfera), quindi malattie come la leucemia sono molto comuni fra gli
astronauti. Veniamo a conoscenza di nozioni interessanti: che chi nasce e vive
sulla luna, tende a crescere di statura molto più di un terrestre, ma la sua
struttura ossea non gli permette di sopportare la gravità della terra.
Queste persone vengono chiamate “seleniti”.

Tutte queste cose vengono accuratamente tenute in considerazione dalla trama di
Planetes, ma gli sceneggiatori sono stati dei maestri nel renderne
l’assimilazione assolutamente leggera da parte dell’ spettatore, lasciando molto
spazio ai personaggi, ottimamente caratterizzati.

Nelle prime puntate sembra di essere in un covo di matti, in una specie di
commedia ambientata nello spazio, fra spazzini in tuta d’astronauta uno più
fuori di testa dell’altro. In questo modo si riesce a conoscerli ad uno a ad
uno: *Ai Tanabe*, nuova assunta, idealista e testarda, *Hachirota Hoshino*,
detto *Hachimaki* per via della [benda da kamikaze][6] che porta sempre in
testa, la controparte e mentore di Tanabe, molto ambizioso, spesso cinico e
disilluso, ma non senza sogni nel cassetto, i due capi, che non chiedono altro
che fare festa tutti i giorni e tenersi lontani dai guai. *Fee*, la pilota della
loro navetta, tabagista accanita: una sfortuna nello spazio, dato che l’aria è
un bene vitale e fumare è vietato tranne che in apposite zone adibite allo
scopo, per questioni di sicurezza e di preservazione delle risorse. Man mano che
la storia prosegue, lo spettatore farà la conoscenza di molti altri personaggi,
tutti ben definiti e con un ruolo ben preciso nella complessa trama di questo
anime.

Dopo poche puntate veniamo a conoscenza di un gruppo terroristico chiamato
*“fronte per la difesa spaziale”*. Inizialmente lo conosciamo per via di alcuni
attentati sulla stazione spaziale che fa da base alla *Technora*, la ditta per
la quale lavora la Sezione Detriti dei nostri protagonisti. In seguito, le
ragioni della loro esistenza saranno più chiare. Scopriremo che *tutto questo
avanzamento tecnologico non ha portato benefici a tutte le nazioni del mondo,
come viene fatto credere, ma ha allargato ancora di più il divario fra i paesi
ricchi e quelli del terzo mondo*, che non hanno le risorse per competere nella
corsa allo spazio e di conseguenza non possono avvantaggiarsi delle risorse
ricavate dallo spazio, l’Elio-3 ad esempio, necessario in seguito agli
esaurimenti delle scorte petrolifere. In mezzo a tutto questo c’è la INTO,
l’equivalente dell’attuale ONU. Quasi subito scopriamo che dietro la loro
facciata imparziale e di preservatori della pace, c’è molta ipocrisia. Oltre a
tutto questo, c’è in preparazione una ambiziosissima missione di esplorazione di
Giove che è, tra l’altro, una grandissima fonte di Elio-3.

Le carte in gioco sono tante, ma vengono mostrate poco alla volta, con saggia
maestria e non prevaricando mai la parte “umana” dell’anime, che racconta delle
relazioni fra i protagonisti ed il loro evolversi. Non mancheranno neppure le
riflessioni sul *senso della vita, l’universo e tutto quanto!*

![Planetes 2]({filename}/images/planetes2.jpg "Planetes 2")

Gli episodi sono ventisei e tutta la storia si può dividere in due parti.
Inizialmente impariamo a conoscere i protagonisti e la realtà di quel periodo,
quando i tempi diventano sufficientemente maturi, inizierà la seconda parte,
molto più drammatica e meno scanzonata della prima, ma decisamente intrigante.
Le ultime puntate sono talmente coinvolgenti (e piene di bastardissimi
[cliffhanger][7]), da obbligarti a guardarle una in fila all’altra. Quelle
puntate ve le ricorderete per tutta la vita! Non mancano neppure le parti
commoventi, che riescono a non essere mai stucchevoli perché non nascono da
stereotipi, ma da dei personaggi che abbiamo imparato a conoscere e di cui
quindi comprendiamo bene il motivo di ciò che fanno e cosa significa per loro.
Anche la colonna sonora è bellissima e sempre adatta alle scene di cui fa da
sottofondo.

Mi sembra superfluo aggiungere che si tratta di una serie animata da non perdere
e che si lascia guardare con piacere anche molte volte. Dopo averlo visto, non
si può non rimanere affascinati dallo spazio. È un anime che ti fa amare lo
spazio, con dei temi sociali e politici affrontati in modo estremamente
intelligente e con grande sensibilità.

[1]: https://it.wikipedia.org/wiki/Elio-3
[2]: https://it.wikipedia.org/wiki/Deuterio
[3]: https://it.wikipedia.org/wiki/Detrito_spaziale
[4]: http://www.ecoblog.it/post/34003/rifiuti-lesa-fara-il-netturbino-spaziale
[5]: https://it.wikipedia.org/wiki/Asfissia
[6]: https://en.wikipedia.org/wiki/Hachimaki
[7]: https://it.wikipedia.org/wiki/Cliffhanger_%28narrativa%29
