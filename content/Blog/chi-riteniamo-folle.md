title:  Chi riteniamo folle?
date:   2015-08-04 18:33
tags:   psicologia
summary: Prendendo spunto da un post sul blog Some1elsenotme, vorrei cercare di capire in che modo percepiamo la follia. Cioè, quali caratteristiche dovrebbe avere un comportamento, perché, da noi, sia ritenuto folle.

Prendendo spunto da un [post sul blog Some1elsenotme][1], vorrei cercare di capire in che modo percepiamo la follia. Cioè, quali caratteristiche dovrebbe avere un comportamento, perché, da noi, sia ritenuto folle.

Penso che per comprendere cosa sia la follia, sia necessario anche capire in che modo noi, presunti savi, la percepiamo. Questo è utile a metterci al riparo da illusioni e dal rimanere imbrigliati in idee preconcette.

Dato che nel post parlerò di congruenza fra l'interpretazione della realtà da
parte del soggetto con quella comunemente accettata, devo prima spiegare cosa
intento con realtà. Avviso che all'interno dell'articolo c'è uno [spoiler][2]
sul film [Memento][3] di Christopher Nolan.

La nostra rappresentazione di *realtà* è un concetto in continuo divenire, che
varia a seconda della cultura del momento. Siamo noi, con i nostri strumenti e
le nostre conoscenze a dare una forma ed una definizione alla realtà.
Successivamente, consideriamo la forma ottenuta, come perfettamente isomorfa con
la realtà oggettiva. Facendo questo, dimentichiamo che nel corso della nostra
storia l'evoluzione della cultura e degli strumenti a disposizione ha cambiato
continuamente la rappresentazione della realtà. Trascuriamo che siamo degli
animali umani, esseri viventi organici, nati in precise condizioni e luoghi e
con una struttura biologica simile e quindi "menti simili". A causa di questo,
abbiamo enormi difficoltà nell'accorgerci di eventuali *vizi sistemici* presenti
all'interno dei nostri schemi mentali comunemente accettati. Questa difficoltà
ci rende estremamente difficile comprendere cosa possa essere oggettivo.

Il post di Gialnuca, commentando la condizione [dell'assassino di Oslo][4],
dice: *"…un modello della realtà che non è congruente con quello che è il mondo
là fuori al giorno d'oggi. Talvolta tale discrepanza può essere vissuta in
maniera dolorosa. Il rendersi conto della distanza tra l'ideologia e la vera
realtà - due concetti che non finiranno mai per coincidere - può nelle menti più
deboli suscitare infatti una frustrazione tale da spingere ai gesti più estremi
e pericolosi."*

Questo sembra anche descrivere quello che percepiamo nelle persone che riteniamo
folli. Consideriamo folle una persona la cui rappresentazione della realtà
differisce da quella della maggior parte delle altre persone: quella comunemente
accettata in un preciso luogo e tempo. La discrepanza descritta, spesso porta il
soggetto ad agire in modo da riallineare le due rappresentazioni della realtà,
interna ed esterna. È utile paragonare questo con quanto descritto dalla [teoria
sociale cognitiva][5] di *Albert Bandura*. Durante questi tentativi, notiamo che
nel soggetto c'è qualcosa di anomalo e le sue azioni ci appaiono inspiegabili ed
*imprevedibili*.

L'imprevedibilità che percepiamo, è dovuta dall'abitudine nostra di vedere
nell'altro il riflesso dei nostri stessi schemi mentali, che consideriamo
normali. Quando questo è disatteso, ci sorprendiamo e giudichiamo la persona
"non normale", probabilmente folle.

Persone con convinzioni simili a quelle del killer, in tempi passati, non
sarebbero state considerate folli. Ma la "sanità mentale" percepita, sembra
evolversi in modo quasi *darwiniano*, di pari passo con la cultura e quindi con
la percezione della realtà. Chi non ha una rappresentazione adatta al qui ed
ora, viene considerato folle. La pericolosità che percepiamo nel folle, nasce
alla nostra incapacità di poter prevedere le sue azioni ed i suoi pensieri,
perché essi poggiano su rappresentazioni molto diverse da quelle che fanno da
base ai nostri. Abbiamo difficoltà ad anticiparlo, a prevederlo, quindi lo
riteniamo pericoloso. Ed infatti non c'è nulla che gli impedisca di diventarlo,
perché tutta la società è costruita per elaborare e prevedere schemi diversi dai
suoi.

Vorrei fare un paio di altri esempi. Quando *Pitagora* dice all'uomo che sta
picchiando un cane randagio di smetterla, perché ha riconosciuto gli occhi del
cane come quelli di un vecchio amico, egli forse non viene considerato folle.
Attualmente, contestualizzandolo, non lo consideriamo folle neppure noi. Perché
conosciamo la sua filosofia e sappiamo capire la sua rappresentazione mentale
della realtà e prevederla. In modo simile, non consideriamo folle una persona
che per convinzioni filosofiche o religiose, crede nella reincarnazione.
Esattamente come Pitagora. Eppure, se qualche sconosciuto dovesse esclamare una
cosa simile, come minimo farebbe alzare un buon numero di sopracciglia. In modo
simile, quando una persona ha esperienze [sinestetiche][6], non viene
considerato un visionario, perché le scienze cognitive, voce autorevole e
comunemente accettata, danno una spiegazione soddisfacente del fenomeno. Ma un
centinaio di anni fa, agli albori della psicologia moderna, ciò non sarebbe
accaduto. La cultura di allora non aveva strumenti per comprendere e fornire uno
schema in grado di fare previsioni su quel fenomeno, quindi chi affermasse di
vedere suoni colorati, o che certe forme gli sembrano avere un dato sapore,
sarebbe apparso come una persona malata.

Anche se non è esattamente lo stesso caso, penso a [Clive Wearing][7] ed al
protagonista del film *Memento*. Nel primo, la lesione che ha subito, gli
impedisce di memorizzare nuovi ricordi, ma ciò influisce in modo marginale sulla
sua rappresentazione della realtà e quindi le sue azioni sono prevedibili. Il
secondo ha un male che gli impedisce di memorizzare ricordi e di ricordarsi
parti del suo passato e questo lo spinge ad avere una rappresentazione distorta
della realtà. Agisce in modo da riallineare la rappresentazione interna della
realtà con quella percepita e le sue azioni sono di difficile comprensione e
prevedibilità, sia per lo spettatore che per l'investigatore che cerca di
trovarlo. Consideriamo entrambi dei soggetti malati, ma solo il secondo ci
appare folle.

Un povero malato di [Alzheimer][8] non viene comunemente considerato folle,
eppure la sua rappresentazione della realtà, a causa delle disfunzioni della
memoria, differisce dalla norma. Verrebbe considerato folle solo dal momento in
cui le sue azioni iniziassero a diventare imprevedibili. Quest'impressione
acquisisce maggiore forza quando la sua imprevedibilità diventa pericolosa.
Volendo spingersi oltre, si potrebbe osservare che, talvolta, quando vengono
messe in atto da persone "savie" delle strategie per poter gestire ciò che viene
considerato folle, esse appaiono folli a tutti coloro che non si sentivano
minacciati dalla altrui percepita follia, ma che al contrario si sentono
minacciati da queste contromisure. Infatti anch'esse si baserebbero su schemi
non comuni, spesso oscuri ed imprevedibili, ed apparirebbero pericolose. Guerre
preventive, controlli a tappeto su tutta la popolazione, panopticon globale...

Fra le cose su cui su basa la rappresentazione della realtà della maggior parte
delle persone, vi sono misurazioni e teorie che vengono comunemente considerate
oggettive, come quelle scientifiche, oppure provenienti da fonti considerate
autorevoli. In passato, più di ora, i testi sacri erano considerai documenti
oggettivamente autorevoli. Questo dovrebbe mettere ancora più in evidenza quanto
la percezione di sanità, dipenda dalla cultura del luogo e del momento.

Ironicamente, talvolta, diamo del folle anche ad un genio musicale, ad un
artista che rielabora la realtà restituendola in modo notevolmente inatteso, in
un modo che la maggior parte delle persone non sarebbe stata in grado di
immaginare e quindi prevedere.

[1]:https://some1elsenotme.wordpress.com/2011/07/24/il-baco-e-la-formica/
[2]:https://it.wikipedia.org/wiki/Spoiler_(cinema)
[3]:https://it.wikipedia.org/wiki/Memento
[4]:https://it.wikipedia.org/wiki/Anders_Breivik
[5]:https://it.wikipedia.org/wiki/Teoria_sociale_cognitiva
[6]:https://it.wikipedia.org/wiki/Sinestesia_(psicologia)
[7]:https://en.wikipedia.org/wiki/Clive_Wearing
[8]:https://it.wikipedia.org/wiki/Malattia_di_Alzheimer
