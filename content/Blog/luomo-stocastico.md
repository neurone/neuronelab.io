title: L'uomo stocastico
date: 2013-12-01 14:16
tags: libri, fantascienza, filosofia, recensioni
summary: Romanzo fantapolitico di Robert Silverberg, molto curato in ogni aspetto: della psicologia dei personaggi all’ambientazione, al meccanismo che fa muovere tutta la storia.

Romanzo fantapolitico di [Robert Silverberg][1], molto curato in ogni aspetto: della psicologia dei personaggi all’ambientazione, al meccanismo che fa muovere tutta la storia.

*Lew Nichols* basa le proprie previsioni sulla raccolta di dati e l’elaborazione
di scenari più o meno probabili ed è ancorato all’idea di poter modificare il
corso degli eventi.  
*Carvajal* ha visioni del futuro e la sua esperienza gli ha insegnato che ciò
che vede, *è*. Osservare il futuro equivale a scavare nei ricordi del passato,
deve solo sapere cosa cercare, avere uno *spunto*. Non c’è scampo al futuro:
tutto funziona deterministicamente. Qualsiasi tentativo di modificare l’esito
degli eventi risulta inefficace. È rassegnato ad essere una semplice pedina
limitata ad osservare e riferire ciò che inevitabilmente avverrà.  
La storia è ambientata nel 1999, in un futuro decisamente inquieto, in fase di
cambiamento. Lew e Carvajal vengono ingaggiati dallo staff del candidato
politico *Paul Quinn*, per rendere più efficace la campagna elettorale ed
prevedere il futuro in modo da operare le scelte più convenienti.

![L'uomo stocastico]({filename}/images/uomo_stocastico.jpg){: style="float:right"}
*L'uomo stocastico* è un romanzo estremamente godibile ed in grado di stimolare
moltissimi pensieri, sia filosofici che politici. Lo lessi nel periodo della
campagna elettorale statunitense, che portò alla presidenza [Barak Obama][2], la
cui vittoria era estremamente prevedibile. Non potei evitare di seguirla con un
senso di inquietudine, causato dalla lettura del romanzo e dalle diverse
analogie che riscontravo nella realtà.

> Stocastico: voce dotta, dal greco *stochastikos*, congetturale, dovuto al
> caso, aleatorio. Questo dice il dizionario. Ma Robert Silverberg dice di più.
> Dice che uno specialista di indagini conoscitive e di statistiche
> previsionali, un professionista della congettura, un mago del calcolo delle
> probabilità, può tutto a un tratto scoprire la vera natura del suo talento. E
> questo talento non ha niente a che fare con la scienza dei numeri, col buon
> senso, con il fiuto commerciale e politico. E’ un dono naturale che, coltivato
> opportunamente, permette all’uomo stocastico di “vedere” come in una sfera di
> cristallo, il futuro. Chi vincerà la terza corsa all’ippodromo? Chi sarà il
> prossimo presidente degli Stati Uniti? Come e quando arriverà la nostra morte?
> Mai come in questo romanzo l’antico sogno dell’umanità è stato presentato con
> tanta acutezza psicologica, con un così vivo senso di ciò che potrebbe essere,
> in concreto, la vita di un autentico veggente.

Il migliore dei mondi possibili?
--------------------------------

[Philip Kindred Dick][3] diceva: “Se non vi piace questo mondo dovreste vederne
qualcuno degli altri.”

Si può interpretare questa idea da un punto di vista evoluzionistico, in cui
consideriamo la natura come un sistema dinamico nel quale le interferenze
reciproche dei singoli elementi concorrono a far emergere un equilibrio che
porta ad un continuo perfezionamento dei vari sottosistemi.  

Questo non solo è il migliore dei mondi possibili, ma è anche l’unica realtà
possibile. Tutto ciò che è accaduto, lo ha fatto portandoci inevitabilmente al
punto dove siamo ora. Non per via di un disegno intelligente deciso a priori, ma
solo per le capacità auto-organizzanti di tutto il sistema.

Se prendessimo in esame un ipotetico evento *A* già accaduto, potendo esaminare
tutte le interazioni che vi sono state, valutandole statisticamente, e
guardassimo a ritroso tutto ciò che ha concorso al suo accadere, ci renderemmo
conto che la probabilità che sarebbe accaduto, si avvicinerebbe a 1 (100%) mano
a mano che procedono nel tempo le varie interazioni dei numerosi fattori. Cioè,
*A* accadde perché la sua probabilità, in quel preciso momento era 1, mentre
quelle degli ipotetici eventi *B, C, D (…)*, era inferiore.

[1]:https://it.wikipedia.org/wiki/Robert_Silverberg
[2]:https://it.wikipedia.org/wiki/Barack_Obama
[3]:https://it.wikipedia.org/wiki/Philip_Dick
