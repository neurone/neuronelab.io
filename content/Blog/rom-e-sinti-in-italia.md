title: Rom e Sinti in Italia - Tra stereotipi e diritti negati
date: 2015-09-21 23:19
tags: società, politica, etica, antropologia, rom e sinti
summary: Bel libro, scritto a più mani, che tratta di un argomento sul quale c'è molta ignoranza e poco desiderio di conoscere. Pochissimi conoscono la storia dei popoli Rom e Sinti, quello che hanno passato e che sono costretti a passare anche ora.

Bel [libro][1], scritto a più mani, che tratta di un argomento sul quale c'è molta ignoranza e poco desiderio di conoscere. Pochissimi conoscono la storia dei popoli Rom e Sinti, quello che hanno passato e che sono costretti a passare anche ora.

Nonostante questo, chiunque ha un'opinione su di loro. È un libro
importante, anche perché attuale, scritto nel 2009, dopo le varie leggi
sull'immigrazione, i "pacchetti sicurezza", e varie campagne elettoarli che
facevano leva sulla paura delle persone per gli stranieri (per quanto i rom ed
i sinti, in buona parte, abbiano la cittadinanza italiana, nonostante non
vengano trattati allo stesso modo degli altri cittadini italiani).

![Rom e Sinti in Italia]({filename}/images/rom-e-sinti.jpg "Copertina: Rom e
Sinti in Italia"){: style="float:right"}
Il libro riporta ciò che si è potuto risalire sulla storia di questi popoli, nei
documenti degli stati con cui sono entrati in contatto (la tradizione rom e
sinta è sostanzialmente orale, quindi, molta della loro storia passata viene
persa fra le generazioni). In un paio di capitoli, che purtoppo, non essendo
pratico di diritto, ho trovato abbastanza pesanti, si esaminano le varie leggi
che coinvolgono stranieri. Nei capitoli successivi viene raccontato e spiegato
lo sterminio scientifico, operato dal regime nazista e da quello fascista
italiano, nei confronti dei popoli rom e sinti. Un genocidio che si svolse in
parallelo a quello ben più noto degli ebrei, ma meno conosciuto... forse perché,
come si afferma, "degli zingari, dopotutto, non importa nulla a nessuno".
Questo argomento viene trattato anche in un documentario su due DVD, intitolato
["A forza di essere vento"]({filename}a-forza-di-essere-vento.md) e pubblicato
dalla Editrice A.

Si prosegue riportando testimonianze di successo di integrazioni fra le culture
in alcuni comuni e città italiane e continua con un bel capitolo, scritto da
Djana Pavlovic, attrice, attivista rom e mediatrice culturale, per concludersi
con il racconto dell'esperienza estremamente positiva della scuola elementare
di Monserrato.

Penso che la lettura di un libro simile sia fondamentale per avere almeno una
minima idea di ciò di cui si parla, quando si tratta di rom e sinti.

Il libro è pubblicato dall'editrice [Ediesse][1].

> Nell'ultimo anno è esplosa, in Italia, una vera e propria «questione Rom».
> Nel passato ha riguardato prevalentemente aspetti socio-culturali, a volte
> causa di conflitto con le popolazioni indigene che non gradiscono la
> vicinanza degli insediamenti di Rom e Sinti. A partire dal 2008, il
> fenomeno ha assunto particolari caratteri, per l'approvazione di una vera e
> propria legislazione speciale per questa categoria di persone, spesso
> cittadini italiani, ai quali, in luogo del diritto comune, si applicano
> norme del tutto peculiari in materia di residenza e di controlli, con la
> possibilità di sottoporre anche i minori a forme di identificazione
> mediante il rilascio delle impronte digitali. Diverse amministrazioni,
> infine, negano ai Rom l'accesso ai servizi e ai benefici previsti per tutti
> i cittadini. Il volume traccia un quadro d'insieme del fenomeno, a partire
> dai presupposti culturali, e approfondisce, sul piano dei diritti, la
> posizione di Rom e Sinti in riferimento alla Costituzione italiana e alla
> copiosa normativa comunitaria volta a proteggere questa etnia. Gli autori
> sono in prevalenza ricercatori che collaborano con università italiane,
> alcuni di etnia rom, a dimostrazione che anche in Italia questo popolo
> incomincia a riflettere sulla propria storia e sulle proprie condizioni di
> vita. Il volume è curato da Gianni Loy, ordinario di Diritto del lavoro
> nell'Università di Cagliari e autore di saggi in materia di diritto
> antidiscriminatorio, e Roberto Cherchi, ricercatore di Diritto
> costituzionale nella stessa università.  Contributi di: Massimo Aresu, Luca
> Bravi, Roberto Cherchi, Paolo Finzi, Gianni Loy, Ester Mura, Djana
> Pavlovic, Eva Rizzin, Ilenia Ruggiu, Tommaso Vitale.

[1]:http://www.ediesseonline.it/catalogo/saggi/rom-e-sinti-italia
