title: I love hacking - Il meglio della rivista "2600
date: 2014-12-30 13:57:32 +0100
tags: libri, recensioni, rete, sicurezza, tecnologia, hacking, cyberpunk
summary: *I <3 hacking*, pubblicato da Shake Edizioni è una raccolta di articoli della storica rivista hacker "2600", che vanno dal 1984 al 2008, selezionati dal suo editore *Emmanuel Goldstein*. Il titolo originale del libro è *The Best of 2600:A Hacker Odyssey*.

*I <3 hacking*, pubblicato da [Shake Edizioni][1] è una raccolta di articoli della storica rivista hacker "2600", che vanno dal 1984 al 2008, selezionati dal suo editore *Emmanuel Goldstein*. Il titolo originale del libro è *The Best of 2600:A Hacker Odyssey*.

Ragionare di tecnologia, informatica e telecomunicazioni senza considerare il
punto di vista hacker, è come ragionare di politica e sociologia, escludendo il
pensiero anarchico: il risultato non può essere che incompleto, parziale, monco.
L'importanza storica è quindi notevole, in quanto descrive un periodo di rapida
espansione e diffusione dei mezzi telematici ed informatici, dal punto di vista
di chi questi mezzi ha contribuito a plasmare sia tecnicamente che
nell'immaginario collettivo.

La selezione degli articoli non è completamente cronologica, ma è suddivisa per
argomenti che spaziano dall'hacking delle reti telefoniche della *Bell*, per poi
passare al nascere delle aziende concorrenti, le BBS, la telefonia cellulare, i
personal computer, [ARPANET][2], la nascita di internet, l'hacking low-tech,
l'hacking in ambiente militare, le radio, la crittografia e lo spionaggio dei
servizi segreti a danno dei cittadini, Google, Paypal ed una grossa sezione
riguardo ai rapporti fra gli hackers e le leggi, nella quale vengono seguiti i
famosi casi di [Kevin Mitnick][3] e del collaboratore di *2600* [Bernie S.][4].

La qualità dell'adattamento italiano, almeno nell'edizione in mio possesso è,
talvolta, incostante. Sono presenti diversi refusi ed in almeno un articolo
mancano delle illustrazioni che il testo sembra intendere dover esserci. Alcuni
termini tecnici vengono, talvolta, tradotti alla lettera, indice che chi ha
curato la traduzione, non era a conoscenza di quell'argomento. Fortunatamente,
queste magagne sono estremamente rare e non compromettono la godibilità del
testo.

Personalmente, ne consiglio la lettura. È un testo molto divertente ed
appassionante e, a mio avviso, d'importanza storica notevole.

*"2600 è stata (ed è) la rivista più importante nella storia dell'hacking. Per
lungo tempo il Nemico pubblico numero 1 dei servizi segreti di tutto il mondo. E
mito di migliaia di giovani smanettoni e idealisti.*

*"Gli hacker, nella loro ingenuità idealistica, rivelano sempre le cose che
hanno scoperto, senza riguardo ai soldi, i segreti delle aziende, o le
cospirazioni del governo. Noi non abbiamo nulla da nascondere, e questa è la
ragione per cui siamo relativamente aperti nelle nostre faccende. [...] Ma il
fatto che non siamo disposti a giocare al gioco dei segreti, ci trasforma in una
minaccia tremenda agli occhi di coloro che vogliono tenere le cose importanti
fuori dalla portata del pubblico. Ormai ci sono molte persone normali che
condividono i valori degli hacker, cioè la libertà di parola, il potere
dell'individuo davanti allo stato o alle corporation [...] perché la difesa
dell'individuo è il vero punto importante."* Emmanuel Goldstein

[1]:http://shake.it/
[2]:https://en.wikipedia.org/wiki/Arpanet
[3]:https://en.wikipedia.org/wiki/Kevin_mitnick
[4]:https://en.wikipedia.org/wiki/Bernie_S
