title: L'incoerenza e la precarietà del Transumanismo Non-Anarchico
date: 2016-08-13 8:00
tags: anarchia, etica, politica, tecnologia, transumanesimo

_Articolo originale su [Institute for Ethics & Emerging Technologies][1]. Tradotto e rilasciato in pubblico dominio col permesso dell'autore._

Più aumentano i mezzi con cui le persone possono agire, più semplice diventa l'attacco e più difficile la difesa.

È una semplice questione di complessità. L'attaccante deve solo scegliere una linea di attacco, il difensore deve mettersi al sicuro da tutte. Questo non vale solo per i piccoli condotti di scarico termico (rif. Guerre Stellari), è anche vero nell'odierno ecosistema del software ed in molti altri sistemi in cui vi sono diverse dimensioni di movimento.

Sia complessità che più gradi di movimento all'interno del sistema, espongono una maggiore superficie di attacco. Per questo motivo, gli attacchi possono arrivare, non solo da tutte le direzioni cardinali, ma anche da sopra e da sotto.

L'arco della storia umana è un arco flesso dalla nostra creatività, dalla ricerca verso nuove opzioni e più modi di esistere ed agire. Verso una maggiore libertà. Ogni invenzione umana espande nell'immediato la quantità di modi di agire di cui disponiamo.

_(questo è stato pronunciato come discorso il 18 ottobre 2015 ad Oakland, California, alla conferenza "Future of Politics" co-sponsorizzata dal IEET)_

Ed intrecciata a questa libertà è senza dubbio arrivata una grande capacità distruttiva. Dall'era in cui solo un'elite poteva essere guerriera, quando l'attacco era l'ambito di pochi scelti, ad un era in cui chiunque poteva portare una lancia o una spada ed uccidere, forse, un'altra persona prima di morire, all'era del moschetto e dell'arma automatica.

Oggi, ognuno di noi porta con sé delle piccole granate nelle proprie borse e tasche. Un accidentale effetto secondario dell'immagazzinare energia per i nostri telefoni e portatili.

Domani lo hobbista con una stampante di RNA nel proprio garage a Seattle sarà in grado di scaricare o mettere assieme un EbolaSARSvirusmortale con una tale apocalittica virulenza che non avrebbe mai potuto evolversi naturalmente. Questo pericolo non è un posto da una singola tecnologia, ma è innato nello stesso arco dello sviluppo tecnologico. Nel momento in cui i nostri strumenti espandono la nostra libertà fisica, essi ci obbligano a cambiamenti nella nostra libertà sociale.

Mentre avanzavamo nel nostro accelerato sviluppo tecnologico, cioè quando le conoscenze che acquisivamo e gli strumenti che inventavamo avevano inesorabilmente espanso la nostra capacità di attacco, anche i nostri sistemi sociali evolvevano. Dovevano farlo.

Dai sistemi d'onore per trattare con pochi guerrieri alle prime democrazie maggioritarie dove il contare le teste dava pressapoco una buona stima di come una battaglia fra le parti sarebbe finita.

Ma mentre le nostre tecnologie espandono le nostre capacità, proteggere i più deboli fra i deboli è diventato sempre più importante. Dai moschetti nei boschi che permisero ad una minoranza di insorti di separarsi dall'Impero Britannico, ai candelotti di dinamite, il grande livellatore, come divenne noto alla classe lavoratrice nelle lotte dell'era progressista.

I nostri sistemi sociali, le nostre istituzioni politiche, la nostra morale civica, si sono adattati con riluttanza a questo contesto di cambiamenti. Solo che non si sono adattati abbastanza velocemente.

Quando parliamo degli sbalorditivi avanzamenti e cambiamenti scatenati dagli effetti di feedback dello sviluppo tecnologico notiamo sempre una comprensibile disperazione nel nostro linguaggio. "Ragazzi, ragazzi, ragazzi, questo è molto importante. Diventerà una cosa importante. Ci sono dei rischi. Sarà meglio che la facciamo nel modo giusto."

Troppo spesso però, le persone rispondono a problemi incredibilmente importanti con "useremo la democrazia" - senza alcuna analisi su cosa questo significhi realmente. "Democrazia", in questo contesto, è un blocco cognitivo, è uno slogan che usiamo per fermare le considerazioni. Per darci delle pacche sulle spalle.

Il concetto che la democrazia sociale ed il transumanismo siano riconciliabili è assurdo.

La democrazia, nel senso di processo di decisione a maggioranza, è primitivo. Deriva da un contesto in cui "quante persone" avevi avrebbe determinato il risultato di una battaglia. Ma anche la democrazia costituzionale, il miniarchismo, il socialismo illuminato, o la tecnocrazia e qualunque forma di governo, richiedono controllo, in un modo fondamentalmente irreconciliabile con il potenziamento tecnologico.

Il controllo è come la difesa. Per funzionare richiede che vengano sfrondate complessità, opzioni e dimensioni.

Tentare il controllo centralizzato sulla tecnologia significa, fondamentalmente, cominciare una guerra che può essere vinta solo distruggendo completamente ogni aspetto significativo delle nostre tecnologie.

David Cameron, Jeb Bush e diversi altri politici, funzionari del governo e capi di polizia del supposto illuminato occidente hanno indipendentemente chiesto di rendere illegale la crittografia. Noi ridiamo di loro, scuotiamo le teste e diciamo "non qui".

Sono qui per dirvi ciò che ogni esperto conosce, anche se cerca disperatamente di nasconderlo.

I sistemi di backdoor possono completamente funzionare. O almeno funzionare **per lo stato**. Non per noi, ovviamente. Ma questo non c'importa quando l'obiettivo diventa il controllo. Quando non sappiamo immaginare alcuna alternativa al controllo. Quando la nostra visione s'è ristretta così drammaticamente da non saper nemmeno cercare altri modi per collaborare o risolvere conflitti.

Internet può facilissimamente diventare un sistema in cui passa solo ciò che viene permesso, in cui ogni pacchetto viene firmato da un infrastruttura di server controllati dal governo, punto a punto a punto.

I dispositivi possono venire forniti di backdoor dalla fabbrica, per arrivare così al consumatore. E che nessuna produzione sia permessa fuori dal controllo statale.

Non siamo ancora al punto in cui la fabbricazione personale è sufficientemente distribuita da rendere la soppressione o la regolamentazione draconiana impossibili.

L'abolizione del computer general purpose è una minaccia reale. Così come le proposte di abolire internet.

Quando si parla di internet, delle tecnologie informatiche, della dissoluzione della proprietà intellettuale, diciamo spesso che la matematica è dalla parte della libertà. Ma mentre ciò, spesso, rende il controllo autoritario difficoltoso, questa difficoltà può ancora venire superata impiegando sufficiente forza, sufficiente rigidità infrastrutturale, e sufficiente appoggio popolare.

La forza più virulenta nelle guerre alla crittografia, nelle guerre del copyright, ed in ogni altra battaglia sulle tecnologie dell'ultimo decennio è stata la narrazione.

Stiamo perdendo la battaglia su molti fronti ed in molti settori demografici.

L'aristocrazia era storicamente anti-tecnologica. Buona parte dell'esplosione di filosofi continentali di metà ventesimo secolo che scrivevano tesi oscurantiste contro la tecnologia e la scienza venivano da una tradizione che sapeva perfettamente bene che se volevano continuare ad essere rilevanti, avrebbero dovuto ridurre l'accesso ai mezzi tecnologici alla gente.

Costruirono visioni Orwelliane di "libertà" che consistevano nel ritirarsi in qualche confinata statica e protetta condizione di vita. Il loro rifiuto della tecnologia equivaleva ad un rifiuto della libertà positiva, la "libertà di". Ciò che incoraggiavano invece era: Libertà _dalla_ conoscenza, libertà _dalla_ scelta, libertà _dalla_ crescita, libertà _dalla_ creatività e _dalla_ ricerca.

Questa corrente reazionaria filtra attraverso la nostra società. È immensamente influente. Non va sottovalutata.

La "libertà di" è distruttiva e complessa. Allarga le possibilità. E quando veramente decentrata, diffusa tra gli individui, rende impossibile il funzionamento del potere. Per ogni attore, individuo o istituzione diventa impossibile controllare la vasta insondabile diversità e complessità del mondo. Impossibile imporre editti, compresi quelli "democratici".

Quando i transumanisti liberali o socialdemocratici dichiarano che ciò di cui abbiamo bisogno è tecnologia "sotto il controllo del Popolo", ciò che non dicono mai è come esattamente questo tipo di controllo dovrebbe funzionare.

A cosa assomiglierebbe un mondo in cui **abbiamo la capacità** di impedire alle persone di stamparsi degli AR-15? Non pensate a quelle confuse associazioni con "democrazia", compresa "democrazia diretta". Domandatevi cosa dovrebbe essere realmente fatto per controllare la terapia genetica? Strutture di governo a sovrintendere l'uso dell'alta tecnologia? Backdoor di massa nei dispositivi di chiunque per monitorare aggressivamente e limitarne l'uso? Controllo totalitario di ogni comunicazione del pianeta? Retate aggressive contro tutti gli hacker e smanettoni? Censimento sistematico di ogni macchinario di fabbricazione esistente? Sorveglianza costante su ogni persona con la conoscenza di come queste cose funzionino? Controllo completo dell'allocazione di ogni risorsa del pianeta?

Questo è l'**unico** risultato della logica della "democrazia sociale" quando applicata alle aspirazioni transumane.

Non possiamo controllare la tecnologia avanzata senza un autoritarismo così completo che farebbe sbavare Hitler e Stalin nelle proprie tombe.

Quindi cosa possiamo fare?

In una conferenza precedente c'è stato un discorso sulla narrativa supereroistica ed io portai una battuta dal terzo film degli X-Men nella quale il presidente afferma: "Che speranza ha la democrazia quando la gente può spostare le città con la mente?"

La risposta inevitabile era: "Beh, abbiamo bisogno di un risveglio etico, una singolarità d'empatia che chiarifichi e raffini i nostri valori."

Assolutamente questo.

A cosa assomiglierebbe? Come ci arriviamo? E quali sono i meccanismi per cui tale mondo possa funzionare? Come vengono risolti i disaccordi?

Fortunatamente non dobbiamo reinventare la ruota. C'è un movimento di lunga data che ha affrontato questi problemi sociali ed etici e negli ultimi due secoli ha sviluppato risposte ed analisi approfondite.

Il termine "Anarchismo" è stato lanciato dal giornalista francese Pierre-Joseph Proudhon - un giornalista e cronista molto famoso, paragonabile all'odierno Glenn Greenwald. È stato adottato come modo per evidenziare e fare a pezzi l'uso Orwelliano di "anarchia" a significare sia la massima libertà - l'assenza di dominio o relazioni di potere - e contemporaneamente violenza caotica, presenza di aspiranti tiranni in competizione e litigiose relazioni di potere. Questo doppio uso in cui il termine "senza dominio" o "anarchia" è stato utilizzato per indicare competizione o litigiose relazioni di potere, è stato storicamente usato per spegnere qualsiasi movimento focalizzato sulla libertà, il più famoso quello contadini nella Guerra Civile Inglese. "Vuoi la libertà? Ma sappiamo tutti che la libertà è oppressione caotica e violenta."

In questa definizione promossa dalle elite del medioevo, la stessa idea di NON controllarci a vicenda, non dominarci a vicenda, non sfruttare, rubare, o far violenza l'un l'altro, è esclusa dal linguaggio stesso. Vien reso impossibile, in un certo senso, anche solo pensarla.

Proudhon contrastò tutto questo riportando il termine alle proprie radici etimologiche, e questo diede il via a due secoli di consistente e diligente resistenza al potere.

Gli anarchici non hanno mai preso potere, abbiamo resistito all'autoritarismo e all'oppressione in ogni arena. Dal denunciare il Marxismo molto prima che le sue aspirazioni draconiane divennero pubbliche, al combattere e morire per resistere al fascismo, combattendo Franco finché non poté più permettersi di unirsi ad Hitler e Mussolini e conducendo la resistenza contro i Nazisti attraverso l'Europa. Abbiamo combattuto i Raubritter, gli zar, gli oligarchi ed i burocrati sovietici.

E siamo stati straordinariamente popolari in diverse regioni e diversi punti della storia, anche se non abbiamo ancora avuto sufficiente massa critica per trasformare il mondo. In ogni istanza in cui l'anarchismo sorse alla popolarità locale con qualche milione di aderenti, come in Spagna ma anche Ukraina e Manchuria, immediatamente, ogni potere circostante mise in attesa le proprie guerre per collaborare nello spegnere gli esempi di un mondi migliore che stavamo portando, di modi migliore di interagire e mitigare le dispute con il prossimo, che non si trasformino in controllo ma costruiscando un tollerabile consenso per tutte le parti nei casi in cui fosse necessario un accordo.

Siamo stati in prima linea non solo per tecnologie come le criptomonete ed il progetto Tor, ma lo siamo stati anche nelle lotte contro patriarchia, razzismo, omofobia, ageismo, abilismo, ecc, ecc. Da molto prima che coalizioni come il "femminismo" divenissero popolari. Contrabbandavamo armi per gli schiavi e pubblicavamo giornali abolizionisti. Siamo fluiti attraverso le vene della nostra società esistente, come avanguardia di miriadi di tecnologie sociali come i crediti cooperativi e le cooperative. Siamo consistentemente serviti come fronte radicale per la coscienza del mondo ed abbiamo giocato un ruolo critico nell'espandere il possibile nello sviluppo e testando sul campo nuove intuizioni e strumenti.

L'anarchismo - come molti commentatori hanno ironicamente notato - è servito come laboratorio per le sinistre e per movimenti di giustizia e resistenza in tutto il mondo. Anche dove restiamo marginali, gli strumenti che inventiamo diventano popolari.

Non c'è bisogno di chiedersi come le persone risolverebbero i conflitti se ogni individuo super-potenziato portasse l'equivalente di un veto nucleare nella propria tasca. Abbiamo già testato e sviluppato forme sociali, avanzate strategie di teorie dei giochi che per le sole ragioni etiche già trattano le persone in quel modo.

Rappresentiamo già l'intelaiatura etica per navigare in un mondo transumano di superpotenziamento individuale. In tutta la nostra apparente marginalizzazione nelle giungle del Chapas o nelle strade di Atene, abbiamo preventivamente sfornato le politiche del futuro per gli ultimi due secoli.

Ma ciò che questa esperienza ha portato è anche una conoscenza delle funzioni dei sistemi di potere, delle loro dinamiche noiose e meccaniche. Il cancro sociopatico delle nostre strutture di potere non se ne andrà tranquillamente in una notte. Non ci sarà una qualche sorta di risveglio che farà improvvisamente cedere il controllo su di noi dai nostri governatori. Permettendo che nuove tecnologie li rendano irrilevanti. Non si sederanno passivamente permettendo che infrastrutture, culture alternative e nuovi mondi si sviluppino nel guscio del vecchio. Hanno sempre contrastato qualsiasi tentativo. E dovranno essere contrastati perché il futuro possa vincere.

L'anarchismo porta una chiarezza dagli occhi d'acciaio nel paesaggio in cui lottiamo.

Dice che anche se il potere statale può talvolta garantire dei cambiamenti, più lo usi più sarà difficile dissolvere quello stesso potere.

I Marxisti pretendevano che il loro obiettivo finale fosse un'utopia di massima libertà senza classi e senza stato, ma i mezzi che scelsero erano incoerenti con quest'obiettivo. Non puoi mandare le persone nel gulag per liberarle. Non puoi regolamentare gli strumenti che il popolo costruisce mantenendo l'impegno di espandere le loro possibilità nella vita, per renderci "più che umani".

Fini e mezzi non sono precisamente 1:1, ma sono profondamente interconnessi. E se l'anarchismo - e la nostra cassetta degli attrezzi di armonia rispettosa e consenso - è l'unico in grado di sopravvivere, l'unico funzionale modo di maneggiare il limite ultravioletto della capacità tecnologica espansa, oggi non possiamo permetterci di muoverci in direzioni opposte. Dobbiamo muoverci in modi che non svendono il futuro in cambio di miglioramenti a breve termine.

In breve, non possiamo permetterci di fare passi indietro, verso un maggior potere statale, nemmeno un maggior potere nelle mani di giganti corporativi come Google, nella speranza che questi mostri che oggi nutriamo perché ci semplificano i lavori, in qualche modo appassiscano di propria volontà. In qualche modo si conformino docilmente e resistano al potere a cui si sono abituati. Dobbiamo prendere la strada apparentemente più difficile, ma che si mantiene coerente.

Fortunatamente, una delle altre cose che l'anarchismo chiarisce è che per vincere non ci serve avere grosse legioni di persone dalla nostra parte. Una piccolissima minoranza può fare un'enorme differenza, può rendere impossibile al controllo di funzionare - può distruggere la rigidità e la sovraestensione insite nei sistemi che cercano di controllarci.

Quando avevo tredici anni mi misi un impermeabile e mi feci dare un passaggio sulla costa del pacifico fino alle strade di Seattle nell'ultimo fine settimana di novembre 1999. Quel giorno, da allora, diventò famigerato. La nostra "vittoria" sul WTO ministeriale fu mitizzata a livelli pericolosi, ma val la pena trasmettere la disperazione che sentivamo prima. Negli anni 90, mentre esso, senza opposizione, cresceva drammaticamente in termini di forza legale ed economica, nessuno sapeva che il WTO esistesse. La visione neoliberale che offriva arrivava dritta dal cyberpunk degli anni 80, un genere di controllo corporativo monopolistico, dove il capitale poteva liberamente attraversare le frontiere per crescere in forza ma la gente era lasciata imprigionata in campi schiavi come quelli in Bangladesh ed Eritrea. Certamente, questo rimane un caso. Ed oggi abbiamo il TPP (ndr. vedi [WikiLeaks - The US strategy to create a new global legal and economic system: TPP, TTIP, TISA.][2]). Ma ogni osservatore è d'accordo che lo slancio di questo processo fu severamente fermato in quel freddo giorno di novembre. Perché poche migliaia di persone lottarono nelle strade, innalzando un tale putiferio che quel processo silenzioso deragliò significativamente.

Lo spettacolo della protesta di strada non è certamente una panacea, ma solo una tattica utile in un limitato contesto e periodo.

Riflette una realtà più ampia, cioè che abbiamo molti strumenti a disposizione che sfuttano i punti deboli nelle sovraestese e rigide istituzionalizzazioni che sono innate in ogni sistema di controllo.

La loro incapacità di gestire il ribollente caos dei giovani studenti nelle strade riflette quanto la complessità computazionale rimanga assolutamente critica nelle questioni politiche.

L'era dell'informazione ha portato l'aumento della complessità su molti fronti per via dell'effetto feedback. La velocità che la tecnologia dell'informazione fornisce alle nostre mutazioni memetiche e culturali ha drammaticamente aumentato la complessità di un mucchio di cose. Prendete l'umorismo, ad esempio. Considerate cosa era divertente nel diciottesimo secolo, negli anni '50, e nei '90, e cos'è divertente oggi. Diamine, non dimentichiamo che nel diciassettesimo secolo dar fuoco ai gatti era un divertimento supremo.

La complessità della nostra cultura, le nostre identità, le nostre narrazioni, le nostre relazioni e le nostre politiche si sono solo accelerate. E con tale complessità arriva la speranza di una ridotta capacità di controllo. Diventa molto più difficile per politici o pubblicitari vendere semplici e potenti narrazioni universali. Stanno già notando l'accrescente diminuzione di ritorni e la minor trazione.

Ciò che questo processo di accelerata complessità rappresenta è una **singolarià sociale**.

Se la singolarità tecnologica è il punto passato il quale non possiamo fare previsioni o mantenere il controllo perché la complessità dello sviluppo tecnologico eccede la nostra comprensione, la singolarità sociale è analogamente il punto passato il quale non possiamo fare previsioni o mantenere il controllo perché la complessità delle nostre culture, idee, e relazioni sono cresciute in ricchezza, varietà, complessità, organicità e astrazione.

Potremmo certamente essere in grado di scatenare l'IA, ma la più grande quantità di potenza computazionale su questo pianeta è attualmente confinato in bassifondi, favelas, baraccopoli, comuni. Non dobbiamo aspettare la possibilità di un qualche decollo fra un decennio o più. Dobbiamo solo liberare e creare una rete migliore fra l'esistente potere delle nostre menti.

L'anarchismo comprende un ricco ecosistema di lavoro teoretico che sarebbe ridicolo cercare di riassumerlo brevemente.

Se sei interessato dalla teoria dei giochi e dal problema dell'azione collettiva suggerisco di leggere Michael Taylor e Elinor Ostrom. Se sei interessato al vasto schieramento di diseconomie di scala, soppresse dallo storico contributo di violenza ed alla tendenza dei mercati liberati verso fini egualitari, vi consiglio di leggere Kevin Carson. Per i sistemi legali policentrici, David Friedman e Robert Murphy. Abbiamo anche un discorso incredibilmente ampio e profondo sulle metodologie e strategie per quanto riguarda il percorso o i percorsi in avanti. Peter Gelderloos e David Graeber hanno ottenuto una certa fama a riguardo.

Ma nel suo nucleo l'anarchismo è una filosofia etica che mira ad espandere la libertà. I suoi più famosi impegni sono politici - l'abolizione dello stato, l'abolizione di concentrazioni centralizzate di potere coercitivo - ma si estende oltre, ad esempio, critiche sul controllo nelle relazioni interpersonali così come critiche alla rigidità ideologica. In questo frangente, il transumanismo rappresenta ancora un altro braccio dell'anarchismo: l'epicentro dell'espandere la libertà in termini fisici e la critica alla timida ritirata verso qualche mortificante "natura umana".

**William Gillis** _è un fisico teorico residente ad Oakland California ed un compagno al [Center for a Stateless Society][3]. S'è organizzato come attivista anarchico per oltre quindici anni ed ha scritto ampiamente di etica e tecnologia._

[1]:http://ieet.org/index.php/IEET/more/gillis20151029
[2]:https://www.youtube.com/watch?v=Rw7P0RGZQxQ
[3]:https://c4ss.org/
