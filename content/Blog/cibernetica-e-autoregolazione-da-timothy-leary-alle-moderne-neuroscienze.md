title:  Cibernetica e autoregolazione: da Timothy Leary alle moderne neuroscienze
date:   22-07-2015  19:16
tags:   fantascienza, filosofia della mente, filosofia, neuroscienze, scienza, psicologia, cyberpunk
summary: "Subiamo mutazioni e diventiamo un'altra specie  e ora ci spostiamo verso Ciberia. Siamo creature che strisciano verso il centro del mondo cibernetico. Ma cibernetica è la materia di cui è fatto il mondo. La materia non è altro che informazioni congelate."* **Timothy Leary**

*Articolo del 2011 di Cristina Coccia.*

*"Subiamo mutazioni e diventiamo un'altra specie  e ora ci spostiamo verso
Ciberia. Siamo creature che strisciano verso il centro del mondo cibernetico.
Ma cibernetica è la materia di cui è fatto il mondo. La materia non è altro che
informazioni congelate."* **Timothy Leary**, in **Caos e Cibercultura**,
immagina che tutto sia ormai riconducibile alla cibernetica, in un mondo
esprimibile come un sistema di informazioni da analizzare, decodificare e
plasmare secondo la nostra volontà. Cibernetica è un termine che **Norbert
Wiener** riporta alla luce, desumendolo dal greco *Kybernetes*, riconducibile a
**Platone** e al **Libro Primo** della Repubblica. In realtà, in quel discorso,
il filosofo ateniese sosteneva l'importanza della figura del "timoniere", come
governatore della nave e, metaforicamente parlando, come individuo capace di
dirigere, consigliare ed esercitare il potere sugli altri cittadini. *"Il
timoniere non si chiama timoniere perché naviga, ma per la sua arte e la sua
autorità sui marinai…Perciò un simile timoniere e capo non ricercherà e non
imporrà l'interesse del timoniere, ma quello del marinaio e di chi gli è
soggetto"*.

![Timothy Leary]({filename}/images/cibernetica2.jpg){: style="float:right"}

Lo stesso **Timothy Leary** s'impegna ad esaminare e comprendere il perché,
proprio nell'era della cibernetica, che si fonda su questo concetto di governo,
la *"democrazia"* sia diventata, invece, *"governo da parte della
maggioranza/folla e nemica della libertà individuale." "Non appena furono
disponibili i dispositivi cibernetici di comunicazione il potere politico fu
afferrato da coloro che governano le onde dell'etere."* E quindi  continua
sostenendo che *"nei primi anni (1950 - 1990) dell'era delle informazioni
elettroniche, la capacità dei governanti religioso-industrial-militari di
manipolare la televisione cambiò la democrazia facendone, da fenomeno di
comunità, un totalitarismo maggioritario basato sulla manipolazione nelle ore
di massimo ascolto.  I media cibernetici in mano a politicanti dotati di budget
pubblicitari scandalosamente grandi si sintonizzano sull'Mdc (minimo
denominatore comune). Anche le nuove e fragili democrazie dell'Europa orientale
dovranno probabilmente passare per questa fase di elezioni televuote da
marketing manipolato dai maghi degli spot e della pubblicità disonesta. E
queste sono le notizie brutte. Quella buona è che non è possibile controllare i
media cibernetici".* Fortunatamente diremmo, ma è ancora così?

![Norbert Wiener]({filename}/images/cibernetica3.jpg){: style="float:left"}

Chiudendo questa parentesi sociologica e tornando a **Wiener**, definiamo
questa scienza come lo *"studio del controllo e della comunicazione negli
animali e nelle macchine"*.  Per fissare le basi della cibernetica, che poi
hanno dato modo a biologi, ingegneri, matematici, filosofi e neurologi di dare
origine a peculiari studi orientati verso la conoscenza degli organismi
artificiali e dei meccanismi della mente umana, è necessario porre la nostra
attenzione su tre gruppi di concetti astratti: quelli relazionali di base,
quelli che identificano i processi dinamici di un sistema e quelli che
riguardano le sue strutture cicliche di controllo e orientamento ad uno scopo.
I concetti del primo gruppo sono: *sistema, varietà, vincolo, entropia e
informazione*. Ogni sistema ha, in sé, dei possibili e potenziali stati
alternativi che questo avrebbe potuto raggiungere ma non lo ha fatto. Perché?
Per rispondere al quesito, è necessario distinguere tra sistema e ambiente,
isolando le proprietà che un sistema possiede o meno e identificandole,
successivamente, con dei valori continui e discreti. L'insieme di tutti questi
valori, assegnati alle proprietà del sistema, definiscono lo *stato*; l'insieme
dei possibili stati è *lo spazio degli stati*. Ogni sistema ha un numero di
possibili stati che potrebbe assumere e denominiamo questo valore come la sua
*varietà*, che rappresenta una sorta di grado di libertà in cui il sistema si
muove. Purtroppo esistono dei vincoli che riducono la varietà effettiva del
sistema e questi vincoli si relazionano alla varietà stessa con formule che
calcolano l'*entropia* di un sistema, in base alla distribuzione della
probabilità di verificarsi di un certo stato. L'entropia è massima se tutti gli
stati di un sistema sono equiprobabili. In realtà, il vincolo diminuisce
l'entropia del sistema e questa diminuzione è misurabile con il concetto di
*informazione*.

Nel secondo e nel terzo macrogruppo ci sono concetti che classificano i
processi circolari in cui un sistema può incorrere: l'autoapplicazione,
l'autoorganizzazione, la chiusura e i cicli di retroazione e di controllo. Per
**William Ashby**, un sistema tende ad un equilibrio autoorganizzandosi e, se
il sistema è formato da sottosistemi, essi tendono ad adattarsi a vicenda. Più
un sistema diviene complesso, come, ad esempio, può esserlo il nostro sistema
nervoso o un sistema di reti neurali artificiali, più questi concetti di base,
che abbiamo descritto finora, si raffinano e iniziano a ramificarsi in una
gerarchia che comprende modellizzazioni, simulazioni, analisi su apprendimento,
cognizione e conoscenza.

Per questo motivo la cibernetica incontra, in questi ultimi anni, varie
discipline come l'ingegneria, la biologia, la psicologia e le neuroscienze.
Dopo la seconda guerra mondiale lo studio dei meccanismi di controllo, in
ingegneria, si sviluppò in maniera esponenziale, basandosi su presupposti come
lo studio della valvola a vapore di Watt e sulla realizzazione di meccanismi
più sofisticati.

![Cibernetica: Maxwell Maltz]({filename}/images/cibernetica4.jpg){: style="float:right"}

Nell'analisi biologica e fisiologica, si iniziò a parlare di autoregolazione di
sistemi viventi e, a questo proposito, successivamente, nel 1960, **Maxwell
Maltz** pubblicò un testo dal titolo **Psicocibernetica**, analizzando come,
nel nostro subconscio, esista un servo-meccanismo che lavori per uno scopo, e
che funzioni come un sistema guida che ci indirizza verso un determinato
traguardo, automaticamente, ma che possiamo imparare a governare con la nostra
coscienza.  Lo stesso **Weiner** sosteneva che non sarebbe mai stato possibile,
in un futuro, costruire un cervello elettronico paragonabile al cervello umano,
in quanto sarebbe sempre mancato un *operatore* che avesse avuto
l'immaginazione e la capacità di governare il suddetto servo-meccanismo. Alcuni
fisiologi come il **Dr. John C. Eccles** evidenziano come la nostra corteccia
cerebrale sia formata da una fitta rete neuronale attraverso cui passano
stimoli elettrici ogni volta che pensiamo, ricordiamo o immaginiamo. Ad ogni
nostro apprendimento, si stabilisce nel cervello una sorta di tatuaggio, un
engramma, una traccia mnestica che conserva gli effetti dell'esperienza nel
tempo e che viene messa da parte nel tessuto cerebrale per poterla, poi, tirare
fuori al momento opportuno, quando viene riattivato un ricordo o innescato un
processo cognitivo. Il nostro compito principale, come "timonieri" del nostro
servo-meccanismo, è riportare in vita questi modelli d'azione che sono stati
appresi in passato.

![Circuit 6]({filename}/images/cibernetica5.png){: style="float:left"}

In realtà, la nostra mente possiede numerose ed insospettabili potenzialità, di
cui non siamo consapevoli. I [livelli di consapevolezza][1] della mente,
secondo il **Dr. Timothy Leary**, sono otto e noi riusciamo, solitamente, ad
accedere soltanto ai primi quattro. Successivamente, esistono altri quattro
circuiti (chiamati così in riferimento, probabilmente, agli engrammi di cui
abbiamo parlato precedentemente), che ci danno accesso a capacità empatiche
maggiori, alle esperienze extracorporee, alla riprogrammazione della nostra
psiche e infine a prendere contatto con la nostra mente universale. Questa
"mente universale" è un concetto rintracciabile nelle filosofie indiane, in
particolare nella divinità **Brahma** (la Prima persona della Trimurti, il
Principio Creatore, che, in quanto tale, contiene in sé tutto l'universo, ma
che è a sua volta presente in ogni creatura), ed è esplicabile come una sorte
di coscienza collettiva, comune a tutti gli individui, che creerebbe una rete
universale completa, includendo tutte le singole menti, collegate con una
superiore, nella quale potrebbero identificarsi.

L'accesso a queste facoltà può avvenire, secondo **Leary**, tramite
meditazione, impegni spirituali e varie esperienze di coscienza espansa,
raggiungibile con l'ingestione di sostanze psichedeliche, come mescalina e DMT
(che fungono da chiave chimica per il sistema nervoso) o con yoga ed esperienze
di deprivazione sensoriale.

![John Lilly]({filename}/images/cibernetica6.jpg){: style="float:right"}

Metodi di deprivazione sensoriale furono studiati dall'inventore della vasca di
deprivazione, il neurofisiologo **John Lilly**, che iniziò a costruire questo
dispositivo nel 1954, combinando gli effetti di LSD e ketamina (un anestetico
per uso prevalentemente veterinario) su soggetti immersi in una soluzione di
solfato di magnesio, (mantenuta a temperatura corporea), in uno stato di
completo isolamento da stimoli sensoriali esterni. Il risultato di tale
combinazione di fattori era il raggiungimento di uno stato onirico profondo che
portava ad allucinazioni e alla possibilità di arrivare a livelli di
introspezione prima inconcepibili.

A questo proposito, si ricorda il film **Stati di allucinazione** (**Altered
States** - di **Ken Russell**) del 1980, tratto quasi esclusivamente dagli
studi di **John Lilly**. Estendendo la nostra mente a questi livelli, non è da
escludere, secondo la maggior parte dei neurofisiologi e degli studiosi della
mente, che ci si possa avvicinare alla coscienza collettiva di cui parlavamo
poc'anzi.  Spingendosi ancora oltre, **Lilly** ipotizzò un ulteriore organismo
di controllo globale, una o più reti di individui con un grado di coscienza
superiore, che governino le coincidenze che si verificano casualmente sulla
Terra, un comitato chiamato E.C.C.O. (Earth Coincidence Control Office).
Quest'idea è anche alla base del racconto di **Dick "Squadra riparazioni"
(Adjustment Team)**, adattato al cinema nel 2011 con il deludente **The
Adjustment Bureau**.

![Waking Life]({filename}/images/cibernetica8.jpg){: style="float:left"}

Il cinema è stato fortemente influenzato dalla cibernetica, soprattutto nella
fantascienza cyberpunk degli anni 80 e 90, ma, tralasciando questo specifico
filone cinematografico, è doveroso portare all'attenzione un interessante film
d'animazione, girato in rotoscope nel 2001, **Waking Life** (di **Richard
Linklater**), che affronta il tema dell'onironautica, in un efficace quadro che
ritrae l'attuale contesto sociale, discutendo di libero arbitrio,
esistenzialismo, coscienza collettiva, istinto ed evoluzione. In un punto in
cui si parla di evoluzionismo, un personaggio afferma: *La nuova evoluzione
nasce dall'informazione, da due tipi di informazione: digitale e analogica. La
digitale, è l'intelligenza artificiale; l'analogica deriva dalla biologia
molecolare, dalla clonazione dell'organismo, e le due si incontrano nella
neurobiologia*.

![Scienze Conigitive]({filename}/images/cibernetica9.gif)

E' proprio da questa bipartizione che hanno origine tutte le discipline che
derivano dalla cibernetica: non esiste più un quadro unitario, ma ci sono
alcuni cibernetici che si muovono nell'ambito dell'informatica e della teoria
degli automi e altri, invece, attivi nel settore dell'intelligenza artificiale,
che volgono maggiormente la loro attenzione verso gli aspetti filosofici e
psicologici del rapporto mente-macchina. Questi tipo di ricerca è riconducibile
al campo delle neuroscienze. Le discipline scientifiche che raggruppiamo sotto
il nome di neuroscienze fanno parte dell'anatomia, della biologia molecolare,
della biochimica, della fisiologia, della genetica e della patologia del
sistema nervoso. Partendo da questo contesto, che mira ad una descrizione
morfologica e fisiologica dei meccanismi di percezione, apprendimento, memoria
e linguaggio, si sale ad un livello superiore, in cui le neuroscienze
s'intersecano con le scienze cognitive, un insieme di settori disciplinari che
studiano i meccanismi di cognizione dei sistemi pensanti, partendo da quelli
più elementari, che fungono da modelli, per arrivare ai più complessi, siano
essi biologici o artificiali. Le scienze cognitive, essendo multidisciplinari,
creano connessioni, spesso effetto di collegamenti che si sviluppano in maniera
consequenziale, tra: filosofia, linguistica, psicologia, informatica,
antropologia e neuroscienze. Le discipline che fanno parte delle suddette
scienze sono: la neurofisiologia, la neuroscienza cognitiva, la psicologia
cognitiva, l'intelligenza artificiale (AI), la linguistica cognitiva e la
filosofia della mente.

![Neurone Specchio]({filename}/images/cibernetica10.jpg){: style="float:right"}

E' lecito pensare che proprio questo tipo di analisi dei nostri processi
cognitivi ed emotivi, sia alla base di una possibile evoluzione: ogni essere
umano vive, pensa, percepisce ed immagina in maniera diversa rispetto a
qualunque altro individuo. Questa singolarità è un fattore che ci dà modo di
evolverci, adattandoci a differenti contesti sociali, relazionandoci con gli
altri e decidendo quali comportamenti imitare o evitare. Creiamo continui
equilibri con l'ambiente nel quale esistiamo, e, per comprendere quale sia il
processo evolutivo più conveniente, la strategia migliore è analizzare noi
stessi, i meccanismi alla base del comportamento individuale, risultato dei
nostri bisogni e dei nostri desideri. Bisogna smettere di pensare che
l'evoluzione sia un processo esterno, passivo, in cui la consapevolezza di sé
non giochi un ruolo fondamentale.  Dobbiamo imparare a governare il singolo
individuo per realizzare le sue potenzialità, per sommare e amplificare le
coscienze individuali, per non esistere più come sistemi separati da membrane
impenetrabili, ma come membri di una rete globale, di un sistema che lavora e
si evolve grazie all'autoconsapevolezza delle sue parti. Forse questo è l'unico
modo per "comprendere" ed "apprendere" l'arte del governare le nostre menti,
creando una nuova intelligenza che non abbia limitazioni nel tempo e nello
spazio, capace di adattarsi e di essere, al tempo stesso, parte integrante
dell'ambiente, mantenendo e valorizzando, tuttavia, l'unicità di ogni
individuo, in quanto singolo sistema pensante.

[1]:http://deoxy.org/8circuit.htm
