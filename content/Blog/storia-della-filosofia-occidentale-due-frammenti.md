title: Storia della filosofia occidentale - due frammenti
date: 2014-12-11 20:00:06 +0100
tags: filosofia
summary: Due frammenti dal libro *Storia della filosofia occidentale* di Bertrand Russell.

Due frammenti dal libro *Storia della filosofia occidentale* di [Bertrand Russell][1].

Nello studiare un filosofo l’atteggiamento giusto non è né di reverenza né di
disprezzo, bensì *prima* una specie di ipotetica adesione perché sia possibile
capire ciò che egli sente, e credere nelle sue teorie, e dopo un risveglio
dell’atteggiamento critico il più possibile simile allo stato d’animo d’una
persona che sta abbandonando le opinioni che fino allora ha sostenuto. Il
disprezzo ostacola la prima parte di questo processo e la reverenza la seconda.
Due cose bisogna ricordare: che un uomo, le cui opinioni e teorie son degne di
essere studiate, si può presumere abbia posseduto una certa intelligenza; e che
d’altra parte è probabile che nessuno sia mai arrivato alla verità completa e
definitiva su un soggetto qualsiasi. Quando un uomo intelligente esprime un
punto di vista che ci sembra evidentemente assurdo, non dobbiamo tentare di
dimostrare che in qualche modo la cosa è vera, ma dovremo provare a capire come
mai sia successo che a lui *sia sembrata vera*. Questo esercizio della fantasia
storica e psicologica allarga subito il campo del nostro pensiero, e ci aiuta a
comprendere quanto sciocchi sembreranno molti dei nostri pregiudizi che ci sono
cari ad un’età di diversa *forma mentis*.

*Uno stralcio del capitolo 5 (Quinto e sesto secolo), parte prima, secondo libro.
Come fu decisa la natura di Cristo e come fu assassinata Ipazia.*

Il V secolo fu il secolo delle invasioni barbariche e della caduta dell’Impero
d’Occidente. Dopo la morte di Agostino nel 430, si ebbe scarsa attività
filosofica; ci fu un secolo di distruzioni, che servì però ampiamente a definire
le linee secondo le quali l’Europa era destinata a svilupparsi. […] Il governo
imperiale venne a cessare, […] si tornò a vivere in un ambito strettamente
locale dal punto di vista sia politico sia economico. Un’autorità centrale
rimase soltanto nella Chiesa, ed anche qui con grandi difficoltà. […

Durante questo periodo di confusione, la Chiesa fu turbata da una complessa
controversia intorno all’Incarnazione. I protagonisti del dibattito furono due
ecclesiastici, Cirillo e Nestorio, dei quali, più o meno per caso, il primo fu
proclamato santo ed il secondo eretico. San Cirillo fu patriarca di Alessandria
dal 412 circa fino alla sua morte nel 444; Nestorio era patriarca di
Costantinopoli. La questione dibattuta era il rapporto tra la divinità di Cristo
e la sua umanità. C’erano due Persone, una umana e una divina? Questa era la
opinione sostenuta da Nestorio. Altrimenti, c’era una sola natura o ce n’erano
due in una persona, una natura umana ed una natura divina? La controversia dette
origine, nel V secolo, a passioni ed ire in misura quasi incredibile. «Una
segreta ed incurabile discordia era venuta a crearsi tra coloro che più temevano
che si confondessero la divinità e l’umanità del Cristo e coloro che più si
preoccupavano che si separassero».

San Cirillo, il difensore dell’unità, era un uomo dallo zelo fanatico.
Approfittò della sua posizione di patriarca per incitare ai *pogrom* contro la
vastissima colonia ebraica di Alessandria. Il suo principale titolo di notorietà
è il linciaggio di Ipazia, una donna di gran riguardo che, in un’epoca bigotta,
aderiva alla filosofia neoplatonica e dedicava le sue facoltà intellettuali alla
matematica. Ella venne «tratta giù dalla sua carrozza, denudata, trascinata fino
alla Chiesa e inumanamente macellata per mano di Pietro il Lettore e di una
folla di fanatici selvaggi e spietati: la sua carne fu strappata dalle ossa con
taglienti conchiglie d’ostrica, e le sue membra, che ancora si agitavano, furono
gettate alle fiamme. Il dovuto corso dell’inchiesta e della punizione fu
arrestato mediante doni tempestivi». Dopo di che, Alessandria non fu più turbata
dai filosofi.

San Cirillo fu addolorato di apprendere che Costantinopoli veniva portata fuori
strada dagli insegnamenti del patriarca Nestorio, il quale sosteneva che ci
fossero due persone in Cristo, una umana ed una divina. Su questa base, Nestorio
si opponeva al nuovo uso di chiamare la Vergine, «Madre di Dio»; secondo
Nestorio, ella era soltanto la madre di una persona umana, mentre la persona
divina, che era Dio, non aveva madre. Su questo problema la Chiesa era divisa:
*grosso modo*, i vescovi ad est di Suez appoggiavano Nestorio, mentre quelli ad
ovest di Suez erano favorevoli a San Cirillo. Si stabilì di convocare un
Concilio ad Efeso nel 431, per definire la questione. I vescovi occidentali
arrivarono per primi, e procedettero a chiudere le porte in faccia ai
ritardatari, decidendo in gran fretta per San Cirillo, che presiedeva. «Questa
baraonda episcopale, alla distanza di tredici secoli, assume il venerabile
appellativo di Terzo Concilio Ecumenico.».

Come risultato di questo Concilio, Nestorio fu condannato come eretico. Non
ritrattò, ma divenne il fondatore della setta nestoriana, che ebbe largo seguito
in Siria ed in tutto l’Oriente. Alcuni secoli più tardi il nestorianesimo era
così forte in Cina che sembrò destinato a divenire la religione definitiva del
paese. Dei nestoriani furono trovati in India dai missionari spagnoli e
portoghesi nel XVI secolo. La persecuzione contro i nestoriani da parte del
governo cattolico di Costantinopoli ebbe il risultato di provocare un
malcontento che favorì i maomettani nella loro conquista della Siria.

La lingua di Nestorio, che con la sua eloquenza aveva sedotto tanti uomini, fu
mangiata dai vermi; almeno così ci assicurano.

Efeso aveva imparato a sostituire Artemide con la Vergine, ma aveva ancora lo
stesso zelo intemperante per la sua dea, come al tempo di San Paolo. Si diceva
che la Vergine fosse seppellita appunto ad Efeso. Nel 449, dopo la morte di San
Cirillo, un sinodo ad Efeso tentò di spingere oltre il trionfo e cadde quasi
nell’eresia opposta a quella di Nestorio; si tratta dell’eresia monofisita, la
quale sostiene che Cristo abbia soltanto una natura. Se San Cirillo fosse stato
ancora vivo, avrebbe certamente sostenuta questa opinione e sarebbe divenuto
eretico. L’imperatore appoggiò il sinodo, ma il Papa lo sconfessò. Infine papa
Leone, lo stesso papa che distolse Attila dall’attaccare Roma, nell’anno della
battaglia di Châlons, promosse la convocazione di un Concilio ecumenico a
Calcedonia nel 451, che condannò i monofisiti e stabilì finalmente la dottrina
ortodossa dell’Incarnazione. Il Concilio di Efeso aveva stabilito che c’è una
sola *Persona*  in Cristo, ma il concilio di Calcedonia stabilì che due sono le
sue *nature*, una umana e una divina. L’influenza del Papa fu decisiva
nell’assicurare questa decisione.

I monofisiti, come i nestoriani, rifiutarono di sottomettersi. L’Egitto, quasi
come un sol uomo, adottò l’eresia monofisita, che si sparse lungo il Nilo e fino
all’Abissinia. L’eresia degli Abissini fu addotta da Mussolini come uno dei
motivi per conquistarli. L’eresia dell’Egitto, come l’opposta eresia della
Siria, facilitò la conquista araba.[…]

[1]:https://it.wikipedia.org/wiki/Bertrand_Russell
