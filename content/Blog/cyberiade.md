title: Cyberiade
date: 2014-08-07 18:35:06 +0200
tags: fantascienza, libri, recensioni
summary: *Cyberiade, OVVERO, Viaggio comico, binario e libidinatorio nell’universo di due fantageni.* Già il titolo è tutto un programma…

*Cyberiade, OVVERO, Viaggio comico, binario e libidinatorio nell’universo di due fantageni.* Già il titolo è tutto un programma…

Lo apro, ed il primo racconto inizia così: *“Un giorno Trurl il costruttore montò
una macchina in grado di creare tutto quello che cominciava per N.”*.  
Non ho potuto, quindi, evitare di buttarmi nella lettura di questa
divertententissima e geniale raccolta di racconti scritta da [Stanislaw Lem][1]!  
Buona parte dei racconti narrano le gesta di due rinomati inventori robotici,
che vivono in un universo quasi completamente popolato da robot e macchine, con
la rara eccezione di qualche umano (specie detta, dei *Visipallidi*, ormai
estinta e ritenuta da molti come mitologica).

I due protagonisti, *Trurl* e *Klaupaucius*, viaggiano nel cosmo costruendo
bizzarri macchinari, piegando le leggi della fisica e delle probabilità per
diletto o per chiunque sia ben disposto a pagarli. I racconti sono scritti sotto
forma di favole e, dietro a questo aspetto giocoso, l’autore si diverte a
sviscerare e talvolta prendersi gioco di argomenti come la nascita della vita
intelligente, l’amore, l’intelligenza artificiale, la percezione della realtà,
le politiche sociali, la ricerca della felicità e l’utopia.

Questo libro, è pane per il palato di qualunque appassionato di fantascienza,
scienza, logica o filosofia.
Per chi ne volesse un assaggio, su questo blog ho trascritto uno dei racconti:
*I guai che provocò la perfezione di Trurl.*

![Cyberiade]({filename}/images/cyberiade.png "Cyberiade"){: style="float:right"}
> Due bizzarri costruttori ingaggiano sfide, su e giù per il cosmo, a suon di
> invenzioni. Congegni, meccaniche e diavolerie per ogni esigenza.  C’è un
> esercito allo sbando in Truffolandia? Servospettri e spaventocotteri
> affronteranno il nemico, eludendo, all’occorrenza, forze poli-poliziesche.
> C’è un principe malato, inguaribile, d’amore? Un disinnamoratore a base di
> debosciatori e libidinoni lo riporterà alla ragione.  Quanto a perizia, e
> fantasia, Trurl e Klapaucius non hanno eguali. Creano un bardo elettronico,
> che invia nell’etere poesie sconvolgenti. Porta i viaggiatori spaziali a
> stati di stupefazione poetica. Compone un carme d’amore espresso in puro
> linguaggio matematico. E provoca sfide all’ultimo bullone con i migliori
> poeti del cosmo, causando disordini pubblici.  Ma progettano anche
> amplificatori ed estintori di possibilità.  Né disdegnano esperienze al
> limite dello psichedelico: un apposito congegno è in grado di mandare a
> spasso la coscienza, che fa il giro di svariati corpi prima di tornare a
> casa.  In questo libro troverete una superba epopea fatta di sfide antiche
> come il mondo e avventure che hanno il sapore del domani e posdomani. Vi
> sorprenderà la grandiosità delle idee, l’ampiezza degli scenari, la
> chiaroveggenza di un caustico cosmologo.  Guarderete all’oggi e al futuro con
> occhi nuovi.  Scritti con l’acume di Einstein, lo spirito epico di Tolkien,
> la grazia di Voltaire, gli apologhi della Cyberiade hanno lasciato a bocca
> aperta generazioni di scienziati.  E ammaliato milioni di lettori in tutto il
> mondo.

[1]:https://en.wikipedia.org/wiki/Stanis%C5%82aw_Lem
