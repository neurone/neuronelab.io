title: Criptosfera
date: 2016-01-14 20:02
tags: fantascienza, recensioni, libri
summary: Bello, con alcune parti molto brillanti. M’ha fatto pensare al secondo filone di cyberpunk (che col cyberpunk originale mantiene solo alcuni legami), quello di Snow Crash e Mindplayers. Banks, in questo libro, ha una grande fantasia, ed un approccio molto personale alla realtà virtuale… un tema che, a mio parere, sembrava essere diventato un po’ stagnante e statico.

![Criptosfera]({filename}/images/criptosfera.jpg "Copertina: Criptosfera"){: style="float:right"}
Bello, con alcune parti molto brillanti. M’ha fatto pensare al secondo filone di cyberpunk (che col cyberpunk originale mantiene solo alcuni legami), quello di [Snow Crash][1] e [Mindplayers][2]. [Banks][3], in questo libro, ha una grande fantasia, ed un approccio molto personale alla realtà virtuale… un tema che, a mio parere, sembrava essere diventato un po’ stagnante e statico. Solo il finale, m’è sembrato un po’ deboluccio, ma è compensato da uno svolgimento spettacolare ed alcuni passaggi, addirittura, esaltanti.

> La morte è stata sconfitta? Sì. Nella realtà virtuale della Criptosfera, una “copia” di noi stessi può continuare a esistere e addirittura a interagire coi vivi. Eppure tutto ciò potrebbe finire da un momento all’altro: un fenomeno misterioso – chiamato l’Invasione – sta infatti minacciando di annientare sia la Terra sia il criptomondo. Una minaccia che sembra tuttavia importare poco al re Adjine VI, determinato piuttosto a non farsi sottrarre neppure una stilla del suo potere. E invece importantissima per il Conte Sessine III, che, dopo essere stato assassinato per l’ennesima volta, è costretto a addentrarsi appunto nella Criptosfera per scoprire chi ha voluto la sua morte. Così il destino dei due mondi grava sulle fragili spalle della ricercatrice Gadfium, che, in risposta a un messaggio inviato da esseri sconosciuti, deve compiere un lungo viaggio per arrivare a capire cosa sia veramente l’Invasione e in quale modo si possa fermare.

Recensione su Fantascienza.com: [Criptosfera][4].

Sul blog _Il futuro è tornato_, c’è un articolo di approfondimento sull’autore. Se volete leggere, è qua: [Iain (M.) Banks: la Cultura di un autore di fantascienza][5]

[1]:https://en.wikipedia.org/wiki/Snow_Crash
[2]:http://www.shake.it/index.php?27&backPID=27&swords=mindplayers&productID=64&pid_product=51&detail=
[3]:https://en.wikipedia.org/wiki/Iain_Banks
[4]:http://www.fantascienza.com/2052/criptosfera
[5]:http://ilfuturotornato.com/2013/01/08/iain-m-banks-la-cultura-di-un-autore-di-fantascienza-prima-parte/