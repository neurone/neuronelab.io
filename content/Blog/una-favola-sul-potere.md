title: Una favola sul potere
date: 2015-03-23 22:57:24 +0100
tags: racconti, allegorie, politica
summary: Nel villaggio, da qualche tempo, le cose andavano male. Le coltivazioni non crescevano più come una volta. A causa di questo, la gente stava cominciando a morire di fame.

Nel villaggio, da qualche tempo, le cose andavano male. Le coltivazioni non crescevano più come una volta. A causa di questo, la gente stava cominciando a morire di fame.

Quando le situazioni diventano disperate, si sa, è il momento in cui iniziano a
verificarsi strani miracoli e comincia a fare capolino la magia. Correva voce
che sulla cima della montagna vicina, un altissimo pinnacolo di roccia senza la
minima vegetazione o vita animale, ci fosse una pianta magica, dalla capacità di
donare un enorme potere al primo che ne avrebbe mangiato. Molta gente cercò di
avventurarsi in solitaria su questa montagna, ma senza successo. Alcuni capi
famiglia decisero di organizzarsi meglio per portare a termine l’impresa, ma non
riuscirono a trovare un accordo su come sarebbe poi stato usato il potere della
pianta magica.

Alcuni rivendicavano il diritto di privilegiare la propria famiglia, se fossero
stati loro a compiere la difficilissima impresa. Altri preferivano tenere
segreto il proprio progetto, dicendo che era troppo complesso per essere
spiegato, ma che avrebbe portato grandi benefici. Altri ancora affermavano che
avrebbero lasciato quel potere ai più poveri ed ai più deboli, perché nessuno
più di coloro che avevano conosciuto di persona il dolore e la miseria, sarebbe
stato in grado di fare buon uso del potere. L’avrebbero certamente usato per
aiutare tutti, senza più distinzioni e discriminazioni. Il protagonista di
questa storia era uno di loro.

Ogni eroe partì separatamente, sostenuto ognuno dalla propria famiglia e da chi
credeva il lui. Dato che l’impresa era molto dura e difficile, ogni fazione
sosteneva il proprio campione donandogli una parte delle proprie già scarseggianti
risorse. Così, di volta in volta, a seguito dell'eroe, partivano delle
spedizioni per portargli cibo, abiti nuovi e strumenti utili, in sostituzione a
quelli che si danneggiavano durante il tragitto.
<!--more-->
Il nostro protagonista procedeva con fatica nella sua scalata. Talvolta dovendo
difendersi dai tentativi di sabotaggio dei suoi avversari, talvolta
dall'ambiente ostile. Il pensiero del bene che avrebbe fatto a tutti ed il
sentirsi apprezzato e supportato dai propri famigliari ed amici gli dava la
forza di proseguire, Quando si guardava indietro vedeva la gente del villaggio
sempre più piccola e la immaginava seguire con lo sguardo la sua scalata. Li
vedeva guardare in alto e talvolta festeggiare quando pensava di aver raggiunto
una nuova importante tappa. Gli approvvigionamenti erano quasi sempre regolari.
Talvolta capitava che per qualche giorno, non arrivasse nessuno a portargli
degli strumenti nuovi e del cibo, ma appena la successiva staffetta lo
raggiungeva, portandogli il necessario per vivere e per proseguire la scalata,
lui ne era sempre estremamente grato, e teneva molto a mostrarlo.

Più si avvicinava alla meta, più diventava elettrizzato, orgoglioso e pieno di
speranze e progetti per il potere che avrebbe acquisito. Gli abitanti del
villaggio diventavano sempre più piccoli e le staffette meno frequenti, ma a lui
sembrava che lo stessero sempre sostenendo ed attendendo con speranza. I
tentativi di sabotaggio ed i furti da parte degli avversari avvenivano invece
sempre più spesso e talvolta anche lui era stato tentato di rendere, occhio per
occhio, ciò che gli altri gli facevano.

Dopo molte settimane, arrivò vicinissimo alla meta. Da qualche giorno non
giungevano più staffette, e gli abitanti del villaggio, verso i quali si
rivolgeva ogni volta che si trovava in difficoltà per cercare conforto, ormai
erano meno che dei puntini. Si sentì solo e temette che lo avessero abbandonato.
Forse gli abitanti non credevano più in lui, ma avrebbero dovuto avere fede
nella rivoluzione che avrebbe portato a beneficio di tutti. Una volta acquisito
il potere della pianta magica, li avrebbe fatti stare tutti bene. Non sarebbero
vissuti nel lusso, ma non sarebbe nemmeno mancato loro il necessario. Sarebbero
stati tutti felici ed in armonia. Il suo era certamente uno spirito nobile ed
altruista, se confrontato a quello di alcuni suoi avversari che, mano a mano che
l’impresa diventava sempre più dura e gli approvvigionamenti scarseggiavano,
restringevano sempre di più il gruppo di persone che avrebbero aiutato con il
proprio nuovo potere, una volta acquisito. Alcuni decisero di condividere il
proprio potere solo con la famiglia, che avrebbe provveduto anch'essa, prima a
se stessa e poi agli altri. Altri avrebbero tenuto il potere solo per se,
utilizzandolo per gli altri solo in cambio di favori e sudditanza. Altri ancora
avrebbero donato parte del proprio potere a persone di fiducia ed organizzato un
sistema strutturato e complessissimo per gestirlo e fare il bene della gente in
base a quanto essa lo meritasse, il tutto deciso in base a calcoli molto
complessi.

Il nostro protagonista, sempre più attanagliato dalla solitudine e dall'idea che
la gente del villaggio l’avesse abbandonato, in un giorno molto freddo, perse la
presa sulla roccia ed iniziò a cadere.

Fu proprio durante la caduta che capì. Vide gli uomini di staffetta, morti
durante i tragitti per raggiungerlo. Altri che si furono fermati per utilizzare
loro stessi il cibo e gli strumenti donati dalla gente del villaggio. Altri
ancora che tornarono indietro, per riferire di aver trovato loro stessi la
pianta miracolosa, chiedendo di essere serviti in cambio di miracoli. Ma la
maggior parte di coloro che vide, durante la lunghissima caduta, erano solamente
morti nel tentativo di raggiungerlo per portargli cibo, abiti e strumenti.

I puntini del villaggio, che quand'era così in alto e vicino alla pianta
portentosa, vedeva talmente piccoli da essere quasi invisibili, durante la
caduta, ritornavano ad avere sempre più la forma delle persone, e le loro azioni
diventavano più chiare e comprensibili. Alcuni lo osservavano davvero nella sua
scalata, con la speranza sempre forte, che lui avrebbe ottenuto il potere magico
ed avrebbe fatto del bene a tutti. Ma molti di quelli che a lui sembrava
stessero facendo questo, invece, erano semplicemente morti. Tutte le risorse che
mandavano a lui, per sostenerlo nella missione, erano cibo, abiti e strumenti,
che avevano tolto a loro stessi. Molti altri di quelli che sembravano esultare,
stavano invece lottando per rubare all'altro le risorse che avrebbero poi
mandato all'eroe nel quale credevano di più. Altri invece si azzuffavano nel
rubarle per sé stessi. Ed anche fra loro, i morti erano a decine.

Nelle ultime frazioni di secondo che precedettero lo schianto a terra, gli
sembrò di vedere alcune persone che aveva notato solo di sfuggita durante
l’organizzazione della sua spedizione. Erano molto lontane dalla periferia del
villaggio, vicino a delle case abbandonate che avevano restaurato. Li vide
coltivare la terra in un modo che non aveva mai visto prima. I terreni che prima
erano usati per le patate, ora erano coltivati a ravanelli, su quelli dove prima
veniva coltivato il grano, ora vi crescevano piante di fagioli. E crescevano
come non li si vedeva più da anni. In tutta la sua vita, né lui, né nessun altro
degli eroi delle spedizioni avrebbe pensato ad un’idea simile. Era convinto che
fosse necessario avere più potere, un potere magico e superiore, per poter dare
vita e benessere a tutte le persone del villaggio. Solo in quel momento si
ricordò che alcune di quelle persone, si erano sempre opposte all'idea della
spedizione per ottenere il potere di salvare tutti. Esse sostenevano, che
avrebbero dovuto semplicemente collaborare fra di loro per trovare, tutti
assieme, una soluzione per salvarsi dalla carestia, e che la ricerca del potere
avrebbe causato solo guai e sofferenza.
