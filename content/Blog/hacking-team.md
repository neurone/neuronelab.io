title: Lo scandalo di Hacking Team
date: 2015-07-14 19:04
tags: privacy, rete, sicurezza, tecnologia
summary: Il leak di *hacking team* ci offre uno spaccato su un mondo interessante, un pezzo di realta’ che preferirebbe rimanere nell’ombra, e condurre un po’ in disparte, i propri affari con governi, intelligence e corpi militari a seguito.

Riporto dal blog di autistici/inventati.

Il leak di *hacking team* ci offre uno spaccato su un mondo interessante, un
pezzo di realta’ che preferirebbe rimanere nell’ombra, e condurre un po’ in
disparte, i propri affari con governi, intelligence e corpi militari a seguito.
Se non fosse che la pubblicita’ e’ l’anima del commercio e la sicurezza
informatica e’ una grande fiera di paese… Il materiale pubblicato si presta a
molte letture e approfondimenti. Io vorrei puntare l’attenzione sull’effimero
mercato della sicurezza IT, sulle sue regole e funzionamento, che sono una
metafora perfetta della fragilita’ sulla quale si basa il nostro buffo mondo.
Gli affari di hacking team giravano attorno a una piattaforma software, RCS
(remote control system), una soluzione  con iconcine gommosette, pensata per
gestire comodamente backdoor su computer e cellulari. Spiccano i nomi degli
agent che connotano il target militare della clientela: scout, soldier, elite.
Il valore in termini monetari del giochino dipende da due fattori: il vettore
di installazione, il non essere rilevato dagli antivirus. Entrambi questi
aspetti riconducono ad un termine che anche i giornali ormai ogni tanto
infilano negli articoli, per fare colore: 0-day. Questo, sconosciuto ai piu’,
oggetto del desiderio,  che riesce a smuovere la libido di ingegneri e
informatici impiegati nel campo della sicurezza.
I software sono scritti da persone, le persone qualche volta sbagliano e
introducono degli errorini che adeguatamente sfruttati permettono di far
eseguire al proprio giocattolino elettronico altre cose, diverse da quelle che
il programma che si sta usando era pensato per fare. Nel nostro caso monitorare
live tutte le attivita’ eseguite su computer e cellulari.
Il codice in grado di sfruttare le sviste prende il nome di exploit, un exploit
0-day non e’ pubblico, quindi solo chi ha trovato l’errorino e quelli a cui ha
voglia di dirlo, possono sfruttare la vulnerabilita’.
In un mondo diverso dal nostro, la ricerca degli errorini sarebbe un giochino
divertente per farsi gli scherzi l’un l’altro.
Nel nostro umanschifo cosi’ splendidamente capitalista, cosi’ trionfalmente
guerrafondaio, cosi’ patologicamente dominato da persone in preda a deliri
ossessivi da disturbo narcisistico di personalita’, non e’ cosi’. Sono i
diamanti del mercato della sicurezza, al piu’ qualche centinaio di righe di
codice ( quando sono fatti a modino ) prezzati in diverse decine di K dollari.
Dalle mail disponibili su wikileaks si puo’ seguire l’evoluzione dell’hacking
team dal 2005 in avanti. Negli ultimi 2 o 3 anni, lo 0-day pack diventa uno
delle loro preoccupazioni principali. Poter offrire vettori di  installazione
con vulnerabilita’ non note, e’ la chiave di volta per fare breccia nel cuore
dei governi e impressionare  le intelligence mondiali.
Una mail del grande capo spiega bene che i loro punti di debolezza all’inizio
del 2014 sono gli exploit e il mobile.  
[https://wikileaks.org/hackingteam/emails/emailid/15113][2]  
Nel 2013 parlavano invece cosi’  
[https://wikileaks.org/hackingteam/emails/emailid/340289][3]  
La situazione e’ ancora piu’ chiara qui  
[https://wikileaks.org/hackingteam/emails/emailid/347574][4]  
Il competitor citato e’ l’israeliana [Nso][5].

Negli anni tentano dunque di infittire i rapporti nel ristretto ed elitario
mercato mondiale degli 0-day. E parallelamente mettono in campo risorse per
produrseli “in house”. Perche’ nell’ultima decade la ricerca di bug da cui
trarre info leak e exploit e’ diventata sempre piu’ una faccenda complicata e
Hacking Team ha bisogno di 0-day di qualita’, o anche no: l’importante e’ che
funzionino. Dalle mail paiono servirsi essenzialmente da persone, che
periodicamente mandano il proprio catalogo dei francobolli rari, con i prezzi
al pezzo. Il loro team di tecnici valuta, prova e sceglie. Compra 0-day per
flash, cose per android  e via cosi’. Comprano sembra da un russo. A quanto si
capisce non ha voluto mostrarsi dal vivo se non incarnato nel proprio codice,
che pare essere elegante e ben studiato. Lo prendono un po’ in giro perche’ e’
uno che posta anche su [zdi][6], bruciandosi cosi’ gli 0-day
([https://wikileaks.org/hackingteam/emails/emailid/24676][7]). Da un americano,
Dustin, un broker, piu’ che uno sviluppatore probabilmente (interessante questa
mail [https://wikileaks.org/hackingteam/emails/emailid/347574][8]). Poi un asiatico,
Eugene, incluso nell’organico di Hacking Team, perche’ ad un certo punto
compare con una mail @hackingteam e sembra impiegato a tempo pieno nello
ricerca/sviluppo di 0-day. La gallina dalle uova d’oro. E’ bellina una mail
nella quale si preoccupano del rilascio di windows 10, perche’ hanno pochi
exploit funzionanti per quella piattaforma e ripongono le loro speranze in uno
per vlc sviluppato da Eugene.  
[https://wikileaks.org/hackingteam/emails/emailid/1039373][9]  

I pagamenti per gli exploit avvengono a trance di mesi, se la vulnerabilita’
dopo tot mesi viene patchata, il pagamento e’ sospeso. Una sorta di garanzia.
I clienti vogliono la silent installation tramite exploit. E nel 2015 hacking
team sembra cresciuta nell’organico e aver ampliato RCS equipaggiandolo con
diversi 0-days e altre cosine all’ultimo grido:
[https://wikileaks.org/hackingteam/emails/emailid/23][10]

Interessante la parte sulla uefi infection, in cui compare Tails.
Il giochino se siete curiosi e’ spiegato piu’ o meno in questa mail
[https://wikileaks.org/hackingteam/emails/emailid/473283][11]

La ricerca della parola uefi mi ha condotto a questo report da manuale di It
crowd [https://wikileaks.org/hackingteam/emails/emailid/19213][12]

Si capisce che sono in due ad una sorta di demo con un  cliente,
l’installazione uefi funziona, ma l’agent scout poi non parte al riavvio della
macchina, allora mentre uno dei due distrae il cliente l’altro lancia un
exploit di quelli che zitti zitti funzionano sicuro (silent installation) e il
tipo non si accorge di nulla. Ripetono il giochino con un popup di avg che
compare durante la demo. Lo chiudono al volo e il cliente di nuovo non nota
nulla. Ancora piu’ bello il test con norton, che rileva l’agent. Allora mentre
il secondo distrae il compratore (che a questo punto possiamo pero’
classificare come un po’ scemotto) l’altro mette in whitelist scout e cosi’ la
sfangano di nuovo.
In ultimo con windows 8.1 e bit defender riescono a ricavare meno informazioni
del dovuto dal sistema, pero’ il cliente tanto non sa bene cosa dovrebbe fare
l’oggetto e quindi non si accorge di niente.

Insomma il loro rapporto con la realta’ e’ piuttosto particolare considerato
che hanno a che fare con militari/intellegence, e sperano di essere rilevati
(salvati dal fallimento) dai servizi segreti italiani. Sembrano divertirsi a
sviluppare questi giochini e si autoincensano in qualita’ di azienda
all’avanguardia nel panorama mondiale, pero’ fanno le supercazzole ai clienti,
come il verduriere che tiene il dito sulla bilancia per fregare la vecchietta
di turno.
Leggendo i loro scambi di email colgo una sorta di incapacita’ nel districarsi
nella complessita’ del reale. Queste poche righe mettono a disagio
[https://wikileaks.org/hackingteam/emails/emailid/58672][13] L’elenco di
supercattivi che si conclude con l’ottocentesco “complotti anarchici e
terroristici” e l’atteggiamento da “ah ma se voi, sapeste umanita’ ingrata,
tutto il bene che vi stiamo facendo” rivela una tendenza a semplificare la
realta’, quale meccanismo di autodifesa e di autoassoluzione. Pensare che
governi/militari/intelligence siano i buoni che  combattono i supercattivi e’
qualcosa che non trova corrispondenza nella storia dell’umanita’.
Per restare sui fumetti, perfino il trio paperinik, paperinika, paper bat
rivelano una maggiore capacita’ di indagine del mondo circostante, e
l’ispettore gadget li batte in simpatia.

Comunque se fossi in hacking team io non me la prenderei troppo per il leak.
Alla fine alcuni di loro non provengono dall’esperienza di antifork.org ? E
nella pagina about di antifork non c’era forse scritto

Antifork Research supports the free software movement, full-disclosure of
security problems and freedom of information. We dislike commercial security
brands, ’cause money doesn’t always mean security. We follow many exciting
projects, related to computer technology.

Prendetela come un ritorno ai bei vecchi tempi, ora tutti sanno tutto:
full-disclosure.
E cmq coraggio, come ama ripetere il grande capo: [BCM!!!][14]

- [Articolo originale][1]

[1]:https://cavallette.noblogs.org/2015/07/8859
[2]:https://wikileaks.org/hackingteam/emails/emailid/15113
[3]:https://wikileaks.org/hackingteam/emails/emailid/340289
[4]:https://wikileaks.org/hackingteam/emails/emailid/347574
[5]:https://www.israelbizreg.com/nso-group-technologies-ltd
[6]:http://www.zerodayinitiative.com/about/
[7]:https://wikileaks.org/hackingteam/emails/emailid/24676
[8]:https://wikileaks.org/hackingteam/emails/emailid/347574
[9]:https://wikileaks.org/hackingteam/emails/emailid/1039373
[10]:https://wikileaks.org/hackingteam/emails/emailid/23
[11]:https://wikileaks.org/hackingteam/emails/emailid/473283
[12]:https://wikileaks.org/hackingteam/emails/emailid/19213
[13]:https://wikileaks.org/hackingteam/emails/emailid/58672
[14]:https://wikileaks.org/hackingteam/emails/?q=boia+a+chi+molla&mfrom=&mto=&title=&notitle=&date=&nofrom=&noto=&count=50&sort=0#searchresult
