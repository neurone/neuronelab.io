title: Perché non siamo il nostro cervello (prefazione)
date: 2016-01-14 20:11
tags: filosofia, filosofia della mente, scienza, neuroscienze, psicologia
summary: Riporto una parte della prefazione del libro _Perché non siamo il nostro cervello_, pubblicato nel 2009. L’autore è Alva Noë, filosofo esternalista.

![Perché non siamo il nostro cervello]({filename}/images/perche-non-siamo-il-nostro-cervello-prefazione.jpg "Copertina: Perché non siamo il nostro cervello"){: style="float:right"}
Riporto una parte della prefazione del libro _Perché non siamo il nostro cervello_, pubblicato nel 2009. L’autore è [Alva Noë][1], filosofo [esternalista][2].

_Questo post era stato scritto originariamente nel 2013_

Viviamo in un’epoca di crescente entusiasmo per il cervello. Soltanto la preoccupazione di trovare un gene per qualunque cosa compete oggi con il diffuso ottimismo che circonda le neuroscienze. Percezione, memoria, piacere o dispiacere, intelligenza, morale… il cervello è considerato l’organo responsabile di ogni cosa. Si crede comunemente che persino la coscienza, il Santo Graal della filosofia e della scienza, sarà presto fatta oggetto di una spiegazione neurale. In un’era come la nostra, contraddistinta da spettacolari e costose tecniche di brain imaging (come la risonanza magnetica funzionale e la tomografia a emissione di positroni), difficilmente passa un giorno senza che le pagine scientifiche dei principali quotidiani e delle più importanti riviste diano notizia di nuove conquiste e sensazionali scoperte.

Dopo decenni di sforzi comuni da parte di neuroscienziati, psicologi e filosofi, l’unico punto che sembra rimanere non controverso circa il ruolo svolto dal cervello nel renderci coscienti, ossia il modo in cui esso dà origine alle sensazioni, ai sentimenti e alla soggettività, è che non ne sappiamo nulla. Anche i più entusiasti delle nuove neuroscienze della coscienza ammettono che, allo stato attuale delle cose, nessuno possiede ancora una spiegazione plausibile del modo in cui l’esperienza – la sensazione della rossezza del rosso! – possa emergere dall’azione del cervello. Nonostante la tecnologia di cui disponiamo e nonostante la sperimentazione sugli animali, non siamo oggi più vicini a comprendere le basi neurali dell’esperienza di quanto lo fossimo cent’anni fa. Ci manca una teoria che spieghi, in maniera sia pure grossolana, quale contributo possa recare il comportamento di singole cellule all’emergere della coscienza. Questo non è di per sé uno scandalo, ma può diventarlo se permettiamo che la propaganda nasconda il fatto che stiamo brancolando nel buio.

Sentiamo spesso ripetere che le neuroscienze della coscienza si trovano ancora in una fase precoce del loro sviluppo. Ciò non è però corretto, in quanto suggerisce che il progresso sia in grado di prendersi cura di sé: è solo questione di tempo, di un normale processo di maturazione. Un’immagine migliore potrebbe essere quella di escursionisti inesperti che si trovano fuori da ogni sentiero senza avere alcuna idea di dove siano: si sono persi, e neppure lo sanno! L’obiettivo di _questo libro_ è aiutarci a capire dove siamo e a trovare la strada per andare avanti.

Il nostro problema consiste nel fatto che abbiamo cercato la coscienza dove non c’è. Dovremmo invece cercarla là dove essa si trova. La coscienza non è qualcosa che accade dentro di noi. Piuttosto, è qualcosa che facciamo o creiamo. Meglio: è qualcosa che realizziamo. La coscienza assomiglia più alla danza che alla digestione.

Lo scopo di questo volume è convincervi di ciò. Voglio anche mostrarvi che quanto sostengo è quello che ci insegna un genuino approccio biologico allo studio della mente e della natura umana. L’idea che l’unica indagine propriamente scientifica della coscienza sarebbe quella che la identifica con eventi nel sistema nervoso è frutto di un riduzionismo ormai datato. Tale idea è analoga a quella che considera la depressione solo un male cerebrale. In un certo senso, questo è ovviamente vero. Esistono “firme” neurali della depressione. L’azione diretta sul cervello, nella forma di una terapia farmacologica, può avere effetti sulla depressione. Ma in un altro senso, questo è ovviamente non vero. È semplicemente impossibile comprendere in termini unicamente neurali perché una persona cada in depressione, ovvero perché proprio questo individuo qui e ora sia depresso. La depressione colpisce persone in carne e ossa, che affrontano, sulla scorta delle loro storie di vita vissuta, i problemi concreti della loro esistenza; e lo fa sullo sfondo non solo di queste storie individuali, ma anche della storia filogenetica della specie. Il dogma che la depressione sia un male cerebrale serve agli interessi delle compagnie farmaceutiche, non c’è alcun dubbio. Serve anche a destigmatizzare la lotta contro la depressione, i che è una buona cosa. Ma esso è falso.

Per progredire nella comprensione della coscienza occorre rinunciare alla microanalisi neurale interna (per dirla nei termini usati da Susan Hurley e da me tempo fa). Il luogo della coscienza è la vita dinamica dell’intera persona o dell’intero animale immersi nel loro ambiente. È solo assumendo una prospettiva olistica sulla vita attiva della persona e dell’animale che possiamo cominciare a rendere intelligibile il contributo che il cervello dà all’esperienza cosciente.

_Questo libro_ contiene delle proposte. L’esperienza umana è una danza che si svolge nel mondo in compagnia di altri individui. Noi non siamo il nostro cervello. Non siamo rinchiusi nella prigione delle nostre proprie idee e sensazioni. Il fenomeno della coscienza, così come quello della vita, è un processo dinamico che coinvolge il mondo. Siamo di casa in ciò che ci circonda. Siamo fuori dalle nostre teste.[…]

Vedi anche:

- [Embodiment radicale – neuroantropologia][3]
- [Azione nella percezione – neuroantropologia][4]

Commenti:
---------
**[dottornomade][5]**
_on 21 gennaio 2013 at 10:54_

In prima approssimazione, però, dai, sì che siamo il nostro cervello. O meglio, ciò che vi accade.

**Masque**
_on 21 gennaio 2013 at 11:22_

Mi sembra di capire che il punto di vista sia del tipo: sì, il cervello è indispensabile perché emerga una coscienza, ma il cervello senza corpo e ambiente, non potrebbe far emergere una coscienza, perché essa è un fenomeno dinamico che scaturisce dall’interazione con corpo e mondo.
Lo scopo del libro sembra essere quello di provocare un punto di vista diverso, un “pensiero laterale” sulla questione della coscienza. Dato che, tramite il riduzionismo delle neuroscienze, non si è ancora riusciti a trovare cosa sia l’esperienza cosciente, forse è perché la si cerca nel modo sbagliato.
Nel primo capitolo fa una panoramica accennando anche a casi di stato vegetativo e di sindrome locked-in. Dice che nel resto del libro, approfondirà un po’ tutto quello a cui ha accennato.

Poi, delle idee mie me le sono fatte, e ne avevo scritto ogni tanto anche qua, ma proprio per questo, al momento non posso dire di più del libro. Rischierei di “fargli dire” cose mie, invece che sue. :)

**[Gianluca Bartalucci][6]**
_on 21 gennaio 2013 at 12:50_

“Mi sembra di capire che il punto di vista sia del tipo: sì, il cervello è indispensabile perché emerga una coscienza, ma il cervello senza corpo e ambiente, non potrebbe far emergere una coscienza, perché essa è un fenomeno dinamico che scaturisce dall’interazione con corpo e mondo.”. Non ho letto il libro, ma ho letto di gente che l’ha letto, e in effetti mi sento di concordare con questo punto di vista.

In un certo senso SIAMO il nostro cervello, che filtra, memorizza, rielabora e reagisce agli stimoli esterni e interni. Nell’altro senso NON SIAMO il nostro cervello, per i motivi che spieghi.

**Masque**
_on 21 gennaio 2013 at 14:02_

Ma nel primo senso, siamo il cervello allo stesso modo in cui siamo le mani, il cuore, gli occhi, oppure in un senso più “speciale”? Cioè, usando i termini di John Searle nel distinguere fra mente umana e “stanza cinese”: è proprio il cervello ad avere quei “poteri causali” che ci dovrebbero distinguere da una macchina priva di volontà e di esperienza cosciente?

**[Gianluca Bartalucci][6]**
_on 22 gennaio 2013 at 12:48_

Chiaramente il cervello non ha potesi speciali e, partendo da vincoli genetici, opera in continuità con la natura, con quello che c’è fuori di esso. Il “primo senso” è, in effetti, una sorta di comodo e forse un po’ impreciso modo di sintetizzare la cosa.

Forse non siamo il nostro cervello, ma siamo la nostra mente: che è un’entità che comprende il cervello ma che va oltre esso, si trova negli hard disk dei nostri computer, nei cervelli delle persone che conosciamo, nei memo che lasciamo sulla scrivania etc etc

Comunque su emule (mi pare) c’era una sorta di breve conferenza (tradotta in italiano, se non ricordo male) che Noe ha tenuto in Italia.

**Masque**
_on 22 gennaio 2013 at 14:36_

So che un anno fa era stato al festival della filosofia a Modena. Ricordo che volevo andare a sentirlo, ma alla fine avevo rinunciato. Forse il filmato arriva da lì.

Il punto di vista alla Hofstadter è, al momento, quello che mi piace di più. Però mi sento un po’ attratto ed incuriosito dall’enattivismo. :)

**[Gianluca Bartalucci][6]**
_on 28 gennaio 2013 at 23:03_

C’entra marginalmente, ma leggendo questo aforisma di Borges ho pensato a simili argomenti:

“I am not sure that I exist, actually. I am all the writers that I have read, all the people that I have met, all the women that I have loved; all the cities I have visited.”
— Jorge Luis Borges

**Masque**
_on 28 gennaio 2013 at 23:30_

Bella. Mi piace! Proprio sabato ho avuto uno scambio di battute con un amico che la pensa in modo esattamente opposto. Tanto che ritiene di poter, da solo, arrivare a qualsiasi teoria o idea che altri hanno già espresso, e rifiuta quasi sempre di leggere ciò che scrivono gli “esperti”, perché non vuole farsi influenzare. Quindi, in qualche modo ammette l’influenza (per lo meno, non scade completamente in un ingenuo solipsismo) ma, secondo me, non si rende conto che è comunque costantemente influenzato. Non viviamo in una cultura che è aliena a termini come “rete neurale” (l’argomento da cui era partita la conversazione) e gli studi già fatti sull’argomento, per non dire del termine stesso, che è in grado di descriversi in modo molto efficace, hanno influenzato anche la cultura di massa, dalla quale è quasi impossibile sottrarsi. Ma evitare di informarsi ulteriormente, ti porta a trascurare molte idee sull’intelligenza e sulla mente, che non sono così intuitive come può essere la banale imitazione di un meccanismo biologico esistente.

**[D Garofoli][7]**
_on 6 febbraio 2013 at 10:10_

“Quindi, in qualche modo ammette l’influenza (per lo meno, non scade completamente in un ingenuo solipsismo) ma, secondo me, non si rende conto che è comunque costantemente influenzato.”

Se é vero che la mente é situata, la cultura in cui uno si sviluppa potrebbe essere costitutiva anche di aspetti “profondi” dell´esperienza cosciente. Cioé, se il caso dei Piraha é vero, la cultura potrebbe scolpire il modo stesso in cui noi “guardiamo alla realtá”, rendendo ció che ad esempio esula dall´esperienza presente privo di significato.

Un problema dell´embodiment radicale secondo me é il rischio di soggettivismo. Io ad esempio ho assistito ad una lezione di Humberto Maturana, in cui veniva messo in dubbio il significato di frasi come “sii realista”. Anthony Chemero su questo tema dedica un intero capitolo del suo libro su ER, proprio per salvare questo approccio da darive solipsiste.

**Masque**
_on 6 febbraio 2013 at 19:03_

Però è strano… Se l’enattivismo è come l’ho inteso io e, man mano che leggo il libro, mi pare che sia il modo in cui lo intende anche Alva Noe, mi sembra strano che si possa arrivare ad una posizione soggettivista. Se la mia mente è ciò che emerge dall’interazione fra il mio corpo ed il mondo (quindi anche altri soggetti), come posso sottovalutare proprio “l’esterno”? In un capitolo del libro, si parla proprio del problema delle altre menti e del rapporto con il mondo. Si dice che gli individui condividono un mondo fisico, ma che la percezione di questo mondo condiviso differisce in base a diversi fattori. Vengono fatti esempi di comportamenti di popolazioni diverse di scimmie. Tuttavia, proprio per il fatto che il mondo fisico è condiviso e comune, non si può affermare che la percezione di esso possa cambiare così radicalmente a seconda del soggetto. Soprattutto se affermi che la coscienza del soggetto stesso emerge dall’interazione con quel mondo comune.
Anche dal punto di vista dei termini, mi viene più facile affiancare il soggettivismo ad un approccio internalista e riduzionista, piuttosto che olistico.

**[D Garofoli][7]**
_on 7 febbraio 2013 at 09:09_

Ora non mi ricordo esattamente qual é il fulcro del dibattito sul soggettivismo. Peró a pensarci su due piedi mi viene in mente una cosa di questo tipo. Se io e te abbiamo una rappresentazione mentale di ció che percepiamo, questo in poche parole significa che abbiamo scattato una foto della realtá circostante, la quale diventa cosí rappresentazione qualitativa nella nostra mente. Se la foto é scattata con la stessa macchina fotografica (occhi umani), é ragionevole pensare che sia la stessa per entrambi.

Se invece la percezione é diretta, il mondo é direttamente una fotografia su cui uno agisce. Percezione é azione, nel senso che per percepire un triangolo noi dobbiamo agire su di esso esplorandolo attivamente e testandolo nella realtá esterna. Ora il problema é che, anche se l´ambiente esterno é uguale, potrebbe darsi che io e te agiamo su di esso in maniera diversa. Quindi la nostra percezione del triangolo dovrebbe essere diversa.

Io credo che il problema potrebbe essere questo. Pensiamoci, poi magari con piú calma me lo rivedo.


[1]:https://en.wikipedia.org/wiki/Alva_No%C3%AB
[2]:https://it.wikipedia.org/wiki/Esternalismo
[3]:https://neuroantropologia.wordpress.com/2012/10/20/embodiment-radicale/
[4]:https://neuroantropologia.wordpress.com/2012/08/20/azione-nella-percezione/
[5]:http://dottornomade.com/
[6]:https://some1elsenotme.wordpress.com/
[7]:https://archaeocognition.wordpress.com/
