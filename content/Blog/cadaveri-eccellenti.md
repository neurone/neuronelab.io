title: Cadaveri eccellenti
date: 2013-12-22 10:42:11 +0100
tags: nonsense
summary: Questa è una collezione di frasi, scritte in compagnia di amici, molti anni fa. Originalmente raccolta su scontrini, tovaglioli, fazzoletti di carta e quanto capitasse a tiro nelle serate alcoliche dei nostri vent'anni.

Questa è una collezione di frasi, scritte in compagnia di amici, molti anni fa. Originalmente raccolta su scontrini, tovaglioli, fazzoletti di carta e quanto capitasse a tiro nelle serate alcoliche dei nostri vent'anni.

Ognuno, a turno, scriveva un pezzo di frase. La frase o "poesia" terminava
quando ci sembrava completa. In linea generale, non c'erano regole fisse.
Per la maggior parte sono frasi senza senso, fatte per far ridere ed infarcite
di numerosissime fantasiose parolacce. :)

Solo in seguito, scoprii che alcuni surrealisti facevano circa le stesse cose,
sia in forma di testo che grafica e che le chiamavano [Cadaveri Eccellenti][1].


Scivoloso pensiero,  
lacrime basiche  
dissolvono anime meccaniche  
intrecciate con margherite invisibili.  
Schiuma diabolica nascente  
impalpabile ma radicata… cado  

Assorbo le anime dinoccolate dei nuclei simpatici  
ove cacca si sgranchisce mentre granchi si scaccolano.  

Pesce scusami se tramonto emettendo turbolenze psichiche  
Sto male pigiando pustole dorate imbrattate col desiderio ancestrale  
Porco cane!  

Sfuggo coalizzandomi  
Stucco impalandomi  
profonadamente piango

Peli riarsi, liquidi corporei fagocitano stantuffi stufi  
Gufi depressi pressano nullatenenti cristalli  
Maiali galli serpeggiano

Trasudo pensieri analcolici sfiorando con la mente diversificazioni argentate  
ossidate nell’orrido.

Valanga introspettiva rigurgitante azoto morto  
incespicante ostacolo putrescente inneggiante merda!!!

Ingranaggio minchiolo  
arrugginisce sadness  
dentro un intestino corroso in latte  
al mandarino Fly

Nobile imbianchino disinibito dai suoi uccellini  
vorrebbe rotolare assieme alla nonna  
dicendole: “Toh!”

Celeste marmellata  
né luce incrostata  
in te

Un arrotino caduto dall’ostrica viennese pagana  
riposa sciogliendo grumi fallici  
Novantesimo fuoco muschiato incurante  
Gioia nebbiosa in blood!

Il cauterizzare fiorellini implica omosessualità

Noralmente infetto la mia sifilide attaccabrighe  
sperando di sublimare la sua sfacciataggine

Barcollante seguendo un giorno buio  
precipito nel Mar Merditerraneo

La Solitudine Gialla si mescola alla pioggia battente  
bussando con veemenza alla porta del mio scroto!

Il Topo grida col culo

Getto peli pubici ai matrimoni

Scorro parole intestinali

Siedo in oleose barelle cannibali

Seduco soffocando pèti

Fulmini cadono  
Eustachio!!!

Guido castelli in discesa

Risuonano silenziosi “fanculo”

Muco dal buco

Rapidamente rutto “cioao”

Sento minacce intestinali

Distanziato mi avvicino

Guardandoti mi è venuto vomito

Poso nudo eretto

Spingo segmenti bugiardi

Schiavizzami bradipo col pollice

Garbatamente faccio cagare

Attendo per pensare

Sento ruggiti anali

Oggi guardo cadere il mio pisello

Il corpo umano resiste di più sognando vitamine

Tempo, libertà, afide narcisista spende milioni in liquirizia e poi soffoca

Anfetamina supposta inside soffia rincagnata mordendo villi personaggi molli

Tosto mi devasto i coglioni

Ingollare cocometi è virile!

Il pus invade il colon di animaletti trasparenti liquidi  
ma solidificano senza sosta annichilendosi plastica frigida

Sole, gufo, morte

Visioni rettali infinite

L’esito rutta senziente

Io penso lassativo

Oggi risciacquo insalata incenerita

Follicolo turgido vorrebbe sentirlo eretto

Capitolo senziente, frustate pulcini mutandari!

Arguisco fluidi tristi fluirmi dentro l’ano

Pellicano nano muore sano

Fondamentalmente gratto scroti

Mi dipingo eiaculando

Poesia d’amore: Labbra screpolate… che fatica.

Guardo siempre capiezzoli

Percorrendo sentieri collosi, giro ingranaggi minchioli sentendomi fecondo.  
Arrivo!

Buio sentiero trasmettimi il meteo

Capto arbitrariamente cazzate alate

Sbrego cazzi coi cavatappi

Si vomitano batuffoli ghiaiati quanto piove ruggine

Sono piegato in cucina: apri würstel da penetrazione

Pannolini intrisi giocano con vecchietti palmipedi stitici

Fumandomi canarini svengo

Gioisco infilandomi preposizioni lì

Instauro logica raggomitolata

Pulviando gnale riduco limtestri inavacc

Ganfo artusi millitrillini cuccioli orgipi lucidi plastici

Liquidi universi nel cesso ristabilizzano umori di vacche che volano nelle boazze ricoperte di smarties.  
Solidi vapori matematici in putrefazione ricorrono ciclicamente cinque giorni al chilometro.  
Molto scoreggioso catatonicamente inebrianti figata nipponica svolazza sogghignando paperino frocio, pippo sadomaso col masso gran sasso tasso grasso
rosso. Passo e chiudo

Importato califfo gusterebbe figa spray causa malfunzionamento prostata infarcita cremosa nucleare.  
Stufo vita carcere, appesantito dai topi gatti persiani scuri recalcitranti evacuerebbe cazzetti sotto vuoto scotti!

Le ninfe mangiano bimbi integrati sopra l’utero di polifemo.

Porca scrofa! Mi sono soffocato grattando crosticine gialle!!

Cigno pois trenette di ragù più latte rancido se mi sgancio missili vuoti gorgoglianti andando avanti?

Se starnuto un ossobuco mi consumo  
il pattume mi opprime orco cane  
schiaccio palle col badile  
urlo cazzo, porca bile!  

Il mondo  
Oceano lacrimante di pettirossi infuocati mi grava sulle pustole  
Vecchia megera frullami passerotti grondanti marsala.

Fuoco statico dall’inferno mi inghiotte la memoria.  
Argentee rivelazioni scorrono fluide attraverso di me.

Ciospe mutanti mangiano pece come fosse rabbia forsennata

Mostro galattico, che culo!

Caustico brodo ingurgito meditando fiori putrefatti di vaselina ruvida  
funghi sporroni sotterranei luminosi sporgono spauriti spargendo tagliatelle
gravide.

Amuleto peloso tinto pisello  
negazione esistenziale  
centrico me  
pece  
porcella racchia squillo.

Svarionato viaggiatore  
tette sante cariche di stelline spente  
muco del mulo trainante norvegesi ubriachi

Selezione frenetica muggisce forte canto giallo bandiera  
disturbato io, disturbato tu  
piango filamentosi punti ingannati dal naso…

Pattumie regalate incessantemente non portano malattie… sfiga!

Mondo rotondo! Bondola sperpera grasso giallo  
camicia collata  
cavoli che arrabbiati totano

Sesso gocciolante indurisce polipi frenetici yoga.

Palline quadrate spigolose aiutano proprietari di scatolette pastorizzate a  
incastrasi ove criceti… sptchuf!!

Orologi spugnosi disseccati cercano mamme cicce, sorelle di nonni adottivi per capire l’ora.

Vivo sdraiato con spillini arrugginiti quando sento scricchiolare coccigi…
soffro!!

Ricordo fiorellini bui sanguinare linfa sporca

Piangevano lacrimoni ghiacciati trafiggendo stambecchi sottili di porfido nano  
Sulla putrescenza sfioravano lavoratori sciocchi.  
…Perché scappa la bimba?

Centrini, chilogrammi, fragoline barate suggellano cugini arroganti fottetevi!  
…per favore Zio gabbiano scavami intrinseche intriganti elefantesse.

Vuoto la merda da quando conobbi tua madre!  
Zoccolara a Trento, tu mi arruffi pensieri vacui… Bacillo!

Ruota universi equivocando Satana pistolino mentre mastico rimasterizzato luce sciolta  
selezione precoce mammella zuccherata impastricciata analmente amalgamata

Shiva summon LeBon  
Cadaveri invitanti scopano santi pupille  
Perpetua risalendo elettriche carnagioni brucianti

Piango sangue precoagulato  
abbraccio lingue ruvide scrivendo chewingum

Felce morta storta piange poverina  
che diarrea concimata s’arrampica spuzando… amen!

Luminoso lampadato ventottenne scalerebbe cavalloni scalabili esente posticcio  
pasticcio osservante conservante pasticcini gravidi

Fuoco scorre animando burattini stellari morti annegando nel sentiero soave.

Filippo nudo gratta culo ‘n’tera.

Rincorro meduse secche, fracasso e godo.

Sfintere: Interstizio doganale insito in culo.

Piantagione sculacciata misericordiosamente mise ricordi suffragati su fregate  
inglobate nel mondo caparbio.

Anale vibrazione cosmica mi percuote siempre quando sono sul water.

Incido orrendo sigillo tramite flatulenze orrende sigillate, appunto, tenendo  
fermo l’ano.

Fumè, se volè.

Prepuzio lacrima bulbi senza zucchero diabetico secernendo calcoli  
esponenzialmente anali aventi diritti civili.

Io no, però, occasionalmente, turgido.

Scappa scrofa sul tetto che scotta.

Ciapa ‘ste careghe nonna!

Vorticosamente rubo fiori fatui

I giorni scivolano su me  
come rondini cagano su te

Internamente godo di sforzi marroni

Stantuffi o vieni?

Crevelletto buono, perchè assorbo flatulenze invece di espellerle? Non è che sei  
andato a male?

Romantico il mio culo canta dopo pranzo sentimenti marci.

In principio illumino cadaveri sul water, successivamente li eviro e tengo botta.

Farfallina agonizzante della tua morte gioisco.

Vedova a genova, tichettio imperituro mi s’insinua zitto zitto deflagrandomi nell’ano.

Sono ingordo di relè bastardi come te non ce n’è.

PornoMinchia è il mio eroe preferito.

Culo o non culo? Chiederò la mio papà che nella mischia ce lo rischia…

Morto fui quando lame tranciarono zucchine intrappolate nelle sorprese vagine.

Sbigottito mi confondo adducendo patetiche rimostranze fintanto che spingo…  
fatta!!

Clorofillami d’immenso. Seguendo intersezioni paludose mi scazzo abbastanza molto…

Cupido scorreggia anche lui

Fottiimax è il nemico di PornoMinchia

Fronde di capezzoli sommergono orizzonti fulgidi punendomi severamente

Fallisco craccando noccioline, Milioni di Tarzan m’ingoiano compiaciuti il seme della vita.

Io esagono, tutti repettano tranne me, che esagono.

Apocalittica mucca purgami cantando

Sei tu che mi rompi le balle divorandomi le suddette balle.  
Attenta che mi gratto pulcino frullato tocchi zaldoni.  
Supersadichecca non avrà mai una parte.

Adesso ti dobbiamo togliere le tue cose

Archibugio salve! Mi presento, sono sempre io medesimo stesso fesso lesso miao miao zik zak!

In principio fotto pecore audaci nel loro sedere, alla fine escogito parole anticoncezionali. Maglioni di sperma.

Erudito nel naso

Vai piccolo scapaccione, lascia il tempo in pace arrogante sacco di pus ti salto e tiro vino.

In piedi funghi stagliati contemplano l’infinito, passa un glande saggio cinese  
ansioso dei propri piedi… Che schifo, se li strofina sul gluteo emorroidato e io  
godo dei miei criceti.

Frullato incisivo. Mi convince

Rugiada, dove sei? E i miei calzini? Scompiscio in uno scomposto elefante vaginale.

Tempo fa roteavo vigili agli incroci.

Ferruginoso muco, grazie degli auguri, oggi è il mio testosterone siciliano che  
al mercato s’è incollato l’inculato giovincello ma che bello, ma che bello.

La Notifica Pèti è illegale, qui ci vorrebbe “Tappen sie Deutch”

La notte borbotta stronzate, la mattina si pente.

Vorticando tassi, invoco Odino, ma lui mi sacrifica un testicolo sull’altare del  
riccio ingrifato. Allora, visto che è bastardo, gli inserisco tubicini mutanti  
nun succidi niunti.

Sagoma colui che mangia sagome le quali mangiano Colui.

L’amore calcolato:  
Un piccolo calcolo cerca l’amore trillullando; una grande parentesi tonda batte  
il quadernino di quella roia di Pulviscolo Frolloccone.  
Trillullare stufa assai, meglio sobobare sucando numeri complessi digerendoli  
poi convessamente.  
La digestione unisce i popoli.  
S’incontrano? Chi? Dove sono?  
Morti, ma felice di sobobarsi l’ano trillullando.

Ansia e Fastidio sono i figli di Pornominchia

Sgambettoso bue partorisce finti conigli e si chiede se fa bene

Concludendo la pipì: E’ passato asa’ tempo… sgoccio

Iride ha le cornea ma non c’entra niente Pornominchia

Vivo di stenti, prenderò il treno per venire a troverti

Un quadro a righine:  
Sul pianeta Sfrangiacazzi, felice correva Ginevra Zozzona tra i fallici arbusti.  
Sodomite ninfee scrotavan nel laghetto, un pesce sobobava triste e solo  
nell’angolino.  
All’improvviso fu il caos tondo tondo che stette, messo alle strette  
dall’impotente mostro marino Cippì.

Petronilla studia i cachi e i cachi studiano

Esegesi della Panasokia, soffro quando carta vetrata fa break-dance sul mio pens.

Corna svitabili a volte salvano il rapporto.

Hai vinto uno spregevole inconveniente.

Carmencita vende la sua … farcita di puntine che quando colano grasso come Slimer sbrodola bava brava.

Io moccio nel calzino, faccio il nodo e butto via.

Lamù la da strupando…No!! Però la da

Ho un sacco di piastrine, liberiamo Barabba!!

Tette spaziali: mia nonna le ha. Mi offuscano il telescopio

Una voglia neutronica mi sprona a metabolizzare cosmici pubi, ma sono imbarazzato dalla vita in giù.

Brucio Chivavva Bonito!

Interrompo il coito causa rumori molesti.

I nani non nanano, i puffi non puffano, dove sono?

Di la tua parola dai: sesso sesso!

Reali mobilitano virgole in piazza sperando galeoni di emmental immaginari.

Io ti ammazzo! Ci ripenso e mi oblitero la cappella.

Granchio aviatore vorrebbe ammarare di cellule mestrue con punte dolci dolci ma grave sifilide s’accomodasse purè.

Fuochista cyborg incarbona diametralmente teenagers aummaumma solo fuck-tronic-led-zigulì-of–emotions-of-steel!

Broncopolmonitoso Gigi mi manchi i piedi miei non ti darò soddisfazione a tu x tu al cubo slimonando tarde fuori i peti!

Ahimè muco, perché rifiuti l’occhio gas come fosse pattume?

Tipicamente forte succhio  
cado privilegiato  
gusto maluccio brioche zigrinate in pillole  
l’hai cesso  
riempio supporto cadaveri banzai

Torpiloqui vivo tardivo pulivo  
mi vo zozzo ma fine mondo lore sì sì sì

Piccolo marinaio intorpidito  
dai che muoricchi senza dita inneggiando capitano uligano culo

Scoiattoli libici dal plutonio, o mio signore

Sono profondo come ano  
porco cano

Ballo brecdanze numeriche power tristi ma tematiche

Frau Potato incatenatrice dei solidi ginnici, fuggi!  
arriva Masto Dildo de la Peña de Ruzem col sombrero in su la capèla

Poche mone saltano mortali parabolando vane scorregge afro  
solamente Blues Ematoma ne gode beffardo e ci mette del suo

Tutti guardano guahiti ma non io  
la critica è gravida di piccoli ipocloriti ma molto incazzati negri e tronfi di  
crema solare ai crauti

Cencio Ocho Meno Uno col bastom te mena per l’aia viziata dell’odor del pube di zio Mimì  
Con le mani sbucci cazzi a scodelle bionde e perverse uguale sangue siempre  
ploch momola pirole de sterco de quel bom però.

Ugo il peloso entra di soppiatto dappertutto senza bussare  
perché ha il silenzio incorporato proprio lì dalla nascita

Non penso di avere caccole abbastanza per fare la torta

Ho vinto 12 scartavetrate sull’usello… Porko giuda!

Cesure è il mio vicino di casa. Salutandolo perdo il pube.

Deflagrami con parabole psichiche mentre la bruma ziga travisando liquidi fosforescenti.

Idilliache grandi labbra sussurrano “quella troia virtuosa s’insinua sempre laddove neanche il buio osa”

Obliando manticore ignude falliche, garantisco.

Tipico idiota sdrazza dibbrutto detriti sincopati nel parco giochi offrendoli T.P.C.

È ora di sbadigliare.

Flaccido nado raccoglie sedimenti arcani fumenti alcalino alcui alcuni  
alchimisti che al mio ginocchio rotola rotola lo rifà sgambettando mi sciupa al  
che stronzi nocciolati raccontano bestemmie rotanti. Saluti da Rimini.  

Cagherò sangue postmerdam.

L’influsso infranatico dalla montagna sorge copioso ai temi cretini rispondo con  
la otto sita nel buco cavo infranatico.

Ero eretto nel mio piccolo: Tracce d’orgoglio dopo l’andropausa.

Al mercato del pèto, per due soldi un azarbaigian mio padre gli demolì la Panda.

Ranocchia storpia colui che viene nel nome del signore

Mai scorderai pèti strèti contràti a 2 a 2 sfuggitimi dallo sfintere reverso

Polsando montagne s’incollano trenta langi di javellotti

Fosfi e Tacci

Balbuzie intestinale incurabile

La roccia fria  
ma non è mia  
mangerò gamberetti a colazione nel bronx

Mirtillo il fusillo passeggiando quà e là, s’imbatte bel bello s’incasina l’uccello  
“Cos’è successo quà sotto?!” esclama Mirtillo.  
Intanto il druido Guttalax sghignazza nell’ombra.  
Mirtillo rinviene, ma è morto!

La nocciolina triste, di un gheriglio abbisogna, suo cugino gaio se lo mena col mortaio.

Astrometalnatica incorpora lo sfrantegaballe dei mondi. È più grosso Pasticciometalciuccio anale, ma non compete con Tezcatlipokemon De La Carbeza O’ Hara/O’ Connor in Brambilla.

- Mi capita di avere visioni celestiali guardando nelle tue narici…  
- Deve essere la somma flatulenza.  
- Io penso che sia amore non corrisposto dal basso verso l’alto  
- Dovresti comprare dei francobolli intestinali  
- Che figata! Così avrei dei villi collezionisti!

Dai! Non fare il neurone che se ne sta lì per conto suo e soffre delle sue pene mentre la vita attorno a lui scorre caoticamente divertita!

- Posso farti la cacca sulla schiena? Di?  
- Solo se poi la passi con cura tra vertebra e vertebra  
- Ho portato apposta il compressore!  
- ??? …ma tu sai che cos’è la cacca?  
- No…


- Vuoi che te la faccia vedere?
- Ho sentito i fenicotteri giocare a morra!  
- Ma tu esisti o sei un’entità?  
- Io Fosfo  
- Io Andrea  
- La fanghiglia putrido-violetta mi dice che se sta facendo presto, credo che  
  andrò alle terme meno un quarto a farmi un quanto di struzzo in autobus.  
- Penso che la vita la regalano proprio a tutti.  
- Scusa Gianpipetto, ma mi chiamano sull’altra via maestra e non.  
- Bene Sveltobraccio… sono contento che ti sono venuti i tortellini di manzo  
  di mare… Quando me li farai assaggiare?  
- Quando saranno cresciuti abbastanza da attraversare la strada da soli.  
- Di a Tarzan che l’amo!

Flebo di liquame erutta sempre letame  
Vorrei tanto lordume su per l’albume  
Fringuellino immondo extraterrestre  
Scavalca figa rupestre

Cogliendo grappoli scrotali colmi di libido, stupro sassolini mollando slinze iridescenti  
Coprofilia a me!  
Le spengo col culo.

L’Anzichenecchi è un mio compagno di casse. (era un beccamorto)

Pantalassa di grassa non ingrassa, ma sposta massa nei punti del corpo che prediligo.

Il seme è qualcosa che mi preme.

Inzaccherare mi ricorda la mano destra.

Secrezioni melliflue canticchiano motivetti anarchici sul ponte dei sospiri.  
Non so più se me lo tiri, verificherò con una martellata.

Sono pinocchio: me lo svito e lo tiro nel fuochio.

Furia gastrica stermina dirompente la flora batterica di Nando.  
Fringuellino c’ha già i vermi, coito da raptus a bolle lungo lungo.  
Bunjee Giampa ha visto tutto e fa rapporto all’Agente Patogeno ma si  
inciampa sul cadavere di Nando che però vive nella memoria di Fringuellino tombarotolante.  
Bunjee Giampa è gay e si soffia il cervello dal naso rotto dall’urto. Che singulto!  
Arretra Franklin da quel disastro, cade con le chiappe su un osso quindi  
spira e ammira al buio nel buco del culo del Gigante Giacomino che muore pian pianino.  
Esalo l’ultimo respiro scrivendo ciò…

Non senti il divanetto tremare?  
No, non sento il dirompere anale…

Quando mi tira non riesco a piegarmi in avanti. Ho lo sterno sfondato,  
perciò mi puntello.

Faccio cagare i sassi riempiendo di limo il letto del Nilo che si sveglia e  
colaziona con il Muesli di merda.

Sartorta si rosicchia, sacripante sarchiapone.

Culo coprofago come periferica di input-output

Vengo soppressato lunedì, martedì esco venendo ben schiacciato dal mio  
orgoglio animaletto.

Perdo dignità come se piovesse nelle mutande!

Obnubilando Liutprando si pente dell’evirazione attuatasi sulla nuova Lucilla che in realtà fumava ornitotteri nella càneva si fotteva col suo ex coso.

Scorgo scaloppine parcheggiate storte in seconda fila alveolo muore dentro.

Arcuato pene in morsa sente Intercity arrivare.

Eros Ramazzotti è made in Finland.

Quando lunedì attraverserò rigurgitando gerundi impossibili pensando  
biecamente la mia vita fuori dal tempo, sentirò a malincuore per telefono  
ornitorinchi invecchiati male che sputano sentenze nei miei occhi aperti con  
le terga.

Meditando nel fosso sono già morto occasionalmente.

Collimo giacendo, sentirò dolore.

Catodo penzolante uccide nel fosso sistematicamente formichine annegando  
nella merda fino alle caviglie.

Ho adocchiato libidinosamente la mia pinguedine stantia, la frittata rotola,  
la cipolla è patogena.

Stefano si crogiola nell’empatica passera, solitario sta manualmente abile.

Acqua, ingurgito pipì fagocitando popò con sforzo mi diletto e merito (un applauso).

Tirando pugni sul mio pacco  
beatamente mi rilasso tronfio,  
le mutande umide insacco
nel mio ventre gonfio.

Iodio saturo rutto farfalline di primavera tutti tocca Masini. (accompagnare con gesto apotropaico)

Furore giramondo scruta l’orizzonte mentre io mi tergo dalla spruzzata della diarreica fine.

Fuochi fatui fanno finta, ego edonista estirpa entità di dionisiche diatribe didascaliche: “dì Fifì”.

Pusillanime nella cloaca sdràza contorcendosi tira su il moccolo secreto e spegne la candela con un peto.

Carciofetto mellifluo, dove stai andando intonso? In caso, se ti scappa lo stronso mettiti d’impegno e fallo di legno.

Intromesso, senza niente, erutta cioccolata da ogni parte.

Oh, tira su tutto lì… anche lì… e lì… oh, lì no!

Più giù ci vai tu barbablu io rimango qui in giarino sloizo come il moscerino morto nell’oceano delle brocche di Robinolosà.

Gli elefanti con gli hard disk bevono cioccolata col limone.

Comincio io che mangio nani bolliti e lucertole ingrifate senza pensare ad apostrofi rossi, sempiterno mestruo bolscevico.

Mediceo antico personaggio scureggiante di sofoclee perturbazioni infiocchettandola.

Ciccio, l’hai detto dopo avermi guardato  
sognando ad occhi aperti  
Scroto senza indugio zompetta eucalipti  
panda felice orsacchiotto pirletta is dying in flames!

Peti musicali echeggiano in rapida sequenza  
scandendo endocrinologi istruiti  
pomposi colpi che svelano cicliche intestinali.

Ohibò, venni baroccamente obsoleto or ora davanti  
e dietro cinguettii lubrificati body body poty poty  
il dolore ciuf ciuf stantuffando piselli giganti  
grandiosi e naturali inside by me mamma.

Tende e pende a est.

Premurosi peti verso me  
sussurrano lussuriose semolate e zuccherate nozioni appiccicose  
venute copiosamente topine truccate alè!

Quando ero verde, puttana!  
Varie giuggiole provavano libidine gargantuesca  
biancastra barba folta  
sprizza da tutti i pori inguinali.

YouTubero rigetta spumeggiante una quantità di bollicine olezzose e  
madrelungua sassone rotoloso brivido felino-suino mel magno col Carlo.

Mina sotto zero! BRRR le mille bolle BRRR!

Simpatia serpeggiosa lunga e coinvolgente penetra ani mali atroci umidi.

Elastico emostatico dinamico interviene sciabordando free villi intestinali  
birilli sexy en zità moscia se ne va emettendo piripiripiripà  
rumori stomachevoli che sorgevano montagne verdi intestinali là.

Quando stanco canguri saltellando cittemmuorto percepisco me stesso  
dopodomani morto.

Fuffi gira la ruota tanto per condizionare l’usignolo turgido  
non è detto forse sì che tutti esauriti siamo.

Precipitando al contrario mi reincarno tutto storto. Ce l’ho corto ma con la messa a terra.

Svanisco nel mio culo e contemplo verdi pascoli di flora intestinale.

Cosa fa Lena? Fugge laddentro rigurgitando fiorellini coccigei. Imparerei dai rubinetti a pisciare diritto. Domani cresceranno funghi.

La vita mi feconda il gomito.

Possono darmi un po’ di gnocca senza discoteca?

Satana mangia polenta.

Aderisco alle troie.

Bruttacciona macina peti! T’incendio le scorregge, ti lavo i denti col Black&Deker e ti trivello l’organo!

Ieri sgusciavo pinguini scartavetrandomi l’alluce e oggi sbriciolo forfora sgnecca sgambettante.

[1]:https://it.wikipedia.org/wiki/Cadaveri_eccellenti
