title: Buddhismo anarchico?
date: 2015-06-30 19:47:39 +0200
tags: anarchia, anatropologia, religione
summary: Riporto questo articolo di *Federico Battistutta* pubblicato sul numero 359, febbraio 2011, di A – rivista anarchica. Si parla dei punti di contatto fra buddhismo e ideologia anarchica.

Riporto [questo articolo][1] di *Federico Battistutta* pubblicato sul numero 359, febbraio 2011, di A – rivista anarchica. Si parla dei punti di contatto fra buddhismo e ideologia anarchica.

Per altri approfondimenti sulle relazioni fra anarchia e religione, vi rimando a
questo articolo della Anarcopedia, ed a quelli ad esso collegati: [Anarchismo e religione][2].

*Quale rapporto può sussistere tra un dottrina politica sviluppatasi all'interno
del movimento operaio e socialista nell'Europa dell'800 e una corrente religiosa
sorta in India nel VI secolo a.C.?*

Un'affermazione paradossale
---------------------------

Accostare anarchismo e buddhismo può apparire un'operazione azzardata. Che
rapporto può sussistere tra una dottrina politica sviluppatasi all'interno del
movimento operaio e socialista nell'Europa dell'Ottocento e una corrente
religiosa sorta in India nel VI secolo a. C.? È vero però che entrambe sono,
rispettivamente, qualcosa di più di un pensiero politico e di uno religioso;
cosicché, se considerate come ‘visioni del mondo', hanno qualcosa da dire anche
al di fuori degli ambiti all'interno del quale sono nate.

![Buddhismo anarchico]({filename}/images/buddhismo_anarchico1.jpg){: style="float:right"}

Partiamo da un'affermazione che potrà apparire paradossale: il buddhismo può
essere considerato an-archico nella misura in cui rifugge dall'affermazione
circa la sussistenza di principi primi. Ma nei fatti è così: l'insegnamento di
Buddha poggia su due parole-chiave: *anicca* e *anatta*. Il primo termine
significa ‘impermanenza', e afferma che non vi è condizione immutabile per ogni
essere e per ogni fenomeno, ma solo un flusso in continuo divenire. Il secondo,
indica l'inesistenza di un io individuale e permanente. Entrambi sono la
negazione di due principi fondanti della tradizione brahmanica dell'India. Non
solo: il buddhismo è anche a-teo, non perché neghi a priori l'esistenza di uno o
più déi (la negazione sarebbe a sua volta l'affermazione di un principio forte,
sia pure in forma negativa), ma nel senso che prescinde dalla questione
teologica. Il nucleo della ricerca e dell'insegnamento del Buddha riguarda la
vita dell'uomo e il suo dolore. (1)

Questo tratto squisitamente an-archico della via di Buddha è così marcato che
alcuni studiosi occidentali hanno dimostrato più di un'incertezza nel
considerare il buddhismo come una religione.
<!--more-->

La via di Buddha
----------------

Notiamo anche che le parole soprariportate *anicca* e *anatta*, come anarchia,
presentano il prefisso *an–* (che noi chiamiamo ‘alfa privativa greca'). Ora la
lettera A, prima ancora di essere il simbolo per eccellenza del movimento
anarchico, è anche un vero e proprio emblema buddhista, al punto da essere
descritta come “la saggezza suprema in una sola lettera”: non è soltanto la
prima lettera dell'alfabeto, che indica perciò l'inizio della comunicazione
linguistica, ma è soprattutto il prefisso privativo da apporre a tutte le
nozioni che vengono negate dalla perfetta saggezza della visione di Buddha
(*prajñaparamita*), in quanto considerate come parole che rimandano a enti privi
di una realtà propria.

Allora, sostenere l'impermanenza di ogni essere, sostenere l'inesistenza di
un'anima imperitura, sostenere la non rilevanza di una fondazione teologica,
reca con sé come immediata conseguenza la negazione di ogni principio di
autorità. C'è un testo minore nei discorsi di Buddha, il *Kalamasutta*, rivolto
cioè ai kalama (nome di un popolo o di un clan). Poiché molti predicatori si
erano recati presso questa gente e tutti avevano proclamato la verità della
propria dottrina, negando valore agli altri insegnamenti, così fu posta a Buddha
la seguente domanda: come possiamo distinguere la verità dalla menzogna? La
risposta fu tagliente: “Non fatevi guidare da dicerie, tradizioni o dal sentito
dire. Non fatevi guidare dall'autorità dei testi religiosi, né solo dalla logica
o dall'inferenza, né dalla considerazione delle apparenze, né dal piacere della
speculazione, né dalla verosimiglianza, né dall'idea ‘questo è il nostro
maestro'. Ma quando capite da soli, o kalama, che certe cose sono salutari e
buone allora accettatele e seguitele”. Ma, a questo proposito, particolarmente
noto è l'ultimo discorso di Buddha, in cui si dice: “siate lampada per voi
stessi, prendete rifugio in voi stessi e non in altro”.

Nel buddhismo zen (una corrente sorta in Cina, poi diffusasi in Giappone, su cui
mi soffermerò maggiormente) c'è un detto famoso: “Se incontri il Buddha,
uccidilo!” È un'affermazione spiazzante, priva di un significato a priori
preciso: forse non ne ha alcuno, forse ne ha centomila. Sicuramente c'è la
denuncia del pericolo di credere in un modello di perfezione fuori di noi – il
Buddha – a cui aderire supinamente. Ma, ci viene suggerito, tale modello non
esiste, è solo un idolo creato dalla nostra mente, ottenuto invertendo di segno
ciò che noi percepiamo come ‘negativo'. Uccidere Buddha può voler dire
rivolgersi finalmente a ciò che c'è, per conoscerlo così com'è, senza volerlo
cambiare ad ogni costo ma in maniera del tutto astratta; questo però è anche
l'unico modo per creare quello spazio in cui il cambiamento possibile può
davvero accadere. (2)

Tra obbedienza e ribellione
---------------------------

Ma, a differenza dell'anarchismo, il buddhismo non esprime in senso stretto una
dottrina politico-sociale, è principalmente una via di conoscenza concernente il
‘mestiere di vivere', il cui obiettivo è la liberazione dal dolore inerente al
fatto stesso di esistere.

Inoltre, sebbene il buddhismo si sia trovato spesso in posizioni controcorrente,
nel momento in cui si è dato una struttura gerarchica e istituzionale, è finito
in più di un frangente per colludere coi vari detentori dell'ordine costituito,
così come in Occidente è accaduto per il cristianesimo. Come esempio di quanto
si sta dicendo si può menzionare uno studio storico, assai documentato, compiuto
da Brian Victoria – un monaco buddhista zen di nazionalità americana e vissuto
molti anni in Giappone – nel quale si evidenzia il supporto dato dai buddhisti
giapponesi a cinquant'anni di guerre di aggressione da parte dell'Impero del Sol
Levante, facendo affiorare in modo purtroppo inconfutabile i rapporti tra il
buddhismo zen giapponese dei primi del Novecento e gli ambienti del nazismo
tedesco e del fascismo italiano. (3) Questo testo, aldilà degli intenti
dell'autore che sono in primo luogo di ricerca e di documentazione storica, è
assai importante per comprendere e denunciare i pericoli che qualsiasi religione
corre (e fa correre alla società nella quale prospera), nel momento in cui si
istituzionalizza e decide di sostenere le scelte politiche di uno stato. I
valori religiosi non possono essere imposti né per legge, né con l'uso della
forza, pena la loro completa decadenza da qualsiasi relazione con la ragion
d'essere della stessa religione.

Detto questo, ci sono eccezioni che paiono voler riscattare il buddhismo
giapponese dalle pagine oscure della sua lunga storia, riconducendolo a quella
matrice anarchica che si trova alla sua origine. Come la vicenda di Uchiyama
Gudo (1874-1911), della scuola Soto zen, coinvolto nelle vicende di alto
tradimento del 1910, che vide coinvolti, oltre lui, alcune decine di
anarco-socialisti. La riprendiamo dal testo di Brian Victoria.
Uchiyama Gudo, ordinato monaco nel 1897, presso il tempio Hozo-ji, divenne in
seguito abate di Rinsen-ji, tempio situato in una regione rurale e montuosa del
Giappone. Questo tempio era piuttosto modesto ed era luogo di incontro religioso
per la comunità contadina. Dialogando con i giovani del villaggio Gudo si fece
portavoce di una riforma agricola che riequilibrasse l'ingiustizia sociale
generata dalla proprietà della terra da parte dei latifondisti.
Gudo sosteneva che l'autentica esperienza monastica buddhista si fondava su una
comunità in cui tutti, compresi i monaci di rango elevato, vestivano allo stesso
modo, mangiavano lo stesso cibo, lavoravano la stessa terra. Non solo: per Gudo
questo modello di vita fondato sull'eguaglianza poteva essere diffuso all'intera
società. Il suo modello sociale era quindi l'antico sangha (comunità) buddhista
con il suo stile di vita comunitario, privo di qualsiasi forma di proprietà
privata.

Nel 1904 Gudo ebbe modo di conoscere il periodico anarco-socialista “Heimin
Shimbun”, verificando così la corrispondenza tra le sue idee e quelle propugnate
da questo giornale. Ciò lo condusse alla consapevolezza di essere pure lui un
anarco-socialista; a questo punto, decise di avviare una stretta collaborazione
con questa rivista, per mezzo della quale ebbe modo di rendere pubbliche le sue
convinzioni sull'assonanza tra i principi del buddhismo con quelli
dell'anarchismo e del socialismo. Ma ben presto, nel 1905, il giornale dovette
chiudere su ordine delle autorità. Ciò non ridusse al silenzio il movimento di
protesta: proseguirono i tumulti e le manifestazioni, anche contro la guerra
russo-giapponese in corso proprio in quegli anni. Dopo che i partiti di
opposizione vennero messi al bando, impossibilitati a svolgere qualsiasi
attività legale, alcuni decisero di passare all'”azione diretta”, simile a
quella dei movimenti anarchici europei, progettando un attentato contro la
famiglia imperiale.

In questo periodo Gudo si attrezzò di una tipografia clandestina, collocandola
nel proprio tempio, dietro l'altare del Buddha. Diede così alle stampe e diffuse
numerose pubblicazioni anarco-socialiste e una sua opera. Ma il 24 maggio 1909,
mentre era di ritorno dal tempio di Eihei-ji, venne arrestato, inizialmente con
l'accusa di aver violato le leggi sulla stampa. In seguito, la polizia dichiarò
di aver rinvenuto nel suo tempio materiali per costruire ordigni esplosivi.
Così, dopo l'incarcerazione di altri oppositori per alto tradimento, Gudo e i
suoi compagni vennero processati e giustiziati, per aver complottato per
assassinare l'imperatore, nel 1911. Due anni prima, i funzionari del Soto zen lo
avevano radiato dall'ordine monastico.

Malatesta e il monaco zen
-------------------------

![Misato Toda]({filename}/images/buddhismo_anarchico2.jpg){: style="float:left"}
Quanto al rapporto fra buddhismo zen e anarchismo, sempre in Giappone, merita
segnalare il più recente contributo di Misato Toda, una docente universitaria
giapponese, studiosa assai competente del pensiero di Errico Malatesta, la quale
ha poi allargato i suoi interessi a personaggi dell'antifascismo quali Piero
Gobetti, Carlo Rosselli e Camillo Berneri. Misato Toda, tenne una conferenza
sull'anarchismo dal punto di vista del buddhismo zen, presso l'Istituto
Orientale di Napoli, nel 1983, in cui vengono presentati con sapienza e levità i
punti di contatto – così come le differenze – tra anarchismo e buddhismo,
citando e raffrontando i detti di Malatesta e di Dogen Zenji, uno dei caposcuola
della corrente zen giapponese. Secondo l'autrice, buddhismo e anarchismo, intesi
come modi di volere, di vivere e di pensare, non sono in contraddizione: “Non
possiamo sfuggire al nostro destino, che ci impone di vivere con gli altri e di
morire da soli”. Si può dire che questa semplice frase compendia bene il
possibile incontro fra questi due mondi apparentemente lontani, che consente di
toccare in un medesimo abbraccio sia la nostra condizione di animali sociali
alla ricerca di una vita degna di essere vissuta, sia il rapporto intimo e
diretto con sé stessi. (4)

In America
----------

Non è un caso, quindi, se proprio il buddhismo zen – che ha cercato di
realizzare nella sua forma più radicale (nel senso di saper andare alla radice)
la testimonianza lasciata da Buddha – ha attratto autori occidentali con marcate
sensibilità libertarie.

È il caso di Gary Snyder, poeta, saggista statunitense, che prese parte
all'esperienza della beat generation ed è stato uno dei più seri studiosi del
buddhismo all'interno di questa corrente. (5)

![Gary Snyder]({filename}/images/buddhismo_anarchico3.jpg){: style="float:right"}

Trascorse parte degli anni tra il 1956 e il 1968 in Giappone, studiando e
praticando zen in alcuni importanti monasteri a Kyoto. Ma egli è anche un
bioregionalista e un attivo critico sociale. Questo duplice interesse viene
esplicitato in uno scritto del 1961, ripubblicato con alcune modifiche alcuni
anni dopo, anche se prese di posizioni analoghe sono riscontrabili in altri suoi
testi. Il titolo è eloquente: *Anarchia buddhista*. (6)

In esso possiamo leggere questa frase che ci riporta alla precedente citazione
di Misato Toda, così come alla vicenda di Uchiyama Gudo: “il dono dell'Occidente
è stato la rivoluzione sociale. Il dono dell'Oriente è stato la visione
individuale che penetra la natura fondamentalmente vuota dell'universo interiore
ed esteriore. A noi servono entrambe le cose.” Qui Snyder propone una sintesi:
da un parte c'è la tradizione del movimento operaio, gli scioperi, le proteste e
le rivendicazioni comunitarie e sociali; dall'altra c'è la compassione scaturita
dal riconoscimento della natura impermanente di ogni cosa. Una non è la
negazione dell'altra, anzi queste possono integrarsi e sostenersi
vicendevolmente. Ed è su tali premesse che forse può crescere e svilupparsi il
buddhismo in Occidente. Con le parole di un testo classico del buddhismo
tibetano: “La nozione del nulla genera la pietà. La pietà abolisce la differenza
fra sé e gli altri. Il confondere sé con gli altri realizza la causa altrui.
Colui che realizza la causa altrui mi troverà. Colui che mi avrà ritrovato sarà
Buddha” (7).

**Federico Battistutta**

Note
----

1. Su questo si è soffermato in maniera esemplare Raimon Panikkar, nel volume
   *Il silenzio di Buddha*. Un a-teismo religioso, Milano, Mondadori, 2006.
   L'ateismo di Buddha non viene qui descritto come una contrapposizione alle
   varie forme più o meno surrettizie di teismo nell'affermazione razionalista
   della non esistenza di Dio, bensì è l'espressione di una religiosità
   finalmente purificata da quell'ombra di idolatria che spesso ottenebra le
   religioni.
2. Sempre su quest'onda, dirà un importante caposcuola del buddhismo cinese:
   “Non crediate che il Buddha sia il fondamentale. Dal mio punto di vista è
   proprio come il buco della latrina”. La raccolta di Lin-chi, a cura di Ruth
   Fuller Sasaki, Roma, Ubaldini, 1985.
3. Brian Victoria, *Lo zen alla guerra*, Dogliani (CN), Sensibili alle foglie,
   2001.
4. Il testo originale della conferenza si trova presso il CIRA (Centre
   International de Recherches sur l'Anarchisme) di Losanna. Di Misato Toda, in
   lingua italiana, cfr. *Errico Malatesta da Mazzini a Bakunin*, Napoli, Guida,
   1988, oltre ad alcuni interventi apparsi sulle pagine di “A”.
5. Sul rapporto tra beat generation e buddhismo cfr. anche: Jack Kerouac, *Il
   sogno vuoto dell'universo. Saggi sul buddhismo*, Milano, Mondadori, 1996, e
   Alan W. Watts, *Beat zen e altri saggi*, Roma, Arcana, 1973.
6. Gary Snyder, *Anarchia buddhista*, in *L'altra America degli anni Sessanta*,
   a cura di Fernanda Pivano, vol. I, Roma, Officina, 1971. La nuova versione
   del testo in questione s'intitola: *Il buddhismo e la rivoluzione che verrà*,
   si trova in Gary Snyder, *La grana delle cose*, Torino, Gruppo Abele, 1987.
   Sempre al buddhismo zen si è accostato anche Ken Knabb, un
   anarco-situazionista americano; ne parla nelle pagine autobiografiche di un
   ponderoso volume che raccoglie diversi suoi scritti. Cfr. Ken Knabb, *Public
   secrets*, Berkeley, Bureau of public secrets, 1997. Dello stesso autore, in
   italiano, si può leggere: *Due saggi critici sul buddhismo impegnato*, da me
   tradotto su “La stella del mattino”, n.4/2003.
7. *Vita di Milarepa*, a cura di Jacques Bacot, Milano, Adelphi, 1966.

[1]:http://www.arivista.org/index.php?nr=359&pag=80.htm
[2]:http://ita.anarchopedia.org/Anarchismo_e_religione
