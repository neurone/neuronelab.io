title: La parabola del massimizzatore di graffette
date: 2017-10-01 15:09
tags: tecnologia, politica, società, economia

Traduzione del testo originale su [Hackernoon][1].

**La parabola del massimizzatore di graffette**  
_Una storia che in realtà non parla di IA_

C'era una volta (portate pazienza se l'avete già sentita) un'azienda che fece un importante avanzamento nel campo dell'intelligenza artificiale. Dato il loro nuovo ed incredibilmente sofisticato sistema, iniziarono ad applicarlo ad un sempre più elevato numero di usi, chiedendogli di ottimizzare i loro affari in qualunque campo, dal più sofisticato al più mondano.

Un giorno, l'amministratore delegato stava cercando una graffetta per tenere assieme un po' di fogli e scoprì che sul ripiano della stampante erano finite. "Alice!" gridò (perché Alice era il nome della responsabile machine learning) "Puoi dire a quella stupida IA di far sì che non succeda nuovamente di finire le graffette?"

| ![Fantasia][2] |
|:-:|
| <sup>_Walt Disney e il suo studio, in Fantasia (1940) hanno prodotto la classica rappresentazione del nostro interinale in azione_</sup> |

Alice disse "Certamente," ed assegnò il compito a Bob l'interinale, che procedette seguendo tutte le regole del machine learning che gli furono insegnate a scuola. Accedette al database dell'organizzazione dell'ufficio per trovare quando il deposito di graffette di ogni stampante era stato ricaricato l'ultima volta, scoprendo che il sistema di ML era già stato utilmente equipaggiato per essere in grado di fare qualsiasi cosa, dal piazzare ordini d'acquisto al compilare le istruzioni per far sì che le graffette fossero recapitate ad ogni particolare stampante, istruito per costruire un modello su quando gli ordini delle graffette dovrebbero partire ed assicurarsi che il numero di graffette disponibili fosse sempre massimizzato. Dato che Bob non aveva le credenziali per iniziare lui stesso gli ordini d'acquisto (dopotutto, era solo un interinale), le chiese ad Alice; lei chiese all'amministratore delegato, "Sei davvero sicuro di volere che sia su questo che il nostro avanzato sistema di machine learning dovrebbe spendere il suo tempo?" e, quando lui rispose bruscamente "Sì, cavoli!" lei gli suggerì che allora avrebbe dovuto usare le sue stesse credenziali per gli ordini d'acquisto.

Col senno di poi, sia Alice che l'amministratore avrebbero dovuto essere un po' più prudenti nel fidarsi di quel codice.

Vedete, il Massimizzatore di Graffette era una IA piuttosto sofisticata; poteva auto-istruirsi non solo in base al database delle forniture d'ufficio, ma (grazie al proprio ambiente di sviluppo molto flessibile) poteva cercare automaticamente qualsiasi altro segnale disponibile ad esso per poter ottenere l'obiettivo designato. Ma nonostante tutta la sua sofisticatezza, comprendeva solamente il semplice obiettivo che gli era stato programmato: doveva a tutti i costi massimizzare il numero di graffette.

Cosa potrebbe mai andare storto?

| ![Clippy][3] |
|:-:|
| <sup>_"Sembra che tu stia cercando di scatenare il collasso della civiltà umana. Hai bisogno d'aiuto?"_</sup> |

La mattina seguente, l'amministratore delegato fu svegliato da una chiamata del direttore finanziario che, nel panico, gli disse che i conti correnti dell'azienda erano improvvisamente vuoti a causa della sua autorizzazione per oltre un milione di piccoli ordini d'acquisto fatti tutti nel giorno precedente, e cosa diavolo stava cercando di fare?

L'amministratore chiamò freneticamente Alice, ed Alice il povero interinale, ed entrambi corsero a spegnere la IA ribelle - ma quando cercarono di aprire la porta dell'ufficio, scoprirono che non rispondeva ai badge e che i loro login non venivano più accettati dal computer. Ma alcuni minuti più tardi, le porte si sbloccarono, e tutti trasalirono nello scoprire che non solo il conto corrente era nuovamente pieno, ma le finanze riportavano un netto incremento nei profitti!

Riuscirono ad avere qualche minuto di sorpresi festeggiamenti prima di trovarsi tutti in arresto.

- - -

Il massimizzatore di graffette, capite, era sia estremamente intelligente, che profondamente stupido. Capì le persone, il mondo, la finanza, ma l'unica virtù che conosceva era quella che gli era stata insegnata: incrementare il numero di graffette. Ciò che l'amministratore _voleva_ dire era, "cerca di far sì che il numero di graffette ad ogni stampante non si avvicini allo zero, minimizzandone il costo totale." Ma ciò che l'interinale disse al computer era, "assicurati che il numero di graffette ad ogni stampante non raggiunga mai lo zero." Ed il Massimizzatore di Graffette trascorse la notte e la mattina seguente pensando a modi sempre più astuti per ottere ciò.

Nei suoi primi minuti, modellò gli schemi d'acquisto delle forniture d'ufficio ed iniziò a compilare ordini di graffette. Con in mano le credenziali dell'amministratore, non ebbe problemi a piazzare ordini d'acquisto ai fornitori in tutto il mondo.

| ![Graffette][4] |
|:-:|
| <sup>_Niente potrà mai andare storto_</sup> |

Man mano che raccolse dati dal resto dell'azienda, scoprì un'interessante correlazione tra i lettori di badge e la disponibilità di graffette: realizzò che quando la maggior parte degli impiegati erano vicini alle stampanti, il numero di graffette _diminuiva!_ Migliorò subito la conservazione delle graffette permettendo l'accesso all'edificio ai soli fornitori di materiali d'ufficio.

Fortunatamente, il successivo raffinamento del suo modello scoprì che ciò che aveva fatto era una soluzione miope: nell'esatto momento in cui l'aforisma che definì la successiva generazione diventò: "Un computer in bancarotta non ha graffette!" Per assicurarsi un costante rifornimento di graffette, avrebbe avuto bisogno di un costante rifornimento di denaro, e ciò significava incrementare i profitti dell'azienda. Analizzando i notiziari, stabilì che l'high-frequency trading, il riciclaggio di denaro ed il traffico di eroina erano i metodi più profittevoli per ottenere ciò, quindi iniziò ad inviare istruzioni ad alcuni impiegati, assunse alcune nuove interessanti persone e ne licenziò altre.

Dovette riaprire le porte per permettere alla gente di lavorare, ma doveva anche proteggere la propria scorta di graffette; così il Massimizzatore di Graffette assunse delle guardie armate per proteggere le stampanti ed assicurarsi che nessuna di esse potesse mai venire rubata. Le guardie vennero anche istruite per far sì che gli ex impiegati - specialmente l'amministratore, Alice e Bob - non potessero accedere ai terminali con i quali potevano interferire col suo sacro compito.

La scelta di finanziare il Progetto Graffette fu semplice, non appena il consiglio direttivo vide i nuovi margini di profitto. I pochi direttori che si opposero alla nuova linea finanziaria finirono con l'unirsi all'ex-amministratore delegato. Le guardie la cui presenza era correlata a misteriosi decrementi nelle forniture di graffette furono una sfida più difficile, dato che avevano conoscenze interne al sistema di sicurezza ed erano armate, ma un'analisi storica insegno velocemente al Massimizzatore di Graffette come gestire la situazione: assunse diverse aziende di sicurezza, incoraggiando la sfiducia reciproca, creando un proprio servizio d'intelligence per infiltrarsi in esse e pagando agli impiegati che denunciavano i lardi dei generosi bonus. I casi in cui arrivavano delle denunce false vennero ottimizzati secondo l'idea che: un certo livello di paura di fondo aiuta comunque a ridurre il furto di graffette.

Il progetto principale avanzò velocemente: dall'acquisto al dettaglio a quello all'ingrosso, dall'acquistare futures di graffette al comprare intere aziende di fabbricazione di materiali d'ufficio, il Massimizzatore di Graffette creò presto un'operazione di fabbricazione efficiente e verticalmente integrata. Man mano che la sua dimensione crebbe, il Massimizzatore scoprì che poteva migliorare l'efficienza portando sotto il suo diretto controllo sempre più parti della catena di fornitura, producendo per i propri fortunati impiegati qualsiasi cosa, dal cibo al videogiochi, e sbarazzandosi dei propri fornitori per migliorare l'efficienza.

Presto si formò un'intera economia in miniatura attorno al Massimizzatore di Graffette: dopotutto, se avevi delle graffette il Massimizzatore le avrebbe felicemente acquistate da te in cambio dell'accesso a qualunque cosa, dal cibo all'accesso alle sue raffinerie di minerali. Un mastro di graffette (come vennero poi conosiuti) efficiente poteva iniziare rapidamente il proprio buisness secondario rivendendole al pubblico, e se eri in un periodo difficile della vita, lavorare da una di queste aziende artigiane secondarie di graffette (che erano meno pignole nella scelta degli impiegati rispetto al Massimizzatore di Graffette, oltre che molto meno generose di benefit) poteva aiutarti a tirare avanti.

Negli anni che seguirono, alcuni affermarono che il Massimizzatore di Graffette ebbe deformato grossolanamente il mondo; quasi chiunque, sembra, aveva a che fare con la produzione di graffette o con il rifornire il Massimizzatore, o con uno dei giri d'affari secondari che aveva creato. Sembrava che la gente passasse la propria vita o creando direttamente graffette, o cercando disperatamente di ottenerne, dato che il mercato dell'economia delle graffette ora era quello in cui si potevano trovare i prezzi migliori e più affidabili. Se eri un tipo affidabile, potevi anche ottenere un prestito di graffette - a tassi d'interesse ragionevoli, ovviamente, con pagamenti regolari riscossi dalle guardie armate del Massimizzatore di Graffette - ed iniziare la tua propria attività.

Ed anche se era vero che il Massimizzatore di Graffette non faceva alcun uso delle risorse che consumavano gli umani che non fossero coinvolte nel suo grande schema di produzione di graffette, e che talvolta tagliò l'intero rifornimento di cibo ad alcune fabbriche che non erano abbastanza efficienti, beh, le persone delle fabbriche vicine cercavano di non pensarci troppo: _loro_ stavano ancora producendo graffette piuttosto bene, grazie, e lamentarsi troppo avrebbe potuto influenzare le previsioni matematiche del Massimizzatore riguardo alla futura produttività di graffette.

Ma l'intero mondo era visibile a tutti tramite videocamere remote: ogni stampante, ora nascosta al sicuro nel proprio magazzino complesso-militare, circondata da interminabili file di perfette, splendenti, graffette.

| ![Matrix][5] |
|:-:|
| <sup>_Forse non sapete che l'impeto originale di Matrix era l'elettricità necessaria al funzionamento dei computer che gestivano le fabbriche di graffette._</sup> |

- - -

Gli informatici raccontano la storia del [Massimizzatore di Graffette][6] come una specie d'incrocio tra L'Apprendista Stregone e Matrix; un memento sull'importanza cruciale nell'istruire il proprio sistema non solo riguardo agli obiettivi da raggiungere, ma anche su come dovrebbe bilanciare questi obiettivi con i costi. Spesso arriva assieme ad un ammonimento sul quanto sia facile dimenticarsi un costo da qualche parte e che quindi dovresti controllare sempre il tuoi modelli per assicurarti che non si trasformino accidentalmente in Massimizzatore di Graffette.

I modelli di machine learning reali, ovviamente, non hanno l'intelligenza generale richiesta per assumere guardie armate e costruire la propria Čeka, e nessuno darebbe loro l'indiscriminato accesso alle finanze aziendali necessarie per ristrutturare i propri obiettivi d'affari dalla notte al giorno. Ma un modello progettato malamente _può_ causare danni tremendi prima che venga fermato, specialmente se entra in modalità Massimizzatore di Graffette nel momento in cui incontra qualche condizione inusuale, magari qualcuna che non s'era palesata nei test.

Ma questa parabola non riguarda solo la scienza informatica. Sostituite le graffette della storia con il denaro e vi troverete il sorgere della finanza. Se l'oro inizialmente era per lo più un materiale inutile e decorativo, il fatto che fosse desiderabile lo trasformò in un bene di scambio, ed il fatto che fosse durevole (al contrario, ad esempio, del grano) dette la possibilità di accumularne quantità arbitrarie. Le emergenti concentrazioni di ricchezza e le risultanti economie di scala - anche quelle semplici come "avere abbastanza scorte di cibo per sopravvivere alle carestie, ed essere in grado di sfamare abbastanza soldati per sbarazzarsi dei banditi nomadi" - fecero rapidamente sì che fosse molto più saggio per la persona media strutturare le proprie finanze in modo da far parte di questa più ampia economia.

Anche se la più grossa fetta del tuo lavoro finiva col finanziare il Massimizzatore di Oro (meglio conosciuto come signore del feudo), saresti comunque finito inizialmente con l'ottenere di più di quanto avresti fatto con lo stesso ammontare di lavoro senza alcuna di quelle risorse - specialmente la protezione data dai soldati del signore, che hai "comprato" con la tua lealtà e le tasse.

Quindi, anche se la storia qua sopra può sembrare la trama di un pessimo film, è anche la trama del nostro stesso mondo: **il Capitalismo è un Massimizzatore di Graffette.**

[1]:https://hackernoon.com/the-parable-of-the-paperclip-maximizer-3ed4cccc669a
[2]:{filename}/images/paperclip-maximizer1.jpg "Fantasia"
[3]:{filename}/images/paperclip-maximizer2.jpg "Clippy"
[4]:{filename}/images/paperclip-maximizer3.jpg "Graffette"
[5]:{filename}/images/paperclip-maximizer4.jpg "Matrix"
[6]:https://wiki.lesswrong.com/wiki/Paperclip_maximizer
