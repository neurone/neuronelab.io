title: In incognito in un macello
date: 2015-10-08 19:27
tags: società, psicologia, antropologia, veg, etica, violenza
summary: Della violenza nei macelli e nell'industria alimentare si sono già dette moltissime cose e penso che la questione sia sufficientemente chiara a chiunque non si rifiuti di prendere in considerazione i fatti conosciuti. Quello che mette in evidenza quest'intervista è un aspetto meno discusso e, a mio avviso, molto più interessante. Si parla infatti di come sia possibile che l'esercizio della violenza diventi accettabile e normale da parte delle persone che la operano direttamente e da chi, da questa violenza, trae beneficio.

Della violenza nei macelli e nell'industria alimentare si sono già dette moltissime cose e penso che la questione sia sufficientemente chiara a chiunque non si rifiuti di prendere in considerazione i fatti conosciuti. Quello che mette in evidenza quest'intervista è un aspetto meno discusso e, a mio avviso, molto più interessante. Si parla infatti di come sia possibile che l'esercizio della violenza diventi accettabile e normale da parte delle persone che la operano direttamente e da chi, da questa violenza, trae beneficio.

È un discorso che si presta ad essere allargato, come fa notare anche l'autore,
in altri ambiti al di fuori del caso del macello industriale qua evidenziato.

> La mia impressione era che, portare l'attenzione vicina a come viene operata
> l'uccisione industrializzata, avrebbe potuto non solo mettere in luce come la
> realtà del massacro animale viene resa tollerabile, ma anche come la distanza e
> l'occultamento operano in analoghi processi sociali: guerra eseguita da
> eserciti di volontari, il subappalto del terrore organizzato a mercenari, e la
> violenza alla base della fabbricazione delle migliaia di oggetti e componenti
> con cui veniamo a contatto nella nostra vita quotidiana.

I meccanismi che permettono questo, sono infatti utilizzabili efficacemente
come strategie politiche. Quando tempo fa mi chiedevo [cosa passasse per la
mente di chi vuol diventare
soldato]({filename}cosa-pensa-chi-vuol-diventare-soldato.md), intendevo capire
anche questo genere di meccanismi. La burocratizzazione della società, il
funzionamento interno dei governi, dell'esercito, sono anch'essi meccanismi che
portano ad ottenere i medesimi effetti. Si comprende, quindi, come sia
possibile accettare la violenza della guerra, di governi, delle forze
dell'ordine sia da parte di chi quella violenza la attua personalmente, sia da
chi ne trae beneficio.

> La lezione, qui, non è che la macellazione ed i genocidi siano moralmente e
> funzionalmente equivalenti, ma piuttosto che la violenza sistematica, su larga
> scala, resa routine, è del tutto coerente con il genere di strutture
> burocratiche ed i meccanismi che associamo tipicamente alla civiltà moderna.

Voglio introdurre quest'intervista mostrando due video divertenti. Il primo è
il famoso sketch dei Monty Python della [barzelletta mortale][1], il secondo è
una [candid camera][2] brasiliana segnalatami da [Rita][3].

Ho fatto personalmente la traduzione dell'intervista. Chi sapesse come
migliorarla, specialmente traducendo alcuni termini gergali in equivalenti
italiani, mi scriva i suggerimenti nei commenti. :)

Fonte: [boingboing][4]. Intervista di [Avi Solomon][5].

![Kill Floor 1]({filename}/images/in-incognito-in-un-macello1.png "Kill Floor 1")

[Timothy Pachirat][6], assistente professore in Politica alla *The New School
for Social Research* ed autore di *Every Twelve Seconds*, non è il primo a
trovare analogie fra la violenza industrializzata e politica, all'interno del
mattatoio. Ma invece di scrivervi una relazione, trovò un lavoro sul campo, per
capire il funzionamento dal punto di vista di chi lavora là. L'ho intervistato
sulla sua esperienza sul "piano del macello".

![Timothy Pachirat]({filename}/images/in-incognito-in-un-macello2.jpg "Timothy
Pachirat"){: style="float:right"}
**Avi Solomon**: Parlaci un po' di te.

**Timothy Pachirat**: Sono nato e cresciuto nella Thailandia del nordest, in
una famiglia Tailandese-Americana. Al liceo ho trascorso un anno nel deserto
delle zone rurali dell'Oregon, come *exchange student*, lavorando in un ranch,
coltivando alfalfa, e – inverosimilmente – diventando running back nella
squadra di football della scuola. Da allora, ho vissuto in Illinois, Indiana,
Connecticut, Alabama, Nebraska, e New York City lavorando come costruttore di
tralicci edilizi, fattorino delle pizze, terapista comportamentale per bambini
autistici, padre casalingo, studente diplomato, operaio di un mattatoio, e, per
gli ultimi quattro anni, come assistente professore di politica nella The New
School for Social Research.

**Avi**: Cos'è stato a suggerirti l'importanza di fare ricerca in campo
[etnografico][7]?

**Timothy**: Come molti ragazzi di razza mista, cultura mista, lingua mista, ho
sviluppato una specie di innata sensibilità etnografica, per via del complesso
terreno culturale nel quale sono cresciuto. Molto prima di aver mai sentito la
parola "etnografia", ad esempio, trascorsi le mie pause autunnali e primaverili
da non diplomato dormendo a fianco ed arrivando a conoscere gli uomini e le
donne senzatetto di Lower Wacker Drive a Chicago, come modo per trovare un
senso all'enorme disuguaglianza che percepivo nella società americana e nel
mondo. Mentre perseguivo un dottorato di ricerca in scienze politiche
all'università di Yale, mi è sembrato naturale gravitare verso un orientamento
nella ricerca che mi permettesse di impegnarmi con tutto il corpo – come
partecipe ed osservatore – con le esperienze vissute di persone con cui non
avrei mai potuto entrare in contatto altrimenti. Stavo imparando molte
fantasiose teorie che sulla carta erano eccitanti, e stavo imparando alcune
potenti tecniche di analisi statistica, ma solo l'etnografia mi ha permesso di
valutare quelle tecniche e concetti made-in-accademia contro le situate,
specifiche e meravigliosamente complesse esperienze vissute nei veri mondi
sociali, dei quali, i concetti e le tecniche si proponevano di descrivere e
spiegare.

**Avi**: Perché hai scelto di andare in incognito in un macello?

**Timothy**: Volevo capire come i processi di violenza di massa divengono
normali nella moderna società, e volevo farlo dal punto di vista di chi lavora
in un macello. La mia impressione era che, portare l'attenzione vicina a come
viene operata l'uccisione industrializzata, avrebbe potuto non solo mettere in
luce come la realtà del massacro animale viene resa tollerabile, ma anche come
la distanza e l'occultamento operano in analoghi processi sociali: guerra
eseguita da eserciti di volontari, il subappalto del terrore organizzato a
mercenari, e la violenza alla base della fabbricazione delle migliaia di
oggetti e componenti con cui veniamo a contatto nella nostra vita quotidiana.
Come nelle sue più evidenti analogie politiche – la prigione, l'ospedale, la
casa di cura, il reparto psichiatrico, il campo profughi, il centro di
detenzione, la stanza degli interrogatori, e la camera di esecuzione – il
moderno macello industrializzato è una "zona di confinamento", un "territorio
segregato ed isolato", nelle parole del sociologo [Zygmunt Bauman][8],
"invisibile", e "del tutto inaccessibile ai membri ordinari della società". Ho
lavorato come operaio di basso livello sul "piano del macello" di un mattatoio
industrializzato, per poter capire, dalla prospettiva di chi vi partecipa
direttamente, come operano queste zone de confinamento.

![Kill Floor 2]({filename}/images/in-incognito-in-un-macello3.png "Kill Floor 2")

**Avi**: Puoi dirci di più del macello in cui hai lavorato?

**Timothy**: Dato che il mio obiettivo non era di scrivere la descrizione di un
posto in particolare, non farò il nome del macello nel Nebraska in cui ho
lavorato, e neppure i nomi delle persone che ho incontrato là. Il macello
impiega circa ottocento lavoratori non sindacalizzati, la maggior parte
immigranti dall'America centrale e del sud, Asia sudorientale, ed Africa
orientale. Produce oltre 820 milioni di dollari l'anno in vendita e
distribuzione all'interno ed all'esterno degli Stati Uniti ed è classificato
fra i maggiori impianti di macellazione di bestiame al mondo per volume di
produzione. La velocità della catena di produzione sul piano del macello è di
circa trecento animali per ora, uno ogni dodici secondi. In una tipica giornata
di lavoro, qua vengono uccisi fra i duemila duecento ed i duemila cinquecento
animali, che sommati fanno più di diecimila animali uccisi per settimana
lavorativa (cinque giorni), o più di mezzo milione di animali macellati ogni
anno.

**Avi**: Quali lavori hai finito per fare, lì?

**Timothy**: Il mio primo lavoro consisteva nell'appendere i fegati nel
congelatore. Per dieci ore ogni giorni, mi trovavo alla temperatura di 1 grado
a prendere fegati appena estirpati da una linea sopraelevata per appenderli a
dei carrelli per essere raffreddati ed impacchettati. Poi venni trasferito agli
scivoli, dove conducevo i bovini vivi nella *knoking box*, dove veniva loro
sparato in testa con una [pistola a proiettile captivo][9] (NB: La Pistola
captiva è provvista di una punta di ferro di 6 cm che penetrando nel cranio
provoca un rapido stordimento, ma non uccide l'animale). Infine, venni promosso
ad una posizione di controllo-qualità, un lavoro che mi diede accesso ad ogni
parte del piano del macello e mi rese un intermediario tra gli ispettori
federali della [USDA][10] ed i manager del piano del macello.

**Avi**: Come ti sei acclimatato al lavoro?

**Timothy**: Lentamente e dolorosamente. Ogni lavoro arrivava con il suo
insieme di sfide fisiche, psicologiche ed emotive. Anche se era fisicamente
impegnativa, la mia battaglia principale nell'appendere fegati dentro il
congelatore era contro l'insopportabile monotonia. Scherzi, battute ed anche il
dolore fisico diventarono modi per negoziare con quella monotonia. Il lavoro
agli scivoli mi tirò fuori dall'ambiente freddo e sterile del congelatore e mi
obbligò a confrontarmi con il dolore e la paura di ogni singolo animale che
veniva condotto lungo la linea serpeggiante fino alla *knocking box*. Lavorare
al controllo qualità mi costrinse a padroneggiare un insieme di requisiti
tecnici e burocratici e mi rese complice nel sorvegliare e disciplinare i miei
ex colleghi alla catena di produzione.

![Every Twelve Seconds]({filename}/images/in-incognito-in-un-macello4.jpg
"Every Twelve Seconds"){: style="float:right"}

**Avi**: Come ti trattavano i colleghi?

**Timothy**: Non sarei mai potuto durare più di qualche giorno nel macello, se
non fosse stato per la gentilezza, l'accoglienza e, in alcuni casi, l'amicizia
del compagni di lavoro alla catena. Mi hanno mostrato come fare il lavoro,
salvato quando ho fatto casini, e, più importante, insegnato come sopravvivere
al lavoro. Eppure, c'erano divisioni e tensioni fra i lavoratori, in base a
razza, genere e responsabilità lavorative. Oltre a mostrare le forme di
solidarietà fra i lavoratori, il mio libro entra anche nei dettagli di queste
tensioni e come le ho affrontate.

**Avi**: Chi il "knocker"?

**Timothy**: Il knocker è il lavoratore che sta nella knocking box e spara
nella testa ad ogni singolo animale con la pistola a proiettile captivo. Dei
121 diversi lavori, all'interno del piano macellazione, che illustro e descrivo
nel libro, solo il knocker vede gli animali mentre sono ancora senzienti e da
loro colpo che si suppone li renda insensibili. In media, ogni giorno, questo
solo operaio spara a 2500 singoli animali, ad una frequenza di uno ogni dodici
secondi.

**Avi**: Chi altro è direttamente coinvolto nell'uccisione di ogni bovino?

**Timothy**: Dopo che il knocker spara agli animali, essi cadono su un nastro
trasportatore dove vengono incatenati ed issati su una linea sopraelevata.
Appesi per le zampe posteriori, viaggiano attraverso una serie di novanta
giravolte, che li porta lontani dalla linea visiva del knocker. Quindi, un
*presticker and sticker*(?) recide le arterie della carotide e la vena
giugulare.  Gli animali si dissanguano mentre continuano a viaggiare sulla
catena sopraelevata fino al *tail ripper*, colui che inizia il processo di
rimozione di parti del corpo e pelli. Degli oltre 800 operai nel piano
macellazione, solo quattro sono coinvolti direttamente nell'uccisione del
bestiame e meno di 20 hanno una visuale sull'uccisione.

**Avi**: Sei stato in grado di intervistare qualche knocker?

**Timothy**: Non sono stato in grado di intervistare direttamente il knocker,
ma ho parlato con molti altri operai riguardo a come essi percepiscono il
knocker.  C'è una specie di mitologia collettiva costruita attorno a questo
particolare lavoratore, una mitologia che permette un implicito scambio morale
nel quale solo il knocker è colui che fa l'uccisione, mentre il lavoro degli
altri 800 operai del macello è moralmente scollegato dall'uccisione. È una
finzione, ma convincente: fra tutti gli operai del macello, solo il knocker
porta il colpo che mette inizio all'irreversibile processo che trasforma le
creature viventi, in morti. Se ascoltaste con sufficiente attenzione le
centinaia di operai che eseguono le altre 120 mansioni nel piano macellazione,
questo potrebbe essere il ritornello che udireste: "Solo il knocker". È
semplice matematica morale: il piano macellazione opera con 120+1 mansioni. E
finché quel 1 esiste, finché vi è una plausibile narrativa che concentra il più
pesante e sporco dei lavori su questo 1, allora gli altri 120 operai del piano
macellazione possono dire, e credere, "Io non faccio parte di questo".

**Avi**: Quali sono le strategie principali usate per nascondere la violenza
nel macello?

**Timothy**: La prima e più ovvia è che la violenza dell'uccisione
industrializzata viene nascosta alla società in generale. Negli Stati Uniti,
oltre 8,5 miliardi di animali vengono uccisi ogni anno per farne cibo, ma
queste uccisioni vengono effettuate da una piccola minoranza composta per lo
più da lavoratori immigranti, che operano dietro a mura opache, per lo più in
luoghi rurali isolati, lontani dai centri urbani. Inoltre, delle leggi promosse
dalle industrie della carne e del bestiame, che sono attualmente al vaglio in
sei stati, criminalizzano il pubblicizzare ciò che avviene nei macelli o in
altre strutture con animali, senza il permesso dei proprietari degli stessi. La
Casa dei Rappresentanti dell'Iowa, ad esempio, lo scorso anno, ha inoltrato una
proposta al senato dell'Iowa che renderebbe reato la distribuzione o il
possesso di video, audio o materiale stampato, raccolto tramite accesso non
autorizzato a macelli o strutture con animali.

In secondo luogo, il macello, nel suo complesso, è diviso in compartimenti.
L'ufficio è diviso dal reparto fabbricazione, che a sua volta è isolato dal
congelatore, il quale è isolato dal piano macellazione. È del tutto possibile
trascorrere anni, lavorando nell'ufficio, nel reparto fabbricazione, o al
congelatore di un mattatoio industriale che macella oltre mezzo milione di
bestiame all'anno, senza neppure incontrare un animale vivo e tanto meno
assistere ad uno che viene ucciso.

Ma la terza, e più importante, il lavoro di uccisione è nascosto persino nel
luogo dove ci si aspetterebbe che fosse più visibile: il piano macellazione
stesso. La complessa divisione in mansioni e spazi, agisce allo scopo di
compartimentalizzare e neutralizzare l'esperienza del "lavoro di uccidere" per
tutti gli operai del piano macellazione. Ho già menzionato la divisione di
mansioni nella quale solo una manciata di operai, su un totale di oltre 800, è
direttamente coinvolta o ha una visuale sull'uccisione degli animali. Per dare
un altro esempio, il piano della macellazione è diviso spazialmente in un lato
pulito ed un lato sporco. Il lato sporco si riferisce a tutto ciò che accade
quando le pelli degli animali sono ancora attaccate ed il lato pulito a tutto
ciò che accade dopo che le pelli sono state rimosse. Gli operai del lato pulito
sono segregati dagli operai del lato sporco anche durante le pause per il
pranzo ed il bagno. Questo si traduce in una specie di compartimentazione
fenomenologica dove quella minoranza di lavoratori che si occupa degli
"animali" quando le pelli sono ancora attaccate, è tenuta separata dalla
maggioranza dei lavoratori che si occupa delle "carcasse" dopo che le pelli
sono state rimosse. In questo modo, la violenza del trasformare un animale in
una carcassa viene tenuta come in quarantena tra i lavoratori del lato sporco,
dove vi è, anche là, un ulteriore divisione di mansioni e spazi.

Oltre alle divisioni di spazio e mansioni, l'uso del linguaggio è un altro modo
per nascondere la violenza dell'uccidere. Fin dal momento in cui il bestiame
viene scaricato dai camion all'interno dei recinti di sosta, i manager ed i
supervisori del piano macellazione, si riferiscono ad esso come "carne". Anche
se sono esseri senzienti vivi che ancora respirano, essi vengono già
linguisticamente ridotti a carne inanimata, oggetti da usare. Similarmente,
esistono una sfilza di acronimi e termini tecnici in tutto il sistema di
controllo sicurezza alimentare, che riducono il lavoro dell'operaio di
controllo qualità ad un regime di tipo tecnico burocratico, piuttosto che ad
uno nel quale sia costretto il confronto con tutta quella massiccia sottrazione
di vite. Anche se l'operaio al controllo qualità ha piena libertà di accesso e
movimento per tutto il piano macellazione e vede ogni aspetto dell'uccidere, la
sua capacità interpretativa viene interdetta dalle richieste burocratiche e
tecniche del lavoro. Temperature, pressioni idrauliche, concentrazioni di
acido, conto dei batteri, e sterilizzazione dei coltelli diventano l'obiettivo
primario, al contrario della massiccia, incessante sottrazione di vite.

**Avi**: C'è qualcuno consapevole di queste strategie, fra chi lavora al
macello?

**Timothy**: Io non penso che qualcuno si sia seduto ed abbia detto,
"Progettiamo un processo di macellazione che crei la massima distanza fra ogni
lavoratore e la violenza dell'uccidere, in modo da permettere ad ognuno di essi
di contribuirvi senza dover affrontare direttamente la violenza". La divisione
fra lato pulito e sporco menzionata prima, ad esempio, è apertamente motivata
da una logica di sicurezza alimentare. Il bestiame entra nel macello ricoperto
di feci e vomito, e dal punto di vista della sicurezza alimentare, la sfida è
di rimuovere le pelli minimizzando il trasferimento di questi contaminanti alla
carne sottostante. Ma la cosa affascinante è che l'effetto di questa
organizzazione di spazio e mansioni, non solo aumenta l'"efficienza" o la
"sicurezza alimentare", ma anche la distanza e l'occultamento del processo di
violenza da parte di chi vi partecipa direttamente. Da un punto di vista
politico, un punto di vista interessato a comprendere come le relazioni di
dominio violento e sfruttamento vengono prodotte, sono proprio questi gli
effetti che contano di più.

![Kill Floor 3]({filename}/images/in-incognito-in-un-macello5.png "Kill Floor
3")

**Avi**: Le fabbriche della morte di Auschwitz avevano gli stessi meccanismi di
lavoro?

**Timothy**: Consiglio lo stupendo libro di Zygmunt Bauman, *Modernità ed
Olocausto*, a chi fosse interessato a come i meccanismi paralleli di distanza
ed occultamento funzionavano per neutralizzare il lavoro di uccisione che
veniva messo in atto ad Auschwitz e negli altri campi di concentramento. La
lezione, qui, non è che la macellazione ed i genocidi siano moralmente e
funzionalmente equivalenti, ma piuttosto che la violenza sistematica, su larga
scala, resa routine, è del tutto coerente con il genere di strutture
burocratiche ed i meccanismi che associamo tipicamente alla civiltà moderna. Il
sociologo francese [Norbert Elias][11] sostiene – in modo convincente, a mio
avviso – che l'"occultamento" e lo "spostamento" della violenza, piuttosto che
la sua eliminazione o riduzione, siano il segno distintivo della
civilizzazione. Dal mio punto di vista, la macellazione industrializzata
contemporanea fornisce un caso esemplare che mette in luce alcune delle
caratteristiche più salienti di questo fenomeno.

**Avi**: La violenza si trova nascosta anche nella più "normale" delle vite.
Come possiamo individuare questa presenza pervasiva nella nostra vita
quotidiana?

**Timothy**: Noi – con "noi" inteso come i relativamente ricchi e potenti –
viviamo in un tempo ed un ordine spaziale nel quale la "normalità" delle nostre
vite richiede una complicità attiva a forme di sfruttamento e violenza, che
altrimenti denigreremmo e disapproveremmo se le distanze fisiche, sociali,
spaziali e linguistiche che ci separano da chi subisce, finissero per
collassare. Questo è vero per la brutale e del tutto inutile detenzione ed
uccisione di miliari di animali all'anno per cibo, per lo sfruttamento e la
sofferenza degli [operai di Shenzhen][12], in Cina, dove vengono prodotti i
nostri iPad e cellulari, per le "tecniche d'interrogatorio avanzate" sviluppate
nel nome della sicurezza, per i "danni collaterali" causati dagli [aerei a
pilotaggio remoto][13] che le nostre tasse finanziano. La nostra complicità non
sta nell'infliggere direttamente la violenza, quanto nel nostro tacito accordo
di guardare altrove e non fare alcune semplicissime domande: "Da dove arriva
questa carne e come ha fatto a finire qua?" "Chi ha assemblato l'ultimo gadget
che mi è appena arrivato per posta?" "Che cosa significa creare categorie di
essere umani torturabili?" I meccanismi di occultamento ed allontanamento
insiti nel nelle nostre divisioni di spazio e mansioni e nell'uso sconsiderato
di linguaggi eufemistici rendono seducentemente facile evitare di perseguire le
complesse risposte a queste semplici domande, nonostante la nostra
determinazione.

Mesi dopo aver lasciato il macello, mi misi a discutere con una brillante amica
su chi fosse più moralmente responsabile dell'uccisione degli animali: chi
mangia la carne, o gli 800 lavoratori che operano l'uccisione. Lei sosteneva,
con passione e convinzione, che le persone che operavano le uccisioni erano le
più responsabili, perché essere erano quelle che eseguivano le azioni fisiche
che sottraevano la vita dagli animali. Chi mangia carne, affermava, è solo
indirettamente responsabile. A quel tempo, io presi la posizione opposta,
ritenendo che coloro che ne beneficiavano alla distanza, delegando questo
terribile lavoro ad altri e scaricandovi la responsabilità, avevano maggiore
responsabilità morale, specialmente in contesti come quello del macello, dove
coloro che nella società hanno meno opportunità, svolgono il lavoro sporco.

Ora sono più propenso a pensare che sia la preoccupazione della responsabilità
morale a servire da deviazione. Nelle parole del filosofo [John Lachs][14], "La
responsabilità di un'azione può venire passata, ma non la sua esperienza." Sono
molto interessato a chiedere cosa possa significare, per chi beneficia del
lavoro fisicamente e moralmente sporco, assumersi non solo parte della
responsabilità, ma anche farne esperienza diretta. Cosa potrebbe significare,
con altre parole, il collasso di alcuni dei meccanismi di allontanamento
fisico, sociale e linguistico che separano le nostre vite "normali" dalla
violenza e lo sfruttamento richiesti per mantenerle e perpetrarle? Esploro
alcune di queste domande in modo più dettagliato nel capitolo finale del mio
libro.

![Cinci Freedom]({filename}/images/in-incognito-in-un-macello6.jpg "Cinci
Freedom - A cow story")

**Avi**: Chi era Cinci Freedom? A quale scopo mitizzante serve?

**Timothy**: Apro il libro con la storia di una mucca che fuggì da un macello
lungo strada, fino quella dove lavoravo. La polizia di Omaha inseguì la mucca e
la mise all'angolo in un vicolo che confinava con il mio macello. Accadde
durante i dieci minuti della nostra pausa pomeridiana e molti degli operai del
macello videro la polizia aprire il fuoco con i fucili contro l'animale. Il
giorno successivo in mesa, la rabbia, il disgusto e l'orrore per l'uccisione
dell'animale da parte della polizia era palpabile, così com'era forte il senso
di identificazione con il trattamento dell'animale nelle mani della polizia.
Eppure, alla fine della pausa pranzo, gli operai tornarono al lavoro su quel
piano macellazione che ammazza 2500 animali ogni giorno.

Cinci Freedom era un'altra mucca [charolaise][15] che fuggì da un macello di
Cincinnati nel 2002. Venne ricatturata dopo svariati giorni solo grazie
all'aiuto dell'equipaggiamento di rilevazione termica fornito da un elicottero
della polizia. A differenza dell'anonima mucca di Omaha che fu abbattuta dalla
polizia, Cinci Freedom divenne immediatamente una celebrità. Il sindaco le
diede le chiavi della città e venne trasportata al *The Farm Sanctuary* a
Watkins Glen, NY, dove visse fino al 2008.

Anche se a prima vita le sorti della mucca di Omaha e di Cinci Freedom sono
state molto differenti, penso che entrambe le risposte fossero modi ugualmente
efficaci per neutralizzare la minaccia rappresentata da questi animali. Le loro
fughe dal macello non erano solo fughe fisiche, ma anche concettuali, momenti
di rottura della routine in un sistema di uccisione altrimenti automatizzato e
normalizzato. Lo sterminio e l'elevazione a celebrità (non dissimile dal
rituale della [grazia presidenziale][16] al tacchino del Giorno del
Ringraziamento) sono entrambi modi per contenere la minaccia rappresentata da
questi momenti di rottura concettuale. Essi mettono in evidenza anche il
limitare le rotture come tattica politica, ad esempio la rottura digitale che
avviene quando vengono resi pubblici dei video scioccanti girati sotto
copertura nei macelli e in altre zone di confinamento dove il lavoro della
violenza viene regolarmente effettuato per nostro contro.

![Kill Floor 4]({filename}/images/in-incognito-in-un-macello7.png "Kill Floor
4")

[1]:https://www.youtube.com/watch?v=IkYo9B8wM2M
[2]:http://laverabestia.org/play.php?vid=4135
[3]:http://www.ildolcedomani.com/
[4]:https://boingboing.net/2012/03/08/working-undercover-in-a-slaugh.html
[5]:https://boingboing.net/author/avi_solomon_1
[6]:http://polsci.umass.edu/people/timothy-pachirat
[7]:https://it.wikipedia.org/wiki/Etnografia
[8]:https://it.wikipedia.org/wiki/Zygmunt_Bauman
[9]:https://it.wikipedia.org/wiki/Macellazione#Stordimento
[10]:https://it.wikipedia.org/wiki/Dipartimento_dell%27Agricoltura_degli_Stati_Uniti_d%27America
[11]:https://it.wikipedia.org/wiki/Norbert_Elias
[12]:http://www.anarca-bolo.ch/a-rivista/?nr=357&pag=80.htm
[13]:https://it.wikipedia.org/wiki/Aeromobile_a_pilotaggio_remoto
[14]:https://en.wikipedia.org/wiki/John_Lachs
[15]:https://it.wikipedia.org/wiki/Charolaise
[16]:https://it.wikipedia.org/wiki/Giorno_del_ringraziamento#La_grazia_presidenziale
