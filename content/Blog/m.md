title: M
date: 2013-12-13 19:14:33 +0100
tags: film, recensioni, thriller
summary: Capolavoro di Fritz Lang del 1931, il primo di Lang ad avere l’audio. *M* è il mostro di Dusseldorf, un assassino di bambine che per mesi ha spaventato una città di più di un milione di abitanti. Imprendibile. Colpiva apparentemente senza alcuno schema, non lasciva indizi e non aveva segni visibili di squilibrio.

Capolavoro di [Fritz Lang][1] del 1931, il primo di Lang ad avere l’audio. *M* è il [mostro][2] di Dusseldorf, un assassino di bambine che per mesi ha spaventato una città di più di un milione di abitanti. Imprendibile. Colpiva apparentemente senza alcuno schema, non lasciva indizi e non aveva segni visibili di squilibrio.

Una persona assolutamente normale, fino a quando in lui non scattava
il meccanismo che lo rendeva un assassino di bambini. In questo film, dalle
atmosfere fortemente [noir][3], il mostro, *Hans Beckert*, è interpretato in
modo divino dal grande [Peter Lorre][4]. Talmente perfetto da mettere i brividi
con un semplice sguardo o una smorfia. Quando fischietta sommessamente per le
strade, o lo si vede cercare di controllarsi dopo aver scorto una bambina,
quando le si avvicina, o quando si sente braccato e agisce come un animale
feroce in trappola. Raramente ho visto interpretazioni così intense. Ogni sua
inquadratura fa percepire la sua imprevedibilità e gli sforzi che compie per
trattenere la ferocia.  
Il suo monologo finale, nel quale spiega la propria situazione e la propria
pazzia, è qualcosa che lascia senza parole.
<!--more-->

![M]({filename}/images/m.jpg){: style="float:right"}
Il film è considerato un capolavoro dell’[espressionismo][5] tedesco, assieme ad
altri come [“Il gabinetto del Dottor Caligari”][6] e non lascia nulla al caso.
Dalla psicologia dei personaggi, alle reazioni degli abitanti, spaventati da
questo sfuggente infanticida. In questo film viene mostrata tutte la gamma di
comportamenti che solitamente si manifesta in tali situazioni di crisi: la
paranoia per la quale qualsiasi persona vista in compagnia di un bambino viene
sospettata o addirittura accusata di essere l’assassino, l'impotenza delle
autorità di fronte ad un nemico sfuggente contro cui si prova a mettere delle
taglie, si propone di aumentarle, fare più retate e più controlli. È
interessante notare che, a distanza di quasi ottant’anni, le reazioni delle
persone siano le stesse. Basti pensare a quando appare nella cronaca un caso di
violenza su bambini. Oltre alle reazioni delle persone comuni, si vede anche
l’impatto che una scheggia impazzita come il mostro, ha nell’ambiente della
malavita organizzata. L’aumento dei controlli ad opera della polizia rischia di
mettere a repentaglio gli affari della criminalità, la quale si organizza per
scovare ed intrappolare l’assassino. Ma non finisce qui, perché il film offre
anche una precisa riflessione riguardo alla pena di morte, ed alla sua
applicazione contro le persone che non sono coscientemente responsabili delle
proprie azioni. Nel processo che viene svolto nell’ultima parte del film, si
sentono molte opinioni a riguardo. Le stesse, attualissime, che emergono
puntualmente riguardo alla pena di morte ed alla giustizia pubblica contro
quella privata. In definitiva, questo è un Signor Film, che consiglio a
chiunque.

[1]:https://it.wikipedia.org/wiki/Fritz_Lang
[2]:https://en.wikipedia.org/wiki/M_%28film%29
[3]:https://it.wikipedia.org/wiki/Film_noir
[4]:https://it.wikipedia.org/wiki/Peter_Lorre
[5]:https://it.wikipedia.org/wiki/Espressionismo
[6]:https://en.wikipedia.org/wiki/The_Cabinet_of_Dr._Caligari
