title: Privacy e censura: Tor, cache navigazione e DNS, filtraggio su Arch Linux
date: 2014-03-11 19:15:35 +0100
tags: censura, libertà, rete, tecnologia, privacy, gnu-linux, software libero, guide, sysadmin, arch
summary: Con questa guida cercherò di spiegare come configurare il proprio pc, in modo da avere i vantaggi della navigazione anonima tramite Tor, il filtraggio di banner e script fastidiosi o maliziosi dalle pagine dei siti, e contemporaneamente, mantenere un consumo di risorse ed una velocità accettabili.

Con questa guida cercherò di spiegare come configurare il proprio pc, in modo da avere i vantaggi della navigazione anonima tramite Tor, il filtraggio di banner e script fastidiosi o maliziosi dalle pagine dei siti, e contemporaneamente, mantenere un consumo di risorse ed una velocità accettabili.

Per fare questo, userò una combinazione di programmi configurandoli per lavorare assieme, e farò delle modifiche alla configurazione predefinita del browser.

I programmi che utilizzerò sono:

- [Tor][1]: un sistema di routing che ci permetterà di ridurre parecchio la
  nostra tracciabilità da parte di provider e siti visitati. Oltre a questo, ci
  darà la possibilità di accedere a quei siti bloccati dalla censura dei filtri
  statali, come The Pirate Bay, oppure a quei siti che impediscono l’accesso in
  base al paese d’origine di chi vi accede, come la webradio Pandora. Tor è
  inoltre supportato ed utilizzato da molti programmi di chat e peer to peer.
- [Privoxy][2]: un proxy senza cache che filtra i contenuti delle pagine.
  Elimina con una buona efficacia banner pubblicitari, popup, script e cookie
  traccianti. Lo scopo è simile a quello dell’accoppiata [Disconnect][3] +
  [Karmablocker][4]. Il vantaggio di utilizzare un servizio locale di proxy, è
  l’indipendenza dal browser. Ovvero, posso avere gli stessi filtri su qualsiasi
  browser che decida di usare, senza la necessità di cercare ed installare
  diverse aggiunte specifiche per ogni browser. In questo modo, posso mantenere
  il browser leggero, oppure utilizzare dei browser che non hanno a disposizione
  quei particolari addon. L’occupazione di risorse di privoxy, è molto inferiore
  a quella del solo Adblock plus, l'addon più famoso per Firefox e Chrome, e
  permette di fare un filtraggio molto più efficace. Nelle prove fatte, ho
  riscontrato che una soluzione con privoxy è più lenta di una in cui vi sono
  solo tor, polipo, pdnsd ed il filtraggio di script e pubblicità è affidato ad
  addon di firefox come Disconnect e Karmablocker. In alternativa, è possibile
  convertire e caricare le liste di AdBlock in Polipo a scapito, però, di un
  occupazione in memoria maggiore.
- [Polipo][5]: un proxy con cache estremamente veloce ed efficiente. Servirà per
  velocizzare la navigazione, riducendo i tempi di attesa che Tor, per via del
  suo funzionamento, inevitabilmente introduce. Questo software è talmente
  leggero e veloce che, utilizzandolo al posto della cache di Firefox, che verrà
  disabilitata per evitare di fare un inutile doppio lavoro di caching, si può
  notare un visibile incremento di prestazioni. Anche in questo caso,
  utilizzando più browser diversi, si ha il vantaggio di avere una cache comune
  che velocizza gli accessi e riduce lo spreco di spazio su disco causato
  dall’avere più programmi diversi, ognuno con la propria cache separata.
- [pdnsd][6]: un server dns che crea una cache locale su disco fisso. I vantaggi
  che porta questo software sono due. Protegge la privacy facendo passare le
  richieste di risoluzione dei nomi degli indirizzi attraverso Tor, rendendole
  quindi anonime e difficilmente loggabili dal provider. Migliora la velocità di
  risoluzione degli indirizzi grazie alla cache locale. Dopo un breve utilizzo,
  la maggior parte delle risoluzioni verranno fatte direttamente sul proprio pc,
  senza dover attendere i tempi di risposta, spesso lunghi, di server esterni.
  Anche questo, compensa la latenza che normalmente verrebbe introdotta da Tor.
  È possibile anche configurare dei server DNS secondari, nel caso la
  risoluzione dei nomi attraverso tor non funzionasse. In quel caso, le
  richieste non sarebbero più distribuite e difficili da tracciare, ma si
  tratterebbe di un compromesso che viene usato solo nei casi peggiori, mentre
  nel caso migliore, la richiesta verrebbe soddisfatta rapidissimamente dal
  proprio pc, senza neppure contattare un server esterno.

Le operazioni da fare non sono per nulla complicate. Personalmente ci sono
riuscito andando per passi successivi e testando il funzionamento dei vari
programmi mano a mano che li configuravo, in modo da rendermi conto subito di
eventuali errori. Cercherò di mantenere la guida chiara e modulare, in modo da
permettere a chi legge di testare mano a mano il funzionamento delle varie parti
e, se preferisce, implementarne soltanto alcuni passaggi invece di tutti, o di
potersi fermare a metà, mantenendo il sistema funzionante. Per poi, magari
aggiungere gli altri passaggi quando vorrà. Nella maggior parte dei casi,
bastano modifiche minime alle impostazioni predefinite. I motivi per cui sto
scrivendo questa guida sono che in rete non ho trovato ancora una guida veloce e
completa sull’integrazione di tutti questi programmi assieme, ma solo di alcuni,
e che voglio mostrare quanto sia semplice migliorare di parecchio la protezione
della propria privacy, ed al tempo stesso aggirare la censura, accedere ad un
web di miglior qualità e velocizzare la propria connessione.

Potete seguire la guida per intero, oppure prendere solo le parti che vi
interessano, adattandole alle vostre esigenze (ad esempio, se non vi interessa
la parte che riguarda Tor, ma volete velocizzare la navigazione con Polipo,
pdnsd e le modifiche a Firefox). La filosofia di unix di avere programmi che
fanno un solo lavoro, ma molto bene, e di poterli combinare, offre un grosso
vantaggio quando si vogliono fare queste cose.

Avendo provato varie combinazioni di questi programmi, posso dirvi quali diano
la miglior velocità e la miglior protezione. In linea generale, tenete presente
che i componenti che rallentano sono quelli che si occupano della sicurezza,
cioè tor e privoxy, mentre quelli che migliorano le prestazioni sono pdnsd e
polipo. Quindi, la configurazione che offre meno sicurezza, ma migliori
prestazioni, è polipo+pdnsd, mentre quella che garantisce maggiore sicurezza è
quella completa, che descriverò in questa guida.

Per la guida mi baserò su un’installazione di [GNU][7] [ArchLinux][8], perché è
la distribuzione che sto utilizzando ora. Quindi, chi utilizza altre
distribuzioni potrebbe dover adattare la guida alla propria situazione.
Probabilmente, per le distribuzioni derivate ed i fork, come [Archbang][9] e
[Chakra][10], si potrà seguire la guida senza dover adattare nulla.
Fortunatamente, i pacchetti presenti in Arch sono quasi sempre “vanilla”, cioè
esattamente come gli sviluppatori originali li hanno preparati, senza le
personalizzazioni che molte altre distribuzioni fanno. Grazie a questo,
qualsiasi distribuzione stiate usando, potere comunque dare un’occhiata
all’esauriente [wiki di Arch][11], trovandovi informazioni e guide utili. Se
usate Debian e derivate, vi consiglio di leggere le note di *liv* scritte in
coda a questo post.

Gli stessi software sono disponibili anche per altri sistemi operativi. In quel
caso, è probabile che dobbiate adattare maggiormente le istruzioni. Tuttavia, in
sistemi chiusi e proprietari, come Windows o Mac OS, dei quali non si ha accesso
al codice, non si può mai essere sicuri di ciò che facciano… Questo può essere
un problema, se il vostro obiettivo è di proteggere la privacy, in quanto non
potrete mai essere certi che non ci siano delle [backdoor][12] oppure che
vengano inviate delle informazioni su di voi a terzi, a vostra insaputa. In quel
caso, l’unica cosa che potete fare è di fiidarvi della “bontà” di chi ha
prodotto il sistema che usate.

Attenzione però a non fidarvi troppo nemmeno delle soluzioni qui proposte,
perché non potranno mai garantirvi una protezione perfetti. Riguardo a questo,
vi segnalo una pagina di avviso della distribuzione live *Tails*, basata su
Debian: [Tails – Warning][13].

Cosa accadrà, in teoria...
==========================

    :::text
    Browser->Privoxy->Polipo->Tor->(Rete)
           _\pdnsd__________/^

Escludendo la risoluzione dei nomi, il viaggio d’andata è il seguente: il
browser richiede la pagina al primo proxy, cioè Privoxy, il quale passa la
richiesta a Polipo che, se non ha i dati desiderati nella propria cache (nel cui
caso, li fornirà subito), la gira a Tor, che infine la invia, attraverso una
connessione criptata, al primo di una delle catene di nodi (circuiti)
attualmente attive, il fornitore della pagina riceverà la richiesta
dall’exit-node, cioè dal nodo della rete Tor che si affaccia ad internet,
anziché dal vostro IP. Il server contattato inizierà a rispondere alle varie
richieste, inviando i dati desiderati (pagina, immagini, oggetti vari…), i quali
transiteranno sui diversi nodi fino ad arrivare alla vostra macchina. Tor riceve
i dati e li passa a Polipo, il quale li memorizzerà nella propria cache per un
possibile successivo utilizzo e li girerà a Privoxy.  Questo esaminerà il
contenuto delle pagine ricevute, ripulendole da banner pubblicitari, spam,
script e cookie indesiderati, e lo passerà infine al vostro browser, che
visualizzerà il risultato sullo schermo.

*Risoluzione dei nomi.* Il browser o qualsiasi altro software che voglia
accedere ad un server in rete, richiederà a pdnsd di trasformare l’indirizzo
nominale in indirizzo numerico. Nel caso quell’indirizzo sia già stato cercato
in precedenza, il dns restituirà immediatamente il risultato, in caso contrario
passerà la richiesta a Tor, il quale contatterà i nodi esterni chiedendo la
risoluzione. Se questo andrà a buon fine, restituirà il risultato a pdnsd, il
quale lo salverà nella propria cache e lo passerà al programma che inizialmente
aveva fatto la richiesta.

Riguardo all’ordine dei vari demoni nella catena, ho fatto dei tentativi anche
invertendo Privoxy e Polipo (Browser->Polipo->Privoxy->Tor->Rete), ma mi è
sembrato tutto più lento, rispetto all’ordine indicato sopra. Nel caso vogliate
fare anche voi questa prova, in coda alla guida spiegherò cosa cambiare per
invertire i due proxy.

In pratica...
=============

Privoxy
-------

Installiamo privoxy, prelevandolo dal repository tramite pacman (o il gestore di
pacchetti usato dalla vostra distribuzione)

`# pacman -S privoxy`

> NB: Come convezione, quando una riga inizia con ‘#’, si intende che il comando
> va lanciato come utente root (o facendo sudo), mentre il carattere ‘$’ indica
> che è sufficiente l’utente normale. Il carattere # viene usato spesso anche
> come inizio commento nei file di configurazione, quindi quando leggete, fate
> attenzione al contesto.

Possiamo testarlo subito, lanciandolo con

`# systemctl start privoxy`

Dovrebbe partire senza alcun errore. Andiamo nelle impostazioni di connessione
del nostro browser, indicandogli di utilizzare come proxy http e ssl quello che
risponde all’indirizzo *localhost* e porta *8118*.

In Firefox, l’impostazione è in *Preferenze->Avanzate->Rete->Impostazioni…*

Disabilitate eventuali plugin come adblock e simili, ed iniziate a navigare. Se
tutto funziona correttamente, dovreste visualizzare le pagine ripulite da banner
e schifezze assortite.

Notare che, fino ad ora, non è ancora stato toccato il file di configurazione di
Privoxy.

Se volete avere una conferma più certa del funzionamento del proxy, ed essere
sicuri che il traffico passi effettivamente attraverso di lui, editate, da
utente root, il file di configurazione, cercando una riga commentata simile a
questa:

    :::text /etc/privoxy/config
    #debug 1 # Log the destination for each request Privoxy let through.

Togliete il # del commento, oppure inserite la riga, non commentata, a mano.

Riavviate privoxy `# systemctl restart privoxy.`

Aprite una finestra di terminale e, da utente root, digitate: `tail -f
/var/log/privoxy/logfile` (a seconda della distribuzione, potrebbe cambiare il
nome e la posizione del file di log. fate riferimento alle voci “logdir” e
“logfile” contenute nel file di configurazione).

Dopodiché, iniziate a navigare. Dovreste veder scorrere sul terminale tutte le
richieste a pagine ed oggetti vari, che transitano per il proxy.

Se tutto funziona a dovere, commentate nuovamente la voce “debug”. Potete quindi
inserire l’avvio del demone di privoxy al boot del sistema. Per fare questo,
date il comando
`# systemctl enable privoxy`

Se vi interessa solamente utilizzare un sistema veloce, leggero ed indipendente
dal browser, per filtrare i contenuti delle pagine (in sostituzione ai già
discussi Karma Blocker e Ghostery), potete fermarvi qua. Vi consiglio comunque
di leggere il capitolo successivo, riguardo alle ottimizzazioni della
configurazione di Firefox.

Se preferite continuare a far loggare il proxy, in modo da verificare eventuali
comportamenti anomali (filtraggi eccessivi o insufficienti) ed eventualmente
correggerli o migliorare le capacità di fitraggio, potete lasciare attiva
l’opzione “debug”, che accetta diversi livelli di verbosità.

    :::text /etc/privoxy/config
    #debug    1 # Log the destination for each request Privoxy let through.
    #debug 1024 # Log the destination for requests Privoxy didn't let through, and the reason why.
    #debug 4096 # Startup banner and warnings
    #debug 8192 # Non-fatal errors

In questo caso, potrebbe interessarvi far sì che il log venga ruotato
periodicamente. Se la vostra distribuzione non lo include già, create uno script
simile a questo in /etc/logrotate.d e dategli i permessi di esecuzione con
chmod.

    :::text /etc/logrotate.d/privoxy
    /var/log/privoxy/logfile {
       create 660 root privoxy
       notifempty
       compress
       postrotate
         /bin/kill -HUP `cat /var/run/privoxy.pid 2>/dev/null` 2> /dev/null || true
       endscript
    }

Come impostazione predefinita, privoxy parte con una serie di regole abbastanza
permissive, e non permette all'utente di essere configurato tramite browser.
Consiglio di abilitare la configurazione da browser aggiungendo questa voce al 
file di configurazione:
    :::text /etc/privoxy/config
    enable-edit-actions 1

Dopodiché, assicuratevi di aver riavviato il demone ed andate alla pagina di
configurazione http://config.privoxy.org/
Selezionate *View & change the current configuraion* ed *Edit* alla riga
*/etc/privoxy/match-all.action*.
Infine, selezionate *Set to Medium*. Se volete una protezione più aggressiva,
provate *Set to Advanced*.

Per approfondire, potete cominciare dalla pagina della wiki di Arch:
[https://wiki.archlinux.org/index.php/Privoxy][14]

Successivamente riprenderò in mano la configurazione di privoxy, per
ottimizzarla ed adattarla all’integrazione con gli altri programmi.

Tor...
------

Anche in questo capitolo, procederò per piccoli passi, facendo verificare il
corretto funzionamento mano a mano.. Installeremo Tor ed useremo la
configurazione già fatta di Privoxy come base a cui collegarlo. Polipo verrà
installato in seguito, una volta che saremo sicuri che Tor funziona a dovere e
che il browser sia impostato con le ottimizzazioni migliori.

Installiamo Tor, prelevandolo dai repository ufficiali: `# pacman -S tor`

Per testarne subito il funzionamento, non è necessario fare alcuna modifica alla
configurazione predefinita.

Lo avviamo come di consueto `# systemctl start tor`

Apriamo il browser, andiamo nelle impostazioni di rete, dove precedentemente
avevamo inserito l’indirizzo e la porta di privoxy. Togliamo privoxy da HTTP ed
inseriamo come proxy SOCKS 5, *localhost* su porta *9050*.

Proviamo ad aprire qualche sito. Al momento, ci saranno delle latenze maggiori
di quelle a cui siamo abituati, ma questo è solo un test per controllare il
funzionamento. Della velocità, ci occuperemo dopo. Se riuscite a navigare,
provate anche ad andare su qualche sito che sapete essere censurato, come
*thepiratebay*. Se Tor funziona a dovreste, riuscirete a visualizzarlo. (Se il
sito è lento a rispondere, potrebbe essere necessario un refresh della pagina.)
In alternativa, potete usare il sito http://torcheck.xenobite.eu/

Come ulteriore prova, potere monitorare i log, in modo simile a come era stato
fatto per privoxy. `# tail -f /var/log/messages.log |grep "localhost Tor"`

> NB: Sul vostro sistema, il log potrebbe essere in un file diverso, tipicamente
> /var/log/syslog. Oppure, il pacchetto Tor potrebbe essere preconfigurato per
> salvare il log su un file a parte.

Ora possiamo modificare il file di configurazione di Tor, /etc/tor/torrc
modificando o aggiungendo queste righe, che serviranno a ridurre un po’ i tempi
di attesa nei caricamenti

    :::text /etc/tor/torrc
    # Try for at most NUM seconds when building circuits. If the circuit isn't
    # open in that time, give up on it. (Default: 1 minute.)
    CircuitBuildTimeout 5
    # Send a padding cell every N seconds to keep firewalls from closing our
    # connections while Tor is not in use.
    KeepalivePeriod 60
    # Force Tor to consider whether to build a new circuit every NUM seconds.
    NewCircuitPeriod 15
    # How many entry guards should we keep at a time?
    NumEntryGuards 8

Se tutto funziona come dovrebbe, andiamo a spiegare a privoxy come passare le
richieste a Tor… Per fare questo è sufficiente inserire nel file di
configurazione `/etc/privoxy/config` la seguente riga (compreso il punto
finale):

    :::text /etc/privoxy/config
    forward-socks5 / localhost:9050 .

Per mantenere il file ordinato, inseritela successivamente alla sezione
commentata dove vengono spiegati i forward.

Riavviamo privoxy con al solito `# systemctl restart privoxy`

Torniamo sulla configurazione di rete di Firefox, nella parte dei proxy.
Togliamo il SOCKS 5 inserito precedentemente e rimettiamo *localhost* su porta
*8118* in HTTP e SSL (come avevamo fatto inizialmente, appena installato
privoxy).

Testiamo la navigazione. Dovremmo poter accedere ai siti censurati e al tempo
stesso, filtrare le pagine da banner e schifezze. Se ci piace veder scorrere
tante scrittine, riabilitate il debug di privoxy, riavviatelo e con due
terminali monitorate sia il logo di privoxy, come precedentemente spiegato, sia
quello di Tor. :D

Ora possiamo configurare anche altri software per utilizzare Tor. Tipicamente,
client di chat e peer to peer, come bittorrent. Per questi ultimi, si raccomanda
di *non far mai passare tutto il traffico attraverso Tor*, ma solamente le
richieste verso i tracker servers. Se il programma permette di passare
attraverso un proxy con protocollo SOCKS, possiamo specificargli l’indirizzo e
la porta di Tor, cioè *localhost* (o *127.0.0.1*) e *9050*.

Come per privoxy, se vogliamo che il demone venga avviato automaticamente al
boot del sistema, diamo il comando `# systemctl enable tor`

...e ottimizzazioni di Privoxy e Firefox
----------------------------------------

Ora è il momento di recuperare un po’ di prestazioni.

In Firefox apriamo la pagina di configurazione avanzata, digitando about:config
nella barra dell’indirizzo. Promettiamo al premuroso browser che non faremo
danni e cerchiamo le impostazioni idicate sotto (iniziano tutte per
network.http), modificandole come descritto.

    :::text about:config
    network.http.keep-alive                            true
    network.http.keep-alive.timeout                    600
    network.http.max-connections                       30
    network.http.max-connections-per-server            15
    network.http.max-persistent-connections-per-proxy  16
    network.http.max-persistent-connections-per-server 6
    network.http.pipelining                            true
    network.http.pipelining.maxrequests                8
    network.http.pipelining.ssl                        true
    network.http.proxy.keep-alive                      true
    network.http.proxy.pipelining                      true

Con queste impostazioni, noi diciamo a Firefox di abilitare il [pipelining][15],
di accettare più connessioni contemporanee e di mantenere le connessioni aperte
più a lungo del normale.

Potete usare queste impostazioni per velocizzare Firefox, anche se non
utilizzate Privoxy e Tor.

Nella configurazione di privoxy, verifichiamo che queste impostazioni siano
commentate:

    :::text /etc/privoxy/config
    #keep-alive-timeout 300
    #default-server-timeout 60
    #socket-timeout 300

Impostare timeout lunghi in privoxy, al contrario, lo rallenta. Quando abbinato
ad un parent proxy, come faremo poi, la situazione peggiora notevolmente,
causando tempi di attesa indesiderati.

Se vi interessava solamente una semplice e veloce installazione di Tor e
Privoxy, con una configurazione ottimizzata di Firefox, potete fermarvi qua.
Proseguite, se volete migliorare ulteriormente la velocità di navigazione,
aggiungendo un velocissimo proxy con cache ed un server DNS locale che possa
passare attraverso Tor.

Fonti ed approfondimenti:

- https://wiki.archlinux.org/index.php/Tor
- https://trac.torproject.org/projects/tor/wiki/doc/FireFoxTorPerf
- http://blog.bodhizazen.net/linux/speed-up-privoxy/
- http://securitywarn.blogspot.de/2011/04/how-to-speed-up-tor-proxy-network.html
- http://www.gabrielweinberg.com/blog/2010/08/duckduckgo-now-operates-a-tor-exit-enclave.html

Polipo
------

Passiamo ora ad installare il proxy Polipo, che ci permetterà di mantenere una
cache locale delle pagine visitate, velocizzando la navigazione.

`# pacman -S polipo`

Creiamo la configurazione, partendo da un file di esempio

`# cd /etc/polipo; cp config.sample config`

Aggiungiamo o decommentiamo queste righe

    :::text /etc/polipo/config
    allowedPorts = 1-65535
    tunnelAllowedPorts = 1-65535

Verifichiamo ed eventualmente inseriamo o modifichiamo le seguenti impostazioni,
utili per ottimizzare le connessioni

    :::text /etc/polipo/config
    maxConnectionAge = 5m
    maxConnectionRequests = 120
    serverMaxSlots = 8
    serverSlots = 2

Polipo utilizza molto efficientemente il *pipelining http*, sia in uscita verso
i server web, sia in arrivo dal browser.

Quest’altra configurazione, servirà a fargli preferire i collegamenti tramite
IPv4 a quelli IPv6, velocizzando le ricerche sui DNS.

    :::text /etc/polipo/config
    dnsQueryIPv6 = reluctantly

Avviamo Polipo e testiamolo con il browser, inserendo il suo indirizzo e la sua
porta nelle impostazioni del proxy HTTP, come già fatto in precedenza con
privoxy. L’indirizzo è sempre *localhost* e la porta la *8123*.

Se tutto funziona a dovere, possiamo disabilitare la cache del browser, che non
ci servirà più. Andiamo nelle impostazioni della cache:
*Preferenze->Avanzate->Rete*, cancelliamo il contenuto della cache, spuntiamo la
casella per non utilizzare la gestione automatica e limitiamo la dimensione
della cache a *0 MB* di spazio.

Personalmente, ho trovato la cache di Polipo molto più veloce di quella nativa
del browser.

Arrivati a questo punto, abbiamo il browser che accede alla rete utilizzando la
cache di polipo al posto della propria.

Ma noi abbiamo già un proxy filtrante e la rete Tor configurati, quindi ora li
metteremo in comunicazione.

Prima, configurerò *privoxy* in modo che passi le richieste a polipo, poi
configurerò polipo in modo che le giri, a sua volta, a *Tor*. Se volete avere
solamente un sistema con filtraggio pagine e cache centrale, seguite solo il
primo passaggio, se volete avere cache e Tor senza filtraggio, seguite solo il
secondo. Per la soluzione completa, seguitele entrambe.

Apriamo la configurazione di privoxy /etc/privoxy/config e, se precedentemente
avevamo configurato il *forward-socks5* verso tor, commentiamolo e sostituiamolo
con questo

    :::text /etc/privoxy/config
    forward / localhost:8123
    forward localhost .

Torniamo alla configurazione del nostro browser e modifichiamo il proxy,
rimettendo l’indirizzo localhost e la porta di *privoxy 8118*. Testiamo la
navigazione. In questo modo, abbiamo la cache di polipo ed il filtraggio delle
pagine di privoxy.

Se ora vogliamo far passare tutto attraverso Tor, apriamo la configurazione di
polipo ed inseriamo o decommentiamo le seguenti righe

    :::text /etc/polipo/config
    socksParentProxy = "localhost:9050
    socksProxyType = socks5

Riavviamo polipo e testiamo la navigazione. Se vogliamo utilizzare privoxy +
polipo + tor, nelle impostazioni del proxy del browser dovranno essere impostati
l’indirizzo e la porta di privoxy (*localhost 8118*), se non ci interessa
privoxy, impostiamo l’indirizzo e la porta di polipo (*localhost 8123*).

Ricordiamo di inserire l’avvio del demone al boot del sistema `# systemctl enable polipo`

Polipo è un server molto leggero ed efficiente. La sua leggerezza è data anche
dal fatto che non controlla dinamicamente la dimensione della cache salvata, la
quale potrebbe potenzialmente crescere senza controllo, anche se è difficile che
questo accada. Possiamo comunque far sì che, il proxy ripulisca, una tantum, la
cache dai vecchi dati, chiamandolo con l’opzione -x. `# polipo -x`

Possiamo anche automatizzare questo processo, utilizzando uno script che verrà
eseguito a scadenza regolare da cron.

Creiamo da utente root in `/etc/cron.daily` il file `polipo`, contenente

    :::sh /etc/cron.daily/polipo
    #!/bin/bash
    /usr/bin/polipo -x >/dev/null 2>&1

e diamogli i permesse di esecuzione `# chmod u+x /etc/cron.daily/polipo`

Per la rotazione dei log, è necessario creare uno script come `/etc/logrotate.d/polipo`

    :::text /etc/logrotate.d/polipo
    /var/log/polipo.log {
            missingok
            rotate 7
            compress
            daily
            postrotate
                    PIDFILE=/var/run/polipo/polipo.pid
                    if [ -f "$PIDFILE" ] ; then kill -USR1 $(cat "$PIDFILE") ; fi
            endscript
    }

È anche possibile consultare il manuale di polipo e modificarne la
configurazione, direttamente dal browser, puntandolo all'indirizzo
http://localhost:8123

A questo punto, abbiamo il nostro sistema di proxy concatenati, configurato e
funzionante. Buona protezione della privacy, buone capacità di aggiramento della
censura e buona velocità di navigazione.

Fonti e approfondimenti

- https://wiki.archlinux.org/index.php/Polipo
- http://www.go2linux.org/modify-polipo-cache-size-and-purge-rules
- http://bodhizazen.net/Tutorials/TOR#socks

Possiamo migliorare ancora un po’, installando un DNS locale, che faccia la
cache degli indirizzi e rediriga le richieste di risoluzione verso la rete di
Tor.

Pdnsd
-----

Installiamo pdnsd nel solito modo `# pacman -S pdnsd`

Creiamo il setup iniziale partendo dal file di esempio, che successivamente
modificheremo. `# cp /usr/share/doc/pdnsd/pdnsd.conf /etc/pdnsd.conf`

Il file di configurazione `/etc/pdnsd.conf` consiste in una sezione globale e
varie sezioni *“server”* commentate, cioè racchiuse fra “/*” e “*/”. Apriamolo
e, nella sezione *“global”*, cambiamo `run_as="nobody";` in `run_as="pdnsd";`

Se vogliamo utilizzarlo in congiunzione a tor, aggiungiamo questa sezione
server, subito dopo la sezione “global”.

    :::text /etc/pdnsd.conf
    # Tor DNS resolver
    server {
    	label = "tor";
    	ip = 127.0.0.1;
    	port = 8853;
    	uptest = none;
    	exclude=".invalid";
    	policy=included;
    	proxy_only = on;
    	lean_query = on;
    }

Lasciamo commentati gli altri server, in modo da evitare che transitino delle
richieste DNS in chiaro. Per avere un minimo di sicurezza sulla propria privacy,
è necessario che *tutto* il traffico inerente la navigazione transiti su canali
sicuri.

Nella configurazione di Tor, in `/etc/tor/torrc`, decommentando questa voce

    :::text /etc/tor/torrc
    ControlPort 9051

Ed aggiungiamo le seguenti

    :::text /etc/tor/torrc
    DNSPort 8853
    AutomapHostsOnResolve 1
    AutomapHostsSuffixes .exit,.onion
    
    TransPort 9040
    TransListenAddress 127.0.0.1

Ora riavviamo tor, facciamo partire pdnsd, e testiamo il funzionamento.

    :::text shell
    # systemctl restart tor
    # systemctl start pdnsd
    $ nslookup duckduckgo.com 127.0.0.1
    $ nslookup http://www.archlinux.org 127.0.0.1

Se abbiamo ottenuto delle risposte positive, passiamo a configurare il sistema
in modo che tutti gli indirizzi vengano risolti dal DNS locale, invece che da
quelli esterni impostati precedentemente.

Sulla mia macchina non utilizzo né *NetworkManager*, né *wicd*, ed ho
l’indirizzo della scheda di rete impostato staticamente, quindi sarà sufficiente
editare il file /etc/resolv.conf, commentando o rimuovendo i nameserver
precedentemente inseriti, ed aggiungendo l’indirizzo del dns locale.

    :::text /etc/resolv.conf
    nameserver 127.0.0.1

Chi utilizza NetworkManager, wicd o simili, dovrà fare la stessa cosa tramite
gli strumenti forniti dal proprio programma gestore di rete, togliendo quindi i
dns precedentemente inseriti, impostandolo di non ottenerli automaticamente
tramite dhcp, e specificando come unico dns l’indirizzo locale *127.0.0.1*

Se volete fare in modo che polipo utilizzi pdnsd per la risoluzione dei nomi,
invece dei resolver interno, aggiungete questa riga a `/etc/polipo/config`

    :::text /etc/polipo/config
    dnsUseGethostbyname = yes

Verifichiamo che tutto funzioni correttamente, aggiungiamo pdnsd fra i demoni
all’avvio… Ed il gioco è fatto! :) `# systemctl enable pdnsd`

Fonti ed approfondimenti

- https://wiki.archlinux.org/index.php/Pdnsd
- http://wiki.debian.org/FreedomBox/Configs/Tor
- http://punto-informatico.it/2070093/PI/Commenti/cassandra-crossing-tor-lezioni-guida.aspx

Appendice: adattamenti per Debian e derivate, by liv
====================================================

Complice un fine settimana lungo, ho seguito passo passo la tua ottima guida
installando su Debian prima Privoxy, poi Tor, infine Polipo, per ottenere una
configurazione Browser->Privoxy->Polipo->Tor->(Rete). Non ho installato pdnsd
perché la velocità era già soddisfacente.  La procedura per Debian è
praticamente identica a quella che hai descritto tu; cambiano solo ovviamente i
file di configurazione, rispetto ad Archlinux, e i singoli comandi. Per quanto
riguarda Tor c’è qualche piccola differenza in più ma la logica è la stessa.
Riporto di seguito tutta la procedura, e soprattutto i riferimenti ulteriori che
ho consultato: magari a qualcuno potranno essere utili…

Per configurare Privoxy su Debian ho seguito, oltre alla tua guida, anche il
wiki di Debianizzati:
http://guide.debianizzati.org/index.php/Privoxy:_navigazione_sicura_a_prova_di_spam

Per quanto riguarda Tor, gli altri miei riferimenti sono stati:
http://guide.debianizzati.org/index.php/Anonimato_in_rete_-_Tor
http://www.nazioneindiana.com/2007/11/05/tor-lezioni-di-guida/
https://www.torproject.org/docs/debian.html.en

A proposito del Torbutton, di cui ora gli sviluppatori Tor sconsigliano l’uso a
meno che non si utilizzi il Tor Browser Bundle, ho consultato:

https://www.torproject.org/torbutton/
https://blog.torproject.org/blog/toggle-or-not-toggle-end-torbutton

Ed ecco la mia procedura:

Dopo l’installazione di privoxy, l’ho lanciato da riga di comando:

`#/etc/init.d/privoxy start`

Per fare in modo che il browser (nel mio caso Firefox/Iceweasel) lo utilizzasse
per navigare ho impostato il proxy accedendo al menu
“Modifica/Preferenze/Avanzate/Rete/Impostazioni”, cliccando sul pulsante
“Impostazioni connessione”.  Ho selezionato la voce “Configurazione manuale del
proxy” e a questo punto ho indicato l’indirizzo 127.0.0.1 e la porta 8118 sia
come proxy HTTP sia come proxy SSL.  Poi ho svuotato completamente la cache del
browser, sempre seguendo la guida di Masque.  Per verificare il comportamento di
Privoxy, ho controllato il relativo log; per farlo ho editato, da utente root,
il file di configurazione /etc/privoxy/config, cercando una riga commentata
simile a questa:

`debug 1 # Log the destination for each request Privoxy let through.`

Eliminando il # del commento e salvando il file così modificato, si fa in modo
che nel file di log si vedano le varie richieste inoltrate. Prima di aprire il
file di log, ho riavviato privoxy con il comando

`#/etc/init.d/privoxy restart`

Quindi da terminale, e sempre da utente root, ho digitato:

`#tail -f /var/log/privoxy/logfile`

Poi ho iniziato a navigare e ho visto che contemporaneamente sul terminale
venivano visualizzate le richieste transitanti per il proxy. Quindi, tutto ok:
pertanto ho ricommentato la voce “debug” nel file di configurazione.
Infine, per inserire l’avvio del demone di privoxy al boot del sistema, sono
semplicemente andata sul menu “Sistema/Preferenze/Applicazioni d’avvio” e ho
aggiunto /etc/init.d/privoxy alle altre applicazioni da lanciare durante il
boot. (sicuramente c’è un sistema più elegante per fare questo…).

Dopo di che, ho affrontato Tor! Operando su una distro Debian, e volendo
ottenere l’ultima versione stabile di Tor, per prima cosa ho aggiunto un
repository di pacchetti dedicato; questo si fa aggiungendo, al file
/etc/apt/sources.list, la seguente riga:

`deb http://deb.torproject.org/torproject.org main`

inserendo il nome della distribuzione Debian (Lenny, Squeeze, Wheezy, Sid) al
posto di .
Quindi, da terminale ho lanciato i seguenti comandi:

    :::text shell
    $ gpg –keyserver keys.gnupg.net –recv 886DDD89
    $ gpg –export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | sudo apt-key add -
    # apt-get update
    # apt-get install deb.torproject.org-keyring
    # apt-get install tor tor-geoipdb

Tor è ora installato. A questo punto, per consentire il “chain” di Privoxy e Tor
ho agito sul file di configurazione /etc/privoxy/config cercando la seguente
riga:

`forward-socks5 / 127.0.0.1:9050 .`

L’ho decommentata; poi, per verificare il corretto funzionamento di Tor ho
aperto il sito http://torcheck.xenobite.eu (anche https://check.torproject.org
va bene).

Per quanto riguarda infine la configurazione avanzata di Firefox (per intenderci
qualla cui si accede digitando about:config nella barra indirizzi) l’unica
differenza è che oltre ai parametri per abilitare il pipelining e per mantenere
le connessioni aperte, ho cambiato il parametro network.http.sendRefererHeader
da 2 a 0. Questa impostazione, che ho trovato sulle “Lezioni di Guida” di
Cassandra Crossing, evita che il browser fornisca al sito corrente l’indirizzo
della pagina precedente visitata.

Per quanto riguarda Polipo, infine, non ci sono differenze sostanziali rispetto
ad Archlinux tranne che di default gira con utente “proxy”, il quale non
dovrebbe avere privilegi di root.

[1]: https://it.wikipedia.org/wiki/Tor_(software_di_anonimato)
[2]: https://it.wikipedia.org/wiki/Privoxy
[3]: https://disconnect.me/disconnect
[4]: https://addons.mozilla.org/en-US/firefox/addon/karma-blocker/
[5]: https://en.wikipedia.org/wiki/Polipo
[6]: https://en.wikipedia.org/wiki/Pdnsd
[7]: http://www.gnu.org/home.it.html
[8]: http://www.archlinux.it/
[9]: http://archbang.org/
[10]: http://chakra-project.it/
[11]: https://wiki.archlinux.org/index.php/Main_Page_(Italiano)
[12]: https://it.wikipedia.org/wiki/Backdoor
[13]: https://tails.boum.org/doc/about/warning/index.en.html
[14]: https://wiki.archlinux.org/index.php/Privoxy
[15]: https://it.wikipedia.org/wiki/HTTP_pipelining
