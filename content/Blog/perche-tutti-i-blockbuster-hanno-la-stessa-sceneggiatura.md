title: Perché tutti i blockbuster hanno la stessa sceneggiatura?
date: 2016-05-11 19:02
tags: film
summary: If you’ve gone to the movies recently, you may have felt a strangely familiar feeling: You’ve seen this movie before. Not this *exact* movie, but some of these exact story beats: the hero dressed down by his mentor in the first 15 minutes (Star Trek Into Darkness,Battleship); the villain who gets caught on purpose (The Dark Knight, The Avengers,Skyfall, Star Trek Into Darkness); the moment of hopelessness and disarray a half-hour before the movie ends (Olympus Has Fallen, Oblivion, 21 Jump Street, Fast & Furious 6).

Lo spiega questo articolo preso da [slate.com][1]

## Save the Movie!
### The 2005 screenwriting book that’s taken over Hollywood—and made every movie feel the same.

*By [Peter Suderman][2]|Posted Friday, July 19, 2013, at 5:55 AM*

If you’ve gone to the movies recently, you may have felt a strangely familiar feeling: You’ve seen this movie before. Not this *exact* movie, but some of these exact story beats: the hero dressed down by his mentor in the first 15 minutes (Star Trek Into Darkness,Battleship); the villain who gets caught on purpose (*The Dark Knight, The Avengers,Skyfall, Star Trek Into Darkness*); the moment of hopelessness and disarray a half-hour before the movie ends (*Olympus Has Fallen, Oblivion, 21 Jump Street, Fast & Furious 6*).

It’s not déjà vu. Summer movies are often described as formulaic. But what few people know is that there is *actually a formula* — one that lays out, on a page-by-page basis, exactly what should happen when in a screenplay. It’s as if a mad scientist has discovered a secret process for making a perfect, or at least perfectly conventional, summer blockbuster.

The formula didn’t come from a mad scientist. Instead it came from a screenplay guidebook, [Save the Cat! The Last Book on Screenwriting You’ll Ever Need][3]. In the book, author Blake Snyder, a [successful spec screenwriter][4] who became an influential screenplay guru, preaches a variant on the basic three-act structure that has dominated blockbuster filmmaking since the late 1970s.

When Snyder published his book in 2005, it was as if an explosion ripped through Hollywood. The book offered something previous screenplay guru tomes didn’t. Instead of a broad overview of how a screen story fits together, his book broke down the three-act structure into a detailed “beat sheet”: 15 key story “beats”—pivotal events that have to happen—and then gave each of those beats a name and a screenplay page number. Given that each page of a screenplay is expected to equal a minute of film, this makes Snyder’s guide essentially a minute-to-minute movie formula.

Snyder, who died in 2009, would almost certainly dispute this characterization. In *Save the Cat!*, he stresses that his beat sheet is a *structure*, not a formula, one based in time-tested screen-story principles. It’s a way of making a product that’s likely to work—not a fill-in-the-blanks method of screenwriting.

Maybe that’s what Snyder intended. But that’s not how it turned out. In practice, Snyder’s beat sheet has taken over Hollywood screenwriting. Movies big and small stick closely to his beats and page counts. Intentionally or not, it’s become a formula—a formula that threatens the world of original screenwriting as we know it.

—

Screenplay gurus like Syd Field and Robert McKee touted the essential virtues of [three-act structure][5] for decades. For Field and McKee, three-act structure is more of an organizing principle—a way of understanding the shape of a story. Field’s [Story Paradigm][6], for example, has just a handful of general elements attached to broad page ranges.

Field and McKee offered the screenwriter’s equivalent of cooking tips from your grandmother—general tips and tricks to guide your process. Snyder, on the other hand, offers a detailed recipe with step-by-step instructions.

Each of the 15 beats is attached to a specific page number or set of pages. And Snyder makes it clear that each of these moments is a must-have in a well-structured screenplay. The page counts don’t need to be followed strictly, Snyder says, but it’s important to get the proportions fairly close. You can see the complete beat sheet, with page numbers and a summary of each beat, [in a sidebar here][7].

Let’s take a journey through this year’s blockbusters and blockbuster wannabes and see the big trailer-ready ways in which Snyder’s beat sheet pops up over and over again. Look at January’s *Gangster Squad*. After an opening image that sets up the conflict between Josh Brolin’s hard-charging cop, Sgt. John O’Mara, and the criminal forces of mob boss Mickey Cohen (Sean Penn), O’Mara is called in to see his gruff police superior. “We got rules around here, smartass,” the chief growls. “Do yourself a favor. Learn ’em.” That’s Snyder’s second beat, **theme stated**. And it’s right at the seven-minute mark, almost exactly when it’s supposed to happen in a 110-minute movie. The rest of the Snyder playbook is there, too: a story-starting **catalyst** midway through the first act, a shootout at the **midpoint** that ups the ante, an **all-is-lost moment** — including a death — between the 75- and 80-minute mark, and a concluding **final act** in which the baddies are dispatched in ranking order, just as Snyder instructs.

Or look at March’s *Jack the Giant Slayer*. There’s an **opening image** that sets up each of the young protagonists’ problems and **states the theme** at the five-minute mark, a **catalyst** at the 12-minute mark, an **act break** between the 25- and 30-minute mark when Jack climbs the beanstalk, and a **false victory** 90 minutes in, when it looks as if the evil giants have been definitively defeated.

*Oz the Great and Powerful* is a fun riff on director Sam Raimi’s quirky early horror films. But check your watch a quarter of the way through and you’ll find a tornado that whisks Oz, and the movie, into its first act. Once Oz has landed, he meets Theodora, the love interest—and the **B-plot**. Baz Luhrmann’s *The Great Gatsby* adaptation was reorganized to fit the formula, with a party-filled **fun and games** second quarter that leads to the decline of the third, in which tragedy looms as the **bad guys close in**.

Field and McKee were obsessed with the theoretical underpinnings of storytelling. But Snyder’s book is far more straightforward. And that’s why it’s conquered the big screen so thoroughly. Indeed, if you’re on the lookout, you can find Snyder’s beats, in the order he prescribes, executed more or less as Snyder instructs, in virtually every major release in theaters today. Even the master storytellers at Pixar stick quite close to Snyder’s playbook: Watching *Monsters University* this summer, I loved the way it toyed with underdog sports and college movie conventions. Yet the story hits every one of Snyder’s beats, including an **opening image** that’s mirrored in the final scene, an **act break** when Mike and Sully reluctantly join forces to compete in the Scare Games, a false victory about three-quarters of the way through when (spoiler!) they “win” the final Scare Games challenge, and an **all-is-lost moment** followed by an emotionally charged **dark night of the soul** next to a moonlit lake afterward.

Yet once you know the formula, the seams begin to show. Movies all start to seem the same, and many scenes start to feel forced and arbitrary, like screenplay Mad Libs. Why does Kirk get dressed down for irresponsibility by Admiral Pike early in *Star Trek Into Darkness*? Because someone had to deliver the **theme** to the main character. Why does Gina Carano’s sidekick character defect to the villain’s team for no reason whatsoever almost exactly three-quarters of the way through *Fast & Furious 6*? Because it’s the **all-is-lost moment**, so everything needs to be in shambles for the heroes. Why does Gerard Butler’s character in *Olympus Has Fallen* suddenly call his wife after a climactic failed White House assault three-quarters of the way through? Because the second act always ends with a quiet moment of reflection — the **dark night of the soul**.

And if the villain of the past few years of movies is the adolescent male for whom it seems all big-Hollywood product is engineered, Snyder’s guidelines have helped that bad guy close the door to other potential audiences. *Save the Cat!* doesn’t go so far as to require that protagonists be men. But the book does tell aspiring screenwriters to stick to stories about the young, because that’s “the crowd that shows up for movies.” Following this advice to its logical conclusion means far more stories about young men—since that’s who shows up at the multiplex the most. It’s not an accident that the chapter on creating a hero is called “It’s About A Guy Who … ” not “It’s About A Person Who … ” And with a young male protagonist, women are literally relegated to the B-plot—the love interest, or “helper,” who assists the male protagonist in overcoming his personal problems. It’s not an accident that Raimi’s megabudget Oz movie featured not Dorothy but a male protagonist.

Watching poorly executed movies with Snyder’s formula in mind can become a tiresome and repetitive slog. How many times can you watch a young man struggle with his problems, gain new power, then save the world? It’s enough to make you wonder: Is overreliance on Snyder’s story formula killing movies?

If so, then all is lost. The major studios increasingly rely on a small number of megabudget blockbusters for their profits. But big budgets mean big risks. And the only way to mitigate those risks is to stick with what’s been known to work before. In other words, formula—and the more precise the formula, the better. America’s greatest art form is headed straight, as the Snyderized *Star Trek* sequel notes, Into Darkness.

—

It’s not that the formula can’t produce good, fun movies: *Monsters University* is very enjoyable. *Star Wars, Die Hard, The Matrix*, and *The Avengers* all follow something like the story path that Snyder laid out. But it does mean that Hollywood produces way too many movies about adolescent men coming to grips with who they are (think *John Carter, Battleship, The Bourne Legacy, Tron: Legacy, Abraham Lincoln: Vampire Hunter*, virtually every superhero movie, and the entirety of the J.J. Abrams canon).

It also means that there’s far less wiggle room for even minor experimentation. Think of a classic popcorn flick like *Jurassic Park*. It’s a pretty classic three-act story, and it includes virtually all of the elements found in Snyder’s beat sheet. But they are [out of order and out of proportion][8]. Now compare that to a modern megablockbuster like *The Amazing Spider-Man*, which follows the Snyder structure [beat by beat][9]. There’s a reason that even [Steven Spielberg][10] is complaining that Hollywood is too reliant on formulaic blockbusters.

We can appeal to screenwriters to buck the trend. But why would they? The formula is incredibly useful. Indeed, I relied on Snyder’s beat sheet to write this piece, using every beat, in the order he lists. (Try reading this piece from the beginning and see if you can spot all the beats. Or [click here][11] to see a version of the essay in which they are all labeled.)

I could see the advantages of the beat sheet. It helped me order my thoughts and figure out what I should say next. But I also found myself writing to fit the needs of the formula rather than the good of the essay—some sections were cut short, others deleted entirely, and other bits included mostly to hit the beat sheet’s marks. It made writing easier, in other words, but it also made me less creative.

That’s why you’ve got that strangely familiar feeling at the movies. Hollywood needs to learn a screenplay style life-lesson of its own: Sure, sometimes you can let the formula guide you. But that shouldn’t be the only thing you know how to do.

[1]: http://www.slate.com/articles/arts/culturebox/2013/07/hollywood_and_blake_snyder_s_screenwriting_book_save_the_cat.single.html

[2]: http://www.slate.com/authors.peter_suderman.html

[3]: https://en.wikipedia.org/wiki/Blake_Snyder

[4]: http://www.imdb.com/name/nm0811414/bio

[5]: https://en.wikipedia.org/wiki/Three-act_structure

[6]: http://www.sydfield.com/paradigm.pdf

[7]: http://www.slate.com/content/slate/sidebars/2013/07/the_save_the_cat_beat_sheet.html

[8]: http://www.blakesnyder.com/2013/04/05/the-jurassic-park-beat-sheet/

[9]: http://www.blakesnyder.com/2012/07/13/the-amazing-spider-man-beat-sheet/

[10]: http://www.hollywoodreporter.com/news/steven-spielberg-predicts-implosion-film-567604

[11]: http://www.slate.com/content/slate/sidebars/2013/07/now_playing_at_your_local_multiplex_save_the_movie.html
