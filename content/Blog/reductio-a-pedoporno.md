title: Reductio a Pedoporno
date: 2016-02-08 22:38
tags: società, filosofia, rete
summary: È fatto noto a chiunque segua con attenzione le discussioni riguardo alla privacy in rete ed alla censura, che l’argomento della pedopornografia viene spesso usato da governi e gruppi industriali per imporre leggi e regolamenti restrittivi e dannosi per i cittadini che usufruiscono della rete.

**Premessa:**

La reductio ad Hitlerum (espressione coniata negli anni 50 da Leo Strauss) è una fallacia logica riconducibile alla categoria degli attacchi ad hominem e dell’appeal to emotion. Essa consiste nel tentare di delegittimare il proprio interlocutore attraverso paralleli fallaci (ovviamente non tutti lo sono) con personaggi malvagi, primo su tutti Adolf Hitler, ma anche Stalin o altri. Uno dei possibili schemi: Mario sostiene x. Anche Hitler sosteneva x. Quindi x è sbagliato. Ovviamente il semplice fatto che Hitler abbia sostenuto x non rende x di per sé errato. Hitler ad esempio amava i cani e la pittura, e non per questo cani e pittura sono da aborrire. […]

Anche Richard Sexton disse  che spesso una discussione può ritenersi s conclusa quando qualcuno dei partecipanti tira fuori dal cassetto, come ultima risorsa,  i nazisti o Hitler. Personalmente aggiungerei che, talora, le discussioni addirittura li prendono come spunto iniziale…Hitler o Stalin… laddove quindi l’invito alla discussione dovrebbe semplicemente essere ignorato.

La “carta” di Hitler può poi essere semplicemente usata per far perdere le staffe all’avversario, che in preda all’ira non sarà più in grado di gestire razionalmente la discussione.

(da [fallacialogiche.it][1])

**La reductio a Pedoporno**

È fatto noto a chiunque segua con attenzione le discussioni riguardo alla privacy in rete ed alla censura, che l’argomento della pedopornografia viene spesso usato da governi e gruppi industriali per imporre leggi e regolamenti restrittivi e dannosi per i cittadini che usufruiscono della rete.

Innumerevoli volte, ormai, sono stati censurati e rimossi dalla rete applicazioni e siti web con l’accusa di essere veicoli per la circolazione di materiale perdopornografico. L’argomento della pedopornografia è usato come ariete di sfondamento da parte dell’industria dell’intrattenimento nella sua inutile battaglia contro la circolazione libera di materiale culturale ed artistico, che si tratti di semplici siti, motori di ricerca, programmi di scambio file alla pari (P2P). Lo stesso argomento viene utilizzato dai governi qualora desiderino una legittimazione popolare a delle disposizioni atte a violare sistematicamente la riservatezza dell’identità degli utenti in rete. Usando il Jolly del pedoporno, buona parte degli oppositori tendono ad arrendersi.

Ecco un esempio di applicazione della Reductio a Pedoporno:

– Ogni persona che pubblica materiale in rete dovrà fornire la propria reale identità. Tutti i fornitori di accesso alla rete dovranno registrare e mantenere registrata l’attività di ogni utente. Andranno bloccati e considerati aggravanti tutti i mezzi atti a nascondere la propria identità in rete. Dovranno essere resi illegali tutti i software di scambio file.

– Ma queste sono violazioni dei diritti dei cittadini. Chiunque ha diritto all’anonimato. In mancanza di esso, vi sarebbe anche una _limitazione della libertà d’espressione_, perché chiunque sia identificato o facilmente identificabile, non potrà comunicare in piena libertà per timore di minacce o ricatti. Questo impedirebbe di potersi esprimere con onestà. Le persone avranno insormontabili difficoltà a capire quali dati sono stati memorizzati su di loro e quale uso ne verrà fatto. Non avranno controllo su di essi e non potranno mai essere sicuri che non verranno usati per danneggiarli. I programmi di scambio file sono solamente dei mezzi che permettono la circolazione di ogni tipo di file, sollevando il carico da server centrali e riducendone il costo, permettendo così a tutti di poter offrire agli altri dei file, anche di grosse dimensioni, senza il costo dell’affitto di spazi a pagamento ed il conseguente obbligo di registrazione e tracciatura.

– Certo… ed ai bambini chi ci pensa?

– Cosa c’entra questo?

– Il pedopornoooooo…. Peeee-doooo-pooooo-rnooooooo!!!

– Arrgh no!! Ecco! Eccoti le chiavi di Internet. Facci tutto quello che vuoi, ma che non si dica in giro che ho difeso il pedoporno!

[1]:http://www.fallacielogiche.it/index.php?option=com_content&task=view&id=74&Itemid=86