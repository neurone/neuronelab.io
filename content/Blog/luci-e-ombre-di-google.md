title: Luci e ombre di Google
date: 2015-03-05 19:22:40 +0100
tags: libri, recensioni, rete, sicurezza, libertà, privacy, tecnologia
summary: Questo saggio, pubblicato dal collettivo Ippolita è acquistabile in libreria da Feltrinelli, oppure scaricabile da loro sito, in formato PDF.

Questo saggio, pubblicato dal collettivo [Ippolita][1] è acquistabile in libreria da Feltrinelli, oppure [scaricabile da loro sito][2], in formato PDF.

Il libro è pubblicato sotto licenza Creative Commons (by-nc-sa), ed è
scaricabile e ridistribuibile liberamente. La donazione non è obbligatoria, ma
ovviamente gradita.

È scritto molto bene e con grande competenza dei temi trattati. Non è
complottistico come potrebbe sembrare, ma mette ben in evidenza il ruolo
dell’azienda e la differenza che passa fra come essa si promuove e ciò invece
che veramente offre e quali siano i suoi obiettivi.

La tendenza ad intendere internet come il solo world wide web, prima, ed ora ad
accentrarne la fruizione quasi esclusivamente tramite gli strumenti forniti da
Google, è in sempre maggiore aumento. Nel tempo, anche Google ha aumentato la
sua pervasività, aggiungendo allo strumento di ricerca, altri servizi che vanno
a coprire ogni possibile utilizzo della rete, partendo dalla posta elettronica,
ad usenet (i newsgroup), ai blog, la gestione di servizi pubblicitari, le chat
testuali e vocali/video, la traduzione online di testi, applicazioni per ufficio
come word processor e foglio di calcolo, l'aggregazione di notiziari (feed
reader), la pubblicazione di libri, mappe geografiche, raccolte di foto,
applicazioni, social network, video, per finire (per ora) rendendo disponibili
dei propri server DNS. Attualmente, esiste la “versione Google” di quasi ogni
servizio e strumento legato alla rete. I dati raccolti da ognuno di questi
servizi vengono messi in relazione con gli altri, creando dei profili utente che
facilitano il filtraggio delle informazioni allo scopo di fornire nelle ricerche
i risultati più adatti, ma anche, soprattutto, le pubblicità più mirate tramite
AdWords.

Solo fintanto che si conosce cosa si utilizza e si ha ben chiaro l'uso che una
azienda privata fa dell'enorme quantità di dati che raccoglie dai suoi utenti ed
il modo in cui li mette in relazione, si può dire di fare un uso consapevole e
disilluso di ciò che apparentemente viene fornito gratuitamente.

Viene spiegato in modo chiaro il funzionamento dell'algoritmo PageRank ed in che
modo questo si traduce nell'utilizzo degli strumenti messi a disposizione.

La più grande illusione che l'utente medio ha, è che Google corrisponda alla
rete e che sia in grado di trovare ogni cosa esistente in modo neutrale e senza
filtri. Niente di più falso. È proprio grazie all'uso di filtri che il motore
di ricerca riesce a districarsi fra l’enorme quantità di dati memorizzata e
fornire all'utente una risposta rapida e presumibilmente soddisfacente. Se
Google non utilizzasse dei filtri per scremare, eliminando dai risultati i dati
che ritiene meno significativi per la ricerca effettuata, l'utente avrebbe
maggiori difficoltà nel trovare una risposta soddisfacente alla propria ricerca.
Questo, significa anche che i dati che il motore di ricerca restituisce
all'utente, non possono che essere parziali ed incompleti. L'utente, non avendo
altri strumenti per verificare ciò che gli viene mostrato (a meno di conoscere
già ciò che sta cercando ed i risultati corretti che dovrebbe ottenere), darà
per scontato che quello fornito da Google, sia il responso più completo ed
efficiente possibile.

Inoltre, il fatto che l'onere del filtraggio sia in mano ad un'azienda che si
pubblicizza come “buona”, non ci deve far illudere che tutto questo potere venga
sempre usato in modo etico e corretto.

La cronaca ha già mostrato in più di un'occasione, casi in cui l'azienda ha
utilizzato i propri filtri per scopi non proprio etici: ad esempio i filtri che
hanno inserito per il governo cinese, ed un filtraggio ad hoc che avevano fatto
durante un processo in cui l’azienda era coinvolta.

> il giorno dell’udienza per il caso American Blind, nel distretto di San José
> dove si teneva il processo i risultati di Google si mostravano
> misteriosamente differenti da ogni altro luogo nel mondo. Per la prima volta
> Google veniva colto a manipolare i risultati per fini diversi da quelli del
> “miglior risultato di una ricerca”. Il parere positivo della corte rispetto
> al caso con Geico (in tutto e per tutto analogo a quello con American Blind)
> fatica a cancellare questa realtà.

Il potere, costruito su tutti i dati di cui sono in possesso, lo hanno già, e
solo loro hanno la facoltà di decidere come utilizzarlo. Questo è il solito
problema esistente in tutti i sistemi nei quali i poteri sono accentrati.

***Un’analisi seria e approfondita sull’universo di Google e sull’industria dei
metadati.***

*Google si è affermato negli ultimi anni come uno dei principali punti di accesso
a Internet. Ci siamo adattati progressivamente alla sua interfaccia sobria e
rassicurante, alle inserzioni pubblicitarie defilate ma onnipresenti; abbiamo
adottato i suoi servizi e l’abitudine al suo utilizzo si è trasformata ormai in
comportamento: “Se non lo sai, chiedilo a Google”. Google ha saputo sfruttare
magistralmente il nostro bisogno di semplicità. Eppure ci troviamo di fronte a
un colosso, un sistema incredibilmente pervasivo di gestione delle conoscenze
composto da strategie di marketing aggressivo e oculata gestione della propria
immagine, propagazione di interfacce altamente configurabili e tuttavia
implacabilmente riconoscibili, cooptazione di metodologie di sviluppo del Free
Software, utilizzo di futuribili sistemi di raccolta e stoccaggio dati.
Il campo bianco di Google in cui inseriamo le parole chiave per le nostre
ricerche è una porta stretta, un filtro niente affatto trasparente che controlla
e indirizza l’accesso alle informazioni. In quanto mediatore informazionale
Google si fa strumento di gestione del sapere e si trova quindi in grado di
esercitare un potere enorme.
Cosa si nasconde dietro il motore di ricerca più consultato al mondo? Quello che
da molti era stato definito e osannato come il miglior strumento per districarsi
tra le maglie di Internet, pare celare molti segreti ai suoi utenti. Si va dalla
scansione delle e-mail del servizio Gmail alla indicizzazione proposta da Google
che sembra in realtà non dare tutte le risposte richieste dall’utente, fino a
ipotesi di violazione della privacy collettiva. Criticare Google attraverso una
disamina della sua storia, la decostruzione degli oggetti matematici che lo
compongono, il disvelamento della cultura che incarna significa muovere un
attacco alla tecnocrazia e alla sua pervasività sociale.*

[1]:http://www.ippolita.net/
[2]:http://www.ippolita.net/libro/luci-e-ombre-di-google
