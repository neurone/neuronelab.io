title: 38 Stratagemmi per ottenere ragione
date: 2015-04-08 21:16:54 +0200
tags: filosofia, dialettica
summary: Conoscere questi stratagemmi di Arthur Schopenhauer, usati solitamente da chi è interessato più a prevaricare, che non alla conoscenza che potrebbe acquisire in una discussione, vi sarà sicuramente utile per difendevi, riportando la discussione sui binari giusti, oppure semplicemente per troncarla prima che il sangue vi vada in ebollizione.

Conoscere questi [stratagemmi][1] di [Arthur Schopenhauer][2], usati solitamente da chi è interessato più a prevaricare, che non alla conoscenza che potrebbe acquisire in una discussione, vi sarà sicuramente utile per difendevi, riportando la discussione sui binari giusti, oppure semplicemente per troncarla prima che il sangue vi vada in ebollizione.

Secondo Schopenhauer la dialettica, o arte del disputare, deriva dalla «naturale
prepotenza del genere umano».

Testo ripreso da http://xoomer.virgilio.it/nowhere-now_here/38strata.html

1. **L’ampliamento**: Portare l’affermazione dell'avversario al di fuori dei
   suoi naturali limiti, interpretarla nella maniera più ampia e generale
   possibile ed esagerarla. Restringere invece la propria e circoscriverla nel
   senso più ristretto. Vedi n.23
2. **L’omonimia**: Usare l’omonimia, per estendere l’affermazione presentata a
   ciò che, al di là del nome uguale, poco o nulla ha in comune con la cosa in
   questione; poi darne una confutazione lampante, e così fingere di avere
   confutato l’affermazione.
3. **Relativo e assoluto**: Prendere l’affermazione presentata in modo relativo,
   come se fosse presentata universalmente, o almeno intenderla sotto tutt'altro
   aspetto e confutarla poi in questo secondo senso. Vedi n.19
4. **Partire da lontano**: Quando si vuol trarre una certa conclusione, non la
   si lasci prevedere, ma si faccia in modo che l’avversario ammetta senza
   accorgersene le premesse (o le premesse delle premesse) una per volta e in
   ordine sparso. Si occulti il proprio gioco finché non è stato ammesso tutto
   ciò di cui si ha bisogno. Vedi n.05, 09
5.  **Premesse false** … (04bis): Per dimostrare la propria tesi ci si può
    servire anche di premesse false, e ciò quando l’avversario non ammetterebbe
    quelle vere. Si prendano allora tesi in sé false ma vere ad hominem
    (concordanti con altre affermazioni e con verità soggettive e relative) e si
    argomenti ex concessis a partire dal modo di pensare
6.  **Postulare quel che si dovrebbe dimostrare**: 1) Ricorrendo a nomi o
    concetti interscambiabili. 2) Farci concedere in generale quel che è
    controverso nel particolare. 3) Quando viceversa 2 cose conseguono l’una
    dall’altra, si postula una per dimostrare l’altra. 4) Farsi ammettere ogni
    singolare per dimostrare l’universale.
7.  **Furbamente erotematici**: Domandare in una sola volta e in modo
    particolareggiato molte cose (nascondendo dove si vuole arrivare). Esporre
    invece rapidamente la propria argomentazione a partire da ciò che è stato
    ammesso.
8.  **Per gli irascibili**: Suscitare l’ira dell'avversario, tormentandolo e
    facendogli apertamente torto in modo sfacciato. Non sarà più in condizione
    di ragionare.. Vedi n.23, n.27
9.  **A balzi depistanti (04ter)**: Porre domande con spostamenti di ogni genere
    e servirsi delle sue risposte per trarre conclusioni diverse e perfino
    contrarie. Non dargli comunque modo di prevenire la nostra conclusione.
10. **Contrari**: Di fronte a risposte negative date di proposito
    dall’avversario, chiedere il contrario della tesi di cui ci si vuole servire
    per avere la sua approvazione. O almeno sottoporgli la tesi e il suo
    contrario per non fargli capire quale vogliamo che lui affermi.
11. **Indirettamente**: Non chiedere di concederci la verità generale se ci sono
    concessi dei singoli casi di un’induzione. Introdurla in seguito come già
    stabilita e concessa facendogli credere di avercela concessa. E così sarà
    per gli ascoltatori.
12. **Scelte appropriate**: In casi di concetto generale senza nome preciso,
    scegliere nomi o definizioni attraverso similitudini che siano favorevoli
    alla nostra affermazione. Stratagemma usato istintivamente nelle parole che
    si utilizzano...
13. **Accostamenti ad arte**: Presentare una tesi opposta a quella che vorremmo
    far accettare e far scegliere all’avversario. Ma l’opposto, esprimerlo in
    modo assai stridente e a rischio di paradosso cosicchè la nostra tesi appare
    la più probabile.
14. **Impertinenza**: Se l’avversario non arriva a favorire la conclusione che
    abbiamo in mente, la si enuncia e si esclama trionfanti come se fosse stata
    dimostrata.
15. **Massima impertinenza**: Se presentiamo una tesi paradossale che ci mette
    poi in difficoltà, sottoponiamo, come se ne volessimo trarne la
    dimostrazione, una tesi giusta ma non del tutto evidente: se l’avversario la
    respinge lo conduciamo /ad absurdum/ e trionfiamo; se la respinge, non tutto
    è perduto e potremmo ricorrere ora al n.14
16. ***Ad hominem o ex concessis***: Di fronte ad un’affermazione altrui
    dobbiamo cercare se non sia in qualche modo anche solo in apparenza, in
    contraddizione con qualche cosa che lui ha detto in precedenza o che
    qualcuno che lui approva ha lodato etc. Se difende il suicidio, gridargli
    subito: “perché non ti impicchi?”...
17. **Sottili salvataggi**: Incalzati da controprove, ci si può salvare con
    sottili distinzioni che non avevamo trovato prima, se la questione consente
    doppi casi o significati.
18. **Sabotaggi**: Se siamo di fronte ad argomentazioni altrui che ci
    batterranno, non consentire di concludere e formularle, interrompere e
    sviare per tempo, spostandosi su altre questioni. Vedi n.29
19. **Dal particolare al generale (03bis)**: Sollecitati a contrastare un
    determinato punto, portiamo la cosa sul generale e parliamo contro tali
    generalità.
20. **Omissioni**: Date e concesse dall'avversario le premesse, tiriamo noi
    direttamente la conclusione. Perfino se manca qualche necessaria premessa,
    la si assuma ugualmente concessa.
21. **Pan per focaccia**: Di fronte ad argomentazioni apparenti o sofistiche,
    meglio liquidarle con contro argomentazioni altrettanto sofistiche ed
    apparenti.
22. **Ribaltoni**: Spacciandola per una *petitio principii* rigettiamo la
    richiesta di ammettere una cosa da cui il problema in discussione
    conseguirebbe immediatamente.
23. **Esagerare (01bis + 08bis)**: Se una affermazione potrebbe essere vera in
    un particolare ambito, stuzzicare l’avversario per indurlo ad esagerare
    oltre il vero. E una volta confutata l’esagerazione è come aver confutata la
    partenza.
24. **Forzare la consequenzialità**: Dalla tesi avversa trarre a forza,
    attraverso false deduzioni e deformazioni, altre tesi anche non
    corrispondenti, ma assurde o pericolose. Facendo sembrare che queste
    discendano dalla sua tesi la si può agevolmente confutare. Vedi n.01
25. **Istanza**: Tra i tanti casi posti per porre un principio generale, Basta
    che si presenti un unico caso per il quale il principio non è valido, e
    questo è demolito. Attenzione agli inganni e alle apparenze.
26. ***Retorsio agumenti***: Quando l’argomento che l’avversario vuole usare può
    essere usato meglio contro di lui. “è un bambino, gli si conceda pur
    qualcosa”; /retorsio/ “Proprio perché un bambino bisogna …(a scelta)”
27. **Implacabili (08ter)**: Se l’avversario ad un certo punto inaspettatamente
    si adira, avete toccato un punto debole ed occorre insistere senza tregua.
28. **Talvolta**: Quando dei colti disputano di fronte ad incolti, avanzare
    obiezioni di cui solo un esperto vede l’inconsistenza; agli occhi degli
    ascoltatori incolti l’obiezione è valida e batte l’interlocutore che può
    perfino essere facilmente messo in ridicolo. Chi ride è dalla propria parte
    facilmente. L’avversario non riuscirà a ribattere sensatamente e soprattutto
    altrettanto in breve per trovare ascolto.
29. **Diversione (18bis)**: Accorti di essere battuti si fa di colpo una
    diversione e si comincia di colpo a parlare d’altro come se fosse pertinente
    ed efficace per confutare l’avversario. Senza ritegno si potrà poi parlare
    non della questione ma dell’avversario stesso.
30. ***Argumentum ad verecundiam***: Invece delle motivazioni, ci si serva di
    autorità, secondo le conoscenze dell’avversario. Si ha buon gioco ricorrendo
    ad autorità rispettate dall’avversario. All’occorrenza le autorità si
    possono non solo distorcere, ma addirittura falsificare o perfino inventare.
    Notare che la gente vede molte autorevoli personalità ed ha profondo
    rispetto per gli esperti di ogni genere. Oltre ad accettare come vere
    opinioni che sono vere solo perché universalmente accettate …
31. **Ironie (30bis)**: Se non si sa opporre nulla alle ragioni esposte
    dall’avversario, ci si dichiari con fine ironia, incompetenti e “sarà
    senz’altro giustissimo quel che dice, ma non si capisce” e si rinuncia ad
    esprimersi. Il tiro contrario è “Mi permetta, con il Suo acume dev’essere
    una inezia capirlo, sarà per colpa della mia cattiva esposizione” e poi
    sbattergli la cosa sul muso in modo che piaccia o no, debba capirla. E
    risulti chiaro che prima effettivamente era lui a non capirla.
32. **Insinuare sospetti**: Per accantonare o almeno rendere sospetta una
    affermazione a noi contraria, ricondurla ad una categoria odiata. Anche se
    la relazione è vaga e tirata per i capelli. Con ciò supponiamo: che
    l’affermazione è risaputa ed è già stata ampiamente confutata …
33. **Vero in teoria, falso in pratica**: Un sofisma che ammette le ragioni e
    tuttavia nega le conseguenze. Questo contraddice la regola: da una ragione
    al suo effetto vige la consequenzialità.
34. **Insistere**: Un avversario che a una domanda o argomento non dà una
    risposta diretta, non prende posizione precisa, evade in vari modi … questo
    dimostra che abbiamo toccato un punto marcio. Insistervi senza sosta.
35. **Schopenhauer è tutto qui**: Funziona solo in circostanze particolari ma
    rende superflui tutti gli altri: anzichè agire sull’intelletto con dei
    ragionamenti, si agisca sulla volontà con motivazioni e l’avversario o gli
    uditori, se hanno gli stessi suoi interessi, saranno conquistati alla nostra
    opinione fosse anche presa dal manicomio. Per lo più, infatti, una briciola
    di volontà pesa più di un quintale di giudizio e persuasione. Se
    l’avversario avverte che la validità della sua opinione arrecherebbe
    notevole danno al suo stesso interesse, la lascerà cadere come un ferro
    bollente. Funziona anche con uditori, e non l’avversario, che hanno un
    interesse comune; a loro le sue tesi appariranno deboli e miserabili anche
    se ottime.
36. **Sbigottire con sproloqui**: Sconcertare, sbigottire l’avversario con
    sproloqui insensati. Se è consapevole della propria debolezza, si
    impressiona e si può perfino spacciarla come la prova più incontestabile
    della propria tesi.
37. **Uno dei primi**: Quando l’avversario pur avendo ragione sceglie per
    fortuna una prova sbagliata, non avendo difficoltà a confutarla, spacciamo
    questa per una confutazione della cosa. In fondo spacciamo un *argumentum ad
    hominem* per uno *ad rem*
38. **Ultimo**: Quando ci si accorge che l’avversario è superiore e finiremo
    male, si diventi offensivi, oltraggiosi, grossolani. Si passi dall’oggetto
    del contendere al contendente attaccando la persona. Si tratta di un appello
    delle forze dello spirito a quelle del corpo o all’animalità. Una regola
    molto popolare. Ma come rispondere? Non basta evitare di essere offensivi
    perché mostrando a uno con calma che ha torto, e dunque pensa e giudica in
    maniera sbagliata, lo si amareggia più che con qualsiasi espressione
    oltraggiosa. Perché … “ogni piacere dell’animo e ogni ardore risiedono
    nell’avere qualcuno, dal confronto con il quale si possa trarre un alto
    sentimento di sé “. La vanità richiede soddisfazioni e nessuna ferita duole
    più di quelle che la colpiscono. Di qui l’amarezza dello sconfitto e il
    ricorso a quest’ultimo stratagemma dove il sangue freddo è d’aiuto. Si
    risponde con calma e senza badare alle offese si ritorna sulla cosa in
    questione.

- [L'arte di ottenere ragione][1]
- [Arthur Schopenhauer][2]

[1]:https://it.wikipedia.org/wiki/L'arte_di_ottenere_ragione
[2]:https://it.wikipedia.org/wiki/Arthur_Schopenhauer
