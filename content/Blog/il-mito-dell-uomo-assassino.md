title: Il mito dell'uomo assassino
date: 2018-04-28 16:35
tags: antropologia, politica, società, psicologia, violenza, etica

Articolo originale: [The Myth of Man the Killer][1], di [Eric Raymond][2] (12 agosto 2009)

Uno degli sbagli più pericolosi del nostro tempo, sta nella credenza che gli esseri umani siano animali unicamente violenti, frenati a malapena dal commettere atrocità l'uno sull'altro dalle restrizioni etiche, religiose e dello stato.

Ad alcuni sembrerà strano contestare quest'idea, dato l'apparentemente incessabile flusso di notizie di atrocità da Bosnia, Somalia, Libano e Los Angeles, di cui soffriamo ogni giorno. Ma, nei fatti, qualche studio di etologia animale (e l'applicazione di alcuni metodi etologici al comportamento umano) è sufficiente per mostrare ad una mente imparziale che gli esseri umani non sono animali particolarmente violenti.

Desmond Morris, nel suo affascinante libro _L'uomo e i suoi gesti: la comunicazione non-verbale nella specie umana_, ad esempio, mostra che lo stile di lotta istintivo degli esseri umani sembra essere attentamente ottimizzato per prevenire di ferirci l'un l'altro. I filmati sulle risse di strada mostrano che il combattimento "istintivo" consiste principalmente di spintoni e colpi nell'area della testa/spalle/gabbia toracica.

È decisamente difficile ferire seriamente un essere umano in questo modo; le aree bersaglio favorite sono per lo più ossa, e lo stile istintivo del colpo produce una forza piuttosto ridotta rispetto allo sforzo fatto. Il confronto tra questo goffo comportamento e l'attacco mirato ai tessuti molli dei lottatori di arti marziali (che hanno imparato a scavalcare l'istinto) che può uccidere sul colpo, è illuminante.

È anche un fatto ben conosciuto ai pianificatori militari, che circa il 70% delle truppe al proprio primo combattimento a fuoco si ritrovano ad essere paralizzate, incapaci di sparare con un'arma letale a dei nemici vivi. È necessario un allenamento ed un intenso lavoro di ri-socializzazione per ricavare dei soldati da delle grezze reclute. Un punto importante, su cui torneremo più tardi, è che la suddetta socializzazione si deve concentrare nell'ottenere un apprendista che obbedisca agli ordini e si identifichi col gruppo (Il Maggiore David Pierson, dell'esercito statunitense scrisse un [saggio illuminante][3] sull'argomento nel numero di Giugno 1999 di _Military Review_).

La violenza criminale è fortemente legata alla sovrappopolazione ed allo stress, condizioni che qualsiasi ricercatore sa possono rendere pazzo anche un topo da laboratorio. Per vedere chiaramente la differenza, confrontate una rivolta urbana con le reazioni post-uragano o post-alluvione in aree rurali. Di fronte ad un disastro comune, è più tipico per gli umani cooperare, che competere.

Gli individui umani, al di fuori di una piccola minoranza di sociopatici e psicopatici, non sono affatto degli assassini nati. Perché, allora, la credenza nell'innata malvagità umana è così pervasiva nella nostra cultura? E quale prezzo paghiamo per questa credenza?

---

Le radici storiche di questa credenza non sono difficili da tracciare. La storia della creazione Giudaico-Cristiana afferma che l'essere umano esiste in uno stato di immoralità e peccato; e la Genesi racconta di due grandi atti di rivolta contro Dio, il secondo dei quali è il primo assassinio. Caino uccide Abele, e noi ereditiamo il "marchio di Caino", ed il mito di Caino - la credenza che nel profondo siamo tutti, in qualche modo, assassini.

Fino al ventesimo secolo, la Cristianità Giudaica tendeva a concentrarsi sul primo; la mela del Serpente, popolarmente, se non teologicamente, equiparata alla scoperta della sessualità. Ma avendo i taboo sessuali perso la loro vecchia forza proibitiva, il "marchio di Caino" è diventato relativamente più importante nell'idea Giudaico-Cristiana di "peccato originale". Le stesse chiese e sinagoghe che benedivano le "guerre giuste" nei secoli passati sono diventate roccaforti del pacifismo ideologico.

Ma c'è una seconda, forse più importante fonte del mito dell'uomo-assassino nella filosofia dell'Illuminismo - la descrizione dello stato di natura da parte di Thomas Hobbes come di "guerra di tutti contro tutti", ed il naturismo reazionario di Rousseau e dei Romantici post-Illuminismo. Oggi, queste visioni del mondo inizialmente contrapposte si sono fuse in una visione della natura che gli antropologi chiamano società pre-statali; questo esatto termine, nei fatti, riflette il mito Hobbesiano.

L'ovvio errore nell'argomento di Hobbes è che prese come necessaria una condizione per sopprimere la "guerra" (l'esistenza di un forte stato centrale), anziché solo sufficiente. Lui sottovalutò l'innata socialità degli esseri umani. Le registrazioni storiche ed antropologiche offrono numerosi esempi di società "pre-stato" (anche popolazioni multietniche/multilingue abbastanza grandi) le quali, seppur violente verso gli esterni, mantennero una pace interna.

Se Hobbes sottovalutò la socialità dell'uomo, Rousseau ed i suoi seguaci la sopravvalutarono; o, almeno, sopravvalutarono la socialità dell'uomo _primitivo_. Contrapponendo la nobiltà e tranquillità che affermavano di vedere nella natura rurale e nel Buon Selvaggio con la fin troppo evidente sporcizia, povertà ed affollamento delle città in forte espansione della Rivoluzione Industriale, essi secolarizzarono la Caduta dell'Uomo. Come i propri attuali discendenti spirituali ancora fanno, trascurarono il fatto che il povero urbano votò all'unanimità con mani e piedi per fuggire da una ancor peggiore povertà rurale.

Il mito Rousseiano dell'uomo tecnologico visto come orrenda verruca sull'incontaminata faccia della Natura è diventato così pervasivo nella cultura occidentale da scalzare ampiamente dalla mente popolare la vecchia immagine contrapposta della "Natura, dalle rosse zanne ed artigli". Forse ciò era inevitabile mano a mano che l'umano otteneva sempre maggior controllo sull'ambiente; la protezione da carestia, piaghe, tempo avverso, predatori, ed altri inconvenienti della natura, incoraggiò la profonda delusione che sia solo la cattiveria umana a rendere il mondo un posto difficile.

Fino al tardo diciannovesimo ed al primo ventesimo secolo, la visione Rousseiana dell'uomo e della natura era un lusso confinato agli intellettuali ed ai ricchi agiati. Solo man mano che l'aumento dell'urbanizzazione e della ricchezza media aumentarono l'isolamento della maggior parte della società dalla natura, essa diventò la base acritica e disarticolata di credenze popolari ed accademiche. (Nel suo libro _War Before Civilization_, Lawrence Keeley ci ha dato una tagliente analisi del modo in cui il mito Rousseiano ha ridotto ampie fasce di antropologia culturale in cieche assurdità.)

In realtà, la Natura è una violenta arena di competizioni intra- e inter-specie nella quale l'assassinio è un evento quotidiano e le fluttuazioni ecologiche portano a morti di massa. Le società umane, fuori dai periodi di guerra, sono, per contrasto, miracolosamente quasi del tutto stabili e nonviolente. Ma il pregiudizio inconscio anche dei più istruiti occidentali, oggi, è che sia vero l'esatto opposto. La visione Hobbesiana della "guerra di tutti contro tutti" è sopravvissuta solo come descrizione del comportamento _umano_, anziché del più ampio stato della natura. L'ecologia pop ha rimpiazzato la teologia pop; il nuovo mito è quello dell'uomo come scimmia assassina.

All'opera c'è anche un altra più cupa forma di romanticismo. Ad una persona che si sente fondamentalmente impotente, la credenza di essere dopotutto, intrinsecamente, micidiale può diventare una gradita illusione. Chi la propaganda sa bene che le fantasie di violenza non vendono bene ai saggi ed ai ricchi, ma piuttosto a lavoratori intrappolati in lavori senza sbocchi, ad adolescenti frustrati, a disoccupati - le persone ai margini, quelle sole e perse.

Per queste persone, il mito della scimmia assassina è una consolazione. Quando tutto il resto fallisce, essa offre la cupa promessa di un'ultima carneficina, di scatenare il mitologico assassino interno per esprimere tutta l'esasperazione in una catarsi di vendetta sanguinosa. Ma se sette umani su dieci non sono capaci di premere il grilletto contro un nemico di cui hanno tutte le ragioni per credere che stia cercando di ucciderli, sembra improbabile che novantasette su cento possano trasformarsi in assassini.

Ed infatti, meno della metà dell'un percento dell'attuale popolazione mondiale uccide in tempo di pace; gli omicidi sono di più di un ordine di grandezza meno comuni degli incidenti mortali domestici. Complessivamente, tutti, tranne un quasi inesistente piccolo numero di omicidi, sono messi in atto da maschi tra i 15 ed i 25[(1)](#n1) anni, e la stragrande maggioranza di questi da maschi _celibi_. Le probabilità di venire uccisi da umani al di fuori di questa parentesi demografica sono paragonabili a quelle di venire uccisi da un fulmine.

---

La guerra è la grande eccezione, la grande legittimante d'omicidi, quell'arena nella quale degli umani qualunque diventano regolarmente degli assassini. La prevalenza speciale del mito della scimmia-assassina, nel nostro tempo, deve senza dubbio qualcosa all'orrore ed alla visibilità della guerra del ventesimo secolo.

Le campagne di genocidi e repressione come l'Olocausto Nazista, le carestie Staliniane, i massacri degli Ankha in Cambogia, e le "pulizie etniche" in Jugoslavia si delineano sempre più nella mente popolare a supporto del mito dell'uomo assassino. Ma non dovrebbero; tali atrocità sono invariabilmente concepite e pianificate da una selezionata e piccolissima minoranza che consiste in meno del 0,5% della popolazione.

Abbiamo visto che in circostanze normali, gli esseri umani non sono assassini; e, infatti, la maggior parte di essi ha degli istinti tali da rendere loro estremamente difficile ingaggiarsi in letale violenza. Come possiamo conciliare questo con lo schema ricorrente della violenza umana nella guerra? E, per ribadire una delle nostre domande iniziali, cosa comporta per noi il mito dell'uomo assassino?

Ci accorgeremo preso che le risposte a queste due domande sono intimamente legate - perché c'è una comunanza cruciale tra la guerra ed il genocidio, che non è condivisa con la relativamente trascurabile letalità dei criminali e degli individui mentalmente malati. Sia la guerra che il genocidio dipendono, criticalmente, dall'_abitudine ad uccidere a comando_. Pierson osserva, eloquentemente, che le atrocità "sono generalmente avviate da tipi di personalità sovra comandate in posizioni di secondo-in-comando, e non da tipi di personalità sottocontrollate." Anche il terrorismo dipende dall'abitudine all'obbedienza; non è Osama bin Laden ad essere morto negli attacchi del 9/11, ma i suoi tirapiedi.

Questa è parte di ciò che Hannah Arendt descriveva quando, dopo i processi di Nuremberg, scrisse la sua indimenticabile frase "la banalità del male". L'istinto che facilitò le atrocità a Belsen-Bergen, Treblinka e Dachau non era la delizia di macchiarsi le mani col sangue dell'omicidio, ma piuttosto la sottomissione acritica nei confronti degli ordini di maschi alfa - anche quando questi ordini erano di orrore e morte.

Gli esseri umani sono primati sociali con istinti sociali. Uno di questi istinti è la docilità, una predisposizione ad obbedire al capo tribù ed agli altri maschi dominanti. Questo era originalmente un adattamento; meno combattimenti per la propria posizione significavano più corpi abili nella tribù o nelle squadre di caccia. Era particolarmente importante che i maschi celibi tra i 15 ed i 25 anni obbedissero agli ordini anche quando questi comportavano rischi ed uccisioni. Questi celibi erano i cacciatori, i guerrieri, gli esploratori e coloro che affrontavano i rischi per la tribù; una squadra cresceva meglio quando i membri erano sia aggressivi verso gli esterni che suscettibili al controllo sociale.

Lungo la maggior parte della storia evolutiva umana, l'effetto moltiplicatore della docilità era limitato a piccoli gruppi di unità umane (250 o meno, solitamente molto meno). Ma quando un singolo maschio alfa od un gruppo di maschi alfa cooperanti tra loro poterono comandare l'aggressività dei maschi celibi di una grande città o di un'intera nazione, le regole cambiarono. La guerra ed il genocidio divennero possibili.

In realtà, né la guerra né il genocidio richiedono più di una comparativa manciata di assassini - una coorte non più grande della metà percentuale di quella percentuale che commette violenze letali in tempo di pace. Entrambi, tuttavia, richiedono l'_obbedienza_ di una vasta popolazione al sostegno. Le fabbriche devono lavorare fuori orario. I camion di munizioni devono venire condotti dove sono richieste pallottole. La popolazione deve accordarsi a non guardare, non sentire e non notare certe cose. _Gli ordini venire essere obbediti._

Gli esperimenti descritti nel libro del 1974 di Stanley Milgram "Obbedienza all'autorità: uno sguardo sperimentale" dimostrarono come delle persone altrimenti etiche potessero venire indotte a torturare attivamente un'altra persona alla presenza di una figura autoritaria che comandasse e legittimasse quella violenza. Essi sono ancora tra i più potenti e disturbanti risultati della psicologia sperimentale.

Gli esseri umani non sono degli assassini nati; davvero pochissimi imparano a gioire dell'omicidio o della tortura. Gli umani, tuttavia, sono sufficientemente docili, tanto che molti possono infine venire addestrati ad uccidere, a sostenere l'omicidio, o ad acconsentire as uccidere a comando da un maschio alfa, dissociando completamente se stessi dalla responsabilità dell'atto. Il nostro peccato originale non è l'assassinio - è l'_obbedienza_.

---

E questo ci porta all'ultimo motivo della prevalenza del mito dell'uomo assassino; cioè che esso incoraggia e legittima il controllo sociale dell'individuo. L'uomo che teme la "guerra" di Hobbes, colui che vede in ognuno dei propri vicini un potenziale omicida, si arrenderà a qualsiasi cosa pur di venir protetto da essi. Chiederà l'aiuto di una forte mano dall'alto; diventerà lo strumento volontario dell'oppressione dei propri compagni. Di fatto, potrebbe anche acconsentire a trasformare se stesso in un assassino. La società viene ridotta in milioni di atomi, frammenti spaventati, ognuno che reagisce alla paura di una violenza individuale immaginaria patrocinando le stesse condizioni politiche che permettono su larga scala la violenza vera.

Anche quando la paura della violenza è meno acuta, il mito dell'uomo assassino ben serve alle potenti elite di ogni tipo. Definire il problema centrale della società come alla repressione di una tendenza universale individuale alla violenza è, implicitamente, una soluzione autoritaria; è voler negare senza alcun esame la tesi che gli interessi individuali e la cooperazione volontaria sono sufficienti all'ordine civile. (Per citare un esempio attuale, il mito dell'uomo assassino è una delle premesse non verificate che stanno alla base delle regolamentazioni sulle armi private.)

In sintesi, il mito dell'uomo assassino degrada ed infine rende impotente l'individuo, e svia inutilmente l'attenzione dai meccanismi _sociali_ e dagli istinti _sociali_ che sottendono virtualmente a tutta la violenza. Se fossimo tutti davvero degli assassini innati, nessuno sarebbe responsabile, e la sporadica violenza del crimine ed il terrorismo e la più sistematica violenza dei governi (che sia nelle società "statali" o "pre-statali", ed in tempo di guerra o altrimenti) sarebbe inevitabile come il sesso.

D'altra parte, se riconoscessimo che la maggior parte della violenza (e _tutta_ la violenza su larga scala) nasce dall'obbedienza, e specialmente dal commissionare la violenza aggressiva ai maschi celibi comandati dai maschi alfa capibranco, allora potremmo iniziare a fare delle domande più produttive. Come: cosa possiamo fare, culturalmente, per distruggere questo concatenamento di cause?

Prima, dobbiamo distinguere l'origine e la portata del problema. In ogni caso, la forma preminente di violenza aggressiva è quella dei governi, sia nella forma esplicita della guerra e del genocidio, che nelle versioni più o meno nascoste in tempo di pace. Prendete come indicatore la più pessimistica stima sul numero di morti da aggressione privata del ventesimo secolo e confrontatela con le stime di fascia più bassa sulle morti causate dalla violenza promossa dai governi (cioè, contate solo le morti in guerra, i genocidi deliberati, e le violenze extra-legali degli organi del governo; non contate le morti incorse nell'attuazione delle leggi anche più dubbie ed oppressive). Anche considerando queste premesse che spingono il rapporto verso il basso, esso sarà certamente di 1000:1 o peggio.

I lettori scettici su questo rapporto dovrebbero riflettere sul fatto che i soli genocidi comandati dai governi (escludendo interamente le guerre) sono stimati all'ammontare di più di 250.000.000 di morti tra il massacro degli Armeni nel 1915 e le "pulizie etniche" di Bosnia e Rwanda-Burundi dei tardi anni '90. La stessa atrocità del 9/11 ed altri atti di terrorismo, per quanto possano essere stati  truci, non sono che gocce in confronto gli oceani di sangue versati dalle azioni dello stato.

Difatti, il dominio dell'intera violenza di branco dell'aggressione dei governi supera anche ciò che rapporto di 1000:1 vorrebbe indicare. La violenza di branco dei governi serve come modello e scusa legittimante, non solo alla mera violenza di altri governi, ma anche alla violenza privata. La cosa in comune a tutti i tiranni è che la loro credenza nella _propria_ speciale causa, giustifichi l'aggressione; i criminali privati imparano e traggono profitto da questo esempio. Il contagio della violenza di massa viene diffuso dalle stesse istituzioni che basano la propria legittimità nella missione di sopprimerla - anche quando ne perpetuano la gran parte.

E ciò è il motivo principale per cui il mito della scimmia assassina è pericoloso. Perché quando tremiamo di fronte allo spettro della violenza individuale, scusiamo o incoraggiamo la violenza sociale; alimentiamo i miti autoritari e l'auto indulgenza che permisero la costruzione dei campi di morte nazisti e dei gulag sovietici.

Non c'è speranza che a breve termine possiamo modificare l'aggressività o la docilità nel genoma umano. E la violenza su piccola scala dei criminali e dei malati è una mera distrazione dall'orripilante e vasta realtà dell'omicidio approvato dal governo e della minaccia di omicidio approvata dal governo.

Per affrontare il vero problema in modo efficace, dobbiamo quindi cambiare le nostre culture in modo che i maschi alfa che si fanno chiamare "governo" smettano di ordinare le aggressioni, oppure i nostri maschi celibi smettano di seguire quegli ordini. Né il consiglio di obbedienza allo stato di Hobbes, né l'idolatria verso l'uomo primitivo di Rousseau sono in grado di affrontare il problema centrale della violenza nell'era moderna - cioè le morti di massa promosse dallo stato.

Per fermare questa calamità, dobbiamo passare oltre al mito dell'uomo assassino ed imparare nuovamente a fidarci della coscienza individuale ed a responsabilizzarla; riconoscere ed affermare la predisposizione _individuale_ nel compiere scelte pacifiche del 97% non sociopatico della popolazione; e considerare ciò che Stanley Milgram ci ha mostrato: che sul cartello che indica la strada che conduce lontano dalla violenza di massa c'è scritto _"Io non obbedirò!"_

---

<a name="n1"></a>1) Un oppositore ha contestato la mia affermazione che i maschi celibi commettano una tutt'altro che piccola minoranza degli omicidi, evidenziando che una statistica del 2001 del Uniform Crime Report indica che solo il 45% degli omicidi la cui età del committente sia stata registrata è stata commessa da maschi tra i 15 ed i 25 anni. Sfortunatamente per la sua obiezione, l'UCR è affetto da un implicito errore di campionamento; esso non include i risultati di quel genere di violenza civile endemica, rara nei paesi ricchi, ma fin troppo comune nel Terzo Mondo. In generale, pare il caso che l'insieme dei maschi celibi sia contemporaneamente la più incline alla violenza e la meno resitente al controllo sociale - tanto che in società e contesti con meno norme effettive contro la violenta (e più alti livelli complessivi di essa) la quota perpetrata dai maschi celibi salga _di molto_. Al contrario, il tasso di sfondo della violenza praticata da persone che non sono maschi celibi, cambia meno. Chiunque abbia dubbi su questo è invitato a studiare (per dire) le registrazioni delle rivolte di L.A. e chiedersi quale sia la fascia demografica prevalente.

---

**Note a margine del traduttore**

Altri articoli a tema _violenza_ sul blog:

+ [In incognito in un macello][4]
+ [Cosa pensa chi vuol diventare soldato][5]
+ [Kropotkin non era pazzo][6]

[1]:http://catb.org/~esr/writings/killer-myth.html
[2]:http://catb.org/~esr
[3]:http://web.archive.org/web/19991008231638/http://www-cgsc.army.mil/milrev/English/MayJun99/Pierson.htm
[4]:{filename}/Blog/in-incognito-in-un-macello.md
[5]:{filename}/Blog/cosa-pensa-chi-vuol-diventare-soldato.md
[6]:{filename}/Blog/kropotkin-non-era-pazzo.md
