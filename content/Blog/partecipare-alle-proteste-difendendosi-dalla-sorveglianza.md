title: Partecipare alle proteste difendendosi dalla sorveglianza
date: 2017-07-22 17:22
tags: privacy, tecnologia, rete, sicurezza

Traduzione dell'articolo [Attending Protests (International) - Surveillance Self-Defense][1] dal sito della _Electronic Frontier Foundation_

Grazie alla proliferazione delle tecnologie ad uso personale, i manifestanti di qualsiasi ideologia politica stanno documentando sempre più le proprie manifestazioni - e gli incontri con la polizia - utilizzando dispositivi elettronici come fotocamere e telefoni cellulari. In alcuni casi, ottenere quel singolo scatto della polizia antisommossa che viene dritta verso di te postato da qualche parte in internet diventa un'azione d'eccezionale potenza e può attirare l'attenzione in modo vitale verso la tua causa. Quelli che seguono sono dei suggerimenti utili da ricordare quando ti troverai ad una manifestazione e dovrai preoccuparti di proteggere i tuoi dispositivi elettronici se, o quando, verrai interrogato, trattenuto o arrestato dalla polizia. Ricordati che questi suggerimenti sono delle linee guida generiche, quindi se hai delle preoccupazioni particolari, sei pregato di parlare con un avvocato.

Preparare il tuo dispositivo personale per una manifestazione
-------------------------------------------------------------

Pensa attentamente a ciò che hai nel tuo telefono prima di portarlo ad una manifestazione. Il tuo telefono contiene una grande quantità di dati privati, che possono includere la tua lista di contatti, le persone che hai chiamato recentemente, i tuoi messaggi di testo ed email, foto e video, dati GPS sulla posizione, la tua cronologia di navigazione e password, ed il contenuto dei tuoi account su social media. Attraverso le password memorizzate o i login attivi, l'accesso al tuo dispositivo può permettere a qualcuno di ottenere ancora più informazioni sui server remoti. (Puoi preventivamente disconnetterti da questi servizi).

In molti paesi, è richiesto registrare la propria SIM card quando si acquista un telefono cellulare. Se porti il tuo cellulare ad una manifestazione, rendi semplice per il governo capire che tu sei lì. Se devi partecipare ad una protesta sconosciuta dal governo e dalle forze dell'ordine, copriti la faccia in modo che sia più difficile identificarti nelle foto. Comunque, tieni presente che mascherarsi potrebbe metterti nei guai in alcuni luoghi dove vige una legge anti-maschere (in Italia: legge 152/1975 art. 5). Inoltre, _non portare con te il tuo cellulare_. Se dovessi assolutamente portarne uno, cerca di portarne uno che non sia registrato col tuo nome.

Per proteggere i tuoi diritti, potresti desiderare rendere più difficili le perquisizioni sui tuoi telefoni già esistenti. Potresti anche prendere in considerazione di portare alla manifestazione un telefono usa e getta o alternativo che non contenga dati sensibili, che non hai mai usato per collegarti ai tuoi account social o di comunicazione, e che non ti importerebbe di perdere o di separartene per un po'. Se hai molte informazioni sensibili o personali sul tuo telefono, la seconda soluzione è probabilmente la migliore.

**Protezione con password e crittografia:** Proteggi sempre il tuo telefono con una password. Ma anche se la protezione con password è una piccola barriera al suo accesso, teni presente che la sola password o bloccare il telefono _non_ è una barriera efficace in caso di analisi forense fatta da esperti. Sia Android che iPhone forniscono da sistema operativo opzioni per criptare completamente la memoria, e dovresti usarle, anche se l'opzione più sicura rimane sempre di lasciare il telefono altrove.

Un problema con la crittografia sui cellulari è che su Android la stessa password usata per criptare la memoria viene usata anche per il blocco dello schermo. È un problema di cattiva progettazione, perché obbliga l'utente a scegliere una password troppo debole per la cifratura, oppure una password troppo lunga e scomoda per il blocco schermo. Il miglior compromesso potrebbe essere una password di 8-12 caratteri casuali che sia anche semplice da scrivere velocemente sul tuo particolare dispositivo. Oppure, se hai accesso di root al tuo dispositivo Android e sai come usare la riga di comando, leggi [qua][2] per le istruzioni su come impostare una password (più lunga) separata per la cifratura della memoria. (Vedi anche "[Communicating with Others][3]" per i dettagli su come criptare i messaggi di testo e le chiamate vocali.)

**Fa un backup dei tuoi dati:** È importante che tu faccia dei backup frequenti dei dati memorizzati sul tuo telefono, specialmente se il tuo dispositivo finisce nelle mani di un ufficiale di polizia. Potresti non riavere indietro il tuo cellulare per un po' (o non riaverlo più) ed è possibile che il suo contenuto venga cancellato, intenzionalmente o meno.

Per simili ragioni, prendi in considerazione di scriverti sul corpo con un pennarello indelebile un numero di telefono importante, ma che non sia incriminante, nel caso tu perda il tuo telefono, ma ti sia permesso di fare una chiamata.

**Informazioni sulla tua posizione della tua cella:** Se porti con te il telefono ad una manifestazione, il governo può facilmente capire che sei lì chiedendo l'informazione al tuo operatore. ([Noi crediamo][4] che i governi dovrebbero ottenere un mandato individuale per ottenere informazioni sulla posizione, ma i governi spesso non sono d'accordo). Se devi tenere nascosta al governo la tua partecipazione alla manifestazione, non portare con te il telefono. Se devi assolutamente portarne uno, fa che non sia registrato col tuo nome.

Se temi di poter venire arrestato ad una protesta, la pratica migliore è di preparare in anticipo un massaggio da mandare ad un amico fidato che sia in un posto sicuro. Scrivi il tuo messaggio di testo per questa persona in anticipo e mettilo in coda così da poterlo inviare rapidamente in casi di emergenza e far sapere a lui che sei stato arrestato. In modo simile, ti puoi organizzare per chiamare un amico alla fine della protesta, in modo che se lui non ricevesse la chiamata, capirebbe che sei stato arrestato.

Oltre ad essere messo al corrente che il tuo telefono è stato sequestrato e che sei in arresto, questo amico fidato dovrebbe essere in grado di cambiare le password dei tuoi account email e social nel caso tu venga obbligato e cedere le tue password alle autorità.

> Tieni presente che nascondere o distruggere delle prove può essere considerata un'azione illegale in alcune giurisdizioni (comprese molte democrazie).

Assicurati che tu ed il tuo amico conosciate bene la legge ed i rischi prima di affidarvi a questo piano. Ad esempio se stai manifestando in un paese con una forte tradizione dello stato di diritto dove manifestare non costituisce crimine in sé, potrebbe accadere che cospirare per tagliare fuori la polizia dai tuoi account ti porti ad infrangere la legge, mentre altrimenti avresti potuto cavartela senza alcuna incriminazione. D'altro canto, sei sei preoccupato per la salute fisica tua e dei tuoi colleghi nelle mani di una milizia incontrollata, proteggere l'identità dei tuoi amici ed i tuoi dati potrebbe essere una priorità maggiore che non collaborare con le indagini.

Sei alla manifestazione - Ed ora?
---------------------------------

Una volta arrivato alla manifestazione, tieni a mente che le forze dell'ordine potrebbero monitorare tutte le comunicazioni nell'area. Potresti voler criptare le tue chat con [ChatSecure][5], oppure le tue conversazioni e messaggi usando Signal.

> Ricorda che anche se le tue comunicazioni sono criptate, i tuoi [metadati][6] non lo sono; il tuo cellulare continuerà a trasmettere la tua posizione ed i metadati sulle tue comunicazioni, come con chi stai parlando e per quanto tempo.

Se vuoi mantenere la tua identità e la tua posizione segrete, assicurati di [rimuovere tutti i metadati dalle tue foto][7] prima di postarli.

In altre circostanze, i metadati potrebbero essere utili per dimostrare la credibilità di prove catturate durante una manifestazione. Il Guardian Project produce uno strumento chiamato [InformaCam][8] che permette di memorizzare i metadati compresi delle informazioni sulla posizione nelle coordinate GPS dell'utente, altezza, direzione cadrinale, lettura del sensore di luminosità, firma dei dispositivi vicini, torri cellulari, e reti WiFi; e serve per mettere in luce le esatte circostanze ed il contesto in cui l'immagine digitale è stata scattata.

[1]:https://ssd.eff.org/en/module/attending-protests-international
[2]:https://code.google.com/p/android/issues/detail?id=29468
[3]:https://ssd.eff.org/en/module/communicating-others#overlay=en/node/43/
[4]:https://www.eff.org/issues/cell-tracking
[5]:https://ssd.eff.org/en/module/how-install-and-use-chatsecure#overlay=en/node/51/
[6]:https://ssd.eff.org/en/glossary/metadata
[7]:http://www.makeuseof.com/tag/3-ways-to-remove-exif-metadata-from-photos-and-why-you-might-want-to/
[8]:https://guardianproject.info/2012/01/20/introducing-informacam/]
