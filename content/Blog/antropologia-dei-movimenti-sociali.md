title: Antropologia dei movimenti sociali
date: 2015-06-17 18:54:44 +0200
tags: antropologia, società, politica, anarchia
summary: Un convegno all’Università degli studi Milano-Bicocca con la partecipazione di studiosi di area libertaria e interesanti riflessioni. Per esempio, sulle lotte indigene in Brasile…

Trascrivo [questo articolo][1] molto interessante che avevo letto sul numero 354 (Giugno 2010) di A – rivista anarchica. Scritto da *Andrea Staid*.

**Un convegno all’Università degli studi Milano-Bicocca con la partecipazione di
studiosi di area libertaria e interesanti riflessioni. Per esempio, sulle lotte
indigene in Brasile…**

“Antropologia dei movimenti sociali”è il nome delle giornate di studio che si
sono tenute il 30 e 31 marzo all’università degli studi di Milano Bicocca,
incontro organizzato dai ricercatori del dottorato di ricerca in antropologia
della contemporaneità.

Ho deciso di parlarne su “A” perché sono stati due giorni particolarmente
interessanti e perché penso che antropologia e pensiero libertario abbiano
parecchie cose in comune. *Non è molto che gli antropologi hanno iniziato a
riflettere sull’anarchismo, da molto tempo però anarchismo e antropologia si
muovono sulla stessa traiettoria, le loro teorie tendono a rimbalzare l’una su
l’altra, mentre è evidente che sin dall’inizio del pensiero antropologico esiste
un’ affinità con l’anarchismo, in particolare nel riconoscere la varietà dei
modi di pensare propri agli esseri umani (1).*  

![Antropologia 1]({filename}/images/antropo_movimenti1.jpg "Antropologia 1")

Il pensiero libertario ha occupato, al contrario di altri sistemi di pensiero,
un posto marginale dentro gli ambiti scientifici e accademici.

Negli ultimi anni nell’ambito dell’antropologia alcune cose stanno cambiando,
vari studiosi che si sentono libertari, si sono avvicinati all’antropologia in
cerca di studi specifici, sulla possibile esistenza di società senza lo stato,
senza potere e dominio, società senza il patriarcato o hanno cercato di
applicare il metodo antropologico per costruire una vera e propria etnografia
dei movimenti sociali.

Con un’esperienza inversa, molti antropologi si sono avvicinati alle idee
libertarie sulla base di ricerche sul campo, attraverso lo studio di culture
“altre”, di sistemi e modi diversi di organizzare la vita sociale e di vivere lo
spazio della politica.

Definire l’antropologia non è semplice, ci sono diverse correnti e tradizioni
antropologiche. Per esempio le definizioni; antropologia culturale è di
derivazione statunitense, antropologia sociale di provenienza britannica, ed
etnologia, tipico della scuola francese.
<!--more-->

Il posto dell’anarchismo
------------------------

In generale possiamo dire che l’antropologia è un campo di sapere scientifico
che studia i sistemi socio-culturali, cioè le società e le culture, prestando
uno sguardo particolare per il diverso. L’antropologia è la scienza che studia
l’uomo dal punto di vista sociale, culturale, fisico e dei suoi comportamenti
nella società (2).  

![Antropologia 2]({filename}/images/antropo_movimenti2.jpg "Antropologia 2")

Altra cosa che distingue l’antropologia è il suo metodo di studio, reputa
fondamentale la ricerca sul campo. *Tradizionalmente, fondamentale per la ricerca
etnografica era considerato l’incontro con l’“altro”, in un’esperienza di
“campo” nella quale l’etnografo doveva oltrepassare un confine più o meno
immaginario ed entrare in contatto con la realtà socio-culturale che intendeva
descrivere (3).*

L’anarchismo, il pensiero libertario in generale è definito come un sistema
filosofico che vuole costruire una società senza dominio, più precisamente:
*L’anarchismo non è una teoria omnicomprensiva e compiutamente chiusa in sé, pur
essendo auto referente nel nucleo duro delle sue coordinate di riferimento. Esso
postula una teoria della massima libertà individuale e insieme collettiva,
scommettendo sulla coniugabilità non paradossale (seppur densa di conflitti)
della libertà singolare e della libertà pubblica.

In tal senso l’anarchismo è innanzitutto una filosofia politica figlia del
disincanto moderno: crede finalmente nel fatto e non solo nella pensabilità che
ciascun individuo possa credere e scegliere di gestire l’organizzazione autonoma
della propria vita, eletta tra altre disponibili nello spazio –tempo
storico-materiale in cui si trova a vivere secondo liberi criteri
personalissimi, in piena responsabilità verso l’eguale opportunità data
all’altro da sé, che poi è il ciascuno a lui vicino. Ma l’anarchismo non è solo
la filosofia dell’unico: è anche teoria di una forma organizzativa della
società, cioè progettazione sperimentale di un assetto collettivo che regola una
delle possibilità in cui si attiva il legame sociale “inventato”
quotidianamente; e precisamente quella fondata sul più ampio esercizio di
libertà, sull’accettazione critica e responsabile delle differenze singolari
sull’eguaglianza di opzioni di forme di vita, sull’orizzontalità dei processi
relazionali, specialmente quelli determinanti la formazione decisionale (4).*

Il presupposto indirettamente gerarchico
----------------------------------------

Confrontiamo le parole di Salvo Vaccaro con quelle di Clifford Geertz e troviamo
uno dei motivi che mi spinge a parlare di questi possibili rapporti tra
libertari e studi antropologici: Vedere noi stessi come ci vedono gli altri può
essere rivelatore. *Vedere che gli altri condividono con noi la medesima natura è
il minimo della decenza. Ma è dalla conquista assai più difficile di vedere noi
stessi tra gli altri, come un esempio locale delle forme che la vita umana ha
assunto localmente, un caso tra i casi, un mondo tra i mondi, che deriva
quell’apertura mentale senza la quale l’oggettività è autoincensamento e la
tolleranza mistificazione (5).*

![Antropologia 3]({filename}/images/antropo_movimenti3.jpg "Antropologia 3")

Quindi ci parla di un’antropologia dialogica che cerca di annullare il
presupposto indirettamente gerarchico secondo cui “noi” studiamo “loro” perché
noi diversamente da loro, siamo emancipati dalle “stranezze” della cultura (6).  
Tornando alla due giorni che si è tenuta all’Università degli studi di Milano
Bicocca, i seminari sono stati molti, particolarmente interessante è stata la
relazione di Stefano Boni, dal titolo *“La politica della prassi sovversiva.
L’attivismo quotidiano nato dalla frammentazione dei movimenti di critica alla
globalizzazione”*. Nel suo intervento Stefano Boni ci ha parlato di come dal
basso si ripensa la politica oggi, di come i nuovi movimenti sociali non cercano
un riconoscimento governativo ma sociale, ha rilevato la differenza che passa
tra la vecchia politica retorica e le reali relazioni sociopolitiche di oggi che
intercorrono tra individui nel quotidiano delle loro vite.

Viviamo un’epoca di restringimento delle storiche pratiche sovversive dei
movimenti politici, sempre meno usuali sono scontri diretti e barricate,
pratiche usuali invece nel conflitto tra 800\900. La società civile è sempre più
passiva e impotente, l’azione diretta è sempre più criminalizzata e i movimenti
radicali sempre più repressi. Quindi l’azione movimentista perde forza. Privi di
scontri nel “gioco del potere” la protesta diventa sempre più telematica,
irreale e impotente si vive nell’impossibilità di imporre la “volontà popolare”.
Per questo cambiano i modi della lotta, secondo Boni dobbiamo rivoluzionare il
vissuto quotidiano, trasformare le nostre vite prima di tutto.

Durante la sua relazione ci ha parlato di una sua ricerca sul campo nel Senese
(*Vivere senza padroni*, elèuthera, 2006) dove ha affrontato la rimessa in
discussione dell’organizzazione sociale. Ha sottolineato l’importanza di provare
a vivere al di fuori degli schemi istituzionali, la necessità di trovare una
forma comune, una prassi collettiva del vivere sociale.

È nel vissuto che si costruisce l’antagonismo, non nei grandi eventi mediatici
del movimento. Perciò questa ricerca sul campo si sofferma sulle prassi di vita
di un frammento di umanità ribelle, al di là degli stereotipi mediatici. Il
movimento è la sua cultura, una cultura che è fatta di valori specifici, di un
immaginario comune e comunitario, di emozioni e idiosincrasie condivise, ma
anche del loro tradursi e manifestarsi in uno stile di vita. In queste modalità
peculiari e distintive di fare le cose e di pensare il mondo si genera
l’identità comune, il “noi” descritto in questa ricerca.

Dopo svariati mesi a contatto diretto con questi gruppi del Senese, ha descritto
nella sua relazione i punti fondamentali di questo metodo di lottare cambiando
la quotidianità delle nostre vite. Fondamentale il superamento dei paradigmi
identitari, vivere un continuo confronto nella messa in discussione delle
pratiche utilizzate, cercare di realizzare dei paradigmi identitari includenti.
Indispensabile in queste pratiche una gestione delle risorse in modo libertario
ed egualitario. Si riformula il modo di fare politica, diventa una lotta per la
“mutazione” culturale.


Una resistenza legata alla terra
--------------------------------

Un’altra relazione molto interessante è stata quella di Alfredo Wagner Berno De
Almeida dell’Universidade Federal dos Amazonas de Brasile, il titolo della sua
relazione *“Nuove forme organizzative dei movimenti sociali della Pan amazzonia”*.
Ci ha parlato dei movimenti indigeni nella Pan amazzonia, di più di tremila
gruppi che lottano per la tutela del loro territorio.

Secondo De Almeida cambiano le forme della lotta, i numeri sono sempre più
grandi e di pari passo mutano anche gli strumenti antropologici per analizzare
società e movimenti di lotta. A cambiare non sono soltanto i movimenti ma anche
il capitalismo, sempre più confliggono due diversi tipi di capitalismo quello
devastante e quello verde. Il ruolo degli antropologi e biologi deve essere la
difesa del patrimonio genetico e delle culture tradizionali.

Nella Pan amazzonia assistiamo a un processo di territorializzazione forte, con
lotte che si avviano sempre più in modo radicale con occupazioni di terre,
scioperi generali che contrastano il capitalismo che sia verde o devastante
perché hanno compreso che non cambia la sostanza della sua arroganza. *Il
capitalismo possiede uno stock di crescite, di tutti i colori: crescita blu con
la destra e i padroni, crescita rossa con la sinistra e i sindacati, crescita
verde con Borloo e il suo compagno Cohn-Bendit. Non diremo mai che tutte queste
politiche si equivalgono… ma la crescita blu, verde, rossa, verde, conducono
tutte allo stesso muro, tutte alimentano la macchina dell’ineguaglianza sociale
e al lavaggio del cervello della popolazione (7).*

In queste lotte si mobilitano donne, uomini, bambini e sciamani, la cosa
veramente peculiare è che appartengono alle etnie più diverse, hanno trovato un
punto di unione con la lotta per la difesa del territorio, una lotta di
riappropriazione ecologista. Mettono in gioco la loro vita, il loro impegno
contro le deforestazioni, luoghi sacri per loro, dove da sempre trovano erbe
medicinali.

De Almeida ci ha parlato di una resistenza e forme politiche legate alla terra
una sorta di politicizzazione della natura e questo ha aperto nuovi scenari
importanti. Terminando ha fatto una critica alle ONG che incidono negativamente
su i gruppi di lotta autonoma e autoctona, producono populismo e supporto al
capitalismo verde, invece ci vuole una rottura con la politica della tutela.
Molti altri sono stati gli interventi interessanti ma non basterebbe la rivista
intera per sviscerarli tutti. Concludo ringraziando tutti\e gli organizzatori e
i relatori delle due giornate.

**Andrea Staid**

Note
----
1. David Graeber, Frammenti di antropologia anarchica, Elèuthera, Milano, 2006.
2. Fabietti, Francesco Remotti (a cura di), Dizionario di antropologia, Zanichelli, Bologna, 1997.
3. Paolo Corbetta, Metodologia e tecniche della ricerca sociale, Il Mulino, Bologna, 1999.
4. Salvo Vaccaro, Cruciverba, Lessico per i libertari del XXI secolo, Zero in Condotta, Milano, 2001.
5. Clifford Geertz, Interpretazione di culture, il Mulino, Bologna, 1988.
6. Andrea Staid, Quel potere senza dominio, Libertaria, anno II, n. 4, 2010.
7. Paul Ariès, La simplicitè volontarie contre le mithe de l’abondance, La Decouverte, Paris, 2010.

[1]:http://www.arivista.org/?nr=354&pag=58.htm
