title: In Real Life
date: 2016-05-09 22:44
tags: fumetti, recensioni
summary: Un fumetto abbastanza unico, questo, il cui tema centrale sono i *MMORPG*, cioè i giochi di ruolo multigiocatore online, ed rapporto fra essi, l'economia ed il lavoro. I disegni dai tratti leggeri ed i personaggi giovani rendono gradevole e molto scorrevole una storia che tratta di argomenti seri, in special modo lo sfruttamento dei lavoratori.

![In Real Life][6]{: style="float:right"}
Un fumetto abbastanza unico, questo, il cui tema centrale sono i [MMORPG][1], cioè i giochi di ruolo multigiocatore online, ed rapporto fra essi, l'economia ed il lavoro. I disegni dai tratti leggeri ed i personaggi giovani rendono gradevole e molto scorrevole una storia che tratta di argomenti seri, in special modo lo sfruttamento dei lavoratori. Scritta per un pubblico adolescente da [Cory Doctorow][2], già autore di due libri qua recensiti: [Little Brother][3] e [Homeland][4] e disegnata da [Jen Wang][7]

La storia narra di *Anda*, una ragazza appassionata di giochi di società e di ruolo, alla quale viene presentato un gioco online, *Coarsegold*. Inizia rapidamente a padroneggiarlo e viene invitata a svolgere delle missioni che le frutteranno dei soldi non nel gioco, bensì nella vita reale. Verrà preso a scoprire cosa è nascosto dietro a queste missioni e le implicazioni etiche.

Come negli altri racconti e romanzi di Doctorow, da me letti, anche qua tutto nasce da situazioni reali. In questo caso, il fenomeno del *Gold Farming* cioè la situazione di ragazzi poveri, sfruttati per [creare ricchezza all'interno di un videogioco, che verrà poi rivenduta dal vero][5] a ragazzi ricchi che la useranno per avanzare all'interno del gioco.

> Cosa si nasconde dietro Coarsegold Online,il massively multiplayer role playing game che spopola tra i ragazzini di tutto il mondo? Chi paga le missioni a cui Anda partecipa, e chi sono realmente i gold farmer che le è stato chiesto di eliminare? La mente di Cory Doctorow e i disegni di Jen Wang danno vita a uno scorcio illuminante sulle implicazioni economiche, politiche e sociali della realtà dei videogiochi, insieme alla storia tenera e crudele di un'amicizia che, pur essendo fatta solo di pixel, è già quasi amore.

Ne consiglio la lettura, specialmente se siete appassionati di videogiochi, giochi di ruolo, o se vi interessano le tecnologie della comunicazione e le questioni etiche e di diritto ad esse legate.

Se vi interessa l'argomento dei MMORPG, vi consiglio anche il geniale romanzo fantascientifico [Arresto di sistema][8] di [Charles Stross][9].

[1]: https://it.wikipedia.org/wiki/MMORPG

[2]: https://it.wikipedia.org/wiki/Cory_Doctorow

[3]: {filename}/Blog/little-brother.md

[4]: {filename}/Blog/homeland.md

[5]: https://en.wikipedia.org/wiki/Gold_farming

[6]: {filename}/images/in-real-life.jpg

[7]: http://jenwang.net/archives/704

[8]: {filename}/Blog/arresto-di-sistema.md

[9]: https://it.wikipedia.org/wiki/Charles_Stross
