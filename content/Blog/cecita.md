title: Cecità
date: 2015-02-04 18:48:29 +0100
tags: fantascienza, libri, recensioni
summary: Come nel romanzo precedentemente recensito, *Le intermittenze della morte*, l'autore crea un espediente per modificare la realtà – in questo caso una improvvisa inspiegabile e dilagante epidemia di cecità “bianca” – e seguire il comportamento dei propri personaggi, nei loro tentativi di adattarsi a questa nuova situazione.

Come nel romanzo precedentemente recensito, *Le intermittenze della morte*, l'autore crea un espediente per modificare la realtà – in questo caso una improvvisa inspiegabile e dilagante epidemia di cecità “bianca” – e seguire il comportamento dei propri personaggi, nei loro tentativi di adattarsi a questa nuova situazione.

Ma non sarà un freddo osservatore. Nei panni del narratore, egli sarà molto presente, descriverà, spiegherà, esprimerà comprensione e compassione per i personaggi.

Sarà con loro. Anche lo stile di scrittura, compatto, visivamente omogeneo, con
la punteggiatura ridotta al minimo, sembra studiato appositamente per creare nel
lettore lo stesso senso di smarrimento ed isolamento che i suoi personaggi
provano. Con la sua stupenda tecnica vi farà sentire come loro, immersi in quel
“mare di latte”, più inquietante e anomalo di una semplice tenebra e dentro il
quale solo ciò che è a portata di tatto e di memoria, è conosciuto. Se non vi
sentirete ciechi, sono sicuro che almeno in una manciata di casi, noterete la
vista annebbiarsi.

I protagonisti stessi, non avranno alcun nome, ma saranno identificati per
quello che fanno o per le caratteristiche più salienti che avevano, quando
ancora si potevano vedere reciprocamente. Quindi avremo: il medico, la moglie
del medico, il primo cieco, la ragazza con gli occhiali scuri, il vecchio con la
benda, e via dicendo.

Quarta di copertina
-------------------

*In una città qualunque, di un paese qualunque, un guidatore sta fermo al
semaforo in attesa del verde quando si accorge di perdere la vista.
All’inizio pensa si tratti di un disturbo passeggero, ma non è così. Gli viene
diagnosticata una cecità dovuta a una malattia sconosciuta: un «mal bianco» che
avvolge la sua vittima in un candore luminoso, simile a un mare di latte. Non si
tratta di un caso isolato: è l’inizio di un’epidemia che colpisce
progressivamente tutta la città, e l’intero paese. I ciechi, rinchiusi in un ex
manicomio e costretti a vivere nel più totale abbruttimento da chi non è stato
ancora contagiato, «scoprono – come ha scritto Cesare Segre – su se stessi e in
se stessi, la repressione sanguinosa e l’ipocrisia del potere, la sopraffazione,
il ricatto, e peggio di tutto, l’indifferenza». Saramago denuncia con intensità
di immagini e durezza di accenti la notte dell’etica in cui siamo sprofondati.
E, paradossalmente, è proprio il mondo delle ombre a rivelare molte cose sul
mondo che credevamo di vedere.*
