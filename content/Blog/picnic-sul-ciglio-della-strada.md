title: Picnic sul ciglio della strada
date: 2014-01-02 19:27:34 +0100
tags: libri, recensioni, fantascienza
summary: Scritto dai fratelli Arkadi e Boris Strugatzki, è famoso anche per essere il romanzo di fantascienza da cui è stato tratto il film Stalker.

Scritto dai fratelli Arkadi e Boris Strugatzki, è famoso anche per essere il romanzo di fantascienza da cui è stato tratto il film Stalker.

> Marmont, una cittadina industriale come tante altre. Eppure, poco oltre la
> periferia, qualcosa è cambiato irreversibilmente. Al di là di hangar e
> capannoni, in mezzo a una natura splendida, si estende un territorio dalle
> caratteristiche uniche. È la Zona: uno dei sei luoghi del mondo “visitati”
> dagli extraterrestri. La Zona: un luogo magico e pericoloso, che pullula di
> fenomeni sconvolgenti, di oggetti dalle qualità straordinarie. Come dopo un
> picnic sul ciglio della strada, a metà del viaggio fra una galassia e
> l’altra, gli extraterrestri hanno mollato i propri avanzi sul prato. “Avanzi”
> che hanno cambiato radicalmente leggi fisiche e natura di quei luoghi.
> Accumulatori eterni, gusci energetici, antigravitometri sono strumenti di
> altissimo valore scientifico ed economico, prede prelibate di studiosi e
> trafficanti. A Marmont è nata una nuova professione, quella di “stalker”. Gli
> stalker entrano nella Zona a caccia di questi oggetti e li rivendono al
> miglior offerente. Tenace “cercatore” dell’Istituto delle civiltà
> extraterrestri, Red Schouart, in arte Roscio, è sedotto dalla potenza della
> Zona. Rapido, fortissimo, deciso, è pronto a strisciare su un suolo
> imprevedibile a temperature insostenibili. L’Eldorado sembra a un passo, in
> quel luogo assoluto, ma non sono né la ricchezza, né il potere, né la verità
> che premono a Roscio: è il brivido estremo della sfida, il desiderio di
> “bucare” lo schermo del possibile che lo spingono a trasgredire le leggi –
> fisiche e morali – di una comunità pavida e corrotta. Questo romanzo è un
> gioiello della letteratura fantastica di tutti i tempi: lo “Stalker” e la
> “Zona” sono diventati veri e propri archetipi.

![Picnic sul ciglio della strada]({filename}/images/picnic.png){: style="float:right"}
Una delle cose più interessanti del racconto, è il rapporto fra le persone e *la
Zona*: un luogo talmente al di fuori delle leggi fisiche conosciute, da
risultare incomprensibile, spaventoso e terribilmente letale, di una
pericolosità accresciuta dal non poterne percepire i pericoli. È emblematico
anche il modo in cui la gente rappresenta a sé stessa *lo scopo* della visita
degli alieni ed il motivo per cui hanno lasciato tutti quegli oggetti
prodigiosi.

Si tende a preferire spiegazioni “dignitose” per l’uomo, attribuendo a questa
visita una intenzionalità, immaginando che gli alieni abbiano effettivamente
*desiderato* un contatto con l’uomo, mentre forse l’ipotesi più probabile è che
la visita sia stata solo una sorta di picnic alieno su di un pianeta a casaccio,
sul quale poi è stata dimenticata la propria immondizia. In un brillante
discorso fra due dei personaggi, vengono vagliate varie ipotesi e si passa da
quella appena descritta, al *come giudicare la presenza o meno della ragione in
esseri radicalmente diversi da noi*. *È così difficile definire cosa sia la
ragione, anche solo prendendo in considerazione noi stessi, che applicare le
stesse regole che utilizziamo per noi a creature dalla natura completamente
ignota, risulta un impresa impossibile ed un sintomo di radicato antropocentrismo.
Tuttavia ci è impossibile non provarci.*
Come studiare gli oggetti lasciati dagli alieni? Siamo riusciti a classificarli,
ad utilizzarli, ma non ancora a capire come funzionano e a riprodurli. Ed anche
riuscendo ad utilizzarli, non è escluso che non stiamo semplicemente *“usando
dei microscopi per piantare dei chiodi”*.  

Il racconto tocca anche uno dei temi tipici della filosofia della scienza,
quando critica la nostra *“potente scienza positivista”* per essere impotente di
fronte al mistero delle Zone e degli oggetti ivi contenuti.