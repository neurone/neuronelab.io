title: Esseri con uno scopo
date: 2015-06-30 22:30:33 +0200
tags: antispecismo, etica, filosofia, veg
summary: Molte persone giustificano la loro abitudine di mangiare animali, affermando che essi sono nati per quello scopo. Ma cos'è e da dove ha origine uno scopo?

Molte persone giustificano la loro abitudine di mangiare animali, affermando che essi sono nati per quello scopo. Ma cos'è e da dove ha origine uno scopo?

Uno scopo esiste solo in presenza di una volontà che lo esprime. Lo scopo della
forchetta, come strumento, nasce dalla volontà del suo costruttore di avere un
mezzo che gli permetta di infilzare comodamente il cibo. Ma lo scopo di una
roccia quale sarebbe? La roccia è un oggetto formatosi spontaneamente in seguito
ad una serie di cause ed interazioni fra materia, al di là dal controllo e dalla
volontà umana. La roccia diventa uno strumento, ed acquisisce uno scopo, quando
una persona desidera esercitare la propria volontà e ritiene che quella roccia
possa servirgli. Nel caso di un essere vivente e senziente, si tradurrebbe in
una prevaricazione della volontà di chi definisce quello scopo, rispetto alla
volontà di chi se lo trova suo malgrado assegnato.

Chi ritiene che gli animali, o le rocce, abbiano uno scopo in relazione
all'uomo, può farlo credendo che quello scopo sia stato attribuito dall'uomo
stesso, oppure da un'altra entità dotata di volontà, come ad esempio un dio
creatore. Nel primo caso, il fatto che lo scopo non sia intrinseco, ma
dipendente dalla volontà di chi quello scopo ha assegnato, è presente già nelle
premesse. Di conseguenza, se gli animali avessero uno scopo, sarebbe solo perché
noi l'abbiamo creato in relazione a noi stessi, egoisticamente. Potremmo dire
che un lombrico non ha alcuno scopo, fino a quando a qualcuno non viene la
brillante idea di utilizzarlo per farsi i lacci delle scarpe. Lasciamo che
questa abitudine si radichi nei secoli, e troveremo gente pronta ad affermare
con convinzione, che i lombrichi nascono per diventare lacci da scarpe. Nel
secondo caso, ci si potrebbe voler chiedere per quale motivo un dio creatore
dovrebbe portare all'esistenza degli esseri senzienti, in grado di avere una
vita completa, riprodursi, evolversi, organizzarsi in società, se il loro scopo
fosse solamente di diventare nutrimento per l'uomo.

Non sarebbe più semplice crearli senza tutte questa complessità e queste
caratteristiche, così simili a quelle umane e nelle quasi siamo spesso in grado
di specchiarci, riconoscendovi un destino comune di mortalità ed impermanenza,
empatizzare osservando analoghi dei nostri stessi comportamenti, come la
capacità di provare emozioni, dolore e paura, caratteristiche fondamentali per
poter preservare la propria vita fuggendo o reagendo ai pericoli mortali, per
poi dargli come scopo l'essere uccisi e mangiati dall'uomo? Perché costringere
l'uomo a fare questo, invece di, più semplicemente, permettergli di coglierli,
inanimati, su degli alberi, come fossero frutta?

Sembrerebbe un dio crudele, quello che pianifica tutto ciò. Ma d'altronde,
perché dare a degli esseri una vita talmente instabile che necessiti di continui
interventi per essere mantenuta? Bere, mangiare, ripararsi dalle condizioni
ambientali pericolose... Perché far sì che senza tali interventi si patisca,
spronando quindi gli esseri viventi ad affannarsi per sopravvivere? Ricordo di
una scena in *Dune*, nella quale un prigioniero del malvagio barone Harkonnen
era stato da lui avvelenato, e l'unico modo per non morire era di leccare
periodicamente l'enzima prodotto dal pelo di un gatto che era stato messo in
cella con lui. Forse, converrebbe smetterla di attribuire uno scopo agli esseri
viventi o chiedersi quale sia e meditare sulla comune condizione condivisa da
tutti gli animali umani e non umani.
