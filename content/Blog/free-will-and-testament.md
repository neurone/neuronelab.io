title: Free will and testament
date: 2014-02-19 20:11:35 +0100
tags: musica, filosofia, filosofia della mente
summary: Una stupenda canzone, che può fare anche da ottima introduzione ad alcuni temi di filosofia della mente. Molto consigliata nel caso stiate per interessarvi ai testi di Daniel Dennett o di Douglas Hofstadter, oppure già li conosciate.

Una stupenda canzone, che può fare anche da ottima introduzione ad alcuni temi di filosofia della mente. Molto consigliata nel caso stiate per interessarvi ai testi di [Daniel Dennett][1] o di [Douglas Hofstadter][2], oppure già li conosciate.

L'autore è [Robert Wyatt][3] ed il brano è contenuto nell'album *Shleep*.

Traduzione del testo basata su quella di questo video: [Robert Wyatt Free Will
and Testament (Youtube)][4]

Ho il mio libero arbitrio ma entro certe limitazioni,  
Non posso desiderare per me stesso mutazioni illimitate,  
Non posso sapere cosa sarei stato se non fossi me,  
Io posso solo immaginarmi.

Così quando dico che mi conosco, come posso saperlo?  
Che genere di ragno capisce l'aracnofobia?  
Ho i miei sensi e la mia consapevolezza di avere i sensi.  
Io li guido? O loro guidano me?

Il peso della polvere supera il peso degli oggetti posati.  
Che cosa può significare, tale gravità senza un centro?  
C'è la libertà di non esistere?  
C'è la libertà dal voler essere?

Il puro impulso ci fa agire in questo o in quel modo.  
Poi noi inventiamo od assumiamo una motivazione.  
Vorrei disperdermi, essere disconnesso. È possibile?  
Che cosa sono dei soldati senza un nemico?  

Essere nell'aria, ma non essere aria, esistere nell'assenza di aria.  
Essere slegato, né compresso, né sospeso.  
Né nato, né lasciato morire.  

Se fossi stato libero, avrei potuto scegliere di non essere me.  
Forze dementi mi spingono all'impazzata su un tapis roulant.  
Forze dementi mi spingono all'impazzata su un tapis roulant.  
Lasciatemi stare per favore, sono così stanco.  
Lasciatemi stare per favore, sono molto stanco.
<!--more-->

**Originale**

Given free will but within certain limitations,  
I cannot will myself to limitless mutations,  
I cannot know what I would be if I were not me,  
I can only guess me.

So when I say that I know me, how can I know that?  
What kind of spider understands arachnophobia?  
I have my senses and my sense of having senses.  
Do I guide them? Or they me?

The weight of dust exceeds the weight of settled objects.  
What can it mean, such gravity without a centre?  
Is there freedom to un-be?  
Is there freedom from will-to-be?

Sheer momentum makes us act this way or that way.  
We just invent or just assume a motivation.  
I would disperse, be disconnected. Is this possible?  
What are soldiers without a foe?

Be in the air, but not be air, be in the no air.  
Be on the loose, neither compacted nor suspended.  
Neither born nor left to die.

Had I been free, I could have chosen not to be me.  
Demented forces push me madly round a treadmill.  
Demented forces push me madly round a treadmill.  
Let me off please, I am so tired.  
Let me off please, I am so very tired.

[1]: https://en.wikipedia.org/wiki/Daniel_Dennett
[2]: https://en.wikipedia.org/wiki/Douglas_Hofstadter
[3]: https://it.wikipedia.org/wiki/Robert_Wyatt
[4]: https://www.youtube.com/watch?v=Fv_F29h_qxM
