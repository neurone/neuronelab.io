title: Nè Newton nè Leibniz ma mente collettiva
date: 2016-03-15 18:53
tags: scienza, filosofia, matematica, società, libertà
summary: All’interno della comunità matematica si discute tuttora, anche se ormai folcloristicamente, della disputa molto accesa e mai conclusa tra Isaac Newton e Gottfried Leibniz per la paternità del calcolo infinitesimale, branca fondante dell’analisi matematica e dunque presupposto per tanti risultati tecnologici e scientifici dell’ultimo secolo. Newton, pur iniziando prima di Leibniz a sviluppare il “metodo delle flussioni”, tardò molto nel pubblicare i suoi risultati. Inizialmente tra i due nacque un produttivo scambio epistolare, tramite il quale gli scienziati iniziarono a comprendere la portata delle loro intuizioni, ma che terminò bruscamente non appena si presentò a entrambi il timore di vedersi strappare il primato della scoperta.

Riporto quest'articolo dal blog, non più raggiungibile [Six Degrees of Freedom][1]

> «Omnia sunt communia», **Thomas Müntzer**

All’interno della comunità matematica si discute tuttora, anche se ormai folcloristicamente, della disputa molto accesa e mai conclusa tra Isaac Newton e Gottfried Leibniz per la paternità del calcolo infinitesimale, branca fondante dell’analisi matematica e dunque presupposto per tanti risultati tecnologici e scientifici dell’ultimo secolo.  
Newton, pur iniziando prima di Leibniz a sviluppare il “metodo delle flussioni”, tardò molto nel pubblicare i suoi risultati. Inizialmente tra i due nacque un produttivo scambio epistolare, tramite il quale gli scienziati iniziarono a comprendere la portata delle loro intuizioni, ma che terminò bruscamente non appena si presentò a entrambi il timore di vedersi strappare il primato della scoperta.  
Nel 1684 Leibniz pubblicò il Nova methodus pro maximis et minimis, primo trattato di analisi matematica mai pubblicato. Fu allora che Newton accusò il collega di plagio, appellandosi alla Royal Society of London, del quale era membro onorario, per essere riconosciuto come padre di quelle scoperte che non pubblicò però prima del 1687, nel suo celebre Philosophiae naturalis principia mathematica. Leibniz dal canto suo sostenne che anche prima di leggere i testi di Isaac avesse ottenuto certi risultati.  
L’accanimento di Newton continuò per anni e la contesa proseguì addirittura anche oltre la morte di Leibniz, coinvolgendo l’ intera comunità scientifica in una vicenda che mai trovò un finale. L’ ipotesi più accreditata, tuttavia, è che entrambi gli studiosi abbiano sviluppato parallelamente le stesse ipotesi giungendo alle stesse conclusioni, usufruendo inoltre ognuno delle conoscenze apprese dall’altro durante i loro pochi scambi. Questa tesi è avvalorata dal fatto che nelle opere dei due si riscontrano diversi approcci, più fisico-applicativo quello di Newton, più teorico-algebrico quello di Leibniz, il che escluderebbe una qualsiasi ipotesi di plagio, in un senso o nell’altro.  
Se vale la pena soffermarsi su vicende storiche come questa è per provare a rimettere in discussione quelle concezioni predominanti che tentano di fondare l’ attuale sistema accademico e culturale sul merito individuale e sulla competitività.

> «Se ho visto più lontano è perché stavo sulle spalle dei giganti», **Isaac Newton**

Nella storia della scienza sono innumerevoli i casi di scoperte o invenzioni avvenute simultaneamente agli antipodi del globo, spesso nella reciproca insaputa degli autori e in modo sorprendente. Seguendo lo sviluppo dell’analisi matematica nell’800 si presentò uno di questi casi, quando alcuni matematici, tra cui Karl Marx, si impegnarono per trovare una formalizzazione migliore del calcolo di quanto avevano saputo fare Newton e Leibniz, giungendo così alla formulazione attuale. Mentre Marx durante gli ultimi anni della sua vita pubblicava i suoi Manoscritti Matematici non sapeva infatti che sia Cauchy che Weierstrass stavano risolvendo i problemi che lui si stava ponendo.  
Gli esempi continuano, basta pensare a quante dispute mai risolte si sono incontrate, dall’invenzione del compasso attribuita sia a Galileo che all’olandese Zieckmesser, al telefono conteso tra Meucci e Bell, o ancora alla formulazione simultanea della Teoria della Relatività da parte di Einstein e Hilbert, anch’essi amici-rivali.  
Diversamente da tanti casi di competizione per la superiorità tra scienziati, un’interessante eccezione è stato il lavoro collaborativo e di profondo rispetto avutosi tra Charles Darwin, padre della teoria evoluzionistica, e il suo amico e collega Alfred Wallace.  
Quando Wallace partì nel 1850 per una spedizione scientifica nel Rio delle Amazzoni aveva già studiato i diari di viaggio di Darwin e sapeva che egli da anni si dedicava alle sue stesse ipotesi. Durante la spedizione i due continuarono gli scambi di lettere, giungendo insieme all’introduzione di nuovi concetti, come appunto la selezione naturale. L’evento più significativo avvenne nell’estate del 1858, quanto dalla Malesia Wallace spedì a Darwin il suo saggio, Sulla Tendenza delle varietà ad allontanarsi indefinitamente dal tipo originale, che lasciò sbalordito Charles nel constatare quanto i risultati esposti fossero esattamente quelle da lui ottenuti. Dopo aver riassunto le teorie Darwin inviò il lavoro alla Società Linneana di Londra firmandolo a nome di entrambi pur sapendo che durante le indagini Wallace si era basato sui suoi scritti precedenti, dimostrandosi uomo dotato di grande onestà intellettuale. Alla faccia del darwinismo.  
Le grandi scoperte sono state raramente frutto di una mente eccelsa distinta dalla massa, quanto invece risultato di continue contaminazioni culturali che proliferando hanno prodotto capolavori in modo spontaneo e spesso collettivo, come fu per la nascita delle geometrie non euclidee, sviluppatesi grazie a più di un matematico in un modo talmente improvviso che il padre dell’ungherese Bolyai arrivò ad affermare:

> «Quando il tempo è maturo per certe cose, queste appaiono in diversi luoghi, proprio come le violette sbocciano dappertutto quando comincia la primavera», **Farkas Bolyai**

La comunità è il vero genio, che grazie agli scambi casuali o meno di conoscenze accresce quel calderone culturale dal quale alcuni individui attingono nei momenti più propizi, per far fruttare la macchina della produzione collettiva.  
L’elogio della genialità individuale eclissa questo processo di creazione intellettuale, e ancor peggio se pretende di distinguere all’ interno della comunità individui più o meno meritevoli secondo canoni calati dall’alto, quando invece la fertilità della cultura risiede proprio nel mescolamento delle attitudini e delle particolarità differenti di ogni individuo.  
A differenza di altre specie animali che non hanno una memoria collettiva l’ essere umano può alimentare il proprio bagaglio culturale usufruendo del patrimonio comune accumulatosi nel tempo, all’ interno del quale confluiranno anche i suoi contributi.  
Ogni nozione che possediamo, così come ogni nostra “invenzione”, è innanzitutto la conseguenza di tanti incontri avuti nella nostra vita, dalle lezioni scolastiche ai colloqui amichevoli con i nostri conoscenti, ed è qui che si produce la cultura: nello scambio libero e conviviale.  
Infognarsi in diatribe di paternità è privo di senso, dal momento che si sta discutendo semplicemente su a chi spetta aver messo l’ultima tegola di una casa costruita da tante e tanti durante i secoli, per di più se questa diatriba ci nega la possibilità di collaborare con menti impegnate sui nostri stessi cavilli. La ricerca più fruttuosa è quella condotta nel rispetto verso la conoscenza, di certo non quella compiuta in nome dell’agonismo accademico.

> «Le idee migliori sono proprietà di tutti», **Seneca**

Anche la definizione stessa di plagio ha subìto un cambiamento negli anni: il primo ad accusare un “plagiatore”, rapitore, fu Marziale nel I secolo, per descrivere qualcuno che aveva interamente ricopiato le sue liriche pubblicandole sotto un proprio nome, ma prima del XVIII secolo i drammaturghi prendevano liberamente in prestito trame e dialoghi senza citare le fonti, e mai si sarebbero sognati, come avviene ai giorni nostri, di aguzzare l’udito per riconoscere in una canzone una coppia di note presenti in un altro brano “plagiato”. Nel suo Poesies il poeta francese Lautréamont elogiava il plagio, auspicandosi addirittura un ritorno alla poesia impersonale. La sua visione divenne un punto di riferimento per i dadaisti, che consideravano qualsiasi composizione un riassemblaggio di materiale riciclato, rifiutando quell’attaccamento alla paternità che oggi trova un esasperazione nella proprietà intellettuale, vero vincolo per la diffusione e la circolazione delle idee.  
Come è doveroso retribuire all’autore il tempo impiegato per la creazione dell’opera, così però non è diritto di nessuno speculare su questa conoscenza collettiva, impadronendosi tramite brevetti e licenze proprietarie di un bene prodotto da ciascuno di noi e la cui circolazione è necessaria per lo sviluppo sociale e culturale.
> «Il plagio è necessario. Il progresso lo implica. Stringe da vicino la frase di un autore, si serve delle sue espressioni, cancella un’idea falsa, la sostituisce con l’idea giusta», **Lautréamont**

In tempi in cui l’ambigua retorica del merito affolla le aule universitarie e giovani ricercatori si tolgono la vita perché perseguitati dalla giustizia per aver distribuito articoli scientifici coperti da copyright, è necessario riprendere a mano le basi della questione, ripartendo dal riconoscimento della genesi collettiva del sapere.  
Chi può dirsi meritevole tra i tanti e chi è proprietario indiscusso di un’idea, quando la produzione immateriale è frutto di un lavoro collettivo al quale ognuno partecipa in modo collaborativo e gratuito?  
Il sapere è oggi sempre più a rischio, minacciato da un nuovo capitalismo che cerca di costringere l’informazione a divenire sua nuova merce, ma a differenza dei beni materiali che possono produrre ricchezza grazie alla loro carenza, la conoscenza è per sua natura inafferrabile e incline alla proliferazione, dunque ostile a diventare fonte di profitto.  

> «Se ci scambiamo una moneta avremo ancora una moneta a testa, se ci scambiamo un’idea avremo entrambi due idee», **Anonimo**

La mente collettiva opera nelle scienze, nelle arti, nella letteratura.  
Non solo i teoremi matematici ma anche le storie sono scritte da ognuno di noi. In ogni romanzo sono narrate le favole udite dall’autore durante la sua infanzia, un ciclo fantasy nasce dal confondersi di miti e leggende tramandate oralmente dai popoli nei secoli, i racconti vengono suggeriti dalla strada, dai nostri vicini, dalle esperienze che compongono la quotidianità.  
Perché anche le storie, come i teoremi, più che invenzioni sono scoperte.

> «Le storie sono oggetti ritrovati, come fossili nel terreno», **Stephen King**

Fossili da scovare, asce di guerra da disseppellire per difendere la cultura come bene comune.  
Contro chi vuole ridurre gli spazi del sapere a fabbriche dedite a un nuovo sfruttamento rivendichiamo ancora una volta la nostra concezione di sapere liberato e di formazione critica, ribadendo che le idee non si sottomettono a nessun padrone e che ogni racconto, in fondo, è una continua e inarrestabile narrazione collettiva.

### Commenti

**Krop**
_on 27 maggio 2013 at 17:48_

Sempre il vecchio Pëtr, ne “La conquista del pane”:

Persino il pensiero, persino le invenzioni, son fatti collettivi nati dal passato e dal presente.  
Sono migliaia d’inventori, conosciuti o sconosciuti, morti nella miseria, i quali hanno preparato le successive invenzioni di ciascuna di queste macchine in cui l’uomo ammira il proprio genio. Sono migliaia di scrittori, di poeti, di dotti, i quali hanno lavorato per perfezionare la dottrina, per dissipare l’errore, per creare infine quest’atmosfera di pensiero scientifico, senza la quale nessuna delle meraviglie del nostro secolo non si sarebbe prodotta. Ma queste migliaia di filosofi, di poeti, di dotti, d’inventori, non erano anche essi il prodotto del lavorìo dei secoli passati prima di loro? E non furono, durante tutta la loro vita, nutriti e aiutati, sì fisicamente che moralmente, da legioni di lavoratori e di artigiani di ogni specie? Non attinsero essi la loro forza d’impulsione da tutto ciò che li circonda?  
Il genio di un Seguin, d’un Mayer, d’un Grove hanno fatto certamente più che tutti i capitalisti del mondo per slanciare l’industria verso nuovi orizzonti, Ma questi genii stessi son figli dell’industria nonchè della scienza. Imperocchè bisognò che migliaia di macchine a vapore trasformassero d’anno in anno, sotto gli occhi di tutti, il calore in forza dinamica, e questa forza a sua volta in suono, luce ed elettricità, prima che queste intelligenze geniali avessero proclamato l’origine meccanica e l’unità delle forze fisiche. E se noi, figli del secolo decimonono, abbiam compreso finalmente quest’idea, se noi abbiamo saputo applicarla, egli è perchè noi vi eravamo stati preparati dall’esperienza di ogni giorno e di ogni ora. Anche i pensatori del secolo scorso l’avevano traveduta ed enunciata: ma essa rimase incompresa, giacchè il secolo decimottavo non si era sviluppato, come il nostro, a fianco delle macchine a vapore.  
Che si pensi soltanto alle diecine d’anni che sarebbero trascorsi ancora nell’ignoranza di questa legge che ci ha permesso di rivoluzionare l’industria moderna, se Watt non avesse trovato a Solis dei lavoratori abili per dar forma reale in metallo ai suoi calcoli teorici, perfezionare le singole parti e render finalmente più docile del cavallo e più maneggevole dell’acqua il vapore imprigionato in un meccanismo completo.  
Ogni macchina ha la medesima storia: lunga storia di veglie e di miserie, di disillusioni e di gioie, di miglioramenti parziali trovati successivamente da più generazioni d’operai sconosciuti che hanno aggiunto all’invenzione primitiva quei piccoli nonnulla, senza dei quali pure l’idea più feconda rimane sterile. Più che tutto ciò, ogni nuova invenzione è una sintesi – risultato di mille invenzioni precedenti effettuatesi nell’immenso campo della meccanica e dell’industria. Poichè la scienza e l’industria, la dottrina e l’applicazione, la scoperta e la realizzazione pratica conducono a nuove scoperte, – tutto così si collega e s’incatena, lavoro cerebrale e lavoro manuale, lavoro del pensiero e lavoro del braccio. Ogni scoperta, ogni progresso, ogni aumento della ricchezza umana ha le sue origini nella somma del lavoro manuale e cerebrale del passato e del presente.  
Per qual diritto allora potrebbe chicchessia appropriarsi la menoma particella di quest’immenso tutto, e dire: “Questo è mio, e non è vostro?”

**Masque**
_on 27 maggio 2013 at 18:00_

ll saggio Kropotkin, che ancora una volta mi sorprende per la sua modernita’. :)  
Grazie del pezzo ;)

**Krop**
_on 27 maggio 2013 at 18:09_

“La conquista del pane” è un gran bel saggio, ricco di riferimenti ancora attuali. Certo, Kropotkin è molto molto positivista, fino a sfociare nel determinismo…cosa che Malatesta non gli perdonerà mai! :D

**Masque**
_on 27 maggio 2013 at 18:23_

Era un po’ il “morbo” del periodo, il positivismo scientifico. Ancor meno, allora, sarebbe da “perdonare” il positivismo di Marx. :)

**Florian**
_on 5 giugno 2013 at 14:20_

Ho visto solo ora questa ripubblicazione :)
Grazie e continuate a seguire 6dof!
Daniele.
