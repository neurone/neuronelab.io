title: Epicuro
date: 2014-12-05 13:18:38 +0100
tags: filosofia
summary: Epicuro insegna che la filosofia non è solamente un esercizio mentale teorico, ma che la ricerca della conoscenza che essa porta, può e deve essere usato efficacemente per migliorare la propria condizione di vita, psicologica ed etica. La ricerca della felicità, può avvenire tramite lo studio filosofico.

Epicuro insegna che la filosofia non è solamente un esercizio mentale teorico, ma che la ricerca della conoscenza che essa porta, può e deve essere usato efficacemente per migliorare la propria condizione di vita, psicologica ed etica. La ricerca della felicità, può avvenire tramite lo studio filosofico.

Secondo Epicuro, la vera felicità, identificata col piacere, non è una qualità
che vada ricercata di per se, quanto invece una qualità che nasce dalla mancanza
del male e del dolore e quindi statica. Per eliminare questi, è necessario
imparare a godersi ogni istante della vita. Al contrario, la ricerca del piacere
in sé, porta ad una forma di esso che lui chiama *cinetica*, cioè dinamica,
mutevole e sfuggente, che dura poco ed al suo termine lascia insoddisfatti.
Quest’ultima affermazione, allontana di molto la sua filosofia dall’essere
etichettata come edonistica.

Quello che egli propone, è un insegnamento, chiamato *tetrafarmaco*, che si
occupa di eliminare le quattro paure principali dell’uomo. Eliminando quelle, si
raggiunge la felicità.

1. Paura degli dei e della vita dopo la morte. – Non si devono temere, perché
   gli dei non si interessano degli uomini.
2. Paura della morte – Quando ci siamo noi non c’è la morte, quando c’è la morte
   non ci siamo noi.
3. Mancanza del piacere – Esso è facilmente raggiungibile.
4. Dolore fisico – Se il male è lieve, il dolore fisico è sopportabile, e non è
   mai tale da offuscare la gioia dell’animo; se è acuto, passa presto; se è
   acutissimo, conduce presto alla morte, la quale non è che assoluta
   insensibilità. E i mali dell’anima? Essi sono prodotti dalle opinioni fallaci
   e dagli errori della mente, contro i quali c’è la filosofia e la saggezza.
<!--more-->
Il primo punto è la conclusione di Epicuro per questo ragionamento: Noi sappiamo
che gli dei sono onnipotenti, ossia hanno potere di fare qualunque cosa.
Sappiamo anche, dalle tradizioni, che gli dei sono buoni e benevolenti, ovvero
desiderano che prevalga il bene sul male. Tuttavia, è facile constatare
l’esistenza del dolore e del male. Come si conciliano queste cose?

- Se gli dei non vogliono il male (benevolenti), ma non possono evitarlo,
  significa che non sono onnipotenti.
- Se gli dei possono evitare il male (onnipotenza) , ma non vogliono, significa
  che non sono benevolenti.
- Se gli dei non possono evitare il male e non vogliono, significa che non sono
  né onnipotenti, né benevolenti.
- Quindi, gli dei devono voler e poter evitare il male. Ma allora, perché il
  male esiste?
  
Epicuro arriva alla conclusione che gli dei non si occupano dell’uomo perché
essi vivono negli *intermundia*, cioè gli spazi che esistono fra gli infiniti
universi, inoltre, in virtù della loro perfezione, non hanno bisogno né di
occuparsi dell’uomo, né di interessarsi alla vita terrena.  Infatti, il male
deriva dai desideri non appagati, siano essi naturali o artificiali, e la
felicità dall’assenza di timori e paure che condizionano negativamente la vita.
Da ciò, segue che gli dei non sono necessari all’uomo affinché esso raggiunga la
felicità ed il bene.

*Quando ci siamo noi, non c’è la morte, quando c’è la morte non ci siamo noi.*
Epicuro ritiene che la materia sia composta di atomi, così come l’anima. Con la
differenza che gli atomi di quest’ultima hanno la caratteristica di essere molto
più mobili degli altri. Di conseguenza, la verità e la felicità si possono
raggiungere attraverso i sensi, la felicità corrisponde al senso del piacere e,
essendo l’anima composta di atomi, la morte non ci deve spaventare perché essa
corrisponde alla completa cessazione dei sensi. Quindi, quando ci siamo noi (e
possiamo facilmente constatare l’esistenza di noi stessi) significa che siamo
vivi, quando invece c’è la morte, noi non ci siamo più, perché ciò che ci
costituisce è diventato inerte e con esso cessano anche i sensi (e la capacità
di constatare la nostra esistenza).

Collegamenti:
- [Epicuro (Wikipedia)][1]
- [Epicureismo (Wikipedia)][2]
- [Epicuro.org][3]

[1]:https://it.wikipedia.org/wiki/Epicuro
[2]:https://it.wikipedia.org/wiki/Epicureismo
[3]:http://www.epicuro.org/
