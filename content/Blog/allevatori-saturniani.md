title: Allevatori Saturniani
date: 2015-09-14 19:00
tags: racconti, etica, veg, antispecismo
summary: Benvenute e benvenuti a questa riunione. Mi presento, sono la presidente della Unione allevatori saturniani. In verità con la maggior parte di voi la conoscenza è già più che approfondita. Lavorando gomito a gomito con molti di voi ho potuto apprezzare il vostro impegno nel far crescere le vostre aziende e spero che anche voi possiate riconoscere e apprezzare il sostegno che vi viene offerto dalla nostra agenzia per lo sviluppo. Il nostro gruppo negli anni è cresciuto e siamo una realtà importante nell’economia dell’intera costellazione. Il comparto dell’allevamento dà lavoro a miglia di persone disseminate in 26 pianeti. Il fatturato è in costante crescita grazie alle continue innovazioni e al nostro costante impegno nel dare pronta risposta alle esigenze del mercato. La prerogativa che ci contraddistingue è la nostra capacità di coniugare al meglio tradizione e innovazione.

Racconto di Lucia Calzà

Benvenute e benvenuti a questa riunione. Mi presento, sono la presidente della
Unione allevatori saturniani. In verità con la maggior parte di voi la
conoscenza è già più che approfondita. Lavorando gomito a gomito con molti di
voi ho potuto apprezzare il vostro impegno nel far crescere le vostre aziende e
spero che anche voi possiate riconoscere e apprezzare il sostegno che vi viene
offerto dalla nostra agenzia per lo sviluppo. Il nostro gruppo negli anni è
cresciuto e siamo una realtà importante nell’economia dell’intera
costellazione. Il comparto dell’allevamento dà lavoro a miglia di persone
disseminate in 26 pianeti. Il fatturato è in costante crescita grazie alle
continue innovazioni e al nostro costante impegno nel dare pronta risposta alle
esigenze del mercato. La prerogativa che ci contraddistingue è la nostra
capacità di coniugare al meglio tradizione e innovazione.

Il motivo di questo nostro ritrovarci oggi ha a che fare proprio con una
interessante innovazione che certamente porterà nuova e importante linfa
all’intero settore. Una innovazione che risponde a una duplice tendenza del
mercato: lo sappiamo bene, i consumatori si fanno sempre più esigenti. La carne
e il latte e i derivati devono non soltanto essere buoni e sani, ma anche
ecologicamente ed eticamente sostenibili. Questo si traduce nella necessità che
l’intera filiera della carne, del latte e dei derivati sia impeccabilmente
rispettosa dell’ambiente e attenta al benessere animale. Perdonatemi se vi dico
queste cose che per tutti voi sono nozioni acquisite. Lo faccio solo per dovere
di protocollo. So bene che le priorità della qualità, del rispetto
dell’ambiente e del benessere animale sono ben presenti nel vostro lavoro di
tutti i giorni e nelle scelte programmatiche, fin da quando abbiamo aderito
alla rete del biologico e a quella del prodotto “eticamente ok”.

Ma ora basta con le premesse, ora voglio velocemente arrivare alla novità che
oggi ci riunisce qui, e lo faccio con grande entusiasmo per tutte le nuove e
interessanti propositive che si stanno aprendo sulle nostre aziende.  
Sapete che fino ad oggi i numerosi tentativi e le tante piccole esperienze di
allevamento di animali umani per carne e derivati sono stati insoddisfacenti e
spesso anche fallimentari. Gli animali umani mal reagivano alla cattività
rifiutando il cibo e producendo una carne e un latte poco nutriente, poco
appetibile e con un costo finale del prodotto al consumatore esageratamente
alto. Oggi ci troviamo a ringraziare quegli allevatori che non si sono dati per
vinti e che di fronte alle difficoltà e agli insuccessi non hanno mai
abbandonato del tutto l’allevamento di umani riversandovi grande passione e
amore. Oggi i loro sforzi e la loro passione viene ricompensata con
l’introduzione di prospettive completamente nuove e di grande interesse per
tutti noi. Con molta fierezza, dopo un lavoro di anni, possiamo comunicarvi di
essere riusciti a superare le difficoltà insite nell’allevamento di animali
umani. Il lungo lavoro di selezione genetica ha dato i suoi frutti e siamo
riusciti ad ottenere degli esemplari che si adattano molto bene alla vita in
cattività. Il loro classico istinto di ribellione era la fonte principale di
insuccesso del loro allevamento, perché col tempo si trasformava in
annichilimento e in uno stato depressivo talmente grave da condurre alla morte.
Ora tutto questo è stato risolto. Gli animali umani di nuova generazione sono
tutta un’altra cosa. Hanno un temperamento mansueto e la loro gestione non
comporta più alcun tipo di problematicità. Questa è sicuramente la variazione
genetica più apprezzabile che migliora le caratteristiche dell’animale umano
che finalmente diventa ottimamente compatibile con una proficua domesticazione.
I vantaggi sono certamente vantaggi per l’allevatore ma anche e soprattutto
vantaggi per l’animale stesso. Tutte le operazioni di stalla fino al trasporto
al macello sono notevolmente più facili da gestire e tutta la vita dell’animale
non è segnata dalle reazioni stressanti che ne rendevano problematico
l’allevamento. L’animale umano di nuova generazione gode di una vita
notevolmente più rilassata. Da anni privilegiamo l’allevamento di quegli
animali che si possono definire animali felici, e finalmente è possibile anche
con gli umani. Oltre a questo primario aspetto le modificazioni genetiche
riguardano la nascita di animali privi di pollice e privi del tendine
responsabile della posizione eretta. Sappiamo che nell’umano il pollice è il
responsabile della mano prensile, e quindi della ossessiva tendenza degli
animali umani a modificare l’ambiente in cui vivono, con tutte le problematiche
che questo comporta. Fino ad oggi l’eliminazione del pollice prensile avveniva
in modo effettivamente cruento mediante il taglio con una apposita cesoia,
mentre da oggi questa operazione dolorosa e costosa viene del tutto superata.
Anche riportare l’animale alla posizione prona comporta un maggior benessere,
soprattutto per l’umana femmina. Infatti per l’animale risulta molto più
agevole sostenere il peso delle mammelle e la femmina è agevolata anche in
tutta la fase della gestazione. Ho usato di proposito l’espressione “riportare
l’animale alla posizione prona” perche la zoo antropologia ha dimostrato che
l’umano originariamente era su quattro zampe. Dico questo pensando alle
prevedibili (quanto noiose) obiezioni del movimento animalista.

Domande dal pubblico
--------------------
*Qualche cosa in effetti si sta muovendo. Mio figlio ha letto sul sito di un
fantomatico movimento che si definisce antispecista che in altri pianeti gli
animali umani sarebbero trattati come animali da compagnia, con grande
soddisfazione e anche con grande coinvolgimento emotivo. L’animale umano
condividerebbe addirittura la vita di casa entrando quasi a far parte delle
famiglie.*

Si certo probabilmente è vero, ma non c’è niente di strano in questo. Per
fortuna siamo in una democrazia e ognuno gode di libertà di scelta, una persona
può decidere che un serpente è il suo migliore amico. Noi di certo non glielo
impediamo, e lui di certo non può pretendere che sia così anche per noi. Oltre
alle scelte individuali poi ci sono le diversità culturali. Per una popolazione
la mucca è un animale sacro e mangia il maiale, un’altra ha un comportamento
esattamente contrario. Quello che prevale per fortuna è sempre la libertà di
scelta e di comportamento, ovviamente nel rispetto degli altri. Non pretendiamo
di modificare le abitudini di un altro popolo, ma quest’altro popolo non deve
pretendere di cambiare le nostre.

*Quando saranno disponibili le fattrici per avviare l’allevamento degli umani?*

Tutto è pronto per partire già domani. E oggi compilando l’apposito modulo
potete prenotare le vostre fattrici che vi verranno consegnata nel giro di
pochi giorni, marchiate, vaccinate, e accompagnate da tutti i documenti che ne
attestano provenienza e caratteristiche, come è previsto dalla legge. Come
ciliegina sulla torta, vi verrà consegnato anche delle applicazioni per donare
all’animale un sottofondo musicale personalizzato. La musica a scopo di
aumentare il benessere animale è già da tempo entrata nelle stalle, oggi è
possibile un passo un più, avendo selezionato per ciascun animale il tipo di
musica a lui più gradito.

*Sono previste sovvenzioni per convertire una stalla all’allevamento di umani?*

Si certo. Il nostro intero percorso di ricerca e di sviluppo è stato possibile
grazie al sostegno statale che ha creduto e crede in questa innovazione nel
settore dell’allevamento e della trasformazione. I contributi sono interessanti
e nei nostri uffici troverete funzionari che vi daranno tutte le informazioni
necessarie. 

*Vorrei informazioni sulla capacità produttiva e sulla redditività di questo
allevamento.*

L’umana femmina geneticamente modificata allo stato attuale è stata denominata
X103. Produce in media 20 litri di latte al giorno di ottimo valore nutritivo,
ovviamente se alimentata secondo i protocolli. Anche la trasformazione del
latte in formaggi e Yogurt dà ottimi risultati sia nel gusto che nelle
caratteristiche organolettiche. La carne migliore si ottiene nella macellazione
dei maschi di età non superiore ai 4 mesi. E’ una carne bianca, tenerissima e
con poco grasso. Proprio quello che il mercato in questo momento richiede.
Siamo certi che il successo di mercato sia garantito, e che valga assolutamente
la pena gettarsi in questa nuova avventura. Anche qui i nostri tecnici assieme
agli analisti del mercato vi daranno tutte le informazioni specifiche per
redigere un piano di reddito riferito alla vostra azienda e alle vostre
specifiche intenzioni.

*L’umano è un animale che si presta a essere utilizzato in una fattoria
didattica?*

Sono felice di questa domanda, e rispondo con un deciso si. Soprattutto l’umana
femmina è estremamente docile e si presta molto bene a interagire con i
bambini. Questo tra l’altro è un altro punto importante della nostra filosofia,
quella cioè di creare cultura, creare conoscenza, perché attraverso
l’esperienza diretta possa nascere consapevolezza e rispetto per gli animali e
per quello che ci donano e in generalità per la sacralità di tutto quello che
ci viene donato dall’universo e che ci garantisce la vita. Ma non può esserci
vero rispetto senza la conoscenza. Per questo motivo come associazione
favoriamo la nascita delle fattorie didattiche e abbiamo predisposto un
apposito piano per le fattorie che inseriranno anche gli animali umani nella
loro proposta didattica. Anche per questo informazioni dettagliate sono a
disposizione dai nostri funzionari che potete chiamare per un colloquio
direttamente nella vostra azienda.

*Ho sentito dire che gli umani sono animali molto intelligenti, è vero?*

E’ vero, gli umani sono dotati di una certa intelligenza, il cui livello è
mediamente paragonabile a quello di un nostro bambino di 3 anni di età. E’ una
intelligenza molto rozza, dominata in modo quasi assoluto dagli istinti. In
natura sulla terra gli umani sono paragonabili a una calamità naturale. Sono
animali estremamente dominanti, prevaricatori e parassitari. Si uccidono con
estrema facilità tra di loro, non hanno sviluppato alcuna forma di intelligenza
volta all’empatia e alla solidarietà. E’ la specie che mette a rischio la
sopravvivenza del pianeta terra, non avendo alcuna cura per il territorio in
cui vivono. Il loro allevamento ha quindi anche un valore zoo antropologico nel
garantire la sopravvivenza della specie che in natura è costantemente a rischio
di estinzione.

*Vi ringrazio di poter esprimere il mio pensiero. Sapendo di questa riunione ho
pensato di scrivere questa riflessione.  
Vi chiedo scusa perché le mie parole non vi risuoneranno gradite e facili da
ascoltare. Vorrei che mi credeste, mi dispiace non poter condividere con voi
l’entusiasmo per il vostro lavoro, e il bello spirito di gruppo che vi anima.
La relatrice ha parlato di crescita del fatturato, ma la civiltà di un popolo
non si misura con il Pil, si misura sulla sua capacità di essere accogliente
dei bisogni di tutti, ma proprio di tutti, saturniani, vesuviani, terrestri,
marziani.  
So bene che sovvertire un vissuto non è facile. Non è facile smettere di
fumare, figurarsi sovvertire una vita intera, una società intera.  
Ma vi chiedo, e lo chiedo soprattutto ai giovani, perché dovete abituarvi a
portare privazione e morte con il vostro lavoro quotidiano? Ve lo dico in modo
molto diretto. Perché non fate una rivoluzione nelle vostre vite, perché non vi
lasciate coinvolgere da una gioiosa conversione. Perché non fate una vera pace
con i vostri animali, potendoli finalmente guardare negli occhi con occhi
sereni. Certo, io e molti altri come me vi sosterremo, noi vi aiuteremo, questo
è doveroso da parte nostra. Uniamoci tutti, crediamo in un mondo migliore,
sogniamo e lavoriamo insieme per un mondo migliore, delicato, sincero,
comprensivo. Basta allevamenti, basta schiavitù, e basta anche con l’imbroglio
della fattoria felice.*

Caro amico, prima di tutto lei non dovrebbe essere qui perché questa è una
riunione privata, ma le diamo ugualmente il benvenuto, perché non abbiamo nulla
da nascondere, e le abbiamo dato la parola per cortesia.  
Per pura cortesia rispondo alle sue istanze, sperando che poi gentilmente lei
ci lasci proseguire.  

L’allevamento è parte fondamentale della nostra storia. Portiamo
avanti il lavoro dei nostri padri, e dei nostri nonni, esattamente come loro
avevano portato avanti il lavoro dei loro padri e dei loro nonni. L’allevamento
ha una tradizione millenaria. Senza persone che si siano dedicate a questo
lavoro noi oggi non saremmo così come siamo: una società fatta di persone
libere, che hanno coltivato il senso della vita, che hanno visto nel lavoro
solidale il fondamento del progresso inteso in tutti i suoi aspetti: morali,
etici, economici, materiali. Che ci sia una differenza tra noi e gli animali è
cosa indubbia. Lei con le sue parole ci sembra del tutto fuori dalla storia.
Che lei voglia sovvertire l’ordine del mondo è legittimo, se lo fa nel rispetto
degli altri.  
La preghiamo di tornare da noi quando avrà delle segnalazioni di infrazioni
delle leggi da parte nostra. Se lei un giorno trovasse dei comportamenti dei
nostri allevatori che non rispettano le leggi sul benessere animale, o
sull’inquinamento ambientale, o sul trattamento dei lavoratori, noi saremo ben
felici di ascoltarla e la ringrazieremo per la segnalazione. Ma che lei oggi
venga qui a parlarci di cose assolutamente astratte non ci pare il caso. Noi
contribuiamo alla crescita della nostra società, svolgiamo un lavoro che
risponde alle richieste del mercato, e lo facciamo a costo di fatica e animati
da passione e senso civico. 

Bene, chiedo scusa per la divagazione.

Ora darei il via libera alle prenotazioni. Al tavolino all’uscita potete
lasciare il vostro codice alla nostra gentile signora Marina.

Vi ringrazio per la cortese attenzione e vi saluto felice delle nuove ed
entusiasmanti prospettive che ci vengono offerte.

Buona giornata e ancora grazie, buon lavoro a tutti !
