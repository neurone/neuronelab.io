title: Little Brother
date: 26-08-2015 18:35
tags: libri, fantascienza, società, recensioni
summary: Scritto per un pubblico di adolescenti e giovani adulti, mette in campo una serie di temi molto seri ed attuali, quali la protezione della privacy ed il controllo di massa da parte degli stati, le leggi antiterrorismo, il bisogno di sentirsi sicuri e la strumentalizzazione di questi temi da parte della politica istituzionale, l'hacktivismo, la libertà d'espressione e il ruolo dei media come informatori imparziali, oppure come mezzi usati dalla politica per mantenere lo status-quo.

![Little Brother]({filename}/images/little-brother.jpg "Copertina: Little Brother"){: style="float:right"}
Davvero un notevole romanzo, questo *Little Brother* - in originale "X" - dello scrittore ed attivista [Cory Doctorow][1].

Scritto per un pubblico di adolescenti e giovani adulti, mette in campo una serie di temi molto seri ed attuali, quali la protezione della privacy ed il controllo di massa da parte degli stati, le leggi antiterrorismo, il bisogno di sentirsi sicuri e la strumentalizzazione di questi temi da parte della politica istituzionale, l'[hacktivismo][2], la libertà d'espressione e il ruolo dei media come informatori imparziali, oppure come mezzi usati dalla politica per mantenere lo status-quo.

Non sorprenderebbe se fosse stato scritto in questo periodo, dopo gli scandali
del [Datagate][3], rivelati da [Edward Snowden][4], ma il libro fu scritto nel
2008 e pubblicato in Italia solo quest'anno da Multiplayer Edizioni. Tanto che,
lo stesso Snowden l'aveva preso con se all'inizio della sua fuga... per avere
qualcosa da leggere.

Tutto ciò che viene raccontato, in parte è già accaduto e se non è già
esistente tutt'oggi, lo potrebbe essere a brevissimo. Ciò che fa, è dimostrare
quanto poco manchi al presente per trasformarsi in una distopia orwelliana, e
vi riesce in modo sorprendente. La narrazione è briosa e scorrevole, tra
genialate tecnologiche, riferimenti a scrittori beat, attivisti anarchici,
[TAZ][5] e flash-mob da deriva situazionista, la tecnologia che viene descritta
e le azioni di hacking sono assolutamente plausibili se non reali, tanto che
spesso sembra di leggere alcune delle storie di hacking pubblicate dalla
rivista ["2600"]({filename}i-love-hacking-il-meglio-della-rivista-2600.md).

> Nella severissima Chavez High School, a San Francisco, il preside ha
> installato un sistema ultramoderno per monitorare minuto per minuto le
> attività degli studenti. Uscire dalla scuola senza permesso, però, non è mai
> stato un problema per Marcus, noto sul web com "w1n5t0n": lui conosce tutti i
> segreti della rete ed è in grado di neutralizzare qualsiasi dispositivo di
> sorveglianza. Mentre i compagni rimangono a scuola, Marcus e i suoi amici
> Darryl, Vanessa e Jolu si divertono per le strade della città, quando
> improvvisamente una terribile esplosione distrugge il centro di San Francisco.
> I quattro, al posto sbagliato nel momento sbagliato, vengono arrestati perché
> ritenuti coinvolti nella strage.
> Chiusi in carcere senza alcun processo e torturati perché confessino, i
> ragazzi sperimentano sulla loro pelle la violenza e la crudeltà della Polizia.
> Grazio a una console modificata per eludere i controlli del governo, una volta
> libero w1n5t0n darà via a una comunità di ribelli non violenti, intenzionati a
> combattere lo strapotere del Dipartimento di Pubblica Sicurezza. La tecnologia
> e l'informatica saranno le armi vincenti per combattere l'oppressione e la
> violenza.

Come bonus, pubblico il racconto brevissimo
[Scroogled]({filename}/pdfs/cory-doctorow-scroogled.pdf), sempre di Cory
Doctorow. Gioco di parole fra "screwed", fottuto, e Google.

[1]:https://it.wikipedia.org/wiki/Cory_Doctorow
[2]:https://it.wikipedia.org/wiki/Hacktivism
[3]:https://it.wikipedia.org/wiki/Divulgazioni_sulla_sorveglianza_di_massa_del_2013
[4]:https://it.wikipedia.org/wiki/Edward_Snowden
[5]:https://it.wikipedia.org/wiki/Zona_temporaneamente_autonoma
