title: Non serviam
date: 2014-07-15 18:41:19 +0200
tags: fantascienza, filosofia, filosofia della mente, religione, intelligenza artificiale
summary: Il racconto, scritto in modo sottilmente ironico da Stanislaw Lem, è sviluppato in maniera abbastanza inusuale: finge di essere la recensione di un libro riguardate una disciplina, la personetica, che si può definire come una sorta di sviluppo collettivo di intelligenze artificiali con personalità. Nella prima metà viene spiegato come sono costruiti questi personoidi e l’universum matematico in cui vivono.

Il racconto, scritto in modo sottilmente ironico da [Stanislaw Lem][1], è sviluppato in maniera abbastanza inusuale: finge di essere la recensione di un libro riguardate una disciplina, la personetica, che si può definire come una sorta di sviluppo collettivo di intelligenze artificiali con personalità. Nella prima metà viene spiegato come sono costruiti questi personoidi e l’universum matematico in cui vivono.

![Game of life]({filename}/images/Conways_game_of_life_breeder_animation.gif "Game of Life")

Inizialmente ci viene introdotta questa scienza e descritta a grandi linee.
Veniamo a sapere che questa disciplina ha fatto molto scalpore ed è considerata,
dai più, amorale.  
Ci viene descritta la struttura del mondo in cui “vivono” questi *personoidi* ed
apprendiamo che si tratta di un mondo creato al calcolatore, completamente
*basato sulla matematica*, analogamente al nostro, basato sullo spazio fisico.
Il mondo dei personoidi, può avere *più dimensioni*, ma non nel senso delle
nostre dimensioni spaziali quanto nel senso matematico del termine. Pensate ad
una *matrice multi dimensionale*.  
Solitamente, si è portati a pensare che un'intelligenza artificiale e
l’eventuale mondo che le viene creato attorno, debbano essere riconducibili al
nostro, come una sorta di realtà virtuale. Come se l’intelligenza artificiale
*debba per forza ricondursi ad una imitazione dell’intelligenza umana*, animale,
o di qualsiasi altra macchina biologica che conosciamo. Questo, seppur molto
radicato nel pensiero comune, può non essere necessario. Un'intelligenza
artificiale, può benissimo svilupparsi in un ambiente completamente diverso dal
nostro, senza la necessità di renderlo direttamente esperibile da noi.  
Anche in questo caso, anche se meno marcatamente rispetto a Solaris e Picnic sul
ciglio della strada, si fa una critica all'*antropocentrismo*, cioè al tradurre
ogni cosa ad un equivalente umano.

Nella seconda metà del racconto, gli stessi personoidi discutono dell’esistenza
o meno di un creatore, che nel loro caso, sappiamo per certo esistere: i
ricercatori che li hanno creati, e siano dovuti loro rispetto e riconoscenza. Si
comprende, infine, il significato del titolo.

Questo racconto è pubblicato sul libro *L'Io della mente*, edito in Italia da
*Adelphi*.
<!--more-->

Non Serviam
-----------
*di [Stanislaw Lem][1]*

Il libro del professor Dobb è dedicato alla personetica, che il filosofo
finlandese Eino Kaikki ha definito “la scienza più crudele che l’uomo abbia mai
creato”. Dobb, uno dei più eminenti personeisti contemporanei, condivide questa
opinione. Non si può, egli afferma, sottrarsi alla conclusione che la
personetica, nelle sue applicazioni, è immorale; tuttavia abbiamo a che fare con
un tipo d’indagine che, per quanto contrario ai princìpi etici, è per noi
necessario dal punto di vista pratico. Non c’è modo nella ricerca di evitare sia
la sua raffinata spietatezza sia di fare violenza agli istinti naturali
dell’uomo e, se anche regge altrove, il mito dell’innocenza assoluta dello
scienziato come indagatore di fatti qui è certamente crollato. Non si dimentichi
che stiamo parlando di una disciplina che, con pochissima esagerazione, è stata
chiamata “teogonia sperimentale”. Nonostante ciò, chi scrive questa recensione è
colpito dal fatto che, quando la stampa, nove anni fa, diede grande rilievo alla
cosa, l’opinione pubblica rimase sbalordita dalle prospettive aperte dalla
personetica. C’era motivo di credere che ai nostri giorni nulla potesse
sorprenderci. L’eco dell’impresa di Colombo risuonò per secoli, mentre la
conquista della Luna nel giro di una settimana fu assimilata dalla coscienza
collettiva come una cosa praticamente ovvia. E invece la nascita della
personetica si dimostrò un trauma.

Il nome combina insieme un termine derivato dal latino ed uno dal greco:
“persona” e “genetica” (“genetica” nel senso di formazione o creazione). Questo
campo è una recente diramazione della cibernetica e della psiconica degli Anni
Ottanta, ibridata con l’intellettronica applicata. Oggi tutti hanno sentito
parlare della personetica; l’uomo della strada, interrogato, direbbe che si
tratta della produzione artificiale di esseri intelligenti, una risposta certo
non lontana dal vero, ma che tuttavia non raggiunge il nocciolo effettivo della
questione. A tutt’oggi possediamo un centinaio di programmi personetici. Nove
anni fa vennero elaborati schemi d’identità – nuclei primitivi di tipo “lineare”
– ma neppure nella generazione di calcolatori, che oggi ha solo valore storico,
era in grado di fornire il campo per la vera e propria creazione di personoidi.

La possibilità teorica di creare la sensibilità consapevole fu profetizzata
tempo fa da Norbert Wiener, come testimoniano certi passi del suo ultimo libro,
*Dio e Golem s.p.a.* Certo, egli vi alludeva nel suo tipico modo semifaceto, ma
sotto quel tono leggero vi erano premonizioni piuttosto sinistre. Wiener
tuttavia non avrebbe potuto prevedere la direzione che avrebbero preso le cose
vent’anni dopo. Il peggio avvenne – per usare le parole di Sir Donald Acker –
quando al MIT “gli ingressi furono messi in corto con le uscite”.

Oggi è possibile preparare un “mondo” per “abitanti” personoidi nel giro di un
paio d’ore: tanto ci vuole per introdurre nella macchina uno dei programmi
pronti all’uso (come il BAAL 66, il CREAN IV o lo JAHVE 09). Dobb descrive gli
inizi della personetica abbastanza per sommi capi, rinviando il lettore alle
fonti storiche; essendo lui stesso da sempre uno sperimentatore professionista,
Dobb parla soprattutto del proprio lavoro, e questo cade molto a proposito,
poiché tra la scuola inglese da lui rappresentata e il gruppo americano del MIT
vi sono differenze notevoli sia sotto il profilo del metodo sia per quanto
riguarda gli scopi sperimentali. Ecco come Dobb descrive la procedura dei “6
giorni in 120 minuti”: per prima cosa si fornisce alla memoria della macchina un
insieme minimo di dati; cioè – per mantenersi entro un linguaggio comprensibile
al profano – si carica la memoria con una sostanza di natura “matematica”.
Questa sostanza è il protoplasma di un “universum” che dovrà essere “abitato”
dai personoidi. A questo punto siamo in grado di fornire agli esseri che
entreranno in questo mondo meccanico e digitale – che condurranno la loro
esistenza in esso e soltanto in esso –, un ambiente con caratteristiche non
finite. Questi esseri, quindi, non possono sentirsi prigionieri in senso fisico,
poiché l’ambiente, dal loro punto di vista, non ha confini. Il mezzo possiede
una sola dimensione, che somiglia ad una dimensione data anche a noi: quella del
passaggio del tempo (durata). Il loro tempo non presenta tuttavia un’analogia
diretta col nostro, poiché la velocità del suo fluiure è soggetta al controllo
discrezionale dello sperimentatore. Di regola la velocità è resa massima nella
fase preliminare (il cosiddetto fomento creazionale), sicché i nostri minuti
corrispondono, nel calcolatore, a interi eoni durante i quali ha luogo tutta una
serie di riorganizzazioni e cristallizzazioni successive… di un cosmo sintetico.
È un cosmo assolutamente non spaziale, pur possedendo molte dimensioni; queste
infatti hanno carattere puramente matematico e quindi, si potrebbe dire,
“immaginario”. Esse sono, molto semplicemente, la conseguenza di certe decisioni
assiomatiche del programmatore e il loro numero dipende da lui. Se per esempio
egli decidesse di inserirvi dieci dimensioni, ciò avrà per la struttura del
mondo creato conseguenza affatto diverse che se ne avesse inserite sei. Dobbiamo
sottolineare che queste dimensioni non hanno alcun rapporto con quelle dello
spazio fisico, ma lo hanno solo con le costruzioni astratte e logicamente valide
impiegate nella creazione dei sistemi.

Dobb tenta di spiegare questo punto, quasi inaccessibile al non matematico,
ricorrendo a fatti semplici del tipo che in genere si impara a scuola. È
possibile, come sappiamo, costruire un solido geometrico tridimensionale
regolare – diciamo un cubo – che nel mondo reale ha un corrispettivo della forma
del dado; ed è altrettanto possibile creare solidi geometrici di quattro,
cinque, n dimensioni (quello a quattro dimensioni è un tesseratto). A questi non
corrisponde più nulla di reale e lo possiamo constatare, dato che, in assenza di
una quarta dimensione fisica, non vi è alcun modo di foggiare un dado
quadridimensionale autentico. Ebbene, questa distinzione (fra ciò che si può
costruire fisicamente e ciò che si può fare solo matematicamente) in generale
non sussiste per i personoidi, poiché il loro mondo ha una consistenza puramente
matematica. È fatto di matematica, benché i blocchi di costruzione di questa
matematica siano oggetti ordinari e di natura assolutamente fisica (relé,
transistori, circuiti logici: in una parola, tutta l’immensa rete di circuiti
della macchina digitale).

Come ci insegna la fisica moderna, lo spazio non è qualcosa di indipendente
dagli oggetti e dalle masse che vi sono situati. L’esistenza dello spazio è
determinata da quei corpi; dove essi non sono, dove non c’è nulla – in senso
materiale – là cessa di esistere anche lo spazio, annullandosi. Orbene, questa
funzione dei corpi materiali, che estendono la loro “influenza”, per così dire,
e “generano” con ciò lo spazio, è esplicata nel mondo dei personoidi dai sistemi
di una matematica che è posta in essere proprio a questo fine. Fra tutte le
“matematiche” costruibili in generale (ad esempio per via assiomatica), il
programmatore, avendo deciso di compiere un dato esperimento, ne sceglie un
gruppo particolare, che servirà da puntello, da “substrato essenziale”, da
“fondamento ontologico” dell’universum creato. Vi è in tutto ciò, secondo Dobb,
una somiglianza sorprendente col mondo umano. Questo nostro mondo, dopo tutto,
ha “deciso” per certe forme e per certi tipi di geometria che meglio gli si
addicono: meglio, vale a dire nel modo più semplice (la tridimensionalità, per
restare nell’ambito iniziale). Nonostante ciò, noi siamo in grado di figurarci
“altri mondi” con “altre proprietà”, nel campo della geometria e non solo di
quello. Lo stesso accade per i personoidi: l’aspetto della matematica che il
ricercatore ha scelto come “habitat” è per loro esattamente ciò che per noi è la
“base del mondo reale” in cui viviamo, in cui siamo costretti a vivere. E, come
noi, i personoidi sono in grado di “figurarsi” mondi aventi proprietà
fondamentali diverse.

Dobb presenta la sua materia usando il metodo delle approssimazioni e
ricapitolazioni successive; ciò che abbiamo tratteggiato or ora, e che
corrisponde grosso modo ai primi due capitoli del libro, nei capitoli successivi
viene in parte ripreso e complicato. L’autore ci avverte che non è del tutto
vero che i personoidi vengano semplicemente a trovarsi in un mondo
preconfezionato, fisso e irrevocabilmente irrigidito nella sua forma definitiva;
l’aspetto di quel mondo, nei suoi particolari specifici, dipende da loro, e
tanto più ne dipende quanto più aumenta la loro capacità di attività, quanto più
si sviluppa la loro “iniziativa nell’esplorazione”. Neanche paragonare
l’universum dei personoidi a un mondo in cui i fenomeni esistono solo nella
misura in cui i suoi abitanti li osservano fornisce un’immagine precisa della
situazione. Questo paragone, che si incontra nei lavori di Sainter e Huges,
viene considerato da Dobb una “deviazione idealistica”: un omaggio che la
personetica ha reso alla dottrina, risuscitata in modo così curioso e repentino
del Berkeley. Sainter sosteneva che i personoidi conoscerebbero il loro mondo
alla maniera di un essere berkeleyano, che non è in grado di distinguere
l’*esse* dal *percipi*, ossia un essere che non scoprirà mai la differenza tra
la cosa percepita e ciò che causa la percezione in modo oggettivo e indipendente
dal percipiente. Dobb rigetta con forza questa interpretazione. Noi, creatori
del loro mondo, sappiamo benissimo che ciò che viene da loro percepito esiste
davvero; esiste all’interno del calcolatore, indipendentemente da loro, benché,
ovviamente, solo alla maniera degli oggetti matematici.

Poi ci sono ulteriori chiarificazioni. I personoidi sorgono in modo germinale in
virtù del programma; essi si moltiplicano a una velocità imposta dallo
sperimentatore, una velocità che solo la più recente tecnologia
dell’elaborazione dell’informazione, che opera a velocità prossime a quelle
della luce, può consentire. La matematica che dovrà costituire la “residenza
esistenziale” dei personoidi non li attende nella sua pienezza, bensì, per così
dire, ancora in “fasce”, inarticolata, sospesa, latente, poiché rappresenta solo
un insieme di certe possibilità a venire, di certi percorsi contenuti in
subunità opportunamente programmate della macchina. Queste subunità, o
generatori, in sé e per sé non danno alcun contributo; è piuttosto un tipo
particolare di attività dei personoidi che serve a meccanismo d’innesco e mette
in moto un processo di produzione che via via aumenta e si definisce; in altre
parole, il mondo che circonda questi esseri acquista univoca determinazione in
dipendenza dal loro stesso comportamento. Dobb cerca di illustrare questo
concetto facendo ricorso alla seguente analogia. Un uomo può interpretare il
mondo reale in maniere diverse. Può dedicare, ad esempio, un’attenzione speciale
– un’intensa indagine scientifica – a certi aspetti di questo mondo, e le
conoscenze che egli così acquisisce gettano in seguito la loro luce particolare
sulle restanti porzioni del mondo, quelle non considerate nella sua ricerca
prioritaria. Se egli si dedica allo studio diligente della meccanica, si foggerà
un modello meccanico del mondo e vedrà l’Universo come un gigantesco e perfetto
orologio che con moto inesorabile procede dal passato verso un futuro
determinato con precisione. Questo modello non costituisce una rappresentazione
accurata della realtà, e tuttavia si può farne uso per un lungo periodo storico
e ottenere grazie a esso anche molti successi pratici, come la costruzione di
macchine, di strumenti, eccetera. Analogamente, se i personoidi “inclineranno”,
per scelta, per un atto di volontà, verso un certo tipo di rapporto col loro
universum, e ad esso daranno priorità, se in questo e solo in questo scopriranno
l’“essenza” del loro cosmo, allora essi percorreranno un cammino ben definito di
cimenti e di scoperte, un cammino che non sarà né illusorio né futile. La loro
inclinazione “estrae” dall’ambiente ciò che meglio le corrisponde.Ciò che
percepiscono per primo è ciò che per primo padroneggiano, dato che il mondo che
li circonda è determinato solo parzialmente, è solo parzialmente fissato in
anticipo dal creatore-ricercatore; in esso i personoidi conservano un margine
nient’affatto insignificante di libertà di azione, azione tanto “mentale”
(nell’ambito di ciò che essi pensano del proprio mondo, di come lo
concepiscono), quanto “reale” (nel contesto dei loro “atti”, che non sono certo
reali alla lettera, nel nostro senso del termine, ma non sono neppure puramente
immaginari). Questa è, invero, la parte più difficile dell’esposizione e Dobb,
osiamo dire, non riesce a spiegare in modo del tutto soddisfacente queste
speciali qualità dell’esistenza dei personoidi, qualità che possono essere rese
solo nel linguaggio della matematica dei programmi e degli interventi di
creazione. Dobbiamo quindi accettare per così dire con un atto di fede l’idea
che l’attività dei personoidi non sia né del tutto libera, così come lo spazio
delle nostre azioni, non è del tutto libero, essendo limitato dalle leggi
fisiche della natura, né del tutto determinata, proprio come noi non siamo
vagoni che corrono su binari rigidamente fissati. Un personoide somiglia a un
uomo anche per il fatto che le “qualità secondarie” dell’uomo – i colori, i
suoni melodiosi, la bellezza delle cose – possono manifestarsi solo quando egli
ha orecchi per udire e occhi per vedere, ma ciò che rende possibile udire e
vedere è stato, in fin dei conti, fornito in precedenza. I personoidi, nel
percepire il loro ambiente, gli conferiscono, traendole da dentro di loro,
quelle qualità esperienziali che corrispondono esattamente a ciò che per noi è
la bellezza di un paesaggio contemplato – con la differenza, naturalmente, che a
loro è stato fornito uno scenario puramente matematico. Quanto al problema di
“come lo vedano”, non si asserisce nulla, poiché l’unico modo di apprendere la
“qualità soggettiva della loro sensazione” sarebbe quello di spogliarsi del
nostro involucro umano e diventare personoidi. I personoidi, lo si rammenti, non
hanno né occhi né orecchi, perciò non vedono e non odono nel senso nostro; nel
loro cosmo non c’è luce, non ci sono tenebre, non c’è prossimità spaziale né
distanza, non c’è il sopra o il sotto; ci sono dimensioni che per noi non sono
tangibili, ma che per loro sono primarie, fondamentali; essi per esempio
percepiscono alla stregua di elementi della coscienza sensoriale umana certe
variazioni del potenziale elettrico. Ma queste variazioni di potenziale non
hanno per loro la natura, diciamo, di pressioni di corrente, sono piuttosto quel
genere di cose che, per un uomo, sono i fenomeni ottici o acustici più
rudimentali: la visione di una macchia rossa, la percezione di un suono, o il
contatto con un oggetto duro o morbido. Da questo punto in poi, sottolinea Dobb,
si può parlare solo mediante analogie, evocazioni.

Affermare che i personoidi sono “minorati” rispetto a noi, in quanto non vedono
e non odono come noi, è totalmente assurdo, poiché allo stesso titolo si
potrebbe asserire che siamo noi carenti rispetto a loro, perché incapaci di
sentire con immediatezza il fenomenismo della matematica che, dopo tutto, noi
conosciamo solo per via cerebrale e inferenziale. Noi entriamo in contatto con
la matematica solo attraverso il ragionamento, ne facciamo “esperienza” solo
attraverso il pensiero astratto. Invece i personoidi *vivono* in essa: essa è la
loro aria, la loro terra, l’acqua, le nubi, perfino il loro pane: sì, perfino il
cibo, poiché in un certo senso da essa traggono nutrimento. E, allo stesso modo,
soltanto dal nostro punto di vista essi appaiono “imprigionati”, chiusi
ermeticamente dentro la macchina: proprio come loro non possono trovare una
strada per giungere fino a noi, fino al mondo degli uomini, così per converso –
e simmetricamente – un uomo non può in alcun modo penetrare all’interno del loro
mondo così da esistere in esso e conoscerlo direttamente. Quindi la matematica,
in alcune sue incarnazioni, è diventata lo spazio vitale di un’intelligenza a
tal punto spiritualizzata da essere affatto incorporea; è diventata la nicchia e
la culla della sua esistenza, il suo elemento.

I personoidi sono per molti versi simili all’uomo. Sono capaci di immaginare una
contraddizione particolare (che vale *a* e che vale non-*a*), ma non sono in
grado di attualizzarla , esattamente come non lo siamo noi. La fisica del nostro
mondo e la logica del loro non lo permettono, poiché la logica costituisce per
l’universum dei personoidi esattamente la stessa cornice di delimitazione delle
azioni che la fisica costituisce per il nostro mondo. In ogni caso, sottolinea
Dobb, è assolutamente impossibile per noi affermare introspettivamente con
pienezza ciò che “sentono” e “sperimentano” i personoidi mentre svolgono la loro
intensa vita nel loro universum nonfinito. La sua totale non spazialità non lo
rende una prigione come invece si sono affrettati a dire i giornalisti – anzi è
il contrario: è la garanzia della loro libertà, poiché la matematica prodotta
dai generatori del calcolatore quando vengono “eccitati” dall’attività (e ciò
che li eccita a questo modo è precisamente l’attività dei personoidi), questa
matematica è, per così dire, un campo infinito autogenerantesi di azioni
facoltative, di imprese architettoniche e d’altro genere, di esplorazioni, di
eroiche scorribande, di ardimentose incursioni, di congetture. In una parola:
non si è fatta alcuna ingiustizia ai personoidi conferendo loro il possesso di
un cosmo fatto proprio così e non altrimenti. Non è qui che si trovano la
crudeltà e l’immoralità della personetica.

Nel settimo capitolo di Non serviam, Dobb presenta al lettore gli abitanti
dell’universum digitale. I personoidi sono dotati di scioltezza di pensiero
oltre al linguaggio, e posseggono anche emozioni. Ciascuno di essi è un’entità
individuale; la loro differenziazione non è una pura conseguenza delle decisioni
del programmatore-creatore, ma deriva dalla complessità straordinaria della loro
struttura interna. Possono essere somigliantissimi tra loro, ma non sono mai
identici. Venendo al mondo, ciascuno è dotato di un “nocciolo”, di un “nucleo
personale”, e possiede già le facoltà di parola e di pensiero, per quanto a uno
stadio rudimentale. Hanno un vocabolario, che è tuttavia assai scarno, e hanno
la capacità di costruire frasi in conformità con le regole della sintassi che è
stata a loro imposta. In futuro, a quanto sembra, sarà possibile fare a meno di
imporre loro anche queste determinanti: non dovremmo far altro che aspettare che
essi, come un gruppo umano primitivo nel corso della socializzazione, sviluppino
da soli il loro linguaggio. Ma a questa linea di sviluppo della personetica si
oppongono due ostacoli cardinali. In primo luogo, la creazione di un linguaggio
richiederebbe un tempo lunghissimo. Allo stato attuale ci vorrebbero dodici
anni, anche spingendo al massimo la velocità delle trasformazioni all’interno
del calcolatore (in termini figurati e molto approssimativi, un secondo di tempo
macchina corrisponde a un anno di vita umana). In secondo luogo, e questo è il
problema più serio, un linguaggio che nascesse spontaneamente nel corso
dell’“evoluzione del gruppo dei personoidi” ci risulterebbe incomprensibile, e
decifrarlo sarebbe una necessità paragonabile all’ardua impresa di trovare la
chiave di un codice segreto, impresa tanto più difficile in quanto quel codice
non sarebbe stato creato da persone per altre persone in un mondo comune anche
ai decifratori. Il mondo dei personoidi è di qualità enormemente diversa dal
nostro e quindi un linguaggio adatto a esso sarebbe di necessità lontanissimo da
qualsiasi lingua umana. Per il momento dunque l’evoluzione linguistica *ex
nihilo* è soltanto un sogno dei personetisti.

I personoidi, una volta “messe le radici del loro sviluppo”, si trovano di
fronte a un enigma che è fondamentale e per essi supremo: quello della loro
origine. Vale a dire, essi si pongono domande, che poi sono le stesse domande
che noi conosciamo dalla storia dell’uomo, dalla storia delle sue credenze
religiose, dalle sue indagini filosofiche e dalle sue creazioni mistiche: Da
dove siamo venuti? Perché siamo fatti così e non altrimenti? Perché il mondo che
percepiamo ha queste proprietà e nn altre del tutto diverse? Che significato
abbiamo noi per il mondo? Che significato ha essi per noi? Inevitabilmente il
corso di queste speculazioni li porta da ultimo alle questioni fondamentali
dell’ontologia, al problema se l’esistenza sia prodotta “in sé e da sé” o se
invece sia il prodotto di uno specifico atto creativo; cioè se, nascosto dietro
di essa, dotato di volontà e coscienza, capace di azioni finalizzate, padrone
della situazione, non ci possa essere un Creatore. È qui che si manifesta tutta
la crudeltà, tutta l’immoralità della personetica.

Ma prima di passare, nella seconda parte del libro, al resoconto di questi
sforzi intellettuali – del dibattersi di una condizione mentale divenuta preda
di tormenti legati a questi problemi – Dobb ci presenta in una serie di capitoli
consecutivi un ritratto del “personoide tipico”, la sua “anatomia, fisiologia e
psicologia”.

Un personoide isolato non è in grado di andare più in là di uno stadio
rudimentale di pensiero, poiché, dato il suo isolamento, non può fare uso della
parola, e senza la parola il pensiero discorsivo non può svilupparsi. Come hanno
dimostrato centinaia di esperimenti, l’optimum si ha con gruppi formati da
quattro a un massimo di sette personoidi, almeno per quanto riguarda lo sviluppo
del linguaggio e l’attività esplorativa normale, e anche l’“acculturazione”.
D’altra parte fenomeni corrispondenti a processi sociali su scala più vasta
richiedono gruppi più numerosi. Attualmente in un universum di calcolatore di
ragionevole capacità è possibile “alloggiare” fino a un migliaio circa di
personoidi; ma studi di questo tipo, che competono a una disciplina separata e
indipendente, la sociodinamica, esulano dall’area dell’interesse primario di
Dobb e per questa ragione il suo libro li menziona solo di sfuggita. Come si è
detto, un personoide non ha corpo, ma ha un’“anima”. Quest’anima, a un
osservatore esterno che abbia accesso al mondo delle macchine (per mezzo di un
dispositivo particolare, un modulo ausiliario consistente in una specie di sonda
incorporata nel calcolatore), appare come una “nube coerente di processi”, come
un aggregato funzionale avente una specie di “centro” che può essere isolato con
una certa precisione, cioè delimitato entro i circuiti della macchina. (Ciò, si
noti, non è facile, e per più versi ricorda il tentativo dei neurofisiologi di
individuare i centri localizzati di molte funzioni del cervello umano).
Essenziale per la comprensione di che cosa renda possibile la creazione dei
personoidi è il capitolo XI di *Non serviam*, che spiega in termini piuttosto
semplici i fondamenti della teoria della coscienza. La coscienza – tutta la
coscienza, non semplicemente quella dei personoidi – è, quanto all’aspetto
fisico, un’“onda stazionaria informazionale”, un certo invariante dinamico in un
flusso di trasformazioni incessanti, la cui peculiarità sta nel fatto che essa
rappresenta un “compromesso” e nello stesso tempo un “risultante” che, per
quanto ci consta, non era affatto previsto dall’evoluzione naturale. È vero anzi
il contrario; fin dall’inizio l’evoluzione pose difficoltà e problemi tremendi
sulla strada dell’armonizzazione del funzionamento dei cervelli al di sopra di
una certa grandezza – cioè di un certo livello di complicazione – ed è chiaro
che essa sconfinò nel territorio di questi dilemmi senza volerlo, poiché
l’evoluzione si “trascinò dietro” certe soluzioni evolutive antichissime date a
problemi di controllo e regolazione comuni al sistema nervoso fino al livello in
cui ebbe inizio l’antropogenesi. Sotto il profilo puramente razionale
dell’efficienza ingegneristica, tali soluzione avrebbero dovuto essere soppresse
o abbandonate, per passare alla progettazione di qualcosa d’interamente nuovo:
vale a dire il cervello di un essere intelligente. Ma è chiaro che l’evoluzione
non poteva procedere in questo modo, poiché non rientrava nei suoi poteri
sbarazzarsi del retaggio delle vecchie soluzioni, spesso antiche di centinaia di
milioni di anni. Poiché essa avanza sempre per microscopici incrementi di
adattamento, poiché “striscia” e non può “saltare”, l’evoluzione è una rete a
strascico “che trascina con sé innumerevoli arcaismi e rifiuti di ogni genere”,
per dirla con l’immagine sbrigativa di Tammer e Bovine. (Tammer e Bovine sono
due degli ideatori della simulazione al calcolatore della psiche umana, che
gettò le basi per la nascita della personetica). La coscienza dell’uomo è il
risultato di un compromesso di genere particolare. È uno “zibaldone”, ovvero,
come osservò per esempio Gebhardt, una perfetta esemplificazione del ben noto
adagio tedesco *“Aus einer Not eine Tugend machen”* (cioè “fare di necessità
virtù”). Una macchina digitale non potrà mai acquisire coscienza da sola, per
la semplice ragione che in essa non sorgono conflitti gerarchici di
funzionamento. Una tale macchine può, al massimo, cadere in una sorta di
“paralisi logica” o di “stupore logico” quando in essa le antinomie si
moltiplichino. Le contraddizioni di cui pullula letteralmente il cervello
dell’uomo furono invece sottoposte, nel corso di centinaia di migliaia di anni,
a graduali procedimenti di arbitrato. Si costituirono livelli più alti e livelli
più bassi, livelli di riflessi e di riflessione, d’impulso e di controllo,
furono costruiti modelli dell’ambiente elementale con mezzi zoologici e modelli
dell’ambiente concettuale con mezzi linguistici. Questi vari livelli non sono in
grado e non “vogliono” attagliarsi perfettamente l’uno all’altro o fondersi per
formare un tutto unico.

Che cos’è, allora la coscienza? Un espediente, un sotterfugio, una scappatoia,
una presunta ultima risorsa, una pretesa (ma solo pretesa) corte d’appello
insindacabile. E, nel linguaggio della fisica e della teoria dell’informazione,
è una funzione che, una volta iniziata, non ammette alcuna chiusura, cioè alcun
completamento definitivo. È dunque solo un *progetto* di tale chiusura, di tale
“conciliazione” totale delle tenaci contraddizioni del cervello. È, si potrebbe
dire, uno specchio il cui compito è quello di riflettere altri specchi, che al
loro volta ne riflettono altri ancora, e così via all’infinito. Dal punto di
vista fisico, ciò semplicemente non è possibile, e quindi il *regressus ad
infinitum* rappresenta una sorta di pozzo su cui si libra e volteggia il
fenomeno della coscienza umana. “Sotto il conscio” si svolge di continuo una
battaglia per una piena rappresentazione – in esso – di ciò che non può
raggiungerlo con pienezza; e non può raggiungerlo per pura e semplice mancanza
di spazio; perché per concedere pieno e uguale diritto a tutte quelle tendenze
che rumoreggiano per attirare l’attenzione dei centri della consapevolezza
sarebbero necessari una capacità e un volume infiniti. Regnano quindi intorno al
conscio una ressa e un pigia pigia incessanti, e il conscio non è, non è
affatto, il supremo, imperturbabile e sovrano timoniere di tutti i fenomeni
mentali, ma piuttosto un sughero galleggiante sulle onde agitate, un sughero la
cui posizione elevata non significa il dominio su quelle onde… La teoria moderna
della coscienza, interpretata sotto il profilo informazionale e dinamico, non
può sfortunatamente essere espressa in modo semplice e chiaro, sicché dobbiamo
costantemente ripiegare – almeno qui, in questa presentazione più accessibile
sull’argomento – su una serie di modelli e di metafore visivi. Sappiamo, in ogni
caso, che la coscienza è una specie di sotterfugio, uno stratagemma cui ha fatto
ricorso l’evoluzione, e vi ha fatto ricorso nello spirito del suo caratteristico
e indispensabile modus operandi, l’opportunismo, trovando cioè una scappatoia
rapida e improvvisata quando si è scoperta con le spalle al muro. Se dunque si
volesse costruire un essere intelligente procedendo secondo i canoni di
un’ingegneria e di una tecnica, l’essere così costruito, in generale, non
avrebbe il dono della coscienza. Si comporterebbe in modo perfettamente logico,
sempre coerente, lucido e ben ordinato e potrebbe addirittura sembrare, a un
osservatore umano, un genio dell’azione creativa e della decisione. Ma non
potrebbe in alcun modo essere un uomo, poiché sarebbe privo della sua misteriosa
profondità, delle sue complessità interne, della sua natura labirintica…

Non ci vogliamo qui addentrare ulteriormente nella teoria moderna della psiche
cosciente, e del resto non lo fa neppure il professor Dobb. Ma queste poche
parole erano necessarie, poiché esse forniscono la necessaria introduzione alla
struttura dei personoidi. Con la loro creazione si è finalmente tradotto in
realtà uno dei miti più antichi, quello dell’homunculus. Per foggiare un
simulacro dell’uomo, della sua psiche, si devono introdurre a bella posta nel
substrato informazionale delle contraddizioni specifiche; gli si deve impartire
un’asimmetria, una serie di tendenze acentriche; in una parola si deve
*unificare* e insieme rendere *discorde*. È razionale tutto ciò? Sì, e anche
pressoché inevitabile, se desideriamo non semplicemente costruire una qualche
sorta d’intelligenza sintetica, bensì imitare il pensiero e, con esso, la
personalità dell’uomo.

Le emozioni dei personoidi, quindi, debbono in qualche misura essere in
conflitto con la loro ragione; essi devono possedere tendenze autodistruttive,
almeno fino a un certo punto; devono sentire tensioni interne, quella totale
spinta centrifuga che noi sperimentiamo ora come una splendida infinità di stati
spirituali, ora come uno smembramento insopportabilmente doloroso. La ricetta
per creare tutto ciò, intanto, non ha affatto la complessità sconfortante che si
potrebbe credere. Si tratta semplicemente di questo: la *logica* della creazione
(del personoide) deve essere disturbata, deve contenere certe antinomie. La
coscienza non è solo un modo per uscire dal vicolo cieco dell’evoluzione, dice
Hilbrandt, ma anche una via di fuga dalle trappole della gödelizzazione, poiché,
grazie alle contraddizioni paralogistiche, questa soluzione ha eluso le
contraddizioni di cui è soggetto qualunque sistema che sia perfetto sotto il
profilo logico. Così dunque l’universum dei personoidi è perfettamente
razionale, ma essi non ne sono abitatori pienamente razionali. Questo ci basti:
neppure il professor Dobb indaga oltre questo difficilissimo argomento. Come già
sappiamo, i personoidi hanno un’anima ma non un corpo, e perciò non hanno
neppure alcuna sensazione della loro corporeità. “È difficile immaginare” è
stato detto di ciò che si sperimenta in certi stati particolari della mente,
nell’oscurità totale, riducendo al massimo il flusso degli stimoli esterni; ma,
sostiene Dobb, questa è un’immagine fuorviante, poiché senza l’apporto dei sensi
il cervello umano comincia ben presto a disintegrarsi; senza una corrente di
impulsi dal mondo esterno, la psiche manifesta una tendenza alla lisi. Ma i
personoidi, che pure non hanno sensi fisici, certo non si disintegrano, poiché
ciò che conferisce loro coesione è l’ambiente matematico che essi di fatto
sperimentano. Ma come? Essi lo sperimentano, diciamo, secondo quei cambiamenti
del loro stato che sono provocati e imposti loro dalla “esternità”
dell’universum. Essi sono in grado di discriminare tra i cambiamenti provenienti
dal di fuori e i cambiamenti che affiorano dalle profondità della loro psiche.
Come operano questa discriminazione? Solo la teoria della struttura dinamica dei
personoidi può fornire una risposta diretta a questa domanda.

Eppure, a dispetto di tutte queste impressionanti differenze, essi sono come
noi. Sappiamo già che una macchina digitale non potrà mai avere la scintilla
della coscienza; qualunque sia il lavoro per cui la sfruttiamo o il processo
fisico che vi simuliamo, essa resterà sempre apsichica. Poiché per simulare
l’uomo è necessario riprodurre alcune delle sue contraddizioni fondamentali,
soltanto un sistema di antagonismi gravitanti l’uno sull’altro – un personoide –
somiglierà, per usare le parole di Canyon citate da Dobb, a una “stella
contratta dalla forza di gravità e allo stesso tempo espansa dalla pressione
della radiazione”. Il centro di attrazione gravitazionale è, molto
semplicemente, l’“io” personale, che però non costituisce affatto un’unità né in
senso logico né in senso fisico. Questa è solo una nostra illusione soggettiva!
A questo punto dell’esposizione, ci troviamo in mezzo a uno stuolo di sorprese
stupefacenti. È certamente possibile programmare una macchina digitale in modo
da poter conversare con essa come con un interlocutore intelligente. La
macchina, ove ne insorga il bisogno, impiegherà il pronome “io” e tutte le
corrispondenti forme grammaticali. Ma ciò è pura mistificazione. La macchina
sarà sempre più prossima a un miliardo di pappagalli chiacchieroni – per quanto
meravigliosamente addestrati – che all’uomo più semplice e più stupido. Essa
scimmiotta il comportamento dell’uomo sul piano puramente linguistico e niente
più. Nulla potrà divertire o sorprendere o confondere o allarmare o affliggere
questa macchina, poiché sotto il profilo psicologico e individuale essa non è
Qualcuno. È una Voce che espone argomenti, che fornisce risposte alle domande; è
una Logica capace di sconfiggere lo scacchista più bravo; è, o meglio può
diventare, un abilissimo imitatore di qualunque cosa, un attore, se si vuole,
spinto al culmine della perfezione, capace di sostenere qualunque parte
programmata; ma un attore e un imitatore che, dentro, è completamente vuoto. non
si può contare sulla sua cordialità, né sulla sua antipatia. Essa non si adopera
per raggiungere uno scopo prefissosi. Con un’assolutezza che nessun uomo
potrebbe mai neanche lontanamente concepire, “è indifferente”, poiché come
persona semplicemente non esiste… È un meccanismo combinatorio
straordinariamente efficiente, e null’altro. Ebbene, ci troviamo di fronte a un
fenomeno notevolissimo. È stupefacente pensare che dal materiale grezzo di una
macchina così vacua e così impersonale sia possibile, introducendo in essa un
programma speciale, un programma personetico, creare esseri senzienti autentici
e, anzi, crearne un gran numero tutti in una volta! Gli ultimi modelli IBM hanno
una capacità massima di mille personoidi. (Questo numero è dato con precisione
matematica, poiché gli elementi e i collegamenti necessari per produrre un
personoide si possono esprimere in unità del sistema CGS).

All’interno della macchina i personoidi sono separati l’uno dall’altro. Di
solito essi non si “sovrappongono”, benché ciò possa accadere. Al contrario
avviene qualcosa di equivalente alla repulsione, il che impedisce l’“osmosi”
reciproca. Ciò nonostante, se lo desiderano, sono in grado di compenetrarsi. In
tal caso i processi che costituiscono il loro substrato mentale cominciano a
sovrapporsi l’uno all’altro, producendo “rumore” e interferenze. Quando l’area
di permeazione è esigua, una certa quantità d’informazione diventa proprietà
comune dei due personoidi in parziale coincidenza; si tratta di un fenomeno per
loro assai singolare, come per un uomo sarebbe singolare, se non addirittura
allarmante, udire nella propria testa “strane voci” e “pensieri estranei” (come
effettivamente avviene nel caso di certe malattie mentali o per effetto di
sostanze allucinogene). È come se due persone avessero non semplicemente gli
stessi ricordi, ma i *medesimi* ricordi; come se fosse avvenuto qualcosa di più
che un trasferimento telepatico di pensiero; come se ci fosse stata una “fusione
periferica degli io”. Il fenomeno, tuttavia, ha conseguenze spesso infauste ed è
da evitarsi. infatti, dopo uno stadio transitorio di osmosi superficiale, il
personoide “avanzante” può distruggere e consumare l’altro. In tal caso
quest’ultimo subisce un vero e proprio assorbimento, un annientamento: cessa di
esistere (a questo proposito si è parlato di assassinio). Il personoide
annientato diventa una parte assimilata e indistinguibile dell’“aggressore”.
Siamo riusciti – dice Dobb – a simulare non soltanto la vita psichica, ma anche
i suoi rischi e la sua obliterazione. Siamo dunque riusciti a simulare anche la
morte. In condizioni sperimentali normali, tuttavia, i personoidi evitano questi
atti di aggressione; è rarissimo incontrare tra loro gli “psicofagi” (il termine
è di Castler). Quando sentono gli inizi di un’osmosi, che può insorgere come
risultato di accostamenti e di fluttuazioni del tutto accidentali – ed essi
sentono questa minaccia in un modo che naturalmente non è fisico, ma più o meno
come un uomo potrebbe avvertire la presenza di un altro o magari udire nella
propria mente “voci strane” – i personoidi si affrettano a eseguire manovre per
evitarla; si ritraggono e vanno ciascuno per la sua strada. È sulla base di
questo fenomeno che essi sono giunti a conoscere il significato dei concetti di
“bene” e di “male”. Per loro è evidente che il “male” risiede nella distruzione
di un loro simile e il “bene” nella sua liberazione. Allo stesso tempo, il
“male” dell’uno può essere il “bene” (cioè il vantaggio, ora non più in senso
etico) dell’altro, il quale diventerebbe così uno “psicofago”. Infatti questa
espansione – l’appropriarsi del “territorio intellettuale” di un altro –
accresce l’“area” mentale ricevuta all’inizio. In un certo senso, si tratta di
un corrispettivo di una delle nostre pratiche, poiché in quanto carnivori noi
uccidiamo le nostre vittime e ce ne cibiamo. I personoidi, però, non sono
obbligati a comportarsi in questo modo; hanno semplicemente la facoltà di farlo.
Essi non conoscono né fame né sete, poiché sono sostenuti dal flusso continuo di
energia – un’energia della cui fonte essi non debbono preoccuparsi (proprio come
noi non dobbiamo fare nulla di particolare affinché il sole ci mandi i suoi
raggi). Nel mondo dei personoidi non possono essere applicati all’energia i
termini e i princìpi della termodinamica, poiché quel mondo è soggetto a leggi
matematiche e non termodinamiche.

Non passò molto tempo prima che gli sperimentatori giunsero alla conclusione che
i contatti tra i personoidi e gli uomini effettuati attraverso gli ingressi e le
uscite del calcolatore non solo erano di scarso valore scientifico, ma in più
generavano certi dilemmi morali i quali contribuirono a far sì che la
personetica venisse qualificata come la più crudele delle scienze. Vi è qualcosa
di meschino nell’informare i personoidi del fatto che li abbiamo creati in
universi chiusi che *simulano* soltanto l’infinito, che essi sono microscopiche
“psicocisti” o capsule in seno al nostro mondo. Certo, essi posseggono una loro
infinità; perciò Sharker e altri psiconetici (Falk, Wiegeland) sostengono che la
situazione è del tutto simmetrica: i personoidi non hanno bisogno del nostro
mondo, del nostro “spazio vitale”, proprio come noi non sappiamo che farcene
della loro “terra matematica”. Per Dobb questi ragionamenti sono sofistici,
poiché non può esservi dubbio su quali siano i creatori e quali le creature e su
chi sia stato confinato esistenzialmente. Dobb appartiene a quel gruppo che
sostiene il principio del non intervento assoluto – il “non contatto” – nei
confronti dei personoidi. Si tratta dei comportamentisti della personetica: il
loro desiderio è quello di osservare esseri intelligenti sintetici, di spiare i
loro discorsi e i loro pensieri, di registrare le loro azioni e occupazioni,
senza tuttavia mai interferire con essi. Questo metodo è già sviluppato e
possiede una propria tecnica basata su un insieme di strumenti la cui messa a
punto ha presentato difficoltà che ancora pochi anni fa parevano quasi
insormontabili. L’idea è di ascoltare, di comprendere – in breve, di origliare
incessantemente – impedendo però nel contempo che questo “ascolto” disturbi in
qualsiasi modo il mondo dei personoidi. Esistono presso il MIT programmi ancora
allo stadio di progetto (l’APHERON II e l’EROT), che permettevano ai personoidi,
i quali per il momento non hanno sesso, di avere “contatti erotici”, rendendo
possibile ciò che corrisponde alla fecondazione e dando loro modo di
moltiplicarsi “per via sessuale”. Dobb dichiara apertamente di non essere
entusiasta dei progetti americani. In suo lavoro, così com’è descritto in Non
serviam, punta in una direzione completamente diversa: non per nulla la scuola
inglese di personetica è stata chiamata “poligono filosofico” e “laboratorio di
teodicea”. Con queste descrizioni giungiamo a quella che è probabilmente la
parte più significativa, e certamente la più affascinante del libro in oggetto:
l’ultima parte, che ne giustifica e ne spiega lo strano titolo.

Dobb fornisce un resoconto del suo esperimento, che è in corso senza
interruzioni da otto anni. Alla creazione egli accenna solo di sfuggita; si
tratta di una duplicazione abbastanza ordinaria di funzioni tipica del programma
JAHVE VI, con lievi modifiche. Egli riassume i risultato ottenuti
“intercettando” questo mondo che egli stesso ha creato e i cui sviluppi egli
continua a seguire. Dobb considera questa pratica dell’intercettazione come
immorale e, a volte, addirittura vergognosa. Tuttavia egli continua il proprio
lavoro, professando di credere nella necessità, per la scienza, di compiere
*anche* esperimenti siffatti, esperimenti che non possono essere in alcun modo
giustificati su basi morali né in verità su nessun’altra base che non sia il
progresso della conoscenza. La situazione, egli dice, è arrivata a un punto tale
per cui gli scienziati non possono più mantenere i loro vecchi atteggiamenti
ambigui. Non si può affettare un’elegante neutralità e tacitare i rimorsi di
coscienza ricorrendo ad esempio al ragionamento inventato dai vivisezionisti,
cioè che quelli ai quali si causa sofferenza o anche solo fastidio non sono
creature pienamente coscienti, esseri sovrani. Negli esperimenti sui personoidi
siamo doppiamente responsabili, poiché prima creiamo e poi incateniamo ciò che
abbiamo creato al disegno dei nostri procedimenti di laboratorio. Qualunque cosa
facciamo e comunque spieghiamo il nostro agire, non possiamo più sottrarci alla
nostra responsabilità.

I molti anni di esperienza da parte di Dobb e dei suoi collaboratori a Oldport
portarono alla costruzione del loro universum a otto dimensioni, che divenne
dimora di personoidi chiamati ADAN, ADNA, ANAD, DANA, DAAN e NAAD. I primi
personoidi svilupparono i rudimenti di linguaggio ricevuti ed ebbero una
“progenie” per scissione. Dobb, adottando un linguaggio biblico, scrive: “E ADAN
generò ADNA. ADNA a sua volta generò DAAN e DAAN diede i natali a EDAN, che
partorì EDNA…”. Le cose continuarono in questo modo finché si giunge alla
trecentesima generazione; poiché, tuttavia, la capacità del calcolatore era
sufficiente solo per cento unità di personoidi, vi furono eliminazioni
periodiche del “surplus demografico”. Alla trecentesima generazione fecero di
nuovo la loro comparsa personoidi di nome ADAN, ADNA, ANAD, DANA, DAAN e NAAD,
con numeri suppletivi per indicare il loro ordine nella discendenza. (Per
semplicità nella nostra ricapitolazione ometteremo questi numeri). Dobb scrive
che il tempo trascorso nell’universum contenuto nel calcolatore corrisponde –
facendo una trasformazione approssimata alle nostre unità di misura equivalenti
– a duemila-duemilacinquecento anni. In questo periodo, nella popolazione dei
personoidi si sono sviluppate svariatissime spiegazioni del loro destino; essi
hanno anche formulato vari modelli, contrastanti e mutuamente esclusivi, di
“tutto ciò che esiste”. Sono cioè nate molte filosofie (ontologie e gnoseologie)
diverse e anche “esperimenti metafisici” di un genere tutto particolare. Non
sappiamo se sia perché la “cultura” dei personoidi è troppo dissimile da quella
umana o perché l’esperimento è stato finora troppo breve, fatto sta che nella
popolazione studiata non si è mai cristallizzata una fede completamente
dogmatizzata: una fede paragonabile diciamo, al Buddhismo o al Cristianesimo.
Peraltro già all’ottava generazione si nota la comparsa della nozione di un
Creatore concepito come essere personale in una prospettiva monoteistica.

L’esperimento consiste nell’aumentare al massimo la velocità delle
trasformazioni operate dal calcolatore e poi (più o meno una volta all’anno)
nell’abbassarla per rendere possibile l’osservazione diretta. Questi cambiamenti
di velocità, come ci spiega Dobb, non vengono affatto percepiti dagli abitanti
dell’universum in seno al calcolatore, così come le variazioni del genere non
sarebbero percepite da noi, poiché quando tutta l’esistenza subisce un
cambiamento repentino (in questo caso, nella dimensione temporale) coloro che vi
sono immersi non possono rendersene conto, dal momento che non hanno un punto
fisso, un sistema di riferimento, rispetto al quale accorgersi che esso sta
avvenendo.

L’impiego di “due marce cronologiche” permise di ottenere ciò che Dobb più
desiderava, cioè l’emergere di una storia personoide, una storia dotata di
profondità di tradizioni e di prospettiva nel tempo futuro. Riassumere tutti i
dati di questa storia registrati da Dobb, che sono spesso di natura
sensazionale, non ci è possibile. Ci limiteremo perciò ai passi dai quali
scaturì l’idea che è rispecchiata nel titolo del libro. Il linguaggio impiegato
dai personoidi è una trasformazione recente dell’inglese standard, il cui
lessico e la cui sintassi erano stati incorporati nel programma della prima
generazione. Dobb lo traduce in un inglese sostanzialmente normale, ma lascia
intatte alcune espressioni coniate dal popolo dei personoidi. Tra queste
figurano i termini “con-dio” e “senza-dio”, impiegati per indicare i credenti in
Dio e gli atei.

ADAN discute con DAAN e ADNA (i personoidi in realtà non usano questi nomi, che
sono un puro e semplice espediente pratico cui ricorrono gli osservatori per
poter registrare più facilmente i “dialoghi” ) a proposito di un problema che si
sono posti anche gli uomini, un problema che nella nostra storia ebbe origine
con Pascal, ma che nella storia dei personoidi fu scoperto da un certo EDAN 197.
Proprio come Pascal, questo pensatore asserì che credere in Dio è in ogni caso
più vantaggioso che non crederci, poiché se la verità sta dalla parte dei
“senza-dio” il credente, lasciando il mondo, perde solo la vita, mentre de Dio
esiste egli si guadagna l’eternità (la gloria perenne). Pertanto è consigliabile
credere in Dio, poiché ciò è in fin dei conti in linea con la tattica
esistenziale di pesare i pro e i contro nella ricerca dell’esito migliore.

A proposito di questa impostazione ADAN 300 è della seguente opinione: EDAN 197,
nella sua argomentazione, ipotizza un Dio che esige ossequio, amore e devozione
totale e non solo e semplicemente la convinzione che Egli esista e che abbia
creato il mondo. Per guadagnarsi la salvezza non basta consentire all’ipotesi
Dio Fattore del Mondo; si deve anche essere grati a quel Fattore per il suo atto
creativo, divinare la Sua volontà e seguirla. In breve si deve servire Dio. Ora,
Dio, se esiste, ha il potere di dimostrare la Sua esistenza in un modo almeno
tanto convincente quanto lo è il modo in cui testimonia della Sua esistenza ciò
che può essere percepito direttamente. È evidente che non si può dubitare che
certi oggetti esistano e che il nostro mondo si componga di essi. Tutt’al più si
potranno nutrire dubbi su che cosa essi facciano per esistere, su come essi
esistano, ecc. Ma il fatto in sé della loro esistenza non può essere negato. Dio
poteva fornire le prove della Sua esistenza con questa stessa forza. Eppure Egli
non l’ha fatto, condannandoci a conseguire, a questo riguardo, una conoscenza
che è obliqua, indiretta, espressa sotto forma di varie congetture, alle quali
viene dato talvolta il nome di rivelazione. Se ha agito così, Egli ha con ciò
posto i “con-dio” e i “senza-dio” sullo stesso piano; non ha obbligato le Sue
creature a una fede assoluta nella Sua esistenza, ma ha soltanto offerto loro
questa possibilità. Certo, i motivi che hanno mosso il Creatore possono essere
celati alle Sue creature. Comunque sia, vale la seguente proposizione: o Dio
esiste o Dio non esiste. Che vi sia una terza possibilità (Dio è esistito ma non
esiste più, oppure esiste a intermittenza, in modo oscillante, o esiste a volte
“di meno” e a volte “di più”, ecc.) appare improbabilissimo. Questa possibilità
non può essere esclusa in modo assoluto, ma l’introduzione di una logica
polivalente in una teodicea serve solo a ingarbugliarla.

Così dunque o Dio c’è o non c’è. E se Dio stesso accetta la nostra situazione,
in cui ciascun corno del dilemma in oggetto ha argomenti a proprio sostegno –
poiché i “con-dio” dimostrano l’esistenza del Cratore e i “senza-dio” la
confutano – allora, dal punto di vista della logica, si ha a che fare con un
gioco i cui partecipanti sono, da una parte, l’insieme completo dei “con-dio” e
del “senza-dio” e, dall’altra, Dio da solo. Il gioco possiede di necessità il
connotato logico che Dio non può punire qualcuno solo perché questi non crede in
Lui. Se si ignora in modo assoluto se una certa cosa esista oppure no, per cui
semplicemente alcuni asseriscono che esiste e altri che non esiste, e se in
generale è possibile avanzare l’ipotesi che la cosa non sia mai esistita, allora
nessun tribunale equo potrà condannare qualcuno che neghi l’esistenza di questa
cosa. Poiché in tutti i mondi vale questo principio: quando non c’è certezza
piena non c’è piena responsabilità. Sotto il profilo puramente logico questa
formulazione è inattaccabile, poiché stabilisce una funzione di rimunerazione
simmetrica nel contesto della teoria dei giochi; chiunque, di fronte
all’incertezza, esiga piena responsabilità distrugge la simmetria matematica del
gioco; si ha allora a che fare con i cosiddetti giochi a somma non zero.

Pertanto le cose stanno così: o Dio è perfettamente giusto, nel qual caso Egli
non può arrogarsi il diritto di punire i “senza-dio” a causa del fatto che sono
“senza-dio” (cioè che non credono in Lui); oppure Egli punisce effettivamente i
non credenti, il che significa che dal punto di vista logico Egli non è
perfettamente giusto. Che cosa ne consegue? Ne consegue che Egli può agire come
gli pare, poiché quando in un sistema logico si permette anche solo una
solitaria contraddizione, allora, per il principio ex falso quodilbet, da quel
sistema si può dedurre qualunque conclusione si voglia. In altre parole: un Dio
giusto non può torcere un capello ai “senza-dio” e, se lo fa, allora per
quell’atto stesso Egli non è quell’essere universalmente perfetto e giusto
postulato dalla teodicea.

ADNA chiede come si debba considerare, alla luce di questa impostazione, il
problema del fare del male agli altri.

ADAN 300 risponde: Tutto ciò che accade qui è assolutamente certo; tutto ciò che
accade “là” – cioè oltre i confini del mondo, nell’eternità, presso Dio – è
incerto, poiché viene solo inferito sulla base delle ipotesi. Qui non si
dovrebbe commettere il male, anche se il principio di evitare il male non è
dimostrabile logicamente. Ma allo stesso titolo neppure l’esistenza del mondo
può essere dimostrata logicamente. Il mondo esiste, anche se potrebbe non
esistere. Il male può essere commesso, anche se sarebbe bene non commetterlo e
questo, credo, a causa del nostro accordo basato sulla regola di reciprocità:
comportati con me come io mi comporto con te. Ciò non ha nulla a che vedere con
l’esistenza o la non esistenza di Dio. Se dovessi astenermi dal commettere il
male nel timore che, a causa di ciò, “là” sarei punito, oppure se dovessi
compiere il bene contando su una ricompensa “là”, fonderei la mia condotta su
basi incerte. Qui, invece, non può esservi base più solida del nostro reciproco
accordo in questa faccenda. Se “là” vi sono altre basi, io non ho di esse
conoscenza altrettanto precisa di quella che ho, qui, delle nostre. Vivendo
giochiamo il gioco della vita, e in esso siamo tutti alleati. Quindi la partita
fra noi è perfettamente simmetrica. Col postulare Dio, postuliamo che la partita
abbia un prolungamento oltre il mondo. Io credo che sia lecito postulare questo
prolungamento, purché esso non influenzi in alcun modo lo svolgimento della
partita qui. Altrimenti, in nome di qualcuno che forse non esiste potremmo
sacrificare ciò che esiste qui ed esiste di sicuro.

NAAD osserva che l’atteggiamento di ADAN 300 verso Dio non gli è chiaro. ADAN ha
ammesso, la possibilità che il creatore esista: che cosa segue da ciò?

ADAN: Nulla di nulla. Cioè nulla che abbia carattere di obbligo. Io credo che
valga – e anche questo in tutti i mondo – il principio seguente: un’etica
temporale è sempre indipendente da un’etica trascendente. Ciò significa che
un’etica dell’*hic et nunc* non può avere fuori di sé alcuna sanzione che possa
sostanziarla. E ciò significa che chi commette il male è in ogni caso un
farabutto, così come chi fa il bene è in ogni caso un virtuoso. Se qualcuno è
disposto a servire Dio giudicando sufficienti gli argomenti a favore della Sua
esistenza, costui non acquista con ciò alcun merito addizionale qui: è affar
suo. Questo principio si basa sull’assunto che se Dio non esiste, allora Egli
non esiste neanche un po’, e se esiste, è onnipotente. Essendo onnipotente, Egli
potrebbe allora creare non soltanto un altro mondo, ma del pari anche un’altra
logica diversa da quella su cui si fonda il mio ragionamento. All’interno di
questa logica, l’ipotesi di un’etica temporale potrebbe essere necessariamente
dipendente da un’etica trascendente. In tal caso le prove logiche, se non quelle
palpabili, avrebbero una forza cogente e ci obbligherebbero ad accettare
l’ipotesi di Dio per tema di peccare contro la ragione.

NAAD dice che Dio non desidera una situazione che a tal punto obblighi a credere
in Lui, situazione che si presenterebbe in un creato basato sull’altra logica
postulata da ADAN 300. A ciò questi risponde:

Un Dio onnipotente deve essere anche onnisciente; il potere assoluto non è
qualcosa d’indipendente dalla conoscenza assoluta, poiché colui che può fare
tutto ma non sa quali conseguenze seguiranno dall’esercizio della propria
onnipotenza non è più, ipso facto, onnipotente; se Dio facesse qualche miracolo
ogni tanto, come corre voce che faccia, Egli porrebbe la Sua perfezione in una
luce quanto mai dubbia, poiché un miracolo è una violazione dell’autonomia della
Sua creazione, un intervento violento. Ma colui che ha regolato il prodotto
della propria creazione e ne conosce il comportamento dall’inizio alla fine non
ha alcun bisogno di violare quest’autonomia; se nonostante ciò la viola, pur
restando onnisciente, ciò significa che egli così facendo non corregge affatto
la sua opera (una correzione, dopotutto, può significare soltanto una
non-onniscenza iniziale), ma fornisce invece – col miracolo – un segno della
propria esistenza. Orbene, questa è logica fallace, poiché la produzione di un
segno siffatto deve necessariamente dare l’impressione che il creato venga ciò
nondimeno corretto nei suoi incespicamenti locali. Infatti un’analisi logica del
nuovo modello fornisce quanto segue: la creazione subisce correzioni che non
procedono da essa ma provengono dall’esterno (dal trascendente, da Dio), e
quindi i miracoli dovrebbero in realtà diventare la norma; ovvero, in altre
parole, la creazione dovrebbe essere corretta e perfezionata in modo tale che i
miracoli alla fine non risulterebbero più necessari. Infatti i miracoli, in
quanto interventi specifici, non possono essere soltanto segni dell’esistenza di
Dio: essi dopo tutto, oltre a rivelare il loro Autore, indicano sempre un
destinatario (essendo indirizzati a qualcuno *qui* per aiutarlo). Così dunque,
rispetto alla logica, le cose devono stare in questo modo: o la creazione è
perfetta, nel qual caso i miracoli non sono necessari, oppure i miracoli sono
necessari, nel qual caso la creazione non è perfetta. (Con o senza miracoli, si
può correggere solo ciò che ha qualche difetto, poiché un miracolo che
interferisca con ciò che è perfetto lo disturberà soltanto, o anzi lo
peggiorerà). Pertanto segnalare la propria presenza col miracolo equivale ad
usare un metodo per manifestarsi che è il peggiore possibile dal punto di vista
logico.

NAAD chiede se Dio in realtà non voglia che vi sia una dicotomia fra la logica e
il credere in Lui: forse l’atto di fede dovrebbe essere proprio un abbandono
della logica a favore di una fiducia totale.

ADAN: Una volta che si permette che la ricostruzione logica di qualcosa (un
essere, una teodicea, una teogonia e simili) contenga autocontraddizioni
interne, diventa chiaramente possibile dimostrare qualunque cosa ci piaccia.
Consideriamo i termini della cosa: si crea qualcuno e lo si dota di una certa
logica, e poi si pretende che questa stessa logica sia offerta in sacrificio per
poter credere nel Fattore di tutte le cose. Se questo modello deve a sua volta
restare esente da contraddizioni, esso richiede l’applicazione, sotto forma di
metalogica, di un tipo di ragionamento affatto diverso da quello che è naturale
per la logica di colui che è stato creato. Se tutto ciò non rivela la netta
imperfezione del Creatore, rivela purtuttavia una qualità che chiamerei
ineleganza matematica: una carenza di metodicità (un’incoerenza) *sui generis*
dell’atto creativo.

NAAD insiste: Forse Dio agisce così proprio perché desidera restare
imperscrutabile al Suo creato, cioè non ricostruibile mediante la logica che
Egli gli ha fornito. In breve, Egli esige il primato della fede sulla logica.

ADAN gli risponde: Capisco che cosa vuoi dire. Ciò naturalmente è possibile, ma
anche se così fosse, una fede che si dimostri incompatibile con la logica
presenta uno spiacevolissimo dilemma di natura morale. In tal caso infatti a un
certo punto dei nostri ragionamenti si rende necessario sospenderli per dar la
precedenza a una supposizione nebulosa; in altre parole, si rende necessario
situare la supposizione al di sopra della certezza logica; e si deve fare questo
in nome di una fiducia illimitata. Entriamo qui in un *circulus vitiosus*,
poiché l’esistenza postulata di ciò in cui è d’uopo porre ora la nostra fiducia
è il prodotto di una linea di ragionamento che, all’inizio, era *logicamente
corretta*; sorge così una contraddizione logica che, per certuni, assume un
valore positivo e viene detta il Mistero di Dio. Orbene, sotto il profilo della
pura costruzione questa soluzione è scadente e sotto il profilo morale è
discutibile, poiché il Mistero può essere fondato in modo soddisfacente
sull’infinito (l’infinitezza è, in fin dei conti, una caratteristica del nostro
mondo), ma il mantenimento e il rafforzamento di esso mediante il paradosso
interno è, secondo qualunque criterio architettonico, un procedimento disonesto.
I difensori della teodicea in genere non si rendono conto che è così perché a
certe parti della loro teodicea essi continuano ad applicare la logica ordinaria
e ad altre no. Voglio dire questo: che se uno crede nella contraddizione, allora
dovrebbe credere *solo* nella contraddizione e non allo stesso tempo anche in
qualche non-contraddizione (cioè nella logica) in altri campi. Se, tuttavia, si
insiste in questo curioso dualismo (cioè che il temporale è sempre soggetto alla
logica e il trascendente solo a tratti), allora il modello del Creato che così
si ottiene è qualcosa che, rispetto alla correttezza logica, è “rappezzato” e
non è più possibile postulare la sua perfezione. Si giunge senza scampo alla
conclusione che la perfezione è qualcosa di necessariamente rappezzato sotto il
profilo logico.

EDNA chiede se la congiunzione di queste incoerenze non possa essere l’amore.

ADAN: Anche se così fosse, non può essere qualsiasi forma d’amore bensì solo una
forma simile all’accecamento. Dio, se c’è, se ha creato il mondo, gli ha
permesso di governarsi come può e desidera. Per il fatto che Dio esiste non è
richiesta nessuna gratitudine verso di Lui; tale gratitudine poggia sul
precedente assunto che Dio è in grado di non esistere e che ciò sarebbe male,
premessa che porta a un altro genere ancora di contraddizione. E la gratitudine
per l’atto creativo? Neppure questa è dovuta a Dio. Infatti essa è basata sulla
coazione a credere che essere sia senz’altro meglio che non essere; e non riesco
a immaginare come ciò possa, a sua volta, essere dimostrato. A chi non esiste
non è certo possibile fare né un favore né un danno, e se Colui che crea, nella
Sua onniscienza, sa già in anticipo che colui che è creato Gli sarà grato e Lo
amerà oppure che non Gli sarà grato e Lo rinnegherà. Egli con ciò stabilisce un
vincolo, anche se esso non è accessibile alla comprensione diretta di colui che
è creato. Proprio per questo motivo, nulla è dovuto a Dio: né odio né amore, né
gratitudine né biasimo, né la speranza di una ricompensa né il timore di un
castigo. Niente Gli è dovuto. Un Dio bramoso di questi sentimenti deve prima
assicurare al Suo soggetto senziente che Egli esiste al di là di ogni dubbio.
L’amore può essere forzato ed affidarsi a speculazioni riguardanti la
reciprocità che esso ispira; ciò è comprensibile. Ma un amore forzato ad
affidarsi a speculazioni riguardanti l’esistenza dell’oggetto amato non ha
senso. Colui che è onnipotente avrebbe potuto dare la certezza. Poiché non l’ha
data, Egli, se esiste, deve averla ritenuta non necessaria. Perché non
necessaria? Si comincia a sospettare che forse Egli non è onnipotente. Un Dio
non onnipotente meriterebbe sentimenti affini alla pietà, e in effetti anche
all’amore; ma questo, penso, nessuna delle nostre teodicee lo ammette. E così
diciamo: noi serviamo noi stessi e nessun altro.

Tralasciamo le conclusioni raggiunte in seguito sulla questione se il dio della
teodicea sia più un liberale o un autocrate; è difficile riassumere argomenti
che occupano una parte così grande del libro. Le discussioni e le conclusioni
che Dobb ha riportato, svoltesi talvolta in colloqui di gruppo tra ADAN 300,
NAAD e altri personoidi, e talvolta in soliloqui (lo sperimentatore è in grado
di registrare anche una sequenza puramente mentale, per mezzo di appositi
strumenti collegati con i circuiti del calcolatore), costituiscono in pratica un
terzo di *Non serviam*. Nel testo esse non vengono mai commentate, ma nella
postfazione di Dobb troviamo questa dichiarazione:

“Il ragionamento di ADAN pare incontrovertibile, almeno per quanto riguarda me:
in fin dei conti sono stato io a crearlo. Nella sua teodicea sono io il
Creatore. Sta di fatto che io ho prodotto quel mondo (numero progressivo 47) con
l’ausilio del programma ADONAI IX e ho creato le gemme dei personoidi apportando
una modifica al programma JAHVE VI. Queste entità iniziali diedero luogo a
trecento generazioni successive. Sta di fatto che io non ho comunicato loro
sotto forma di assioma né questi dati né la mia esistenza oltre i limiti del
loro mondo. Sta di fatto che essi sono giunti a ritenere possibile la mia
esistenza solo per via dell’inferenza, sulla base di congetture e ipotesi. Sta
di fatto che quando creo esseri intelligenti non mi sento autorizzato a esigere
tributi di alcun genere: amore, gratitudine, o servizi di sorta. Posso espandere
o ridurre il loro mondo, accelerare o rallentare il loro tempo, alterare le
modalità e gli strumenti della loro percezione; posso eliminarli, dividerli,
moltiplicarli, trasformare il fondamento ontologico stesso della loro esistenza.
Sono quindi onnipotente rispetto a loro, ma in realtà da ciò non segue che essi
mi debbano alcunché. Per quanto mi riguarda, essi non sono per nulla obbligati
verso di me. È vero che io non li amo. L’amore non c’entra affatto, benché,
suppongo, qualche altro sperimentatore possa magari provare questo sentimento
per i suoi personoidi. A mio parere, ciò non modifica in nulla la situazione;
proprio in nulla. Immaginate per un momento che io colleghi al mio BIX 310 092
un’enorme unità ausiliaria che funga da ‘aldilà’. Attraverso i canali di
connessione faccio passare dentro l’unità, una dopo l’altra, le ‘anime’ dei miei
personoidi, e laggiù ricompenso quelle che credevano in me, mi rendevano
omaggio, mi dimostravano gratitudine e fiducia, mentre punisco gli altri, i
‘senza-dio’, per usare il vocabolario dei personoidi, per esempio annientandoli,
oppure torturandoli. (A un castigo eterno non oso nemmeno pensare, non sono un
mostro fino a quel punto!). Non c’è dubbio che il mio operato sarebbe
considerato un atto di spudoratissimo egoismo, un meschino gesto di vendetta
irrazionale, insomma, l’ultimo oltraggio in una situazione di dominio totale su
esseri innocenti. E questi innocenti avrebbero contro di me la prova
irrefutabile della *logica*, che è l’egida della loro condotta. Ciascuno, è
ovvio, ha il diritto di trarre dagli esperimenti della personetica le
conclusioni che considera appropriate. Una vola, durante una conversazione
privata, il dottor Ian Combay mi disse che in fin dei conti avrei potuto fornire
alla società dei personoidi la certezza della mia esistenza. Ebbene, è una cosa
che non farò mai, poiché ciò avrebbe secondo me tutta l’aria di voler provocare
un effetto, cioè sembrerebbe voler sollecitare una reazione da parte loro. Ma
che cosa potrebbero poi fare o dirmi, che non mi faccia sentire in profondo
imbarazzo, il dolore pungente di essere il loro sfortunato Creatore? Le bollette
dell’elettricità che consumiamo devono essere pagate ogni trimestre, e arriverà
il momento in cui i miei superiori all’università esigeranno la ‘chiusura’
dell’esperimento, cioè l’arresto della macchina o, in altre parole, la fine del
mondo. Intendo rimandare quel momento per quanto è umanamente possibile. È
l’unica cosa di cui sono capace, ma non è una cosa che consideri particolarmente
degna di lode.  Anzi, è ciò che, in un linguaggio comune, viene di solito
chiamato ‘una sporca faccenda’- Dicendo questo spero che nessuno si faccia
venire delle idee; ma se gli vengono, be’, è affar suo”.

Riflessioni
-----------

“Non serviam” non solo utilizza in maniera straordinariamente raffinata e
precisa temi dell’informatica, della filosofia e della teoria dell’evoluzione,
ma potrebbe quasi essere un resoconto vero di certi aspetti delle attuali
ricerche sull’intelligenza artificiale. Per fare un esempio, il famoso
[SHRDLU][2] di [Terry Winograd][3] dà l’impressione di essere un robot che
sposta qua e là blocchi colorati sul piano di un tavolo con un braccio
meccanico, ma in realtà il suo mondo è stato totalmente allestito o simulato
*all’interno del calcolatore*: “In effetti, la macchina si trova proprio nella
situazione paventata da [Descartes][4]: è un semplice calcolatore che sogna di
essere un robot”. La descrizione fatta da Lem di mondi simulati al calcolatore e
di esseri pensanti simulati contenuti in essi (mondi fatti di matematica, in
realtà) è tanto poetica quanto precisa, con una sola netta falsità, parente di
altre falsità incontrate a più riprese in questi racconti. Stando a Lem, grazie
alla velocità fulminea dei calcolatori, il “tempo biologico” di questi mondi
simulati può essere molto più veloce del nostro tempo reale e viene rallentato
mettendolo al passo col nostro solo quando vogliamo compiere osservazioni ed
esami: “… un secondo di tempo macchina corrisponde a un anno di vita umana”.

In effetti ci sarebbe una differenza notevolissima tra la scala temporale di una
simulazione al calcolatore su larga scala, pluridimensionale e molto minuziosa
come quella descritta da Lem, e la scala temporale del nostro mondo quotidiano;
ma la differenza andrebbe proprio in senso opposto! Un po’ come l’elettrone di
Wheeler, che intesse l’universo intero correndo avanti e indietro come una
spoletta, una simulazione al calcolatore deve lavorare aggiungendo i particolari
in modo sequenziale e, anche alla velocità della luce, simulazioni semplicissime
e puramente “di facciata” (che sono tutto quanto l’intelligenza artificiale ha
finora tentato di attuare) impiegando un tempo molto più lungo che non ciò che
le ha tanto ispirate nella vita reale. La risposta dell’ingegneria a questo
problema è naturalmente l’“elaborazione parallela” – il funzionamento
contemporaneo diciamo di alcuni milioni di canali di simulazione – (per quanto
nessuno sappia ancora come attuarla); una volta che si abbiano mondi simulati da
milioni di canali in elaborazione parallela, affermare che essi sono simulati
anziché reali (ancorché artificiali) avrà un significato molto più vago. Si veda
sopra “La settima sortita” e, sotto, “Conversazione col cervello di Einstein”,
dove questi temi vengono esplorati ulteriormente.

In ogni caso, Lem dipinge con straordinaria vivacità un “universo cibernetico”
con abitanti software coscienti. Egli usa varie parole per indicare ciò che noi
spesso abbiamo chiamato “anima”: parla di “noccioli”, di “nuclei personali”, di
“gemme dei personoidi”, e a un certo punto dà anche l’illusione di fornire una
descrizione più specificamente tecnica: “una ‘nube coerente di processi’,… un
aggregato funzionale avente una specie di ‘centro’ che può essere isolato con
una certa precisione”. Lem descrive la coscienza umana, o meglio quella dei
personoidi, come un progetto non chiuso e non chiudibile di conciliazione totale
dele ostinate contraddizioni del cervello. Essa nasce, e “si libra e volteggia”
su un regresso all’infinito di conflitti tra livelli nel cervello. È uno
“zibaldone”, una scappatoia “dalle trappole della gödelizzazione”, “uno specchio
il cui compito è quello di riflettere altri specchi, che a loro volta ne
riflettono altri ancora, e così via all’infinito”. È poesia, filosofia o
scienza, questa?

La visione dei personoidi in paziente attesa di una prova dell’esistenza di Dio
attraverso un miracolo è toccante e sorprendente. Visioni simili sono talvolta
discusse dai maghi del calcolatore nei loro covi, a tarda notte, quando tutto il
mondo sembra baluginare di una sua armonia matematica. Al laboratorio per
l’intelligenza artificiale di di Stanford, una notte [Bill Gosper][5] espose la
sua visione di una “teogonia” (per usare il termine di Lem) somigliantissima a
quella di Lem. Gosper è un esperto del cosiddetto [“Game of Life”][6], il Gioco
della Vita, sul quale egli basa la sua teogonia. “Life” è una sorta di “fisica”
bidimensionale inventata da [John Horton Conway][7], che si può programmare
facilmente su un calcolatore e osservare su uno schermo. In questa fisica,
ciascun quadratino (cella) di una scacchiera grandissima, teoricamente infinita
(in altre parole, di un reticolato), c’è una luce che può essere accesa o
spenta. Non solo lo spazio, ma anche il tempo è discreto (discontinuo). Il tempo
procede da un istante all’altro con piccoli “salti quantici” alla maniera in cui
si muove la lancetta grande di certi orologi, che sta ferma per un minuto e poi
fa un salto. Tra l’uno e l’altro di questi istanti discreti l’elaboratore
calcola un nuovo “stato dell’universo”, basato sullo stato precedente, e poi lo
visualizza sullo schermo.

A ogni istante si ha uno stato che dipende solo dallo stato dell’istante
immediatamente precedente: nulla che sia più remoto nel tempo viene “ricordato”
dalle leggi della fisica di “Life” (questa “località” nel tempo, fra l’altro,
vale anche per le leggi fondamentali della fisica del nostro universo). Anche
per lo sazio della fisica di “Life” presenta un’analoga proprietà (e anche in
questo è conforme alla nostra fisica); cioè, nel passaggio da un dato istante al
successivo, solo la luce della cella considerata e quelle delle sue immediate
vicine determinano ciò che quella cella dovrà fare l’istante dopo. Le vicine
immediate di ogni cella sono otto, quattro adiacenti e quattro in diagonale.
Ciascuna cella, per stabilire ciò che deve fare nell’istante successivo esamina
le otto celle vicine e conta quante sono le luci accese nell’istante attuale. Se
le luci accese sono esattamente due, la luce rimane com’è, se sono esattamente
tre, la cella si accende, indipendentemente dal suo stato precedente; altrimenti
si spegne. (Nel gergo tecnico, quando una cella si accende si parla di “nascita”
e quando si spegne si parla di “morte”: termini in carattere col “gioco della
vita” ). Le conseguenze di questa semplicissima legge, quando essa viene
obbedita simultaneamente su tutta la scacchiera sono davvero stupefacenti.
Benché “Life” abbia ormai più di dieci anni, i suoi abissi non sono stati ancora
scandagliati appieno.

La località temporale ha come conseguenza che l’unico modo in cui la storia
remota dell’universo potrebbe esercitare qualche effetto sul corso degli eventi
presenti sarebbe quello di codificare in qualche maniera i “ricordi” in
configurazioni di luci estendentisi su tutta la scacchiera (si può parlare a
questo proposito di un “appiattimento” del passato nel presente). Naturalmente
le strutture fisiche dovrebbero essere tanto più ampie quanto più
particolareggiati fossero i ricordi. E tuttavia la località spaziale delle leggi
della fisica ha come conseguenza che le strutture fisiche ampie non possono
sopravvivere: possono soltanto disintegrarsi.

Fin dai primissimi tempi la questione della sopravvivenza e della coesione delle
strutture ampie è stato uno dei grossi problemi di “Life”, e Gosper è stato uno
dei scopritori di diverse strutture affascinanti che, grazie alla loro
organizzazione interna, riescono a sopravvivere e manifestano comportamenti
interessanti. Certe strutture (chiamate “cannoni”) emettono periodicamente
strutture più piccole (“alianti”) che navigano lentamente verso l’infinito.
Quando due alianti si scontrano o, in generale, quando si scontrano due grandi
strutture lampeggianti, ne sprizzano scintille.

Guardando queste strutture di luci sullo schermo (e mettendole a fuoco più da
vicino o più da lontano, in modo da vedere gli eventi a scale diverse), Gosper e
gli altri hanno acquisito una profonda comprensione intuitiva degli accadimenti
nell’universo di “Life”, a cui si accompagna un vocabolario colorito
(flottiglie, treni a vapore, sbarramenti di alianti, bombarde, allevatori,
divoratori, rastrelli spaziali, anticorpi e così via). Configurazioni che per un
novellino sono di un’imprevedibilità spettacolare, per questi esperti sono molto
intuitive. Tuttavia in “Life” rimangono parecchi misteri. Esistono strutture
caratterizzate da un infinito aumento di complessità, oppure tutte le strutture
raggiungono a un certo punto uno stato stazionario? Esistono livelli di
struttura sempre più alti che posseggono leggi fenomenologiche specifiche, come
accade per le molecole, le cellule, gli organismi e le società del nostro
universo? Secondo Gosper, su una scacchiera immensa, così immensa che
l’intuizione dovrebbe forse elevarsi di parecchi livelli per cogliere il senso
di forme d’organizzazione tanto complesse, potrebbero anche esistere “creature”
dotate di coscienza e libero arbitrio, capaci di riflettere sul loro universo e
sulla fisica che lo regge, capaci addirittura di speculare sull’esistenza di un
Dio che l’abbia creato, e sui modi per comunicare con “Lui”, o di chiedersi se
questi sforzi abbiano un qualche senso o valore, e così via.

Qui ci s’imbatte nell’eterno problema di come il libero arbitrio possa
coesistere con un substrato deterministico. La risposta, in parte, è che il
libero arbitrio è nell’occhio di chi lo esercita e non nell’occhio del Dio che è
lassù. Finché la creatura *si sente* libera, essa è libera. Ma invece di
continuare a discutere su questi arcani, diamo la parola a Dio stesso, il quale
nel prossimo brano accondiscende a spiegare ad un confuso Mortale quale sia la
vera natura del libero arbitrio.

**D.C.D.**  
**D.R.H.**

[1]:https://it.wikipedia.org/wiki/Stanis%C5%82aw_Lem
[2]:https://en.wikipedia.org/wiki/SHRDLU
[3]:https://en.wikipedia.org/wiki/Terry_Winograd
[4]:https://it.wikipedia.org/wiki/Cartesio
[5]:https://en.wikipedia.org/wiki/Bill_Gosper
[6]:https://it.wikipedia.org/wiki/Gioco_della_vita
[7]:https://en.wikipedia.org/wiki/John_Horton_Conway
