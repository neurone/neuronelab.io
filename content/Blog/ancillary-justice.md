title: Ancillary Justice
date: 2016-05-03 19:19
tags: fantascienza, recensioni, libri
summary: Mi ero avvicinato a questo libro dopo averne letto una recensione e notato diverse similitudini con il *Ciclo della Cultura* di *Iain M. Banks*. Quella serie di romanzi mi aveva tanto affascinato, con la sua ambientazione insolita e la smisurata fantasia dell'autore, che qualsiasi cosa che appaia simile mi attira irresistibilmente. Sapevo di non dovermi aspettare un clone od una sorta di prosecuzione ufficiosa, quindi non m'ero fatto tante illusioni. Ad *Ancillary Justice* mi aveva attirato anche la quantità di premi letterari di genere che il romanzo era riuscito ad aggiudicarsi. Dato che altri appassionati di fantascienza avranno notato le stesse similitudini, descriverò in cosa *Ancillary Justice* differisce dai romanzi della *Cultura*.

![Ancillary Justice][5]{: style="float:right"}
Mi ero avvicinato a questo libro dopo averne letto una recensione e notato diverse similitudini con il [Ciclo della Cultura][1] di [Iain M. Banks][2]. Quella serie di romanzi mi aveva tanto affascinato, con la sua ambientazione insolita e la smisurata fantasia dell'autore, che qualsiasi cosa che appaia simile mi attira irresistibilmente.

Sapevo di non dovermi aspettare un clone od una sorta di prosecuzione ufficiosa, quindi non m'ero fatto tante illusioni. Ad *Ancillary Justice* mi aveva attirato anche la quantità di premi letterari di genere che il romanzo era riuscito ad aggiudicarsi. Dato che altri appassionati di fantascienza avranno notato le stesse similitudini, descriverò in cosa *Ancillary Justice* differisce dai romanzi della *Cultura*.

È un ottimo romanzo di fantascienza, con alcune trovate originali ma che tendono a spiazzare un po' il lettore. Penso che Banks abbia già scritto tutto quanto si possa scrivere riguardo ad astronavi enormi governate da intelligenze artificiali dalle capacità cognitive e sensibilità incomparabilmente superiori a quelle umane e con l'abilità di manifestarsi in più corpi simultaneamente. Escludendo le capacità enormemente superiori, questo è presente anche in Ancillary Justice, ma dall'originale punto di vista di uno dei corpi controllati dalla IA della nave, dopo aver perso la nave.

La narrazione segue due archi temporali alternati, in modo simile al capolavoro [La guerra di Zakalwe (Use of Weapons)][3] ma più lineare. Mentre in *Use of Weapons* i due archi andavano in ordine cronologico inverso per poi ricongiungersi nel finale dando complemento alla storia narrata, in *Ancillary Justice* vanno entrambi nella stessa direzione temporale fino a congiungersi portando al frenetico finale della storia.

Anche in *Ancillary Justice* la società non fa particolari distinzioni di genere e ciò si manifesta narrativamente nell'uso casuale dei pronomi di genere, scelta che inizialmente spiazza un po' il lettore. Personalmente ho apprezzato l'intenzione ed ho deciso di stare al gioco, abituandomi presto. Nel *Ciclo della Cultura*, il genere non è considerato una discriminante degna di nota, perché nella società che vi viene descritta, chiunque può in qualunque momento cambiare il proprio genere ed il proprio sesso.

L'altra scelta originale che può spiazzare il lettore, è la presenza di identità multiple. Nei flashback della protagonista, essa descrive in prima persona ciò che viene visto dalle varie *ancelle* - umani riprogrammati per essere controllati come zombie dalla IA delle navi - dal punto di vista della IA. Quindi, la situazione di una consapevolezza il cui punto di vista è multiplo.

A differenza del *Ciclo della Cultura*, in cui viene descritta una avanzatissima società anarchica in economia di [post-scarsità][4] i cui dilemmi morali e contraddizioni vengono messi in evidenza dalle avventure dei protagonisti, i quali spesso hanno a che fare o sono agenti del "Contatto" o delle "Circostanze Speciali" (due divisioni il cui scopo è di occuparsi dei contatti con le civiltà aliene, oppure di fare quei lavori che la Cultura giudica al limite dell'etico), nell'universo di *Ancillary Justice* si vive all'interno di un'enorme distopia fascista estremamente aggressiva, governata da un tiranno, anch'esso dalla mente *distribuita*. Sono presenti dilemmi morali, ma ciò che lascia un po' spiazzati è che i personaggi, pur essendo ostili al dittatore, non sembrano mettere in discussione più di tanto la struttura della loro stessa società, che è una dittatura millenaria, tanto che il termine "tiranno" non esiste nella loro lingua e viene presa a prestito dalla lingua di una delle civiltà conquistate.

Penso che sia un buon romanzo, ma che manca di emozionare e meravigliare. Alcune idee sono ottime, per molte altre avrebbe potuto osare molto di più. Ne consiglio comunque la lettura. Ora ho in coda il seguito: *Ancillary Sword*.

> Justice of Toren, un’astronave dotata di intelligenza artificiale, ha trasportato per millenni le truppe del Radch, una superpotenza aggressiva ed espansionista basata sulla totale sottomissione e sorveglianza della popolazione. Justice of Toren non controllava solo i propri sistemi e computer, ma anche altre astronavi e milizie umane, soldati di cui sono rimasti soltanto i corpi, mentre i loro ricordi e ogni parte senziente sono stati eliminati.
> 
> Breq è quel che resta di Justice of Toren, un corpo dalle sembianze umane privato della propria identità e connesso con l’intelligenza artificiale dell’astronave, ciò che tutti definiscono un’ancella, rifiutando di considerarla umana.
> 
> Eppure Breq avverte di avere una storia, e quando rinviene nella neve il corpo di un uomo abbandonato come un relitto fuori dal tempo, sente che appartiene al suo passato. Nella vendetta che sta tramando contro chi ha manipolato un intero popolo per farne una massa di automi al servizio della guerra, questo fatto straordinario può essere solo un vantaggio: nessuno in tutto l’impero potrebbe mai sospettare l’esistenza di un’entità autonoma, pronta a ribellarsi all’ordine stabilito.

...ma se ne avete la possibilità, non fatevi mancare i romanzi di Iain Banks. ;-)

[1]: https://it.wikipedia.org/wiki/Ciclo_della_Cultura

[2]: https://it.wikipedia.org/wiki/Iain_Banks

[3]: https://it.wikipedia.org/wiki/La_guerra_di_Zakalwe

[4]: https://en.wikipedia.org/wiki/Post-scarcity_economy

[5]: {filename}/images/ancillary-justice.jpg
