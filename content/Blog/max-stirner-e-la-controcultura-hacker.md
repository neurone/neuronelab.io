title: Max Stirner e la controcultura hacker
date: 2015-10-14 20:46
tags: anarchia, filosofia, hacking, software libero, gnu-linux, cyberpunk
summary: Pubblico il saggio sull’*anti-filosofo* Max Stirner, scritto da Nicola Durante. Dopo un’introduzione sul pensiero di Stirner e la terminologia da lui usata, viene messo a confronto con il fenomeno delle controculture, ed in particolare quella hacker. Verrà esaminata la sua idea di libertà, individualista e radicale, e confrontata con idea di libertà di stampo più sociale, promossa dalla Free Software Foundation, evidenziandone le differenze.

Pubblico il saggio sull’*anti-filosofo* [Max Stirner][1], scritto da Nicola Durante. Dopo un’introduzione sul pensiero di Stirner e la terminologia da lui usata, viene messo a confronto con il fenomeno delle controculture, ed in particolare quella hacker. Verrà esaminata la sua idea di libertà, individualista e radicale, e confrontata con idea di libertà di stampo più sociale, promossa dalla Free Software Foundation, evidenziandone le differenze.

Un testo molto interessante, specialmente per le idee poco comuni contenute. È
sempre necessario un notevole sforzo per potersi liberare dal pensiero comune,
dalla cultura dominante e dalla tradizione. Per questo motivo, le idee espresse
da chi riesce a farlo, come se osservasse dal di fuori la società, hanno enorme
valore.

Potete scaricare il saggio in formato
[PDF]({filename}/pdfs/max_stirner_e_la_controcultura_hacker.pdf), oppure [Testo
semplice (formattato
Markdown)]({filename}/misc/max-stirner-e-la-controcultura-hacker.txt.gz).

MAX STIRNER E LA CONTROCULTURA HACKER
=====================================
*di Nicola Durante*

**Prima edizione: settembre 2008**  
*some right reserved*

NOTA INTRODUTTIVA
-----------------
Questo breve scritto non rappresenta, come forse vorrebbe qualche lettore
disattento o malizioso, un incentivo alla non-collaborazione e al solipsismo.
Nella sua provocatorietà vuole invece essere uno spunto per riflettere sulla
differenza che passa tra “servire” e “collaborare”. La cooperazione non è un
ideale davanti al quale inchinarsi ma uno strumento indispensabile per la
felicità di ognuno. Servire significa invece assecondare acriticamente
qualsiasi richiesta nel nome di una causa che mai ci appartiene veramente. Se
lo Stato mi chiede di uccidere ed essere ucciso in guerra, nel nome della
Patria, io non ubbidirò. Se una Chiesa mi sprona a convertire con la prepotenza
l’infedele, nel nome di un Dio, io non lo farò. Chiunque conosca un pò la
storia e sappia osservare il presente dovrebbe rendersi conto di quante teste
sono cadute e di quante ostilità sorgono tuttora nel nome delle “religioni” e
dei “buoni ideali”.

MAX STIRNER E LA CONTROCULTURA HACKER
-------------------------------------
[Max Stirner][1] è l’autore di “L’unico e la sua proprietà” il libro più scandaloso
e controculturale della filosofia moderna. La controcultura hacker è un
movimento con inclinazioni antiautoritarie il cui habitat è costituito
prevalentemente dal mondo informatico. La propulsione del filosofare
stirneriano richiama quella di ogni controcultura: liberare il singolo dalle
costrizioni che ne ostacolano la volontà creativa. Il seguente studio, facendo
dialogare l’unico di Stirner con la controcultura hacker, ne mette in luce non
solo il momento liberatorio, individuato nella rivolta teorizzata da Stirner,
ma anche quello contraddittorio, definito religioso, in cui l’individuo con la
promessa di libertà viene imprigionato dallo spirito della controcultura.

MAX STIRNER – UN FILOSOFO ANTI-FILOSOFO
=======================================
Johann Kaspar Schmidt (Bayreuth, 25 ottobre 1806 – Berlino, 26 giugno 1856),
noto come Max Stirner, rappresenta un caso piuttosto bizzarro nella storia
della filosofia occidentale. Reso immediatamente famoso, o meglio famigerato,
dalla sua unica opera “L’unico e la sua proprietà” che vide la luce nel 1844,
in pochi anni venne dimenticato per poi rinascere come ideologo del movimento
anarchico nel 1898 grazie alla sua tuttora unica biografia scritta dallo
scozzese John Henry Mackay che sarà il suo primo e più fedele evangelista.

Oggi molti ricordano Stirner solo perché Marx e Engels (che inizialmente non
nasconde un certo entusiasmo per l’unico) ne parlano con intenzioni distruttive
e infamanti nella “Ideologia tedesca” in cui, riga per riga, le affermazioni di
Stirner vengono isolate, aggredite e malmenate. La critica di Marx ha
sicuramente contribuito ad eclissare Stirner, la cui fortuna come filosofo, se
escludiamo pochissimi coraggiosi interpreti, è stata piuttosto misera. Basta
sfogliare qualsiasi manuale della storia della filosofia occidentale per
constatare come a Stirner sia concesso pochissimo spazio.

Addirittura alcuni critici non lo considerano nemmeno un filosofo. È curioso
rilevare che neanche lo stesso Stirner si considerasse un filosofo anzi,
nell’unico, indossa consapevolmente le vesti di sofista, di anti-filosofo e si
proclama nemico del pensiero e della metafisica. Il filosofo è infatti, secondo
Stirner, alla ricerca di una verità davanti a cui prostrarsi e «si differenzia
del credente solo perché crede a molte più cose del secondo, il quale, dal
canto suo, pensa assai meno, avendo già i suoi articoli di fede; inoltre,
sempre secondo Stirner, «chi ha nella verità un idolo, un principio sacro, si
deve umiliare davanti a essa, mettendo da parte coraggio e baldanza, non può
opporsi alle sue richieste o resistere coraggiosamente, insomma deve rinunciare
al coraggio eroico della menzogna.»

Stirner aggiunge di non esprimere i suoi pensieri per amore della verità o
amore dei suoi lettori ma esclusivamente per dispiegare se stesso, come fa
l’uccello sopra i rami che canta solo per saziarsi della propria melodia.

Da questi brevi cenni possiamo capire il tono generale dell’opera di Stirner in
cui il nostro filosofo non manca mai di mortificare il lettore prendendosi
gioco dei capisaldi della sua educazione.

Stirner prende in prestito la dialettica hegeliana costituita da tesi, antitesi
e sintesi e la impiega per fabbricare delle triadi assurde o incomplete,
ridicolizzandola; ad esempio all’inizio dell’opera, concludendo l’illustrazione
dei tre stadi della vita umana (fanciullo – giovane – adulto), dopo aver
trascinato il lettore in una serie di sofismi, lo fredda scrivendo: «Come sarà,
infine, il vecchio? Se lo diventerò, ci sarà tempo di parlarne»; sicuramente
anche questa mancanza di sistematicità ha contribuito ad allontanare da Stirner
i favori della critica filosofica. D’altra parte, se rimaniamo ancorati ad una
lettura superficiale dell’unico, possiamo solo fraintendere le parole di
Stirner, in quanto lui stesso ammette che, per cercare di comunicare, è stato
costretto ad utilizzare un vocabolario che non gli appartiene e che vuole
invece dissolvere. Stirner si riferisce al vocabolario che usiamo tutti noi,
quello che lui chiama cristiano-metafisico.

Con questo proposito riporta in onore tutti quei termini che erano stati
bollati d’infamia dal linguaggio cristiano e decide di fregiarsi del titolo di
egoista. Dobbiamo sempre tener presente che il termine “cristiano” assume in
Stirner un’accezione molto vasta; inoltre il cristianesimo non comincerebbe con
le prediche di Cristo, ma con quelle di Socrate. Cristianesimo, in Stirner, non
indica la dottrina cristiana, ma più estesamente tutta la dimensione del
pensare che procede per concetti.

Lo stesso Stirner scrive parlando di sé in terza persona: «Ciò che Stirner dice
è una parola, un pensiero, un concetto; ciò che intende non è una parola, non è
un pensiero, non è un concetto»..

Ma perché tanta avversione nei confronti del pensiero?

Secondo Stirner il pensiero può essere causa di alienazione, questo avviene
perché l’idea pensata può sfuggire a chi l’ha pensata, sottomettendolo,
diventando sua guida, cominciando a vivere una vita autonoma e facendosi
ideale, missione e quindi costrizione.

Dato il “rovesciamo linguistico” operato da Stirner passiamo adesso a spiegare
il significato che assumono alcuni concetti comuni nell’accezione stirneriana.

SANTITA’, RELIGIONE ED EGOISMO
------------------------------
Abbiamo appena detto che Stirner si accorge che l’idea (oggetto), una volta
allontanata da chi l’ha pensata (soggetto), si innalza diventando santa e
sottomettendo il suo artefice. Nasce in questo modo la religione, termine che
Stirner utilizza in modo molto ampio poiché religioso (o santo) è tutto ciò che
si erge sopra l’individuo e lo comanda. Secondo Stirner i così detti atei,
lungi dall’essere liberi dalla religione, non sono altro che gente pia che ha
sostituito Dio con l’idea di uomo (umanità), idea a cui si sono sottomessi. E’
in questo ambito che si sviluppa la polemica con Feurbach, filosofo critico del
cristianesimo. «La storia va in cerca dell’uomo» dice Stirner, «esso però sono
Io, Tu, Noi. Cercato come un essere misterioso, come il divino, dapprima come
il Dio, poi come l’uomo (umanità), esso viene trovato come il singolo, il
finito, l’unico». Precisiamo che questo Io non è da intendersi in senso
fichtiano, come opposizione al non-io, ma più concretamente come l’io esistente
e caduco: l’individuo nella sua limitatezza.

Secondo Stirner, Feuerbach, nel tentativo di liberare l’uomo da Dio, lo ha
invece legato a sé in maniera ancora più indissolubile facendo diventare l’uomo
“divino”. Il singolo, sia nella religione cristiana che in quella umana di
Feuerbach, non si appartiene mai: prima doveva cercare se stesso in Dio e nelle
sue leggi, ora invece nell’idea astratta di uomo e nella moralità umana. La
santità viene percepita da Stirner come estraniazione e quella di Fuerbach non
è altro che una santità atea. E poiché “ciò che è santo” è destinato a fuggire
l’uomo possiamo convertire la santità con la stessa religiosità, infatti il
religioso è colui che pone le idee sopra di sé. Stirner chiama il religioso:
fanatico e ossessionato, osservando che il fanatismo è proprio delle persone
colte, poiché «la persona colta ripone interesse nelle cose spirituali. Ora,
quando un tale interesse si manifesta in atto, diviene” fanatismo “; è cioè un
interesse fanatico per una cosa sacra ” (fanum)». L’uomo religioso è, per
Stirner, l’uomo alienato, che fonda la sua causa su delle idee irraggiungibili.

E’ in questo contesto che deve essere inteso il termine egoista. L’egoista,
mettendo tutto “sotto di sé”, è il non-alienato, il non-religioso. L’egoista è
un individuo che si appartiene, perché, come recita Stirner all’inizio (e alla
fine) dell’opera, fonda la sua causa su nulla.

Attenzione, questo “porre sotto di sé” non è riferito alla potenza che
l’egoista può esercitare nei confronti delle cose. Altrimenti detto: l’egoista
non può fare tutto quello che vuole.

Sarebbe stolto, ammette Stirner, affermare che non esistono potenze superiori a
noi stessi. Lo “stare sotto o sopra” è riferito all’approccio che l’egoista
adotta nei confronti di queste potenze superiori, che è completamente diverso
da quello dell’epoca della religione. L’egoista si proclama nemico di tutte le
potenze superiori quando invece la religione c’insegna a farcele amiche e a
comportarci umilmente nei loro confronti.

L’egoista è un profanatore che tende tutte le sue forze contro ogni timor di
Dio, perché questo timore lo renderebbe schiavo di tutto ciò che egli lasciasse
sussistere come sacro.

Anche l’umanesimo feuerbachiano è dunque per Stirner una religione, o meglio: è
solo l’ultima metamorfosi della religione cristiana. «Il liberalismo, infatti,
è una religione, perché separa da me la mia essenza e la pone al di sopra di
me, perché innalza l’Uomo allo stesso modo in cui un’altra religione innalza il
suo Dio e i suoi idoli, perché fa di ciò che è mio qualcosa che è al di là,
perché fa di tutto ciò che è mio, delle mie proprietà e della mia proprietà,
qualcosa di estraneo, cioè un’ “essenza”, insomma perché mi pone fra gli uomini
e mi assegna così una “vocazione”».

Vocazione, ideale, missione, scopo sono tutti termini che in Stirner hanno
un’accezione negativa, perché separano l’individuo e lo costringono a cercare
vanamente se stesso in un mondo spettrale.

Qualsiasi etica, qualsiasi dover-essere spingono l’uomo nel baratro
dell’alienazione e della frustrazione. Infatti, ribadisce Stirner, il cristiano
è impossibilitato a diventare pienamente cristiano ed è destinato a rimanere
sempre un povero peccatore. Ma se il cristianesimo condanna tutti gli uomini ad
essere poveri peccatori, per Stirner gli uomini sono invece tutti perfetti
poiché nel momento in cui si sottrae la sfera dell’ideale sono già tutto quello
possono essere.

VERITA’
-------
Per spiegare il modo di intendere la verità di Stirner citerò il seguente
passo: «Tutte le verità sotto di me sono care; una verità sopra di me, una
verità secondo la quale io debba dirigermi, io non la riconosco. Per me non c’è
verità alcuna, poiché al di sopra di me niente ha valore! Neppure la mia
essenza, neppure l’essenza dell’uomo è superiore a me!»

Stirner non fonda, come fa la metafisica tradizionale, la verità nel rapporto
di conformità tra intelletto e cosa, ma sul rapporto che il singolo intrattiene
con la sua proprietà. Un’idea è vera quando mi appartiene ossia la riconosco
come mia proprietà e, in quanto mia, ci posso fare quello che voglio.
Proprietà, come vedremo tra breve, è anche sinonimo di libertà: si spiega così
il titolo dell’opera (L’unico e la sua proprietà). Proprietà è il contrario di
santità: proprio è ciò che sta sotto di me e dunque mi appartiene. Santo è ciò
che sta sopra e perciò sono io ad appartenergli. La misura della verità
dell’idea è data, in questa prospettiva, soltanto dall’impotenza del singolo
rispetto all’idea; dal fatto che il soggetto non si sente più padrone dell’idea
perché l’ha innalzata e gli si è sottomesso.

«La verità è per me come la realtà mondana per i cristiani: “nulla e vanità”.
Essa esiste esattamente come le cose di questo mondo continuano a esistere,
sebbene il cristiano ne abbia dimostrato la nullità; ma essa è vana, perché
essa non ha il suo valore in se stessa, ma in me. Per sé essa è senza valore.
La verità è una creatura».

LIBERTA’
--------
Il motore del filosofare stirneriano è l’esigenza di libertà. Però, secondo
Stirner, la libertà come siamo soliti intenderla è ingannatrice perché include
sempre la prospettiva di un nuovo dominio.

A tale proposito dice: «Così la rivoluzione poteva certamente dare ai suoi
difensori il senso esaltante di lottare per la libertà, ma in verità solo
perché si tendeva verso una libertà determinata e perciò verso un nuovo
dominio, il dominio della legge.»

Stirner fa poi notare che la libertà non può essere zoppa, ma solo totale,
perché un pezzetto di libertà non è la libertà; però la libertà totale non è
possibile nel senso cristiano del termine poiché, osserva Stirner, questa si
configura come una privazione. Infatti “essere libero da qualcosa” significa
soltanto, comunemente parlando, “esserne privo” o “essersene sbarazzato”. “Egli
è libero da questo pregiudizio” equivale a dire “non ne è mai stato
prigioniero” oppure “se ne è sbarazzato”.

Stirner definisce inautentico questo modo di concepire la libertà, oltre che
irrealizzabile, perché, come ogni ideale, si dimostra un fantasma
irraggiungibile (non possiamo liberarci da tutto), e chi obbedisce al grido
della libertà tradizionale arriva sino a rinnegare e ad annientare se stesso.
Se vogliamo essere liberi realmente dobbiamo quindi sbarazzarci di questa
libertà e la dobbiamo rimpiazzare con la dimensione della proprietà, che non
rappresenta un pezzo, ma tutta la libertà. Per questo, libero in senso
autentico è solo l’egoista che, collocandosi al di sopra di tutto, si rende
simile al Dio cristiano. Stirner ci consiglia così di rivolgerci solo a noi
stessi piuttosto che ai nostri déi e idoli. Ci sprona a mettere fuori ciò che
siamo dentro, e a manifestarci per quel che siamo alla luce del giorno:«”Dio”,
come se lo sono sempre rappresentato i cristiani, è un buon esempio di come uno
possa agire solo per impulso proprio. Senza chieder consiglio a nessuno. Agisce
“come gli piace”. E l’uomo, stolto, potrebbe fare altrettanto e invece si
impone il dovere morale di comportarsi come “piace a Dio”.»

Stirner sottolinea, sorprendendo il lettore, come i primi cristiani si
liberarono dall’olimpo pagano proprio grazie a questo egoismo; inoltre,
osservando che tutto quello che facciamo, lo facciamo per amore di noi stessi,
ipotizza: «Se un giorno vi accorgeste che Dio, i comandamenti, ecc., vi
arrecano solo danni, vi limitano e vi portano alla rovina, certamente li
ripudiereste, nello stesso modo in cui una volta i cristiani condannarono la
fede in Apollo o in Minerva o la morale pagana.

Certo, essi li sostituirono con Cristo e (più tardi) Maria e con la morale
cristiana, ma anche questo lo fecero per il bene della loro anima, quindi per
egoismo o individualità propria».

L’individualità è per Stirner la creatrice di tutto ed è sempre stata
considerata la creatrice delle nuove produzioni di importanza universale.

Da queste riflessioni possiamo dedurre che chi non si proclama egoista è
soltanto un egoista ipocrita che Stirner definisce “ingannatore di sé” e
“fustigatore di sé” e vive quindi una condizione di schiavitù. Il nostro
filosofo sottolinea che nessuna religione ha mai potuto fare a meno di
promettere ricompense, sia che queste si riferissero all’aldilà che
all’aldiqua, poiché l’uomo è avido e gratis non fa niente.

«Voi siete egoisti e non lo siete, perché rinnegate l’egoismo. Quando sembra
che lo siate più decisamente, ecco che subito dichiarate ripugnanza e disprezzo
per la parola “egoista”.»

Per Stirner, l’unico modo per essere autenticamente liberi è quello di
“appropriarsi del mondo” perché la libertà comunemente intesa non è
realizzabile. Per capire cosa intenda con “appropriarsi del mondo” dobbiamo
sempre avere presente il senso stirneriano della parola proprietà. Proprio è
ciò che cade sotto di me. Sono, di conseguenza, autenticamente libero solo se
riconosco che nulla vale più di me, solo se sono egoista e mi appartengo.
Interessante, a questo riguardo, è il seguente passo: «Le catene della realtà
scavano nella mia carne in ogni momento le ferite più profonde.

Ma io resto mio. Schiavo di un padrone, io penso solo a me stesso, al mio
vantaggio; certo, le sue percosse mi colpiscono: io non ne sono libero; ma io
le sopporto solo per calcolo, per un mio vantaggio, per esempio per ingannarlo
e ammansirlo mostrandomi paziente, oppure per non attirarmi, con la mia
resistenza, qualcosa di peggio. Ma siccome io ho in mente me stesso e il mio
proprio interesse, coglierò al volo la prossima occasione propizia per
schiacciare il padrone. La mia liberazione da lui e dalla sua frusta sarà
allora semplicemente la conseguenza del mio egoismo precedente.»

A questo punto Stirner distingue tra auto-liberazione ed emancipazione:
l’emancipazione è una libertà che viene concessa da altri; l’emancipato vive
una libertà “monca” ed assomiglia ad un cane che si trascina un pezzo di
catena, mai completamente libero. Chi si è liberato da solo invece è diventato
padrone di se stesso impiegando la propria forza. E forza, abbiamo detto, non è
sinonimo di violenza (come vorrebbero alcuni anarchici), ma di “capacità di
appropriarsi del mondo”.

AMORE
-----
Come per la libertà, Stirner traccia due dimensioni dell’amore: quella
autentica e quella inautentica. L’amore inautentico è quello cristiano,
disinteressato. L’amore autentico invece, è quello dell’egoista. L’amore
cristiano è l’amore inteso come legge suprema, universale. Anche l’amore degli
umanisti atei è dunque cristiano. L’amore di questo genere è una dimensione
antiesistenziale che si pone al singolo come comandamento, in cui non si ama il
singolo individuo, ma la specie. Chi è infatti pieno di amore “santo” non può
far altro che amare ciò che è santo in colui che ama, ossia il fantasma; ed è
proprio in forza di questo amore santo che l’uomo muove guerra al singolo ed
arriva a torturarlo. Scrive Stirner: «Ma chi è pieno di amor sacro ama solo lo
spettro dell’”uomo vero” e perseguita con cieca crudeltà il singolo, cioè
l’uomo reale, appellandosi flemmaticamente al diritto di procedere contro “ciò
che è inumano”. Egli trova che sia cosa giusta e inevitabile essere spietato
fino all’inverosimile, perché l’amore per l’entità generale o lo spettro lo
obbliga a odiare tutto ciò che non è spettrale, ossia l’egoista o il singolo;
questo è il senso del famoso fenomeno d’amore che chiamano “giustizia”» e
ancora «Voi amate l’uomo, e perciò torturate il singolo, l’egoista; il vostro
amore degli uomini non riesce in somma ad altro che a torturare gli uomini.»

L’amore inautentico, quello cristiano-metafisico, si configura come un diritto
che viene preteso dal singolo. La patria si aspetta di essere amata
disinteressatamente, così la famiglia e così pure l’uomo in quanto facente
parte della specie uomo.

Come risponde l’egoista a tali pretese di amore?.

L’egoista risponde di amare soltanto colui che lo rende felice e che gli piace.
Per Stirner, l’amore, come ogni altro sentimento, non può essere un
comandamento ma solo una proprietà del singolo, e come ogni sentimento va
conquistato. «Se una Chiesa, un popolo, una patria, una famiglia, ecc., non
sanno guadagnarsi il mio amore, io non li amerò, e io stabilisco il prezzo del
mio amore a mio piacimento». A dispetto di quanto ci si potrebbe aspettare, le
azioni “altruistiche” non sono affatto estranee all’egoista. Stirner riconosce
che l’egoista può arrivare a rinunciare a innumerevoli cose pur di vedere
rifiorire il sorriso sul volto di chi ama e a mettere a repentaglio la propria
vita, il proprio benessere o la propria libertà, infatti il piacere egoistico
consiste proprio nel godere della felicità di chi amiamo. Ma c’è una cosa che
l’egoista non è disposto a sacrificare: se stesso. Per l’egoista niente è
abbastanza alto perché egli si debba umiliare in sua presenza, niente è
abbastanza autonomo ed estraneo che egli debba vivere per amore di esso, niente
tanto sacro che debba offrirglisi in olocausto.

Stirner afferma: «Anch’io amo gli uomini, non solo alcuni singoli, ma ognuno.
Ma io li amo con la consapevolezza dell’egoismo; io li amo perché amarli mi
rende felice, io amo, perché l’amore è per me un sentimento naturale, perché mi
piace. Io non conosco alcun “comandamento d’amore”. Io provo compassione e
simpatia per ogni essere dotato di sensibilità e il suo tormento mi tormenta,
il suo rallegrarsi mi rallegra: io posso uccidere, ma non torturare». La
tortura sadica è prerogativa dell’uomo religioso: nessuno può essere più
spietato di chi si sente dalla parte del giusto e portavoce del Bene, perché il
senso del diritto e della virtù rende crudeli e intolleranti.

Sorprendentemente anche l’amore sensuale viene messo da Stirner sullo stesso
piano di quello religioso, anzi è esso stesso religioso perché, pur cambiando
l’oggetto d’amore (una singola persona), non viene a mutarsi l’atteggiamento di
dipendenza e sottomissione nei suoi confronti.

Infatti, anche nell’amore sensuale, nella così detta “possessione amorosa”, il
soggetto è costretto ad inchinarsi dinnanzi alla strapotenza e alterità
dell’oggetto amato, che gli diventa dunque estraneo.

L’amore autentico è solo quello che nasce dalla consapevolezza dell’egoismo e
che sgorga dall’interesse personale, scorre nel letto dell’interesse personale
e sfocia di nuovo nell’interesse personale.

Passiamo ora a sondare l’aspetto anarchico di Max Stirner.

L’EGOISTA E LO STATO
--------------------
Come suggerisce Giorgio Penzo, annoverare Stirner tra gli anarchici significa
falsare tutto il suo pensiero, poiché la dimensione anarchica si presenta solo
come un momento “collaterale” che non esaurisce di certo la problematica
stirneriana. L’anarchismo stirneriano è una conseguenza del vivere
egoisticamente, ossia autenticamente. La legge non viene rifiutata sul piano
del contenuto, ma su quello formale: perché sta sopra di me ed è quindi
alienante. Stirner non si scaglia assolutamente contro un determinato stato, o
legge o società; anzi, arriva a sostenere che sarebbe ridicolo ammonire
paternamente i governanti di uno stato affinché non intralcino il libero
sviluppo personale con le loro leggi, anche perché, osserva Stirner, se
seguissero il suo consiglio si dimostrerebbero poco saggi. Infatti il
legislatore è costretto a seguire la sua natura: legiferare implacabilmente,
così come un corvo è costretto a gracchiare. Dunque a Stirner non sta a cuore
la questione dello stato o della società, perché è convinto che è proprio della
natura della società e dello stato reggersi su leggi, che, come tali, sono
sante e di conseguenza estranee all’io inteso come egoista.

L’egoista non intende combattere affinché le leggi di uno stato siano
sostituite da altre leggi, magari più liberali di quelle vigenti, ma si
limiterà a spogliarle della loro dimensione santa.

Spogliare della santità la legge significa ricondurla in una dimensione
autentica e presentarla nella sua vera natura che è la forza.

In questo modo, il rapporto autentico che si configura tra stato e cittadino è
quello della forza.

Conseguentemente, l’egoista, fa cadere sotto di sé anche il concetto di
diritto, facendolo diventare sua proprietà: “mio diritto”, ma questo comporta
inevitabilmente la dissoluzione del concetto stesso che non è mai del singolo,
ma sempre sociale. Il diritto, diventando mio, viene a coincidere dunque con la
mia forza: i miei diritti sono quelli che ho la forza di conquistarmi. Da qui
si chiarifica la massima stirneriana: la forza precede il diritto.

Una cosa appartiene a chi sa prendersela e assicurarsela fino a che non gli
viene tolta di nuovo e, allo stesso modo, la libertà appartiene a chi se la
prende.

Stirner esemplifica: «Quando i romani non ebbero più alcun potere contro i
germani, l’impero romano universale appartenne a questi ultimi e sarebbe stato
ridicolo se qualcuno avesse voluto insistere dicendo che i romani restavano
comunque i veri proprietari».

Lo stato, per sopravvivere, necessita che il singolo riconosca nella volontà
dello stato l’unica volontà autentica, cioè la intenda come diritto, così che
la volontà del singolo verrà considerata arbitraria e quindi come crimine: «Chi
è un io dello Stato, cioè un bravo cittadino o suddito, può vivere indisturbato
in quanto è quell’io, non se stesso».

Anche il termine “criminale” ha un’accezione positiva nel vocabolario
stirneriano; per criminale non si intende colui che trasgredisce una
determinata legge, ma il rapporto che viene ad instaurarsi tra il singolo e la
legge nel momento in cui il singolo tenti di porsi sul piano dell’autenticità.

Ma la volontà di uno stato liberale non dovrebbe combaciare con quella del
singolo individuo, visto che le leggi emanate non sono altro che manifestazione
della volontà del popolo? Niente affatto. Secondo Stirner, anche in questo
caso, il singolo si trova soverchiato dall’universale che viene a rappresentare
l’unica volontà legittima: «Se il popolo ha la libertà di stampa, non sono io
ad averla, sebbene stia in mezzo a questo popolo: una libertà popolare non è la
mia libertà, e la libertà di stampa, intesa come libertà del popolo è
necessariamente accompagnata da una legge sulla stampa che si rivolge contro di
me».

Per Stirner un popolo non può essere libero che a spese del singolo: quanto più
il popolo è libero tanto più l’individuo è legato, e rafforza questa tesi
portando come esempio il popolo ateniese, che proprio nel suo periodo più
libero istituì l’ostracismo, scacciò gli atei e avvelenò Socrate, l’uomo più
savio tra gli ateniesi.

Secondo Stirner lo stato permette sì al singolo di valorizzare tutti i suoi
pensieri e di scambiarli con altri, ma solo finché i pensieri del singolo sono
quelli dello stato. Se il singolo nutre invece dei pensieri che lo stato non
può approvare, cioè non può far suoi, non avrà di sicuro il permesso di
valorizzarli e di scambiarli con altri. Il singolo può comportarsi come un
individuo solo per grazia dello stato, il quale gli concede documenti di
identità e passaporto, ma non gli è permesso di valorizzare ciò che è suo, a
meno che questo si riveli come qualcosa dello stato, che ha ricevuto “in feudo”
da esso: «Le mie vie devono essere le sue vie, altrimenti mi metterà sotto
sequestro; i miei pensieri devono essere i suoi pensieri, altrimenti mi tapperà
la bocca.».

Nello stato, secondo Stirner, non c’è alcuna proprietà, ma esiste solamente la
proprietà dello stato perché solo grazie allo stato io ho ciò che ho, e solo
grazie allo stato io sono ciò che sono.

La mia proprietà privata è solo quella parte di proprietà dello stato che lo
stato stesso mi concede, privandone altri membri dello stato, e per questo
motivo viene detta privata.

Cosa farà allora l’egoista se le vie dello stato non saranno più le sue?

Così risponde Stirner: «Io mi curerò solo di me, senza preoccuparmi dello
Stato! I miei pensieri, che non hanno bisogno di sanzione, beneplacito o grazia
alcuna, costituiscono la mia vera proprietà, una proprietà di cui posso far
commercio. In quanto miei, infatti, essi sono mie creature e io posso
scambiarli con altri pensieri: io li do via in cambio di altri, che diventano
così la nuova proprietà che io mi sono acquistato.»

RIVOLTA E RIVOLUZIONE
---------------------
Nella tematica della legge si inserisce il momento della rivolta che, a mio
parere, permette di collegare l’egoismo di Stirner alla genesi delle
controculture.

Il termine rivolta (Emporung) è un momento puramente esistenziale in cui il
singolo decide di superare la dimensione alienante (sacra) per poter essere
soltanto se stesso.

A differenza della rivoluzione, che è un atto violento che morde sul terreno
politico-sociale, la rivolta si sviluppa su un piano intimo ed è un mutamento
esistenziale che dipende da una interna insoddisfazione del singolo.

L’egoista, colui che è in costante rivolta, non ha come prima intenzione quella
di cambiare lo stato attuale delle cose, ma quella di elevarsi, di far cadere
l’oggetto sacro sotto di sé, facendolo sprofondare nel nulla. Nell’egoismo il
soggetto si riconosce superiore all’oggetto. E’ chiaro che alla ribellione
potrà seguire il cambiamento delle istituzioni esistenti, ma si tratterà solo
di un momento secondario, di un effetto collaterale dell’egoismo.

Stirner, per cercare di spiegare meglio il concetto di rivolta, prende
nuovamente come esempio i cristiani e comincia ad illustrare la fondazione del
cristianesimo, che avvenne in un clima di insoddisfazione politica a cui però i
primi cristiani non parteciparono. Anzi, i liberali del tempo rinfacciavano ai
primi cristiani di aver predicato l’ubbidienza nei confronti dell’ordinamento
sociale e politico allora esistente e di aver dunque comandato di “dare a
Cesare quello che è di Cesare”.

Nonostante questo, Cristo stesso fu incolpato di “trame politiche” mentre
nessuno era più lontano da lui da tali attività, come gli stessi Vangeli ci
dicono.

Non celando, in questo contesto, una certa ammirazione per la figura di Cristo,
Stirner scrive: «Ma perché non era un rivoluzionario, un demagogo, come gli
ebrei avrebbero ben voluto, perché non era un liberale?

Perché egli non si aspettava la salvezza da un cambiamento delle condizioni e
tutto quell’ordinamento gli era indifferente».

Cristo, secondo Stirner, non era un rivoluzionario ma, come ciascuno di quei
cristiani primitivi: un ribelle, uno che si solleva. E continua: «Per questo il
suo principio era solo: “Siate astuti come serpenti”, che esprime la stessa
cosa dell’altro principio, più specifico: “Date a Cesare ciò che è di Cesare”;
egli non conduceva alcuna battaglia liberale o politica contro l’autorità
costituita, ma voleva, incurante di quest’autorità e da essa indisturbato,
percorrere la propria strada».

Cristo era dunque il nemico mortale e vero distruttore dello stato esistente
perché deviò il corso delle sorgenti vitali del mondo pagano facendolo così
appassire. Non gli interessava il rovesciamento dell’esistente e così lo murò
edificandogli sopra, tranquillo e incurante, il suo tempio, senza far caso alle
grida di dolore che venivano da quel che aveva murato.

Conclude così il nostro filosofo: «Bene, forse quel che è successo all’ordine
pagano del mondo capiterà anche a quello cristiano? Una rivoluzione non lo
farebbe certo finire se prima non vi sarà una ribellione!».

ASSOCIAZIONE E SOCIETA’
-----------------------
La società, per Stirner, rappresenta la dimensione inautentica del vivere in
comune. Nella società il singolo è costretto ad inchinarsi dinnanzi alla
moralità e quindi a vivere in una dimensiona alienante: la moralità non si
confà con l’individuo perché essa non ammette l’io ma solo l’uomo ch’io
rappresento. Nonostante questa premessa, Stirner è convinto che la condizione
originaria dell’uomo sia quella di vivere socialmente e non in solitudine, un
vivere sociale autentico deve quindi presupporre un superamento (non un
miglioramento) della società che, secondo Stirner, si realizza
nell’associazione.

Nell’associazione (o unione) l’egoista viene sì a perdere, inevitabilmente, una
parte della propria libertà (come siamo soliti intenderla), ma mai la
proprietà, che è poi ciò che lo rende egoista. Non dobbiamo dimenticare che per
Stirner la proprietà non è altro che la libertà autentica, mentre la libertà
classica è solo un ideale, e come tale, un fantasma irrealizzabile. Abbiamo già
detto che la libertà, intesa come proprietà, corrisponde a non riconoscere
nulla sopra di sé, mentre, la libertà cercata dall’uomo spirituale, si connota
come “liberarsi di” e non trova mai realizzazione perché il singolo sarà sempre
costretto a fronteggiare delle forze a lui superiori di cui non potrà
liberarsi.

Noi non possiamo, ad esempio, volare come gli uccelli, poiché la nostra volontà
non potrebbe mai liberarci dalla legge della gravità.

Lo scopo dell’associazione non è quindi la libertà e tanto meno l’uguaglianza
perché anche l’uguaglianza è un fantasma, e infatti, dice Stirner: «noi siamo
uguali solo quando veniamo pensati come uomini». «Io sono un uomo e tu sei un
uomo, ma “uomo” è solo un pensiero, un’entità generale; né tu né io possiamo
venir espressi a parole, noi siamo indicibili perché solo i pensieri possono
venir detti e consistono nel venir detti.»

Come si compone allora l’associazione? L’associazione è la semplice unione di
egoisti che si alleano per moltiplicare le loro forze e arrivare dove
singolarmente non riescono.

L’Associazione non sta insieme per un legame né di sangue né di fede (spirito).
«Nel legame spirituale, qual è quello di una società, di una Chiesa, il singolo
non ha altro significato che quello di essere un membro dello stesso spirito;
nell’un caso come nell’altro ciò che tu sei come unico deve essere represso».

Secondo Stirner, ogni società ha di mira l’individualità del singolo che tenta
di sottomettere alla propria potenza. L’egoista è sì disposto a veder ridotta
la propria libertà, ma non accetta che la propria individualità gli venga
strappata.

Nell’associazione questo non avviene, perché l’”altro” non è considerato sotto
il punto di vista umano, cioè religioso, ma personale, egoistico.

Chiarisce Stirner: «E’ ben diverso che la società limiti la mia libertà oppure
la mia propria individualità. Nel primo caso, essa è un’unificazione, un
accordo, un’unione; ma se si attenta all’individualità, la società è una
potenza per sé, una potenza al di sopra di me, qualcosa che mi resta
inaccessibile e che io posso certo ammirare, adorare, venerare e rispettare, ma
non dominare e distruggere: non lo posso fare perché io mi rassegno. La società
sussiste grazie alla mia rassegnazione, al mio rinnegamento di me, alla mia
viltà chiamata umiltà.»

Per distruggere la società e preservare la propria individualità, l’egoista non
ha bisogno di azioni violente, di una rivoluzione, ma basta che superi il
momento dell’umiltà, ossia: si ribelli; e la ribellione è un gesto puramente
interiore. Da questa considerazioni risulta chiaramente che Stirner non possa
essere considerato un anarchico visto che non mostra alcun interesse a
modificare minimamente l’ordine delle cose in una determinata società.

Quello che interessa a Stirner è cambiare il rapporto tra io e oggetto e non
alterare il contenuto dell’oggetto. Ed è proprio in riferimento a questo
rapporto che va compresa la “forza” esercitata dall’egoista. Non si tratta di
una forza bruta e violenta ma di un innalzarsi. Un innalzarsi che è frutto
della consapevolezza di essere unico, ed “essere unico” significa riconoscere
che non c’è nulla sopra di me e che tutto è mia proprietà, perchè da me tutto
nasce e tutto muore.

L’unico e la sua proprietà si conclude con le seguenti parole: « Proprietario
del mio potere sono io stesso, e lo sono nel momento in cui so di essere unico.
Nell’unico il proprietario stesso rientra nel suo nulla creatore, dal quale è
nato. Ogni essere superiore a me stesso, sia Dio o l’uomo, indebolisce il
sentimento della mia unicità e impallidisce appena risplende il sole di questa
mia consapevolezza. Se io fondo la mia causa su di me, l’unico, essa poggia
sull’effimero, mortale creatore di sé che se stesso consuma, e io posso dire:
io ho fondato la mia causa su nulla».

MAX STIRNER E LE CONTROCULTURE
==============================
Ora che abbiamo chiarito a grandi linee il pensiero stirneriano, non è
difficile constatare come esso rappresenti un grumo di controculturalità. Se
per controculturale intendiamo un modo di pensare in contrasto con quello
dominante.

Dobbiamo tenere in conto che Stirner scrive l’unico in un clima in cui
l’idealismo di Hegel ha raggiunto il suo apice, e che quella di Hegel è una
filosofia dove l’individuo è una marionetta nelle mani dello Spirito.

A parte questa ovvia osservazione, mi sembra proficuo incrociare l’unico di
Stirner con le controculture perché da questo incontro possiamo ricavare una
chiave di lettura per comprendere le stesse. Questa chiave è rappresentata dal
momento della rivolta che può essere considerato il minimo comun denominatore
della genesi di ogni controcultura.

Dicevamo che la rivolta deve essere distinta dalla rivoluzione e corrisponde al
momento dell’innalzarsi al di sopra della dimensione degli ideali, dei valori.
Una volta che il singolo si è innalzato si trova dinanzi a nulla ed è solo in
questa condizione che è possibile la creazione di valori nuovi, alternativi a
quelli consolidati. Essere egoisti, secondo Stirner, significa rimanere padroni
di questi valori, perché una volta innalzati diventerebbero essi stessi padroni
di chi li ha creati.

Stirner, per farci capire cos’è una rivolta, ci porta l’esempio dei primi
cristiani che si liberarono della morale pagana. E che cos’è il primo
cristianesimo monoteista, antischiavista e pacifista se non una controcultura
generata dall’ideologia imperiale romana, politeista, militare e schiavista?

Abbiamo visto che i cristiani si dimostrano egoisti. Rivoltoso ed egoista sono,
nel linguaggio stirneriano, termini affini: l’egoista è l’individuo in perpetua
rivolta. Però i cristiani non riescono a mantenere accesa la fiamma della
ribellione e finiscono per sostituire Apollo con Cristo; Stirner ribadisce che
questa sostituzione è sempre opera dell’egoismo, per salvare le loro anime,
dice, ma in questo caso si riferisce all’egoismo inautentico, camuffato,
sottomesso a dei valori. A tal proposito, vorrei aprire una parentesi per
muovere una critica interna al filosofare stirneriano. La mia idea è che la
dimensione della rivolta, a differenza di quanto sostiene Stirner delineando il
suo Egoismo, possa essere considerata (se vogliamo rimanere sul piano
dell’autenticità) solo un momento spontaneo e involontario (come nella nascita
della controcultura cristiana) e non un approccio costante e volontario,
finalizzato al raggiungimento della purezza (autenticità). Altrimenti la
rivolta verrebbe innalzata nella dimensione santa dell’essere, forgiando
l’ideale dell’Egoismo (autentico), caratterizzato dal dover-essere in rivolta.
Prefiggersi la missione di “essere autenticamente egoisti”, sotto questa luce,
conduce verso un’aporia insanabile perchè l’egoista è tale proprio perché
rifiuta la dimensione santa (inautentica) dell’essere ossia di dirigersi verso
un ideale, verso ciò che non è già. Leggendo l’unico, si ha talvolta
l’impressione che il singolo debba sottomettersi all’ideale dell’Egoismo con il
fine di vivere autenticamente, e in nessuna pagina di tutta la sua opera
Stirner mette “in pericolo di morte” la sua “idea fissa”, eppure egli stesso
scrive: «Un mio pensiero è veramente mio proprio solo se io non esito in nessun
momento a metterlo in pericolo di morte, se io non ho da temere, nella sua
perdita, una perdita per me, una perdita di me.

Un pensiero è veramente mio proprio se io lo posso sì sottomettere, ma esso non
può mai sottomettere me o rendermi strumento fanatico della sua realizzazione».

Considerazioni di questo genere, riguardo l’egoismo stirneriano, non sono certo
singolari.

Giorgio Penzo, a cui va il grandissimo merito di aver fatto rivalutare Stirner
come filosofo scrive: «Se, come dice Stirner, non si deve più scrivere sopra la
porta del nuovo tempio quel detto apollineo:«Conosci te stesso», ma piuttosto
si deve scrivere il detto: «Valorizzati (verwerte dich), come mai, si può
chiedere, è possibile ciò? In altre parole, come mai è possibile che l’io sia
capace di mantenersi sempre nel sentiero dell’autenticità senza cadere in
quello dell’inautenticità, cioè dello spirito? Del resto, dice Stirner, non già
l’uomo è la misura di tutto, bensì l’io. Così, se ognuno di noi, per non cadere
nella dimensione vaga di uomo, deve continuamente riproporre davanti a sé in
ogni sua azione la dimensione autentica dell’agire; soltanto così è possibile
all’io guadagnare la sua vera realtà. Perciò appare di nuovo, sia pure nel solo
campo dell’agire, la duplicità dell’io come io autentico ed io inautentico. Ed
allora ricompare ancora la dimensione dell’apparenza (io inautentico), e
riappare così ancora quello che era stato l’oggetto principale di tutte le
critiche stirneriane, cioè l’orizzonte dei valori e della santità.»

Dopo questa parentesi critica, che non intacca la profondità del pensiero di
Stirner, vista la sua ricchezza di problemi esistenziali, torniamo a parlare
del suo rapporto con le controculture.

Grazie a Stirner, abbiamo constatato che l’egoismo autentico della rivolta era
riscontrabile nella controcultura cristiana. Ora vedremo come il momento della
rivolta sia presente anche a livello archetipico nei miti che riassumono la
nascita di ogni controcultura In questo compito mi farò aiutare da un libro
piuttosto ambizioso Controculture – da Abramo ai no global, scritto da Ken
Goffman (icona controculturale) e Dan Joy. Si tratta di un libro che vuole
rispondere alla domanda: “Cos’è la controcultura?” mettendo in mostra le
tematiche comuni ricorrenti nelle controculture di periodi e luoghi diversi. E’
significativo constatare come molte controculture analizzate dagli autori del
libro siano diventante, col passare del tempo, delle religioni in senso
proprio, a dimostrazione di come la religione sia figlia della rivolta.

La vicinanza dei due temi (esistenzialismo stirneriano e nascita delle
controculture) è tale che già nell’introduzione del libro, che tratta il
rapporto tra controcultura e autorità, compare un aneddoto che viene richiamato
anche nell’unico di Stirner (sebbene non in maniera esplicita) al momento di
illustrare il rapporto tra l’egoista e lo stato.

Si tratta dell’incontro tra Diogene e Alessandro Magno, in cui il governatore
del mondo chiede al pensatore cinico, che si sta godendo la luce del sole, come
possa rendersi utile; l’irriverente risposta del vecchio filosofo (che è anche
quella delle controculture verso l’autorità) è nota a tutti: “spostati, perché
mi stai facendo ombra.”.

Nell’unico troviamo scritto: «Quanto siano sciocche le chiacchiere, le frasi
vuote di senso dei liberali politici, si può vedere dall’opera del Neuwerk. ”
Sulla partecipazione al governo dello Stato “. In quel libro si biasimano gli
indifferenti e gli apatici, che non sono cittadini dello Stato nel vero senso
della parola, e l’autore fa intendere che non si può esser uomini degni di
questo nome se non si prende viva parte alle cose dello Stato. In ciò egli è
logico, poiché, ammesso che lo Stato sia tutore di tutto ciò che è ” umano “,
noi non possiamo aver in noi nulla di umano se non prendiamo parte alle cose
dello Stato. Ma che prova cotesto contro l’egoista? Nulla poiché l’egoista
considera se stesso quale unico tutore dell’essenza umana e si contenta di dire
allo Stato: fatti in là perchè mi nascondi il sole. Solo quando lo Stato entra
in rapporti o in conflitto con la proprietà individuale, l’egoista prende un
interesse diritto alle cose dello Stato. Se il dotto, solito a studiare tra le
quattro pareti della sua stanza, non si sente oppresso dalle condizioni che
impone ai cittadini lo Stato, dovrà egli occuparsi della cosa pubblica perchè ”
tale è il suo dovere “? Fino a tanto che lo Stato agisce in modo da non turbare
i suoi interessi, che bisogno ha il dotto di levar gli occhi dai suoi libri? Lo
facciano coloro che voglion mutare quelle condizioni in modo più conforme ai
loro bisogni. Il sacrosanto dovere non potrà mai costringere la gente a
riflettere sulle condizioni dello Stato, come non la può costringere a
dedicarsi alle scienze, o alle arti. L’egoismo soltanto può spingerli a far
ciò[..]».

Nell’ambito della nostra ricerca, mi sembra interessante questo passo, non solo
perché indica un’affinità tra egoista e controculture nel modo di rapportarsi
nei confronti dell’autorità: porsi su un piano superiore disinteressandosi di
essa; ma anche perché Stirner sottolinea che il sacrosanto dovere non può mai
costringere le persone ad interessarsi non solo allo Stato, ma anche alle
scienze e alle arti, perché questo lo può fare solo l’interesse personale. Arte
e Scienza, le discipline cardine della maggioranza dei movimenti
controculturali, perché attraverso esse l’individuo si può dispiegare
liberamente, sono necessariamente, per Stirner, “materie da egoisti”. E molte
opere cinematografiche e letterarie, portavoce del pensiero comune, che
rappresentano in maniera stereotipata: lo scienziato come genio solitario,
vanaglorioso e assetato di conoscenza e l’artista come individuo creativo,
asociale e sregolato, sembrano dare una certa ragione a Stirner, perché non
raffigurano di certo delle persone umili e inclini a rispettare la santità
delle convenzioni. Dato per scontato che quelli appena descritti sono
stereotipi: idee comuni fortemente radicate che spesso non trovano riscontro
nella realtà, rimane comunque evidente che lo scienziato si realizzi nel
“conoscere” mentre l’artista, più specificatamente, nel “creare”. In alcuni
casi, come in quello della controcultura hacker, le due figure arrivano a
sovrapporsi nell’hacker programmatore; teniamo anche presente che la
distinzione tra artista e scienziato non è così netta, visto che il divorzio
forzato tra arte e scienza è cosa relativamente recente, voluto dai romantici
che vedevano il rapporto fra queste due discipline come una lotta fra mondo
della vita e ragione astratta. Ad ogni modo, creare e conoscere, sono azioni
che vengono associate alla divinità e che, secondo i timorati di Dio, non si
confanno all’uomo, perché esso deve rimanere umile. Gli spiriti religiosi
vedono nel creare e nel conoscere un peccato, una sfida nei confronti di Dio.
Mi sembra significativa a tal proposito questa testimonianza dello
“scienziato-artista” Linus Torvalds, noto hacker di cui parleremo in maniera
esaustiva: «Dentro i confini del computer, sei tu il creatore. Controlli –
almeno potenzialmente tutto ciò che vi succede. Se sei abbastanza bravo, puoi
essere un dio. Su piccola scala. Dicendo questo ho appena offeso più o meno il
50 per cento della popolazione mondiale. Ma è vero. Crei il tuo mondo e le sole
cose che limitano ciò che puoi fare sono la potenza della macchina e ormai
sempre più spesso – le tue capacità». Non a caso il nostro libro sulle
controculture comincia con l’analisi dell’antico mito di Prometeo in cui si
narrano le gesta di quello che si può considerare il primo peccatore
“tecnologico” dell’antica Grecia. Fatta questa considerazione, riguardo
l’aspetto individualistico e “orgoglioso” dell’essenza delle controculture, che
si fondano sull’espressione della volontà creativa e conoscitiva individuale,
quella che Stirner definisce ”dispiegamento di sé”; ne consegue che le
controculture hanno come fine primario la libertà individuale che si raggiunge
distruggendo ciò che Stirner chiama il principio di stabilità, il vero
principio della religione, la quale si dà da fare per creare “santuari
intoccabili”, “verità eterne”, insomma qualcosa di “sacro”.

Per gli autori di controculture infatti, lo scopo principale delle
controculture non è quello di smantellare i regni del controllo esterno o
scatenare la guerra contro chi detiene il potere, ma quello di cercare prima di
tutto di vivere con la maggior libertà possibile, evitando le costrizioni nei
confronti della volontà creativa individuale. Dunque l’esponente
controculturale, sotto questo aspetto, più che un rivoluzionario, dovrebbe
essere considerato un ribelle (se ci atteniamo alla terminologia di Stirner).
Anche se vedremo che, nella realtà, le cose non stanno sempre così.

LA RIVOLTA DI PROMETEO E LUCIFERO
---------------------------------
Nell’opera del tragediografo Eschilo, il Prometeo incatenato, vengono narrate
le gesta di un dio che viene considerato, da Goffman, controculturale. Prometeo
è una divinità greca che dà inizio ai sacrifici animali. Riassumendo la sua
storia: un giorno Prometeo, dopo aver diviso le interiora e la carne di un
toro, dalle ossa e il grasso, sfida Zeus chiedendogli di scegliere la sua
parte; l’altra metà sarà destinata agli uomini. Zeus, ingannato, sceglie le
ossa e il grasso e così si adira nei confronti di Prometeo e degli uomini che
vengono puniti con la privazione del fuoco. Prometeo decide di rubare il fuoco
ma Zeus lo castiga incatenandolo ad una roccia, dove ogni giorno sarà dilaniato
da un’aquila che gli mangerà il fegato. Ogni notte il fegato si rigenera,
pronto per essere divorato nuovamente.

In questo mito il fuoco rappresenta una metafora della conoscenza (che si
traduce in tecnologia), ciò è palese quando Prometeo afferma di aver donato
agli umani la conoscenza architettonica, la matematica, la scrittura, la
medicina e i mezzi di trasporto «Io trassi il cavallo alle stanghe del carro, e
lo feci tutt’uno alle briglie.. fu mia, solo mia, la scoperta di un mezzo
marino – vele come ali – per la gente che corre le onde». Goffman osserva che
per i greci antichi, che non svilupparono pienamente le loro scienze tecniche
perché temevano la superbia, questo era un racconto ammonitore, analogamente
per i cristiani, la hybris scientifica, raffigurava la volontà di superare i
confini prestabiliti da Dio. Goffman cita anche il Werblowsky del Lucifer and
Prometheus, che alla fine di una sottile argomentazione giustifica il ladro del
fuoco :«…per Eschilo… Prometeo ha trasgredito… E’ un peccatore e non
semplicemente l’eroe di un giusta guerra di liberazione contro i tiranni
crudeli, come ritiene una certa scuola di pensiero»[..] ma «poiché l’ordine di
Zeus è quello di un cosmo statico, ogni umana aspirazione e sforzo
costituiscono una rivolta».

L’archetipo più somigliante a Prometeo, nella mitologia giudaico-cristiana, è
la figura di Lucifero. Infatti, già ai tempi delle prime dottrine cristiane, si
sviluppò, parallelamente alla tradizione teologica, un’interpretazione della
figura di Lucifero in chiave gnoseologica-salvifica. In questo caso la parola
Lucifero viene tradotta letteralmente in “colui che porta la luce” ovvero “la
conoscenza”. Lucifero è colui che ha stimolato l’uomo ad una conoscenza
elevatrice, sfidando la volontà di Dio che aveva scelto per l’uomo un destino
da schiavo. Innegabilmente un alone luciferino pervade tutta l’opera
controculturale di Stirner, alla cui base c’è un atto conoscitivo paragonato,
anche in questo caso, alla luce del sole: essere unico consiste nel riconoscere
che tutto nasce da me e muore in me e che ogni essere superiore a me stesso,
sia Dio o l’uomo, indebolisce il sentimento della mia unicità e impallidisce
appena risplende il sole di questa mia consapevolezza.

A questo punto è facile vedere come “la conoscenza”, metaforizzata nella luce,
sia l’emblema della controculturalità, conoscenza che si configura come strada
verso la libertà individuale. La conoscenza (intesa come tecnologia) sembra
però non piacere a tutti gli esponenti controculturali, alcuni dei quali, come
il “tecnofobo” Ted Roszak, intonano a gran voce: “Vade retro, Prometeo!”.

Questo diverso tipo di approccio nei confronti della conoscenza tecnologica, ha
portato a distinguere le controculture in prometeiche e antiprometiche e può
venire compreso solo se teniamo presente che oggigiorno la tecnologia è
considerata una sorta di divinità da adorare, senza la quale non si può vivere.
Se intesa in questo modo, la tecnologia può venire investita dal momento della
rivolta portando al desiderio di un “ritorno alla terra”. E per questo alcuni
gruppi controculturali antiprometeici auspicano il trasferimento
dell’agricoltura e di altre risorse essenziali per la sopravvivenza, come
l’acqua e le fonti energetiche, ai domicili dei singoli o a piccole comunità
autosufficienti. In tal modo rivendicano la possibilità di un sostentamento
autonomo provvedendo con i propri mezzi a quelle necessità che oggi sono quasi
esclusivamente acquistate mediante un vasto sistema agroindustriale. E’ un modo
per rifiutare di sostenere i principi autoritari ed economici di tale sistema e
sottrarre ad esso potere affidandolo all’individuo e alla vita comunitaria.

CONTROCULTURA E RELIGIONE
-------------------------
Visti i modelli controculturali di Prometeo e Lucifero, si ha l’impressione che
la controcultura consista esclusivamente nella lotta alla dimensione religiosa
(nel senso stirneriano del termine), quella barriera che blocca la libera
crescita dell’individuo e non gli permette di esprimersi, di dispiegarsi come
vorrebbe. In parte, questa valutazione, che deriva dall’identificare la
controculturalità con il momento della rivolta stirneriana, è veritiera, ma
incompleta. Ad essere precisi i miti di Prometeo e Lucifero spiegano solo il
momento della rivolta, quello che porta alla nascita della controcultura, ma
non colgono la controcultura nella sua interezza. Come già sottolineato in
precedenza, il momento della rivolta si esaurisce nello scavalcamento del
sacro, in cui l’individuo viene a trovarsi davanti ad un vuoto di valori, che è
indispensabile per crearne altri.

Una volta che nuovi valori sono stati creati l’individuo creatore (e lo stesso
Stirner ne è la dimostrazione ) o qualche suo discepolo, ne può diventare
succube ed è a questo punto che mi pare di individuare quello che chiamerò il
“momento religioso” della controcultura. Nonostante questo innalzamento di
valori, la componente rivoltosa di una controcultura non muore mai
definitivamente, e si riaccende ogniqualvolta un ribelle rivive genuinamente la
rivolta dei primi rivoltosi oppure mette in crisi quelli che sono i valori
ormai consolidati e innalzati nel momento religioso della controcultura stessa.
Se, in questo secondo caso, il ribelle troverà dei “discepoli” si potrà
verificare uno scisma interno che porterà alla nascita di una nuova
controcultura. Rivolta e religione sono dunque due aspetti inseparabili nei
movimenti controculturali, infatti un solo rivoltoso, o un gruppo di ribelli
(sino a che riescono a mantenersi tali) non formano una controcultura ma un
insieme di individui completamente autonomi, situazione sociale che trova
analogia con l’associazione (o unione) delineata da Stirner. A mio avviso,
anche se l’associazione intesa da Stirner può sembrare una condizione puramente
ideale, le sue caratteristiche si possono riscontrare, in un certo qual modo,
in ogni controcultura che si trovi in una fase embrionale e in cui non si sia
ancora sviluppato il momento religioso. Il momento della rivolta e la creazione
di nuovi valori, riguarda solo un numero ridotto di individui che avvertono
come alienante e coercitivo il contesto culturale in cui vivono, o solo alcuni
aspetti di esso. Ciò che lega i rivoltosi di una controcultura non è un ideale
comune, ma il disagio procurato dal sentimento di prigionia ed estraneamento da
sé. I ribelli (a differenza dei rivoluzionari) sono accomunati da esigenze
individuali (non da valori in nome dei quali decidono di combattere insieme) e
solo per questo motivo decidono di unire le loro forze. Nel momento religioso
la componente rivoltosa tende a spegnersi, e cresce invece, attraverso un
contagio virale, alimentato dall’emulazione (sinonimo di fiacchezza del
pensiero) e dall’aspirazione ad essere membri di un gruppo elitario, il numero
di aderenti (fedeli) alla controcultura. A questo punto la controcultura, se
non si dissolve prima, può venire assorbita da un’altra controcultura, oppure
può sfociare nel grande fiume della cultura mainstream e in alcuni casi, questo
dipende dagli oggetti innalzati dalla controcultura, potrà cristallizzarsi in
un religione in senso proprio, che a sua volta, in seguito ad una rivolta, sarà
in grado di generare altre controculture e religioni. Ogni religione infatti,
se osservata con sguardo storico, nasce con una rivolta; questo lo abbiamo già
visto nel caso del cristianesimo e lo vedremo tra poco(sfruttando la simbologia
di un racconto) con la religione ebraica, il cui patriarca può essere
considerato una sorta di esponente controculturale. Abramo, da rivoltoso, si
innalzò al di sopra del politeismo vigente e vi costruì sopra il suo tempio
monoteista.

ABRAMO, L’ICONOCLASTA
---------------------
La leggenda di cui sto per parlare è tratta dal Midrash, il commento rabbinico
alla Bibbia che utilizza parabole e racconti per metterne in luce gli
insegnamenti giuridici e morali, e che venne scritto dai rabbini nei secoli
successivi alla diaspora romana. Sarò più preciso nel riassumere questo
racconto, che vede come protagonista il patriarca ebreo, perchè è meno noto dei
miti esposti in precedenza. Terah, il padre di Abramo, era un costruttore di
icone di pietra e legno che abitava con il figlio nella città di Ur. Abramo non
credeva nel culto delle immagini sacre e, una volta che il padre gli lasciò in
custodia il negozio, si rivolse ad un cliente chiedendogli: “Quanti anni hai?”.

“Cinquanta” rispose il cliente. “Pover’uomo!”, esclamò Abramo. “Hai
cinquant’anni e vorresti adorare un oggetto vecchio un giorno?”. Sentitosi
umiliato, l’uomo se ne andò pieno di vergogna.

In seguito, il giovane Abramo, chiese al padre chi fosse il Dio che aveva
creato il paradiso, la terra e i figli dell’uomo. Per rispondere alla domanda
del figlio, Terah lo portò a vedere una serie di idoli scolpiti in legno,
indicandoli come coloro che avevano creato tutto quello che esiste sulla terra.

Allora Abramo andò da sua madre e le chiese un po’ di carne speziata da offrire
agli dei; portò la carne alle icone, ma queste non parlavano, né ascoltavano,
né tanto meno mangiavano la carne.

Abramo derise in cuor suo gli dei ed urlò:”Povero mio padre e tutta la sua
iniqua generazione, i cui cuori sono tutti inclini alla vanità, che servono
questi idoli di legno e pietra, che non possono mangiare, né odorare, né
ascoltare, né parlare, che hanno bocche senza verbo, occhi senza vista,
orecchie senza udito, mani senza tatto e gambe senza movimento!”. Quindi Abramo
afferrò un’ascia, distrusse tutti gli dei del padre e quando ebbe finito la
pose tra le mani del dio più grande di tutti e se ne andò. Il padre, udendo
quel frastuono, corse da Abramo e gli chiese: “Perché hai infierito così
malvagiamente sui miei dei?”. Abramo rispose:”Ho posto davanti a loro della
carne speziata e quando gliel’ho avvicinata, affinché potessero mangiarla,
tutti hanno proteso le mani per prenderla prima che il più grande potesse
afferrarla per cibarsi. Così, quest’ultimo, ha afferrato l’ascia e li ha
distrutti tutti e, guarda, l’ascia è ancora tra le sue mani, come puoi vedere”.
Il padre adirato: “Tu menti! In questi dei c’è spirito, anima o potere di fare
quello che mi hai detto? Non sono forse di legno e pietra? Non li ho creati io
stesso? Sei stato tu a porre l’ascia nella mani del grande dio per poter dire
che è stato lui a colpirli tutti”. Ricevuta la risposta che si attendeva Abramo
ribatté: “Come puoi dunque servire questi idoli che non hanno potere di fare
nulla?

Possono questi idoli in cui credi renderti libero? Possono udire le tue
preghiere quando li invochi?”. A questo punto Terah fece arrestare suo figlio e
lo consegnò al re perché fosse rieducato.

Commentare la rivoltosità di Abramo in questo racconto, come l’analogia tra i
suoi intenti e quelli dell’opera stirneriana, mi sembra superfluo.

Dopo aver presentato i simboli della controcultura, nell’ambito della
fondazione mitica di due grandi correnti storiche, da cui è emersa la civiltà
occidentale moderna: la tradizione classica e quella giudaico cristiana,
Goffman passa ad analizzare le caratteristiche comuni alle controculture
arrivando a sostenere la condivisione di metavalori, quali il “primato
dell’individualità” e l’“antiautoritarismo”. Noi invece abbiamo constatato che
il minimo comun denominatore controculturale non è un sistema di valori o
metavalori ma la rivolta, a cui è estranea qualsiasi sfera etica in quanto
superamento dei valori. Ad ogni modo questi metavalori, secondo Goffman,
dovrebbero distinguere le controculture dalla società dominante, nonché le
controculture da sottoculture e minoranze religiose. Ma noi abbiamo già
intravisto che, seguendo la definizione stirneriana di “religione”, queste
presunte differenze tra “minoranze religiose” e controculture non sono poi così
assodate e presto lo vedremo, in maniera specifica, analizzando la
controcultura hacker. In tale contesto vorrei muovere una critica a Goffman,
pur apprezzando nel complesso il suo libro. Goffman, forse a causa di un
eccesso di partigianeria (non dimentichiamo che stiamo parlando di un’icona
controculturale), non sembra cogliere nella sua globalità il fenomeno delle
controculture. Ribadisce di continuo che l’individuo, per la controcultura, è
sempre centrale e che «la partecipazione alla gran parte delle controculture
richiede raramente che gli individui facciano, dicano, pensino o credano
qualcosa di preciso e che tutto ciò che si richiede è l’impegno a contribuire
al processo di abolizione dell’asservimento all’autorità, sia quella applicata
all’esterno che quella radicata nella mente, in modo da permettere
all’individualità di svilupparsi». Descritta in questo modo la controcultura
assomiglia molto all’associazione vagheggiata da Stirner, ma noi abbiamo già
argomentato a proposito, e siamo arrivati alla conclusione che non è così.
Goffman ci nasconde che le controculture sono fenomeni molto estesi, dove gli
individui ribelli, che vivono in maniera autentica la rivolta, sono un numero
infinitesimale rispetto ai membri adoranti. Questi fedeli allo spirito della
controcultura possono apparire certamente dei rivoltosi se paragonati al
cittadino modello, succube delle autorità ortodosse, ma quanti di loro
oserebbero opporsi alla sacralità del verbo delle loro icone controculturali e
allo spirito della controcultura?

In sintesi: Goffman non riconosce il momento religioso delle controculture. E’
in questa dimensione che vengono sanciti i codici morali di una controcultura,
la sua etica. Ed è qui che l’individuo ritorna ad essere schiavo di un
dover-essere, imponendosi una missione “santa”; e nella sfera santa l’
individuo non è più tale, ma un “essere morale” che agisce al servizio di
un’idea e considera suo onore essere uno strumento, un arnese della
controcultura.

LA CONTROCULTURA HACKER: TRA RIVOLTA E SANTITA’
===============================================
Ora entriamo nel cuore del nostro studio, e analizziamo in maniera specifica
una controcultura: quella hacker. La mia intenzione è testare la condizione
attuale della controcultura hacker utilizzando i due momenti controculturali
che ho precedentemente esposto: la rivolta e il momento religioso. Abbiamo già
detto che la rivolta costituisce l’aspetto “vulcanico” della controcultura, in
cui l’individuo manifesta la sua creatività, mentre il momento religioso, pur
essendo anch’esso costitutivo e inevitabile, soffoca l’individuo nello spirito
della controcultura. Realisticamente, questi due aspetti, finiscono per
coesistere simultaneamente e convivere, non solo all’interno di una
controcultura, ma anche in uno stesso individuo (Stirner docet). E’altrettanto
vero, però, che alcuni soggetti sono meno propensi a cadere nelle reti della
religiosità, rispetto ad altri che magari, pur ostentando il loro disprezzo e
superiorità nei confronti delle religioni, diventano dei veri e propri
sacerdoti di culti alternativi (inclusa la Scienza). Analizziamo il caso della
controcultura hacker.

La maggior parte degli odierni hacker fa risalire l’etimologia del termine
‘hacker’ al M.I.T. (Massachusetts Institute of Technology), una delle
università di ricerca più importanti al mondo, dove veniva utilizzata già nei
primi anni ‘50 per indicare una goliardata, uno scherzo solitamente innocuo,
per esempio lasciare una carcassa fuori dalla finestra del dormitorio. In
questa accezione di “hack” era implicito una sorta di divertimento creativo ma
sostanzialmente inoffensivo, a cui si ispirava l’utilizzo al gerundio:
‘hacking’. Nella seconda metà degli anni ‘50, il termine assunse una
connotazione sempre più “ribelle”: fare “hacking” era un modo per dare sfogo a
pensieri e comportamenti repressi dal rigido percorso di studio dell’istituto,
e poteva consistere nell’accedere ingegnosamente alle stanze proibite del
campus o utilizzare il sistema telefonico in maniera non ortodossa per
telefonare gratuitamente (phreaking). I primi ad autodefinirsi ‘computer
hacker’ furono degli studenti appassionati di modellismo ferroviario riuniti
nel Tech Model Railroad Club, per l’esattezza i membri del comitato Signals and
Power, che con hacking intendevano uno stratagemma che ottimizzasse
l’efficienza complessiva del sistema ferroviario del club, insomma un gioco
ozioso in grado di migliorarne le prestazioni. Non ci volle molto perché questi
primi hacker mettessero le mani, abusivamente, sullo TX-0 del campus (un
supercomputer(1) interamente a transistor) così che, sul finire degli anni ‘50,
l’intero comitato Signals and Power si era trasferito nella sala di controllo
del TX-0. Il verbo ‘to hack’ significava ora migliorare le performance del
software utilizzando metodi poco convenzionali rispetto a quelli usati nella
scrittura del software ufficiale. Un hacker era dunque chi riduceva la
complessità del codice sorgente (2)con un ‘hack’ (letteralmente “accettata”).
Il termine indicava anche lo sviluppo di software ludico, come ad esempio,
Spacewar!, il primo videogioco interattivo sviluppato nei primi anni ‘60.
Spacewar! comprendeva tutte le caratteristiche dell’hacking tradizionale: era
altamente innovativo e divertente e finì per diventare il passatempo preferito
di quanti lavoravano ai mainframe (specie di supercomputer) in ogni parte del
mondo, inoltre, questo videogioco, si diffuse piuttosto facilmente tra i
programmatori, visto che gli hacker, avendolo realizzato per pura passione, non
lo facevano pagare o tentavano di limitarne la diffusione. Nella seconda metà
degli anni ‘70, un “computer hacker” era chiunque scrivesse del codice software
solo per il gusto di farlo, specificatamente indicava chi avesse grosse
capacità nell’ambito della programmazione. In questo periodo il termine perse
genericità e venne sempre di più associato al mondo dei computer. La
controcultura hacker cominciò ad assumere sempre più delle connotazioni
“religiose”: per potersi considerare hacker, non era più sufficiente che un
programmatore scrivesse del codice per passione, ma doveva far parte
dell’omonima cultura e onorarne le tradizioni allo stesso modo in cui un
contadino del Medio Evo giurava fedeltà alla corporazione dei vinai. Gli hacker
di istituzioni elitarie come il MIT, Standford e Carnegie Mellon cominciarono a
codificare l’”etica hacker”: le norme che governano il comportamento quotidiano
dell’hacker, di cui parlerà il libro “Hackers. Gli eroi della rivoluzione
informatica” scritto da Steven Levy nel 1984. E’ a partire dai primi anni ‘80
che il termine hacker assume anche una connotazione negativa: i computer erano
oramai alla portata di molti e i programmatori comuni finirono per trovarsi a
stretto contatto con i veri hacker via ARPANET (antenata dell’odierna
INTERNET). In questa promiscuità, secondo alcuni hacker “religiosi”, dei comuni
programmatori strumentalizzarono le filosofie anarchiche tipiche della cultura
hacker di ambiti come quello del MIT, e cominciarono ad essere lesivi:
diffondendo virus, perpetrando attacchi informatici, ecc. Sempre secondo questi
hacker, nel corso di un simile trasferimento di valori andò perduto il tabù
culturale originato al MIT contro ogni comportamento malevolo, doloso. Il
termine “hacker” assunse connotati punk, nichilisti. Quando polizia e
imprenditori iniziarono a far risalire quei crimini a un pugno di programmatori
rinnegati che citavano a propria difesa frasi di comodo tratte dall’etica
hacker, il termine prese ad apparire su quotidiani e riviste in articoli di
taglio negativo(3).

HACKER E CRACKER
----------------
Tutt’ora il termine hacker è utilizzato prevalentemente (quasi esclusivamente
dalla grande stampa) con l’accezione di ‘pirata informatico’. Sentendosi
diffamati, gli hacker religiosi, hanno coniato nuovi termini con l’intenzione
di mettere un po’ di ordine e potersi distinguere dagli utenti informatici
dannosi che spesso vengono confusi con gli hacker “veri”. E’ interessante
constatare, in pertinenza con il nostro studio, come gli hacker aderenti
all’etica hacker considerino “nichilisti” quelli che loro chiamano cracker,
ossia i pirati che si introducono abusivamente in un sistema e lo danneggiano
in nome di idee anarchiche, ma molto più frequentemente, per guadagnare
illegalmente del denaro (spionaggio industriale, frodi, ecc.).

Seguendo l’esempio linguistico di Stirner e volendo rivalutare il termine
“nichilista”, mi piace far rilevare come questo aggettivo non sia quasi mai
usato onestamente nell’accezione corretta di “senza valori”, ma sempre facendo
riferimento a dei sistemi di valori non condivisi. Per esempio Nietzche, grande
debitore di Stirner, considera i cristiani dei nichilisti perché riducono a
nulla il mondo empirico. Allo stesso tempo i cristiani, erroneamente,
considerano Nietzche un nichilista, perché riduce a nulla i valori cristiani.
Ma in realtà Nietzche non porta a compimento la morte di Dio: la distruzione di
tutti i valori, perché vuole esplicitamente soppiantare il Crocifisso con
Dioniso, sostituire dei valori con altri valori. Bene, se usiamo il termine
“nichilista” in maniera corretta, non sarà difficile rendersi conto che neanche
i cracker sono “senza valori” come pretendono gli hacker “religiosi”. I
cracker, più semplicemente, dimostrano di non condividere l’etica hacker.
L’attività del cracker, che danneggi un sistema per assecondare un ideale
anarchico (attacchi contro enti autoritari) o per avidità di denaro (frodi nei
confronti dei normali utenti), morde sempre sul terreno religioso dei valori; e
che ci si prostri dinnanzi ad un ideale di antiautoritarismo o a Mammona (nella
Bibbia, la personificazione delle ricchezze disoneste), quello che rimane
presente è la condizione di servaggio nei confronti di un entità estranea.
Nichilista, senza valori (sopra di sé) è solo l’individuo nel momento della
rivolta, il ribelle che agisce guidato dalla propria passione, dal desiderio di
dispiegarsi. Quindi, se proprio dobbiamo identificare ciò che divide un cracker
dagli hacker della prima ora, i primi rivoltosi della controcultura (che però
non poteva definirsi ancora tale, mancando il momento religioso), non dobbiamo
cercare nell’ambito dell’etica normativa (perchè gli hacker non ne hanno una
sino agli anni ‘70), ma in quello motivazionale: la passione degli hacker
ribelli da una parte, l’ideologia anarchica o il “denaro facile” dei cracker
dall’altra.

Rispondiamo ora ad una domanda che turba da molti anni il mondo
dell’informatica: gli hacker sono “buoni” o “cattivi”? Sono dei pirati
informatici, sì o no?

Risposta: dipende. Gli hacker “religiosi” che si sono sottomessi all’etica
hacker sono costretti ad essere buoni e in un certo senso non possono esserlo,
perché non sono loro ad agire, ma lo spirito dell’etica hacker attraverso di
loro. Diversamente, un hacker “rivoltoso” essendo libero (anche dall’etica
hacker), è caratterizzato da un agire egoistico e dunque non prevedibile, in
quanto motivato dalla propria passione, che è unica. L’hacker rivoltoso
potrebbe sviluppare dei programmi e rilasciarli liberamente, magari senza
intascare un soldo, se per lui condividere è fonte di gioia; come, allo stesso
tempo, potrebbe agire illegalmente forzando qualche serratura virtuale. Ma se
questo accadrà, non sarà di certo per denaro o perché è stato guidato da
qualche ideale anarchico, ma solo per manifestare liberamente se stesso. In
quest’ottica, l’etica hacker, intesa in senso normativo, appare ridicola.
Infatti obbligare chi lo fa già spontaneamente, a scrivere free software o
condividere le proprie conoscenze è assurdo quanto (tornando ad un esempio caro
a Stirner) obbligare un uccello a cantare. Se teniamo presente che la seguente
è una delle definizioni di hacker più apprezzata (dagli stessi hacker)
arriviamo alla conclusione che l’espressione “etica hacker” è una specie di
ossimoro, una contraddizione in termini:

Un hacker è una persona che si impegna nell’affrontare sfide intellettuali per
aggirare o superare creativamente le limitazioni che gli vengono imposte, non
limitatamente ai suoi ambiti d’interesse (che di solito comprendono
l’informatica o l’ingegneria elettronica), ma in tutti gli aspetti della sua
vita.

RELIGIOSITA’ NELLA CONTROCULTURA HACKER
---------------------------------------
ERIC S. RAYMOND – COME DIVENTARE UN HACKER
------------------------------------------
L’aspetto religioso della controcultura hacker raggiunge una delle sue massime
espressioni nel documento “Come diventare un hacker” di Eric S. Raymond, figura
controversa della controcultura hacker e portavoce del movimento open-source.
Il segreto per diventare un hacker, secondo Raymond, consiste nell’emulazione
dei “maestri” hacker, le figure di spicco della controcultura. In pratica, si
tratta di annichilire la propria persona e diventare una sorta di scimmia
ammaestrata che imita in tutto e per tutto lo stile di vita hacker
esemplificato dal maestro:

«the most effective way to become a master is to imitate the mind-set of
masters — not just intellectually but emotionally as well». (Il metodo più
efficace per diventare un maestro è imitare l’impostazione mentale dei maestri
– non solo intellettualmente ma anche emotivamente).

E per rafforzare questa tesi, Raymond, cita a suo favore un moderno poema Zen
di ignota provenienza:

> To follow the path:  
> look to the master,  
> follow the master,  
> walk with the master,  
> see through the master,  
> become the master.  
> 
> Per seguire la via:  
> osserva il maestro,  
> segui il maestro,  
> cammina a fianco del maestro,  
> guarda attraverso gli occhi del maestro,  
> diventa il maestro.

«So, if you want to be a hacker, repeat the following things until you believe them [..] »
Così, se vuoi essere un hacker, ripeti le seguenti cose sino a che non le credi veramente.

1. Il mondo è pieno di problemi affascinanti che aspettano di essere risolti.
2. Nessuno dovrebbe mai risolvere lo stesso problema una seconda volta.
3. La noia e i lavori “da sgobboni” sono un male.
4. La libertà è un bene.
5. L’atteggiamento non sostituisce la competenza.

E’ sorprendente come questi enunciati, che vista la loro “razionalità”
permettono di essere assimilati senza invocare la fede del credente, siano
stati trasformati in “verità rivelate” in cui sforzarsi di credere. Tutti
converranno che non possano essere considerati degli articoli di fede in cui si
può solo credere ciecamente. Come spiegare allora l’esistenza del documento
“Come diventare un hacker”?

Innanzi tutto, non dimentichiamo che legge scritta e religione sono sempre
complementari, infatti l’impulso a legiferare, a sottomettere e sottomettersi
dinnanzi ad una “sacra scrittura” è prerogativa di tutte le religioni in senso
proprio. Cos’è la scrittura se non l’unico modo per dare “forza” e solidificare
le idee? Ma ritorniamo allo scritto di Raymond. E’ lecito chiedersi se una
persona che ha bisogno di autosuggestionarsi, per credere che il mondo è pieno
di problemi affascinanti, sia atta a diventare un hacker (concedendo che lo si
possa diventare attraverso un tutorial(4). Questo sembra importare poco a
Raymond perché il suo obbiettivo è evidentemente quello di fare proseliti e
aumentare il più possibile i membri della controcultura hacker; non si tratta
di una nostra interpretazione, Raymond esplicita i suoi intenti evangelici in
un paragrafo intitolato molto significativamente “Servire la cultura hacker”,
dove l’ultima esortazione è: “Finally, you can serve and propagate the culture
itself” (Finalmente, puoi servire e diffondere la cultura stessa).

Ebbene, il proselitismo, esigenza comune a molte religioni (in senso stretto e
in senso stirneriano), è presente anche nella controcultura hacker, e
costituisce una delle facce del suo momento religioso.

L’altra faccia della medaglia è rappresentata, invece, dall’esigenza di
purezza, e vede il suo più grande esponente nell’hacker Richard Stallman.

RICHARD STALLMAN – GUERRA TERMINOLOGICA
---------------------------------------
Il momento religioso di una controcultura è caratterizzato dalle guerre. Guerre
intestine e contro nemici esterni. Abbiamo visto, analizzando il pensiero di
Stirner, che le guerre e le rivoluzioni si sviluppano in un terreno sociale;
esse sono estranee al rivoltoso che non ha ideali in nome di cui combattere,
sottomettere e sottomettersi, ma sono perpetrate piuttosto dall’uomo religioso,
che, sentendosi portavoce del bene, si arroga il diritto di imporre i propri
ideali usando (nei casi estremi) qualsiasi mezzo, seguendo il motto: il fine
santifica i mezzi. Il massimo ideologo e campione della polemica nel panorama
della controcultura hacker, in constante lotta, non solo con il mondo del
software proprietario, ma anche con gli stessi esponenti di quello libero, si
definisce ateo e porta il nome di Richard Matthew Stallman. Stallman è un degli
hacker più noti, per le sue doti indiscusse di programmatore: ha scritto due
tra i programmi liberi più apprezzati (l’editor Emacs e il compilatore(5) GCC)
ma soprattutto per il suo attivismo ideologico a favore del movimento del
software libero che lo ha portato a fondare la [Free Software Foundation][2]. Nel
1983 Stallman lancia il progetto Gnu (acronimo ricorsivo di Gnu is not
Unix(6)), il cui scopo è la creazione di un sistema interamente libero chiamato
Sistema GNU; per arrivare a questo risultato, all’interno del progetto, vengono
creati programmi per coprire ogni necessità informatica: compilatori, lettori
multimediali, programmi di crittografia, ecc; il sistema GNU però, non dispone
tuttora di un suo kernel(7) utilizzabile e perciò viene solitamente affiancato
dal kernel Linux (di cui parleremo in seguito) dando vita al sistema operativo
GNU/Linux. Che quasi tutti, nonostante le lamentele del nostro hacker
religioso, chiamano più semplicemente Linux. La visione di Stallman riguardo al
software è di tipo manicheo. Per lui esiste solo il Male: il software
proprietario, e il Bene: il software libero.

L’ideale a cui Stallman vuole sacrificare tutto è dunque la libertà del
software. Badate bene, asserire che Stallman sia un sostenitore del software
libero è quantomeno un eufemismo poiché arriva a propugnare una completa
abolizione del software proprietario, in quanto (parole sue) “crimine contro
l’umanità”. Nel caso sfuggisse a chi sta leggendo, il software libero è
qualsiasi software rilasciato con una licenza che permetta a chiunque di
utilizzarlo e che ne incoraggi lo studio, le modifiche e la ridistribuzione, e
si contrappone al software proprietario che ha invece le caratteristiche
opposte. Attenzione, perché l’aggettivo “libero” (riferito al software) non
indica assolutamente che il software in questione possa essere utilizzato in
maniera indiscriminata, anzi, la licenza GPL (General Public License, scritta
dallo stesso Stallman) sotto la quale viene distribuito buona parte del
software libero, è una delle più restrittive in assoluto. Eccola riassunta
brevemente:

Il testo della GNU GPL è disponibile per chiunque riceva una copia di un
software coperto da questa licenza. I licenziatari (da qui in poi indicati come
“utenti”) che accettano le sue condizioni hanno la possibilità di modificare il
software, di copiarlo e ridistribuirlo con o senza modifiche, sia gratuitamente
sia a pagamento. Quest’ultimo punto distingue la GNU GPL dalle licenze che
proibiscono la ridistribuzione commerciale.

Se l’utente distribuisce copie del software, deve rendere disponibile il codice
sorgente a ogni acquirente, incluse tutte le modifiche eventualmente effettuate
(questa caratteristica è detta copyleft). Nella pratica, i programmi sotto GNU
GPL vengono spesso distribuiti allegando il loro codice sorgente, anche se la
licenza non lo richiede. Ci sono casi in cui viene distribuito solo il codice
sorgente, lasciando all’utente il compito di compilarlo.

L’utente è tenuto a rendere disponibile il codice sorgente solo alle persone
che hanno ricevuto da lui la copia del programma o, in alternativa,
accompagnare il software con una offerta scritta di rendere disponibile il
sorgente su richiesta e per il solo costo della copia. Questo significa, ad
esempio, che è possibile creare versioni private di un software sotto GNU GPL,
a patto che tale versione non venga distribuita a qualcun altro. Questo accade
quando l’utente crea delle modifiche private al software ma non lo
distribuisce: in questo caso non è tenuto a rendere pubbliche le modifiche.

Dato che il software è protetto da copyright, l’utente non ha altro diritto di
modifica o ridistribuzione al di fuori dalle condizioni di copyleft. In ogni
caso, l’utente deve accettare i termini della GNU GPL solo se desidera
esercitare diritti normalmente non contemplati dalla legge sul copyright, come
la ridistribuzione. Al contrario, se qualcuno distribuisce un software (in
particolare, versioni modificate) senza rendere disponibile il codice sorgente
o violando in altro modo la licenza, può essere denunciato dall’autore
originale secondo le stesse leggi sul copyright.

Rimandando di qualche pagina alcune considerazioni sulla suddetta licenza,
ritorniamo a parlare di Stallman e segnaliamo, ancora una volta, il legame
indissolubile tra parola (che diventa legge) e religiosità. Osserviamo, più da
vicino, la cavillosità terminologica di Stallman che non ha eguali nella
controcultura hacker. Innanzi tutto, uno dei suoi criteri per concedere
un’intervista ad un giornalista è che quest’ultimo accetti di usare la sua
terminologia dall’inizio alla fine dell’articolo.

In alcuni casi è arrivato a chiedere ai giornalisti di leggere parti della
“filosofia” GNU prima di un’ intervista, per “motivi di efficienza” e a
rifiutare un’intervista per mancanza di adesione terminologica
dell’intervistatore. Stallman è soprattutto attento alle parole quando si parla
del rapporto tra software e libertà. Instancabilmente chiede alle persone che
parlano di Linux di utilizzare il termine “GNU/Linux” (che pronuncia “GNU Slash
Linux), perché la sola parola ‘Linux’, a suo dire, nuoce alla sostenibilità del
movimento del software libero rompendo il collegamento tra il software e la
“filosofia” del software libero del progetto GNU. Dal 2003 circa, ha cominciato
ad usare anche il termine “GNU+Linux” che pronuncia “GNU plus Linux”, mettendo
in evidenza che Linux è solo una variante di GNU, e che il progetto GNU è il
suo principale sviluppatore.

Inoltre, a suo cospetto, bisogna evitare il termine “proprietà intellettuale”,
poiché il termine “Proprietà Intellettuale” è stato ideato per confondere le
persone, e viene usato per evitare una discussione intelligente sulle
specifiche di copyright, brevetti, e leggi sul marchio, trattando senza
distinzione aree di leggi che sono più dissimili che simili. Prevedibilmente,
la sua pedanteria verbale, e i suoi continui sforzi per convincere la gente
dell’importanza della terminologia sono fonte di regolare incomprensione e
contrasto non solo con i non addetti ai lavori, ma come dicevamo prima, anche
con gli stessi hacker. Ad esempio: Stallman è categoricamente contrario
all’utilizzo del termine open-source software (utilizzato da Raymond) per
indicare il software libero perché non fa venire in mente ciò che Stallman
considera come valore del software: la libertà.

Open-source significa solamente “sorgente aperto” e non fa riferimento alla
libertà, e di conseguenza, a suo giudizio, non porterà le persone a dare valore
e difendere la propria libertà. Un’ alternativa che Stallman accetta è
“unfettered software” (software senza restrizioni), comunque, “free software” è
il termine che preferisce di gran lunga. Per analogia sostiene il termine
“software proprietario” piuttosto che “closed source software”(sorgente chiuso)
quando ci si riferisce ad un software che non è libero. A Stallman piace
mettere in evidenza come il termine libero non sia da intendersi come gratuito
(ambiguità in cui si incorre nella lingua inglese utilizzando l’aggettivo free
che significa anche “gratuito”) e precisa: libero come il pensiero, non gratis
come una birra offerta da un amico”.

Se guardiamo bene, questa scontro terminologico (Free versus Open-source), non
è altro che una guerra santa che vede l’esigenza di purezza di Stallman
scontrarsi con gli intenti evangelici di Raymond. Fu proprio Raymond che nel
1998 scrisse, assieme ad altre personalità del free software, la Open Source
Definition, il documento fondamentale del movimento Open source. Come abbiamo
già visto, la fissazione di Raymond è la diffusione della controcultura hacker
che a suo modo di vedere trova la sua massima espressione nel movimento
open-source. Raymond è un vero evangelista, ed è chiaro come il suo tentativo
di diffondere il verbo hacker fino a farlo penetrare nel mainstream venga
ostacolato dall’eccessiva pretesa di purezza dell’ideologia di Stallman.
Opensource è dunque un termine “astuto” che nasce per amicarsi il mondo
extra-hacker, e sostituire l’ambiguo ”free”, aggettivo che è guardato con
troppo sospetto dal mercato e da chi non è un hacker.

LE GUERRE DEL SOFTWARE
----------------------
![Software Wars]({filename}/images/max-stirner-e-la-controcultura-hacker.png
"Software Wars")

L’immagine raffigura schematicamente i fronti della principale guerra santa del
software. Si tratta di una guerra nota (grossomodo) anche ai non specialisti
del settore informatico e vede una serie di società e movimenti che lottano per
sconfiggere l’impero del Male, quello di Microsoft, il cui imperatore è uno
degli uomini più ricchi del mondo: Bill Gates. Forse avrete notato che alla
foto di Gates è stata aggiunta una coda da diavolo; ebbene, questo è un esempio
di come il pensiero religioso lavori similmente ai computer, utilizzando un
sistema binario: 0 e 1, bianco e nero, bene e male. In questo quadro Microsoft
è chiaramente il Male. Secondo alcuni detrattori di Microsoft, Bill Gates
sarebbe addirittura l’incarnazione dell’anticristo. Per supportare questa tesi
si servono di una lunga serie di argomentazioni tra cui una “fantasiosa”
interpretazione del seguente passo dell’Apocalisse di Giovanni: «E lui obbligò
tutti, piccoli e grandi, ricchi e poveri, liberi e schiavi a ricevere un
marchio nella loro mano destra o nella loro fronte, in modo tale che nessuno
poté né comprare né vendere senza il marchio che è il nome della bestia od il
numero del suo nome. […] Questo numero è 666″» Apocalisse 13:16-18

A tal proposito, uno di questi esegeti ci invita a riflettere: «Sai che il vero
nome di Bill Gates è William Henry III? Oggi lo si conosce come Bill Gates
(III), dove III sta per “terzo”. Allora, cosa c’è di agghiacciante in questo
nome? Se prendi tutte le lettere del nome Bill Gates III e ottieni il
corrispondente codice ASCII (American Standard Code for Information
Interchange) di ogni lettera e le sommi tutte… otterrai il numero 666 che è il
numero della bestia.[…]La Bibbia dice che senza il segno della bestia non si
potrà vendere, comprare, né fare transazioni commerciali, ma non è forse vero
che Microsoft avrà presto il monopolio totale dell’informatica? E ancora:
presto usare Internet sarà una necessità per il business. Internet la si
conosce anche come World Wide Web (ragnatela mondiale), e si scrive WWW: ma
un’altra forma di scrivere W è VI, così che W W W è VI VI VI, cioè 6 6 6.
Ultima cosa: l’Apocalisse dice che il marchio della bestia si porterà sulla
mano e sulla fronte: le parole mouse e schermo vi dicono niente?».

Verrebbe da pensare che la guerra del software, schematizzata nell’immagine, si
risolva esclusivamente in una grande guerra in cui tutte le “forze” del
software sono alleate contro il nemico Microsoft, l’impero del Male. La realtà
è molto più complessa e impossibile da trattare esaurientemente in questa sede.
Mi limiterò a segnalare alcune guerre fratricide che vedono combattere hacker
contro altri hacker. Una prima guerra “civile”, interna alla controcultura
hacker, l’abbiamo accennata poco fa parlando della polemica terminologica tra
Stallman e Raymond, scatenata da due modi diversi di concepire la “religione”
del software libero. Da una parte l’intransigenza di Stallman e la sua ricerca
di una purezza cristallina, ottenibile solo con l’estirpazione totale del
software proprietario; dall’altra la flessibilità di Raymond, pronto a scendere
a compromessi a scapito della purezza pur di guadagnarsi sempre nuovi fedeli. A
proposito di questo scontro interno, mi sembra di cogliere una situazione
analoga, con le debite proporzioni, a quella in cui si vennero a trovare i
primi cristiani con la conversione al cristianesimo di una parte dei farisei. I
farisei (similmente a Stallman) volevano preservare la purezza delle leggi di
Mosè e obbligare alla circoncisione, e ad altri rituali, i pagani convertiti al
cristianesimo, ma Paolo (come Raymond) si oppose a questa volontà in nome di
una “salvezza” accessibile a tutti: «al giudeo restando giudeo, al gentile
restando gentile, al barbaro restando barbaro, al greco restando greco». Dopo
il concilio di Gerusalemme si giunse ad una specie di compromesso in cui si
stabilì che Pietro avrebbe predicato il Vangelo solo ai cristiani circoncisi
mentre Paolo a tutti gli altri. Tuttavia il problema venne superato solo in
parte, perché, di fatto, una divisione permase, e ne troviamo traccia nella
maggior parte delle Lettere di San Paolo, nelle quali risalta la sua continua
lotta contro le problematiche create nelle Chiese dai credenti giudaizzanti.
Questa “doppia anima” divisa tra proselitismo e purezza è presente anche
all’interno della controcultura hacker, ed è riscontrabile a qualsiasi livello
“gerarchico” del movimento; dai grandi “maestri” hacker ai semplici
utilizzatori di Linux che si uniscono nei Linux User Group (8) di tutto il
mondo.

LINUX VS BSD (GPL VS BSD)
-------------------------
Linux contro Bsd (Berkeley Software Distribution) è uno scontro che concerne,
in realtà, più le licenze con le quali viene distribuito il software, che il
software vero e proprio. In questa guerra troviamo schierati i fedeli dei
sistemi operativi Linux contro gli utilizzatori delle distribuzioni Bsd.

Dal punto di vista tecnico, Linux e Bsd, sono piuttosto simili, essendo
derivati entrambi dal sistema operativo Unix. Bsd è la variante originaria di
Unix sviluppata presso l’università di Berkley; di come nacque Linux parleremo
più avanti. Cosa differenzia questi sistemi? Molto sinteticamente: Linux è
superiore sotto l’aspetto della compatibilità hardware e della quantità di
tutorial disponibili, conseguenze di una comunità più estesa e “religiosa”;
mentre Bsd ha, generalmente, la meglio sul piano della solidità e sicurezza
informatica; OpenBSD, uno dei sistemi Bsd più apprezzati, è reputato, “out of
the box(9)”, uno dei sistemi operativi meno vulnerabili in assoluto. Ciò che
divide maggiormente i due sistemi operativi è “la filosofia di base”, incarnata
dalla rispettiva licenza di distribuzione. Linux viene distribuito con la
licenza GPL mentre Bsd sotto l’omonima licenza BSD.

Avrete constatato come agli ideologi informatici (anche a quelli molto attenti
all’uso delle parole) piaccia condire i loro discorsi con la parola filosofia
quando sarebbe invece più consono utilizzare il termine ideologia. Ad ogni
modo, le caratteristiche comuni alle due licenze in questione si possono
riassumere nelle quattro libertà (teorizzate da Stallman) che ogni software,
per essere definito libero, deve poter garantire:

- Libertà di eseguire il programma per qualsiasi scopo (chiamata “libertà 0”)
- Libertà di studiare il programma e modificarlo (“libertà 1”)
- Libertà di copiare il programma in modo da aiutare il prossimo (“libertà 2”)
- Libertà di migliorare il programma e di distribuirne pubblicamente i
  miglioramenti, in modo tale che tutta la comunità ne tragga beneficio
  (“libertà 3”)

Entrambe le licenze garantiscono queste quattro libertà, ciò che le diversifica
è l’interpretazione dell’ultimo punto (libertà 3), che nella licenza GPL si
traduce in un comandamento:

- Distribuisci pubblicamente i miglioramenti, in modo tale che tutta la
  comunità ne tragga beneficio!

Secondo questa legge: io, come programmatore, sono costretto a cedere la mia
libertà al programma, posso sì modificarlo a mio piacimento però se voglio
distribuirlo sono costretto ad essere altruista, ossia a rilasciare le
modifiche che ho apportato in modo che tutti possano vederle.

Ne consegue che la libertà promossa dalla licenza GPL non è assolutamente
quella dell’utente nei confronti del software ma proprio il contrario: il
software è libero, ma io non lo sono. Aggiungiamo che, oltre all’imposizione di
consegnare le modifiche apportate, la GPL mi obbliga a rilasciare il programma
da me modificato sotto la stessa licenza, il che la fa assomigliare ad una
sorta di virus dell’altruismo che infetta qualsiasi cosa tocchi. In
un’intervista al Chicago Sun Times, Steve Ballmer, CEO di Microsoft, ha
dichiarato: “Linux è un cancro che si attacca, nel senso della proprietà
intellettuale, a tutto ciò che tocca, questo è il modo con cui la licenza (GPL)
opera”.

Sotto questo aspetto la licenza BSD è invece molto più vicina alla libertà dal
software dei primi hacker, che, non ossessionati dal destino di ciò che avevano
creato solo per passione, potevano, stirnerianamente parlando, essere
considerati degli egoisti. Infatti un software rilasciato sotto licenza BSD può
essere modificato e ridistribuito utilizzando la stessa licenza oppure un’altra
licenza qualsiasi senza l’obbligo di ridistribuire le modifiche apportate al
codice sorgente. A differenza della GPL, questa licenza piace molto a Microsoft
e ne ha sfruttato a pieno la liberalità migliorando Windows NT e i successivi
sistemi operativi (closed-source) grazie all’implementazione di una massiccia
porzione di codice tratto dal sistema operativo Bsd. Ora, colgo lo spunto per
lanciare una provocazione ai filantropi informatici, che sono molto spesso
utenti religiosi di Linux: se amate il vostro prossimo dovreste gioire di
quello che chiamate “furto di codice” permesso dalla licenza BSD, perché i
sistemi operativi Microsoft sono installati nella stragrande maggioranza dei
computer di tutto il mondo e di conseguenza la qualità della vita informatica
della maggioranza delle persone è migliorata. Perchè ora, grazie a Bsd e alla
sua licenza, c’è un po’ di Unix anche dentro Windows. O forse aveva ragione
Stirner, e non amate il vostro prossimo come credete, ma solamente il
“linuxiano” che c’è in lui?

LINUX VS LINUX
--------------
Volete scatenare una guerra santa? Provate a chiedere ingenuamente in qualsiasi
spazio virtuale dedicato a Linux (newsgroup, forum, blog, chat o mailing list)
qual è la distribuzione Linux migliore. E sì, perché di sistemi operativi
basati sul kernel Linux ce ne sono molti e vengono chiamate appunto
distribuzioni. Le differenze tra distribuzioni Linux sono di carattere tecnico,
organizzativo e “filosofico”. Quelle di carattere tecnico consistono in un
diverso supporto dell’hardware e nella configurazione del software. Quelle di
carattere organizzativo sono spesso motivate da scelte tecniche ma anche da
ragioni storiche o, come dicevamo, “filosofiche”. La prima considerazione da
fare, per capire come una semplice domanda su un sistema operativo possa essere
fonte di tanti attriti, riguarda l’orientamento assolutistico proprio di una
buona parte del mondo Linux che vive “religiosamente” il sistema operativo. E
qual è il senso profondo della religione se non la ricerca dell’Assoluto che si
pone al di sopra del permanente, del relativo, e perciò del singolo? Ritornando
alla domanda iniziale (qual è la distribuzione Linux migliore?), pochi
“linuxiani” risponderanno informandosi preventivamente sulle esigenze
dell’utente che l’ha posta; pochi chiederanno “come utilizzerai il sistema
operativo?”. Questa osservazione è ancora più pertinente quando viene chiesto
un confronto tra Linux ed un altro sistema operativo. Quel “migliore”,
difficilmente, viene inteso in relazione alle esigenze del singolo
utilizzatore, ma viene invece interpretato, in maniera assoluta, su un piano
ideale e astratto. In questi frangenti emergono le “due anime” della
controcultura hacker di cui parlavamo prima. Gli utenti più “puri”
risponderanno “Debian!”, perché Debian è attualmente una delle poche
distribuzioni a contenere nativamente solo software non proprietario. Non a
caso, per Debian, Stallman ha scritto vrms (Richard Matthew Stallman Virtuale),
tool paragonabile ad un esorcista virtuale atto a purificare il computer dal
software proprietario. Vrms analizza i programmi installati e comunica
all’utente quali sono i programmi non liberi, per alcuni software poi, espone
anche le ragioni per cui quel determinato pacchetto non può considerarsi
libero. Una curiosità: Stallman sarebbe stato in disaccordo con parti della
definizione di software libero fornita da Debian. Ad essere precisi, i
debianisti (si fanno chiamare così gli utenti di Debian), pur profondamente
convinti che la loro distribuzione sia la migliore in assoluto, non la
consigliano agli utenti alle prime armi, con la scusante che non è
semplicissima da configurare. Questo consiglio, piuttosto che dimostrare la
loro filantropia nei confronti dei novizi (che chiamano spesso,
sprezzantemente, “utonti”), serve ad alimentare invece il loro orgoglio, che
viene nutrito dal sapersi utilizzatori di un sistema operativo complesso,
destinato a solo pochi eletti (amano definirsi “duri e puri”). Il debianista
convinto raccomanderà dunque all’utente novello di farsi le ossa con le altre
distribuzioni (considerate inferiori sul piano tecnico e morale) per poter fare
in seguito il “grande salto” di qualità verso Debian. L’altra anima del mondo
Linux risponde alla complessità e purezza morale di Debian con l’immediatezza e
“tolleranza di costumi”. Ubuntu, per esempio, distribuzione derivata da Debian
e finanziata da Canonical Ltd, è il tipico esempio di software che si focalizza
sulla semplicità di utilizzo (anche per questo, attualmente, è la distribuzione
Linux più diffusa al mondo). Ubuntu, come recita l’home page del suo sito, è
una parola africana che significa “umanità verso gli altri”(gli utenti di
Debian sostengono, scherzosamente, che significhi “non so installare Debian”),
dunque si proclama esplicitamente come distribuzione filantropa. Il suo slogan
è “Linux per essere umani”: il desiderio di condividere un software libero,
semplice e completo con il maggior numero di persone, anche quelle che non sono
esperte con il computer”. Ubuntu, secondo molti utenti Linux, è proprio una
distribuzione completa che dimostra una notevole compatibilità hardware ed una
immediatezza impressionante. Però, una critica che le si può muovere contro
riguarda l’eccessiva accondiscendenza verso le novità software che non collima
molto con la stabilità e la sicurezza del sistema operativo (non essendo state
testate a sufficienza). Questa sviluppata apertura ad accogliere immediatamente
ciò che è nuovo serve proprio ad accattivarsi la simpatia di chi non è ancora
esperto di computer ed è quindi poco interessato alla maturità del software,
preferendo, sempre e in ogni caso, la novità. D’altronde, l’opera di
proselitismo, non può essere efficace se non si assecondano, nei limiti del
possibile, i desideri degli utenti comuni che, non vivendo religiosamente il
sistema operativo, non vedono neanche la necessità di privarsi di un software
solo perché non è libero. Non a caso, alcuni software di Ubuntu, compresi
alcuni moduli(10) del kernel, sono impudentemente proprietari. Inutile dire che
questa indulgenza verso il nemico è vista, dai puristi Linux, come blasfemia e
viatico contro lo Spirito del Software Libero.

Termino questo argomento aggiungendo che lo spirito da crociata non si limita
ai soli sistemi operativi. Esistono moltissime micro-guerre all’interno del
mondo Linux; si potrebbe dire: una per ogni genere di programma. Ad esempio,
nell’ambito dei desktop environment(11), si combatte la guerra tra KDE e Gnome;
di queste guerre non parlerò oltre visto che le cause scatenanti sono affini a
quelle già trattate, e imputabili, come già constatato, ad un approccio
religioso nei confronti del software, in cui l’individuo è al servizio dei
programmi, e non, come ci si potrebbe aspettare, il contrario.

E Linus Torvalds, l’autore della prima versione del kernel Linux, quale
distribuzione preferisce?

La risposta è nello stralcio di una recente intervista che riportiamo qui di
seguito: «In realtà non mi interesso più di tanto alle distribuzioni, fintanto
che siano facili da installare e da tenere ragionevolmente aggiornate. Mi curo
del kernel e di alcune applicazioni, delle quali in realtà mi interessa solo un
piccolo numero. Quando si tratta di distribuzioni, una delle principali
questioni per me è sempre stata la facilità d’installazione. Sono un tipo
tecnico, ma ho un’area di interesse molto specifica, e non voglio occuparmi del
resto. Le sole distribuzioni che io abbia esplicitamente evitato sono quelle
conosciute per essere “troppo tecniche” – come quelle che ti incoraggiano a
compilare i programmi da te ecc. Certo, sono in grado di farlo, ma quello per
me annulla in qualche modo tutto il senso di avere una distribuzione, quindi mi
piacciono quelle famose per essere semplici da usare. Non ho mai usato Debian
pura, per esempio, ma mi piace Ubuntu».

Questa intervista ha probabilmente deluso quei puristi che si aspettavano che
Linus, il guru dei guru di Linux, preferisse le distribuzioni difficili a
quelli amichevoli, convinti come sono che le cose facili da usare siano solo
per gli incapaci, per gli “utonti”. Con queste sconcertanti (per i puristi)
considerazioni del ribelle Torvalds, chiudiamo l’esposizione del momento
religioso della controcultura hacker e cominciamo a parlare proprio di hacker
rivoltosi. Faccio presente che l’analisi sinora svolta, per motivi didascalici,
ha voluto consapevolmente prendere in considerazione degli atteggiamenti
estremi (che sono, comunque, piuttosto diffusi). La realtà cidimostra che
esistono anche molti utenti Linux che si divertono a sperimentare vari sistemi
operativi e non biasimano o umiliano qualcuno solo perché utilizza un sistema
operativo “chiuso” o una distribuzione Linux che è scesa a compromessi
tecnico-ideologici. Insomma: esistono anche linuxiani che usano Linux, e che
non si fanno usare da Linux.

LA RIVOLTA NELLA CONTROCULTURA HACKER
-------------------------------------
LINUS TORVALDS – RIVOLUZIONARIO PER CASO
----------------------------------------
Rivoluzionario per caso – come ho creato Linux (solo per divertirmi) è il
titolo dell’autobiografia di Torvalds, e merita un’attenzione particolare
nell’ambito del nostro studio. Sia trattando l’unico di Stirner, che il libro
sulle controculture di Goffman, avevamo concluso che il rivoltoso, colui che
semina involontariamente il terreno per la nascita della controcultura, non si
prefigge il fine di cambiare il mondo, ma cerca piuttosto di esprimersi
liberamente instaurando un processo che può modificare il contesto circostante.
Al contrario, il rivoluzionario si batte per cambiare l’ordine delle cose,
avendo come fine la realizzazione di un ideale. Ora, se teniamo come valide
queste definizioni, ne risulta che Richard Stallman è un rivoluzionario e non
un rivoltoso.

Stallman non è uno che si “innalza” e procede per la sua strada incurante dei
valori che non condivide. Egli agisce invece sul piano politico-sociale
cercando di cambiare i valori vigenti e realizzare l’ideale di libertà del
software . Chiaramente anche in Stallman è presente la passione dell’hacker, ma
nel suo caso si è messa al servizio di un’idea, diventandone succube. Lo stesso
discorso può essere valido per Raymond e molti altri esponenti della
controcultura hacker, ma soprattutto per quelli che vengono considerati i
grandi traditori dello “spirito” di questa cultura, che lo hanno smesso di
servire per prendere ordini dal denaro. (Nessuno può servire due padroni;
perché o odierà l’uno e amerà l’altro, o avrà riguardo per l’uno e disprezzo
per l’altro. Voi non potete servire Dio e Mammona. Matteo 6,24). Ad esempio,
l’amore per la conoscenza informatica di Bill Gates quando scrisse il MITS 4K
BASIC, il primo linguaggio di programmazione per il primo vero personal
computer (il MITS Altair 8800), doveva essere almeno pari a quello di Torvalds
quando iniziò a sviluppare Linux. Ma Gates, come ci racconta la storia
dell’informatica e dell’economia mondiale, elevò il denaro sopra di sé,
facendolo diventare, come per molti “ex hacker” la stella polare del suo agire.
Abbiamo visto con Stirner che l’ideale, per definizione, non appartiene mai
all’individuo, ma bensì il contrario. L’individuo, quando si trova nella
dimensione religiosa dell’ideale, appartiene ad esso e lo serve affinché questa
idea santa, in quanto l’individuo la riconosce superiore a sé, si possa
realizzare. Nell’essere sottomessi alla libertà o al denaro non vi è alcuna
differenza dal punto di vista esistenziale, entrambe le condizioni sono
alienanti e caratterizzate dall’asservimento del soggetto ad un oggetto. Così
si pronuncia esplicitamente Stirner: «L’oggetto ci trasforma in ossessi, nella
sua forma sacra non meno che in quella profana, come oggetto soprasensibile non
meno che come oggetto sensibile. Desiderio e mania hanno di mira sia l’una sia
l’altra cosa: l’avidità di denaro e la nostalgia del paradiso sono allo stesso
livello».

Tornando a Linus Torvalds e al titolo della sua autobiografia. Cos’è un
rivoluzionario “per caso” se non un rivoltoso? “Per caso” sottintende proprio
la mancanza di un fine “elevato”, di una pianificazione e perciò indica un
piano dell’agire sgombero da ideali (l’ideale configura la vita come
“missione”). Un rivoltoso non è un rivoluzionario, anche se può apparire tale
allo sguardo dell’uomo “religioso” poiché a quest’ultimo è difficile
comprendere un agire, soprattutto se di carattere altruistico, svincolato da
ideali. Torvalds usa il termine “entertainment” (divertimento) per descrivere
il tema della sua attività. Fare qualcosa, e può anche essere qualcosa di molto
impegnativo e “altruistico”, solo per divertimento è la caratteristica
dell’hacker nel momento della rivolta. “Per divertimento” o “per passione” è
una motivazione che connota l’hobbysta, che fa qualcosa perché gli piace farlo
(come l’uccello canta sul ramo) e non perché “deve farlo”. Lo stesso Torvalds
ammette: «Linux è stato decisamente un hobby (ma uno di quelli seri, i
migliori) per me: non ci faccio soldi e non fa nemmeno parte dei miei studi
universitari. L’ho fatto tutto nel mio tempo libero e con la mia macchina».
Torvalds non aveva quindi nessuna intenzione di rivoluzionare il mondo
dell’informatica con la sua invenzione e tantomeno di diventare un paladino
della libertà dopo averlo reso disponibile sotto licenza GPL, e spiega: «A
differenza di molti estremisti della GPL, che ritengono che ogni nuova
innovazione software dovrebbe essere aperta a tutti con una General Public
License, io credo che dovrebbe essere il singolo inventore a poter decidere
cosa fare della propria invenzione». Inizialmente Torvalds non pensava nemmeno
che Linux sarebbe uscito dal suo computer, ciò accadde perché assecondò le
richieste di altri hacker che erano interessati al suo programma. Questa
attenzione ravvivò la passione di Torvalds, che vide nella condivisione del suo
software uno stimolo per continuare a svilupparlo. A parte “la passione” (o
divertimento), motivazione che sta indubbiamente alla base dell’hacking
autentico, Torvalds ci aiuta a capire anche altri aspetti che spiegano, in
maniera più concreta, l’altruismo hacker: «Gli hacker dell’open source non sono
i corrispettivi hi-tech di Madre Teresa di Calcutta. I loro nomi vengono
associati ai contributi che forniscono, nelle credit list o negli history file
allegati a ciascun progetto. Gli sviluppatori più prolifici attraggono
l’attenzione delle aziende che analizzano il codice sperando di trovare – e in
seguito assumere – i migliori programmatori. Gli hacker sono motivati in larga
parte anche dalla stima che possono guadagnare agli occhi dei loro colleghi
grazie ai propri contributi. È un fattore motivazionale importante. Tutti
vogliono fare bella figura con i propri colleghi, migliorare la propria
reputazione, elevare il proprio status sociale. Lo sviluppo open source dà ai
programmatori questa possibilità».

LINUX, FIGLIO DELLA RIVOLTA
---------------------------
La rivolta di Linus Torvalds è legata principalmente al superamento di due
“valori” in fase di consolidamento nel periodo in cui iniziò a sviluppare
Linux. Il suo primo gesto “dissacrante”riguarda la scelta di un approccio
monolitico per il kernel Linux, considerato da alcuni illustri accademici del
tempo come obsoleto. La seconda dissacrazione riguarda invece la preferenza per
un modello di sviluppo (definito più tardi da Raymond: “a Bazaar”) ritenuto
sino ad allora, dagli esperti del settore, inadeguato per progetti di una certa
complessità. Tutto cominciò con la lettura del libro “Sistemi operativi:
progettazione e implementazione” di Andrew S. Tanenbaum, noto come uno dei
testi “sacri” dell’informatica. Leggendo il capitolo sul sistema operativo
Unix, Torvalds cominciò ad essere attratto dalla sua potenza e semplicità: «A
mano a mano che leggevo e iniziavo a capire Unix, sentivo l’entusiasmo che mi
cresceva dentro. E sinceramente non se n’è mai più andato». Unix è un sistema
operativo inizialmente sviluppato da un gruppo di ricerca dei laboratori AT&T e
Bell Labs che fu distribuito gratuitamente alle università di tutto il mondo
affinché potessero utilizzarlo per scopi didattici. Non si trattò di un caso di
beneficenza: AT&T, essendo monopolista delle telecomunicazioni americane, per
legge, poteva offrire al mercato un range di servizi limitato a queste e Unix
non fu reputato un business pertinente. Così, AT&T, non potendoci guadagnare
nulla in termini economici, decise di “regalare” Unix alle università per fini
di studio. A quel tempo (1991) non esisteva una versione di Unix per i pc/386
domestici, così Torvalds, spinto dalla passione, decise di acquistare e
installare nel suo computer nuovo: Minix, una versione di Unix per 386 scritta
da Tanenbaum, lo stesso autore del libro che lo stava entusiasmando. Torvalds
non era però completamente soddisfatto di Minix: «c’era una serie di funzioni
deludenti in Minix. Quella peggiore era l’emulazione di terminale, che era
importante perché era il programma che usavo per collegarmi al computer
dell’università»; Inizialmente, Linus, pensò di migliorare Minix utilizzando
delle patch(12) scritte da Bruce Evans, un hacker australiano, che rendevano
Minix molto più efficiente. Per applicare le patch bisognava però, per
questioni di licenza, comprare la versione ufficiale di Minix e poi fare un
sacco di lavoro per inserire le patch di Evans. . Era una faticaccia. Frustrato
dalla situazione, Torvalds, abbandonò il proposito di migliorare Minix e decise
di scrivere un suo emulatore di terminale. Questo emulatore di terminale
cresceva di giorno in giorno anche grazie alla richieste e alle correzioni di
altri hacker che dialogavano con Torvalds attraverso il newsgroup dedicato a
Minix. Una “provvida sventura” (per gli amanti di Linux) capitò il giorno in
cui Torvalds cancellò per sbaglio Minix dal suo PC: invece di reinstallarlo,
decise di imboccare una strada propria e rendersi autonomo scrivendo un sistema
operativo partendo da zero. Ed è a questo punto che avviene il “parricidio” nei
confronti di Tanenbaum, autore del libro che, a suo dire, gli cambiò la vita.
Invece di adottare per Linux un approccio microkernel, come aveva fatto
Tanenbaum per Minix, Torvalds optò per un kernel monolitico, che Tanebuam
definì poco dopo un grande passo indietro verso gli anni 70. Torvalds
giustifica nel seguente modo la scelta di un kernel monolitico a scapito di un
microkernel: «La teoria alla base del microkernel parte dall’idea che i sistemi
operativi sono complessi. Per cui si cerca di eliminare parte della complessità
con una forte modularizzazione. Il principio dell’approccio microkernel è che
il kernel – che si trova alla base della base della base – dovrebbe fare il
meno possibile. La sua funzione principale è comunicare. Tutte le altre cose
che un computer offre sono servizi disponibili tramite i canali di
comunicazione microkernel. Nell’approccio microkernel lo spazio dei problemi va
suddiviso al punto che nessuna sua parte sia complessa. Io lo trovavo un
approccio stupido. É vero, semplifica ogni singola componente. Ma le
interazioni lo rendono molto più complesso di quanto potrebbe essere se molti
servizi fossero inclusi nel kernel stesso, come avviene in Linux». L’approccio
microkernel è dunque teoricamente (idealmente) migliore e più sicuro, ma, nella
realtà, si dimostra generalmente poco performante nel gestire molti servizi
assieme, a causa dei caratteristici tempi di latenza. L’orientamento pragmatico
è stata una delle carte vincenti di Torvalds. Un microkernel è certamente
migliore nelle operazioni “mission cirtical”, se usato in situazioni in cui al
software non è assolutamente concesso di sbagliare, ad esempio nei bracci
robotici di uno space shuttle o in macchine che lavorano il vetro, dove un
errore anche piccolo può costare centinaia di migliaia di euro. I kernel
monolitici tendono invece ad essere più semplici da progettare correttamente, e
possono quindi evolversi più rapidamente di un sistema basato su microkernel:
senza queste caratteristiche, Linux non sarebbe diventato quello che è oggi.

Per quanto riguarda invece la rivolta inerente il modello di sviluppo adottato
da Torvalds, si può riassumere in quella che Raymond (grande ammiratore di
Torvalds) definisce “legge di Linus”, illustrata nel manifesto open-source: “La
cattedrale e il bazaar”. La “legge di Linus”, secondo la quale: “dato un numero
sufficiente di occhi, tutti i bug vengono a galla” è una delle chiavi del
successo del progetto del kernel Linux. Prima di Linux si riteneva che ogni
progetto di una certa complessità avesse bisogno di essere adeguatamente
gestito e coordinato da una piccola gerarchia, come avviene per la costruzione
di una cattedrale. Altrimenti si stimava che il progetto sarebbe collassato
sotto il peso di moltissime revisioni e modifiche tra di loro incompatibili
perchè prodotte da troppe persone diverse. Torvalds non scelse questo approccio
tradizionale, ma decise che tutti potessero contribuire allo sviluppo di Linux,
facendolo così diventare il più grande progetto collaborativo senza fini di
lucro, che la storia conosca. Questo sistema di sviluppo, che fa pensare
all’aspetto caotico di un bazar, si è dimostrato talmente efficace, che col
crescere del numero di sviluppatori, non solo il progetto non è collassato, ma
la qualità e l’affidabilità del software è addirittura migliorata. Bisogna
riconoscere che l’apertura alle modifiche di Linux è anche garantita
dall’utilizzo della licenza GPL di Stallman, che però, fu scelta da Torvalds,
per motivazioni più “egoistiche”:«Impieghi sei mesi della tua vita per un
progetto, vuoi metterlo a disposizione di tutti, vuoi farne qualcosa, ma non
vuoi che qualcuno se ne approfitti. Io volevo che la gente lo potesse vedere e
che potesse fare dei cambiamenti e dei miglioramenti in base alle proprie
preferenze. Ma volevo anche essere certo di trarne un vantaggio: vedere cosa
facevano. Volevo poter avere sempre accesso alle sorgenti in modo che se
avessero fatto dei miglioramenti li avrei potuti utilizzare anch’io».. Mi
sembra evidente che l’approccio dei due alla General Public License è piuttosto
differente, quello del creatore di Linux è pragmatico e legato ad una esigenza
personale. Infatti Torvalds ritiene che ogni programmatore dovrebbe essere
libero di fare quello che vuole del proprio software, e quindi anche di
scegliere di non distribuirlo attraverso la licenza GPL; Stallman, invece, come
abbiamo già visto esaurientemente, vorrebbe togliere la libertà ad ogni
programmatore per darla interamente al software. Questa divergenza di vedute si
è esacerbata con il rifiuto di Torvalds di rilasciare Linux sotto la versione 3
della licenza GPL. La GPL versione 3 nasce con l’intento di proibire la
tivoization, termine coniato da Stallman per indicare una violazione specifica
della licenza GPL. Tivoization si riferisce all’utilizzo che TiVo, un’azienda
californiana, fa del software rilasciato sotto licenza GPL nel proprio omonimo
videoregistratore digitale. Il software di TiVo, incorpora il kernel Linux e il
software GNU, rilasciati sotto la versione 2 della licenza GPL che, come
sappiamo, obbliga i distributori a rendere disponibile il codice sorgente del
programma affinché gli utenti possano adattarlo ai propri scopi. Stallman crede
che TiVo abbia aggirato questa norma, facendo sì che il prodotto esegua solo
programmi la cui firma digitale è tra quelle autorizzate dall’azienda che, in
questo modo, da una parte obbedisce alle richieste della GPL versione 2,
permettendo agli utenti la modifica del codice sorgente del loro software, ma
dall’altra impedisce a quest’ultimo di essere eseguito dal videoregistratore.
Si può dire che TiVo, in questo contesto, si è comportata proprio come un
scaltro hacker. E’ riuscita, attraverso un espediente ingegnoso, un vero e
proprio hack, ad aggirare le limitazioni impostegli dalla licenza GPL. Riguardo
alla tivoization, Torvalds non è affatto d’accordo con la posizione di Stallman
e sostiene invece che l’utilizzo di firme digitali private è necessario per
migliorare la sicurezza dei sistemi; inoltre crede che le licenze sul software
dovrebbero solo tentare di controllare il software e non la piattaforma sul
quale gira, e afferma: “Secondo me uno dei motivi per cui Linux ha avuto tanto
successo è la qualità del progetto, non certo l’atteggiamento di crociata che
molti gli vogliono attribuire” ed ha aggiunto a proposito della GPL: “Non è la
GPL ad aver reso famoso GNU/Linux, ma è Linux ad aver reso “presentabile” la
GPL, essendo dannatamente meno integralista di quello che la Free Software
Foundation (di Stallman) vuole”. Stallman risponde a queste osservazioni
rivolgendosi agli utenti: «Se volete la libertà non seguite Torvalds”».

Rimanendo in tema di rivolta, mi sembra di poter sostenere che Torvalds è
rimasto tuttora un ribelle. Per quanto continui a punzecchiare gli estremisti
del Free Software, e a difendere Linux dall’avidità di chi pensa solo ai
quattrini, non possiamo dire che Linus sia diventato schiavo di Linux. A tale
proposito è significativa questa asserzione contenuta nella sua autobiografia:
«un giorno, tra vent’anni, arriverà qualcuno a dirci che ne ha avuto abbastanza
(di Linux) e creerà un suo sistema operativo chiamato «Fredix» Senza tutta la
zavorra storica che Linux avrà accumulato. Ed è esattamente così che deve
andare».

L’ETICA HACKER E LO SPIRITO DELL’ETA’ DELL’INFORMAZIONE
-------------------------------------------------------
A parte Torvalds, esistono molti hacker che, oltre a non cadere vittime della
religiosità informatica, stanno con il loro egoismo (in senso stirneriano)
minando silenziosamente anche alcuni aspetti “religiosi” della nostra società.
Mi riferisco alla mentalità che, secondo Pekka Himanem, docente universitario
esperto di nuove tecnologie, ci rende schiavi da molto tempo, ovvero l’etica
del lavoro protestante analizzata nel classico di Max Weber, L’etica
protestante e lo spirito del capitalismo. Questa tesi è il tema centrale del
libro di Himanem intitolato“Etica Hacker”. Secondo Himanen l’etica hacker si
presenta come una nuova etica che sfida quella protestante (l’etica attuale dei
paesi occidentali) e lo spirito del capitalismo; Himanen intende l’etica hacker
in un’accezione che trascende il mondo dell’informatica includendovi i valori
di tutte quelle persone (professionisti dell’informazione, artisti, scienziati,
ingegneri) che vivono una “vita appassionata”, il cui adagio non è “il tempo è
denaro”, ma piuttosto (il più egoistico) “la vita è mia”; questi appassionati
non intendono il lavoro come dovere etico o mero strumento per arricchirsi, ma
come una passione che permette di coltivare i propri interessi. In questo
scenario il lavoro non viene mai a perdere il suo aspetto giocoso e diviene
simile ad un hobby. Anche il termine “etica protestante”, osserva Himanem, deve
essere inteso in senso ampio, a prescindere dal credo religioso personale; la
forza di questa etica, che vede il lavoro come una vocazione divina, fine a se
stesso, è tale che oggi viene spesso vista come “natura umana” da cui non ci si
può liberare, mentre, sottolinea Himanen, si tratta di una peculiarità storica.
Un prototipo dell’etica del lavoro protestante era presente, in epoca
medioevale, all’interno dei monasteri; la regola benedettina, per esempio,
spiegava che la natura del lavoro non era importante, perché il suo fine ultimo
non era quello di fare qualcosa, ma di sottomettere l’anima del lavoratore
facendogli fare qualsiasi cosa gli venisse detta. Con la Riforma protestante
questo punto di vista si diffuse in tutto il mondo al di là delle mura dei
monasteri.

Himanem, citando Weber, sottolinea che il capitalismo aveva trovato nell’etica
protestante la sua giustificazione religiosa, ma ben presto si emancipò e
cominciò ad operare autonomamente. Come tutti possono osservare l’essenza
dell’etica protestante è sopravvissuta sino all’attuale network society, dove
ha assunto delle connotazioni ancora più oppressive per l’individuo. Non solo
il tempo dedicato al lavoro è stato ottimizzato (a favore del lavoro, non del
lavoratore) ma anche il tempo libero. Anche quando si stacca dal lavoro, non si
è più liberi semplicemente di “essere”, ma si deve realizzare il proprio
“essere” particolarmente bene. Per esempio, soltanto un pivello si rilassa
senza aver eseguito corsi di tecniche di rilassamento. Essere semplicemente un
hobbista nel proprio hobby è considerato imbarazzante. Prima è stata tolta la
giocosità al lavoro, poi è stata tolta dal gioco, e ciò che resta è un tempo
libero ottimizzato. In sostanza: il processo di ottimizzazione dell’intera vita
ha trasformato l’intera vita in lavoro, infatti anche il tempo libero assume le
forme dell’orario lavorativo. Un’ulteriore osservazione di Himanem riguarda le
tecnologie apparentemente liberatorie come Internet e il telefono cellulare. Se
facciamo caso, queste innovazioni, non portano ad una flessibilità del tempo a
favore dell’individuo, ma al rafforzamento della centralità del lavoro,
portandoci a vivere in una continua situazione d’emergenza.

Ironicamente i primi ad adottare il telefono sono stati proprio i
professionisti dell’emergenza come i medici e i poliziotti. Parlando poi
dell’etica del denaro, Himanem arriva a concludere che se nel vecchio
capitalismo l’equilibrio tra lavoro e denaro pendeva a favore del lavoro. Nella
new economy invece, il lavoro, pur essendo ancora un valore autonomo, (chi non
lavora viene moralmente condannato anche se ricco), sta perdendo terreno a
favore del denaro come risultato delle stock option, delle aziende start-up e
delle azioni come incentivo salariale. Se i protestanti del XVII secolo avevano
bandito le scommesse, ora la new economy dipende da queste. In questa cultura
frenetica e incentrata sul guadagno, gli hacker, sempre secondo Himanem,
sfidano i retaggi dell’etica protestante con una loro etica che ha influenzato
anche i professionisti “non informatici”.

Un hacker può raggiungere gli amici a metà giornata per un lungo pranzo, poi
recuperare il lavoro nel pomeriggio tardi o il giorno successivo. L’Etica
hacker, piuttosto che intendere rigidamente l’orario lavorativo, asseconda
invece il ritmo della creatività dell’individuo favorendo l’autoorganizzazione.
Un hacker, a detta di Himanem, ha un rapporto con il tempo analogo a quello
delle persone che erano impegnate nell’accademia di Platone, riassumibile nel
termine scholè, il corrispettivo dell’”otium latino”; l’hacker è padrone del
suo tempo. Ai tempi dell’accademia platonica, se un individuo libero poteva
impegnarsi a fare certi lavori, nessun altro possedeva il suo tempo. Non avere
la responsabilità del proprio tempo, ascholia, veniva associato ad uno stato di
schiavitù. Per quanto riguarda il rapporto denaro-lavoro, Himanem scrive che
l’hacker accetta un lavoro solo se è intrinsecamente stimolante, mettendo
quindi in secondo piano l’aspetto economico; e porta l’esempio di alcuni hacker
che, dopo aver costruito imprese intorno ai loro programmi (ad esempio Mitch
Kapor, fondatore di Lotus13) abbandonarono gli affari perchè il loro originale
“spirito” hacker era virato verso uno stile di tipo imprenditoriale, facendoli
sentire alienati.

Dopo aver illustrato i contenuti salienti del libro di Himanem, che sono di
grande interesse in rapporto al tema della rivolta, mi permetto di fare un
appunto utilizzando, ancora una volta, la critica esistenziale di Stirner. Il
saggio di Himanem è decisamente ricco di esempi che parlano di hacker
rivoltosi, tra cui, i più significativi sono quelli di Torvalds, che, tra
l’altro, ne ha scritto il prologo, e Steve Wozniack, illustre informatico che,
poco dopo aver fondato Apple assieme a Steve Jobs e rivoluzionato il mondo dei
computer, lascia l’azienda per dedicarsi all’insegnamento dei bambini. Si
tratta di individui che non avevano nessuna intenzione di cambiare il mondo per
realizzare un ideale, ma che lo hanno fatto accidentalmente, nel momento in cui
hanno assecondato la propria passione. Nonostante ciò, Himanem, cogliendo
esclusivamente l’aspetto etico, ossia il momento religioso della controcultura
hacker, trasporta anche l’agire degli hacker rivoltosi dal piano della rivolta
a quello della rivoluzione, facendoli apparire tutti come hacker “religiosi”.
Non a caso Himanem parla spesso di sfida sociale degli hacker nei confronti
dell’etica protestante, come se gli hacker si sentissero investiti da una
missione sociale. Insomma: Himanem riduce quella che è una rivolta
dell’individuo per superare l’alienazione, in una guerra tra spiriti alienanti:
lo spirito hacker che si scontra con quello protestante. L’apice di questo
processo è raggiunto nel settimo capitolo del saggio, quando Himanem riassume i
valori dell’etica hacker innalzando e sottraendo all’individuo anche ciò che lo
caratterizza come tale: la creatività e la passione. Creatività e passione
diventano santi, valori da seguire, Himanem li indica chiaramente come
valori-guida.

Un’ultima riflessione: vedendo l’hacker solo in chi si sottomette all’etica
hacker, Himanem è costretto a ribadire in continuazione espressioni del tipo
“Non tutti gli hacker saranno d’accordo con quello che ho scritto” e a trovare
delle attenuanti per eventuali deviazionismi di hacker noti.

Senza dubbio gli hacker più propensi ad accettare acriticamente tutto ciò che
scrive Himanem sono quelli ammaestrati dai saggi di Raymond, autore che Himanem
cita di continuo. Abbiamo visto come negli scritti di Raymond anche l’azione
più spontanea per un appassionato, ad esempio quella di condividere le proprie
conoscenze, viene sacralizzata e tramutata in un dovere etico a cui non ci si
può sottrarre se si vuole essere chiamati hacker. Noi siamo però d’accordo con
Stirner quando sostiene che l’attaccamento alle parole è solo una questione per
ossessi. Se qualcuno si sente un hacker, un appassionato, è così importante che
il mondo lo chiami con tale parola invece che con un’altra? E’ possibile che
una parola possa scalfire la sua passione? Ma soprattutto: se un hacker è una
persona che si impegna nell’affrontare sfide intellettuali per aggirare o
superare creativamente le limitazioni che gli vengono imposte, non
limitatamente ai suoi ambiti d’interesse, ma in tutti gli aspetti della sua
vita, perché limitarsi ad essere un hacker?

**Nicola Durante**

Note
----
1. I supercomputer sono elaboratori appartenenti a grandi società o enti di
   ricerca che condividono il loro utilizzo tra molti utenti e sono progettati
   per ottenere enormi potenze di calcolo.
2. Il codice sorgente di un programma è costituito dalle istruzioni,
   appartenenti ad un linguaggio di programmazione, che servono per ottenere il
   programma. Se paragoniamo un software ad una torta, il codice sorgente è la
   ricetta che serve per prepararla.
3. Sam Williams – Codice Libero, Richard Stallman e la crociata per il software
   libero (Appendice B: Hack, hacker e hacking)
4. Un tutorial o how-to è la raccolta di istruzioni essenziali su di uno
   specifico argomento senza troppe pretese di rigore e completezza espositiva.
5. Un compilatore è un programma che traduce il codice sorgente di un software
   nel software vero e proprio: il linguaggio macchina binario comprensibile
   solo al computer.
6. Unix è un sistema operativo sviluppato a partire dal 1969 dai laboratori
   AT&T e Bell Labs, donato per fini di studio alle università di tutto il
   mondo. Molti sostengono che grazie ad Unix l’informatica si è evoluta da
   aspetto pratico delle scienze matematiche a scienza autonoma.
7. Il kernel (nucleo) è il software del sistema operativo che fornisce ai
   processi in esecuzione sull’elaboratore un accesso sicuro all’hardware.
8. Un Linux User Group (LUG) è un gruppo formato da persone che condividono la
   passione per il Software libero e in particolare per il sistema operativo
   Linux.
9. Out of the box = nella configurazione predefinita e pronta all’uso.
10. I moduli del kernel, noti in altri sistemi operativi come driver, sono
    porzioni di kernel che possono essere caricate in memoria solo quando se ne
    presenta la necessità.
11. Il desktop environment è l’interfaccia grafica (GUI) che permette di usare
    interattivamente un computer tramite icone e finestre.
12. Una patch è una porzione di software atta a modificare un programma,
    solitamente correggendo degli errori, detti anche bachi (bug).
13. Lotus Software, ora acquisita da IBM, era nota soprattutto per il foglio di
    calcolo Lotus 1-2-3.

Bibliografia Essenziale
=======================

- Ken Goffman con Dan Joy, Controculture – da Abramo ai no global, Fazi Editore
  Roma 2004
- Pekka Himanem, L’etica hacker e lo spirito dell’età dell’informazione,
  Feltrinelli Milano 2007
- Giorgio Penzo, Max Stirner – La rivolta esistenziale, Marietti Genova 1992
- Eric Steven Raymond, How To Become a Hacker,
  http://www.catb.org/~esr/faqs/hacker-howto.html
- Eric Steven Raymon, The Cathedral and the Bazaar,
  http://www.catb.org/~esr/writings/cathedral-bazaar/
- Max Stirner, L’unico e la sua proprietà (con un saggio di Roberto Calasso),
  Adelphi Milano 2002
- Linux Torvalds e Diamond David, Rivoluzionario per caso – come ho creato
  Linux (solo per divertirmi), Garzanti. Milano 2001

Le utilizzazioni consentite dalla legge sul diritto d’autore e gli altri
diritti non sono in alcun modo limitati da quanto sopra.

Questo è un riassunto in linguaggio accessibile a tutti del Codice Legale (la
licenza integrale): [http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode][3]

[1]:http://ita.anarchopedia.org/Max_Stirner
[2]:https://it.wikipedia.org/wiki/Free_Software_Foundation
[3]:http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
