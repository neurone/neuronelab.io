title: Robocop 2014
date: 2015-06-24 20:58:56 +0200
tags: fantascienza, filosofia della mente, film, recensioni, filosofia, cyberpunk
summary: Il remake di Robocop, classico del cinema fantascientifico, ha ricevuto molte meritate critiche, ma ha qualche interessante merito che chi s'interessa di scienze cognitive e filosofia della mente potrebbe apprezzare.

Il remake di Robocop, classico del cinema fantascientifico, ha ricevuto molte meritate critiche, ma ha qualche interessante merito che chi s'interessa di scienze cognitive e filosofia della mente potrebbe apprezzare.

![Robocop 2014]({filename}/images/robocop.jpg "Robocop 2014")

Chi conosce il [film originale][1], noterà sicuramente le differenze, che potrebbero
essere viste come mancanze, di questo remake. La critica al consumismo ed al
dominio delle megacorporazioni è portata in modo meno satirico, grottesco e
cinico rispetto all'originale. Questa parte viene coperta quasi esclusivamente
dagli interventi dello show televisivo di *Pat Novak*, caricatura
dell'opinionista/influencer parziale, venduto ed opportunisticamente
patriottico. Al confronto del film di [Verhoeven][2], vi è anche molta meno
violenza esplicita, cosa che gli ha fatto guadagnare il [rating][3] *PG-13*,
rispetto al *R* del suo predecessore.

Come molti film recenti, la sceneggiatura, la disposizione degli eventi ed il
tempo dedicato ad essi sanno di *già visto*. Questo pare essere uno dei
moltissimi film che seguono lo schema dettato dallo sceneggiatore *Blake Snyder*
nel suo libro seminale [Save the cat!][4].

Ciò che il film porta di positivo, viene valorizzato solo se si fanno i dovuti
*compiti a casa* con delle opportune ricerche. I nomi di alcuni personaggi
principali, infatti, sono degli ottimi suggerimenti riguardo a cosa cercare. Il
maggior oppositore all'introduzione dei robot nelle forze di polizia, infatti,
porta il nome del filosofo [Hubert Dreyfuss][5], notoriamente critico sulle idee
classiche sull'intelligenza artificiale. Egli sostiene che i principali assunti
della ricerca sul IA siano sbagliati: che il cervello sia analogo all'hardware
di un computer e la mente al software e che la mente funzioni similmente a degli
algoritmi che elaborano rappresentazioni discrete e simboli. Nega che i nostri
comportamenti si possano comprendere e predire in modo oggettivo e libero da
ogni contesto. Non nega che possa esistere un'intelligenza artificiale, ma
ritiene che per esserci, richieda un corpo simile al nostro inserito nel nostro
mondo comprendente una cultura ed una società simili alle nostre. Questa
posizione è condivisa dai sostenitori dell'[embodyment][6].  Nel film, il
personaggio che prende il nome di Dreyfuss, sostiene che utilizzare dei robot al
posto di umani su questioni di vita o di morte che richiedano un giudizio etico
e non solo razionale, sia sbagliato e pericoloso. Per essere un buon giudice,
non è sufficiente applicare ciecamente delle regole, come farebbe una macchina
esclusivamente razionale, ma saper prendere delle decisioni difficili in assenza
di regole che ci dicano come agire.

Lo scienziato costruttore di *Robocop*, *Dennett Norton*, prende il nome dal
filosofo [Daniel Dennett][7] e, come lui, ha un approccio
*logico-funzionalista*. Al contrario di Dreyfuss, sostiene che gli umani siano
delle macchine elaboratrici di informazioni e che la mente possa essere ritenuta
libera dal contesto in cui risiede. Il nostro cervello è come un processore che
elabora su più livelli ciò che viene percepito dai sensi, attraverso processi
che avvengono precedentemente alla nostra coscienza. Nel momento in cui qualcosa
arriva al livello cosciente, essa è già stata elaborata ed interpretata. Di
conseguenza, abbiamo solamente l'illusione di avere una volontà, in quanto la
nostra interazione con l'ambiente avverrebbe prima di arrivare alla coscienza.
In modo simile a come avrebbe dimostrato [Benjamin Libet][8] con i suoi
[esperimenti][9]. Questo appare evidente in una scena del film in cui Norton
Dennett spiega che nel momento in cui Robocop va in modalità combattimento,
l'intelligenza artificiale che affianca il suo cervello prende il controllo del
corpo, ma la mente di *Alex Murphy* continua a ritenere di essere in controllo.
La stessa situazione viene descritta nell'esperimento mentale di Daniel Dennett
["Dove sono?"]({filename}dove-sono.md).

Se fosse stato seguito un approccio al tema più originale, ne sarebbe potuto
uscire un gran bel film. Al contrario, rimane un buon film d'azione, con una
sceneggiatura e dei personaggi prevedibili e degli ottimi spunti per delle
ricerche, ma purtroppo visibili solo da chi sa già cosa cercare.

Un articolo interessante, scritto dal filosofo [enattivista][10] [Alva Noë][11]:
[Deconstructing The Philosophies Of 'RoboCop'][12].

[1]:https://it.wikipedia.org/wiki/RoboCop_(film_1987)
[2]:https://it.wikipedia.org/wiki/Paul_Verhoeven
[3]:https://en.wikipedia.org/wiki/Motion_Picture_Association_of_America_film_rating_system#MPAA_film_ratings
[4]:http://www.slate.com/articles/arts/culturebox/2013/07/hollywood_and_blake_snyder_s_screenwriting_book_save_the_cat.html
[5]:https://en.wikipedia.org/wiki/Hubert_Dreyfus
[6]:https://en.wikipedia.org/wiki/Embodied_cognition
[7]:https://en.wikipedia.org/wiki/Daniel_Dennett
[8]:https://it.wikipedia.org/wiki/Benjamin_Libet
[9]:http://www.informationphilosopher.com/freedom/libet_experiments.html
[10]:https://en.wikipedia.org/wiki/Enactivism
[11]:https://en.wikipedia.org/wiki/Alva_No%C3%AB
[12]:http://www.npr.org/sections/13.7/2014/04/04/295314242/deconstructing-the-philosophies-of-robocop
