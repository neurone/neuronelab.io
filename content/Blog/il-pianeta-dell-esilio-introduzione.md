title: Il pianeta dell'esilio - Introduzione dell'autrice
date: 2016-10-09 20:55
tags: libri, fantascienza, società

Tutti gli scrittori di fantascienza si sentono chiedere, con magnifica
regolarità: "Da dove nascono le sue idee?". Nessuno di noi sa mai bene cosa
rispondere, a parte Harlan Ellison, che replica con un secco "Schenectady!".

La domanda è diventata una specie di barzelletta e perfino una vignetta del «New
Yorker»; eppure, di solito è posta con sincerità, perfino con bramosia; di certo
non vuole essere una domanda stupida. Il problema, il motivo per cui
"Schenectady" è l'unica risposta possibile, è che non è la domanda giusta. E
alle domande sbagliate non ci sono risposte giuste, come testimoniano le opere
di coloro che tentavano di scoprire le proprietà del flogisto. A volte si tratta
semplicemente di una formulazione imprecisa; questo è ciò che chi pone la
domanda vuole davvero sapere: "La scienza nella sia fantascienza deriva dal
leggere o conoscere la scienza?". (Risposta: sì.) Oppure: "Capita mai che gli
scrittori di fantascienza si rubino le idee a vicenda?". (Risposta:
continuamente.) Oppure ancora: "L'azione nei suoi libri deriva dal fatto che lei
ha vissuto in prima persona tutte le esperienze dei suoi protagonisti?".
(Risposta: Dio me ne scampi!) Ma a volte gli interroganti non riescono a essere
specifici; divagano e passano ai _be', tipo, insomma..._ e a quel punto sospetto
che ciò a cui davvero mirino sia qualcosa di complesso, difficile e importante:
cercano di capire l'immaginazione come funziona, come un artista la usi o ne sia
usato. Sappiamo così poco sull'immaginazione che non riusciamo neppure a porre
le domande giuste in proposito, figuriamoci dare le risposte giuste. Le sorgenti
della creazione rimangono un mistero anche per la più saggia fra le psicologie,
e spesso un artista è l'ultima persona in grado di dire alcunché di
comprensibile sul processo della creazione. Benché nessun altro abbia mai detto
molto, la cosa ha un suo senso. Credo che il miglior posto da cui cominciare sia
proprio Schenectady, leggendo Keats.

In anni recenti, mi sento sempre rivolgere (si tratta solo di me, in questo
caso) anche una seconda domanda. E cioè: "Perché scrive così tanto di uomini?".

Non è una domanda stupida, questa. E non è nemmeno una domanda sbagliata, per
niente, benché a volte contenga un preconcetto che rende difficile dare una
risposta diretta. Nei miei libri e nelle mie storie le donne ci sono, e spesso
sono le protagoniste o incarnano il punto di vista principale; e così, quando
qualcuno mi chiede "Perché scrive sempre di uomini?", rispondo "Non è vero", e
lo dico con una certa stizza, perché la domanda così formulata è sia accusatoria
che imprecisa. Posso mandar giù piccole dosi di accusa o imprecisione, ma la
combinazione mi riesce letale.

Ma, di nuovo, a prescindere dalla sua formulazione, ciò che questa domanda porta
con sé è una preoccupazione concreta e pressante. Una risposta leggera è odiosa,
una breve, impossibile.

_Il pianeta dell'esilio_ è stato scritto tra il 1963 e il 1964, prima che il
femminismo si risvegliasse dalla sua paralisi trentennale. Il libro mostra il
modo in cui, ai primi tempi, gestivo i personaggi maschili e femminili - un modo
"naturale" (cioè felicemente integrato), ancora lontano dal risveglio e abituato
all'inconsapevolezza in cui ero stata cresciuta. A quei tempi, potevo dire in
perfetta buona fede, e di sicuro anche con compiacimento, che semplicemente non
mi interessava che i miei personaggi fossero maschi o femmine, l'importante era
che fossero umani. Perché diamine una donna dovrebbe scrivere solo di donne? Non
avevo autoconsapevolezza e non mi sentivo obbligata in alcun modo, quindi ero
sicura di me, poco portata agli esperimenti e soddisfatta nella mia
convenzionalità.

La storia comincia con Rolery, ma presto il punto di vista passa a Jakob e poi a
Wold, quindi torna a Rolery e poi cambia di nuovo: è una storia a focalizzazione
interna multipla. Gli uomini sono più apertamente attivi e molto più eloquenti.
Rolery, una giovane donna inesperta che proviene da una cultura rigidamente
tradizionale e patriarcale, non combatte, non prende l'iniziativa in campo
sessuale, né diviene leader sociale o assume qualunque altro ruolo che possa
essere etichettato come "maschile", nella sua cultura come nella nostra del
1964. Ciononostante, Rolery è una ribelle, tanto dal punto di vista sociale
quanto da quello sessuale. Benché il suo comportamento non sia aggressivo, il
suo desiderio di libertà la conduce a rompere bruscamente con il proprio
sostrato culturale: unendosi a un essere alieno, si sottopone a una
trasformazione integrale. Sceglie l'Altro. Questa piccola ribellione
individuale, che giunge in un momento cruciale, innesca una serie di eventi che
porteranno al completo cambiamento e alla riedificazione di due culture e
società.

Jakob è l'eroe, attivo, eloquente, tutto preso tra combattere con valore e
governare con zelo, ma il motore centrale degli eventi del libro, _chi sceglie_,
all'atto pratico è Rolery. Ho incontrato il taoismo prima del femminismo
moderno. Dove alcuni vedono solo un Eroe dominante e una Piccola Donna passiva,
io vedevo, e vedo tuttora, lo spreco e la futilità connaturate all'aggressività,
e la profonda efficacia del _wu wei_, "agire tramite il non agire".

Tutto giusto; ma resta il fatto che, in questo libro, come nella maggior parte
degli altri miei romanzi, è agli uomini che spetta il grosso dell'azione, in
tutti i sensi del termine, e di conseguenza sono loro a essere più spesso sotto
i riflettori. "Non mi interessava" che il mio protagonista fosse maschio o
femmina: dalla spensieratezza alla noncuranza il passo è colpevolmente breve.
Gli uomini prendono il sopravvento.

Perché glielo permettiamo? Be', è sempre stato tanto più facile parlare di
uomini che fanno cose, dal momento che la stragrande maggioranza dei libri che
parlano di gente che fa cose parla appunto di uomini, ed è la nostra tradizione
letteraria... e perché, da donna, è probabile che non si abbia poi tutta questa
esperienza in fatto di combattere, stuprare, governare eccetera, ma si è notato
che sono gli uomini a occuparsene... e perché, come ha osservato Virginia Woolf,
la prosa inglese non è adatta a descrivere l'essere e l'agire femminile, a meno
che in certa misura non la si reinventi da zero. È difficile prendere le
distanze dalla tradizione, è difficile inventare, è difficile reinventare la
propria lingua madre. Si svicola e si sceglie la strada più semplice. Nulla può
spingerci ad andare controcorrente, a scegliere la strada più difficile, se non
una coscienza profondamente scossa, e con ogni probabilità arrabbiata.

Ma la coscienza deve _essere_ arrabbiata. Se si sforza di arrivare alla rabbia
con la ragione, produce solo senso di colpa, che soffoca le sorgenti della
creazione ai primi zampilli.

Sono spesso molto arrabbiata, in quanto donna. Ma la mia rabbia femminista è
solo una componente, una parte della furia e della paura che mi assalgono
quando guardo a ciò che tutti ci stiamo facendo l'un l'altro, alla terra, e alla
speranza di libertà e di vita. Ancora oggi "non mi interessa" che qualcuno sia
maschio o femmina, quando tutti sono nostri simili e nostri figli. Davanti a un
innocente imprigionato ingiustamente, dovrei preoccuparmi del suo sesso? Davanti
a un bambino che muore di fame, dovrei preoccuparmi del suo sesso?

Certe femministe radicali rispondono che sì, dovrei. Partendo dal presupposto
che l'ingiustizia sessuale sia alla base di ogni ingiustizia, sfruttamento e
sopraffazione cieca, è una posizione solida. Io non posso accettare il
presupposto, e dunque non posso agire su queste basi. Se mi sforzassi di farlo,
dal momento che io agisco tramite la scrittura, scriverei male e in modo
disonesto. Dovrei forse sacrificare l'ideale di verità e bellezza a vantaggio di
un principio ideologico?

Di nuovo, una femminista radicale potrebbe rispondere che sì, dovrei. Benché a
volte quella risposta coincida con la voce del Censore, mossa puramente
dall'oscurantismo fanatico o autoritario, non sempre è così: potrebbe parlare in
nome dell'ideale stesso. Per costruire il nuovo, è necessario fare tabula rasa
del vecchio. La generazione cui tocca fare tabula rasa si ritrova sulle spalle
tutto il dolore della distruzione e ben poco della gioia della creazione. Il
coraggio di accettare questo compito insieme all'ingratitudine e all'infamia che
lo accompagnano non sarà mai lodato abbastanza.

Ma questo coraggio non può essere imposto o simulato. Nel primo caso, porta solo
al rancore e all'autodistruttività, nel secondo porta al Feminist Chic,
discendente diretto del Radical Chic. Un conto è sacrificare l'appagamento al
servizio di un ideale; un altro è soffocare il pensiero lucido e il sentire
autentico al servizio di un'ideologia. Un'ideologia è preziosa solo nella misura
in cui vi si ricorre per _rafforzare_ la lucidità e l'autenticità del pensiero e
del sentire.

E da questo punto di vista, l'ideologia femminista è stata immensamente preziosa
per me. Ha costretto me e ogni altra donna pensante di questa generazione a
conoscere meglio noi stesse: a distinguere, spesso con grande dolore, ciò che
davvero pensiamo e crediamo delle facili "verità" e dai "fatti" che ci sono
stati (subliminalmente) insegnati circa l'essere maschi, l'essere femmine, i
ruoli sessuali, la fisiologia e la psicologia femminili, ;a responsabilità
sessuale eccetera. Fin troppo spesso ci siamo accorte che _non avevamo alcuna_
opzione o convinzione davvero nostra, ma che avevamo introiettato i dogmi della
nostra società; e così, ecco giunto il momento di scoprire, inventare, creare le
nostre verità, i nostri valori, noi stesse.

Questa riedificazione del sé femminile è una liberazione e un sollievo per
coloro che vogliono e necessitano di un sostegno di gruppo, o il cui essere
donne è stato sistematicamente offeso, degradato, sfruttato durante l'infanzia,
il matrimonio, sul lavoro. Per altre come me, per le quali il gruppo di pari non
rappresenta una casa e che non sono state alienate dal loro essere donne, questo
lavoro d'indagine di sé non è facile. "Mi piacciono le donne, mi piace come sono
io, perché agitare le acque?", "Non mi _interessa_ se sono uomini o donne...",
"Perché diamine una donna dovrebbe scrivere solo di donne?". Sono tutte domande
valide; nessuna ha una risposta facile; ma tutte devono, adesso, _essere poste_
e _trovare risposta_. Un'attivista politica può basare le proprie risposte
sull'ideologia del suo movimento nel dato frangente, ma un'artista deve trovarle
scavando in se stessa, e continuare a scavare finché non è sicura di essere
arrivata il più vicino possibile alla verità.

Io continuo a scavare. Uso gli strumenti del femminismo, e cerco di capire cosa
mi fa lavorare e come lavoro, così da non lavorare più nell'ignoranza o
nell'irresponsabilità. Non è né rapido, né indolore; si avanza a tentoni nel
buio del corpo e della mente - un viaggio  molto lungo da Schenectady. Quanto
poco sappiamo davvero di noi stessi, donne o uomini!

Ecco, una delle cose che credo di aver recuperato dai miei scavi è questa: la
"persona" di cui tendo a scrivere spesso non è esattamente o non è totalmente né
un uomo né una donna. A livello superficiale, ciò significa che gli stereotipi
sessuali sono in gran parte neutralizzati (gli uomini non sono predatori
libidinosi e le donne non sono straordinarie bellezze), e il sesso in sé è
visto più come _una relazione che conta come atto_. Il sesso serve
principalmente a definire il genere, e l'etichetta "uomo" o "donna" non
esaurisce il genere dell'individuo, né vi si avvicina a sufficienza. Di certo,
tanto il sesso quanto il genere sembrano utilizzati per definire il significato
di "persona" o di "sé". Una volta, agli inizi del mio risveglio, ho chiuso
questa relazione in una persona unica, un androgino. Ma più spesso essa appare
nella sua veste più convenzionale e manifesta - una coppia. Entrambi in uno, o
due a formare un intero. Non c'è yin senza yang, né yang senza yin. Mi è stato
chiesto una volta quale fosse, a mio parere, il tema costante, centrale del mio
lavoro, e ho risposto d'impulso: "Il matrimonio".

Non ho ancora scritto un libro degno di quel tema formidabile (e incredibilmente
fuori moda). Ancora non ho neppure capito cosa intendessi con quella risposta.
Ma rileggendo questa tranquilla storia d'avventura dei miei inizi, credo che il
tema ci sia - non chiaro, non forte, ma le tracce si incamminano in quella
direzione. "E andato che capisco dove devo andare."

_Ursula K. Le Guin_,  maggio 1977
