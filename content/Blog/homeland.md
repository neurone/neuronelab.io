title: Homeland
date: 2016-01-27 18:47
tags: libri, fantascienza, società, recensioni
summary: Il seguito del bellissimo _Little Brother_ di _Cory Doctorow_ non delude le aspettative. È la naturale prosecuzione della storia e, come nel precedente, racconta il presente dal punto di vista di un giovane hacker ed attivista.

![Homeland]({filename}/images/homeland.jpg "Copertina: Homeland"){: style="float:right"}
Il seguito del bellissimo [Little Brother]({filename}/Blog/little-brother.md) di [Cory Doctorow][15] non delude le aspettative. È la naturale prosecuzione della storia e, come nel precedente, racconta il presente dal punto di vista di un giovane hacker ed attivista.

I temi sono immutati, ma più attuali anche se il romanzo precede lo scandalo delle [intercettazioni globali][1] della [NSA][2] statunitense e del [GCHQ][3] britannico. Infatti il libro fu scritto nel 2012, poco prima delle rivelazioni di [Edward Snowden][4]. Anche Homeland, come Little Brother, [fece compagnia a Snowden][10] nella sua fuga.

Mentre il primo romanzo, alla cui recensione [vi rimando]({filename}/Blog/little-brother.md) per inquadrare meglio lo stile e le tematiche di questo seguito, sentiva ancora le influenze delgli eventi del [9/11][5] e delle conseguenti limitazioni delle libertà individuali e di libera espressione e circolazione delle informazioni ad opera dei governi, questo si concentra di più sulle conseguenze della recessione economica, la situazione statunitense degli [studenti indebitati a vita][6] ed il ruolo di corporazioni private, agenzie di _contractors militari_ e governi.

Viene illustrato lo sviluppo nelle tecniche di censura e repressione che descritte nella loro evoluzione più subdola e nascosta, rispetto all'aperta violenza mostrata nella prima parte della storia.

Un bellissimo romanzo che incoraggia all'attivismo. Molto belle anche le postfazioni di [Jacob Appelbaum][7] ([Wikileaks][8]) e [Aaron Swartz][9], programmatore ed attivista morto suicida in seguito a delle incriminazioni ingiuste e sproporzionate.

[Little Brother (download)][11] e [Homeland (download)][12] sono scaricabili gratuitamente dal sito di Cory Doctorow, sotto licenza [Creative Commons Attribution-Noncommercial-ShareAlike][13], in praticamente ogni formato di testo esistente. Oppure è disponibile in italiano, nel catalogo di [Multiplayer Edizioni][14].

>In “Little Brother” avevamo lasciato il giovane Marcus Yallow arbitrariamente detenuto e brutalizzato dal governo a seguito di un attacco terroristico a San Francisco. Un’esperienza questa che lo portò a diventare leader di un intero movimento di giovani prestigiatori tecnologici, in lotta contro uno Stato di polizia tirannico.
>
>Alcuni anni dopo, l’economia della California collassa, ma il passato da hacktivista di Marcus gli fa ottenere un lavoro come webmaster per un politico impegnato in una crociata riformista. Poco dopo, la sua nemesi di un tempo, Masha, emerge dall’underground politico per fargli dono di una chiavetta USB contenente cablogrammi stile Wikileaks con prove lampanti delle malefatte di aziende e governi. È roba incendiaria – e se Masha dovesse sparire dalla circolazione, Marcus dovrà renderla nota al mondo. A quel punto il protagonista assiste al rapimento di Masha da parte degli stessi agenti governativi che lo hanno detenuto e torturato anni prima.
>
>Marcus può diffondere l’archivio che Masha gli ha dato – ma non può ammettere di essere il divulgatore, perché la cosa costerebbe l’elezione al suo datore di lavoro. È circondato da amici che ricordano cosa ha fatto qualche anno prima e lo considerano un eroe hacker. Non può neanche partecipare a una manifestazione senza essere trascinato sul palco e vedersi mettere in mano un microfono. E lui non è del tutto sicuro che limitarsi a piazzare l’archivio su Internet – prima di essersi letto i milioni di parole che contiene – sia la cosa giusta da fare.
>
>Nel frattempo, qualcuno inizia a pedinarlo, qualcuno che ha l’aria di essere abituato a infliggere dolore finché non ottiene le risposte che desidera…
>
>Un romanzo dal ritmo serrato, pieno di passione e tanto attuale quanto il futuro prossimo, “Homeland” è in ogni aspetto alla pari con “Little Brother”, un inno all’attivismo, al coraggio e al desiderio di rendere il mondo un posto migliore.
>
>Homeland ha vinto il Premio Prometheus 2014

[1]:https://en.wikipedia.org/wiki/Global_surveillance_disclosures_%282013%E2%80%93present%29
[2]:https://en.wikipedia.org/wiki/National_Security_Agency
[3]:https://en.wikipedia.org/wiki/Government_Communications_Headquarters
[4]:https://en.wikipedia.org/wiki/Edward_Snowden
[5]:https://en.wikipedia.org/wiki/September_11_attacks
[6]:https://en.wikipedia.org/wiki/Student_debt
[7]:https://en.wikipedia.org/wiki/Jacob_Appelbaum
[8]:https://en.wikipedia.org/wiki/WikiLeaks
[9]:https://en.wikipedia.org/wiki/Aaron_Swartz
[10]:http://craphound.com/homeland/2014/12/02/when-ed-snowden-met-marcus-yallow/
[11]:http://craphound.com/littlebrother/download/
[12]:http://craphound.com/homeland/download/
[13]:https://creativecommons.org/licenses/by-nc-sa/3.0/
[14]:http://edizioni.multiplayer.it/libri/apocalittici/homeland
[15]:https://en.wikipedia.org/wiki/Cory_Doctorow