title: Discorsi sull'intelligenza
date: 2016-07-19 20:34
tags: psicologia, etica, filosofia
summary: Articolo preso da some1elsenotme.wordpress.com: Sto leggendo I miti del nostro tempo di Umberto Galimberti. Lettura non banale. Nel capitolo che riguarda il mito dell’intelligenza, in particolare, ho trovato almeno un paio di paragrafi che mi son piaciuti parecchio. Il primo, La mimetizzazione dell’intelligenza, riguarda il concetto di intelligenza come capacità dell’intelligenza stessa di manifestarsi pubblicamente o meno a seconda di ciò che viene richiesto da una certa situazione sociale (questa di Galimberti è un’idea che ho maturato da anni: interessanti le connessioni che lui produce tra tale concetto e le caratteristiche della personalità narcisistica). Il secondo accenna al rapporto tra giustizia e tutto ciò che sappiamo e scopriamo, anno dopo anno, sul funzionamento dello psiche, con gli strumenti del diritto che sono sempre più grossolani quando si trovano a maneggiare i più radicali progressi scientifici.


Da [some1elsenotme.wordpress.com][1]

Sto leggendo I miti del nostro tempo di Umberto Galimberti. Lettura non banale.
Nel capitolo che riguarda il mito dell’intelligenza, in particolare, ho trovato
almeno un paio di paragrafi che mi son piaciuti parecchio. Il primo, La
mimetizzazione dell’intelligenza, riguarda il concetto di intelligenza come
capacità dell’intelligenza stessa di manifestarsi pubblicamente o meno a seconda
di ciò che viene richiesto da una certa situazione sociale (questa di Galimberti
è un’idea che ho maturato da anni: interessanti le connessioni che lui produce
tra tale concetto e le caratteristiche della personalità narcisistica). Il
secondo accenna al rapporto tra giustizia e tutto ciò che sappiamo e scopriamo,
anno dopo anno, sul funzionamento dello psiche, con gli strumenti del diritto
che sono sempre più grossolani quando si trovano a maneggiare i più radicali
progressi scientifici.

Incollo qui alcune parti, quelle che ritengo più significative, tratte dai due
paragrafi:

> 2\. La mimetizzazione dell’intelligenza Se siamo tutti intelligenti, ognuno a
>    suo modo, sarà tendenza di ciascuno mostrare, ogni volta che se ne presenta
>    l’occasione, la specificità della propria intelligenza. Il risultato di solito
>    è: o la mortificazione di quanti sono costretti ad assistere all’esibizione
>    dell’altrui abilità mentale, o l’invidia che, opportunamente mascherata, trova
>    sfogo nella maldicenza intorno ad altri aspetti della personalità di chi fa
>    sfoggio della propria intelligenza, o infine il disinteresse per ciò che la
>    persona intelligente va dicendo, creando un vuoto intorno al suo discorso che
>    ricade su se stesso senza i riscontri attesi. A parità di capacità
>    intellettuali è allora più intelligente non tanto chi eccelle in una
>    determinata abilità mentale, ma chi è in grado di percepire in anticipo
>    l’effetto che un’eventuale esibizione di intelligenza può produrre in chi
>    ascolta. E siccome l’effetto è quasi sempre deprimente, più intelligente sarà
>    chi è capace di mimetizzare la propria intelligenza. “Mimetizzazione” è una
>    parola solitamente impiegata a proposito di quegli animali che sanno
>    confondersi con l’ambiente in modo da non essere individuati da possibili
>    aggressori, così come “mimetico” si chiama l’abbigliamento che in battaglia
>    indossano i militari, sempre allo scopo di non essere individuati e quindi di
>    poter sorprendere il nemico a sua insaputa. Mimetizzare la propria
>    intelligenza significa allora saperne modulare l’espressione a seconda del
>    contesto in cui ci si trova, percependo in anticipo il livello di comprensione
>    di coloro che ci ascoltano e le possibili reazioni che l’intervento può
>    produrre. Questa capacità anticipatoria, che evita le reazioni negative, è
>    tipica di quelle intelligenze non narcisistiche, capaci di “mettersi nei panni
>    degli altri” e calibrare perfettamente come un certo discorso, per
>    intelligente che sia, può essere percepito dall’altro e davvero compreso.
> 
>    Gli antichi filosofi, a differenza dei sapienti che ritenevano di possedere
>    la verità, sapevano che un conto è la verità, un conto è la comprensione
>    della verità. E alla comprensione della verità hanno dedicato la loro
>    massima cura, istituendo, a partire da Socrate, le scuole, persuasi
>    com’erano che una verità non compresa non serve a niente. A condizionare la
>    comprensione non sono solo fattori culturali, ma soprattutto ed
>    eminentemente fattori emotivi, per cui, ad esempio, se una classe di
>    studenti si sente amata dal suo professore l’apprendimento sarà facilitato,
>    se un messaggio viene veicolato da un testimonial apprezzato dal pubblico,
>    sarà più facilmente recepito.
>    
>    Ciò significa che un’intelligenza che si accompagna a una competenza
>    emotiva sa che cosa, di quanto esprime, può essere recepito o rifiutato. E,
>    se le interessa che il messaggio passi, questa intelligenza sa anche
>    rinunciare a dire tutto quello di cui è competente, per limitarsi a
>    enunciare solo ciò che può essere compreso. Riduce quindi le sue
>    possibilità enunciative a favore della trasmissibilità dei messaggi. In una
>    parola, mimetizza la sua intelligenza a misura della recettività di chi
>    ascolta, per favorire l’acquisizione delle informazioni.
>    
>    La mimetizzazione dell’intelligenza è quindi una grande virtù: la virtù
>    degli insegnanti che non sfoggiano tutto il loro sapere, ma solo quello che
>    può essere recepito e nelle forme in cui può essere recepito; la virtù
>    degli psicoanalisti che, pur individuando dopo due sedute di che cosa
>    soffre il paziente, attendono molte sedute affinché il paziente pervenga da
>    sé alla sua verità; la virtù dei genitori che, pur avendo presenti le
>    capacità che i figli potrebbero tradurre in professioni, attendono che i
>    figli le riconoscano da soli, sorreggendo i loro percorsi con piccoli
>    accenni quando i figli sono nella condizione di recepirli; la virtù dei
>    politici che hanno il polso del paese reale e non solo degli obiettivi che
>    vogliono perseguire, indipendentemente dal consenso o dal dissenso
>    opportunamente valutato; ma direi anche la virtù delle veline, alcune delle
>    quali hanno senz’altro significative capacità intellettuali, che però, dato
>    il contesto, non è il caso di esibire in un concorso di bellezza, dove
>    l’attenzione è tutta concentrata sulle misure e le forme del corpo.
>    
>    La mimetizzazione dell’intelligenza è la virtù delle persone veramente
>    intelligenti, che sanno coniugare la verità con la comprensione della
>    verità, per la quale sono disposti a rinunciare all’esibizione di sé per la
>    cura dell’altro e la comprensione delle modalità con cui l’altro può capire
>    quanto si va dicendo. All’intelligenza che sa mimetizzarsi compete quella
>    virtù che possiamo chiamare altruismo, qui inteso non come “buonismo”, ma
>    come percezione di ciò che è altro da me, perché consapevole che gli altri,
>    con le loro obiezioni anche grossolane, possono costituire uno stimolo a un
>    ulteriore ricercare e intendere e trovare.
>    
>    Dimensioni, queste, tutte impedite alle intelligenze narcisistiche che, non
>    percependo nulla dell’altro, del suo livello di comprensione e del valore
>    delle sue obiezioni (che i narcisisti scambiano per attacchi),
>    irrigidiscono la loro intelligenza, facendola diventare sempre più
>    dogmatica, e alla fine arida e fossilizzata, perché non dialogica e non
>    recettiva di quanto gli altri e il mondo hanno ancora da insegnare.
> 
>    […]
> 
> 4\. La capacità di “intendere e volere” Che rapporto c’è tra intelligenza e
>    follia? Come spiegare la lucidità mentale con cui si difendono i colpevoli e
>    gli indiziati di orrendi delitti che così di frequente ricorrono nelle
>    cronache quotidiane? Mi riferisco a quelle storie truci che raccontano, in
>    forma drammatica e crudele, quel che può contorcere l’animo umano, fino a
>    spingerlo a compiere azioni a tal punto aberranti da rasentare l’incredibile.
>    Si tratta di padri o di madri che uccidono i figli, di figli che sterminano la
>    loro famiglia. Che tipi umani sono i protagonisti di queste storie? E in che
>    stato si trova la loro intelligenza? I tribunali infliggono l’ergastolo a chi
>    non è folle o, come dice la legge, a “chi è in grado di intendere e volere”.
>    Ma che significato hanno queste due categorie “intendere” e “volere” in uso
>    nell’Ottocento quando si pensava che le sofferenze psichiche fossero
>    degenerazioni cerebrali che portavano alla demenza, “senile” se si era vecchi,
>    “precoce” se si era giovani? Cento anni di psichiatria hanno dimostrato che
>    tutti i cosiddetti “malati di mente” – siano essi schizofrenici, paranoici,
>    maniaci – sono in grado di “intendere” e “volere”, salvo quando la loro mente
>    è obnubilata da una crisi che può durare un attimo, alcuni giorni, alcuni
>    mesi.
> 
>    Se infliggono la pena, i giudici escludono senza ombra di dubbio che i
>    responsabili di questi delitti o di queste stragi, quando compiono il loro
>    gesto senza alcun movente o per futili motivi, al momento del delitto
>    fossero in preda a una crisi di follia. Ma allora delle due l’una: o erano
>    perfettamente “normali”, cioè in possesso delle loro facoltà mentali, e
>    allora abituiamoci a considerare nella “norma” qualsiasi persona che, senza
>    movente, ne uccide un’altra. Oppure un comportamento del genere ha tutte le
>    caratteristiche della follia, e allora chi compie questi delitti non deve
>    essere rinchiuso in un carcere, ma affidato al servizio sanitario.
>    
>    Questa alternativa, così evidente, non è più evidente in un’aula di
>    tribunale, perché gli strumenti di cui la legge dispone per interpretare la
>    “malattia mentale” sono così obsoleti da non cogliere nessuna delle
>    sindromi che offuscano la mente. Che cosa vuol dire infatti “infermità
>    mentale” o addirittura “seminfermità mentale”, che si è soliti addurre per
>    evitare l’ergastolo? Nulla, proprio nulla. Perché “infermo” o “seminfermo”
>    sono categorie che non appartengono neppure al repertorio medico, ma al
>    linguaggio popolare in riferimento a chi ha problemi motori. L’ordine
>    giudiziario si trova a formulare giudizi, utilizzando parole e concetti a
>    cui non corrisponde nulla di scientifico che possa dare un minimo di
>    competenza e plausibilità al giudizio che formula. E che dignità ha
>    l’ordine psichiatrico, quando non reagisce a sentenze che esprimono giudizi
>    di competenza psichiatrica, con un linguaggio che, se fosse usato da uno
>    specializzando in psichiatria, costui si sentirebbe invitato a cambiare
>    immediatamente ordine di studi?
>    
>    Gli psichiatri hanno dimenticato che la loro scienza è nata quando nel
>    1793, in Francia, Philippe Pinel liberò i folli dalle prigioni, dimostrando
>    che i folli erano appunto “folli” e non “delinquenti”, anche se poi li
>    rinchiuse in un’altra prigione che si chiama manicomio? E oggi che abbiamo
>    chiuso anche i manicomi cosa facciamo? Torniamo a rinchiudere i folli in
>    prigione, per aver derubricato “l’agire senza movente” dai sintomi della
>    follia, quando invece è il primo segno del deragliamento della ragione, la
>    cui procedura è leggibile nella rigorosa consequenzialità del rapporto
>    causa-effetto, azione-motivazione?
>    
>    Se concediamo, come ci insegna Eugenio Borgna, che la “malattia mentale”
>    non è una condizione stabile e definitiva che interdice perennemente la
>    mente, impedendo alla persona di “intendere” e “volere”, allora dobbiamo
>    ammettere che persino gli esecutori dei più efferati delitti “senza
>    movente” dispongono della loro mente e della loro volontà, anche se in
>    occasione del delitto non ne dispongono liberamente per l’influsso di
>    passioni che, come ci insegna Platone “ottundono la mente”.
>    
>    […]
>    
>    Questo problema la psichiatria francese lo dibatte dal 1835, quando un
>    giovane contadino normanno, Pierre Rivière, sgozza una sorella, un fratello
>    e la madre per “liberare” il padre dalla persecuzione della moglie.
>    Arrestato dopo un mese di latitanza, Rivière stende una “memoria” in cui
>    racconta la storia della sua famiglia e i moventi del suo gesto.
>    
>    Questo straordinario documento, di una lucidità sorprendente, pone i
>    giudici, che si interrogano, davanti alla domanda:  che cos’è veramente la
>    follia? È possibile che un criminale perda la sua ragione per un istante e
>    la recuperi in seguito? È possibile delirare anche per mesi, anche in due
>    (folie à deux), su un solo oggetto o un solo tema (idea fissa) e conservare
>    intatte tutte le proprie percezioni e ideazioni? Sì, è possibile. Ben
>    presto il caso Rivière supera i limiti del fatto di sangue per diventare un
>    momento significativo di discussione tra il potere politico, quello
>    giudiziario, quello medico e quello giornalistico. Michel Foucault e i suoi
>    allievi del Collège de France hanno discusso e raccolto in un volume tutta
>    la documentazione apparsa sulla vicenda: le perizie medico-legali, le
>    dichiarazioni dei testimoni, gli articoli dei giornali, la “memoria” di
>    Rivière, e dopo centocinquant’anni da quell’episodio in Francia si tornò a
>    discutere non più del fatto ovviamente, ma dei problemi che quel fatto
>    aveva sollevato in ordine alla possibilità di una migliore acquisizione, da
>    parte del potere giudiziario, delle conoscenze guadagnate dal sapere
>    psichiatrico.
>    
>    Una giustizia, infatti, che non fa tesoro delle competenze scientifiche è
>    una giustizia che finisce con l’essere “primitiva”, perché si limita a
>    soddisfare i sentimenti di vendetta o di risarcimento, prescindendo dalle
>    competenze che una scienza, la psichiatria, ha faticosamente guadagnato in
>    due secoli di storia, facendo fare alla nostra civiltà quel progresso che
>    ha consentito di portare i folli prima fuori dalle prigioni, perché non
>    sono accomunabili ai delinquenti, e poi fuori da quelle prigioni che sono i
>    manicomi, perché la follia non è uno stato permanente che impedisce di
>    “intendere” e “volere”, ma, come scrive Franco Basaglia, “uno stato
>    temporaneo di crisi”.
>    
>    In gioco qui non è l’esser “giustificazionisti” o “giustizialisti”, ma la
>    disponibilità dell’ordine giudiziario ad aprirsi a competenze e quindi a
>    disporre di strumenti adeguati e non arretrati per giudicare. Del resto le
>    sentenze giudicano i fatti, ma ci sono alcuni fatti che aprono problemi,
>    che non possono essere chiusi come si chiudono le tombe o le porte delle
>    carceri, altrimenti la giustizia resta troppo elementare e finisce col non
>    essere mai all’altezza del progresso scientifico e dell’evoluzione sociale.
>    […]

[1]:https://some1elsenotme.wordpress.com/2013/08/06/discorsi-sullintelligenza/
