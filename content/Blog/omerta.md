title: Omertà
date: 2015-03-15 12:38:50 +0100
tags: libertà, privacy, rete, sicurezza, tecnologia
summary: In occasione della premiazione agli Academy Award del documentario Citizenfour sulla fuga di Edward Snowden e la pubblicazione dei documenti segreti della NSA che provano il programma di sorveglianza globale della comunicazioni statunitense e di altri stati, riprendo un articolo che scrissi sul vecchio blog nel periodo in cui iniziarono a venir pubblicate le notizie.

In occasione della premiazione agli Academy Award del documentario [Citizenfour][1] sulla fuga di Edward Snowden e la pubblicazione dei documenti segreti della NSA che provano il programma di sorveglianza globale della comunicazioni statunitense e di altri stati, riprendo un articolo che scrissi sul vecchio blog nel periodo in cui iniziarono a venir pubblicate le notizie.

Se giudicassimo le azioni di persone come Snowden, Bradley Manning o Julian
Assange, secondo gli insegnamenti morali con cui siamo soliti giudicare le
persone che, mettendo a repentaglio la propria stessa sicurezza, denunciano i
crimini della malavita organizzata, dovremmo considerarli degli eroi. Di
controverso, se giudicassimo le reazioni degli stati denunciati da questi eroi,
noteremmo delle forti analogie con quelle che ci aspettiamo dai malavitosi nei
confronti di chi li denuncia e testimonia contro di essi.

[Bradley Manning][2], ex militare, testimone dei comportamenti immorali ed
illegali dell'esercito degli Stati Uniti, denunciò quello che sapeva, passando
molti documenti incriminanti a [WikiLeaks][3], che si occupò di fornirli ai
maggiori organi di stampa mondiali, facendo sì che *la gente venisse messa al
corrente dei crimini* che i militari statunitensi ed i loro alleati stavano
commettendo. Fra questi documenti, ad esempio, c’è il [famoso filmato][4] in
cui si vedono degli aviatori, a bordo del proprio elicottero, *uccidere
volontariamente dodici persone, civili*.

[Edward Snowden][5], ex sistemista della [NSA][6], testimone dei comportamenti
immorali ed illegali dei servizi segreti statunitensi, denunciò quello che
sapeva, passando molti documenti incriminanti a vari giornali mondiali, fra cui
il [The Guardian][7] britannico, mostrando alla gente come i servizi segreti
statunitensi [pagano][8] i maggiori provider e software house (Microsoft, Skype,
Apple, Google, Facebook, Dropbox, Yahoo, Paltalk, AOL…), per spiare e
memorizzare sistematicamente le comunicazioni dei propri cittadini e dei
cittadini stranieri che utilizzano servizi informatici e di comunicazione
statunitensi.

![Dates when PRISM collection began for each provider]({filename}/images/prism-slide-51.jpg "Dates when PRISM collection began for each provider")

Questi programmi di sorveglianza globale, prendono il nome di [PRISM][9] ed
[Xkeyscore][10], a cui si aggiunge il britannico [Tempora][11], ad opera del
[GCHQ][12], i dati raccolti vengono utilizzati anche dai servizi segreti e la
polizia di altri paesi alleati (ad esempio, [furono usati][13] dalla polizia
neozelandese per organizzare l'incursione, poi dichiarata illegale, nella casa
del proprietario di [Megaupload][14]) ed i server utilizzati per questi
programmi di sorveglianza sono sparpagliati in tutto il mondo.

![Where is X-Keyscore]({filename}/images/nsa-x-keyscore-servers.jpg "Where is X-Keyscore") 

*Manning* finì processato e, nel frattempo, incarcerato e detenuto in
condizioni inumane.

> l'avvocato e opinionista Glenn Greenwald, in un articolo del 15 dicembre,
> denuncia le condizioni inumane a cui è sottoposto il soldato, che per gli
> standard di alcuni paesi costituirebbero tortura, sottolineando che Manning
> non è stato condannato per alcun reato.
> 
> David House, informatico e ricercatore, che visita Bradley due volte al mese,
> riferisce che Manning viene tenuto in isolamento per 23 ore al giorno, dorme
> con le luci accese e viene controllato ogni cinque minuti. Inoltre è
> costretto a dormire indossando soltanto un paio di pantaloncini, esponendo la
> pelle a diretto contatto con una coperta molto simile a un tappeto. Durante
> la notte viene svegliato dalle guardie se non completamente visibile. l'unica
> forma di esercizio consentitagli consiste nel camminare in circolo in una
> stanza per un’ora al giorno, e viene incatenato durante le rare occasioni in
> cui può ricevere visite. David House riferisce anche che le sue condizioni di
> salute psicofisica sono in peggioramento e che Bradley è catatonico.
> 
> A marzo il portavoce del dipartimento di stato americano PJ Crowley si
> dimette in seguito alle sue dichiarazioni riguardo al trattamento di Bradley
> Manning, da lui definito “ridicolo, controproducente e stupido”.
**Wikipedia IT http://it.wikipedia.org/wiki/Bradley_Manning#Accusa_e_detenzione**

Ora è stato *condannato definitivamente* a 35 anni di carcere con ventidue capi
di accusa. Dopo il pronunciamento della sentenza si è detto pentito di ciò che
fece.

*Snowden* è perseguitato dalla legge ed attualmente in esilio in *cerca di asilo
politico*. Temporaneamente ne ha ottenuto uno dalla Russia.

Il giornale *The Guardian*, ha subito [un’incursione dei servizi segreti britannici][15],
il [GCHQ][12]. Durante quest'incursione, sono stati distrutti PC e dischi fissi.

> The mood toghened just over a month ago, when I received a phone call from
> the centre of government telling me: ‘Yoùve had your fun. Now we want the
> stuff back.’ … one of the more bizarre moments in the Guardian’s long history
> occurred — with two GCHQ security experts overseeing the destruction of hard
> drives in the Guardian’s basement just to make sure there was nothing in the
> mangled bits of metal which could possibly be of any interest to passing
> Chinese agents. ‘We can call off the black helicopters,’ joked one as we
> swept up the remains of a MacBook Pro.
**The Guardian http://www.theguardian.com/commentisfree/2013/aug/19/david-miranda-schedule7-danger-reporters**

*David Miranda*, compagno di *Glenn Greenwald*, il giornalista che si è
occupato di pubblicare le informazioni passategli da Snowden, fu arrestato
all'aeroporto di Heatrow e [detenuto per nove ore][16] con l'accusa di
terrorismo, subendo interrogatori, perquisizioni ed, infine, *sequestro di tutte
e sue apparecchiature elettroniche*.

> The partner of the Guardian journalist Glenn Greenwald, who has written a
> series of stories revealing mass surveillance programs by the National
> Security Agency (NSA), was held for almost nine hours on Sunday by UK
> authorities as he passed through the Heathrow airport on his way home to Rio
> de Janeiro. David Miranda was stopped by officers and informed that he would
> be questioned under the Terrorism Act 2000. The 28-year-old was held for nine
> hours, the maximum the law allows before officers must release or formally
> arrest the individual.  According to official figures, most examinations last
> under an hour, and only one in 2,000 people detained are kept for more than
> six hours. Miranda was released without charge, but officials confiscated
> electronics including his mobile phone, laptop, camera, memory sticks, DVDs
> and games consoles. ‘This is a profound attack on press freedoms […] to
> detain my partner for a full nine hours while denying him a lawyer, and then
> seize large amounts of his possessions, is clearly intended to send a message
> of intimidation to those of us who have been reporting on the NSA and GCHQ,’
> Greenwald commented.
**The Guardian http://www.theguardian.com/world/2013/aug/18/glenn-greenwald-guardian-partner-detained-heathrow**

[Julian Assange][17], capo redattore di WikiLeaks è in esilio ed *in attesa di
asilo politico* dall'Ecuador. Rischia di venire *estradato* negli Stati Uniti,
dove dovrà subire un processo con l'accusa di *spionaggio*.

Bradley Manning e Edward Snowden *non erano gli unici a conoscenza* degli
illeciti che decisero di denunciare.

Tutte le persone elencate, sono state accusate di essere traditori e spie, di
mettere in pericolo gli stati di cui stavano denunciando i reati dei rispettivi
governi, polizie ed eserciti.

Ad un’osservazione superficiale, ciò che è accaduto alla redazione del The
Guardian è un’azione volta ad impedire che altre notizie compromettenti,
pericolose per la sicurezza nazionale, vengano pubblicate. Chiunque, tuttavia,
può arrivare a pensare che i dati distrutti in modo così scenico, erano molto
probabilmente stati salvati anche altrove e, se a questo si aggiunge l'episodio
dell'arresto del compagno di Glenn Greenwald, si ottiene che ciò che rimane, è
solo *l'atto intimidatorio*.

Il paradosso appare abbastanza evidente, specialmente se si fa un parallelo con
ciò che ci è sempre stato insegnato riguardo al *dovere civico* di contribuire
al rispetto della legge, in special modo nelle situazioni in cui è coinvolta la
criminalità organizzata.

Ci è stato insegnato e continuamente ripetuto che il **testimone** che denuncia
un crimine, nel caso il crimine sia stato commesso da qualche privato, è
effettivamente un testimone e bravo cittadino; nel caso sia stato commesso dalla
malavita organizzata, considerando il pericolo che corre, è un testimone che
*necessita di protezione* e, per il coraggio che dimostra nonostante le
intimidazioni che potrebbe aver subito, viene spesso definito “eroe”, mentre
viene disprezzato e *chiamato traditore o spia dai suoi ex compagni malavitosi*.

Poco sopra, abbiamo visto alcuni casi in cui i crimini sono stati commessi da
governi, le cui reazioni sono evidentemente molto più simili a quelle dei
malavitosi, che non a quelle dei democratici custodi delle leggi, che ci hanno
sempre insegnato a considerare.

Da questo, segue che, secondo il nuovo vocabolario:

- Il testimone di crimini commessi dai governi è un traditore ed una spia.
- L'omertoso, colui che tace, è un bravo *cittadino e patriota*.

*Corollario:* Nel caso in cui sia il governo stesso a far trapelare informazioni
riservate che potrebbero mettere in pericolo i propri agenti, allora non c’e`
alcun problema. Vedi il caso di [Valerie Plame][17].

Personalmente, mi sento più al sicuro sapendo che in giro ci sono anche
traditori e disertori.

- [Citizenfour][1]

[1]:https://en.wikipedia.org/wiki/Citizenfour
[2]:https://it.wikipedia.org/wiki/Bradley_Manning
[3]:https://it.wikipedia.org/wiki/Wikileaks
[4]:https://www.youtube.com/watch?v=to3Ymw8L6ZI
[5]:https://it.wikipedia.org/wiki/Edward_Snowden
[6]:https://it.wikipedia.org/wiki/National_Security_Agency
[7]:https://it.wikipedia.org/wiki/The_Guardian
[8]:https://www.commondreams.org/headline/2013/08/23-2
[9]:https://en.wikipedia.org/wiki/PRISM_(surveillance_program)
[10]:https://en.wikipedia.org/wiki/X-Keyscore
[11]:https://en.wikipedia.org/wiki/Tempora
[12]:https://it.wikipedia.org/wiki/GCHQ
[13]:http://www.itnews.com.au/News/354407,nz-police-affidavits-show-use-of-prism-for-surveillance.aspx
[14]:https://it.wikipedia.org/wiki/Megaupload
[15]:http://www.theguardian.com/commentisfree/2013/aug/19/david-miranda-schedule7-danger-reporters
[16]:http://www.theguardian.com/world/2013/aug/18/glenn-greenwald-guardian-partner-detained-heathrow
[17]:https://en.wikipedia.org/wiki/Valerie_Plame
