title: Più che umano
date: 2016-08-28 20:58
tags: fantascienza, recensioni, libri
summary: Più che umano di Theodore Sturgeon era rimasto a prendere la polvere sulla mia scrivania per anni. Un grosso errore, perché questo è uno dei più bei romanzi fantascientifici che abbia letto.  Il tema è la nascita di una nuova evoluzione del genere umano, che emerge da uno sviluppo psichico collettivo, in contrasto alle precedenti evoluzioni fisiche del _homo_.

Più che umano di [Theodore Sturgeon][1] era rimasto a prendere la polvere sulla
mia scrivania per anni. Un grosso errore, perché questo è uno dei più bei
romanzi fantascientifici che abbia letto.

Il tema è la nascita di una nuova evoluzione del genere umano, che emerge da uno
sviluppo psichico collettivo, in contrasto alle precedenti evoluzioni fisiche del
_homo_.

Mi piace l'approccio a quello che è un tema complesso e di grande portata,
descritto attraverso le vite ed i sentimenti delle persone che compongono questo
_umano gestalt_, non come termiti di un termitaio, cellule di un organismo o
parti di una noiosa mente alveare, ma come individui che contribuiscono
all'emergere di un qualcosa di evolutivamente più avanzato, consapevoli della
propria ed altrui condizione, ma non del tutto consapevoli di ciò che emerge dal
totale.  Come ci si sente ad essere organo di un organismo? E come quando si sa
che una parte di quell'organismo è malata o che la sua morte porterebbe alla
scomparsa dell'organismo stesso? Ed infine, come si deve approcciare questa
nuova e solitaria entità alle altre specie?

Non posso giudicare la prosa, avendo letto l'adattamento italiano, ma ciò che ho
letto riesce ad essere di una poesia ed insieme di una brutalità sorprendente,
specialmente nel primo dei tre episodi. Si nota proprio una netta differenza fra
il primo episodio e gli altri. Nel primo episodio, che riesce in alcuni passaggi
ad essere anche commovente, vi è la gemma di tutto il romanzo e sarebbe
sufficiente a se stesso; gli altri due sono approfondimenti ed elaborazioni sul
tema, ma la cui qualità narrativa, secondo me, è inferiore.  L'intreccio e lo
svolgimento di entrambi è infatti molto simile: un duro scavare nei ricordi di
un passato rimosso. Questo, ed il ritmo molto più dilatato e meno appassionato
rispetto al primo episodio, li rende leggermente meno avvincenti, anche se
abbastanza per mantenere l'interesse del lettore. Il finale non mi ha
soddisfatto pienamente... ma la bellezza del primo episodio e lo svolgimento del
tema lungo tutto il romanzo, mi hanno appassionato abbastanza da aver voluto
leggere il libro in un solo boccone e giudicarlo comunque ottimamente. Anche il
romanzo, a quanto pare, è maggiore della somma delle sue parti.

[1]:https://it.wikipedia.org/wiki/Theodore_Sturgeon
