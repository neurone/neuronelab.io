title:  Le ragioni del veganismo
date:   2015-07-21 10:17
tags:   antispecismo, etica, filosofia, veg, ecologia, diritto
summary: Appunti di una conferenza col filosofo Luigi Lombardi Vallauri.

Sul [sito di Luigi Lombardi Vallauri][2] si possono trovare anche le lezioni,
trasmesse nel 2004, 2005 e 2007 su Radio Rai Tre: *Meditare in Occidente*.

*In generale* sono tutte quelle della nonviolenza e dell'animalismo. Nello
specifico vedrei come utili alla discussione almeno sei argomenti.

1. ETICO
    1. Gli animali sono esseri senzienti, capaci di dolore, soggetti di una vita
       (animalismo animalista)
    2. La violenza sugli animali disonora l'uomo come soggetto morale
       (animalismo umanista)
    3. La crescita spirituale dell'uomo passa attraverso una decrescita della
       violenza (animalismo spirituale)

2. ECOLOGICO
    1. Mangiare animali riduce la biodiversità, la bellezza del mondo:
       deforestazione, desertificazione, riduzione delle specie animali e
       vegetali, impoverimento dei mari e delle acque (argomento
       ecologico-estetico)
    2. Mangiare animali riduce la salubrità dell'ambiente: inquina l'aria, le
       acque, i suoli (argomento ecologico-igienico)
    3. Mangiare animali esaurisce risorse naturali essenziali: consuma acqua,
       piante in misura insostenibile (argopmento ecologico-economico)

3. ECONOMICO
    1. Mangiare animali non è indispensabile al PIL
    2. Nel prezzo dei cibi animali bisognerebbe includere le esternalità, che
       invece vengono caricate sui beni comuni
    3. Il valore dei beni/servizi offerti dalla natura, monetizzato, è altissimo
       (argomento economico-ecologico)

4. SOCIALE  
    Mangiare animali è costoso, possono permetterselo i ricchi. Più mangiano
    animali i (nuovi) ricchi, meno hanno da mangiare i (nuovi) poveri. Mangiare
    animali accresce la fame umana nel mondo (argomento della giustizia
    alimentare umana)

5. DIETETICO  
    La dieta vegan fatta bene è la più vicina alla dieta ideale per la salute e
    la longevità umana

6. GASTRONOMICO  
    Mangiare vegan può essere edonisticamente accettabile: le piante e le loro
    combinazioni sono più varie delle carni e delle loro combinazioni

Decisivo ("categorico" nel senso kantiano) è comunque l'argomento etico.

Le ragioni dell'animalismo
--------------------------

Gli animali sono meritevoli di tutela in base a due criteri: il valore e la
soggettività. Il loro *valore* è quello di bioarchitetture meravigliose per
ingegneria, grazia, mistero, sconfinata fantasia; vivificano con la propria
presenza gli ecosistemi, ispirano potentemente, in tutte le culture,
l'autocomprensione dell'uomo. La tutela in base al valore, che riconosce loro lo
status di beni equiparabili ad altri beni ambientali o ai beni culturali,
s'iscrive nel quadro più ampio della tutela della biodiversità: difendere contro
l'invasione antropocentrica moderna la bellezza/ricchezza immemoriale del mondo.

La *soggettività* si accerta attraverso lo studio dei sistemi nervosi centrali e
dei comportamenti. Segnatamente i vertebrati sono esseri senzienti, comunicanti,
in grado di soffrire, godere, apprendere, provare affetti, emozioni, sviluppare
capacità: doti che in condizioni favorevoli si manifestano pienamente ma che
subiscono una mortificazione quasi totale nella dismisura della violenza cui le
vittime sono sottoposte dentro gli allevamenti intensivi, gli impianti di
macellazione, i lavoratori di sperimentazioni/vivisezione.

Di fronte all'attuale barbarie sarebbe comunque preferibile uno scenario *vita
degna - morte indolore*; preferibile ma non aproblematico, in quanto la
privazione di una vita degna massimizza il danno.

All'**animalismo ambientalista** (del valore) e all'**animalismo animalista**
(della soggettività) è importante affiancare un *animalismo umanista*, in difesa
dell'onore dell'uomo. L'uomo è disonorato dal modo in cui tratta gli animali.
Non è "sviluppo della persona" (art 3.2 della Costituzione italiana) maltrattare
e uccidere gli animali, servirsi per cibo, vestito, dei loro corpi reificati.
L'imperativo di pietà e giustizia si estende anche sul carnefice, non solo sulla
vittima. *Noblesse oblige*: la nobiltà crea doveri, non privilegi. I due
animalismi sono sinergici: più è vero che gli animali sono senzienti e
intelligenti, più sono gravi i doveri dell'uomo nei loro confronti; più è vero
che l'uomo supera gli animali non umani in razionalità e spiritualità, più sono
gravi i suoi doveri nei loro confronti.

E il *diritto*? Tutte le ricerche registrano uno spostamento del baricentro del
diritto, negli ultimi due decenni, da un'attenzione quasi esclusiva per gli
interessi animali a una qualche considerazione per gli interessi animali.
L'articolo 13 del Trattato di Lisbona del 2009, norma europea di rango
paracostituzionale, statuisce che "l'Unione e gli Stati membri tengono
pienamente conto delle esigenze in materia di benessere degli animali in quanto
esseri senzienti". Il Titolo IX-bis del codice penale italiano, entrato in
vigore nel 2004, prevede sanzioni carcerarie e pecuniarie a carico di chi, per
crudeltà o senza necessità, uccida, maltratti, abbandoni, detenga animali in
condizioni produttive di gravi sofferenze. Esistono leggi di protezione delle
diverse specie animali destinate alla macellazione. Al tempo stesso la
macellazione è espressamente esentata, insieme alla caccia, alla pesca, alla
sperimentazione/vivsezione, dalla qualifica di "uccisione" e di
"maltrattamento". La contraddizione delle leggi speciali con i principi generali
e con il realismo ontologico è evidente e può sanarsi solo attraverso un sempre
maggiore riconoscimento dei diritti animali.

Alle quattro forti ragioni fin qui evocate vorrei, a titolo personale,
aggiungerne una quinta, più selettiva: una ragione *spirituale*. Nessun uso
violento degli animali non necessario, cioè finalizzato principalmente al piacere
o al guadagno, è [dharmico][2], perché il *dharma* include come elemento essenziali
la nonviolenza (*ahimsa*), l'amore-compassione (*karuna*). La violenza sugli
esseri senzienti, sia quella consapevole e culturalmente/religiosamente
legittimata, sia quella non consapevole, perpetrata per abitudine e
psicologicamente rimossa, non può non costituire un ostacolo sulla via verso la
liberazione sapienziale, verso la mente dell'illuminazione-beatitudine, che non
è concepibile come egoica e priva di compassione.
    

[1]:https://it.wikipedia.org/wiki/Luigi_Lombardi_Vallauri
[2]:http://www.lombardivallauri.it/LLV/LLV.html
[3]:https://it.wikipedia.org/wiki/Dharma
