title: L'uomo che venne dalla Terra
date: 2015-07-02 19:25
tags: recensioni, film, fantascienza
summary: Se non vi sareste mai aspettati di guardare un film di fantascienza girato come un lungo dialogo fra personaggi seduti nel salotto di una casa di montagna, dovrete guardare *“L’uomo che venne dalla Terra”*.

Se non vi sareste mai aspettati di guardare un film di fantascienza girato come un lungo dialogo fra personaggi seduti nel salotto di una casa di montagna, dovrete guardare *“L’uomo che venne dalla Terra”*.

In un periodo in cui, nella fantascienza cinematografica, sull’altare della spettacolarità vengono continuamente sacrificate la sceneggiatura, la logica e la coerenza, questo film fa l’esatto opposto proponendo una trama intelligente, verosimile, coerente e dei personaggi intelligenti e vitali impegnati nel tentativo di comprendere o confutare la storia che viene loro narrata.

![Locandina]({filename}/images/man_from_earth.jpg){: style="float:right"} *John
Oldman* è un uomo che sostiene di essere vissuto 14.000 anni, nato come uomo di
[Cro-Magnon][1] e, nel corso della sua vita, migrato in continuazione da luogo
a luogo nel tentativo di comprendere perché non invecchiasse e di nascondere
questa caratteristica agli altri uomini. Non riesce a ricordare i dettagli di
tutta la storia della civiltà umana che ha incontrato, allo stesso modo in cui
l’oblio della memoria rimuove dalle nostre menti i ricordi che sembrano meno
salienti, ma ha vissuto ed incontrato molte personalità notevoli della storia,
ed anche lui, in una occasione lo è stato. È stato testimone delle
trasformazioni di uomini ed insegnamenti estremamente saggi nella loro
semplicità, in complessi miti religiosi, prima, e strumenti di potere poi.
Raccontando tutto questo ai suoi amici, quasi costretto, prima di doversi
nuovamente trasferire, incontrerà la loro incredulità, le loro reazioni,
talvolta aggressive – per difendere le proprie convinzioni e conoscenze. Essi
si sentiranno feriti, illuminati, scettici, preoccupati per lui; e con loro
noi, che ascoltiamo assieme, questa narrazione. Un film di fanta-antropologia e
filosofico, che merita almeno una visione, se non altro per la sua unicità
all’interno di questo genere.

Il film è costato “appena” 200.000 dollari e la sceneggiatura è ad opera di
[Jerome Bixby][2], già sceneggiatore di alcuni episodi di [Star Trek][3] e [Ai
confini della realtà][4].

[1]:https://it.wikipedia.org/wiki/Uomo_di_Cro-Magnon
[2]:https://en.wikipedia.org/wiki/Jerome_Bixby
[3]:https://it.wikipedia.org/wiki/Star_Trek
[4]:https://it.wikipedia.org/wiki/Ai_confini_della_realt%C3%A0_(serie_televisiva_1959)
