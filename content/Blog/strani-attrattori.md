title: Strani Attrattori
date: 2014-12-30 15:07:16 +0100
tags: fantascienza, libri, recensioni
summary: La qualità dei racconti, articoli, poesie e saggi contenuti in questa raccolta è, secondo me, elevatissima. Lo si può immaginare, anche leggendo l'elenco degli autori, fra cui Bruce Sterling, William Gibson, J.G. Ballard, William S. Burroughs, Robert Anton Wilson.

La raccolta è curata da Hakim Bey, già autore del famoso classico
dell'underground *T.A.Z. Zone Temporaneamente Autonome*, assieme a Rudy Rucker e
[Robert Anton Wilson][1], co-autore assieme a [Robert Shea][2] della trilogia
degli *Illuminati!*.  
La qualità dei racconti, articoli, poesie e saggi contenuti in questa raccolta è, secondo me, elevatissima. Lo si può immaginare, anche leggendo l'elenco degli autori, fra cui Bruce Sterling, William Gibson, J.G. Ballard, William S. Burroughs, Robert Anton Wilson.

I pezzi contenuti sono spesso a sfondo distopico e satirico, irriverenti ed
oltraggiosi, talvolta illuminanti o surreali. Ne consiglio la lettura a chiunque
abbia desiderio di letture *alternative*.
Sul sito della *Shake* è possibile leggere il primo racconto della raccolta:
[Metamorfosi N. 89][3]. Bello, anche se non fra i miei preferiti. In futuro,
potrei trascriverne uno o due a scopo esemplificativo.

Quarta di copertina
-------------------

Alla fine degli anni Ottanta, sulle migliori riviste e fanzine di fantascienza
americane e inglesi, viene pubblicato un annuncio che invita a spedire
contributi per la redazione di un’antologia riservata a brani censurati dagli
editori “commerciali” a causa della durezza degli argomenti trattati o dello
stile spericolato. Il risultato sconcerta gli stessi promotori dell’iniziativa:
“classici” come Farmer, “cattivi maestri” della new wave come Ballard e
Burroughs, i migliori esponenti del cyberpunk, primi tra tutti Sterling e
Gibson, e le “speranze” della nascente scena underground rispondono
entusiasticamente inviando stupendi racconti brevi.

Questa raccolta viene allora affidata a tre grandi maestri della scrittura
fantascientifica e radicale: il prodotto finale è una miscela esplosiva – ad
altissimo livello – di sperimentalismi, argomenti scottanti, provocazioni
culturali, statement politici e humour nerissimo.

Finalmente anche in Italia una delle più importanti antologie di fantascienza,
considerata dai critici allo stesso livello di Dangerous Visions e Mirrorshades,
una raccolta che ha messo in luce le gravi contraddizioni dell’editoria
mainstream e le potenzialità creative della più completa libertà di scrittura.
Completano il volume le illustrazioni dei migliori artisti della scena
alternativa americana.

Edizione italiana a cura di Fabio Gadducci e Mirko Tavosanis.

*Strani attrattori è una raccolta di piccoli capolavori di letteratura
d’avanguardia e fantascientifica, in cui sono presenti e sapientemente miscelati
straordinari racconti di Bruce Sterling, John Shirley, William Gibson, J.G.
Ballard, Paul Di Filippo, Richard Kadrey, Hakim Bey, Lewis Shiner, W.S.
Burroughs, Marc Laidlaw, Colin Wilson, Philip J. Farmer, Rudy Rucker, Don Webb e
altri...*

[1]:https://en.wikipedia.org/wiki/Robert_Anton_Wilson
[2]:https://en.wikipedia.org/wiki/Robert_Shea
[3]:http://www.shake.it/index.php?id=1004
