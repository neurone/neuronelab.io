title: I contrari alle vaccinazioni pensano di saperne più dei medici esperti
date: 2018-08-13 10:48
tags: scienza, vaccini, psicologia

Tradotto da [www.psypost.org][1]

Uno dei settori più controversi delle politiche sanitarie degli ultimi due decenni è stato quello riguardo alla sicurezza delle vaccinazioni. I vaccini prevengono le epidemie di [malattie in passato molto diffuse][2], come la polio; ed il consenso scientifico [sostiene fermamente la loro sicurezza][3]. Eppure, molti americani [rifiutano][4] o [ritardano][5] la vaccinazione dei propri figli, spaventati dalla possibilità che possa portare all'autismo, nonostante il consenso scientifico [confuti questa affermazione][6].

Le opinioni anti-vacciniste sono state alimentate in gran parte dal [numero crescente di casi di autismo diagnosticati][7], così come da uno [studio su The Lancet][8], [ora sfatato][9], che collegava l'autismo al vaccino morbillo-parotite-rosolia (MPR) - spingendo molti genitori a vedere nella vaccinazione una potenziale spiegazione per la l'autismo diagnosticato nei propri figli.

Il crescente movimento "anti-vax", locale ed estero, ha visto genitori [rifiutarsi di fornire ai propri figli le vaccinazioni scolastiche obbligatorie][10], [un crescente numero di celebrità][11] mettere in dubbio la sicurezza dei vaccini, ed addirittura [proprietari di animali rifiutarsi di vaccinare i propri cani][12] - obbligando l'Associazione Veterinaria Britannica a emanare in aprile [una dichiarazione][13], che affermava che i cani non possono sviluppare l'autismo.

Dato il [consistente messaggio][3] dalla comunità scientifica riguardo alla sicurezza dei vaccini e le prove dell'efficacia degli stessi, visibile dall'eradicazione delle malattie, per quale motivo lo scetticismo verso i vaccini perdura?

Una possibilità è che l'atteggiamento verso i medici esperti, possa aiutare a spiegare perché vengono abbracciate le opinioni anti-vax. Nello specifico, [partendo da uno studio precedente][14], il nostro team di ricerca sostiene che alcuni adulti statunitensi potrebbero appoggiare le posizioni politiche anti-vax, in parte perché credono di saperne di più dei medici esperti riguardo all'autismo ed alle sue cause. Abbiamo voluto testare questa teoria.

Scetticismo sui vaccini e conoscenza
------------------------------------

Quella delle vaccinazione è stata una delle più grandi storie di successi della salute pubblica. Portò all'eradicazione del vaiolo ed alla diffusa eliminazione della polio. [Eradicazione][15] di una malattia, significa che è stata permanentemente spazzata via e che non sono più necessari altri interventi; fino ad ora, il vaiolo è stato l'unica malattia ad essere stata eradicata. [Eliminazione][15] significa la riduzione a zero dell'incidenza in una specifica area geografica come risultato di interventi intenzionali. La vaccinazione ha protetto milioni di persone dalle devastazioni del tetano, della pertosse e della varicella.

Eppure, lo scetticismo sui vaccini persiste, estendendosi al regno della politica, dove molti politici mettono in dubbio la sicurezza dei vaccini. Degno di nota, il presidente Donald Trump ha messo in discussione le [credenziali dei medici che chiedevano le vaccinazioni][16], ha spinto per rallentare i programmi di vaccinazione ed ha scelto Robert Kennedy Jr., scettico dei vaccini, a presiedere un gruppo amministrativo sulla sicurezza dei vaccini.

Ci siamo chiesti: Potrebbe essere l'incapacità degli anti-vaccinisti di valutare accuratamente le proprie conoscenze e abilità rispetto a quelle dei medici esperti a giocare un ruolo nel plasmare le loro posizioni verso i vaccini? Questa incapacità di valutare al propria conoscenza viene chiamata Effetto Dunning-Kruger, ed è stato inizialmente [identificato dalla psicologia sociale][18]. L'effetto [Dunning-Kruger][19] si presenta quando la mancanza di conoscenza degli individui su di un particolare argomento, li porta a valutare inaccuratamente la propria competenza su di esso. L'ignoranza sulla propria stessa ignoranza può portare le persone carenti di conoscenza su di un argomento a ritenersi più esperti rispetto a coloro che sono comparativamente meglio informati. Noi ci riferiamo ad esso come "eccesso di fiducia".

L'effetto Dunning-Kruger e le posizioni anti-vax
------------------------------------------------

Per provare le nostre ipotesi, nel dicembre 2017, la nostra ricerca ha chiesto a più di 1.300 americani di confrontare il proprio livello di conoscenza percepito riguardo le cause dell'autismo a quello di medici e scienziati. Dopo aver fatto ciò, abbiamo chiesto agli intervistati di rispondere sia ad una serie di domande di conoscenza fattuale riguardo all'autismo, che alla misura in cui essi sono d'accordo riguardo alla disinformazione sul potenziale collegamento tra vaccinazione infantile ed autismo.

Abbiamo scoperto che il 34 percento degli adulti statunitensi del nostro campione sentono di saperne altrettanto se non di più, rispetto agli scienziati, sulle cause dell'autismo. Un po' di più, il 36 percento, ha lo stesso sentore riguardo alla propria conoscenza in confronto ai medici.

Nel nostro campione abbiamo anche trovato forti evidenze di effetto Dunning-Kruger. Il sessantadue percento di coloro che hanno avuto i risultati peggiori nel nostro test sulla conoscenza dell'autismo, credevano di sapere altrettanto o di più riguardo alle cause dell'autismo, sia di dottori, che di scienziati, paragonato al solo 15 percento di coloro che hanno avuto i migliori risultati. Similmente, il 71 percento di coloro che appoggiano fortemente la disinformazione riguardo al collegamento tra vaccini ed autismo, sentono di saperne altrettanto o di più dei medici riguardo alle cause dell'autismo, paragonato al solo 28 percento di coloro che rifiutano fermamente quella disinformazione.

Abbiamo recentemente pubblicato le nostre scoperte sulla rivista [Social Science and Medicine][20].

Che effetto ha questo sulle politiche sui vaccini?
--------------------------------------------------

Le nostre ricerche hanno anche scoperto che questi effetti Dunning-Kruger portano importanti implicazioni sulle politiche sui vaccini.

Oltre a misurare la conoscenza sull'autismo, il nostro sondaggio chiedeva agli intervistati di condividere le loro opinini su diversi aspetti delle politiche sui vaccini. Ad esempio, abbiamo chiesto agli intervistati se appoggiano o no la scelta dei genitori di non vaccinare i propri bambini prima di mandarli alla scuola pubblica. Gli intervistati potevano rispondere se erano fortemente d'accordo, d'accordo, né d'accordo né in disaccordo, in disaccordo o fortemente in disaccordo con quella posizione.

Abbiamo trovato che circa un terzo, o 30 percento, delle persone che pensano di sapere più dei medici esperti sulle cause dell'autismo sostengono fortemente di dare ai genitori la libertà assoluta di non vaccinare i propri figli. Per contrasto, il 16 percento di coloro che non pensano di sapere più dei medici professionisti, sostenevano lo stesso.

Il nostro studio ha trovato inoltre che le persone che pensano di sapere di più dei medici esperti hanno una maggior tendenza a fidarsi delle informazioni sui vaccini provenienti da fonti non esperte, come le celebrità. Questi individui tendono maggiormente a sostenere persone non esperte per dei ruoli forti nei processi di creazione di politiche che riguardano vaccini e vaccinazioni.

Una battaglia in salita?
------------------------

In ultima analisi, i nostri risultati portano l'attenzione alla battaglia in salita che la comunità scientifica deve affrontare quando si confronta con il crescente sentimento anti-vax del pubblico e dei politici. Nonostante la montagna di prove sulla sicurezza e l'importanza dei vaccini portate da dottori e scienziati continui a crescere, molti americani pensano di saperne più degli esperti che cercano di correggere le loro false percezioni.

Pertanto, dovrebbe diventare una priorità il trovare nuovi modi di presentare il consenso scientifico sui vaccini ad un pubblico che è scettico verso i medici esperti. Il nostro studio suggerisce che un campo interessante per una futura ricerca potrebbe essere l'osservare se le informazioni pro-vaccini provenienti da fonti non esperte come le celebrità, possano persuadere chi ha posizioni anti-vaccini a cambiare idea.

*[di Mattew Motta][21], borsista post dottorato, [University of Pennsylvania][22]; [Steven Sylvester][23], professore assistente, politiche pubbliche, [Utah Valley University][24], e [Timothy Callaghan][25], professore assistente, Texas A&M University School of Public Health, [Texas A&M University][26]*

*Questo articolo è stato inizialmente pubblicato su [The Conversation][27]. Leggi [l'articolo originale][28].*

[1]:https://www.psypost.org/2018/08/opponents-of-vaccination-think-they-know-more-than-medical-experts-51932
[2]:https://www.cdc.gov/vaccines/parents/diseases/child/14-diseases.html
[3]:https://www.ncbi.nlm.nih.gov/pubmed/20669467
[4]:https://www.bcbs.com/the-health-of-america/reports/early-childhood-vaccination-trends-america
[5]:https://www.ncbi.nlm.nih.gov/pubmed/21969290
[6]:http://nationalacademies.org/hmd/reports/2004/immunization-safety-review-vaccines-and-autism.aspx
[7]:https://www.cdc.gov/ncbddd/autism/data.html
[8]:https://www.nytimes.com/2010/02/03/health/research/03lancet.html
[9]:https://www.nytimes.com/2015/02/02/us/a-discredited-vaccine-studys-continuing-impact-on-public-health.html
[10]:https://www.theatlantic.com/education/archive/2015/02/schools-may-solve-the-anti-vaccine-parenting-deadlock/385208/
[11]:https://nypost.com/2015/02/09/10-anti-vaccine-celebs-that-should-come-with-a-surgeon-generals-warning/
[12]:https://www.nytimes.com/2018/04/27/world/europe/britain-dogs-autism-vaccine.html
[13]:https://www.telegraph.co.uk/news/2018/04/25/dogs-cannot-get-autism-british-veterinary-association-warns/
[14]:https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4318315/
[15]:https://www.cdc.gov/mmwr/preview/mmwrhtml/su48a7.htm
[16]:https://www.washingtonpost.com/news/the-fix/wp/2017/01/10/donald-trump-is-rekindling-one-of-his-favorite-conspiracy-theories-vaccine-safety/?noredirect=on&utm_term=.353a6065d93c
[17]:https://www.statnews.com/2017/02/15/vaccines-trump-panel-kennedy/
[18]:https://www.avaresearch.com/files/UnskilledAndUnawareOfIt.pdf
[19]:https://pdfs.semanticscholar.org/8226/22ed711dfc0f63a232f31ac3163fb3cb8b55.pdf
[20]:https://www.sciencedirect.com/science/article/pii/S027795361830340X
[21]:https://theconversation.com/profiles/matthew-motta-472268
[22]:https://theconversation.com/institutions/university-of-pennsylvania-1017
[23]:https://theconversation.com/profiles/steven-sylvester-509500
[24]:https://theconversation.com/institutions/utah-valley-university-2123
[25]:https://theconversation.com/profiles/timothy-callaghan-310063
[26]:https://theconversation.com/institutions/texas-aandm-university-1672
[27]:https://theconversation.com/
[28]:https://theconversation.com/why-vaccine-opponents-think-they-know-more-than-medical-experts-99278
