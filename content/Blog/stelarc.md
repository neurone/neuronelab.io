title: Stelarc
date: 2014-08-07 18:56:51 +0200
tags: filosofia, filosofia della mente, arte, cibernetica, cyberpunk
summary: Stelarc è un artista ed uno sperimentatore molto particolare. Utilizza il proprio corpo come oggetto per le sue performance.

[Stelarc][1] è un artista ed uno sperimentatore molto particolare. Utilizza il proprio corpo come oggetto per le sue performance.

Una sorta di [body-art][2] [cibernetica][3]. [Esso][1] infatti, tramite protesi, interfacce, computer e collegamenti alla rete, esplora l’essere cyborg, l’estendere il proprio corpo sulla macchina ed il sentire il proprio corpo mosso indipendentemente dalla propria volontà. Oppure, in altre parole, studia cosa significa essere un corpo.

![Stelarc2]({filename}/images/stelarc2.jpg "Stelarc2"){: style="float:right"}
Alcune sue “invenzioni” famose sono il *terzo braccio*, pilotato
tramite il movimento dei muscoli addominali e delle onde elettriche che li
percorrono, il *braccio ed il corpo virtuali* che vengono pilotati tramite
gesti, suoni e respiro, lo *stimbod*, che permette ad un utente di scegliere su
di uno schermo quali muscoli dell’artista far muovere tramite delle
stimolazioni elettriche, la sua evoluzione, il *ping-body*, che permette questa
interazione a degli utenti collegati al [suo sito][4], vari esperimenti di
*sospensione* del corpo tramite ganci e contrappesi, ed il corpo esteso che
include oltre a varie protesi meccaniche anche impianti di luci ed audio.

Stelarc viene spesso associato alle idee del filosofo [Andy Clark][5],
principale promotore della teoria della [mente estesa][6].

In rete si possono trovare [molti video][7]. Queste sono alcune immagini delle sue
performance.

![Stelarc1]({filename}/images/stelarc1.jpg "Stelarc1")
![Stelarc3]({filename}/images/stelarc3.jpg "Stelarc3")
![Stelarc4]({filename}/images/stelarc4.jpg "Stelarc4")

[1]:https://en.wikipedia.org/wiki/Stelarc
[2]:https://it.wikipedia.org/wiki/Body_art
[3]:https://it.wikipedia.org/wiki/Cibernetica
[4]:http://stelarc.org/
[5]:https://en.wikipedia.org/wiki/Andy_Clark
[6]:https://en.wikipedia.org/wiki/Extended_mind
[7]:https://www.youtube.com/results?search_query=stelarc
