title: Dio è taoista?
date: 2014-07-22 21:02:38 +0200
tags: filosofia, filosofia della mente
summary: Racconto pubblicato su "L'io della mente", edito da Adelphi. *di Raymond Smullyan*

Racconto pubblicato su "L'io della mente", edito da Adelphi. *di [Raymond Smullyan][1]*

*Mortale:* Perciò, o Dio, io ti prego, se hai un briciolo di pietà per questa tua
creatura sofferente, liberami dal dover avere il libro arbitrio!

*Dio:* Tu rifiuti il dono più grande che io ti abbia fatto?

*Mortale:* Come puoi chiamare dono ciò che mi è stato imposto? Io ho il libero
arbitrio, ma non per mia scelta. Non ho mai scelto liberamente di avere il libro
arbitrio. Devo avere il libro arbitrio, che mi piaccia o no!

*Dio:* Perché vorresti non averlo?

*Mortale:* Perché il libero arbitrio significa responsabilità morale e la
responsabilità morale è un peso che non posso sopportare!

*Dio:* Perché trovi così insopportabile la responsabilità morale?

*Mortale:* Perché? A dire la verità non so spiegarne il perché; so soltanto che è
così.

*Dio:* D’accordo, in tal caso supponiamo che io ti assolva da ogni responsabilità
morale, ma ti lasci il tuo libero arbitrio. Questo ti andrebbe?

*Mortale (dopo una pausa):* No, temo di no.
<!--more-->

*Dio:* Ah, proprio come pensavo! Dunque la responsabilità morale non è l’unico
aspetto del libero arbitrio che non ti va. Che cos’altro ti disturba del libero
arbitrio?

*Mortale:* Col libero arbitrio io sono in grado di peccare e io non voglio
peccare!

*Dio:* Se non vuoi peccare, perché pecchi?

*Mortale:* Buon Dio! Non lo so perché pecco: pecco e basta! Vengono le tentazioni,
e per quanto mi sforzi non riesco a resistere.

*Dio:* Se è proprio vero che non riesci a resistere alle tentazioni, allora non
pecchi per tua libera scelta, e quindi (almeno secondo me) non pecchi affatto.

*Mortale:* No, no! Non posso fare a meno di pensare che se solo mi sforzassi di
più potrei evitare il peccato. Mi risulta che la volontà è infinita. Se uno con
tutto il cuore non vuole peccare, non pecca.

*Dio:* E dunque dovresti saperlo. Ti sforzi al massimo per non peccare, oppure no?

*Mortale:* Sul serio, non lo so! Al momento sento di fare tutti gli sforzi di cui
sono capace ma poi, quando ci ripenso, mi tormenta l’idea che forse non li ho
fatti.

*Dio:* In altre parole, insomma, non sai se hai peccato oppure no. Non si può
quindi escludere la possibilità che tu non abbia peccato affatto!

*Mortale:* Certo che questa possibilità non è esclusa, ma forse ho peccato ed è
questo pensiero che mi fa paura!

*Dio:* Perché ti fa tanta paura il pensiero di aver peccato?

*Mortale:* Non lo so perché! Intanto, tu hai una certa fama d’infliggere castighi
piuttosto atroci nell’aldilà!

*Dio:* Ah, è questo che ti turba! Perché non l’hai detto subito, invece di menare
il can per l’aia tirando in ballo il libero arbitrio e la responsabilità? Perché
non hai semplicemente chiesto di non punirti per nessuno dei tuoi peccati?

*Mortale:* Sono abbastanza realista, credo, da sapere che tu non acconsentiresti a
questa richiesta!

*Dio:* Ma davvero! Tu sai realisticamente a quali richieste io accontento, eh?
Bene, ecco che cosa farò: ti concedo una dispensa specialissima di peccare
quanto ti piace e ti do la mia divina parola d’onore che non ti punirò neanche
un poco. D’accordo?

*Mortale (spaventatissimo):* No, no, non farlo!

*Dio:* Perché no? Non ti fidi della mia divina parola?

*Mortale:* Certo che mi fido! Ma non capisci, io non voglio peccare! Aborrisco il
peccato con tutto me stesso, indipendentemente dai castighi che può procurarmi.

*Dio:* In tal caso farò qualcosa di meglio. Eliminerò il tuo orrore per il
peccato. Ecco una pillola magica! Inghiottila e non avrai più alcun orrore per
il peccato. Potrai peccare allegramente in lungo e in largo senza provare
rimorsi, senza provare orrore, e ti prometto nuovamente che non sarai mai punito
né da me né da qualunque altro ente. Sarai felice e beato per tutta l’eternità.
Ecco qua la pillola!

*Mortale:* No, no!

*Dio:* Non ti stai comportando in modo irrazionale? Voglio addirittura eliminare
l’orrore che hai del peccato, che è il tuo ultimo ostacolo.

*Mortale:* Non la voglio prendere lo stesso!

*Dio:* Perché no?

*Mortale:* Io credo che la pillola eliminerà sì il mio orrore futuro per il
peccato, ma l’orrore che ne ho adesso è sufficiente a togliermi la volontà di
prenderla.

*Dio:* Ti ordino di prenderla!

*Mortale:* Mi rifiuto di farlo!

*Dio:* Come? Rifiuti di tua volontà, di tuo libero arbitrio?

*Mortale:* Sì!

*Dio:* Allora il tuo libero arbitrio si direbbe ti faccia piuttosto comodo, no?

*Mortale:* Non capisco!

*Dio:* Non sei contento ora di avere il libero arbitrio che ti permette di
rifiutare un’offerta così spaventosa? Che ne diresti se ti obbligassi a prendere
questa pillola, volente o nolente?

*Mortale:* No, no! Ti prego, non farlo!

*Dio:* Naturale che non lo farò; sto soltanto cercando di farti capire. Va bene,
mettiamola così: invece di obbligarti a prendere la pillola, supponiamo che io
esaudisca la tua preghiera iniziale di toglierti il libero arbitrio, ma ti dica
anche che nel momento in cui non sarai più libero prenderai la pillola?

*Mortale:* Una volta scomparsa la mia volontà, come farei a decidere di prendere
la pillola?

*Dio:* Non ho detto che tu decideresti; ho detto soltanto che la prenderesti.
Agiresti, diciamo, in conformità a leggi puramente deterministiche, le quali, di
fatto, te la farebbero prendere.

*Mortale:* Rifiuto lo stesso.

*Dio:* Dunque rifiuti la mia offerta di toglierti il libero arbitrio. Siamo un po’
lontani dalla tua preghiera iniziale, non ti sembra?

*Mortale:* Adesso ho capito dove vuoi arrivare. Il tuo argomento è ingegnoso, ma
non sono sicuro che sia proprio giusto. Ci sono alcuni punti che dovremo
riesaminare.

*Dio:* Ma certo.

*Mortale:* Ci sono due cose che hai detto che mi sembrano contraddittorie. Prima
hai detto che non si può peccare se non lo si fa col libero arbitrio. Ma poi hai
detto che mi avresti dato una pillola che mi avrebbe privato del libero
arbitrio, così da permettermi di peccare a mio piacimento. Ma se non avessi più
il mio libero arbitrio, come potrei, secondo la tua prima asserzione, essere in
grado di peccare?

*Dio:* Stai confondendo due parti distinte della nostra conversazione. Io non ho
mai detto che la pillola ti priverebbe del libero arbitrio, ma solo che
eliminerebbe il tuo orrore per il peccato.

*Mortale:* Non credo proprio di capire.

*Dio:* E va bene. Ricominciamo daccapo. Supponiamo che io accetti di toglierti il
libero arbitrio, ma mettendo bene in chiaro che dopo tu compirai un numero
enorme di azione che tu ora consideri peccaminose. Da un punto di vista tecnico,
in tal caso non peccherai, poiché non compirai quelle azioni di tuo libero
arbitrio. E quelle azioni non comporteranno alcuna responsabilità morale, alcune
colpevolezza morale, o alcun qualsivoglia castigo. Nondimeno saranno tutte
quelle azioni del tipo che tu ora consideri peccati, avranno tutte questa
qualità che ora t’ispira orrore; ma il tuo orrore scomparirà, perciò, allora,
non proverai orrore per quelle azioni.

*Mortale:* No, però ne provo orrore ora e questo orrore di adesso è sufficiente a
farmi rifiutare la tua proposta.

*Dio:* Hmm! Fammi capire bene: tu non vuoi più che io ti tolga il libero arbitrio.

*Mortale (con riluttanza):* No, credo di no.

*Dio:* D’accordo, acconsento di non togliertelo. Ma ancora non mi è del tutto
chiaro perché tu non vuoi più sbarazzarti del tuo libero arbitrio. Dimmelo di
nuovo, per favore.

*Mortale:* Perché, come mi hai detto tu, senza libero arbitrio peccherei ancora
più di adesso.

*Dio:* Ma ti ho già detto che senza libero arbitrio non puoi peccare.

*Mortale:* Ma se ora decido di sbarazzarmi del libero arbitrio, tutte le cattive
azioni che commetterò in seguito saranno peccato, non del futuro, bensì del
momento presente, in cui decido di non avere il libero arbitrio.

*Dio:* Mi sembri proprio intrappolato!

*Mortale:* Certo che sono intrappolato! Mi hai cacciato in un orribile doppio
vincolo! Qualunque cosa faccia è sbagliata: se mi tengo il libero arbitrio
continuo a peccare e se lo abbandono (col tuo aiuto, naturalmente) pecco in
questo momento, nell’atto di abbandonarlo.

*Dio:* Ma anche tu cacci me in un doppio vincolo. io sono disposto a lasciarti o a
toglierti il libero arbitrio, come preferisci, ma nessuna delle due alternative
ti soddisfa. Desidero aiutarti, ma a quanto pare non mi è possibile.

*Mortale:* È vero!

*Dio:* Ma dal momento che non è colpa mia, perché sei lo stesso in collera con me?

*Mortale:* Perché sei stato tu a cacciarmi in questo orribile dilemma.

*Dio:* Ma, a sentir te, non avrei potuto fare niente che andasse bene.

*Mortale:* Niente che vada bene ora, vorrai dire, ma ciò non significa che non
avresti potuto fare qualcosa prima.

*Dio:* Perché? Che cos’avrei potuto fare?

*Mortale:* È chiaro: non avresti mai dovuto darmi il libro arbitrio. Ora che me
l’hai dato, è troppo tardi: qualunque cosa io faccia è sbagliata. Ma tu non
avresti mai dovuto darmelo.

*Dio:* Ah, ecco! E perché sarebbe stato meglio se non te l’avessi mai dato?

*Mortale:* Perché allora non sarei mai stato in grado di peccare.

*Dio:* Ah sì? Be’, sono sempre pronto a imparare qualcosa dai miei errori.

*Mortale:* Come!?

*Dio:* Lo so, suona un po’ autoblasfemo. Racchiude quasi un paradosso logico! Da
un lato, come ti hanno insegnato, è moralmente sbagliato per un essere senziente
sostenere che io sia in grado di sbagliare. Dall’altro, io ho il diritto di fare
qualunque cosa. Ma anch’io sono un essere senziente. Dunque la questione è: ho o
non ho il diritto di sostenere di essere in grado di sbagliare?

*Mortale:* Questa è una battuta di cattivo gusto! Intanto, una delle tue premesse
è falsa. Non mi è stato insegnato che un essere senziente sbaglia se mette in
dubbio la tua onniscienza, ma solo che sbaglia un mortale. Ma poiché tu non sei
mortale, sei ovviamente esente da questo obbligo.

*Dio:* Bene, quindi a livello razionale questo lo capisci. Eppure mi sei sembrato
scosso quando ho detto: “Sono sempre pronto a imparare dai miei errori”.

*Mortale:* Certo che sono rimasto scosso. Scosso non dalla tua autobestemmia (come
l’hai chiamata scherzosamente), non dal fatto ch tu non avevi alcun diritto di
dirlo, bensì dal semplice fatto che tu l’hai detto, perché mi hanno insegnato
che in realtà tu non sbagli mai. Quindi mi ha sbalordito sentirti affermare che
ti è possibile sbagliare.

*Dio:* non ho sostenuto che sia possibile. Dico solo che sono pronto a imparare
qualcosa dai mie eventuali errori. Ma questo non dice nulla sul fatto che io
realmente abbia errato o possa mai errare.

*Mortale:* Oh, per favore, smettiamola con queste sofisticherie. Ammetti o non
ammetti che è stato uno sbaglio darmi il libero arbitrio?

*Dio:* Be’, è proprio questo il punto che secondo me dovremmo approfondire.
Permetti che ti riassuma il dilemma in cui ora ti dibatti. Tu non vuoi il libro
arbitrio perché con esso puoi peccare, e tu non vuoi peccare (cosa che, a dire
il vero, continua a lasciarmi perplesso: in un certo senso tu devi voler
peccare, altrimenti non peccheresti. Ma per il momento lasciamo perdere).
D’altra parte, se tu decidessi di rinunciare al libro arbitrio, saresti
responsabile adesso delle tue azioni future. Ergo, io non avrei mai dovuto darti
il libero arbitrio.

*Mortale:* Appunto!

*Dio:* Capisco perfettamente il tuo disagio. Molti mortali, e perfino alcuni
teologi, si sono lamentati dicendo che sono stato ingiusto perché da una parte
sono stato io, non loro, a decidere che dovessero avere il libero arbitrio, e
dall’altra considero loro responsabili di ciò che fanno. In altre parole,
ritengono di essere obbligati a tener fede a un contratto con me, che però esso
non hanno mai sottoscritto.

*Mortale:* Appunto!

*Dio:* Come ho detto, capisco perfettamente il disagio che la situazione comporta.
E riconosco la giustezza della lagnanza, tuttavia questa lagnanza sorge solo da
un’interpretazione non realistica del termini della questione. Ora te li
illustrerò per bene e vedrai che i risultati ti sorprenderanno. Ma invece di
dirteli subito, continuerò a usare il metodo socratico.  
Per ricapitolare ancora una volta, a te dispiace che io ti abbia dato il libero
arbitrio. Io sostengo che quanto ne vedrai le vere conseguenze non sarai più
dispiaciuto. Per dimostrare la mia tesi, ecco ciò che farò. Creerò un nuovo
universo, un nuovo continuum spazio-temporale. In questo nuovo universo nascerà
un mortale esattamente identico a te: a tutti gli effetti pratici potremmo dire
che rinascerai tu. A questo nuovo mortale, a questo nuovo te, io posso dare o
non dare il libero arbitrio. Che cosa vuoi che faccia?

*Mortale (con gran sollievo):* Oh, ti prego! Risparmiagli di dover avere il
libero arbitrio!

*Dio:* Sta bene, farò come dici. Ma ti rendi conto, non è vero, che questo nuovo
te privo di libero arbitrio commetterà azioni orribili di ogni genere?

*Mortale:* Ma non saranno peccati, poiché egli non avrà il libero arbitrio.

*Dio:* Che tu li chiami peccati o no, resta il fatto che saranno azioni orribili,
nel senso che causeranno grandi sofferenze a molti esseri senzienti.

*Mortale (dopo una pausa):* Buon Dio, mi hai messo di nuovo in trappola! Sempre
lo stesso tranello! Se ora ti do il nulla osta per la creazione di questa nuova
creatura priva del libero arbitrio, che nondimeno commetterà azioni atroci,
allora è sì vero che essa non peccherà, ma sarò di nuovo io il peccatore, per
aver ratificato la cosa.

*Dio:* In tal caso ti farò condizioni più favorevoli. Ecco, ho già deciso se
creare questo nuovo te con o senza il libero arbitrio. Ora scriverò la mia
decisione su questo pezzetto di carta e te la mostrerò solo in seguito. Ma la
mia decisione è ormai presa ed è assolutamente irrevocabile. Non c’è proprio
nulla che tu possa fare per cambiarla; in questa faccenda tu non hai alcuna
responsabilità. Ebbene, ciò che voglio sapere è questo: quale decisione speri
che io abbia preso? Ricordati bene, la responsabilità della decisione ricade
tutta su di me, non su di te. Quindi puoi dirmi in tutta onestà e senz’alcun
timore quale decisione speri che io abbia preso.

*Mortale (dopo una pausa lunghissima):* Spero che tu abbia deciso di dargli il
libero arbitrio.

*Dio:* Davvero interessante! Ho tolto il tuo ultimo ostacolo! Se non gli darò il
libero arbitrio, non ci sarà nessuno a cui si potrà imputare alcun peccato. E
allora perché speri che io gli dia il libero arbitrio?

*Mortale:* Perché, peccato o non peccato, la cosa importante è che, se tu non gli
dai il libro arbitrio, lui (almeno stando a ciò che hai detto) sarà causa di
sofferenza per gli altri, e io non voglio che nessuno soffra.

*Dio (con un infinito sospiro di sollievo):* Finalmente! Finalmente hai capito
qual è i nocciolo della questione!

*Mortale:* E sarebbe?

*Dio:* non è il peccato che conta! Ciò che conta è non far soffrire gli uomini o
gli altri esseri senzienti!

*Mortale:* Parli come un utilitarista!

*Dio:* Ma io sono un utilitarista!

*Mortale:* Come!?

*Dio:* Come o non come, io sono un utilitarista. Non un unitarista, bada bene: un
utilitarista.

*Mortale:* Non posso proprio crederci!

*Dio:* Sì, lo so, la tua educazione religiosa ti ha insegnato altrimenti.
Probabilmente mi hai concepito più come un kantiano che come un utilitarista, ma
è solo perché la tua educazione era sbagliata.

*Mortale:* Sono senza parole!

*Dio:* Davvero? Be’, forse non è poi un gran male: in verità tu hai la tendenza a
parlare troppo. Ma ora bando agli scherzi. Perché mai, secondo te, ti avrei dato
il libero arbitrio?

*Mortale:* Perché? Non ho mai riflettuto molto sul perché; quello che mi
preoccupava era che non avresti dovuto dartelo! Ma perché me l’hai dato? L’unica
risposta ch mi viene in mente è la solita spiegazione religiosa: senza libero
arbitrio non si è in grado di meritare né la salvezza né la dannazione. Quindi
senza libero arbitrio non potremmo acquistarci i diritto alla vita eterna.

*Dio:* Molto interessante! Io la vita eterna ce l’ho; pensi che abbia mai fatto
qualcosa per meritarmela?

*Mortale:* No, naturalmente! Per te è diverso. Tu sei già così buono e perfetto
(almeno così si dice) che non hai bisogno di meritare la vita eterna.

*Dio:* Dici davvero? Ciò mi colloca in una posizione invidiabile, non ti pare?

*Mortale:* Non credo di capirti.

*Dio:* Ecco: io sono felice e beato per l’eternità senza dover mai soffrire o far
sacrifici o lottare contro le tentazioni o altre cose del genere. Senza “meriti”
di alcun genere io godo di una beatissima vita eterna. E invece vi, poveri
mortali, dovete sudare e soffrire e siete lacerati da conflitti tremendi sulla
morale, e tutto questo per che cosa? Non sapete neppure se io esisto veramente o
no, o se esistono davvero un aldilà e un’altra vita o, se esistono, che
posizione occupate voi in tutto questo. Per quanto vi industriate a placarmi
comportandovi “bene“, non avete mai alcuna vera garanzia che ciò che per voi è
“il meglio” sia sufficiente per me, e quindi non siete mai veramente sicuri di
ottenere la salvezza. Pensaci un momento! Io l’equivalente della “salvezza” ce
l’ho già e non ho mai dovuto sottopormi a questo processo infinitamente
opprimente di guadagnarmela. Non m’invidi per questo?

*Mortale:* Ma è blasfemo invidiarti!

*Dio:* Oh, andiamo! Non stai mica parlando col tuo insegnante di religione, stai
parlando con me. Blasfemo o no, il punto importante non è se tu hai il diritto
di essere invidioso di me, ma se lo sei. Sei invidioso?

*Mortale:* Per forza lo sono!

*Dio:* Bene! Per come tu concepisci il mondo ora, hai tutte le ragioni di
invidiarmi. Ma se tu arriverai a una concezione più realistica, non proverai più
alcuna invidia. E così te la sei proprio bevuta, l’idea che ti hanno insegnato,
che la vostra vita sulla terra è come un periodo di esame e che lo scopo per cui
vi è stato dato il libero arbitrio è di mettervi alla prova, per vedere se
meritate la beatitudine eterna. Ma una cosa m lascia perplesso: se tu credi
veramente che io sia così buono e benevolo come si va sbandierando, perché
dovrei imporre agli uomini di meritarsi cose come la felicità e la vita eterna?
Perché non dovrei concedere queste cose a ciascuno, che le meriti o no?

*Mortale:* Ma mi è stato insegnato che il tuo senso della morale, il tuo senso
della giustizia, impone che il bene sia ricompensato con la felicità e il male
sia punito con la sofferenza.

*Dio:* Allora ti hanno insegnato male.

*Mortale:* Eppure la letteratura religiosa è piena di questa idea! Prendi “I
peccatori nelle mani di un Dio adirato” di Jonathan Edwards, dove sei descritto
nell’atto di tenere sospesi i tuoi nemici come scorpioni ripugnanti sopra il
pozzo infuocato dell’inferno, impedendo loro di precipitare nel fato che
meritano soltanto in virtù della tua misericordia.

*Dio:* Per fortuna non mi sono dovuto sorbire le tirate del signor Jonathan
Edwards. Sono state pronunciate poche prediche più ingannevoli di quella. Già il
titolo, “I peccatori nelle mani di un Dio adirato”, la dice lunga. In primo
luogo, io non mi adiro mai; in secondo luogo, io non penso affatto in termini di
“peccato”; in terzo luogo, io non ho nemici.

*Mortale:* Con questo vuoi dire che non ci sono uomini che tu odii o che non ci
sono uomini che odiano te?

*Dio:* Intendevo dire la prima cosa, benché sia vera anche la seconda.

*Mortale:* Ma andiamo, io conosco uomini che hanno dichiarato apertamente di
odiarti. A volte ti ho odiato anch’io.

*Dio:* Vuoi dire che hai odiato l’immagine che hai di me. Non è lo stesso che
odiare me come realmente io sono.

*Mortale:* Intendi forse dire che non è sbagliato odiare un falso concetto di te,
mentre è sbagliato odiarti come tu sei realmente?

*Dio:* No, non voglio affatto dire questo, voglio dire qualcosa di molto più
drastico! Ciò che dico non ha assolutamente nulla a che fare col giusto o con lo
sbagliato. Dico che se uno mi conosce per quello che sono realmente, gli è
psicologicamente impossibile odiarmi.

*Mortale:* Dimmi: dal momento che noi mortali abbiamo idee così errate sulla tua
vera natura, perché non ci illumini? Perché non ci guidi per la retta via?

*Dio:* Che cosa ti fa credere che non lo faccia?

*Mortale:* Voglio dire, perché non ti mostri direttamente ai nostri sensi e non ci
dici chiaro e tondo che sbagliamo?

*Dio:* Sei davvero così ingenuo da credere che io sia il genere di essere che può
apparire ai vostri sensi? Sarebbe più giusto dire che io sono i vostri sensi.

*Mortale (sbalordito):* Tu sei i miei sensi?

*Dio:* Non proprio: sono qualcosa di più. Ma è un’idea più vicina alla realtà che
io sarei percepibile dai tuoi sensi. Io non sono un oggetto; come te, io sono un
soggetto, e un soggetto può percepire ma non può essere percepito. Tu non puoi
vedere me più di quanto tu possa vedere i tuoi pensieri. Puoi vedere una mela,
ma l’evento costituito dal tuo vedere la mela non è visibile. E io sono assai
più simile al vedere la mela che alla mela stessa.

*Mortale:* Se non ti posso vedere, come faccio a sapere che esisti?

*Dio:* Domanda giusta! Coma fai appunto a sapere che esisto?

*Mortale:* Be’, non sto forse parlando con te?

*Dio:* Come fai a sapere che stai parlando con me? Supponi di dire a uno
psichiatra: “Ieri ho parlato con Dio”. Che cosa pensi che ti direbbe?

*Mortale:* Dipende dallo psichiatra. E poiché gli psichiatri sono per lo più atei,
probabilmente mi direbbero che ho parlato con me stesso.

*Dio:* E avrebbero ragione!

*Mortale:* Come? Vuoi dire che non esisti?

*Dio:* La tua capacità di trarre conclusioni false è sbalorditiva. Solo perché
stai parlando con te stesso ne segue che io non esisto?

*Mortale:* Ma se penso di parlare con te mentre in realtà sto parlando con me
stesso, in che senso esisti tu?

*Dio:* La tua domanda è basata su due fallacie più un equivoco. Se tu ora stai
parlando o no con me e se io esisto o no sono due questioni del tutto separate.
Anche se tu ora non stessi parlando con me (ma è evidente che lo stai facendo),
ciò non significherebbe ugualmente che io non esisto.

*Mortale:* Be’, d’accordo, è ovvio! Quindi, invece di dire “Se io sto parlando con
me stesso, allora tu non esisti”, allora avrei dovuto dire: “Se io sto parlando
con me stesso, allora è evidente che non sto parlando con te”.

*Dio:* Un’asserzione molto diversa, non c’è dubbio, ma falsa anch’essa.

*Mortale:* Ma andiamo, se sto solo parlando con me stesso, com’è possibile che
stia parlando con te?

*Dio:* L’uso che fai della parola “solo” è molto fuorviante! Posso elencarti una
serie di possibilità logiche secondo le quali se parli con te stesso ciò non
esclude che tu stia parlando con me.

*Mortale:* Dimmene una sola!

*Dio:* Be’, ovviamente una di queste possibilità è che tu e io coincidiamo.

*Mortale:* Che pensiero blasfemo… almeno, se lo avessi espresso io!

*Dio:* Secondo certe religioni, sì. Secondo altre, è la verità pura, semplice e
immediatamente percepibile.

*Mortale:* Dunque l’unica via per uscire dal mio dilemma è credere che tu e io
coincidiamo?

*Dio:* Nient’affatto! Questa è solo una delle vie d’uscita. Ce ne sono diverse
altre. Per esempio, può darsi che tu sia una parte di me, nel qual caso tu
parleresti con quella parte di me che sei tu. Oppure potrei essere io una parte
di te, nel qual caso parleresti con quella parte di te che sono io. O ancora: tu
e io potremmo essere parzialmente sovrapposti, nel qual caso tu parleresti con
l’intersezione e quindi sia con te sia con me. L’unico caso in cui il tuo
parlare con te stesso sembrerebbe implicare che tu non stia parlando con me è
che tu e io fossimo completamente disgiunti; ma anche in tal caso sarebbe
possibile che tu stessi parlando a entrambi.

*Mortale:* Quindi tu sostieni di esistere.

*Dio:* Nient’affatto. Di nuovo trai conclusioni false! La questione della mia
esistenza non si è neppure presentata. Io ho detto soltanto che dal fatto che tu
stai parlando con te stesso non si può in alcun modo dedurre che io non esista,
per non parlare del fatto secondario che tu non stia parlando con me.

*Mortale:* Va bene, questo te lo concedo! Però ciò che io voglio sapere veramente
è se tu esisti!

*Dio:* Che strana domanda!

*Mortale:* Perché? Gli uomini se lo chiedono da innumerevoli millenni.

*Dio:* Lo so! Non è strana la domanda in sé; ciò che intendo dire è che è molto
strano farla a me!

*Mortale:* Perché?

*Dio:* Perché io sono proprio quello della cui esistenza tu dubiti! Capisco
perfettamente la tua preoccupazione . Hai paura che la tua attuale esperienza
con me sia una pura allucinazione. Ma come ti puoi aspettare di ottenere da un
essere informazioni attendibili sulla sua esistenza, quando sospetti che proprio
quell’essere non esista?

*Mortale:* Quindi non mi vuoi dire se esisti o no?

*Dio:* Non lo faccio per cattiveria! Voglio solo farti notare che nessuna delle
risposte che potrei darti riuscirebbe in alcun modo a soddisfarti. Va bene,
supponi che io dica: “No, non esisto”. Che cosa dimostrerebbe ciò? Assolutamente
nulla! Oppure potrei dire: ”Sì, esisto”. Ti convincerebbe questo? No,
naturalmente!

*Mortale:* Ebbene, se tu non puoi dirmi se esisti o no, chi altri può dirmelo?

*Dio:* Questa è una cosa che nessuno ti può dire. È una cosa che puoi scoprire
solo tu, da solo.

*Mortale:* Ma che cosa devo fare per scoprirla da solo?

*Dio:* Anche questo non le to può dire nessuno. È un’altra cosa che dovrai
scoprire da solo.

*Mortale:* In che modo puoi dunque aiutarmi?

*Dio:* Di questo non ti preoccupare, lascia fare a me! Intanto però abbiamo
cambiato argomento, e vorrei tornare a parlare dello scopo che secondo te io
avrei avuto quando ti ho dato il libero arbitrio. La tua prima idea, cioè che ti
abbia dato il libero arbitrio per vedere se meriti o no la salvezza, può piacere
a molto moralisti, ma è un’idea che a me fa proprio orrore. non riesci a pensare
a una ragione più piacevole, una ragione più umana, per cui io ti avrei dato il
libero arbitrio?

*Mortale:* Ebbene, una volta feci questa domanda a un rabbino ortodosso, e lui mi
disse che noi siamo stati fatti in modo tale che non ci è materialmente
possibile gioire della salvezza se non sentiamo di essercela guadagnata. E per
guadagnarcela naturalmente abbiamo bisogno del libero arbitrio.

*Dio:* Questa spiegazione è in effetti molto più piacevole di quella che mi hai
dato prima, ma è ancora ben lungi dall’essere giusta. Secondo l’ebraismo
ortodosso, io ho creato gli angelo, i quali non hanno il libero arbitrio. Essi
mi vedono direttamente e sono attratti dalla bontà in modo così completo che non
hanno mai la benché minima tentazione verso il male: non hanno davvero alcuna
scelta. Eppure godono di una felicità eterna, anche se non se la sono mai
guadagnata. Quindi, se la spiegazione del tuo rabbino fosse giusta, perché non
avrei potuto creare solo angeli invece di creare anche i mortali?

*Mortale:* E chi lo sa! Perché?

*Dio:* Perché questa spiegazione è sbagliata. In primo luogo non ho mai creato
angeli bell’e pronti: tutti gli esseri senzienti si avvicinano da ultimo a uno
stato che si potrebbe chiamare “angelico”. Ma proprio come la specie degli
esseri umani si trova a un certo stadio di evoluzione biologica, così gli angeli
non sono che il risultato finale di un processo di Evoluzione Cosmica. L’unica
differenza tra il cosiddetto santo e il cosiddetto peccatore è che il primo è
immensamente più vecchio del secondo. Purtroppo ci vogliono innumerevoli cicli
vitali per apprendere quello ch è forse il fatto più importante dell’universo:
che il male fa soffrire. Tutti gli argomenti dei moralisti, tutte le ragioni
addotte per sostenere che gli uomini non devono compiere azioni malvagie,
impallidiscono e perdono ogni significato alla luce dell’unica verità di fondo,
che il male è sofferenza.  
No, amico mio, io non sono un moralista. Sono un perfetto utilitarista. L’avermi
concepito come un moralista è una delle grandi tragedie del genere umano. Il mio
ruolo nel disegno delle cose (se è lecito usare quest’espressione fuorviante)
non è né di punire né di premiare, bensì di agevolare il processo per cui ogni
essere senziente consegue la perfezione ultima.

*Mortale:* Perché hai detto che quell’espressione è fuorviante?

*Dio:* Ciò che ho detto era fuorviante per due motivi. In primo luogo è impreciso
parlare del mio ruolo nel disegno delle cose: io sono il disegno delle cose. In
secondo luogo, è altrettanto fuorviante dire che io agevolo il processo per cui
gli esseri senzienti raggiungono l’illuminazione: io sono quel processo. Gli
antichi taoisti andarono molto vicino al segno quando dissero di me (mi
chiamavano “Tao”) che io non faccio le cose, eppure è attraverso di me che viene
fatta ogni cosa. In termini più moderni, io non sono la causa del Processo
Cosmico, io sono il Processo Cosmico stesso. Direi che la definizione più
precisa e feconda che l’uomo possa dare di me – almeno allo stadio attuale della
sua evoluzione – sia che io sono il processo stesso di illuminazione. Coloro che
desiderano pensare al diavolo (anche se io vorrei che non lo facessero!)
potrebbero definirlo, analogamente, come il tempo disgraziatamente lungo che il
processo richiede. In questo senso, il diavolo è necessario; il fatto è che il
processo richiede un tempo lunghissimo e io non ci posso fare proprio nulla. Ma
ti assicuro che una volta che il processo sia compreso in maniera più esatta,
quel tempo così penosamente lungo non sarà più considerato come una limitazione
essenziale o come un male. Si vedrà che esso costituisce l’essenza stessa di
questo processo. So che questo non è del tutto consolante per te, che ti trovi
ora nel mare finito della sofferenza; ma la cosa stupefacente è che, una volta
che tu abbia afferrato questo atteggiamento di fondo, la tua sofferenza finita
comincerà a diminuire, finché da ultimo svanirà.

*Mortale:* Questo me l’hanno detto, e sono incline a crederlo. Ma supponi che io
personalmente arrivi a vedere le cose attraverso i tuoi occhi eterni. Io allora
sarò più felice, ma non ho forse degli obblighi verso gli altri?

*Dio (ridendo):* Mi fai venire in mente i buddhisti Mahāyāna! Ciascuno dice: “Io
non entrerò nel Nirvāṇa se prima non ci vedrò entrare tutti gli altri esseri
senzienti”. Così ciascuno aspetta che siano gli altri a entrare per primi. Non
c’è da meravigliarsi che ci mettano tanto tempo! Il buddhista Hīnayāna commette
lo sbaglio opposto. Egli crede che nessuno possa minimamente aiutare gli altri a
ottenere la salvezza: ciascuno deve fare tutto da solo. E quindi ciascuno si
sforza di conseguire solo la propria salvezza. Ma proprio questo atteggiamento
distaccato rende impossibile la salvezza. La verità è che la salvezza è un
processo in parte individuale e in parte sociale. Ma è un grave errore credere –
come fanno molti buddhisti Mahāyāna – che conseguire l’illuminazione significhi
per così dire, essere messo fuori servizio e non poter più aiutare gli altri. Il
modo migliore per aiutare gli altri è di vedere prima la luce noi stessi.

*Mortale:* Nella descrizione ch fai di te stesso c’è qualcosa che mi turba un
poco. Tu ti descrivi essenzialmente come un processo. Questo ti pone in una luce
molto impersonale, mentre tantissime persone hanno bisogno di un Dio personale.

*Dio:* E così, dal momento che hanno bisogno di un Dio personale, ne consegue che
io sono un Dio personale?

*Mortale:* No, naturalmente. Ma per essere accettabile dai mortali, una religione
deve soddisfare i loro bisogni.

*Dio:* Me ne rendo conto. Ma la cosiddetta “personalità” di un essere è in realtà
più nell’occhio di chi guarda che nell’essere in questione. Tutto questo acceso
dibattere se io sia un essere personale o impersonale è piuttosto sciocco,
perché nessuna delle due posizioni è giusta o sbagliata. Da un certo punto di
vista io sono personale, da un altro non lo sono. Per un essere umano è la
stessa cosa: una creatura di un altro pianeta può vederlo in modo puramente
impersonale, come semplice insieme di particelle atomiche che si comportano
secondo leggi fisiche rigorose. Questa creatura potrebbe provare per la
personalità dell’essere umano la stessa considerazione che l’uomo comune ha per
una formica. Eppure una formica ha tanta personalità individuale quanta un
essere umano per esseri che, come me, conoscono veramente la formica. Vedere una
cosa come impersonale non è né più giusto né più sbagliato che vederla come
personale, ma in genere quanto più si conosce qualcosa tanto più personale essa
diventa. Per illustrare quanto dico, tu mi pensi come un essere personale o
impersonale?

*Mortale:* Be’, ti sto parlando, no?

*Dio:* Precisamente! Da questo punto di vista, il tuo atteggiamento nei miei
confronti potrebbe essere definito personale. E tuttavia, da un altro punto di
vista, non meno valido, mi si può anche vedere come impersonale.

*Mortale:* Ma se tu sei davvero un processo, cioè una cosa astratta, non riesco a
capire che senso possa avere che io parli con un semplice “processo”.

*Dio:* Mi piace il modo in cui dici “semplice”. Allo stesso modo potresti dire che
vivi in un “semplice universo”. E poi, perché ogni cosa che si fa dovrebbe avere
senso? Ha senso parlare con un albero?

*Dio:* Eppure molti bambini e molti primitivi lo fanno.

*Mortale:* Ma io non sono né un bambino né un primitivo.

*Dio:* Eh già, purtroppo.

*Mortale:* Perché purtroppo?

*Dio:* Perché molti bambini e molti primitivi hanno un’intuizione primordiale che
quelli come te hanno perduto. Francamente penso che ti farebbe un gran bene
parlare con un albero ogni tanto, anche più che parlare con me! Ma esso che
abbiamo di nuovo cambiato argomento! Per l’ultima volta, vorrei che cercassimo
di arrivare a capire perché ti ho dato il libero arbitrio.

*Mortale:* Io ho continuato a pensarci.

*Dio:* Vuoi dire che non hai seguito con attenzione la nostra conversazione?

*Mortale:* Sì che l’ho seguita. Ma nel frattempo, a un altro livello, ho
continuato a pensarci.

*Dio:* E sei arrivato a qualche conclusione?

*Mortale:* Dunque, tu dici che il motivo non è quello di mettere alla prova il
nostro merito. E hai confutato il motivo che per godere delle cose noi abbiamo
bisogno di sentire che dobbiamo meritarle. E sostieni di essere un utilitarista.
E la cosa più significativa di tutte è che mi sei sembrato contentissimo quando
mi sono reso conto d’un tratto che non è il peccare un sé che è male, ma solo la
sofferenza che esso provoca.

*Dio:* Ma certo! Che altro ci potrebbe essere di male nel peccare?

*Mortale:* D’accordo, tu lo sai e adesso lo so anch’io. Ma purtroppo io ho passato
tutta la vita sotto l’influenza di quei moralisti che ritengono che il peccare
sia male in sé. Comunque sia, mettendo insieme tutti questi pezzi, mi viene da
pensare che l’unica ragione per cui ci hai dato il libero arbitrio è perché
credi che col libero arbitrio che uomini probabilmente causeranno meno
sofferenza agli altri – e a se stessi – che senza libero arbitrio.

*Dio:* Bravo! Questa è di gran lunga la ragione migliore che hai dato finora! Ti
posso assicurare che se avessi deciso di darvi il libero arbitrio, l’avrei fatto
proprio per questa ragione.

*Mortale:* Ma come! Vuoi dire che non hai deciso di darci il libero arbitrio?

*Dio:* Mio caro, non potevo decidere di darvi il libero arbitrio più di quanto
potessi decidere di fare equiangolo un triangolo equilatero. Naturalmente potevo
decidere di fare o di non fare un triangolo equilatero, ma dopo aver deciso di
farlo non avevo altra scelta che farlo equiangolo.

*Mortale:* Pensavo che tu potessi fare tutto!

*Dio:* Solo le cose che sono logicamente possibili. Come disse san Tommaso: “È
peccato considerare il fatto che Dio non può fare l’impossibile come una
limitazione del Suo potere”. Sono d’accordo, solo che invece di dire peccato
come fa lui, io direi errore.

*Mortale:* Comunque sono ancora sconcertato da quanto mi hai fatto capire, cioè
che non hai deciso di darmi il libero arbitrio.

*Dio:* Ebbene, è giunto il momento di farti sapere che tutta la nostra
discussione, fin dall’inizio, si è basata su una fallacia mostruosa! Abbiamo
parlato a un livello puramente morale; tu hai cominciato col lamentarti che io
ti abbia dato il libero arbitrio e hai sollevato tutta la questione se avessi
dovuto farlo. Non ti è mai venuto in mente che io non avevo assolutamente alcuna
scelta.

*Mortale:* Brancolo ancora nel buio!

*Dio:* Certo! Perché sei solo capace di considerare il problema con gli occhi del
moralista. Non ti è mai passato per la testa di considerare i più fondamentali
aspetti metafisici della questione.

*Mortale:* Non vedo ancora dove tu voglia andare a parare.

*Dio:* Invece di chiedermi di toglierti il libero arbitrio, non avresti forse
dovuto chiedere prima di tutto se ce l’hai, il libero arbitrio?

*Mortale:* Ma io lo davo semplicemente per scontato.

*Dio:* E perché?

*Mortale:* Non lo so. Ce l’ho, il libero arbitrio?

*Dio:* Si.

*Mortale:* Allora perché mi hai detto che non avrei dovuto darlo per scontato?

*Dio:* Perché non avresti dovuto. Solo perché una cosa è vera, non ne segue che la
si debba dare per scontata.

*Mortale:* Comunque mi tranquillizza sapere che la mia intuizione naturale sul
fatto che posseggo il libero arbitrio è giusta. Mi è successo a volte di temere
che abbiano ragione i deterministi.

*Dio:* I deterministi hanno ragione.

*Mortale:* Un momento: il libero arbitrio ce l’ho o non ce l’ho?

*Dio:* Ti ho già detto di sì. Ma questo non significa che il determinismo sia
sbagliato.

*Mortale:* Insomma, le mie azioni sono determinate dalle leggi della natura o no?

*Dio:* La parola determinate qui è una sottile ma possente causa di
fraintendimenti, e ha contribuito parecchio a confondere le acque nelle
controversie sul libero arbitrio e il determinismo. Le tue azioni sono
certamente in accordo con le leggi della natura, ma dire che esse sono
determinate dalle leggi della natura crea un’immagine psicologia totalmente
fuorviante; fa pensare cioè che la tua volontà possa essere in qualche modo in
conflitto con le leggi della natura e che questa sia in qualche modo più potente
di te e possa “determinare” le tue azioni, che tu lo voglia o no. Ma è
semplicemente impossibile che la tua volontà entri mai in conflitto con la legge
naturale. In realtà tu e la legge naturale siete la stessa identica cosa.

*Mortale:* Come sarebbe a dire che io non posso entrare in conflitto con la
natura? Supponiamo che io mi intestardissi e determinassi di non obbedire alle
leggi della natura. Che cosa potrebbe fermarmi? Se mi intestardissi abbastanza,
nemmeno tu potresti fermarmi!

*Dio:* Hai perfettamente ragione! Io certo non potrei fermarti. Nulla potrebbe
fermarti. Ma non ci sarebbe alcun bisogno di fermarti, poiché non potresti
neppure cominciare! Goethe ha espresso molto bene tutto ciò: “Nel tentare di
opporci alla Natura noi, nell’atto stesso di farlo, operiamo secondo le leggi
della natura!”. Non capisci che le cosiddette “leggi della natura” non sono
altro che una descrizione di come appunto tu e gli altri esseri agite? Sono
semplicemente una descrizione di come tu agisci, non una prescrizione di come
dovresti agire, non un potere o una forza che costringe o determina le tue
azioni. Per essere valida, una legge della natura deve tener conto di come tu di
fatto agisci, o, se preferisci, di come tu scegli di agire.

*Mortale:* In realtà, dunque, tu sostieni che io sono incapace di determinare di
agire contro la legge naturale?

*Dio:* È interessante che tu abbia usato per due volte l’espressione “determinare
di agire” invece che “scegliere di agire”. Questa identificazione è molto
frequente. Spesso si usa l’asserzione “Sono nella determinazione di far questo”
come sinonimo di “Ho scelto di far questo”. Ma proprio questa identificazione
psicologica dovrebbe rivelare che il determinismo e la scelta sono molto più
vicini tra loro di quanto potrebbe sembrare. Naturalmente tu potresti benissimo
dire che la dottrina del libero arbitrio dice che sei tu a compiere questo atto
di determinazione, mentre la dottrina del determinismo afferma, a quanto pare,
che le tue azioni sono determinate da qualcosa che con tutta evidenza sta fuori
di te. Ma la confusione è in gran parte causata dalla dicotomia che tu compi
dividendo la realtà in “te” e “non te”. Suvvia, dov’è che in realtà finisci tu e
comincia il resto dell’universo? Oppure, dov’è che finisce il resto
dell’universo e cominci tu? Una volta che tu riesca a vedere il cosiddetto “te”
e la cosiddetta “natura” come una totalità continua, non sarai più tormentato
dal dubbio se sei tu a controllare la natura o la natura a controllare te. E
così sparirà tutto questo pasticcio del conflitto tra libero arbitrio e
determinismo. Se mi è lecito usare un’analogia un po’ grossolana, immagina due
corpi che si muovano l’uno verso l’altro a causa dell’attrazione gravitazionale.
Se fosse senziente, ciascun corpo potrebbe domandarsi se è lui stesso oppure
l’altro a esercitare la “forza”. In un certo senso la esercitano entrambi, in un
certo senso non la esercita né l’uno né l’altro. Meglio di tutto è dire che ciò
che conta è la configurazione dei due.

*Mortale:* Poco fa hai detto che tutta la nostra discussione era basata su una
fallacia mostruosa, ma ancora non mi hai detto quale sia questa fallacia.

*Dio:* Ma come, è l’idea che io avrei potuto crearti senza libero arbitrio! Ti
comportavi come se questa fosse una possibilità autentica e ti domandavi perché
io non l’avessi scelta! Non ti è mai venuto in mente che un essere senziente
senza libero arbitrio non è più concepibile di quanto lo sia un oggetto fisico
che non eserciti attrazione gravitazionale. (E per inciso, l’analogia tra un
oggetto fisico che esercita l’attrazione gravitazionale e un essere senziente
che esercita il libero arbitrio è più stretta di quanto tu pensi!). In tutta
onestà, riesci a immaginarti un essere cosciente privo di libero arbitrio? Come
sarebbe mai fatto? Una cosa che secondo me ti ha portato tanto fuori strada
nella tua vita è che ti hanno detto che io ho fatto all’uomo il dono del libero
arbitrio. Come se prima avessi creato l’uomo e poi, come ripensandoci, lo avessi
dotato di questa ulteriore proprietà del libero arbitrio. Forse tu penserai che
io abbia una sorta di “pennello” con cui ritocco certe creature col libero
arbitrio e altre no. No, il libero arbitrio non è un “extra”: esso è parte
integrante dell’essenza stessa della coscienza. Un essere cosciente senza libero
arbitrio è semplicemente un assurdo metafisico.

*Mortale:* Ma allora perché mi hai dato corda per tutto questo tempo, discutendo
quello che io pensavo fosse un problema morale mentre, come tu dici, la mia
confusione di fondo era di natura metafisica?

*Dio:* Perché pensavo che sarebbe stata buona terapia espellere dal
tuo sistema un po' di questo veleno morale. Gran parte della tua confusione
metafisica era dovuta a nozioni morali sbagliate, e quindi bisognava per prima
cosa occuparsi di quelle.

E ora dobbiamo lasciarci, almeno fino a quando non avrai di nuovo bisogno di
me. Penso che la nostra attuale unione ti sarà di utile sostegno per un bel po’.
Ma ricordati quello che ti ho detto a proposito degli alberi. Naturalmente non è
necessario che tu parli davvero con loro, se ciò ti mette in imbarazzo; ma ci
sono tante cose che puoi imparare da loro, e anche dalle pietre, dai ruscelli e
dalle altre manifestazioni della natura. Nulla vale quanto un orientamento
naturalistico per dissipare tutti questi morbosi pensieri di “peccato”, di
“libero arbitrio” e di “responsabilità morale”. A un certo stadio della storia
queste nozioni furono effettivamente utili: mi riferisco ai giorni in cui i
tiranni avevano un potere illimitato e solo il timore dell'inferno era in grado
di frenarli. Ma da allora l'umanità è cresciuta, e questo raccapricciante modo
di pensare non è più necessario.

Potrebbe esserti d'aiuto ricordare quanto dissi una volta attraverso gli
scritti del grande poeta Zen Seng-Ts'an:

*Se vuoi raggiungere la nuda verità,  
non preoccuparti di giusto e sbagliato.  
Il conflitto tra giusto e sbagliato  
è la malattia della mente.*

Vedo dalla tua espressione che queste parole ti consolano e ti atterriscono
allo stesso tempo! Di che cosa hai paura? Che se abolisci nella tua mente la
distinzione tra giusto e sbagliato sarà più probabile che tu commetta azioni
sbagliate? Perché sei così sicuro che l'autocoscienza relativa al giusto e allo
sbagliato non porta a compiere più azioni sbagliate che azioni giuste? Credi
veramente che le persone cosiddette amorali, quando si tratta di azioni e non di
teoria, si comportino in modo meno etico che non i moralisti? No naturalmente!
Anzi, moltissimi moralisti riconoscono la superiorità etica del comportamento
della maggior parte di coloro che teoricamente assumono una posizione amorale.
Sembrano davvero sorpresi che questa gente si comporti così bene senza princìpi
etici! Mai che pensino che è proprio in virtù della mancanza di princìpi morali
che la loro buona condotta si manifesta così liberamente!

Forse che le parole “Il conflitto tra giusto e sbagliato è la malattia della
mente umana” esprimono un'idea tanto diversa dalla storia del Paradiso terrestre
e della caduta dell'uomo perché Adamo mangiò il frutto della conoscenza? Questa
conoscenza, bada bene, era conoscenza di princìpi etici, non di sentimenti
etici, poiché questi ultimi Adamo già li aveva. C'è molta verità in quella
storia, anche se io non ho mai ordinato ad Adamo di non mangiare la mela: mi
limitai a consigliargli di non farlo. Gli dissi che non glie ne sarebbe seguito
alcun bene. Se quello sciocco mi avesse dato retta, quanti guai si sarebbero
potuti evitare! E invece no, lui credeva di sapere tutto! Ma vorrei che i
teologi capissero una buona volta che io non sto punendo Adamo e tutta la sua
progenie per quell'azione: è il frutto di quell'albero che è di per sé velenoso
e i suoi effetti, ahimè, si protraggono per innumerevoli generazioni.  
E ora devo proprio congedarmi. Spero davvero che la nostra discussione dissipi
un po' della tua morbosità etica, e la sostituisca con un orientamento più
naturalistico. Ricorda anche le meravigliose parole che pronunciai una volta per
bocca di Lao-Tsu, quando rimproverai Confucio per il suo moraleggiare:  

*Tutto questo parlare di bontà e di dovere, queste continue punture di
spillo, snervano e irritano l'ascoltatore. Faresti meglio a studiare come
avviene che il Cielo e la Terra mantengano il loro corso eterno, e il sole e la
luna il loro splendore, le stelle le loro schiere ordinate, le bestie e gli
uccelli i branchi e gli stormi, e gli alberi e gli arbusti la loro posizione
eretta. Anche tu dovresti imparare queste cose per guidare i tuoi passi col
Potere Interiore, per seguire il corso che la Via della Natura stabilisce; e ben
presto non avrai più bisogno di affaticarti e predicare dappertutto la bontà e
il dovere… Il cigno non ha bisogno di fare il bagno ogni giorno per mantenersi
bianco.*

*Mortale:* Vedo che hai proprio un debole per la filosofia
orientale!
*Dio:* Oh, nient'affatto! Alcuni dei miei pensieri più belli sono fioriti nella
tua patria, l'america. Per esempio, non ho mai espresso la mia nozione di
“dovere” con maggiore eloquenza che attraverso i pensieri di Walt Whitman:

*Nulla io do come doveri,  
Ciò che gli altri danno come doveri,  
[io lo do come impulso vitale.*

Riflessioni
-----------

Con questo dialogo arguto e brillante facciamo la conoscenza di Raymond
Smullyan, logico e prestigiatore pittoresco, che è anche una specie di taoista,
in un modo suo personale. Di Smullyan seguiranno altri due brani, ugualmente
profondi e godibili. Il dialogo appena letto è tratto da *The Tao is
Silent*, una raccolta di scritti che illustrano ciò che accade quando un
logico occidentale incontra il pensiero orientale. Il risultato è insieme
perscrutabile e imperscrutabile (com'era da prevedersi).

Esistono senza dubbio molte persone religiose che considererebbero questo
dialogo blasfemo al massimo grado, così come alcune persone religiose pensano
che sia blasfemo aggirarsi in una chiesa con le mani in tasca. Noi pensiamo
invece che questo dialogo sia *pio*, che sia una profonda dichiarazione
religiosa su Dio, sul libero arbitrio e sulle leggi della natura, blasfema solo
a una lettura superficialissima. Nel corso del dialogo, Smullyan (per bocca di
Dio) lancia parecchie frecciate contro il pensare confuso o superficiale, contro
le categorie preconcette, le risposte semplicistiche, le teorie pompose e le
rigidezze moralistiche. In realtà, stando a ciò che Dio sostiene nel dialogo,
dovremmo attribuirne il messaggio non a Smullyan, bensì a Dio. È Dio, che parla
per bocca di Smullyan, il quale a sua volta parla per bocca di Dio, che ci offre
il suo messaggio.

Proprio come Dio (o il Tao, o l'universo, se si preferisce) ha molte parti,
ciascuna col suo libero arbitrio – io e voi ne siamo esempi –, così ciascuno di
noi ha a sua volta parti interne dotate di un *loro* libero arbitrio (benché
queste parti siano meno libere di noi). Ciò è soprattutto chiaro nel conflitto
interiore del Mortale, incapace di decidere se “egli” vuole o non vuole peccare.
Vi sono “persone interne” – homunculi o sottosistemi – che lottano per il
controllo.

Il conflitto interiore è uno degli aspetti più familiari e allo stesso tempo
meno compresi della natura umana. Un famoso slogan per una marca di patatine
fritte suonava così: “Scommettiamo che non ce la fate a mangiarne una sola!”, un
modo breve ed efficace per ricordarci le nostre divisioni interne. Ci mettiamo a
risolvere un rompicapo avvincente (per esempio il famoso “cubo magico”) ed
eccoci prigionieri: non riusciamo più a metterlo giù. Cominciamo a suonare un
brano musicale o a leggere un bel libro e non riusciamo a smettere, anche se
sappiamo che ci aspettano molte altre cose urgenti da sbrigare.

Chi è che comanda qui? Esiste un qualche essere complessivo in grado di imporre
ciò che accadrà? O c'è solo anarchia, scariche disordinate di neuroni, e sia
quel che sia? La verità deve stare in qualche punto intermedio. Certamente
l'attività del cervello consiste appunto nelle scariche dei neuroni, così come
l'attività di una nazione consiste nella somma delle azioni compiute dai suoi
abitanti. Ma la struttura del governo – che è a sua volta un insieme di attività
di individui – impone un potente tipo di controllo dall'alto verso il basso
sull'organizzazione del complesso. Quando il governo diventa troppo autoritario
e quando abbastanza persone sono veramente insoddisfatte, allora c'è la
possibilità che la struttura globale sia attaccata e crolli: una rivoluzione
interna. Ma per lo più le opposte forze interne giungono a compromessi di vario
tipo, talvolta trovando il giusto mezzo fra due alternative, talvolta
avvicendandosi al potere e così via. I modi in cui si può giungere a questi
compromessi a loro volta caratterizzano fortemente il tipo di governo. Lo stesso
vale per le persone: lo stile con cui vengono risolti i conflitti interni è uno
dei tratti più caratteristici della personalità.

È un mito diffuso che ogni persona dia un'unità, una sorta di organizzazione
unitaria con volontà propria. Una persona è invece un amalgama di molte
sottopersone, ciascuna con *una volontà propria*. Le “sottopersone” sono assai
meno complesse della persona complessiva e di conseguenza hanno problemi di
disciplina interna molto meno gravi. Se sono a loro volta divise, è probabile
che le *loro* parti componenti siano così semplici da possedere loro sì un'unica
volontà; e se no, si può continuare a scendere la scala. Questa organizzazione
gerarchica della personalità è cosa che non lusinga molto la nostra dignità, ma
vi sono molte prove a suo favore.

Nel dialogo, Smullyan se ne esce con una splendida definizione del Diavolo: il
tempo disgraziatamente lungo che ci vuole perché un essere senziente nella sua
totalità giunga all'illuminazione. Questa idea del tempo necessario perché si
manifesti uno stato complesso è stata studiata sotto il profilo matematico da
Charles Bennett e Gregory Chaitin. Secondo la loro affascinante teoria, è forse
possibile dimostrare, con argomenti simili a quelli che stanno a fondamento del
Teorema di Incompletezza di Gödel, che non esistono scorciatoie nello sviluppo
di intelligenze sempre più elevate (o, se si preferisce, di stati sempre più
“illuminati”); in breve, che si deve pagare al “Diavolo” il suo tributo.

Verso la fine del dialogo, Smullyan tocca argomenti che abbiamo affrontato nel
corso di tutto questo libro: il tentativo di conciliare il determinismo e la
“causalità verso l'alto” delle leggi della natura con il libero arbitrio e la
“causalità verso il basso” che tutti noi sentiamo di esercitare. L sua acuta
osservazione che spesso diciamo “Sono nella determinazione di far questo” quando
vogliamo dire “Ho scelto di far questo” lo porta alla spiegazione del libero
arbitrio che prende le mosse della dichiarazione di Dio che “il determinismo e
la scelta sono molto più vicini tra loro di quanto potrebbe sembrare”. La
riconciliazione che Smullyan opera elegantemente tra queste opposte concezioni
si basa sulla nostra accettazione di cambiare punto di vista, di smettere di
pensare in modo “dualistico” (cioè di suddividere il mondo in parti come “me” e
“non me”) e di vedere l'universo come una totalità priva di confini, in cui le
cose fluiscono l'una nell'altra e si sovrappongono, senza margini o categorie
chiaramente definiti.

È un punto di vista che a tutta prima suona strano in bocca a un logico, ma in
fin dei conti chi ha mai detto che i logici siano sempre rigidi e rigorosi?
Perché non dovrebbero essere proprio i logici, più di chiunque altro, a capire i
luoghi dove la logica nitida e affilata si troverà necessariamente in difficoltà
di fronte a questo universo caotico e disordinato? Una delle tesi favorite di
Marvin Minsky è che “la logica non vale per il mondo reale”. In un certo senso
questo è vero. Si tratta di una delle difficoltà che fronteggiano i ricercatori
d'intelligenza artificiale, i quali cominciano a rendersi conto che nessuna
intelligenza può essere basata solo sul ragionamento; o meglio, che il
ragionamento isolato è impossibile, poiché esso dipende dal precedente
allestimento di un sistema di concetti, percezioni, classi, categorie – li si
chiami come si vuole – in base ai quali viene capita ogni situazione. È qui che
entrano in gioco le inclinazioni e le scelte. Non solo la facoltà raziocinante
deve essere disposta ad accettare le prime caratterizzazioni di una situazione
che la facoltà percettiva le porge; se poi il raziocinio ha dei dubbi su
quell'impostazione, la facoltà percettiva deve essere a sua volta disposta ad
accettare tali dubbi e a tornare sui propri passi per reinterpretare la
situazione, creando in tal modo una circolazione continua tra i diversi livelli.
Questa interazione tra il sottosé percipiente e il sottosé raziocinante dà luogo
a un sé totale: il Mortale.

**D.R.H.**

[1]: https://it.wikipedia.org/wiki/Raymond_Smullyan
