title: Filosofie hacker. Il diritto d'autore in internet
date: 2014-02-06 21:03:34 +0100
tags: filosofia, software libero, gnu-linux, hacking, cyberpunk
summary: Tesi di laurea di Maria Elena Malva sulle filosofie hacker.

Questo documento è scaricabile anche nei formati [OpenDocument
ODT]({filename}/misc/Filosofie_hacker-Il_diritto_d_autore_in_internet.odt),
[Portable Document Format
PDF]({filename}/pdfs/Filosofie_hacker-Il_diritto_d_autore_in_internet.pdf) e
[Testo semplice (formattato
Markdown)]({filename}/misc/filosofie-hacker-il-diritto-dautore-in-internet.txt.gz).

**Copyright (c)  2013   Maria Elena Malva**

*è garantito il permesso di copiare, distribuire e/o modificare questo documento
seguendo i termini della Licenza per Documentazione Libera GNU, Versione 1.1 o
ogni versione successiva pubblicata dalla Free Software Foundation; con le
Sezioni Non Modificabili (senza Sezioni non Modificabili), con i Testi Copertina
(nessun Testo Copertina), e con i Testi di Retro Copertina (nessun Testo di
Retro Copertina). Una copia della licenza è acclusa nella sezione intitolata
"Licenza per Documentazione Libera GNU".*

*Indice*  
*Introduzione*  

1. Max Stirner, L’Unico e la sua proprietà -- 1.1 Rivolta e Rivoluzione -- 1.2 Società e Associazione
2. Le Controculture -- 2.1. Gli Hacker -- 2.2. Il free software
3. Etica hacker e conoscenza ai tempi del web -- 3.1. Creative Commons -- 3.2. Wikipedia
4. Attivismo in rete -- 4.1. Anonymous  

*Note*  
*Bibliografia*  
*Sitografia*  
*Ringraziamenti*  
*Licenza per Documentazione Libera GNU*

   

Introduzione
============

Questo lavoro si presenta come una riflessione su alcuni concetti del pensiero
di Max Stirner, delineati nel saggio *l’Unico e la sua Proprietà*1 pubblicato
nel 1844, che vengono successivamente messi a confronto con il fenomeno delle
controculture, in particolare quella hacker. L’attenzione si concentra sul
concetto di libertà, in una prospettiva individualista e radicale, intesa come
la possibilità e capacità individuale che ognuno ha di rendersi libero. Essa
viene in seguito confrontata con l’idea di libertà di stampo più sociale
promossa dalla Free Software Foundation dell’hacker Richard Stallman.
<!--more-->

Stirner introduce una distinzione tra emancipazione e auto-liberazione: nel
primo caso si tratta di una libertà che è concessa da altri e che non rende mai
un uomo davvero libero; l’auto-liberazione è invece il modo che ha l’uomo per
liberarsi da solo, diventare padrone di se stesso esclusivamente attraverso la
propria forza, una forza che non ha nulla a che vedere con la violenza, ma che
s’identifica con la capacità di appropriarsi del mondo.

Nel titolo dell’opera, *L’unico e la sua proprietà*, sono nominati i due
fondamenti della filosofia stirneriana: da una parte il nuovo soggetto,
*l’unico*, dall’altra una nuova concezione dell’oggetto che viene inteso come
*proprietà*, entrambi pienamente teorizzati insieme al concetto di “rivolta” e
alla “unione degli egoisti”. Ultimo concetto fondamentale è quello di “egoismo”,
definito da Stirner come il principio stesso della vita; esso “è inglobante
perché è l’essenza di ogni cosa, nello stesso tempo è escludente perché unifica
solo formalmente: il carattere che accomuna ogni esistente e la sua assoluta
originalità, l’irriducibile diversità.”2

Grazie alla distinzione tra rivolta e rivoluzione si può tracciare e comprendere
il legame con la filosofia hacker: il nucleo di fondo della filosofia di Stirner
consiste nel focus sul rapporto tra soggetto e oggetto, in cui l’individuo cerca
costantemente di superare la minaccia di sottomissione da parte dell’oggetto. La
rivolta segna, infatti, il passaggio da una dimensione religiosa del rapporto
soggetto-oggetto, che risulta alienante, ad una dimensione egoistica dello
stesso, per cui l’oggetto non è più sacro ma diventa proprietà dell’io.

Le motivazioni della rivolta, per Stirner, non sono di ordine sociale, ma
personale: è l’individuo che non sopporta più di essere governato dalle
istituzioni. Ma la modifica della relazione tra l’individuo e le istituzioni non
può partire dall’oggetto, cioè lo Stato o la società. Questo perché Stato e
società sono entità astratte, definizioni d’insiemi d’individui, e ogni
cambiamento reale non può tentare di modificare queste entità, quanto piuttosto
i soggetti stessi che la compongono. L’individuo che si ribella non lo fa per
cambiare la costituzione o per creare un nuovo ordine sociale, ma lo fa per
trasformare il suo rapporto con gli altri.

A questo punto è possibile tracciare un collegamento tra questo tipo di
approccio e le controculture, in particolare quelle degli anni ’60 e ’70. Questo
periodo rappresenta il momento più significativo dal punto di vista della
“visibilità” per le controculture, anche se è bene ricordare che l’atteggiamento
controculturale, cioè quello di qualcuno che si oppone ai valori della cultura
dominante, è “un fenomeno perenne, probabilmente antico quanto la civiltà umana
e verosimilmente quanto la cultura stessa”.3 Esempi di atteggiamenti
controculturali sono presenti nella mitologia greca classica con la figura di
Prometeo, e nella mitologia ebraico-cristiana, con la figura di Lucifero.

La controcultura degli anni 60’ e ‘70, nacque in California e si diffuse presto
in molte altre parti del mondo, sviluppandosi in due diverse direzioni: da un
lato quella politicamente impegnata, che nasceva nelle università, con il
dichiarato obiettivo di cambiare le istituzioni e che organizzò forme di
protesta radicali come la marcia contro la guerra in Vietnam o le manifestazioni
per i diritti civili. Dall’altro, quella che invece ricercava nuove forme di
aggregazione sociale attraverso l’evasione dalla realtà sia fisica che mentale e
di cui sono esponenti significativi gli *Hippies* e la *New Age*.

È proprio quest’ultima a rappresentare un importante collegamento con la
filosofia di Stirner e la successiva controcultura hacker: già qui si
costituisce una nuova forma di antagonismo, un ricercare mondi diversi
attraverso l’assunzione di droghe o la creazione di ‘comuni’, senza dover
necessariamente abbattere o riformare i mondi precedenti.

Negli anni ’80, con l’invenzione del primo personal computer e la sua successiva
commercializzazione, inizia una fase d’interpretazione di tipo controculturale
dell’oggetto; si tratta di una nuova tecnologia che sarà in grado di cambiare
prima le coscienze e successivamente il mondo.

Quello di “cultura underground” è un termine nato per indicare quell’insieme di
movimenti giovanili di tendenza alternativa, accomunata dall’intento di porsi in
antitesi o in alternativa alla cultura di massa popolare. Sorta all’interno di
società del capitalismo avanzato, in un’epoca di grandi trasformazioni
dell’industria culturale dovuta allo sviluppo dei mezzi di comunicazione di
massa, la cultura underground proponeva un utilizzo alternativo dei nuovi mezzi
di comunicazione, basato sulla diffusione di stili e principi di vita differenti
da quelli della cultura dominante. Fra tutti questi movimenti, una grande
rilevanza fu assunta dal punk: sorto a metà degli anni ’70 e, con il tempo,
declinato in un’infinità di classificazioni, fu caratterizzato da un’energica
rabbia e aggressività nei confronti di qualsiasi forma di controllo, soprattutto
il controllo sociale esercitato dai mass media e dalle organizzazioni religiose.
Proprio per questi tratti in comune, la controcultura telematica ha scelto di
unire il termine “cyber” a quello “punk”, per indicare il mantenimento dello
stesso spirito ribelle trasferito nel cyberspazio.

Grazie alla nascita del pc, l’avvento di internet e del web, nasce un nuovo
territorio in cui rivivere gli ideali controculturali, luogo di libera
espressione e liberazione personale.Ripercorrendo le tappe della nascita della
cultura hacker, Steven Levy4 rintraccia l’origine della storia degli hacker, a
partire dalla fine degli anni ’50, all’interno del Massachuttes Institute of
Technology (MIT), prestigiosa università di ricerca americana. In quegli anni il
termine hack era utilizzato per indicare scherzi goliardici e spettacolari che
gli studenti più ingegnosi mettevano in atto durante l’anno accademico.

Un nuovo significato del termine nacque sempre all’interno del MIT, e
precisamente nella *Tech Model Railroad Club*, un club di modellismo ferroviario
in cui esisteva una divisione (chiamata *Signals and Power*) con il compito di
fornire energia elettrica per i trenini. All’interno di questo gruppo il termine
“hack” descriveva un progetto che aveva uno scopo preciso, ma che aveva anche
quello di divertire attraverso la sola partecipazione. Coloro che si dedicavano
con maggior passione alle attività del gruppo, con un’attenzione morbosa verso
la tecnologia che consentiva di risolvere i problemi in maniera ingegnosa e
creativa, venivano definiti hacker. Da qui nasce quella che Levy chiama la
“prima generazione di Hacker”, che sviluppò una filosofia influenzata sia dallo
spirito antiautoritario tipico della controcultura, sia da altre caratteristiche
che erano considerate tipiche della natura stessa della comunicazione e del
computer.

La scena iniziò a mutare negli anni ’70, quando una nuova generazione di hacker
iniziò a muoversi in California. La prima generazione, rinchiusa nei laboratori
del MIT, non fu coinvolta nelle contestazioni di fine anni ’60, mentre in
California il clima culturale della contestazione aveva dato vita a un nuovo
movimento digitale che si impegnava a rendere accessibile a tutti il computer.
Il sogno del personal computer cominciava a prendere vita, ma contemporaneamente
lo sviluppo di software poneva un singolare dilemma di mercato, relativo alla
sua proprietà: era qualcosa da tutelare in quanto frutto del lavoro creativo dei
programmatori, come sosteneva Bill Gates, o era qualcosa da condividere perché,
come affermavano gli hacker, “tutto quello che serve a migliorare il computer
deve essere libero”5?

Questa svolta economica creò le condizioni per la nascita di quella che per Levy
può essere considerata la “terza generazione” di hacker, nuove leve di
programmatori che erano cresciute in solitudine, perdendo il senso di comunità
che caratterizzava i loro predecessori, che “non sentivano la necessità della
condivisione delle tecniche e della libertà di circolazione delle informazioni.
Le leggi del mercato prevalsero e le aziende di software proprietario
s’imposero” 6.

In questa nuova ottica di tipo economico, nel 1984 Richard Stallman, hacker
della prima generazione, dopo un lungo periodo di riflessione intraprese la sua
battaglia politica, dedicandosi a scrivere i codici per un sistema operativo
libero, avviando il progetto GNU.Nel 1985, sempre Stallman fondò la *Free
Software Foundation* (FSF) “Un’organizzazione no-profit con la missione di
sostenere ed educare in nome degli utenti di computer di tutto il mondo”7, che
mantiene la definizione di software libero per indicare ciò che deve contenere
un particolare programma per essere considerato tale.

Quindi, un software libero fa riferimento alle libertà degli utenti e della
comunità. Tutti hanno la possibilità di eseguire, copiare, distribuire,
studiare, cambiare e migliorare il software. Soltanto in questo modo gli utenti
possono controllare il programma e non esserne controllati. In questo senso,
quindi, un programma proprietario si configura come uno strumento di abuso. Si
crea così una comunità di individui attivi e motivati da precise finalità
politiche: l’obiettivo diviene la libertà di espressione in campo digitale.
Inizia la crociata contro brevetti e diritto d’autore, con la creazione del
cosiddetto *copyleft*.

Nel 1991 Linus Torvalds iniziò la scrittura su un kernel proponendo un approccio
leggermente diverso da quello appena descritto: pubblicare i codici sorgenti
prima ancora che l’applicativo fosse terminato, chiedendo un feedback e una
collaborazione immediata a paritaria a tutti coloro che ne avessero le capacità
e la voglia. Grazie a quest’ulteriore sviluppo, si supera definitivamente l’idea
di “scienziati chiusi in laboratorio” e si passa ad abbracciare l’idea dello
spazio sconfinato di internet “Uno spazio misto e spurio, in cui la
contaminazione fra reale e virtuale diventa la norma e non l’eccezione”.8

Con l’affermazione sempre più ampia di Linux come sistema operativo, i software
della FSF iniziarono a perdere la loro valenza politica e la libertà iniziava a
passare in secondo piano. Per sottolineare questa nuova direzione, si andò alla
ricerca di una nuova terminologia che potesse definire meglio le nuove
posizioni. “Alla fine degli anni Novanta di Bruce Perens ed Eric S. Raymond
fondano la Open Source Initiative (OSI); la quale fa riferimento alla Open
Source Definition, a sua volta derivata dalle Debian Free Software Guidelines,
ovvero una serie di punti pratici che definiscono quali criteri legali debba
soddi- sfare una licenza per essere considerata effettivamente «libera»: o
meglio, con il nuovo termine, Open Source”9. Il termine mantiene in parte una
sfumatura etica, per il riferimento al concetto di “apertura”, che con il tempo
è però uscito dal suo senso puramente tecnico assumendo una connotazione più
ampia.

Da una parte il Free Software poneva l’accento sulle libertà (“il software
libero è una questione di libertà, non di prezzo”10), dall’altra l’Open Source
si concentrava, secondo le logiche di mercato, a cercare i modi migliori per
diffondere un prodotto in base a criteri open. S’impose in maniera netta il
modello meno politico e più orientato alle possibilità commerciali; la Free
Software e la sua filosofia rimase in una posizione marginale e bollata come
“estrema e non sostenibile economicamente”11.

L’etica hacker e il libero scambio sono bollate come “pirateria informatica”, ma
non hanno niente a che fare con il crimine. Si tratta di “meccanismi virtuosi di
sviluppo culturale e tecnologico caratterizzati da una particolare attitudine
verso la conoscenza, una curiosità e una sete di sapere lasciate in eredità
dalle controculture degli anni ‘60 nate all’interno dei campus universitari
statunitensi”.12 L’etica hacker, o meglio lo spirito che la guida, è nato molto
prima dei calcolatori elettronici e dei computer; si manifesta ogni qualvolta
gli individui decidono che la conoscenza in grado di cambiare il mondo è più
importante delle regole stabilite per mantenere l’ordine e lo status quo.

Essere un hacker significa appartenere ad una categoria di persone che condivide
il gusto per risolvere i problemi non perdendo lo spirito del divertimento e del
gioco, non perdendo la curiosità di capire come funzionano le cose, anche a
costo di smontarle pezzo per pezzo. Questo può valere per un computer, per un
ferro da stiro, o per un qualsiasi altro strumento tecnologico, ma anche per le
regole sociali.

L’importanza che assume internet in questo contesto non riguarda “il modo in cui
conosciamo o la conoscenza stessa”13, quanto piuttosto la possibilità per un
infinito numero di individui di poter organizzare il proprio processo di
conoscenza in base a un’infinita disponibilità di informazioni e dati, e quindi
anche di scegliere percorsi che si allontanano dalla “uniformità della
conoscenza” che ha il fine di “mantenere la stabilità del modello prevalente di
ordine”.14 Inoltre internet offre la possibilità di condividere la conoscenza,
anche quella che non segue i “percorsi tradizionali”.

Lawrence Lessig, docente alla Stanford Law School e fondatore dello *Stanford
Center for Internet and Society* è da molto tempo impegnato a studiare le
implicazioni giuridiche del software libero. Nel 2001, insieme ad altre figure
di spicco nell’ambito delle comunicazioni e della proprietà intellettuale come
James Boyle, Michael Carroll ed Eric Eldred, ha realizzato il progetto Creative
Commons (CC), il cui obiettivo “è di dare vita ad un movimento di consumatori e
produttori che contribuiscano alla costruzione del pubblico dominio e che, con
il loro impegno, ne dimostrino l’importanza per la creatività altrui.”15

Il punto di partenza è l’eccessiva estensione sia di ampiezza che di durata del
copyright, che secondo Lessig è il frutto delle pressioni delle grandi lobby,
come la Walt Disney Company, e la conseguente elaborazione di un copyright più
elastico, in cui non tutti i diritti sono riservati, realizzata attraverso le
*Creative Commons Public Licenses* (CCPL).

Quello che è importante sottolineare in questa fase del lavoro è che non si sta
cercando un modo per abolire il concetto di proprietà, come lo stesso Lessig
afferma:
 
> Viviamo in un mondo che onora la “proprietà”. Io appartengo a questo mondo.
> Credo nel valore della proprietà in generale, e credo anche nel valore di
> quella strana forma di proprietà che gli avvocati definiscono “proprietà
> intellettuale”. Una società vasta e diversificata non può sopravvivere senza
> la nozione di proprietà; una società vasta, diversificata e moderna non può
> prosperare senza la nozione di proprietà intellettuale16.

La CCPL si presenta come una posizione intermedia tra due eccessi17: da un lato
chi sostiene la politica del “tutti i diritti riservati”  e dall’altro chi la
nega completamente sostenendo che un’opera pubblicata diventa di pubblico
dominio. Un’opera rilasciata con una CCPL consente determinate libertà che vanno
al di là di quelle permesse dalle libere utilizzazioni consentite nei diversi
Paesi. Si parla di “alcuni diritti riservati” perché i limiti di tali libertà
sono determinati dalle scelte dell’autore.

Nel 1999 Richard Stallman scriveva il saggio L’enciclopedia Universale Libera e
le risorse per l’apprendimento,18 in cui sosteneva che “il World Wide Web avesse
le potenzialità per svilupparsi in un'enciclopedia universale che copra tutti i
campi della conoscenza”19.

Questo manifesto ispirò vari progetti di enciclopedie on line, alcuni, come
l’Enciclopedia Filosofica di Stanford, basati comunque sulle logiche editoriali
di proprietà intellettuale. Altri, come la svedese Susnig.nu, la *Enciclopedia
Libre* e Wikipedia sono invece veri e propri wiki, basati sul modello *Open
Content* (“contenuto libero”). Il concetto trae ispirazione da quello di Open
Source, ma qui ciò che si rende liberamente disponibile all’utilizzo non è il
codice sorgente di un programma, quanto piuttosto i suoi contenuti editoriali.

Le caratteristiche più rilevanti del progetto sono l’apertura e la libertà.
Apertura, nel senso che si basa su contributi redatti da volontari, utenti anche
occasionali che non sono tenuti a specificare identità o competenze, che possono
inserire nuove voci o modificare quelle già esistenti. È comunque possibile la
registrazione di un utente ed esistono alcune forme di protezione da atti di
vandalismo, ma “la totale apertura e, in fondo, la fiducia nello spirito
collaborativo della collettività, rimane un punto forte del progetto”.20

La libertà si riferisce all’utilizzo che chiunque può fare dei contenuti: sono
completamente accessibili, gratuiti, possono essere utilizzati in ogni modo,
anche a scopi commerciali. Questo è possibile grazie alla licenza GNU FDL,
licenza di tipo permissivo che permette distribuzione, creazione di opere
derivate e uso commerciale del contenuto a condizione che si mantenga
l’attribuzione agli autori e che il contenuto resti disponibile attraverso la
stessa licenza. In Wikipedia s’intrecciano tre importanti dimensioni: quella
tecnologica legata al tipo di software utilizzato, quella giuridica relativa
alla particolare forma di gestione e tutela del diritto d’autore, e la
dimensione culturale che richiama l’etica hacker e le modalità di condivisione
della conoscenza che vanno oltre l’ambito informatico e coinvolgono i processi
di produzione delle informazioni. Wikipedia può essere considerata come uno dei
migliori e più convincenti esperimenti di raccolta e organizzazione collettiva
della conoscenza.

Infine il concetto di *hacking* ha avuto uno sviluppo anche nell’ambito
dell’attivismo politico: *l’hacktivism* è l’emergere di un’azione politica
popolare, di auto-attività di gruppi di persone nel cyberspazio. Si tratta di
“una sorta di utopia anarchica, una specie di ideologia dell'autogestione che
sentiva di fare a meno dei direttivi, delle federazioni, delle forme associative
e politiche tradizionali: la rete era partecipazione dal basso e comunicazione
diretta senza filtri”21. Non significa semplicemente “ogni politica associata al
cyberspazio”22 perché in questo caso chiunque potrebbe essere considerato un
attivista digitale, visto che oggi sono poche le attività politiche, sociali e
culturali che in una forma o in un’altra non coinvolgano anche forme di
virtualità. 

Piuttosto, l’hacktivism che emerge dalla fine del ventesimo secolo, è un
fenomeno sociale e culturale specifico, in cui l’azione diretta delle politiche
popolari è stata tradotta e riportata nei mondi virtuali.23 Il movimento
cyberpunk e in generale tutta la scena controculturale, soprattutto americana,
hanno ricevuto nuovi spunti di dibattito e riflessione grazie agli scritti di
Hakim Bey, soprattutto grazie al suo saggio *T.A.Z. Zone temporaneamente
autonome*, (1991).

All’interno del saggio, Bey riprende i concetti e le differenze, già affrontate
in questo lavoro con Max Stirner, tra rivoluzione e sollevazione: 

> Sollevazione, o la forma latina insurrezione, sono parole usate dagli storici
> per etichettare rivoluzione fallite –movimenti che non si conformano alla
> forma prevista, la traiettoria approvata dal consenso; rivoluzione, reazione,
> tradimento, la fondazione di uno Stato ancora più opprimente- il girare della
> ruota, il ritornare della storia ancora e ancora nella sua forma più alta:
> stivale sulla faccia dell’umanità per sempre.24

La sollevazione e l’insorgere permettono invece di uscire da questo circolo
vizioso e di alzarsi, sorgere, per prendersi cura di se stessi e dei propri
interessi. La Rivoluzione è qualcosa che cerca permanenza e durata, mentre la
sollevazione è qualcosa di temporaneo che lascia sempre un cambiamento profondo.
In quest’ottica, l’insurrezione si configura come una rottura sia dal punto di
vista temporale che istituzionale, non dipendendo dalla volontà di creare un
nuovo ordine sociale: “Se la Storia è Tempo come dice di essere, allora la
sollevazione è un momento che salta su e fuori dal Tempo, viola la Legge della
Storia. Se lo Stato è Storia, come dice di essere, allora l’insurrezione è il
momento proibito, un’imperdonabile negazione della dialettica”25. In questo
senso, la dialettica in questione “non è altro che quella tra Rivoluzione e
Stato, Stato interpretato come totem liberticida contro cui combattere tentando
di creare degli spazi alternativi ed antagonisti”26.

La TAZ non deve essere considerata come un fine esclusivo in sé che prenda il
posto di tutte le altre forme di organizzazione; può però procurare una forma di
arricchimento se associata alla sollevazione senza comportare uno scontro
diretto con lo Stato: si tratta di una sommossa che libera temporaneamente
un’area (che riguarda una porzione di terra, di tempo o di immaginazione) che si
dissolve prima che lo Stato la possa schiacciare.

Viene introdotto il concetto di Rete, definito come “la totalità di tutto il
trasferimento di informazione e comunicazione”.27 Alcuni trasferimenti sono
privilegiati e spettano solo ad alcune categorie (ad esempio i dati militari),
il che conferisce alla Rete un aspetto gerarchico; la maggior parte resta però
aperta a tutti, conferendo ad essa anche un aspetto orizzontale. All’interno
della Rete si è sviluppata una Contro-Rete, definita “Tela”, costituita da una
struttura aperta alternativa orizzontale di scambio informatico. Il termine di
Contro-Rete continuerà ad indicare l’uso ribelle e illegale della Tela, come
l’haking: “Rete, Tela e Contro-Rete sono tutte parti dello stesso intero modello
complesso. Si confondono l’una con l’altra in innumerevoli punti”.28

Uno sviluppo molto recente del concetto di hacktivism è rappresentato da
Anonymous. Le difficoltà di fornire una definizione del fenomeno derivano dal
fatto che Anonymous è, come suggerisce il nome, volutamente avvolta nel mistero.
Non ci sono leader, nessuna struttura gerarchica, nessun epicentro biografico.
Tuttavia, come osserva Gabriella Coleman, “mentre ci sono forme di
organizzazione e di logiche culturali che innegabilmente plasmano le sue
molteplici espressioni, resta un nome che qualsiasi individuo o gruppo può
adottare come proprio”.29

Le attività del gruppo nascono su una piattaforma di immagini, il cui scopo era
principalmente il divertimento e la presa in giro. Nel 2007 si realizza il primo
attacco per protestare contro un episodio avvenuto in Alabama, in cui si vietava
il bagno in una piscina ad un malato di AIDS. Successivi attacchi furono
condotti a danno di un conduttore radiofonico con tendenze razziste e di un
pedofilo, fino all’anno successivo, quando, con l’attacco alla chiesa di
Scientology, si delineava un gruppo con una chiara vocazione politica. La difesa
della libertà digitale in tutte le sue forme diventa lo scopo principale
d’azione del gruppo.

Buona parte degli attacchi di Anonymous si basano sul *Distributed Denial of
Service* (DDoS), cioè l’invio di una grossa quantità di dati a un sito, al fine
di saturarne le connessioni e renderlo inaccessibile: “Il gruppo di hacktivisti
sostiene che gli attacchi *Distributed Denial of Service* siano pratiche di
protesta simili a quelle messe in atto dal movimento Occupy: gli hacktivisti
impediscono l'accesso ai siti Web presi come bersaglio nello stesso modo con cui
gli attivisti ostacolano l'ingresso agli edifici”30.

Un nuovo punto di svolta è rappresentato dall’iniziativa *OpTunisia*, nata per
sostenere le proteste dei tunisini contro il governo ra il 2010 e il 2011.
Secondo Gabriella Coleman, OpTunisia ha rappresentato un altro punto di svolta
nella formazione politica di Anonymous come un movimento di protesta.
Considerando che la maggior parte delle operazioni precedenti riguardavano
principalmente Internet o censure, questa operazione si trasferisce esattamente
nel’attivismo per i diritti umani in quanto convergenti con un movimento sociale
esistente”31. A partire da questo momento, moltissime saranno le operazioni di
questo tipo: dopo la Tunisia, l’attenzione si è spostata in Egitto, Libia, Nuova
Zelanda, Italia.

Considerando il fenomeno di Anonymous in un’ottica stirneriana, si potrebbe
affermare che gli hacker di Anonymous possono essere definiti “egoisti”, ossia
degli “unici” che si ribellano e si associano volontariamente solo per il
raggiungimento di un obiettivo comune.

1\. Max Stirner, L’Unico e la sua proprietà
===========================================

*L’Unico e la sua proprietà* rappresenta l’unica opera di Max Stirner, edito da
Otto Wigand nel 1844. Tutto quello che sappiamo su Striner si deve a Henry
Mackay, il poeta rivoluzionario scozzese-tedesco, che ha riportato in luce la
sua opera e alcune informazioni riguardo al suo autore. La sua biografia su
Stirner esce nel 1898 a Berlino: questa data segna l’inizio di una lenta
riconsiderazione della persona e dell’opera di Stirner. Importante fu pure la
pubblicazione dell’edizione popolare dell’*Unico*, uscita presso Reclam nel
1893, che rese l’opera accessibile al grande pubblico. Nel 1898 viene pubblicata
anche la prima edizione degli *Scritti Minori*.

Max Stirner, pseudonimo di Johann Caspar Schmidt (Bayereut 1806 – Berlino 1856),
ha frequentato l’università studiando filologia e filosofia presso Enlargen,
Berlino e Köningsberg, ha insegnato in una scuola di moda femminile, contratto
un matrimonio infelice e morto in solitudine, oppresso dai debiti e abbandonato
da tutti.32

Per più di un decennio incontrò a Berlino molti pensatori radicali, ma è stato
influenzato maggiormente dalla filosofia hegeliana che non dallo spirito
rivoluzionario del 1848.

Se Stirner è un pensatore poco conosciuto, può essere considerato come un autore
ancor meno letto. “Sfortunato come uomo e come scrittore, sacrifica tutta la sua
vita per il solo scopo di poter esprimere le sue idee sull’uomo.[…]Si potrebbe
essere tentati di avvicinare in certo qual modo la figura di Stirner a quella di
Socrate.”33 Entrambi affrontano serenamente il giudizio e la condanna della
società, perfettamente consapevoli del loro ruolo di amanti della sapienza,
anche se si tratta di due concezioni opposte di sapienza. La posizione di
Stirner, come quella di Nietzsche, è di opposizione ai valori socratici. Si
tratta di una posizione che intende demolire tutti gli ideali creati dal
pensiero umano da Socrate in poi, attraverso una dialettica esistenziale portata
con estrema coerenza logica sino all’assurdo. È lo stesso Stirner a non
considerarsi un filosofo: nell’*Unico* indossa volontariamente le vesti di
anti-filosofo perché il filosofo è alla ricerca di una verità verso cui
prostrarsi, esattamente come il credente che cerca una verità sacra a cui
inginocchiarsi.

Stirner invece dichiara di esprimere i propri pensieri non per amore della
verità o per amore verso i suoi lettori, ma solo per spiegare se stesso,
esattamente come fa un uccello che canta solo per saziarsi della sua melodia.

Il libro di Stirner è stato tradotto in inglese da Stephen T. Byington con
un’introduzione di J. L. Walker, su invito di Benjamin R. Tucker, filosofo
anarchico americano, con il titolo The Ego and his Own (*L’Unico e la sua
proprietà*). Chi ha tradotto dal tedesco deve aver incontrato non poche
difficoltà, infatti il risultato finale del titolo non è appropriato, ed è lo
stesso Tucker nella prefazione a chiarire che il senso non è esattamente
equivalente a quello tedesco. “*Der Einzige* significa "l'uomo unico", una
persona di una individualità precisa, ma nel libro stesso l’autore modifica e
arricchisce il significato del termine. L'uomo unico diventa l'ego e un
proprietario (ein *Eigener*), un uomo che è in possesso di proprietà, in
particolare del proprio essere. Come la sua proprietà egli è unico, e il termine
stesso indica che il pensatore che propone questo punto di vista è un
individualista estremo.”34

J\. L\. Walker nella sua introduzione paragona Stirner a Nietzsche e conferisce
una posizione di superiorità al primo, definendolo un vero anarchico non meno di
Josiah Warren. Walker dichiara: "In Stirner abbiamo il fondamento filosofico di
libertà politica. Il suo interesse per lo sviluppo pratico di egoismo per la
dissoluzione dello Stato e l'unione di uomini liberi è chiara e marcata, e si
armonizza perfettamente con la filosofia economica di Josiah Warren”.35

La scelta del termine egoista fa parte della volontà dell’autore di ripristinare
tutti quei termini che hanno assunto un’accezione negativa nel linguaggio che
lui definisce cristiano-metafisico. Cristianesimo per Stirner non coincide con
la dottrina cristiana, ma tutta la dimensione del pensare che procede per
concetti e che quindi non nasce con Cristo, ma da Socrate in poi.

Quando l’*Unico* fu pubblicato, in un primo momento la censura intervenne sulla
sua diffusione, ma poco dopo l’ordinanza fu abolita perché l’opera era
considerata così assurda e priva di logica da non poter rappresentare un
pericolo. In molti non le risparmiarono critiche: da Ludwig Feuerbach a Bruno
Bauer, fino alle più famose critiche di Marx e Engels che “le dedicarono
addirittura un libro dello stesso numero di pagine dell’*Unico*, se si pensa che
*L’ideologia tedesca* […] è per quasi due terzi rivolta a confutare le posizioni
piccolo-borghesi di Stirner.”36

La scoperta di alcuni tratti del pensiero filosofico di Stirner è stata un
processo lento e complesso, “questo anche perché l’impostazione filosofica di
fondo di Stirner è, come quella di Nietzsche, non solo al di fuori ma pure in
forte polemica con la tradizione del pensiero occidentale.”37 La svolta in
questo senso è rappresentata dagli studi di Giorgio Penzo38 che interpreta
Stirner non come una guida ideologica, ma come un pensatore che intende
impostare il suo pensiero filosofico privandolo della trascendenza, “dato che
tutta la possibilità dell’uomo si esaurisce nel puro conoscere, per mostrare
però dove conduce il conoscere se viene portato fino alle sue estreme
conseguenze.”39

È lecito chiedersi se la fortuna di Stirner sia dovuta all’interpretazione del
suo pensiero condotta sotto un punto di vista filosofico, o piuttosto se essa
non sia legata ad una interpretazione condotta da un punto di vista ideologico.
Stirner si può considerare promotore di un’ideologia oppure un filosofo? 

Nel mondo accademico l’autore resta in ombra; negli ultimi decenni il suo
pensiero è stato reintrodotto nei dibattiti sull’anarchismo connesso al
post-strutturalismo e al post-anarchismo. Da sempre però è male interpretato,
sia volontariamente che involontariamente e in questo senso si può affermare che
“i recenti dibattiti offrono la possibilità di spazzare via i pregiudizi del
passato, tuttavia, alcuni vecchi errori sono ancora costantemente ripetuti”.40   

La sua notorietà è legata al fatto che egli è considerato uno dei rappresentanti
più importanti dei movimenti libertari. È riconosciuto dagli anarchici come un
loro teorico e al suo nome vengono accostati quelli di altri teorici
dell’anarchismo a lui contemporanei come Proudhon o Bakunin. Probabilmente, un
ruolo importante in questa interpretazione è ricoperto da Mackay, responsabile
della presentazione al pubblico di Stirner, che da entusiastico poeta
dell’anarchismo, ha messo in luce gli aspetti che più lo coinvolgevano
dell’opera e dell’autore.

Per quanto riguarda il concetto generale di anarchia, per andare oltre la
credenza popolare che lo identifica con il concetto di “caos”41, che è ben
lontano ad esempio dalle teorie di Tolstoj, Godwin e Kropotkin, tutti
riconosciuti come anarchici, George Woodcock afferma:

> L’originaria parola greca anarchos significa semplicemente “senza un
> superiore”; la parola anarchia può dunque essere usata, in un contesto
> generale, per indicare o la condizione negativa del disordine determinato
> dalla mancanza di un governo o la condizione positiva del non avere un governo
> perché il governo non è necessario al mantenimento dell’ordine42.

Nel 1840 Pierre-Joseph Proudhon pubblicava l’opera *Che cos’è la proprietà?* in
cui “rivendicava a se stesso, per primo, il titolo di anarchico”.43 L’autore
aveva intuito l’ambiguità del greco *anarchos* ed era risalito ad esso proprio
per questo motivo, per sottolineare che la critica all’autorità che teorizzava
non implicava necessariamente una difesa del disordine.

Il concetto generale espresso da Proudhon nel 1840 sarà ciò che lo accomuna con
anarchici di un periodo più tardo, come Bakunin e Kropotkin, nonché a molti
pensatori precedenti e successivi, come Godwin, Tolstoj e lo stesso Stirner, che
elaborarono sistemi anti-governativi “senza accettare il nome di anarchici”.44
Ed è proprio in questo senso che si può parlare di anarchismo al singolare,
nonostante le sue variazioni: “Un sistema di pensiero sociale, mirante a
cambiamenti fondamentali nella struttura della della società e in particolare
alla sostituzione dello stato autoritario con qualche forma di libera
cooperazione tra individui liberi”45.

Gli anarchici possono sostanzialmente essere d’accordo sui fini ultimi, ma
sviluppano posizioni diverse sulla tattica più conveniente per raggiungerli, e
questa differenza è evidente soprattutto per quanto riguarda la violenza: 

> I tolstojani non ammettevano la violenza in nessuna circostanza; Godwin
> sperava di determinare cambiamenti mediante discussione; Proudhon e i suoi
> seguaci mediante l pacifica proliferazione di organizzazioni cooperative;
> Kropotkin accettava la violenza, ma solo a malicuore e solo perché la riteneva
> inevitabile nelle rivoluzioni e considerava le rivoluzioni tappe inevitabili
> del progresso umano; persino Bakunin, benché combattesse su molte barricate ed
> esaltasse il carattere sanguinario delle insurrezioni contadine, ebbe dei
> momenti di dubbio.46
 
Michael Schmidt e Lucien van der Walt sostengono, nel loro saggio *Black Flame*
(2009) che le concezioni comuni di anarchismo includono molti pensatori che non
erano effettivamente anarchici, e tracciano questo fenomeno per lo studio
seminale di Eltzbacher dell'anarchismo in cui ha individuato "sette saggi":
Godwin, Stirner, Proudhon, Bakunin, Kropotkin, Tucker, e Tolstoj”.47 Le analisi
di Eltzbacher e quelle di Schmidt e van der Walt risultano molto diverse fra
loro, questo soprattutto a causa della sintesi “altamente selettiva e
superficiale”48 condotta dagli autori di *Black Flame* sull’opera di Stirner.
Questi ultimi sostengono che esiste una sola versione corretta del canone
anarchico che, nella lista dei sette anarchici, si rintraccia solo in Bakunin e
Kropotkin. Per includere tutti si dovrebbe definire la dottrina a un livello
così generale da permettere di eliminare le radicali differenze tra questi
pensatori.

L’egoismo di Stirner non è un invito agli individui ad essere meno altruisti, ma
piuttosto un invito a capire la differenza tra un comportamento altruistico
motivato da un interesse personale, o “l’amore per gli altri”49 e un
comportamento motivato dal senso del dovere, “un dovere sacro”.50 Non si tratta
di una negazione della collaborazione, gli individui sono esortati ad associarsi
liberamente in tutte le forme che possono consentire un avanzamento dei propri
interessi.

Secondo Elmo Feiten esistono dei punti di contatto tra l’opera di Stirner e i
classici dell’anarchismo, “indipendentemente dal fatto che Stirner stesso sia
considerato un anarchico o no”51. Data però la problematicità del suo pensiero,
“è del tutto plausibile non etichettare Stirner come anarchico, semplicemente
perché le implicazioni politiche della sua critica non sono sufficientemente
chiare per confrontarle con le politiche degli anarchici”52

In genere, quando si associa Stirner all’anarchismo, egli viene collocato in una
precisa posizione, quella dell’anarco-individualismo: “Si tratta di un gruppo
minoritario e quasi marginale, che si presenta nella forma di una specie di
atomismo nei rapporti sociali.”53 Ancora, si tratta di un indirizzo che si
sviluppa piuttosto tardi e che è rappresentato proprio da Stirner e
dall’influenza che inizia ad esercitare verso la fine del secolo
sull’orientamento dei gruppi anarchici. Si deve anche riconoscere che gli stessi
teorici del movimento anarchico hanno sentito la necessità di distinguere
l’anarchismo individualista di Stirner dal movimento anarchico quale puro
movimento sociale.

Secondo Penzo, associando Stirner al concetto di egoismo si è portati a pensare
a quest’ultimo elemento alla luce di una dimensione inautentica. Spesso infatti
si associa l’egoismo all’anarchismo, per spiegare il primo attraverso il
secondo, correndo però il rischio di precludersi la via per comprendere
l’orizzonte “ontologico” dell’egoismo. Così come è riduttivo considerare Stirner
come un rappresentante poco significativo della sinistra hegeliana. Fa parte
senza dubbio di questa categoria, perché è quella la cultura del suo tempo e il
periodo storico della sua attività, ma la tematica di Stirner sorpassa quel
periodo storico e quella cultura aprendo una nuova dimensione del filosofare.

Penzo interpreta Stirner come il rappresentante di una nuova dimensione
esistenziale dell’essere secondo la quale l’autenticità dell’io deve ritrovare
il suo fondamento solo in se stesso: “l’unico denota il momento autentico
dell’io come sua proprietà, la cui dinamica interna è la rivolta che indica
l’atto di un superamento continuo della dimensione santa dell’oggetto perché
l’io possa essere se stesso.”54 Le dimensioni dell’unico e della rivolta
implicano la dialettica esistenziale stirneriana i cui poli sono dati dal
momento dell’io come spirito e da quello autentico dell’io come Ego. Quindi,
secondo Penzo, Stirner non intende eliminare i vari ideali, ma vuole superare la
loro “santità”: niente può essere pensato sopra se stessi, altrimenti cade il
concetto autentico di verità secondo cui tutto ciò che è al di sopra dell’io
come unico è falso, e tutto ciò che è al di sotto è vero.

Non è corretto affermare, come fanno spesso i critici, che Stirner muova una
guerra a tutti gli ideali, specialmente quello religioso. Questo perché,
nell’ottica stirneiana, non si può dire che la religione sia un ideale tra gli
altri o il più nobile tra questi, ma piuttosto si deve dire che ogni ideale è,
in quanto tale, religione.

La critica alla santità e all’autorità che si fonda sul concetto non è solo una
critica filosofica ma anche pedagogica: l’educazione autentica, infatti,
dovrebbe liberare da ogni autorità, da ogni schiavitù dell’oggetto del sapere.
Questo porta Stirner a criticare anche la concezione della legge, che si
configura come la forza di uno o più singoli sugli altri, e che assume la
dimensione di “santità”: “Per Penzo il compito di Stirner diventa quello di
fugare tale santità della legge riportandola alla dimensione dell’assurdo e
dimostrando come essa sia spirito e puro fantasma, il tutto con considerazioni
paradossali, considerate spesso così poco razionali da non poter essere prese
sul serio e per questo Stirner ancor oggi incontra difficoltà ad entrare nella
storia del pensiero occidentale.”55

L’elemento più rilevante emerso dalle analisi del pensiero di Stirner è
l’originalità della sua critica che tenta di decostruire il senso di ciò che
normalmente si attribuisce alla coscienza con la forza dell’abitudine: “Nozioni
come *libertà, verità,* vengono da Stirner private della loro *aura* metafisica
che da sempre le connotava e denunciate come illusioni.”56 La libertà diventa
quindi un contenitore privo di contenuto che spinge l’io alla continua ricerca
di qualcosa che ha in sé solo la forza della suggestione, e Stirner cerca di
abbandonare questa concezione identificando la libertà con le reali possibilità
di ciascuno. La libertà va intesa come la possibilità e capacità individuale che
ognuno ha di rendersi libero.

La volontà stirneriana di collegare il concetto di libertà a quello di proprietà
facendo dell’io il centro e il punto di riferimento di tutta la realtà, nasce
dal rifiuto di un’idea di libertà che egli considera metafisica e propria di
tutta la tradizione etico-politica occidentale. E’ proprio in questa
indifferenza e svalutazione della libertà che si rappresenta l’immagine più vera
dell’uomo. “In realtà la libertà deve essere vista solo come capacità
dispositiva e acquisitiva dell’uomo in ordine alle sue volontà, ai suoi desideri
per esaudirli e al limite per cancellarli. L’uomo vuole paradossalmente la
libertà per non sentirne più il peso, per trasformare la libertà in
proprietà.”57

Stirner introduce anche una distinzione tra emancipazione ed auto-liberazione:
nel primo caso si tratta di una libertà che è concessa da altri e che non rende
mai un uomo davvero libero; l’auto-liberazione è invece il modo che ha l’uomo
per liberarsi da solo, diventare padrone di se stesso esclusivamente attraverso
la propria forza, una forza che non ha nulla a che vedere con la violenza, ma
che identifica la capacità di appropriarsi del mondo.

Allo stesso modo il concetto di verità, inteso come un valore capace di dare un
ordine e un indirizzo alla vita individuale e collettiva, viene superato in
favore di una verità che “può imporsi come tale alla coscienza solo sulla base
di un doveroso riconoscimento (necessario per la sua costituzione) attraverso il
quale l’individuo stabilisce con la sua servitù, l’appartenenza ad un
determinato ordine di valori.”58

L’Unico è diviso in due parti: la prima è intitolata *Der Mensch* (L’uomo) ed
inizia con una esplicita polemica nei confronti di Feuerbach e Bruno Bauer,
“visti come rappresentanti di punta dell’umanitarismo degli anni si Stirner,
come le ultime espressioni intellettuali della modernità, esito estremo del
cristianesimo”59; la seconda parte è chiamata *Ich* (Io), mantiene sempre il
carattere critico ma ha anche una connotazione propositiva perché Stirner
presenta le categorie filosofico-politiche che secondo lui sono necessarie a
definire una vita individuale non alienata.

Nella prima parte dell’*Unico* è ripercorsa la storia delle vicende umane sia da
un punto di vista di crescita individuale (*Una vita d’uomo*), sia nella
prospettiva più generale della storia dell’umanità (*Uomini del tempo antico e
del moderno*), prendendo in prestito la dialettica hegeliana per realizzare
delle triadi come bambino-giovane-adulto, atichi-moderni-contemporanei. “Sempre
secondo la lezione hegeliana, l’età originaria dell’umanità (antichità) e
dell’individuo (infanzia) viene identificata con la dimensione naturalistica,
«il mondo della natura», mentre l’età successiva (modernità-gioventù) con la
dimensione spirituale, «il regno dello spirito».”60

Nella seconda parte Stirner mantiene la tripartizione ternaria e presenta alcune
categorie originali.

Nel titolo dell’opera, *L’unico e la sua proprietà*, sono nominati i due
fondamenti della filosofia stirneriana: da una parte il nuovo soggetto,
*l’unico*, dall’altra una nuova concezione dell’oggetto che viene inteso come
*proprietà*, entrambi pienamente teorizzati nella seconda parte, insieme al
concetto di “rivolta” e alla “unione degli egoisti”. Ultimo concetto
fondamentale è quelli di “egoismo”, definito da Stirner come il principio stesso
della vita, “è inglobante perché è l’essenza di ogni cosa, nello stesso tempo è
escludente perché unifica solo formalmente: il carattere che accomuna ogni
esistente e la sua assoluta originalità, l’irriducibile diversità.”61

La teorizzazione dell’egoismo, inteso come motore della storia e delle vicende
personali di ogni individuo, non equivale a dire che esso esiste in una sola
forma: ne esistono tante quanti sono gli uomini che la storia ci mostra. Data la
prevalenza di soggetti alienati, le manifestazioni di egoismo sono sempre
incomplete e contraddittorie. Un essere alienato è per Stirner colui che non
vive in funzione di sé, ma che vive per l’altro da sé, che storicamente ha preso
la forma di natura presso gli antichi e di spirito a partire da Cristo, fino ai
liberali, che hanno teorizzato una nuova forma di spirito che è la società. “Con
«ego» Stirner intende il singolo individuo, non riconducibile ad alcuna
categoria universale e intersoggettiva; con «altro da sé» ogni dimensione
religiosa, politica e sociale che trascende il singolo, lo considera come sua
parte e gli si impone come superiore, regolandone la vita e l’azione con una
serie di regole deontologiche di varia natura.”62

> La forma più elevata di egoismo deve ancora realizzarsi: coincide con la
> coscienza che il singolo ha di sé in quanto assoluto, che lo spinge a ridurre
> l’altro da sé a “oggetto” e quindi a strumento finalizzato alla propria
> realizzazione. Di conseguenza, “a parere di Stirner, Dio e lo stato vivranno
> fino a quando l’uomo spenderà la propria vita per servirli, e parimenti
> morranno soltanto quando l’individuo servirà la sua propria causa, quella
> egoistica.”63

1\.1  Rivolta e Rivoluzione
---------------------------

Il termine *Empörung*, attraverso il quale Stirner definisce il concetto di
“rivolta”, si traduce, oltre che con “ribellione”, anche con indignazione. Il
concetto è introdotto come elemento di polemica contro le teorie della proprietà
di Bruno Bauer e della proprietà sociale. Stirner sostiene che attraverso
quest’ultima concezione della proprietà non c’è stato nessun miglioramento
rispetto alla proprietà borghese; “si nega, anzi, la stessa conquista della
proprietà privata, conquista della borghesia che se per un verso fa dipendere la
proprietà dallo Stato, che la regola e la garantisce, per un altro sostiene per
la prima volta il diritto alla proprietà individuale, che ognuno è
proprietario.”64 Comunisti e umanitari infatti ripristinano relazioni feudali,
in cui nessuno ha una proprietà e tutti ricevono ogni cosa dalla società da cui
sono dipendenti, in un sistema definito di “vassallaggio globale”.65

La critica del potere statale elaborata da Stirner si sviluppa nel contesto
storico delle rivoluzioni democratiche, socialiste e comuniste del XVIII e XIX
secolo, e ciò è curioso perché ognuna di queste rivoluzioni aveva come obiettivo
quello di estendere i diritti umani e garantire la piena partecipazione di tutti
gli individui al processo politico. Ognuna prometteva il superamento
dell’alienazione attraverso l’applicazione del potere statale nella società e
nella vita degli individui.  Stirner critica queste rivoluzioni perché esse, nel
tentativo di superare le vecchie forme di alienazione e repressione dei regimi
precedenti, ne hanno introdotte di nuove. Si tratta di moderne espressioni
politiche di alienazione.

L’egoismo dialettico di Stirner concepisce tutti i sistemi sociali come delle
costruzioni che nascono in determinate circostanze storiche a causa delle
debolezze dei vecchi regimi e della forza dell’opposizione. Dunque, “anticipando
Marx, Stirner ha distinto il borghese dal cittadino, ma ha usato questa
distinzione per arrivare ad una conclusione completamente opposta rispetto a
socialisti e comunisti. Nell’*Unico e la sua proprietà*, afferma che non è
l’individuo o la vera persona che è stata liberata dalla rivoluzione, ma solo il
cittadino come specie, la categoria del liberalismo politico”.66

La società comunista è lontana dall’essere associazione, resta una dimensione
astratta perché prende in considerazione non il singolo, ma l’uomo. “Infatti,
parlando del benessere di tutti, il comunista parla in fondo di un benessere
astratto, poiché, evidentemente, non aspirando tutti allo stesso benessere, si
ha che il benessere di tutti non costituisce il benessere di ciascuno”.67

A questo punto Stirner presenta il concetto di rivolta in alternativa a quello
di rivoluzione, ed è proprio sulla base dell’analisi delle differenze tra i due
concetti che la rivolta viene caratterizzata:

> “rivoluzione e rivolta non devono essere considerati sinonimi. La prima
> consiste in un rovesciamento della condizione sussistente o status, dello
> Stato o della società, ed è perciò un’azione politica o sociale; la seconda
> porta certo, come conseguenza inevitabile, al rovesciamento delle condizioni
> date, ma non parte di qui, bensì dall’insoddisfazione degli uomini verso se
> stessi, non è una levata di scudi, ma un sollevamento dei singoli, cioè un
> emergere ribellandosi, senza preoccuparsi delle istituzioni che ne dovrebbero
> conseguire. La rivoluzione mirava a creare nuove istituzioni, la ribellione ci
> porta a non farci più governare da istituzioni, ma a governarci noi stessi, e
> perciò non ripone alcuna radiosa speranza nelle «istituzioni»”.68

L’egoista è un nemico dello stato ma non cerca di acquisire il potere politico o
di trasformare la società. “Invece di «prendere accordi», Stirner sostiene che
il ribelle si occupa di «esaltare se stesso» al di sopra dello stato e delle
condizioni sociali esistenti”.69 Le trasformazioni ci saranno ma saranno una
conseguenza dell’azione e non il suo fine.

La rivolta rappresenta il solo modo che ha l’unico per mettersi in rapporto con
l’esteriorità che tende ad annullare la sua unicità; si tratta di una continua
rivolta contro tutto ciò che potrebbe dominarlo. Se la rivolta non porta alla
liberazione da ogni impedimento, da ogni soggezione ideologica, l’unico non
riuscirà mai a raggiungere il suo scopo: innalzarsi al di sopra di tutto quello
che potrebbe sottometterlo. Ne consegue che “ci sarà sottomissione finchè
l’io-unico cercherà l’onnipotenza fuori di sé invece di vederla in sé.”70 

Il nucleo di fondo della filosofia di Stirner consiste nel focus sul rapporto
tra soggetto e oggetto, in cui l’unico cerca costantemente di superare la
minaccia di sottomissione da parte dell’oggetto. La rivolta segna, infatti, il
passaggio da una dimensione religiosa del rapporto soggetto-oggetto, cioè
alienante, ad una dimensione egoistica dello stesso, per cui l’oggetto non è più
sacro ma diventa proprietà dell’io.

Le motivazioni della rivolta non sono di ordine sociale, ma personale: è
l’individuo che non sopporta più di essere governato dalle istituzioni. Ma la
modifica della relazione tra l’individuo e le istituzioni non può partire
dall’oggetto, cioè lo Stato o la società. Questo perché Stato e società sono
entità astratte, definizioni di insiemi di individui, e ogni cambiamento reale
non può tentare di modificare queste entità, quanto piuttosto i soggetti stessi
che la compongono. L’egoista non si rivolta per scrivere una nuova costituzione
o per creare un nuovo ordine generale, “ma per trasformare l’ordine delle sue
relazioni, il ruolo che egli ha nel rapporto con gli altri.”71

L’esempio di rivoltoso che Stirner propone è l’ultimo che ci si aspetterebbe e
si manifesta nella critica che Stirner muove a Feuerbach. Nell'*Essenza del
cristianesimo* (1841), Feuerbach ritiene che la religione, in particolare quella
cristiana, abbia un contenuto positivo che consente di scoprire quale sia
l'essenza dell'uomo. La critica di Stirner si concentra, in particolare, sul
fatto che attraverso questa impostazione non si supera la subordinazione
dell'individuo allo spirito72: “Cercando un paragone che illustri più
chiaramente la cosa, mi viene in mente, contro ogni aspettativa, la fondazione
del cristianesimo.”73 

Quindi Gesù Cristo diventa il primo ribelle e il cristianesimo un esempio di
rivolta storica. Per Stirner il cristianesimo segna il passaggio dal mondo
antico alla modernità, il passaggio dalla “condizione naturale” al “mondo dello
spirito”. Gesù è descritto da Stirner come un individuo che ha vinto il mondo
antico, naturale, prima trasformando le coscienze, e poi, con la
generalizzazione di tale processo, le condizioni di vita “che i moderni
cristiani hanno adeguato alla nuova coscienza spirituale, edificando il mondo
dello spirito.”74 Secondo Stirner furono certamente gli antichi stessi a
generare il nuovo che li soppiantò, cioè la dottrina cristiana, soprattutto
attraverso le scuole socratiche, scettici, stoici e cinici che predicavano
l’indifferenza e il non-senso del mondo, preparando le coscienze all’avvento
dell’annuncio cristiano di un “mondo vero”.

Il secondo momento che corona il superamento della condizione naturale si ha
quando Gesù e successivamente i suoi discepoli iniziano a vivere da uomini
spirituali, orientando l’esistenza in funzione dello spirito:

> Ma perché non era un rivoluzionario, un demagogo, come gli ebrei avrebbero ben
> voluto, perché non era un liberale? Perché egli non si aspettava la salvezza
> da un cambiamento delle *condizioni* e tutto quell’ordinamento gli era
> indifferente. […] egli non conduceva alcuna battaglia liberale o politica
> contro l’autorità costituita, ma voleva, incurante di quell’autorità e da essa
> indisturbato, percorrere la propria *strada*.75 

La ribellione di Cristo non nasce nei confronti della costituzione politica di
Roma, ma perché egli è insoddisfatto della propria esistenza e questa esistenza
non dipende da uno status politico. Il rivoltoso vuole solo percorrere la
propria strada, al di fuori e al di sopra della politica, anche se quando questo
diventerà un comportamento diffuso porterà necessariamente alla distruzione
dell’ordine politico esistente. Se ne può concludere che “proprio perché non gli
interessava il rovesciamento dell’esistente, egli ne era in realtà il nemico
mortale e il suo vero distruttore.”76

Il rivoluzionario è considerato un riformatore, visto che intende solo
modificare l’ordine gerarchico esistente con uno che considera migliore, “ma
lasciando in sostanza invariata la dipendenza dell’individuo dal principio che
lo trascende e lo ingloba come suo membro e sua parte”77; il ribelle è il vero
distruttore dell’ordine esistente perché lo abbandona e lo svuota di ogni suo
contenuto. La ribellione cristiana con il tempo ha però assunto il carattere
della rivoluzione perché lo “spirito” in nome del quale la ribellione era nata,
ha assunto il carattere di principio su cui fondare la morale, il diritto e
quindi un nuovo ordine politico con le sue istituzioni. 

A distanza di molti secoli, con la rivoluzione francese il principio religioso e
le gerarchie socio-politiche spostano il loro centro da Dio all’Uomo e quindi
sul concetto di nazione, popolo, ma questo cambiamento non muta la dipendenza
dell’individuo da qualcosa che gli è sempre esterno ed estraneo. Nei confronti
di queste metamorfosi dello spirito, l’egoista per Stirner deve sempre
ribellarsi come ha fatto Gesù Cristo, ma mai per cercare un nuovo principio
generale che darà vita a nuovi vincoli per sottomettere i singoli, piuttosto in
nome di se stesso, cioè “dell’individuo contro ogni gerarchia e ogni principio
universale”.78

Gli antichi si rivoltarono contro la natura, trasformandola da oggetto di
venerazione a strumento per la soddisfazione dei loro bisogni e diventarono così
“moderni”. Allo stesso modo, per diventare egoisti, i moderni dovranno usare lo
spirito nello stesso modo in cui Cristo e primi cristiani usarono la natura.


1\.2  Società e Associazione
----------------------------

Nel capitolo che ha per titolo “l’individuo proprietario” Stirner nomina per la
prima volta l’”unione”, a conclusione di una critica mossa alla società e allo
Stato, descritti come mediatori delle relazioni tra individui che, in virtù di
una astratta natura umana, legano gli uomini fra loro con vincoli religiosi,
cioè morali, giuridici e politici. Lo Stato ostacola l’azione del singolo
sottomettendola all’interesse generale, non permettendo al singolo di emergere e
farsi valere.

Per Stirner, un popolo può essere libero solo a spese del singolo, anzi, quanto
più il popolo è libero, tanto più il singolo sarà legato, e spiega questa
relazione portando come esempio il popolo ateniese, che nel suo momento di
massimo splendore e libertà istituì l’ostracismo, allontanò gli atei e condannò
a morte Socrate, il più brillante tra gli ateniesi.

La differenza tra società e associazione è che la prima si fonda su leggi
generali che intendono essere oggettivamente valide e per questo motivo
alienanti, l’associazione si fonda invece sul superamento di ogni santità: “Se
il concetto di rivolta esprime il rapporto autentico dell’io rispetto a se
stesso, cioè al proprio fondamento, il concetto di associazione esprime il
rapporto autentico dell’io rispetto all’altro”.79 Chiaramente questo ha luogo
quando s’intende l’altro non come membro di una società governata da leggi
astratte, ma come unico.

Questa definizione di associazione è soltanto apparentemente un concetto
anarchico nel senso corrente del termine, dato che Stirner non vuole eliminare
il vincolo che unisce in società gli individui. “Egli non vuole eliminare la
legge ma solo rendere problematica la dimensione formale di essa, quella cioè
estrinseca che tende a configurarsi in modo dogmatico”80 e che Stirner definisce
santità.

Stirner dichiara: “Così noi due, lo Stato e io, siamo nemici. Io, l’egoista, non
ho a cuore il bene di questa «società umana», non le sacrifico nulla, mi limito
ad utilizzarla; ma, per poterla utilizzare appieno, preferisco trasformarla in
mia proprietà, in mia creatura, ossia io l’anniento e costruisco al suo posto
l’i*unione* (Verein) *degli egoisti*.”81 Si tratta quindi di un’alternativa allo
stato che articola le relazioni dalla prospettiva dell’”individuo proprietario”.
Da un lato, quindi, Stirner assolutizza il valore del singolo, ma dall’altro non
trascura il legame che esiste tra il soggetto e il mondo in cui vive.

Come ha notato John Welsh82, Stirner non era solo molto critico nei confronti
delle ideologie come l’umanesimo e le relazioni di potere istituzionalizzate
come lo stato, lo era anche nei confronti della società. Egli credeva che il
concetto di macrolivello di una società o di una nazione tendesse a imporre
sugli individui identità e credenze alienanti e vincolanti.

Così come molti altri teorici sociali, Stirner postula un conflitto e
un’opposizione fondamentale tra la società e l’individuo, ma a differenza della
maggior parte di questi teorici, egli non ritiene necessario risolvere tale
conflitto o riconciliare le due posizioni: “Nelle teorie del contratto sociale
di Hobbes, Locke e Rousseau e in quelle sociologiche di Mead, Cooley e Mills la
relazione tra individuo e società è concepita come un reciproco scambio in cui
entrambe le parti sono presumibilmente in grado di forzare la concessione
dell’altro. In questo modo, ognuno da e riceve dalla relazione.”83 

Nelle teorie del contratto sociale si presuppone un violento e pericoloso stato
di natura in cui gli individui corrono molti rischi, compresa la morte, a causa
dell’assenza di una coercizione istituzionalizzata che abbia potere sufficiente
per prevenire la violenza. In questo caso la relazione tra società e individuo
nasce per proteggere quest’ultimo, che si sottomette al potere e all’autorità
dello stato. Soltanto Locke, nella sua teoria, cerca di mantenere una forma di
individualismo e di protezione dell’individuo dallo stato. Per Stirner lo “stato
di natura” non è un’egoistica guerra di tutti contro tutti, ma “un’esistenza
strutturata, istituzionalizzata e collettivizzata in cui lo stato, la società e
la cultura sono precedenti alla nascita e alla interazione della persona. Per
Stirner la società è lo stato di natura.”84

La “proprietà egoistica” e l’”unione degli egoisti” sono le due modalità
attraverso cui l’unico si rapporta al modo in cui vive con gli altri:

> Ciascuno è il centro del suo mondo. Mondo è soltanto ciò che non è egli
> stesso, ma che però gli appartiene, che è in rapporto con lui, che esiste per
> lui.  Tutto gira intorno a te; tu sei il centro del mondo esterno e il centro
> del mondo del pensiero. Il tuo mondo arriva fin dove arriva la tua capacità di
> capire; e ciò che tu abbracci, è tuo per il solo fatto che lo comprendi. Tu
> unicamente sei *unico* soltanto insieme alla tua proprietà.85 

Affermare che ciascuno è il centro del suo mondo, significa di fatto negare ogni
forma di gerarchia o autorità che invece vorrebbero imporsi e spogliare
l’individuo della sua proprietà.86Inoltre essendo ogni individuo unico, non
esiste più un solo centro, ma tanti centri quanti sono gli unici. L’unicità si
lega al concetto di reciprocità. Stirner definisce il mondo come l’insieme di
relazioni che l’unico intrattiene con l’altro da sé, “la centralità rispetto al
mondo è dunque centralità rispetto ai propri rapporti, ed essendo questi ultimi
«espressione di reciprocità, azione, *commercium* tra i singoli», vediamo ancora
come centralità e reciprocità si presuppongano a vicenda.”87

Il concetto di unico esclude quello di assolutezza, perché assoluto significa
privo di rapporti e relazioni. Per Stirner ogni unico è irriducibilmente diverso
dall’altro, l’uguaglianza potrebbe esistere solo se si introducesse un mediatore
esterno agli unici come lo Stato o Dio che stabilisca una gerarchia. In questo
modo non si valuta più l’altro sulla base del rapporto che si intrattiene con
lui, ma in relazione al mediatore che collega entrambi.

“Se ognuno, in quanto unico, è «esclusivo ed esclusivista», la sua esistenza non
può tendere alla comunità, bensì all’*unilateralità*.”88Senza qualcosa che ci
accomuni, non c’è nulla che ci separi o ci renda nemici. E’ proprio la
consapevolezza dell’unilateralità che ci consente di sollevarci contro la
gerarchia e la dipendenza che genera ogni Stato, e porre le basi per una forma
associativa con presupposti molto diversi. Perciò, ne conclude Stirner, “non
miriamo dunque alla comunità, ma all’unilateralità. Non cerchiamo la comunità
più comprensiva possibile, la «società umana», ma cerchiamo invece negli altri
soltanto mezzi e organi che possiamo usare come una nostra proprietà!”89

Questo passaggio è stato duramente criticato perché sembra tendere ad
incentivare lo sfruttamento di un altro essere che viene considerato solo un
“mezzo”, sembra inoltre negare che possano esistere relazioni di tipo non
conflittuale; se invece si assume dal punto di vista della filosofia stirneriana
si nota come in effetti questa rappresenti l’unica reale forma di relazione che
non neghi la centralità dell’individuo e che si basi sulla reciprocità.

La concezione religiosa consiste nell’attribuire un valore assoluto a qualcosa,
indipendentemente dal rapporto che si intrattiene con essa. Essa deve quindi
essere posta in relazione ad un essere superiore che fa da mediatore e di cui la
cosa stessa fa parte (lo Stato, Dio). In questo modo non si entra mai in
relazione con il singolo, ma con il cittadino, il cristiano:

> Se io mi prendo cura di te, perché ti voglio bene, perché il mio cuore trova
> alimento e le mie esigenze un soddisfacimento in te, non accade in virtù di un
> essere superiore […] ma per piacere egoistico: tu stesso, col tuo modo di
> essere, hai per me valore, infatti il tuo essere non è un essere superiore,
> non è superiore a te, non è più generale di te, è unico come te stesso, perché
> è te stesso.90

L’egoismo diventa l’unico modo per riconoscere e apprezzare il valore
dell’altro, e anche se non posso cogliere l’interezza della sua unicità, posso
comunque apprezzare le caratteristiche che mi si presentano. Nel rapporto con
l’altro, inteso come oggetto e non più soggetto, “se io ne posso far uso, mi
intenderò e mi accorderò con lui per accrescere la *mia potenza* con questa
allenza e per poter riuscire, riunendo le nostre forze, dove uno solo
fallirebbe.”91

L’attenzione posta da Stirner al concetto di utilità delle relazioni che gli
unici intrattengono tra di loro mostra come tra questi individui ci sia un
reciproco interesse alla persona e non alla reciproca rinuncia come invece
pretendono la religione e la morale. 

Sulla base di queste nuove caratteristiche, deve realizzarsi l’unione degli
egoisti, il cui elemento principale è che il singolo si associa per il suo
interesse individuale e non in virtù di un “bene comune”: “Pensare, come fa lo
stesso Proudhon, alla società come un soggetto collettivo, ad una «persona
morale», significa condannare, in nome di un religioso interesse generale, il
singolo individuo ad una delle peggiori forme di dispotismo.”92 L’unico deve
usare la società come un mezzo, non essere un mezzo per la società. L’unico può
affermarsi solo nell’unione, perché l’unione non lo possiede, ma è l’unico a
possederla o che può farne uso. Soltanto così si riconosce la proprietà, perché
questa non è data da un essere superiore come nella proprietà privata (che è una
concessione dello Stato). “Insomma, la questione della proprietà non si può
risolvere così facilmente come vagheggiano i socialisti e perfino i comunisti.
Essa si risolverà soltanto con la guerra di tutti contro tutti.”93

Solo quando si accetterà il continuo scontro tra gli interessi degli unici
l’unione potrà diventare “come una «spada» per accrescere le proprie capacità e
dunque, poiché ognuno è unico solo insieme alla sua proprietà, rafforzare il
sentimento della propria unicità.”94 La scelta dell’associazione o il suo
abbandono devono essere sempre scelte volontarie e con questa scelta l’individuo
non rinuncia alla sua individualità, ma anzi l’afferma. L’unico compie questa
scelta perché ha bisogno degli altri per raggiungere obiettivi che da solo non
riuscirebbe a raggiungere e quindi nella vita associativa in realtà non
sacrifica niente.

La durata dell’unione dipende dagli interessi degli unici che la compongono, che
prendono parte di volta in volta alla creazione delle regole. Non si tratta solo
di un’alternativa alla società, ma anche di uno strumento per la
ribellione.Unilateralità e separatezza di ogni unico restano tali, anzi si
realizzano pienamente solo nell’unione: “La separatezza non rimanda ad una unità
di genere, ma spezza in modo irreparabile quella unità. Allude, non alla
frammentarietà di un mondo altrimenti unito, ma prospetta la pluralità in luogo
dell’unità.”95 Le parti restano separate e non possono comunicare tra loro, come
le monadi di Leibniz.

Il contrasto fra queste parti, il conflitto, cessa nella separazione in virtù
dell’unicità. Questa unicità comporta una tale differenza tra gli unici da non
poter rendere possibile lo scontro. Non può esserci uno scontro tra esseri che
non hanno niente in comune.Non si può far coincidere l’unicità con l’isolamento,
perché l’individuo che si associa non è da considerare meno egoista di quello
che ne rimane all’esterno: “Ciò che cambia è solo l’oggetto del suo
egoismo.”96Anzi, secondo Stirner amare un uomo rappresenta un forma di
“proprietà” in più rispetto a chi non ama nessuno: “Stirner non difende il
potere dell’individuo di dominare gli altri, in quanto dimostra in modo
estremamente significativo come l’esercizio del potere sia una pratica
fortemente disindividuizzante.”97 Inoltre il dominio è qualcosa che nasce dal
desiderio di fuggire da una condizione di uguaglianza, mentre se il presupposto
di partenza è la differenza di ogni uomo dall’altro, anche la pretesa di una
superiorità come principio uniformante viene meno.

La rivolta per Stirner non può essere oggettivata e deve essere incessante. Per
questo l’unione deve configurarsi come un *unirsi incessante* (anche perché se
si consolidasse darebbe vita a un nuovo ordine con nuove leggi, nuove
istituzioni). Ciò comporta che l’associazione abbia bisogno della società
esistente perché sono proprio le sue leggi che offrono l’opportunità, o meglio,
spingono gli individui ad opporsi e ribellarsi ad esse: “La rivolta come unione
degli unici presuppone l’esistenza di una società già costituita che non si
vuole cambiare riformandola, o rivoluzionare rovesciandola, ma semplicemente
nullificare evadendola.”98 In conclusione, quindi, se da un lato la rivolta
intende nullificare l’ordine esistente, riesce allo stesso tempo a renderlo
eterno.


2\. Le Controculture
====================

Una controcultura può essere definita come “l’insieme delle manifestazioni
culturali di opposizione ideologica (dette anche cultura alternativa),
inizialmente proprie di gruppi emarginati, generalmente giovanili (beats,
hippies, movimenti di contestazione ecc.), sviluppatesi a partire dagli anni
Sessanta del Novecento con marcato carattere anticonformistico e con dichiarata
avversione non solo alle manifestazioni della cultura ufficiale ma anche ad
altri aspetti della vita e del costume della società (tra cui il consumismo e il
progresso tecnologico.)”99

La bibliografia inerente le controculture degli anni ‘60 e ’70 contiene elenchi
infiniti di movimenti che, anche se operavano con obiettivi, metodologie e
approcci completamente diversi tra loro, possono essere raggruppati sotto la
voce “controcultura” perché tutti mossi da una coesione di fondo. Per la prima
volta si parla di movimento generazionale, in cui i giovani si presentano come
gruppo che critica fortemente la società e ne chiede il cambiamento dei valori
fondanti, focalizzandosi in particolare su elementi come la libertà e
l’auto-espressione.

Per quanto riguarda questo lavoro, è utile aprire una parentesi sull’argomento
perché permette di far luce e di spiegare con più chiarezza la relazione tra la
filosofia di Stirner e la filosofia hacker, che è a tutti gli effetti una
controcultura. Ma è bene tenere sempre a mente, come hanno spiegato Ken Goffman
e Dan Joy100, che “la controcultura è un fenomeno perenne, probabilmente antico
quanto la civiltà umana e verosimilmente quanto la cultura stessa.”101

Nonostante l’attenzione si concentri qui soprattutto sulle controculture degli
anni ’60 e ’70, perché di fatto rappresentano un precedente significativo della
controcultura hacker, Goffman e Joy ricordano che già Prometeo rappresenta un
ribelle della controcultura mitica: nell’opera di Eschilo, il *Prometeo
incatenato*, si racconta di questa divinità greca che dà inizio ai sacrifici
animali. Un giorno, dopo aver diviso un toro in due parti, da una parte le
interiora e la carne, dall’altra ossa e grasso, sfida Zeus a scegliere quella
giusta, mentre il resto verrà destinato all’uomo. La scelta di Zeus però ricade
sulla parte di ossa e grasso, il che lo rende furioso sia nei confronti di
Prometeo, perché ritiene di essere stato ingannato dalla sua capacità di
previsione, sia degli umani, e decide così di punire questi ultimi privandoli
del fuoco. Prometeo allora lo ruba e, non contento, continua a sfidare Zeus che
lo punisce incatenandolo ad una roccia, dove ogni giorno sarà dilaniato da
un’aquila che gli mangerà il fegato. Ogni notte il fegato si rigenera, pronto
per essere divorato nuovamente. In questo mito il fuoco rappresenta una metafora
della conoscenza (o anche tecnologia) e secondo Goffman questo racconto serviva
come monito per i Greci antichi, che non svilupparono pienamente le loro scienze
tecniche per paura della superbia. 

Nella mitologia ebraico-cristiana, una figura molto simile a Prometeo è la
figura di Lucifero (tradotta letteralmente in *“colui che porta la luce”*,
ovvero la conoscenza), che ha stimolato l’uomo ad una conoscenza elevatrice, in
netta contrapposizione con la volontà di Dio: “È evidente come “la conoscenza”,
metaforizzata dalla luce, sia l’emblema della controculturalità, conoscenza che
si configura come strada verso la libertà individuale.”102

In ogni caso, la conoscenza intesa come tecnologia sembra non avere sempre la
stessa accoglienza positiva: “[…] Studiosi illuminati come Roger Shattuck ed
esponenti della controcultura come Ted Roszak intonano a gran voce <<Vade retro,
Prometeo!>>”103 e questo perché la tecnologia ha avuto come conseguenze anche
aspetti poco piacevoli come ad esempio la divisione dell’atomo, la bomba ad
idrogeno, il surriscaldamento globale.

Questo diverso tipo di approccio nei confronti della tecnologia ha portato ad
una distinzione tra controculture antiprometeiche e proprometeiche: nel primo
caso ci riferiamo ad esempio agli hippy che auspicano il ritorno alla terra,
seguaci di religioni orientali o New Age, ambientalisti. Nel secondo caso, la
controcultura prometeica è composta soprattutto da hacker e sperimentatori
tecnologici, tecnici di imprenditoria digitale che credono fortemente che la
tecnologia possa cambiare il mondo.

Tornando alla controcultura degli anni 60’ e ‘70, che nacque in California ma
che si diffuse presto in molte altre parti del mondo, si sviluppò in due diverse
direzioni: da un lato quella politicamente impegnata, che nasceva nelle
università, con il dichiarato obiettivo di cambiare le istituzioni e che
organizzò forme di protesta radicali come la marcia contro la guerra in Vietnam
o le manifestazioni per i diritti civili. Dall’altro quella che invece ricercava
nuove forme di aggregazione sociale attraverso l’evasione dalla realtà sia
fisica che mentale e di cui sono esponenti significativi gli *Hippie* e la *New
Age*. Spesso in quegli anni i due movimenti erano così diversi fra loro da
essere percepiti come separati. Theodore Roszak, nel suo *Nascita di una
controcultura*104, supera questa separazione considerando i movimenti come due
facce della stessa medaglia: si tratta semplicemente di due approcci diversi al
cambiamento. Da un lato abbiamo un approccio tradizionale, che programma
l’azione, che richiede disciplina e organizzazione, di impronta marxista;
dall’altro un approccio basato sul presente, che non vuole un cambiamento nel
futuro, ma lo vuole qui e ora.

Come osservato da Dick Hebdige in *Subculture: the Meaning of Style*105, per
quanto vi siano diverse direzioni e vari sviluppi, tutte le ricerche e gli studi
sul movimento controculturale tendono a riconoscere come illustre precursore del
movimento la *Beat Generation* degli anni ’50. È proprio dalla sensazione di
estraneità e disillusione che caratterizzò questo decennio, insieme alla guerra
del Vietnam e a molti omicidi politici come quello di Kennedy, che il sentimento
di estraneità si amplificò e condusse alle critiche dei vari aspetti della vita
sociale: la tecnologia, l’educazione, il sesso, il maschilismo, l’identità,
perfino il modo di vestire e di portare i capelli.

Nel tentativo di riformare la cultura americana, iniziò una fase di studio di
altre culture che potessero in qualche modo rappresentare e tutelare le nuove
esigenze di quegli individui. In questo senso ebbero molta fortuna le culture
orientali, che in generale sviluppavano una concezione del sé molto diversa
dalla concezione razionale occidentale.

Autori come Arthur Marwick, Norman Ryder e Landon Jones hanno cercato di
spiegare le controculture a partire dal fenomeno del “baby boom”: l’elevatissimo
numero di nascite negli Stati Uniti e in tutto il mondo dopo la seconda guerra
mondiale, che raggiunge l’età post adolescenziale proprio negli anni ’60, non
rappresenterebbe altro che “il normale, fisiologico sconvolgimento e sentimento
di sfida generazionale adolescenziale e tardo-adolescenziale acutizzato e
amplificato dall’enorme numero di giovani. La generazione precedente non fu in
grado, quindi, di assimilare, plasmare o addomesticare questa forza per
integrarla nella società adulta”.106 La spiegazione demografica non è
sufficiente perché, ad esempio, in luoghi come l’Australia, protagonista di uno
dei più grandi baby boom, non si sono sviluppati movimenti controculturali,
mentre altri paesi come la Francia sono stati la culla di movimenti radicali pur
non essendosi verificato l’aumento demografico. Inoltre non si tiene conto che
la contestazione non coinvolse tutti i nati in un determinato periodo storico,
ma solo gli appartenenti ad una certa classe. Tutti gli studi convergono
sull’idea che si è trattato di una ribellione di classe, la classe media (e
comunque non tutti gli appartenenti a questa classe ne fecero parte). Si può
parlare nello specifico di *white middle-class*, senza dimenticare che la
componente di colore ebbe un enorme peso, con la differenza che nel primo caso
l’obiettivo era uscire dalla società dominante, nel secondo si rivendicavano i
diritti per poter accedere a quella società.

Altro aspetto che tutta la bibliografia mette in relazione con il movimento
controculturale è la nascita della televisione, perché permette di analizzare il
complesso rapporto tra la controcultura e la tecnologia, vista come un mezzo
utilizzato dalla società per controllare la natura e la società stessa.

Molti studiosi riconoscono un legame tra televisione e controcultura, anche se
in forme e modi diversi. Sta di fatto che all’inizio degli anni ’60 il 92% degli
americani possedeva una televisione, e la grande industria aveva individuato nei
giovani un importante e proficuo target di riferimento. I valori promossi da
questo medium si riferivano all’armonia e all’unione familiare, tuttavia come
dimostrato da Meyrowitz107, hanno avuto un effetto destabilizzante mutandone la
“geografia situazionale”. “I giovani erano introdotti nel mondo adulto
attraverso il nuovo medium e ne scoprivano le contraddizioni, le ingiustizie,
elementi che ne minavano l’autorità, minandone la legittimità”.108 Secondo
Meyrowitz ciò ha rappresentato una grande influenza per la nascita della
controcultura, perché questi giovani, arrivati all’università, si trovarono di
fronte una realtà completamente diversa da quella che aveva mostrato e promesso
la televisione. Allo stesso tempo la televisione sembra avere il merito di aver
creato una nuova comunità generazionale con un senso di appartenenza e una
coscienza condivisa mediata proprio dalla televisione. 

Un’altra personalità di spicco per il movimento della controcultura è Marshall
McLuhan109, il quale ha sostenuto che all’introduzione di una nuova tecnologia
corrisponde sempre un’alterazione degli equilibri sensoriali e che questa
alterazione modifica la percezione del mondo e ne favorisce il cambiamento. Con
i media elettronici, in particolare la televisione, si abbandona la precedente
“cultura tipografica” e il modo di ragionare analitico, sequenziale e astratto,
a favore di una cultura prevalentemente orale che favorisce gli aspetti acustici
e tattili, consequenziali e istantanei.110

Come McLuhan, anche Samuel Ichiye Hayakawa cercò di spiegare la relazione tra
televisione e controculture (le sue tesi furono pubblicate in due numeri di
*“The Guide”* nel 1970). Mentre per il primo l’esperienza televisiva era attiva
e costruttiva, per Hayakawa si trattava di una regressione che causò la
ribellione giovanile. La distanza abissale tra la realtà televisiva e quella
quotidiana, ma soprattutto l’idea che fosse sempre possibile trovare una
soluzione facile e immediata a tutti i problemi, come spesso suggeriva la
pubblicità, non trovando un riscontro causò nei giovani un rifiuto dei valori
con i quali erano cresciuti. Anche per questo motivo, la relazione tra
controcultura e televisione era destinata ad esaurirsi: la televisione
continuava a rappresentare la società e i valori da cui i giovani  volevano
allontanarsi, in più era sempre più corrotta dall’industria commerciale.

Nonostante il rifiuto della tecnologia, già negli anni ’60 la controcultura era
ad essa legata più di quanto si possa immaginare. Basti pensare che l’LSD, la
droga che più di tutto riusciva a “mettere in relazione l’individuo e il Tutto,
con gli altri e la natura in un’armonia mistica e spirituale”111, altro non era
che una tecnologia per creare un mondo parallelo, così come in un certo senso la
televisione: “Anche la Natura da loro così tanto desiderata e ricercata, portava
dentro di sé i germi di quella società tecnologica che tentavano di superare e
rinnegare, mostrando l’ennesima contraddizione di un movimento che, come abbiamo
visto, ne presentava più di qualcuna.”112

Come già detto, all’interno della controcultura convivevano due correnti: da un
lato quella più “politicizzata” i cui gruppi rientrarono in quella che fu
definita *New Left*, dall’altro quella che presentava una nuova forma di
antagonismo, che voleva vivere in un mondo diverso senza necessariamente
riformare o abbattere quello precedente. Si tratta degli *hippie*, che cercavano
di costituire questa nuova realtà in modi diversi, dall’assunzione di droghe
psichedeliche alla creazione delle comuni. Proprio questa corrente resisterà al
tempo e influenzerà tutte le controculture che la seguiranno, in particolare la
*cyberculture*. Secondo Timothy Leary113, scrittore e psicologo americano,
nonché personaggio fondamentale della controcultura degli anni ’60, la
rivoluzione psichedelica è legata e rappresenta un fondamentale precedente della
rivoluzione informatica. 

Gli elementi che permisero la fusione di controcultura e cybercultura sono
relativi soprattutto all’evoluzione tecnologica del computer: negli anni ’80,
con l’invenzione del primo personal computer e la sua successiva
commercializzazione, inizia una fase di interpretazione di tipo controculturale
dell’oggetto, una nuova tecnologia che sarà in grado di cambiare prima le
coscienze e successivamente il mondo. 

La “cultura underground” è un altro termine nato per indicare quell’insieme di
movimenti giovanili di tendenza alternativa, accomunata dall’intento di porsi in
antitesi o in alternativa alla cultura di massa popolare. Sorta all’interno di
società del capitalismo avanzato, in un’epoca di grandi trasformazioni
dell’industria culturale dovuta allo sviluppo dei mezzi di comunicazione di
massa, la cultura underground proponeva un utilizzo alternativo dei nuovi mezzi
di comunicazione, basato sulla diffusione di stili e principi di vita differenti
da quelli della cultura dominante. Di tutti questi movimenti, una grande
rilevanza fu assunta dal punk: sorto a metà degli anni ’70 e, con il tempo,
declinato in un’infinità di classificazioni, fu caratterizzato da un’energica
rabbia e aggressività nei confronti di qualsiasi forma di controllo, soprattutto
il controllo sociale esercitato dai mass media e dalle organizzazioni religiose.
Proprio per questi tratti in comune la controcultura telematica ha scelto di
unire il termine “cyber” a quello “punk”, per indicare il mantenimento dello
stesso spirito ribelle trasferito nel cyberspazio.114

Stewart Brand, creatore di *The Whole Earth Catalog*, il cui scopo era di
permettere al lettore di trovare praticamente ogni informazione utile, a partire
dall'idea che gli umani avrebbero quindi sviluppato una tecnologia ed una
cultura nuova, positiva e sostenibile, in un articolo per Rolling Stone
dichiara: 

> Le droghe psichedeliche, le comuni e le cupole geodesiche erano dei vicoli
> ciechi, ma i computer rappresentavano la strada di accesso a mondi che
> andavano al di là dei nostri sogni. Hippie e rivoluzionari fallirono nel loro
> intento... tutti fallirono a parte loro, e a quei tempi noi non sapevamo
> nemmeno che esistessero! Non finivano in televisione come Abbie [Hoffman] e
> non strombazzavano in giro quello che facevano; si limitavano a inventare il
> futuro, e lo facevano con un sorprendente senso di responsabilità, che
> incorporarono nella loro tecnologia, proprio all’interno dei chip – una
> completa fusione di alta tecnologia e cultura pop molto terra terra.115 

Grazie alla nascita del pc, l’avvento di internet e del web, nasce un nuovo
territorio in cui rivivere gli ideali controculturali, luogo di libera
espressione e liberazione personale.

A partire dagli anni Settanta, come osservato da Goffman e Joy, “mentre alcuni
hippie fuggirono dalla civiltà rifugiandosi nei boschi, una piccola cerchia di
innovatori, inventori e ingegneri visionari, influenzati dalla fantascienza, si
concentrarono sui potenziali insiti negli apparecchi dell’informazione e della
comunicazione, in particolare telefoni e computer.”116 Nacquero così i primi
*“phone phreaks”*, “pirati del telefono”, persone ossessionate dall’idea di
ricavare cose gratis dalle grandi aziende, come riuscire a fare telefonate
internazionali senza pagare. Uno di questi individui, tale John Draper, poi noto
come Captain Crunch, scoprì che “soffiando in un fischietto distribuito in
omaggio con una famosa scatola di cereali (*“Cap’n Crunch”*) vicino alla
cornetta, si otteneva come risultato di resettare la centralina telefonica della
*Ma Bell*; inventò così un circuito, il “blue box”, in grado di riprodurre la
stessa frequenza del fischietto (2600 Herz), grazie al quale era possibile
effettuare chiamate senza che venissero addebitate.”117 Da questo momento, tutti
gli appassionati del genere iniziarono a condividere le tecniche più curiose e
innovative su un bollettino periodico chiamato *“TAP”*, di cui si parlerà più
avanti.

Nel 1977, Stephen Wozniak e Steve Jobs, due individui che rientravano nella
categoria appena descritta, diedero vita al primo personal computer realizzabile
commercialmente, *Apple II*, con l’idea di aver creato un grande strumento di
eguaglianza sociale, poiché offriva agli individui un mezzo che permetteva alla
loro forza creativa di competere con le potenze che detenevano gli altri canali
di comunicazione. 

La maggior parte delle persone appena citate, da Wozniak a Jobs, da Captain
Crunch ad Abbie Hoffman, ebbero un ruolo più o meno importante nella
controcultura hacker. 

2\.1 Gli Hacker
---------------

Prima di iniziare ad analizzare l’hacking, che a tutti gli effetti può essere
considerato una controcultura, è bene fare chiarezza su una serie di
definizioni, significati e distinzioni necessarie per procedere concentrandosi
su alcuni aspetti, tralasciandone altri.

Una delle definizioni più chiare del termine *hacking* è fornita da
Wikipedia118: “È uno dei più inflazionati vocaboli legati al mondo
dell’informatica. Essendo strettamente legato allo sviluppo delle tecnologie di
elaborazione e comunicazione dell’informazione, ha assunto diverse sfumature a
seconda del periodo storico e dello specifico ambito di applicazione.” In ambito
tecnico, si può definire come “lo studio dei sistemi informatici al fine di
potenziarne capacità e funzioni.” Anche se spesso associato all’ambito
informatico, il termine può essere utilizzato genericamente per indicare ogni
situazione in cui si utilizzi creatività e immaginazione nella ricerca della
conoscenza; si tratta di uno strumento, e come tale può assumere una valenza
positiva o negativa a seconda di chi lo usa e per quale fine.

Generalmente, chi si impegna nell’hacking viene chiamato hacker: si tratta di
“persone a cui piacciono davvero le macchine, sono persone curiose di sapere
come funzionano e nulla può mitigare questa curiosità. […] Si divertono a
studiare le macchine, smontarle e rimontarle. Le migliorano incessantemente. Nel
caso delle macchine digitali, scrivono codici per farle funzionare in maniera
automatica, per connetterle fra loro; letteralmente le nutrono e danno loro
vita. Queste persone sono gli hacker.”119

Nell’immaginario collettivo, l’hacker è spesso dipinto come un ragazzino
solitario e geniale, con grossi problemi di socializzazione, più a suo agio
davanti uno schermo che davanti alle persone in carne e ossa. Si tratta di
*nerds*, ragazzi che non brillano nello sport, sono timidi con le ragazze, ma
che sono in possesso si altre capacità, spesso non comprese. Conoscono, sono
affascinati e incuriositi dalle macchine: potenzialmente sono tutti *crackers*,
ossia persone in grado di appropriarsi e di distruggere i dati e le vite altrui
per puro divertimento, per soldi, per vendetta.

Per cercare di superare questo stereotipo, ampiamente diffuso in ambito
cinematografico e spesso anche giornalistico, può essere utile risalire alle
origini del fenomeno.

Nel libro *Hackers. Gli eroi della rivoluzione informatica*120 Steven Levy
rintraccia l’origine della storia degli hacker, a partire dalla fine degli anni
’50, all’interno del Massachuttes Institute of Techonolgy, prestigiosa
università di ricerca americana. In quegli anni il termine hack era utilizzato
per indicare scherzi goliardici e spettacolari che gli studenti più ingegnosi
mettevano in atto durante l’anno accademico. Il campus era ricco di tunnel
sotterranei vietati che venivano sistematicamente esplorati dagli studenti più
avventurosi e incuranti dei divieti, che diedero vita al “tunnel hacking”.
Infine, il sistema telefonico dello stesso campus offriva la possibilità di
nuovi scherzi a costo zero, attraverso la già citata pratica del “phone
phreaking”. Divertimento creativo ed esplorazione già costituivano le basi per
le future accezioni del termine.

Sempre secondo Levy, un nuovo significato del termine nacque sempre all’interno
del MIT, e precisamente nella *Tech Model Railroad Club*, un club di modellismo
ferroviario in cui esisteva una divisione (chiamata *Signals and Power*) con il
compito di fornire energia elettrica per i trenini; in questo gruppo il termine
“hack” descriveva un progetto che aveva uno scopo preciso, ma anche che “portava
divertimento solamente con la partecipazione.”121 Coloro che si dedicavano con
maggior passione alle attività del gruppo, con un’attenzione morbosa verso la
tecnologia che consentiva di risolvere i problemi in maniera ingegnosa e
creativa, venivano definiti hacker.

Nel 1959 venne inaugurato il primo corso di programmazione per computer del MIT
e gli studenti del gruppo *Signals and Power*, che parteciparono entusiasti,
ebbero i loro primi approcci con quello che possiamo definire come l’antenato
del moderno computer. A partire dallo stesso spirito di gioco, creatività e
scoperta, il verbo “to hack” passò dall’indicare le saldature sui circuiti dei
modellini, a comporre insieme vari programmi, migliorare il software e renderlo
più veloce; “significava penetrare nelle viscere della macchina per carpirne i
segreti. Rimanendo fedele alla sua radice, il termine indicava anche la
realizzazione di programmi aventi l’unico scopo di divertire e intrattenere
l’utente”.122

Esempio evidente di tale attività è la realizzazione di Spacewar, il primo
videogioco interattivo, sviluppato dagli hacker del MIT con il solo scopo di
intrattenere e divertire, che rappresenta una importante novità dal punto di
vista del software perché completamente libero e gratuito.

Lo spirito collaborativo veniva ulteriormente incentivato da un particolare
sistema operativo utilizzato sugli elaboratori: “ L’ITS (Incompatible
Time-Sharing System) che fungeva da biblioteca collettiva dei programmi, a cui
ogni hacker del Laboratorio di Intelligenza Artificiale poteva accedere. Questo
sistema di scambio cooperativo permise sia la crescita delle abilità degli
hacker, sia un avanzamento rapido nei risultati della ricerca sui
calcolatori.”123

Alla fine degli anni ’50, il MIT era già uno dei centri di ricerca più rinomati
nel campo delle nuove tecnologie. Il Pentagono decise di finanziarla,
inserendola nel suo programma *ARPA* (Advanced Research Projects Agency). Con
questa iniziativa il Ministero della Difesa americano offriva soldi alle
università affinché queste sviluppassero i computer. Era lasciata loro molta
libertà di ricerca, ma era chiaro che il Pentagono avesse come fine ultimo
l’applicazione dell’informatica nelle strategie militari.124 Nel 1969, i
computer delle università inserite nel progetto vennero collegati in una rete di
comunicazione chiamata *ARPAnet*, cellula originaria di Internet.

Il gruppo che Steven Levy definisce “la prima generazione di hacker”, nato
all’interno del MIT, fu affidato ai professori John McCarthy, Jack Dennis e
Marvin Minsky, uno dei padri dell’intelligenza artificiale, che per primo capì
l’importanza di attirare le menti più brillanti e lasciarle lavorare con la
maggiore libertà possibile. Al gruppo di studenti di questi anni, nel 1971 si
aggiunse Richard Stallman. Non si trattava di uno studente del MIT, era una
matricola di Harvard che un giorno per curiosità decise di visitare il
laboratorio di intelligenza artificiale di quella università che già
esteticamente si presentava molto diversa da quella che stava frequentando: “Con
il labirinto di corridoi che univa una serie di edifici interconnessi, il campus
del MIT presentava un’estetica minimalista rispetto allo spazioso villaggio
coloniale di Harvard. Analoga l’impressione riguardo agli iscritti del MIT, una
variegata raccolta di studenti un po’ svitati, noti più per la predilezione alle
birichinate che per la brillante carriera politica.”125

Questa differenza così netta con il suo luogo di provenienza si rifletteva
perfettamente anche nel laboratorio di intelligenza artificiale: qui sembrava
mancare una gerarchia e lo stretto controllo tipici di Harvard, nessun
“laureando/custode”, nessuna lista di attesa per l’accesso ai terminali. Solo
una serie di terminali aperti e braccia robot, probabilmente usati per qualche
esperimento precedente. Stallman iniziò a frequentare stabilmente il
laboratorio, e presto fu messo al corrente anche delle tradizioni morali
condensate nell’ “etica hacker”: 

> essere un hacker significava qualcosa di più che sviluppare semplicemente dei
> programmi. Voleva dire scrivere il miglior codice possibile e stare seduti
> davanti a un terminale anche per 36 ore consecutive, se per riuscirci
> occorreva tutto quel tempo. Fatto ancor più importante, significava aver
> continuamente accesso alle migliori macchine esistenti e alle informazioni più
> utili. Gli hacker dicevano apertamente di voler cambiare il mondo tramite il
> software, e Stallman imparò che l’hacker istintivo supera ogni ostacolo pur di
> raggiungere un tale nobile obiettivo. Tra questi ostacoli, i maggiori erano
> rappresentati dal software scadente, dalla burocrazia accademica e dai
> comportamenti egoistici.126

Diversamente da quanto succedeva ad Harvard, i computer dei laboratori del MIT
non erano considerati una proprietà privata: proprio a partire dal  momento in
cui Stallman iniziò a frequentare i laboratori, si era creato un tacito accordo
tra amministratori, professori e studenti. A questi ultimi era garantito pieno e
libero accesso alle macchine, in cambio di riparazioni e miglioramenti del
software. (C’è da dire che anche prima di questo accordo, quando succedeva che
un terminale venisse chiuso a chiave la notte in qualche studio, qualcuno già
eccelleva nella pratica del “lock hacking”, “l’arte di irrompere negli uffici
dei professori per “liberare” i computer sequestrati”127).

Goffamn e Joy128 sostengono che questa prima generazione sviluppò una filosofia
influenzata sia dallo spirito antiautoritario tipico della controcultura, sia da
altre caratteristiche che erano considerate tipiche della natura stessa della
comunicazione e del computer. È Steven Leavy a enunciare le linee guida di
questa filosofia: 

> L’accesso ai computer e a qualunque cosa che ti possa insegnare il
> funzionamento del mondo deve essere illimitato e totale. Tutte le informazioni
> devono essere libere. Non fidarti mai dell’autorità, e cerca di promuovere
> sempre la decentralizzazione. Gli hackers devono essere giudicati dalla loro
> abilità nel lavoro, e non da falsi criteri come i risultati scolastici, l’età,
> la razza o la posizione. I computer possono cambiare la vita in meglio.129

Chiaramente l’idea di accesso totale e libero ai computer fa riferimento a un
momento storico precedente allo sviluppo del pc, in cui le macchine erano ancora
soltanto a disposizione delle università o delle strutture militari e, in linea
di principio, significava permettere a chiunque volesse di “metterci le mani
sopra”. Ma c’è anche un altro aspetto non meno importante: accesso totale
significava *open source*130. Bisognava garantire a chiunque la possibilità di
introdursi in qualsiasi parte della macchina, compreso il codice sorgente,
perché questo poteva permettere un miglioramento del sistema operativo. Il
software doveva essere concepito come un bene da condividere liberamente. Per
una breve definizione di open source, si può fare riferimento al Dizionario
Informatico on line: “Con Open Source si indicano i programmi il cui codice
sorgente è pubblico e disponibile per l’uso e la modifica: in altre parole
libero, a differenza dei programmi gratuiti che vengono distribuiti senza costi
ma senza poter apportare modifiche. Il codice Open Source viene tipicamente
creato tramite collaborazione di programmatori, i quali apportano man mano
miglioramenti al codice sorgente originario e diffondono il risultato a
beneficio della comunità.”131

La scena iniziò a mutare negli anni ’70, quando una nuova generazione di hacker
iniziò a muoversi in California. La prima generazione, rinchiusa nei laboratori
del MIT, non fu coinvolta nelle contestazioni di fine anni ’60, mentre in
California il clima culturale della contestazione aveva dato vita a un nuovo
movimento digitale che si impegnava a rendere accessibile a tutti il computer.
Secondo gli autori di *Etica hacker: l’imperativo è hands on*132, questa nuova
generazione si sviluppa nella Bay Area di San Francisco, ed era caratterizzata 

> dall’interesse a diffondere l’uso del computer anche al di fuori del ristretto
> ambito dei ricercatori, nella convinzione che questo avrebbe potuto portare un
> miglioramento qualitativo della vita delle persone. Il primo progetto di
> diffusione e uso sociale dell’informatica si realizzò nel 1969, anno di
> fondazione della *Community Memory* di Berkeley. Quest’organizzazione, formata
> da volontari patiti dell’informatica, aveva lo scopo di compilare una banca
> dati metropolitana e un annuario dei servizi per la popolazione; in questo
> modo, attraverso terminali posti in luoghi come lavanderie, biblioteche,
> negozi, etc, era possibile un piccolo scambio di informazioni e di opinioni
> tra gli abitanti della comunità.133

Nacque l’*Homebrew Computer Club*, gruppo costituito da ingegneri, ricercatori,
dilettanti e amatori che trafficavano con l’elettronica nei loro garage, ma che
avevano anche il supporto di personaggi come Ed Roberts, che in New Mexico aveva
fondato la Model Instrumentation Telemetry System, “una delle prime compagnie
interessate allo sviluppo dell’informatica per tutti.”134 Due giovanissimi
membri di questo gruppo erano Steve Wozniak e Steve Jobs, futuri fondatori di
Apple, che iniziarono la loro attività ispirandosi al già citato Captain Crunch:
impararono a costruire *blue boxes* e iniziarono a venderle agli studenti
dell’università di Berkeley, che in questo modo potevano tenersi in contatto con
parenti ed amici, telefonando gratis; “ i soldi raccolti con le blue boxes
diventarono il primo capitale su cui Wozniak e Jobs cominciarono a fondare la
legittima e riverita Apple.”135 Per loro, il principio del libero accesso ai
computer significava consegnare il computer nelle mani degli utenti privati.

Il sogno del personal computer cominciava a prendere vita, ma contemporaneamente
lo sviluppo di software poneva un singolare dilemma di mercato: ”Una volta che
una persona fosse entrata in possesso del software, teoricamente avrebbe potuto
distribuire delle copie a ogni essere umano sul pianeta, pur mantenendo
l’originale per sé. Soprattutto per coloro che avevano la larghezza di banda
sufficiente per trasferire il software su internet, non si presentava
assolutamente alcun problema di proprietà fisica o scambio di merci.”136 Nella
comunità hacker esisteva un tacito accordo, secondo cui chi sviluppava un
programma particolarmente utile poteva chiedere agli altri un piccolo e
volontario contributo economico, in modo da sostenere il lavoro del
programmatore.

 Pochi pensarono di ricavarne un’attività redditizia, e tra i pochi c’era uno
 studente di Harvard di nome Bill Gates. Appassionato di computer e
 autodefinitosi hacker, secondo la prospettiva fiorita al MIT, lasciò
 l’università e si offrì, insieme all’amico Paul Allen, di scrivere per la Model
 Instrumentation Telemetry System di Ed Roberts un nuovo linguaggio BASIC per il
 computer Altair137, necessario a tutti gli amatori che volevano potenziare le
 loro macchine. Non era un’impresa facile, ma i due ci riuscirono, e Gates non
 solo avviò così la sua carriera imprenditoriale, ma iniziò a provocare anche un
 mutamento nella mentalità degli hackers. Il problema nacque qualche mese dopo
 lo sviluppo del programma, durante la sua presentazione. Il programma fu
 infatti rubato e poco dopo un membro dell’ *Homebrew Computer Club*, tale Dan
 Skol, copiò il linguaggio e lo diffuse gratuitamente, così come dettava l’etica
 originaria degli hackers.

Ciò portò Gates a scrivere una lettera aperta, indirizzata principalmente ai
membri del gruppo di Skol, e pubblicata nell’ottobre del 1976 sulla rivista
*Computer Notes*, in cui accusava gli hacker di danneggiare non la sua società,
ma la sua persona. Nella lettera chiede: “Perché succede questo? La maggior
parte degli hobbisti deve essere consapevole che molti di voi rubano il
software. L'hardware va pagato, ma il software sembra sia qualcosa da
condividere. A chi gliene importa, poi, se le persone che ci hanno lavorato
sopra non vengono retribuite?”. Questo è il punto di partenza di una guerra che
si protrarrà per molti anni e che coinvolgerà l’azienda di Bill Gates in una
causa antitrust con il governo degli Stati Uniti, ma che chiarisce anche, ancora
una volta, il punto di vista degli hacker: tutto quello che può servire a
migliorare i computer deve essere libero. Un punto chiave nella causa antitrust
contro la Microsoft è proprio l’ostinazione del suo fondatore a non rivelare i
codici dei suoi programmi, in modo da evitare che la concorrenza possa produrre
accessori per Windows. Ed è il punto chiave anche della rottura con la filosofia
hacker che ha visto la luce nei laboratori del MIT, dove chiunque poteva sedersi
a computer e migliorare un programma sempre aperto.

In questo stesso periodo, la scena di New York era dominata da Habbie Hoffman,
considerato uno dei maggiori esponenti della contestazione americana e fondatore
dello *Youth International Party* (da cui venne il nome *yippies*). Non si
tratta di un hacker come quelli presi in considerazione finora, ma secondo la
sua dottrina politica era impossibile realizzare una rivoluzione senza poter
disporre di un sistema di comunicazione gratuito. Inoltre, come sostiene
Mastrolilli, “durante la guerra del Vietnam, il governo americano aveva imposto
una tassa speciale sulle telefonate, proprio per finanziare il conflitto. Quindi
per i contestatori chiamare gratis era diventato quasi un dovere morale, oltre
che una necessità pratica e politica.”138 Quindi, nel 1971, Hoffman si alleò con
un esperto di elettronica che si faceva chiamare Al Bell, e insieme fondarono la
rivista “Youth International Party Line”, su cui venivano sistematicamente
pubblicati i metodi per telefonare senza pagare. Dopo il 1975 il movimento della
contestazione cominciò a perdere forza, mentre il movimento tecnologico
continuava ad avanzare. Per questo motivo Bell decise di tenere in vita la
rivista, cambiandone il nome in “Technical Assistance Program”, o più
semplicemente, *TAP*.

Nel 1977 San Francisco ospitò la First Annual West Coast Computer Fair, la prima
fiera dell’informatica popolare. Il personal computer si stava diffondendo,
realizzando così i sogni degli hacker della seconda generazione, ma nessuno
aveva calcolato il rovescio della medaglia.

Già nel 1975, quando Bill Gates scriveva la sua lettera aperta e di protesta, si
poteva intuire un cambiamento: “L’informatica stava uscendo dalle università,
anche se l’informatica di massa era ancora ben lontana; con l’abbattimento dei
costi dell’hardware molti neofiti si avvicinavano alle tecnologie grazie ai PC.
Paradossalmente, da una situazione di scambio e libera circolazione dei saperi
nei laboratori di ricerca, l’epoca d’oro degli hacker del MIT negli anni
Sessanta, si andava verso la chiusura dei codici e la mera
commercializzazione”139.

Questa svolta economica creò le condizioni per la nascita di quella che per Levy
può essere considerata la “terza generazione” di hackers, nuove leve di
programmatori che erano cresciute in solitudine, perdendo il senso di comunità
che caratterizzava i loro predecessori, che “ non sentivano la necessità della
condivisione delle tecniche e della libertà di circolazione delle informazioni.
Le leggi del mercato prevalsero e le aziende di software proprietario
s’imposero.”140

Nei primi anni ’80 si verificarono anche importanti novità tecnologiche, come
l’espansione del vecchio ARPAnet, “nello stesso tempo, i geni dell’hardware
svilupparono le conoscenze necessarie per consentire ai personal computer di
fare chiamate telefoniche, e quindi collegarsi al nuovo Internet.”141 Il
computer, soprattutto quando era collegato ad internet, permetteva di
condividere informazioni liberamente, istantaneamente, a qualsiasi distanza;
attraverso esso si realizzava una nuova forma comunicativa definita “da molti a
molti”. Prima di questa svolta, emittenti ed editori potevano trasmettere le
informazioni da una fonte  a molti individui con un meccanismo autolimitante,
con internet invece, qualsiasi informazione era resa disponibile a chiunque
attraverso il solo collegamento. Questo comporta una serie infinite di
implicazioni, soprattutto da un punto di vista politico, che vedremo più avanti.
Per adesso quello che serve a concludere il discorso è l’ultimo sviluppo del
termine hacker.

A partire dalla fine degli anni ’80 la figura dell’hacker passò dall’essere
percepita come quella di un rivoluzionario attore controculturale che poteva
scoprire qualsiasi informazione e rivelarla al mondo intero, ad un mero
scassinatore elettronico che invadeva la privacy dei cittadini. In quegli anni
anche la cronaca offriva moltissimi casi che hanno contribuito a costruire la
nuova percezione negativa. Una effettiva svolta negli attacchi si verificò:
eventi come il blocco dei centralini della AT&T nel 1990 che lasciarono tutta
New York senza telefono per molte ore, o il black out dei sistemi di
comunicazione dei tre principali aeroporti sempre di New York nel 1991, si
configurarono come eventi che non solo irritavano la popolazione, ma potevano
rivelarsi anche enormemente dannosi; “quando polizia e imprenditori iniziarono a
far risalire quei crimini a un pugno di programmatori rinnegati che citavano a
propria difesa frasi di comodo tratte dall'etica hacker, quest'ultimo termine
prese ad apparire su quotidiani e riviste in articoli di taglio negativo.
Nonostante libri come *“Hackers”* avessero fatto parecchio per documentare lo
spirito originale di esplorazione da cui nacque la cultura dell'hacking, per la
maggioranza dei giornalisti “computer hacker” divenne sinonimo di “rapinatore
elettronico”.”142

Nell’analisi dell’evoluzione del concetto, Sam Williams rileva che da oltre due
decenni gli hackers si lamentano di questi presunti abusi, ma “le valenze
ribelli del termine risalenti agli anni '50 rendono difficile distinguere tra un
quindicenne che scrive programmi capaci di infrangere le attuali protezioni
cifrate, dallo studente degli anni '60 che rompe i lucchetti e sfonda le porte
per avere accesso a un terminale chiuso in qualche ufficio. D'altra parte, la
sovversione creativa dell'autorità per qualcuno non è altro che un problema di
sicurezza per qualcun altro.”143 In ogni caso, la maggioranza degli hacker ha
sentito così fortemente la necessità di allontanarsi da comportamenti dannosi,
da coniare il termine “cracker” (chi, volontariamente, infrange un sistema di
sicurezza informatico per rubare o manomettere dati), ma anche la distinzione
tra “black hat” e “white hat”, rispettivamente chi rivolge nuove idee verso
finalità distruttive e chi si dedica a finalità positive, o quanto meno,
informative.

Nel 1983, in un racconto pubblicato nel numero di novembre della rivista
*Amazing Stories*, Bruce Bethke utilizzò per la prima volta il termine
*“cyberpunk”.* Da quel momento, il termine è stato usato per descrivere le opere
di scrittori di fantascienza come William Gibson, specialmente *Neuromante*
(1984), ma anche opere di Bruce Sterling, Pat Cadigan, Lewis Shiner, Greg Bear:
“Il cyberpunk è poi stato ripreso nella teoria sociale e culturale come un’utile
risorsa per comprendere le presunte evoluzioni verso una nuova epoca.”144

Nell’analisi del fenomeno condotta da Antonio Caronia e Domenico Gallo145 il
termine inizialmente suonava come un ossimoro: “Punk cibernetico: la ribellione
disincantata e rabbiosa del punk coniugata all’uso generalizzato delle nuove
tecnologie.”146 Nato per indicare un gruppo di scrittori americani che stavano
trasformando la scena della fantascienza, ben presto il termine si è esteso ad
indicare “la nuova veste assunta negli anni Settanta da settori consistenti del
movimento underground, le pratiche di uso libertario e non convenzionale delle
tecnologie, l’hackeraggio, il phone-phreaking, la rivendicazione attiva di una
radicale libertà nei nuovi territori immateriali del cyberspazio.”147 Anche
secondo Salvatore Proietti circoscrivere il campo di interesse risulta
difficile: “Delimitare il cyberpunk come categoria è operazione problematica;
coloro che ne hanno parlato come “scuola” si sono presto trovati a
diagnosticarne la morte, e i tentativi di proporne un canone hanno prodotto
liste eterogenee e periodizzazioni arbitrarie. D’altra parte, risulta ovvio che
negli ultimi anni le metafore della Science Fiction, e del cyberpunk in
particolare, si sono sviluppate in dialogo con la teoria letteraria, femminista
e scientifica americana. Una specificità del cyberpunk è la sua diffusione
(prima ancora di una vera canonizzazione) in tutti i campi del discorso
sociale.”148

Oltre al già citato William Gibson, le cui opere sono ritenute fondamentali
nella letteratura cyberpunk, un contributo determinante è stato il lavoro di
Bruce Sterling; è lui che, sotto lo pseudonimo Vincent OmniaVeritas, ha creato
la fanzine “Cheap Truth”, rivista amatoriale fotocopiata che è stato il primo
organo della letteratura cyberpunk e da cui vennero lanciati i primi proclami
contro il copyright e in generale “contro il conservatorismo del mondo della
fantascienza ufficiale.”149 È Sterling a tracciare la via “politica” del
cyberpunk, sue sono le riflessioni ideologiche più organiche e approfondite e la
piena consapevolezza dell’intreccio tra motivi letterali e sociali.

In Italia, a partire dal 1986 e fino al 1998, la rivista che rappresenta il
punto di riferimento del cyberpunk è Decoder, nel cui numero n 5 del 1990 si
legge: “Il personaggio del cyberpunk è un mutante iperattrezzato alla
sopravvivenza nel nuovo habitat decisamente superiore al vecchio sapiens sapiens
e si muove alla conquista dei propri obbiettivi contro tutto e tutti, nichilista
e solo, senza verità da dare o da cercare, ma intento solo alla soddisfazione
delle proprie necessità”. In Europa nascono gruppi orientati alla
sperimentazione tecnica, come il *Chaos Computer Club* di Amburgo, ma spesso
sono più marcatamente luoghi di elaborazione politica e culturale, come nel caso
di Decoder, o Hacktic in Olanda, fino alla rete europea *ECN* (*European Counter
Network*). Come osservato nel testo Open non è free, “Questi gruppi cercano
nelle nuove tecnologie degli strumenti di comunicazione per gruppi di affinità
sociale e/o ludica, per esperimenti di partecipazione pubblica e politica alla
vita delle comunità reali; il mondo della rete è percepito come una frontiera di
libertà nella quale rielaborare sconfitte e vittorie della controcultura
underground.” 150

Da fenomeno letterario si passa, come già detto, ad un fenomeno dalle valenze
anche politiche: i membri del movimento accettano criticamente gli strumenti
tecnologici per intraprendere un discorso puramente controculturale, cioè
antagonista, ai media della comunicazione. Si promuove uno scambio di
informazioni libero e illimitato, senza alcun vincolo politico, economico,
tecnico. Per definire meglio gli itinerari politici del cyberpunk hanno avuto
luogo una serie di convention e conferenze in cui si sono confrontati diversi
esponenti cyberpunk. Una delle più importanti è stata la Conferenza
internazionale per l’uso alternativo delle tecnologie (*ICATA*), del 1989 ad
Amsterdam, “In quell’occasione Lee Felsestein, il «papà» dei Personal Computer,
che già negli anni Settanta aveva fondato il Community Memory Project e poco
dopo l’Homebrew Computer Club, portò l’esperienza della Bay Area: il confronto
con i gruppi americani diven- tava sempre più serrato, si elaborava con
entusiasmo l’etica hacker.”151

Alla fine dell’incontro, è stato redatto un manifesto contenente i principi di
base del movimento152: 

> 1. Lo scambio libero e senza alcun ostacolo dell'informazione sia un elemento
>    essenziale delle nostre libertà fondamentali e debba essere sostenuto in
>    ogni circostanza. La tecnologia dell'informazione deve essere a
>    disposizione di tutti e nessuna considerazione di natura politica,
>    economica o tecnica debba impedire l'esercizio di questo diritto.
> 2. Tutta intera la popolazione debba poter controllare, in ogni momento, i
>    poteri del governo; la tecnologia dell'informazione deve allargare e non
>    ridurre l'estensione di questo diritto.
> 3. L'informazione appartiene a tutto il mondo, essa è prodotta per tutto il
>    mondo. Gli informatici, scientifici e tecnici, sono al servizio di tutti
>    noi. Non bisogna permettere loro di restare una casta di tecnocrati
>    privilegiati, senza che questi debbano rendere conto a nessuno del loro
>    operato.
> 4. Il diritto all'informazione si unisce al diritto di scegliere il vettore di
>    questa informazione. Nessun modello unico di informatizzazione deve essere
>    imposto a un individuo, una comunità o a una nazione qualsiasi. In
>    particolare, bisogna resistere alla pressione esercitata dalle tecnologie
>    “avanzate” ma non convenienti. Al loro posto, bisogna sviluppare dei metodi
>    e degli equipaggiamenti che permettano una migliore convivialità, a prezzi
>    e domanda ridotti.
> 5. La nostra preoccupazione più forte è la protezione delle libertà
>    fondamentali; noi quindi domandiamo che nessuna informazione di natura
>    privata sia stockata, né ricercata tramite mezzi elettronici senza accordo
>    esplicito da parte della persona interessata. Il nostro obiettivo è di
>    rendere liberamente accessibile i dati pubblici, proteggere senza
>    incertezze i dati privati. Bisogna sviluppare delle norme in questo senso,
>    insieme agli organismi e alle persone interessati.
> 6. Ogni informazione non consensuale deve essere bandita dal campo
>    dell'informatica. Sia i dati che le reti devono avere libertà d'accesso. La
>    repressione dei pirati deve divenire senza fondamento, alla maniera dei
>    servizi segreti.
Parallelamente domandiamo che tutte le legislazioni, in
>    progetto o già in applicazione, rivolte contro i pirati e che non
>    perseguono scopi criminali o commerciali, siano ritirati immediatamente.
> 7. L'informatica non deve essere utilizzata dai governi e dalle grandi imprese
>    per controllare e opprimere tutto il mondo. Al contrario, essa deve essere
>    utilizzata come puro strumento di emancipazione, di progresso, di
>    formazione e di piacere. Al contempo, l'influenza delle istituzioni
>    militari sull'informatica e la scienza in generale deve cessare. Bisogna
>    che sia riconosciuto il diritto d'avere delle connessioni senza alcuna
>    restrizione con tutte le reti e servizi internazionali di comunicazione di
>    dati, senza interventi e controlli di qualsiasi sorta. Bisogna stabilire
>    dei tetti di spesa, per paese, per avere accesso a questi vettori di
>    comunicazione di dati pubblici e privati. Si deve facilitare quei paesi
>    senza una buona infrastruttura di telecomunicazione e la loro
>    partecipazione nella struttura mondiale. Noi ci indirizziamo agli
>    utilizzatori progressisti di tecnologie di informazione nel mondo affinché
>    socializzino le loro conoscenze e specializzazioni in questo campo con
>    delle organizzazioni di base, al fine di rendere possibile uno scambio
>    internazionale e interdisciplinare di idee e informazioni tramite delle
>    reti internazionali.
> 8. Ogni informazione è al contempo deformazione. Il diritto all'informazione è
>    al contempo inseparabilmente legato al diritto alla deformazione, che
>    appartiene a tutto il mondo. Più si produce informazione, e più si crea un
>    caos di informazione sfociante sempre più in rumore. La distruzione
>    dell'informazione come del resto la sua produzione, è il diritto
>    inalienabile di ognuno.
> 9. Bisognerebbe sovvertire i canali regolamentari e convenzionali
>    dell'informazione grazie a dei detournaments e dei cambiamenti surrealisti
>    degli avvenimenti, al fine di produrre del caos, del rumore, dello spreco i
>    quali, a loro volta, saranno considerati come portatori di informazione.
> 10. La libertà di stampa deve applicarsi anche alle pubblicazioni
>     tecno-anarchiche, che appaiono in giro, per reclamare la liberazione dei
>     popoli, la fine delle tirannie della macchina e del sistema sugli uomini.

2\.2 Il free software
---------------------

Come racconta Carlo Gubitosa nel suo libro *Hacker, scienziati e pionieri*153,
la storia del free software (software libero), ha inizio nel 1984, quando negli
Stati Uniti Richard Stallman, dopo un lungo periodo di riflessione, intraprese
la sua battaglia politica, dedicandosi a scrivere i codici per un sistema
operativo libero, avviando il progetto GNU. È direttamente sul sito
http://www.gnu.org che si possono rintracciare al meglio gli intenti e le
caratteristiche del progetto: 

> GNU è un sistema operativo tipo Unix distribuito come software libero:
> rispetta la vostra libertà. Potete installare versioni di GNU (per la
> precisione, sistemi GNU/Linux) che sono costituite esclusivamente da software
> libero. Il Progetto GNU è nato nel 1984 con l'obiettivo di sviluppare il
> Sistema GNU. Il nome “GNU” è un acronimo ricorsivo per “GNU's Not Unix” (GNU
> Non è Unix) e si pronuncia gh-nu (con la g dura, una sola sillaba senza pause
> tra la g e la n). Un sistema operativo tipo Unix è costituito da un insieme di
> applicazioni, librerie e strumenti di sviluppo, oltre a un programma
> utilizzato per allocare le risorse e comunicare con l'hardware, noto come
> kernel. Hurd, il kernel di GNU, non è ancora pronto per l'utilizzo ordinario,
> e per questo GNU viene tipicamente usato con un kernel di nome Linux. Questa
> combinazione è il sistema operativo GNU/Linux. GNU/Linux è usato da milioni di
> persone, ma molti lo chiamano erroneamente "Linux".154

Il “Manifesto GNU”, pubblicato da Stallman nel 1985, contribuisce alla notorietà
del progetto, spargendo la voce tra gli addetti ai lavori.

Nel 1985, sempre Stallman fondò la *Free Software Foundation* (FSF)
“Un’organizzazione no-profit con la missione di sostenere ed educare in nome
degli utenti di computer di tutto il mondo”155, che mantiene la definizione di
software libero per indicare ciò che deve contenere un particolare programma per
essere considerato tale.

Quindi, un software libero fa riferimento alle libertà degli utenti e della
comunità. Tutti hanno la possibilità di eseguire, copiare, distribuire,
studiare, cambiare e migliorare il software. Soltanto in questo modo gli utenti
possono controllare il programma e non esserne controllati. In questo senso,
quindi, un programma proprietario si configura come uno strumento di abuso.

Il termine “free” in inglese ha un doppio significato, “libero” e “gratis”, per
questo il discorso sul software è spesso accompagnato da una spiegazione “Il
software libero è una questione di libertà, non di prezzo. Per capire il
concetto, bisognerebbe pensare alla 'libertà', come nella libertà di parola, non
di birra gratis".

Sempre secondo la Free Software Foundation, un software è libero se l’utente che
lo utilizza gode di quattro libertà fondamentali: “Libertà di eseguire il
programma, per qualsiasi scopo (libertà 0). Libertà di studiare come funziona il
programma e di modificarlo in modo da adattarlo alle proprie necessità (libertà
1). L'accesso al codice sorgente ne è un prerequisito. Libertà di ridistribuire
copie in modo da aiutare il prossimo (libertà 2). Libertà di migliorare il
programma e distribuirne pubblicamente i miglioramenti da voi apportati (e le
vostre versioni modificate in genere), in modo tale che tutta la comunità ne
tragga beneficio (libertà 3). L'accesso al codice sorgente ne è un
prerequisito.” 

Quello su cui si pone l’attenzione nell’esercizio di queste libertà è lo scopo
dell’utente e non dello sviluppatore: come utente si può eseguire il programma
per i propri scopi, ma quando si distribuisce ognuno è altrettanto libero di
usarlo per i propri fini, che quindi non possono essere imposti da chi lo
sviluppa o distribuisce.

I software del progetto GNU vengono rilasciati sotto la General Public License
(GPL), “licenza scritta da Stallman che rese *de facto* un applicativo libero,
accessibile a tutti, modificabile e distribuibile in qualsiasi modo, purché
accompagnato da codice sorgente.”156

Si crea così una comunità di individui attivi e motivati da precise finalità
politiche: l’obiettivo era la libertà di espressione in campo digitale. Inizia
la crociata contro brevetti e diritto d’autore, con la creazione del cosiddetto
*copyleft*.

Come osservato nell’articolo *Free and Open Source nell’ordinamento italiano*,
”Il diritto d’autore (e il suo corrispondente anglo-americano *copyright*) è lo
strumento giuridico per eccellenza posto a tutela dell’attività creativa, quale
particolare espressione del lavoro intellettuale. L’elemento fondante di questo
specifico istituto è dunque rappresentato dall’individuazione di un atto
creativo originale.”157

Il copyleft (gioco di parole difficilmente traducibile in italiano, che si usa
il termine “left”, “ceduto” all’opposto di “right”, “diritto”, ma anche per
indicare un rovesciamento di significati e di direzioni: “destra” e “sinistra”)
viene spesso tradotto con “permesso d’autore”. Sostanzialmente, si tratta di un
ribaltamento del copyright: “Teoricamente, il livello massimo della libertà con
cui può essere distribuito il software è quello che corrisponde alla
denominazione di «pubblico dominio» (*public domain*), che da molti anni viene
largamente adottata nella comunità degli informatici.”158 Lasciare il software
in un regime di pubblico dominio però comporta che chiunque possa farne ciò che
vuole, “ma pubblico dominio in questo caso non vuol dire 'proprietà di tutti'
intendendo 'tutti' come 'comunità organizzata', bensì intendendo 'tutti' come
'ciascuno'. Di conseguenza, ciascuno potrebbe anche attribuirsi arbitrariamente
i diritti esclusivi di tutela e iniziare a distribuire il software da lui
modificato come se fosse software proprietario, criptando il sorgente e
misconoscendo la provenienza pubblica del software, senza che nessuno possa
agire nei suoi confronti.”159

Questo comportava il rischio di vanificare gli scopi del progetto GNU, gli
sforzi e la filosofia di tutti coloro che si erano dedicati al progetto.
Bisognava trovare il modo di garantire il mantenimento della libertà che si
rivendicava, e fu proprio Richard Stallman ad elaborare la soluzione: “Egli capì
che l'arma più efficace per difendersi dalle maglie troppo strette del copyright
(così come si è evoluto negli ultimi anni) stava nel copyright stesso. Per
rendere dunque un software veramente e costantemente libero era sufficiente
dichiararlo sotto copyright e poi riversare le garanzie di libertà per l'utente
all'interno della licenza, ribaltando così il ruolo della stessa e creando un
vincolo di tipo legale fra la disponibilità del codice e le tre libertà
fondamentali di utilizzo, di modifica e di ridistribuzione”.160 Detto in parole
molto più semplici, come spiegato nell’articolo *Il copyleft spiegato ai
bambini*: ”Io metto il copyright, quindi sono proprietario di quest'opera,
dunque approfitto di questo potere per dire che con quest'opera potete farci
quello che volete, potete copiarla, diffonderla, modificarla, però non potete
impedire a qualcun altro di farlo, cioè non potete *appropriarvene* e fermarne
la circolazione, non potete metterci un copyright a vostra volta, perché ce n'è
già uno, appartiene a me.” 161

Agli inizi degli anni ’90 la comunità GNU era quasi riuscita a realizzare il suo
sistema operativo, “Ma non esiste un sistema operativo se non esiste il kernel.
Il kernel è il motore software per ogni computer ed è ciò che distingue un
sistema operativo da un altro. La FSF tentò per anni e senza alcun successo di
scrivere questo ultimo fondamentale elemento.”162 Le cause di questo fallimento
furono molteplici, fatto sta che mentre Stallman e i suoi lavoravano senza sosta
(e senza riuscire di venirne a capo), nel 1991 uno studente finlandese di nome
Linus Torvalds affrontava il problema da un punto di vista leggermente diverso:
“iniziò la scrittura di un kernel ispirato ai sorgenti di Minix – un sistema
operativo che Andrew S. Tannenbaum aveva concepito a scopo didattico – con una
vera e propria chiamata alla armi sui principali newsgroup di programmazione. In
due anni formò una comunità che con veloci rilasci di nuove versioni, un’ampia
cooperazione e la totale apertura delle informazioni consentì di testare
estensivamente il codice.”163 La diversità nell’approccio di Torvalds consisteva
nel pubblicare i codici sorgenti prima ancora che l’applicativo fosse terminato,
chiedendo un feedback e una collaborazione immediata a paritaria a tutti coloro
che ne avessero le capacità e la voglia.

Un’analisi di questo nuovo approccio la propone Eric S. Raymond, che in suo
saggio del 1997, dal titolo *La cattedrale e il bazar* spiega la differenza e ne
riconosce la forza: 

> Per anni avevo predicato il vangelo Unix degli strumenti agili, dei prototipi
> immediati e della programmazione evolutiva. Ma ero anche convinto che
> esistesse un punto critico di complessità al di sopra del quale si rendesse
> necessario un approccio centralizzato e a priori. Credevo che il software più
> importante andasse realizzato come le cattedrali, attentamente lavorato a mano
> da singoli geni o piccole bande di maghi che lavoravano in splendido
> isolamento, senza che alcuna versione beta vedesse la luce prima del momento
> giusto. Rimasi non poco sorpreso dallo stile di sviluppo proprio di Linus
> Torvalds – diffondere le release presto e spesso, delegare ad altri tutto il
> possibile, essere aperti fino alla promiscuità. Nessuna cattedrale da
> costruire in silenzio e reverenza. Piuttosto, la comunità Linux assomigliava a
> un grande e confusionario bazaar, pullulante di progetti e approcci tra loro
> diversi.164

Grazie a questo ulteriore sviluppo, si chiude definitivamente con l’idea di
“scienziati chiusi in laboratorio” e si passa ad abbracciare l’idea dello spazio
sconfinato di internet “Uno spazio misto e spurio, in cui la contaminazione fra
reale e virtuale diventa la norma e non l’eccezione”.165

In pochissimo tempo nacquero delle comunità autonome organizzate a seconda delle
specifiche esigenze: nel 1995 si realizzano il progetto *APACHE*, un server web,
*PHP*, linguaggio per web dinamico, e *Gimp*, programma per la manipolazione di
immagini della GNU.

Con l’affermazione sempre più ampia di Linux come sistema operativo, i software
della FSF iniziarono a perdere la loro valenza politica e la libertà iniziava a
passare in secondo piano.

Secondo Simone Aliprandi, il 1998 rappresenta un punto di svolta: “Nel 1998 un
fulmine a ciel sereno illuminò i volti di quella comunità emergente: la Netscape
(importante impresa statunitense di software) decise di diffondere il suo
prodotto di punta (il browser Navigator) sotto i parametri dell'Opensource. Una
mossa abbastanza inaspettata che fu un grande segnale che i tempi erano
maturi.”166 Era il momento di fare il grande passo, cioè uscire dal mondo
sotterraneo della comunità hacker ed entrare nel mercato mondiale
dell’informatica, contro i grandi colossi del software proprietario. Era un
passo necessario per permettere alle imprese finanziatrici di conoscere (e
investire) in questa nuova realtà, ma era altrettanto rischioso perché rischiava
di perdere i principi etici che guidavano l’azione, come spesso sottolineava
Stallman. Per quest’ultimo il concetto di libertà era da tutelare in tutte le
sue sfaccettature, “sarebbe stato il mondo degli affari a doversi adeguare e a
trovare altre vie per sensibilizzare i nuovi potenziali clienti/utenti”.167 

Cominciava a delinearsi un nuovo profilo all’interno della  comunità, costituito
da personaggi come Tim O'Reilly, editore di manuali informatici, Bruce Perens e
soprattutto Eric Raymond, personaggio di spicco e attento osservatore della
realtà hacker. Tutti avevano partecipato alla realizzazione del progetto,
insieme a Linus Torvalds, di “liberazione del sorgente” di Netscape Navigator,
creando una famosissima licenza (la MPL, Mozilla Public License) alternativa a
quella redatta da Stallmann per la distribuzione dei prodotti GNU (la GPL). La
MPL era risultata più funzionale al caso Netscape (oltre che priva di componenti
ideologiche) ed era parsa subito ben accetta alla comunità hacker mondiale. 

Questi nuovi hacker, che lo stesso Aliprandi definisce piuttosto come
“programmatori indipendenti”, non sentivano la necessità di salvaguardare gli
aspetti ideologici della libertà, ma sottolineavano l’importanza di praticità e
funzionalità. Libertà doveva essere intesa anche nelle scelte: senza irrigidirsi
su posizioni ideologiche e diventarne in un certo senso schiavi, si doveva poter
essere liberi di usare anche un software proprietario, nel caso in cui questo
fosse più adatto all’uso specifico. Un esempio è fornito da Williams : ”Nel
corso di una discussione sul crescente dominio sul mercato da parte di Windows e
tematiche analoghe, Torvalds ammise di ammirare molto PowerPoint, il software
per le presentazioni di diapositive della Microsoft. […]Perché mai prendersela
con il software proprietario solo per principio? Essere un hacker non
significava dover soffrire, ma piuttosto fare bene il proprio lavoro.” 168

Per sottolineare questa nuova direzione, si andò alla ricerca di una nuova
terminologia che potesse definire meglio le nuove posizioni: fu proprio Raymond
a proporre “open source”. Il termine mantiene in parte una sfumatura etica, per
il riferimento al concetto di “apertura”, che con il tempo è però uscito dal suo
senso puramente tecnico assumendo una connotazione più ampia.

Nel 1998 Raymond propose anche l’istituzione di un’organizzazione che
coordinasse i vari progetti e vigilasse sul corretto uso del termine open
source: nacque la a Open Source Initiative (OS “In pratica, con un’immagine
matematica, la OSI sta al concetto di 'open source' come la FSF sta al concetto
di 'free software'. Due distinte e rilevanti strutture organizzative per
progetti che nella maggior parte dei casi andavano verso la stessa direzione. Il
canone dell'unità e della cooperazione poteva essere ormai depennato dalla lista
dei punti cardine dell'etica hacker.”169

Da una parte il Free Software poneva l’accento sulle libertà (“il software
libero è una questione di libertà, non di prezzo”170), dall’altra l’Open Source
si concentrava, secondo le logiche di mercato, a cercare i modi migliori per
diffondere un prodotto in base a criteri open.

Si impose in maniera netta il modello meno politico e più orientato alle
possibilità commerciali; la Free Software e la sua filosofia rimase in una
posizione marginale e bollata come “estrema e non sostenibile economicamente”.

3 Etica hacker e conoscenza ai tempi del web
============================================

È difficile stabilire con precisione una data in cui collocare la nascita di
Internet, tuttavia possiamo considerare il progetto ARPAnet ("Advanced Research
Projects Agency NETwork") realizzata dal Dipartimento della Difesa degli Stati
Uniti nel 1969 come primo embrione di quello che poi sarebbe diventato
l’internet che conosciamo.171 

Con il suo sviluppo e la sua diffusione, internet ha introdotto una serie di
cambiamenti notevoli; da un punto di vista tecnico ha enormemente velocizzato le
comunicazioni, ha ridotto i costi della ricerca dati, ha azzerato le distanze.
Ma ciò che, secondo Lawrence Lessig, è più importante è che internet ha avuto
“un effetto sul modo in cui si costruisce la cultura”172. 

Il senso e la portata di questo cambiamento sono enormi ma, paradossalmente,
sembrano passare inosservati; sempre secondo Lessig, possono essere compresi
distinguendo la “cultura commerciale” da quella “non commerciale” e seguendo la
mappa delle regolamentazioni legislative di entrambe. Nel primo caso si fa
riferimento a quella cultura pensata e prodotta con l’obiettivo di vendere, il
secondo caso riguarda tutto il resto, come ad esempio gli anziani che nei parchi
o nelle strade raccontavano storie ai ragazzi o a chiunque si trovasse a passare
da quelle parti. Nel corso della nostra tradizione storica, la cultura non
commerciale non era soggetta a regole e i metodi con cui le persone
condividevano e rielaboravano la loro cultura erano ignorati dalla legge.

La legge si concentrava sulla creatività commerciale, progettando gli strumenti
di tutela e di incentivo per i creatori, riconoscendo i loro diritti esclusivi
sulle opere, in modo che queste potessero essere vendute sul mercato. Oggi,
questa divisione non è più possibile e, ad impostare lo scenario di questo
cambiamento, è stato proprio internet: “Per la prima volta nel corso della
nostra tradizione, le modalità correnti con cui gli individui creano e
condividono cultura ricadono all’interno della regolamentazione giuridica, che è
stata estesa fino a portare sotto il proprio controllo una quantità di cultura e
creatività mai raggiunta prima”.173 La giustificazione di questo cambiamento è
data dall’esigenza di tutelare la creatività commerciale attraverso

> l’applicazione al mondo delle idee, della cultura e delle opere dell’ingegno
> di un concetto base dell’economia tradizionale: il valore di un bene è
> determinato dalla sua scarsità. L’applicazione di questo principio economico a
> beni immateriali come un programma informatico o una sequenza di note musicali
> ha come conseguenza una rigida tassazione di ogni forma di utilizzo o
> duplicazione delle opere dell’ingegno, e un lavoro incessante di monitoraggio
> e controllo per reprimere, sanzionare, criminalizzare e scoraggiare qualunque
> utilizzo di questi beni immateriali che non produce un immediato vantaggio
> economico per chi ne controlla i diritti di riproduzione.174

Secondo Lessig, la tutela non è a favore degli artisti: si tratta di un
“protezionismo” mirato a mantenere lo status quo delle grandi aziende
commerciali che producono cultura e che si sentono enormemente minacciate dalle
nuove forme di partecipazione al processo di elaborazione della cultura che
prima di internet non erano ipotizzabili.

Un importante strumento di tutela è il “copyright”, sostanzialmente un accordo
tra cittadini ed autori che dovrebbe mirare al beneficio della collettività
attraverso una maggiore produzione di arte e cultura, ma in pratica si traduce
in norme e vincoli che vanno a beneficio delle grandi aziende e a discapito dei
cittadini: 

> L’idea di base di questo accordo è semplice: i cittadini, tramite apposite
> leggi, concedono agli autori una maggiore possibilità di guadagno che si
> traduce in una maggiore produzione creativa. Cessato questo intervallo di
> tempo, però, l’interesse culturale della collettività, temporaneamente
> accantonato per garantire agli autori una maggiore autonomia produttiva,
> ritorna prioritario rispetto agli interessi economici dei singoli: le opere
> dell’ingegno vengono “liberate” per sempre, e chiunque può utilizzarle, anche
> a scopi commerciali.175

Inizialmente la concessione di un copyright all’autore di un’opera copriva un
periodo di tempo inferiore a trent’anni; oggi, ad esempio nel caso di un film,
lo stesso diritto è garantito per centoventi anni. Molto attiva nella battaglia
a favore del prolungamento del copyright è da sempre la Walt Disney Company:
infatti nel 1998, con il compimento del settantesimo anno di età, Topolino si
apprestava a diventare un’opera libera che chiunque avrebbe potuto utilizzare
anche a fini commerciali. Nello stesso anno, negli Stati Uniti si approvava la
“Sonny Bono Copyright Term Extension Act”, ribattezzata poi “Mickey Mouse
Copyright Extension Act”, che portava da settanta a novanta anni il limite
concesso alla Walt Disney Company per lo sfruttamento economico di Topolino,
limite che verrà ulteriormente prolungato a centoventi anni.176

Per restare nell’ambito di esempi eclatanti, Gubitosa ci ricorda che la canzone
“Happy Birthday to You” che si intona ai compleanni, pubblicata nel 1935, fa
guadagnare ogni anno due milioni di dollari alla Warner/Chapell Music, che ne
detiene i diritti di sfruttamento economico fino al 2030. Altro esempio è la
musica dei Beatles, che non può essere considerata parte integrante del
patrimonio culturale dell’umanità e deve essere acquistata e regolarmente pagata
agli eredi di Michael Jackson, che nel 1985 ne ha acquistato i diritti.

Questi esempi posso portare a chiedersi quali siano i limiti della tutela degli
interessi degli autori e dove cominciano gli interessi della collettività.

Come sostenuto da Jim Thomas177, mentre i principi generali di “giusto” e
“sbagliato” si mantengono relativamente costanti, le prospettive e il contesto
in cui vengono giudicati spesso cambiano e una maggiore comprensione delle
azioni può portare ad esaminare il tutto con una nuova sensibilità: 

> Ad esempio la convivenza tra coppie non sposate che è diventata una cosa
> sempre più comune a partire dal 1960, una volta era considerata
> inequivocabilmente immorale e un esempio di "degrado sociale morale”. Questa
> accettazione non coinvolge tanto uno spostamento o ridefinizione dei principi
> fondamentali del concetto di “decenza”, quanto piuttosto un nuovo
> apprezzamento per le ragioni, motivazioni e benefici della convivenza, in
> parte accompagnati dalla modifica delle norme sessuali e una riduzione
> asimmetrica dei rapporti di potere tra uomini e donne.178

Esempio per alcuni aspetti simile, perché riflette su una legge solo quando
sorge un nuovo problema, dovuto a una nuova tecnologia, è fornito dallo stesso
Lessig: nel 1903, quando i fratelli Wright inventarono l’aeroplano, la
legislazione americana attribuiva al proprietario di un terreno la proprietà
dello stesso per quanto riguardava il sottosuolo e lo spazio al di sopra. Con
l’avvento del traffico aereo, per la prima volta questo principio acquistava
importanza: qualcuno poteva alzarsi e rivendicare il diritto di proteggere la
sua proprietà, come di fatto fecero due contadini del North Carolina nel 1945:
“I coniugi Causby iniziarono a perdere i polli a causa dei voli a bassa quota
degli aerei militari (sembra che i polli volassero terrorizzati contro le pareti
del granaio e morissero), quindi sporsero denuncia sostenendo che il governo
violava illegalmente la loro proprietà terriera”. 179 La Corte Suprema esaminò
il caso, ma il giudice liquidò la faccenda senza molti giri di parole, come
riporta Lessig: “[Tale] dottrina non ha spazio nel mondo moderno. L’aria è
un’autostrada pubblica, come ha dichiarato il Congresso. Se ciò non fosse vero,
ogni volo transcontinentale sarebbe soggetto a infinite denunce per violazione
di proprietà. Il senso comune si ribellerebbe all’idea”.180

In ogni caso, il principio fondamentale del sistema di diritto consuetudinario è
che la legge si adegua alle tecnologie dell’epoca. E cambia mentre si adegua,
soprattutto se non c’è un soggetto potente sulla sponda opposta del cambiamento.

La proprietà intellettuale, definita dagli accordi *Trips* (*Trade-Related
Aspects of Intellectual Property Rights*, uno degli accordi siglati, all'interno
delle attività dell'Organizzazione mondiale per il Commercio, al termine
dell'Uruguay Round al quale hanno partecipato 123 paesi discutendo di
telecomunicazioni, farmaci anti-Aids, regolamentazioni bancarie e biogenetica,
ed entrati in vigore a metà degli Anni Novanta.)181 è quell’insieme di norme che
riguardano il diritto d’autore, brevetti, modelli, disegni e che riassume oltre
due secoli di dibattito in cui si è cercato di stabilire se le idee e la
conoscenza facciano parte dei beni naturali o dei beni pubblici. 

> La “guerra” che è stata dichiarata alle tecnologie di Internet - quel che Jack
> Valenti, presidente della Motion Picture Association of America (MPAA),
> definisce la sua “guerra personale contro il terrorismo” - è stata presentata
> come una battaglia sulle norme giuridiche e sul rispetto della proprietà. Per
> sapere da che parte stare in questa guerra, la maggioranza crede che si debba
> decidere soltanto se si è a favore o contro la proprietà.182 

A questo schieramento si contrappone chi non crede nella proprietà delle idee e
sostiene che si tratti di un bene che diventa una proprietà pubblica una volta
divulgato. Carlo Gubitosa sostiene che si tratti di un principio economico
completamente diverso: “ Nella società dell’informazione il valore di un bene
immateriale, concettuale o artistico è determinato dalla sua diffusione. Un
libro, un brano musicale o un programma informatico hanno un valore
proporzionale al numero di persone che conoscono e utilizzano quel testo, quella
musica o quel programma.”183

Nel testo *Permesso d’autore*184, si evidenzia una consistente parte della
letteratura classica che si esprime in questo senso e che va da Pierre-Joseph
Proudhon, il filosofo francese che nel XIX secolo teorizzò fra i primi il
concetto di anarchia come organizzazione politica e sociale dei cittadini senza
l'intermediazione dello stato, all'americano Benjamin R. Tucker per il quale: 

> Dalla giustizia e dalla necessità sociale della proprietà delle cose concrete
> abbiamo erroneamente desunto la giustizia e la necessità sociale della
> proprietà delle cose astratte – cioè la proprietà nelle idee – con il rischio
> di annullare in larga e deplorevole misura quella caratteristica fortunata
> delle cose in circostanze non ipotetiche ma reali – cioè la possibilità
> incommensurabilmente fruttuosa, per un numero qualsiasi di persone, di usare
> nello stesso tempo le cose astratte in un qualsiasi numero di luoghi
> diversi.185 

Un secolo più tardi anche l’economista e storico esponente del liberalismo
Friedrich von Hayek, in *Perché non sono un conservatore* (1997), si schiererà
contro eccessivi strumenti e forme di controllo, riconoscendo nella concorrenza
non solo materiale, ma anche delle idee, la migliore soluzione per arricchire la
società.

Dal canto suo, Lawrence Lessig riprende alcune dichiarazioni di Thomas
Jefferson, terzo presidente degli Stati Uniti, il quale, in una lettera del
1813, scrive: 

> Chi riceve un'idea da me, riceve una conoscenza che non toglie nulla alla mia,
> così come chi accende la sua candela con la mia si fa luce senza per questo
> lasciarmi al buio. Che le idee circolino liberamente, una dopo l'altra, in
> tutto il mondo, perché gli uomini possano a vicenda trarne istruzione morale e
> miglioramento personale, senza negare un fatto voluto espressamente da una
> natura benevola, che le ha fatte come il fuoco, libere di diffondersi ovunque
> senza perdere in nessun punto la propria intensità [...]. Le invenzioni non
> possono dunque, per loro natura, essere soggette a un regime di proprietà.186

Lessig e Beccaria, nei rispettivi testi, riportano esempi di casi in cui la
tolleranza di copie non autorizzate e il riutilizzo delle idee creative sono un
interessante business: il fenomeno del *doujinshi*, riviste nate e che godono di
grande popolarità soprattutto in Giappone. Si tratta di una forma di fumetti, o
meglio manga, che si configurano come fumetti-imitazione. I loro creatori
“attingono a piene mani da personaggi e storie manga originali per proporre
versioni rielaborate sia narrativamente che stilisticamente”.187 L’importante è
che non si tratti di una mera copia: l’autore deve sempre fornire un contributo
originale a quello che crea, che si tratti di piccole sfumature o di radicali
sconvolgimenti.

In una dimensione certamente più contenuta, anche in Italia si assiste allo
sviluppo di un fenomeno simile: si tratta delle *fanfictions*, opere scritte dai
fan (da qui il nome) prendendo come spunto le storie o i personaggi di un'opera
originale, sia essa letteraria, cinematografica, televisiva o appartenente a un
altro medium.

Nell’articolo *Elaborazione di testi nelle comunità di fan: indagine sulla
produzione di fictions in Italia*,188 si esaminano i due principali archivi
disponibili su internet, uno italiano, EFP Fanfiction, e uno rivolto ad un
pubblico geograficamente più eterogeneo, Fanfiction.net, contano rispettivamente
4.158.157 e 153.747 storie. Tuttavia, le fandom (sottoculture costituite
dall’insieme dei fan appassionati a un determinato prodotto e delle pratiche che
lo riguardano, come forum, *fanvideos*, *fanfictions*) dedicate ai prodotti
mediatici difficilmente possono aspirare a creare qualcosa che sia più che
amatoriale, a causa, in particolare, delle leggi sul copyright.

In Giappone esistono 33 mila associazioni di autori e appassionati e
manifestazioni culturali che ogni anno attirano 450 mila partecipanti, ma anche
in questo Paese la legislazione non permette la copia non autorizzata. Tuttavia 

> la normativa giapponese tende a mitigare tale rigore legale. Alcuni ritengono
> che sia proprio il beneficio che ne deriva al mercato dei manga a spiegare
> questa permissività. Per esempio, secondo l’ipotesi di Salil Mehra, professore
> di legge alla Temple University, il mercato dei manga accetta queste
> violazioni tecniche perché lo stimolerebbero a diventare più ricco e
> produttivo. Sarebbe peggio per tutti se i doujinshi venissero vietati, perciò
> la legge non li proibisce.189

Nel gennaio del 1986, un hacker che si faceva chiamare The Mentor scrisse un
saggio dal titolo *Manifesto Hacker*190, in cui affidava al popolo delle rete
una sottospecie di carta d’identità in cui erano contenute abitudini, codici
morali e regole destinate a diventare piuttosto note:

> Io sono un hacker: entrate nel mio mondo.  Avete mai guardato che cosa c’è
> dentro gli occhi di un hacker, voi con la vostra mente pretecnologica e la
> vostra psicologia da due soldi? Vi siete mai chiesti quali sono le forze che
> danno forma alla mia vita? Ora questo mondo è nostro, ed è il mondo degli
> elettroni e dei circuiti, dominato dalla bellezza delle reti. Noi esploriamo
> le frontiere della conoscenza e voi ci chiamate criminali. Siamo una comunità
> che esiste a dispetto delle differenze razziali, della nazionalità e delle
> religioni, e voi continuate a chiamarci criminali. Siete voi quelli che
> costruiscono bombe atomiche, che dichiarano guerra ad altri paesi, siete voi
> che uccidete, imbrogliate, ci mentite e provate a convincerci che lo fate per
> il nostro bene, ma alla fine i criminali siamo noi. Sì, io sono un criminale,
> e il mio crimine è la curiosità. Il mio crimine è quello di giudicare le
> persone per quello che dicono e pensano, e non per le loro apparenze. Il mio
> crimine è quello di essere più intelligente di voi, e questo non me lo
> perdonerete mai. Io sono un hacker, e questo è il mio manifesto. Potete
> fermarci individualmente, ma non potrete mai fermarci tutti.

L’etica hacker e il libero scambio sono bollate come “pirateria informatica”, ma
non hanno niente a che fare con il crimine. Si tratta di “meccanismi virtuosi di
sviluppo culturale e tecnologico caratterizzati da una particolare attitudine
verso la conoscenza, una curiosità e una sete di sapere lasciate in eredità
dalle controculture degli anni ‘60 nate all’interno dei campus universitari
statunitensi”.191 L’etica hacker, o meglio lo spirito che la guida, è nato molto
prima dei calcolatori elettronici e dei computer; si manifesta ogni qualvolta
gli individui decidono che la conoscenza in grado di cambiare il mondo è più
importante delle regole stabilite per mantenere l’ordine e lo status quo.

Essere un hacker significa appartenere ad una categoria di persone che condivide
il gusto per risolvere i problemi non perdendo lo spirito del divertimento e del
gioco, non perdendo la curiosità di capire come funzionano le cose, anche a
costo di smontarle pezzo pezzo. Questo può valere per un computer, per un ferro
da stiro, o per un qualsiasi altro strumento tecnologico, ma anche per le regole
sociali, che possono essere smontate e ricostruite secondo criteri diversi: 

> Gli hacker non sono guidati in ciò che fanno da un interesse economico, ma
> usano i computer come uno strumento per l’espressione libera e creativa della
> loro mente. Rincorrendo soluzioni sempre più efficaci a problemi sempre più
> complessi, gli hacker migliorano continuamente circuiti elettronici e
> programmi, accettando nuove sfide intellettuali per il puro gusto di vincerle.
> Un hacker è una persona che non vuole solo risolvere un problema, ma sente il
> bisogno di sottometterlo alla propria intelligenza.192

A questo punto può essere utile aprire una parentesi sul concetto di
conoscenza193: la conoscenza può essere definita come una serie di costruzioni
che ogni individuo realizza per affrontare la complessità dell’ambiente in cui
si trova. Queste costruzioni tendono proprio a semplificare la complessità
dell’ambiente e tentano di conferire ad esso un “ordine”. Per realizzare queste
costruzioni, ogni individuo deve compiere una scelta tra le infinite
caratteristiche presenti nell’ambiente. Quindi da un lato ci troviamo di fronte
ad un processo creativo, in cui si realizza una descrizione della realtà unica e
nuova, che prima non c’era, mentre dall’altro lato si tratta di una
ricombinazione, perché parte da una combinazione di caratteristiche già
disponibili nell’ambiente: “Questo doppio carattere della conoscenza permette di
affermare che non c’è né creazione senza ricombinazione, né ricombinazione senza
creazione”.194

In un processo creativo, che si tratti di musica, di informatica, o della
stesura di una tesi di laurea, nessuno è in grado di “partire da zero”, cioè di
estrarre dalla propria mente qualcosa di nuovo senza prima attingere a ciò che
già conosce. Il genio isolato dal mondo che crea grazie all’ispirazione delle
Muse è un’immagine senza dubbio affascinante, ma in concreto di difficile
realizzazione. Nell’articolo di Monceri sono riportati come esempi la narrativa
di fantascienza e i film: i “mostri” e gli “alieni” hanno sempre qualche
caratteristica riconoscibile e riconducibile a qualche categoria, proprio perché
la creazione parte sempre dalla ricombinazione di elementi conosciuti. Il
passaggio da ricombinazione a creazione dipende dalla capacità degli individui
di unire elementi solitamente percepiti come “separati”: “In una parola, è la
capacità di vedere in modo diverso dal solito, un’abilità che tutti gli esseri
umani devono sviluppare per sopravvivere in un ambiente complesso, ma che
soltanto pochi trasformano in una delle abilità più importanti per la loro
vita”.195

L’individuo che seleziona gli elementi da combinare, lo fa sempre a partire da
una sua prospettiva e sceglierà questi elementi sulla base delle sue esperienze,
di ciò che sente più vicino al suo pensiero: in questo momento io sto
consapevolmente scegliendo degli autori e degli argomenti per la realizzazione
della mia tesi di laurea, ma il mio obiettivo non è realizzare un “collage” di
posizioni autorevoli, quanto piuttosto partire da una serie di posizioni date
per aggiungere qualcosa di “mio” all’argomento trattato.

L’articolo di Flavia Monceri che è alla base di questa parte del lavoro, ad
esempio, è stato scelto per una serie di motivi: mi è stato fornito direttamente
dall’autore, è gratis, è anche facilmente reperibile on-line, utilizza concetti
molto semplici per spiegare argomenti complessi. Questi elementi non possono
essere considerati senza specificare che sono scelti da me: Maria Elena Malva,
studentessa dell’Università del Molise, studentessa e laureanda di Flavia
Monceri.

Non mi sto limitando a leggere un articolo e ad utilizzare sinonimi per
riportare il suo significato nella mia tesi, citandone l’autore originale (il
che, trattandosi di processo creativo, mi metterebbe già in pace con la
“dichiarazione di originalità” da me firmata all’inizio di questa tesi), ma lo
sto spiegando, fra le altre cose, a partire dalla mia posizione di studentessa
universitaria che affronta una tesi di laurea, piuttosto che dalla posizione
dell’autore, professore universitario.

L’importanza che assume internet in questo contesto non riguarda “il modo in cui
conosciamo o la conoscenza stessa”196, quanto piuttosto la possibilità per un
infinito numero di individui di poter organizzare il proprio processo di
conoscenza in base a un’infinita disponibilità di informazioni e dati, e quindi
anche di scegliere percorsi che si allontanando dalla “uniformità della
conoscenza” che ha il fine di “mantenere la stabilità del modello prevalente di
ordine”.197 Inoltre internet offre la possibilità di condividere la conoscenza,
anche quella che non segue i “percorsi tradizionali”.

3\.1 Creative Commons
---------------------

Lawrence Lessig, già abbondantemente citato in questo lavoro, è un giurista
americano. Ha insegnato diritto ad Harvard e a Stanford, è responsabile dello
Stanford Center for Internet and Society ed è da molto tempo impegnato a
studiare le implicazioni giuridiche del software libero: 

> Oltre a numerosi articoli e saggi pubblicati nelle maggiori riviste di
> dottrina giuridica e riportati in gran parte in Internet, ha pubblicato tre
> importanti libri: *Code and other laws of cyberspace* (1999), completo saggio
> di taglio accademico sulle nuove frontiere del diritto nel mondo digitale; *Il
> futuro delle idee* (2006) in cui inizia a delineare la sua posizione
> pionieristica rispetto al destino dei “commons” nel mondo delle comunicazioni
> telematiche e multimediali; e *Cultura Libera: un equilibrio fra anarchia e
> controllo, contro l’estremismo della proprietà intellettuale* (2005),
> considerabile il vero manifesto del movimento Opencontent.198

Nel 2001, insieme ad altre figure di spicco nell’ambito delle comunicazioni e
della proprietà intellettuale come James Boyle, Michael Carroll ed Eric Eldred,
ha dato vita ad un progetto che “che si rivelerà presto uno dei più ambiziosi in
fatto di libera diffusione delle conoscenze e della creatività in generale,
addirittura più ambizioso del progetto GNU e della Open Source Initiative per
l'ampiezza del panorama cui si rivolge”.199

La Creative Commons (CC) è un’“organizzazione non-profit registrata in
Massachussetts ma di casa presso la Stanford University e condotta da un
consiglio di amministrazione che include esperti di *ciberlaw* e di proprietà
intellettuale. […] L’obiettivo di CC è di dare vita ad un movimento di
consumatori e produttori che contribuiscano alla costruzione del pubblico
dominio e che, con il loro impegno, ne dimostrino l’importanza per la creatività
altrui.”200 Il punto di partenza è infatti l’eccessiva estensione sia di
ampiezza che di durata del copyright, che secondo Lessig è il frutto delle
pressioni delle grandi lobby, come la già citata Walt Disney Company, e la
conseguente elaborazione di un copyright più elastico, in cui non tutti i
diritti sono riservati, realizzata attraverso le *Creative Commons Public
Licenses* (CCPL). Questa si presenta come una posizione intermedia tra due
eccessi201: da un lato chi sostiene la politica del “tutti i diritti riservati”
e dall’altro chi la nega completamente sostenendo che un’opera pubblicata
diventa di pubblico dominio. Un’opera rilasciata con una CCPL consente
determinate libertà che vanno al di là di quelle permesse dalle libere
utilizzazioni consentite nei diversi Paesi. Si parla di “alcuni diritti
riservati” perché i limiti di tali libertà sono determinati dalle scelte
dell’autore.

> La Creative Commons cerca di facilitare la creazione di opere sulla base di
> lavori altrui, rendendo semplice agli autori sostenere che altri siano liberi
> di attingere al loro lavoro e di creare su di esso. Tutto grazie a semplici
> tag (nel codice HTML) legati a descrizioni che le persone possono leggere e
> vincolati a licenze a prova di bomba. […]Questi tag, o contrassegni, vengono
> poi collegati alle versioni delle licenze che il computer è in grado di
> leggere e che gli permettono di identificare automaticamente il contenuto per
> cui è possibile la condivisione. L’insieme di questi tre elementi - una
> licenza legale, una descrizione che le persone possono leggere e alcuni tag
> che la macchina può leggere - costituiscono una licenza Creative Commons.202

In questo modo gli autori non perdono i loro diritti, ma possono gestirli
autonomamente attraverso una procedura relativamente semplice, senza la
necessità di fare riferimento ad un legale.

Nel dicembre del 2002 la CC ha rilasciato inizialmente undici licenze, poi
divenute sei, che sono valide in tutto il mondo, hanno una durata pari a quella
del copyright e non sono revocabili.203 Nascono con l’obiettivo di tutelare le
diverse esigenze degli autori e questo è possibile attraverso la loro struttura
che “è tale da avere un layout di base formato da alcune disposizioni comuni a
tutte le CCPL su cui s’innestano alcune clausole-varianti”.204 Come spiegato da
Alvanini, la parte comune prevede che nessuna licenza limiti gli usi consentiti
(noti con il termine *fair use*), l’esaurimento del diritto e la libertà di
espressione altrui.

Il licenziatario deve sempre ottenere il permesso dell’autore per fare cose che
quest’ultimo ha deciso di limitare; deve mantenere l’indicazione di diritto
d’autore intatta su tutte le copie dell’opera, per rendere sempre individuabile
il detentore dei diritti e la licenza scelta; non deve alterare i termini della
licenza; deve inserire in ogni opera un link che rimandi alla licenza scelta
dall’autore e non deve utilizzare mezzi tecnologici per impedire ad altri
licenziatari di esercitare l’uso consentito dalla legge.

Ogni licenza permette di copiare l’opera con qualsiasi mezzo e su qualunque
supporto; distribuire l’opera attraverso i più disparati circuiti, “con
l’esclusione in determinati casi dei circuiti prevalentemente
commerciali”;205modificare il formato dell’opera; comunicare, recitare,
rappresentare, eseguire, esporre al pubblico l’opera, compresa la trasmissione
audio-digitale.

La parte che differenzia le licenze è quella in cui sono indicate le libertà che
l’autore vuole concedere sulla propria opera e le condizioni per fruire di tali
libertà. All’indirizzo http://www.creativecommons.org si può seguire una
procedura guidata che permette la scelta e la combinazione di quattro opzioni
(*License Conditions*): la caratteristica “Attribution” si riferisce all'obbligo
di rendere merito all'autore originario dell'opera e quindi di citarlo in ogni
utilizzazione dell'opera; la caratteristica “No Deriv” indica il divieto di
apporre modifiche all'opera e quindi di crearne opere derivate; la
caratteristica “Non Commercial” vieta l'utilizzo dell'opera per scopi
commerciali; infine la caratteristica “Share Alike”, che letteralmente si
traduce “condividi allo stesso modo”, indica invece l'obbligo di applicare alle
opere da essa derivate lo stesso tipo di licenza dell'opera originaria.

Ogni CCPL, anche se unica da un punto di vista giuridico, è disponibile in tre
diverse “vesti”: la prima e più importante perché l’unica ad essere rilevante
sul piano legale, è il *Legal Code*, “ il classico documento per addetti ai
lavori, formato da alcune premesse e da otto articoli, in cui si disciplina la
distribuzione dell’opera e l’applicazione della licenza”206. Il rischio di un
documento così “tecnico” è che chi non ha piena padronanza di tale linguaggio
possa avere difficoltà a leggere e comprenderne il senso, con la conseguenza di
utilizzare le licenze con poca consapevolezza o in modo diverso da quello
voluto.

Si è perciò pensato di realizzare versioni sintetiche di tali licenze, scritte
in un linguaggio più accessibile, che consiste in una veste grafica della
licenza: “L’autore può scegliere attraverso delle icone, i cosiddetti visuals,
le diverse condizioni con cui licenziare la propria opera. Un’opera licenziata
Creative Commons, accanto all’icona CC, presenterà sempre anche le icone che
rappresentano le condizioni secondo cui è possibile utilizzare l’opera”207.
Questa seconda veste è chiamata *Commons Deed* (“atto per persone comuni”) ed è
importante sottolineare che non ha valore legale. “Il Commons deed riassume
dunque in poche righe il senso della licenza e rimanda con un link al Legal
code, nonché alle varie traduzioni in altre lingue disponibili”208.

L’ultima veste è la *Digital Code*, che consiste in una serie di metadati che
rendono la licenza facilmente rintracciabile dai motori di ricerca. I metadati
sono informazioni aggiuntive, dei tag, che si possono allegare a qualsiasi file
digitale e che si possono visualizzare solo grazie ad alcuni procedimenti
informatici.

L’insieme di una licenza legale, un documento che ne semplifica comprensione e
utilizzo e dei tag che il computer può riconoscere, costituiscono una licenza
Creative Commons.

Lo sviluppo del dibattito intorno a questi temi, che ha portato alla versione
3.0 delle CCPL, cioè quella attualmente disponibile, è iniziato intorno al 2005
come conseguenza del confronto tra CC e Debian (un sistema operativo libero che
utilizza Linux come Kernel. Molti programmi di questo sistema appartengono a GNU
project, infatti il sistema è noto anche come “Debian GNU/Linux209), e tra CC e
il Massachusetts Institute of Technology. 

Questa università, infatti, già nel settembre 2002, cioè “prima ancora del
rilascio della *Creative Commons core licensing suite*”210, aveva lanciato il
progetto “*OpenCourseWare*”211, che utilizzava una versione precoce della
*Attribution-NonCommercial-ShareAlike License*. Il progetto, come spiegato sul
sito, è una pubblicazione digitale libera e aperta di materiale educativo di
alta qualità, organizzato in corsi. Questa apertura è possibile grazie a licenze
flessibili come le CCPL.

Creative Commons e MIT hanno deciso di lavorare insieme per realizzare una nuova
versione della licenza già in uso, per risolvere una delle grandi preoccupazioni
del MIT, data la sua prestigiosa reputazione: “Garantire che quando le persone
traducono e adattano localmente i contenuti del MIT sotto i termini della
licenza BY-NC-SA, sia specificato che essi lo stanno facendo sotto i termini
della licenza e non come un risultato di un rapporto speciale tra il MIT e la
persona che ha tradotto quel contenuto”.212

In sostanza si tratta di una clausula di “Non approvazione” che impedisce al
licenziatario di interpretare il requisito dell’attribuzione delle CCPL come
base per fraintendere la natura del rapporto con il licenziante. Questa clausola
è stata quindi integrata nella nuova versione delle licenze, la 3.0.

3\.2 Wikipedia
--------------

Il concetto di bias come monopolio della conoscenza si deve principalmente ad
Harold Innis213 e alla tradizione degli studi della scuola di Toronto. In *Le
tendenze della comunicazione*, Innis individua le principali tendenze, o *bias*,
della comunicazione nei monopoli del papiro, della pergamena e della stampa. A
questi monopoli dei media fanno riferimento soggetti specifici: l’Impero, la
Chiesa e gli Stati Nazionali, istituzioni che “nel corso della storia
incorporano i media secondo logiche particolari di potere e controllo sulla
dimensione spazio/tempo”.214

L’oggetto di tali monopoli è la conoscenza. Secondo Innis, il monopolio
determina quale sarà il carattere della conoscenza da comunicare e questa
influenza sarà così forte da creare una civiltà la cui vita e flessibilità
saranno via via più difficili da mantenere. I vantaggi del nuovo mezzo saranno
tali da portare alla nascita di una nuova civiltà.

La civilizzazione dell’Occidente è quindi interpretata come “un processo
dialettico fra diversi monopoli mediali: quello della pietra fondato sulla
continuità delle civiltà antiche nel tempo, quello successivo del papiro e della
pergamena funzionale al controllo amministrativo dell’Impero romano sullo
spazio, e poi in età moderna, i monopoli di stampa e quotidiani, quali
espressione della libera circolazione del sapere e, allo stesso tempo, della
nascita degli Stati Nazionali”215. La sua analisi si spinge fino a metà del
‘900, quando inizia la diffusione del mezzo televisivo, ponendo così le basi per
lo studio dei “nuovi media”. Lo stesso autore si rende conto della difficoltà
che comporta l’applicazione del modello economico dei monopoli agli studi sulla
comunicazione, ed è lui stesso a spiegare nell’introduzione al libro che il
modello è stato esteso fino al suo estremo limite.

Con la nascita e lo sviluppo di internet e lo sviluppo di logiche nuove di
condivisione della conoscenza, la dimensione storica della nostra cultura si sta
ridefinendo sulla base di processi di elaborazione e codifica delle conoscenze
caratterizzati dal cambiamento delle forme di comunicazione ed esperienza di
quella che Manuel Castells chiama la *società in rete*216. 

In questo nuovo modello, i processi culturali sono più complessi e conflittuali
che non omogenei, perché mentre prima i mediatori del sapere erano le
istituzioni “classiche” come la scuola e l’università, oggi da un lato i
monopoli dell’industria culturale mantengono la loro influenza, ma dall’altro
lato vedono crescere l’importanza di soggetti individuali che sono in grado di
esprimersi attraverso “forme personali di creatività progettuale”.217

Di conseguenza, scrive Castells in un saggio dal titolo *Comunicazione, Potere e
Contropotere nella network society*,

> i rapporti di potere – ossia le relazioni che servono da fondamento a tutte le
> società – e i processi che sfidano i rapporti di potere istituzionalizzati
> sono sempre più plasmati e determinati dalla sfera della comunicazione. Per
> “potere” si intende la capacità strutturale, da parte di un attore sociale, di
> imporre la propria volontà su di un altro (o più d’uno) attore sociale. Tutti
> i sistemi istituzionali riflettono rapporti di potere, ma anche i loro limiti,
> negoziati attraverso un processo storico di domino e contro-dominio. Per
> contropotere, si intende la capacità, da parte di un attore sociale, di
> resistere e sfidare i rapporti di potere istituzionalizzati. In realtà, tali
> rapporti sono per loro natura conflittuali, essendo le società eterogenee e
> contraddittorie. Il rapporto tra tecnologia, comunicazione e potere riflette,
> dunque, valori e interessi contrastanti, e innesca un conflitto tra una
> pluralità di attori sociali. 218

Gli individui rivestono un ruolo sempre più importante nel creare nuove modalità
di gestione e condivisione della conoscenza e quindi anche nel riconfigurare
tecnologie e media.

Nel modello e negli studi di Innis la dimensione sociale ha un’importanza
secondaria: il monopolio si riferisce alle istituzioni, agli specifici mezzi di
comunicazione e alle diverse relazioni di potere. Quello che emerge oggi è
invece proprio il fatto che la dimensione sociale abbia un ruolo determinante
nella costruzione e nell’adattamento delle tecnologie della comunicazione, cioè
in definitiva il fatto che il processo è divenuto bidirezionale. 

L’origine e lo sviluppo di internet, che possiamo considerare come il nuovo
medium per eccellenza, è dovuta secondo Castells “alla commistione unica di
strategia militare, cooperazione dell’alta tecnologia, imprenditorialità
tecnologica e innovazione controculturale”.219 La cultura della prima
generazione di utenti della rete, con le sue tendenze libertarie, comunitarie e
utopiche ha influenzato la Rete in due diverse direzioni: da un lato tendeva a
restringere l’accesso solo a coloro che avevano sufficienti conoscenze e tempo
da dedicare alla vita nel cyberspazio, e di questa epoca sussiste ancora uno
spirito pioneristico che non vede di buon occhio la commercializzazione della
rete. Dall’altro l’origine controculturale della rete è ancora evidente nel
carattere informale e autodiretto della comunicazione, l’idea del contributo da
molti a molti, il fatto che ogni individuo possieda la propria voce e la propria
dimensione individuale. Secondo Castells, anche oggi questo filo conduttore tra
origini controculturali di internet e internauti odierni è ben evidente e lo
dimostra il fatto che la rivista *Wired*, nata come voce controculturale, sia
attualmente accettata da parte del mondo degli affari e sia l’espressione più
alla moda della cultura internet e dei suoi usi.220

Presupposto fondamentale per lo sviluppo di internet e per la creazione di nuove
applicazioni è l’apertura dell’architettura informativa del web e la trasparenza
dei suoi meccanismi di funzionamento: “Grazie al circolo virtuoso fra
architettura aperta, possibilità di innovazione e relativa facilità
nell’implementare contenuti sul web, la rete fu il terreno fertile per
alimentare i processi spontanei di collaborazione e condivisione di risorse”.221
Questo circolo è evidente anche oggi nei modelli partecipativi come i blog, le
newsletter o i più recenti WIKI, guidati da alcuni valori fondamentali come il
valore della comunicazione libera e orizzontale, il perseguimento autonomo dei
propri interessi con la possibilità per ogni individuo di pubblicare contenuti
propri, e l’idea di una comunità on line che si configura come uno strumento di
organizzazione, costruzione del significato e azione collettiva.

Tuttavia negli sviluppi più recenti di internet si assiste ad un’inversione di
tendenza:  il carattere dominante sembra essere quello della privatizzazione
delle conoscenze collettive. In generale, parliamo dei monopoli del software
proprietario, ma questa tendenza si può rintracciare anche nell’ambito di
conoscenze di pubblica utilità, come la ricerca scientifica, ad esempio
nell’ambito delle ricerche sulla mappatura del DNA.

Per quanto riguarda le “guerre del software”, si è già accennato all’antagonismo
tra il colosso Microsoft, Linux e tutti i programmi scritti e distribuiti
secondo la logica dell’Open Source. Il rischio di un intreccio tra software
proprietario e privatizzazione della conoscenza potrebbe definire uno scenario
all’interno del quale pochi attori hanno il potere di definire non solo quali
sono gli strumenti per l’accesso alla conoscenza, “ma anche la logica, i criteri
di ordinamento e infine la conoscenza stessa”.222

Proprio per cercare di contrastare questa tendenza, nel 1999 Richard Stallman
scriveva il saggio *L’enciclopedia Universale Libera e le risorse per
l’apprendimento*,223 in cui sosteneva che “il World Wide Web avesse le
potenzialità per svilupparsi in un'enciclopedia universale che copra tutti i
campi della conoscenza”224.

Questo manifesto ispirò vari progetti di enciclopedie on line, alcuni, come
l’Enciclopedia Filosofia di Stanford, basati comunque sulle logiche editoriali
di proprietà intellettuale. Altri, come la svedese Susnig.nu, la *Enciclopedia
Libre* e Wikipedia sono invece veri e propri wiki, basati sul modello *Open
Content* (“contenuto libero”). Il concetto trae ispirazione da quello di Open
Source, ma qui ciò che si rende liberamente disponibile all’utilizzo non è il
codice sorgente di un programma, quanto piuttosto i suoi contenuti editoriali.

La definizione che si trova nella versione italiana di Wikipedia è la seguente: 

> Wikipedia è un'enciclopedia online, collaborativa e gratuita. Disponibile in
> oltre 280 lingue, Wikipedia affronta sia gli argomenti tipici delle
> enciclopedie tradizionali sia quelli presenti in almanacchi, dizionari
> geografici e pubblicazioni specialistiche.  Wikipedia è liberamente
> modificabile: chiunque può contribuire alle voci esistenti o crearne di nuove.
> Ogni contenuto è pubblicato sotto licenza Creative Commons CC BY-SA e può
> pertanto essere copiato e riutilizzato adottando la medesima licenza.225

Le caratteristiche più rilevanti del progetto sono l’apertura e la libertà.
Apertura, nel senso che si basa su contributi redatti da volontari, utenti anche
occasionali che non sono tenuti a specificare identità o competenze, che possono
inserire nuove voci o modificare quelle già esistenti. È comunque possibile la
registrazione di un utente ed esistono alcune forme di protezione da atti di
vandalismo, ma “la totale apertura e, in fondo, la fiducia nello spirito
collaborativo della collettività, rimane un punto forte del progetto”.226

La libertà si riferisce all’utilizzo che chiunque può fare dei contenuti: sono
completamente accessibili, gratuiti, possono essere utilizzati in ogni modo,
anche a scopi commerciali. Questo è possibile grazie alla licenza GNU FDL,
licenza di tipo permissivo che permette distribuzione, creazione di opere
derivate e uso commerciale del contenuto a condizione che si mantenga
l’attribuzione agli autori e che il contenuto resti disponibile attraverso la
stessa licenza. Significativa eccezione sono loghi aziendali, saggi di canzoni,
foto giornalistiche già protette da copyright, che sono utilizzate con una
rivendicazione di fair use.

La data di nascita ufficiale di Wikipedia è il 15 gennaio 2001 per iniziativa di
Jimmy Wales, ma le sue origini risalgono ad un anno prima, quando una società
commerciale americana, la Bomis.com, progettò un’enciclopedia libera on line dal
nome *Nupedia*, che doveva essere redatta da un numero ristretto di esperti che
ne avrebbero garantito autorevolezza e attendibilità. Quindi l’idea iniziale era
quella di un’enciclopedia “libera”, ma non “aperta”. Wikipedia era un progetto
parallelo, già pensato con la caratteristica dell’apertura, le cui voci migliori
sarebbero poi state “adottate” dalla più autorevole Nupedia. Quando i progetti
presero vita, i redattori di Nupedia si opposero alla troppa apertura di
Wikipedia, ma rallentarono molto la loro attività a causa dei complessi processi
di revisione e dal numero ristretto di collaboratori. Nel frattempo Wikipedia in
pochi anni crebbe in maniera esponenziale, finendo con oscurare completamente il
progetto più “elitario”, che nel 2003 chiuse i battenti.

Dal 2004, inizia una fase di crescita ancora maggiore: 

> Alla fine del 2010 la sola versione inglese conteneva più di 3.500.000
> articoli; ma esistono 278 diverse Wikipedia redatte in altrettante lingue e
> dialetti del mondo, le più importanti delle quali, come la tedesca, la
> francese e l’italiana, contengono centinaia di migliaia di articoli. In media,
> ogni giorno
contribuiscono alla produzione
delle pagine della versione inglese
> di Wikipedia 10.000 persone. Wikipedia è dunque multilinguistica e
> multiculturale. Globalmente le statistiche riportano 91.000 autori attivi che
> lavorano a 17 milioni di articoli. Gli ultimi dati del gennaio 2010 segnano 13
> milioni e mezzo di utenti registrati, di cui 1.766 amministratori, e 78
> milioni di visitatori al mese.227

In Wikipedia s’intrecciano tre importanti dimensioni: quella tecnologica legata
al tipo di software utilizzato, quella giuridica relativa alla particolare forma
di gestione e tutela del diritto d’autore, e la dimensione culturale che
richiama l’etica hacker e le modalità di condivisione della conoscenza che vanno
oltre l’ambito informatico e coinvolgono i processi di produzione delle
informazioni.

Per quanto riguarda il software, già il nome Wikipedia rimanda al concetto di
Wiki: un sistema di “modifica aperta”228 che permette a chiunque di consultare
documenti sul web e modificarli in tempo reale senza vincoli di alcun tipo. Un
sito Wiki è un insieme di documenti ipertestuali che permette a chi lo utilizza
di aggiungere, modificare e organizzare i contenuti in maniera relativamente
semplice e veloce.

> Caratteristica peculiare del software Wiki è anche quella di registrare in una
> cronologia ogni modifica apportata e permettere, in caso di necessità, di
> riportare rapidamente l'intero sistema a una versione precedente, annullando
> eventuali modifiche scorrette o non gradite. Nella pratica un wiki diventa,
> quando è implementato con successo, un deposito organizzato di conoscenza,
> amministrato in modo cooperativo e dotato di una puntuale memoria storica sul
> proprio sviluppo.229

La seconda dimensione riguarda i principi del software libero, di cui si è
parlato nel capitolo precedente, a cui ogni programma relativo a Wikipedia si
ispira. La scelta di questo tipo di software è coerente con la visione
complessiva della conoscenza come bene pubblico di cui tutto il progetto si fa
portatore, e in effetti, “l'idea di un'enciclopedia aperta e libera può essere
considerata essa stessa un'estensione dei principi ispiratori del software
libero verso altri ambiti di conoscenza”.230

Come già detto, il software libero non dev’essere confuso con il pubblico
dominio, che è totalmente privo di tutela dal punto di vista del copyright. Al
contrario, il software libero gode della piena tutela della legge sul diritto
d’autore, ma in un senso diverso rispetto a quello tradizionale: la legge tutela
la libertà del software, invece di impedire la sua riproduzione non autorizzata.

L’ultimo aspetto riguarda le radici culturali ed etiche, le stesse che hanno
guidato la nascita e lo sviluppo del software libero e delle varie licenze. Si
tratta dell’etica della cultura hacker, di cui “parte delle sue caratteristiche,
come la flessibilità e il decentramento, hanno accompagnato la trasformazione
del sistema produttivo industriale, mentre la sua componente più visionaria e
libertaria ha dato origine ai movimenti *open content* e *open knowledge*”.231
Centrata su valori come quello della passione, dell'apertura e della
responsabilità, l'etica hacker sostiene una visione della conoscenza come frutto
di lavoro collettivo e di conseguenza anche come inalienabile patrimonio comune.

A differenza delle comunità hacker, in cui il maggiore impulso alla
partecipazione comunitaria è data dalla sfida intellettuale all’innovazione
rispetto ai prodotti software di mercato, oppure il “traguardo di una
gratificazione posticipata”232, in Wikipedia la comunità rappresenta un valore
in sé e il legame con gli altri la ragione fondamentale per donare un prodotto
utile. In Wikipedia quindi la comunità e le sue regole rappresentano la logica
fondamentale dei processi di costruzione della conoscenza.

Per quanto riguarda le logiche organizzative e i processi decisionali, Wikipedia
è di proprietà della *Wikimedia Foundation*, una società senza scopo di lucro
creata nel 2003 con sede in Florida, negli Stati Uniti. Il consiglio direttivo
di questa società si riserva tutte le decisioni finali in casi di controversie
su Wikipedia, controllando di fatto la proprietà dell'infrastruttura tecnologica
(hardware e software) e mantenendo una funzione di orientamento sugli sviluppi
generali dei progetti.

In Italia, nel 2005 è nata la *“Wikimedia Italia – Associazione per la
diffusione della conoscenza libera”*, affiliata alla Wikimedia Foundation, con
organizzazione e funzioni simili. Wikimedia Foundation e tutte le affiliate dei
vari paesi basano le loro entrate economiche principalmente su donazioni da
parte di privati e aziende.

Trattandosi di un’organizzazione a partecipazione volontaria, è sempre molto
difficile determinare la precisa quantità di persone che sono attivamente
coinvolte; il contributo individuale può essere anonimo e variare anche di
molto, sia dal punto di vista della quantità di scrittura che della continuità
temporale. (Ad esempio un utente può correggere solo una virgola di una voce,
per una sola volta nella vita, e non contribuire mai più).

Per provare ad avere un’idea del numero di soggetti coinvolti si potrebbe allora
considerare il numero di utenti registrati (4,6 milioni per il progetto
principale in lingua inglese, circa 180.000 per il solo progetto in lingua
italiana), ma anche in questo caso la registrazione non garantisce l’effettiva
attività. Nelle statistiche di Wikimedia sono considerati “utenti attivi” gli
utenti che hanno apportato almeno cinque modifiche nell'ultimo mese (43001 per
il progetto in lingua inglese, 2700 per Wikipedia in italiano):233

> Il nucleo di “utenti attivi” e ancor più quello degli “utenti molto attivi”
> (coloro che hanno apportato più 100 modifiche nell’ultimo mese, 4330 per il
> progetto inglese, 470 per quello italiano) di Wikipedia non è unito solo dallo
> scopo strumentale di far avanzare il progetto, ma evidenzia la condivisione di
> norme e valori più generali e allo stesso tempo specifici, che rendono
> possibile la costruzione di un senso di identità collettiva.234

Con un approccio simile a quello della cultura hacker, i valori impliciti della
comunità di Wikipedia derivano dall’individuazione di “cinque pilastri”235,
ovvero cinque indicazioni sulla natura del progetto e le idee guida che
dovrebbero farlo crescere: il primo punto è che Wikipedia è un’enciclopedia che
comprende caratteristiche delle enciclopedie "generaliste", delle enciclopedie
"specialistiche" e degli almanacchi. Sono escluse le definizioni da dizionario,
le discussioni tipo chat, i saggi personali, le ricerche originali, la
propaganda, le cronache di attualità, le autobiografie promozionali. Secondo
punto che ha una certa rilevanza è che Wikipedia ha un punto di vista
*neutrale*: 

> Ogni voce dovrebbe riportare le diverse posizioni e le diverse teorie sulla
> voce trattata, con un linguaggio chiaro e imparziale. Naturalmente i problemi
> relativi al rispetto di questo pilastro, che è considerata condizione
> imprescindibile per la vita di Wikipedia, crescono nel caso di voci
> controverse, dal contenuto politico o religioso. Per ovviare all'inevitabile
> parzialità soggettiva di ogni singolo autore, Wikipedia si affida ai benefici
> della redazione collettiva236. 

Il terzo pilastro, già ampiamente analizzato dal punto di vista delle licenze, è
che Wikipedia è libera, con riferimento alla licenza GNU FDL.

Wikipedia ha un codice di condotta, esistono delle norme che regolano i rapporti
tra gli utenti: rispettare gli altri, evitare gli attacchi personali, cercare il
consenso piuttosto che le “guerre di modifica”, mostrarsi accoglienti con i
nuovi arrivati.

Infine, l’ultima indicazione riprende pienamente lo spirito libertario
dell’etica hacker, invitando a diffidare dell'autorità, della burocrazia e ad
agire secondo il buon senso comune, anche contro l'interpretazione letterale di
una data regola. *Wikipedia non ha regole fisse: non essere timido*, il motto è
diretto ad incoraggiare chi ha paura di fare qualche danno anche involontario
data la complessità dell’ambiente, perché è sempre possibile tornare alla
versione precedente di una voce.

Questo insieme di regole non ha carattere repressivo e quando si rende
necessaria una decisione formale, più che seguire norme e codici, si cerca di
seguire il “metodo del consenso”: “Si tratta di un processo decisionale basato
estesamente sul confronto e sulla discussione, teso a trovare soluzioni di
mediazione accettabili da tutte le parti in causa. Il metodo del consenso non
significa necessariamente “mettere tutti d'accordo” quanto piuttosto ricondurre
le situazioni di conflitto all'interno dei limiti di compatibilità del
sistema”.237

Quest’ultimo aspetto permette di accennare alla gerarchia dei ruoli all’interno
della comunità: anche se aperta a chiunque, non tutti gli utenti sono uguali.
Gli utenti anonimi si trovano al gradino più basso della gerarchia. Quelli
registrati possiedono una propria pagina che mostra pubblicamente la cronologia
degli interventi e in generale tutta la vita pubblica all’interno della
comunità, e ogni utente registrato costruisce la propria reputazione sulla base
proprio di questa attività.

Ad un livello superiore si trovano gli amministratori, eletti secondo un
meccanismo piuttosto complicato che indica ad esempio le caratteristiche di
eleggibilità del candidato e la definizione di elettorato attivo. Attualmente
gli amministratori del progetto italiano sono 86 e oltre ad un numero
consistente di doveri, godono anche di poteri esclusivi. La gerarchia prevede
poi ulteriori ruoli di livello ancora più elevato (*burocrati, steward, check
user*), ricoperti da un numero molto ristretto di individui.

Questo discorso sulla gerarchia si collega al problema dell’accuratezza e
affidabilità delle voci, oltre che alla possibilità per chiunque di inserire
informazioni errate volontariamente. Esistono una serie di casi eclatanti che
spiegano meglio il problema e le sue conseguenze: nel 2005 la versione inglese
di Wikipedia è stata oggetto di una notevole illuminazione mediatica in
conseguenza della scoperta della falsificazione della biografia di John
Seigenthaler, giornalista molto noto negli Stati Uniti. Questo episodio ha dato
vita ad un acceso dibattito circa la capacità della comunità di Wikipedia di
tenere sotto controllo l'evoluzione dell'enciclopedia al crescere della
notorietà del progetto e del numero di utenti occasionali che, in buona o in
cattiva fede, contribuiscono a modificarlo.

In questo caso la reazione non fu sottoposta alla discussione collettiva e Jimbo
Wales, fondatore e presidente della Wikimedia Foundation, decise di cambiare
alcune regole. Da quel momento, agli utenti anonimi della versione inglese di
Wikipedia non fu più consentito aggiungere nuove voci. Inoltre Wales richiese
l'implementazione nel software wiki di nuove funzionalità di controllo riservate
ad alcune decine di amministratori esperti nominati direttamente da lui:

> Al momento di assumere rapidamente decisioni cruciali, insomma, Wikipedia si è
> rivelata tutt'altro che orizzontale o democratica, smentendo con questo non
> tanto il modo in cui essa presenta se stessa, quanto piuttosto le immagini
> entusiaste e ingenue con cui viene rappresentata dalle letture esterne più
> comuni.238

Sempre nel 2005, uno studio condotto dalla rivista Nature ha messo a confronto
precisione e accuratezza di Wikipedia e dell’Enciclopedia Britannica. Attraverso
il processo della “blind review”, i risultati mostravano un’accuratezza delle
voci di Wikipedia molto simile a quelli della prestigiosa enciclopedia.

In conclusione Wikipedia può essere considerata come uno dei migliori e più
convincenti esperimenti di raccolta e organizzazione collettiva della
conoscenza. Talvolta si parla di “intelligenza collettiva”, utilizzando un
concetto di Pierre Lévy (1996) il quale attribuisce all'attività comunicativa
sociale la capacità di generare e mantenere un'attività cognitiva di livello
superindividuale. In parte si tratta di una metafora, ma è innegabile che
determinati tipi di attività, come ad esempio quella scientifica, siano rese
possibili solo dalla collaborazione di più menti collegate dalla lingua e dai
suoi supporti. Nessuna mente singola è in grado di progettare e realizzare
esperimenti e ricerche di un elevato livello di complessità senza la possibilità
di cooperare con altri.239

Ma si può parlare anche di “intelligenza connettiva”, come fa De Kerckhove,
(1999), che l’autore stesso, in un’intervista rilasciata insieme allo stesso
Levy, definisce come

> Un sistema di connessione aperta. Non si trattava di riferirsi ad un
> contenitore chiuso, ma ad una connessione da persona a persona all'interno di
> una rete molto specifica. Questa connessione con la sua specificità che non
> sta nel contenitore collettivo di un sapere, di una conoscenza, di uno
> scambio, mi fu suggerito di chiamarla "connettiva". Questo concetto è
> formidabile per capire questi processi che la tecnologia digitale ha
> apportato, e mi ha permesso di scoprire l'intelligenza, o, meglio, l'inconscio
> connettivo ricco di possibilità. Continuo a prendere ispirazione dal lavoro di
> Levy e cerco di coinvolgerlo alla pratica diretta tramite l'intelligenza
> connettiva. Devo aggiungere che il concetto di intelligenza collettiva è così
> importante e fondamentale che merita di essere diviso in ulteriori zone
> d'esplorazione. Questo non elimina la possibilità di una ricerca parallela o
> interna all'interno di essa. Considero l'intelligenza connettiva in quanto una
> delle forme dell'organizzazione all'interno dell'intelligenza collettiva. Come
> Freud aveva trovato molto più interessante l'inconscio privato mentre Hume si
> era indirizzato verso l'inconscio collettivo, io mi trovo più interessato, per
> il mio lavoro, nell'esplorare sul campo, con le persone, in tempo reale.
> Preferisco la pratica dell'intelligenza collettiva nella sua rete specifica
> che chiamo intelligenza connettiva, piuttosto che lasciare semplicemente il
> concetto svilupparsi da solo senza sperimentazione. Amo lavorare con le mani.
> C'è un altro aspetto che mi appassiona. Una vecchia battuta di Molière in "Les
> femmes savantes" recita in questo modo: "Un gentiluomo è qualcuno che sa tutto
> senza avere imparato niente". Penso che con Internet, con il Web e con
> l'accesso che abbiamo a questa intelligenza collettiva, a questa base
> cognitiva, siamo tutti dei gentiluomini. Possiamo avere accesso a tutto senza
> avere imparato mai niente. Ciò è divertente, fa parte del piacere di
> appartenere della nostra epoca, di essere legati a questa formidabile memoria
> collettiva.240

L’importante è tenere sempre a mente che si tratta di un progetto portato avanti
con tenacia, ma anche con grandi difficoltà ed è ben lontano dall’essere quel
mondo di pace e fratellanza che spesso viene dipinto da alcuni media. Anzi, si
può affermare che parte del grande interesse generato da Wikipedia derivi dalla
sua complessità e dalla quantità e qualità di problemi che i suoi utenti sono
chiamati ad affrontare quotidianamente:

> Così come la vita di Wikipedia non è tutta rose e fiori, il passaggio a una
> società basata sulla conoscenza aperta non è scevro da difficoltà e fatiche
> che coinvolgono tanto la collettività quanto i singoli individui. Sul piano
> collettivo e delle politiche pubbliche la questione èevidentemente complessa e
> non può essere ridotta a facili dicotomie buono/cattivo. È in ogni caso
> auspicabile che si tenga costantemente presente la natura della conoscenza
> tecnica e scientifica come bene pubblico globale (Gallino, 2007),
> inconciliabile con quella di un bene materiale scarso.241

Sul piano individuale una società della conoscenza aperta richiede ai soggetti
che la abitano lo sviluppo di doti e requisiti specifici. Di fronte al
sovraccarico informativo che proviene da internet e dagli altri mezzi di
comunicazione vecchi e nuovi, le disuguaglianze sociali non riguardano tanto i
“ricchi” e i “poveri” di informazione, quanto piuttosto gli strumenti cognitivi
critici con i quali ognuno elabora le (molte) informazioni che acquisisce. In
questo senso, gli utenti di Wikipedia stanno già facendo pratica.

4  Attivismo in rete
====================

La storia dell’hacking ha avuto, fin dalla sua nascita, una serie infinita di
sviluppi in svariati campi. Si può rintracciare un comune denominatore soltanto
incrociando tre fattori: “Occuparsi di computer, usare il computer per
migliorare qualcosa, farlo in modo non convenzionale”.242 Queste tre
caratteristiche non sono comunque sufficienti per definire un’attività, e
proprio per questo al termine hacker viene sempre accostato qualcos’altro:
hacker del software, hacker dell’hardware, hacktivism, hacker art e molto
ancora.

Il concetto di “activism” si riferisce alle modalità di organizzazione e
propaganda tipiche dei movimenti politici di base (detti anche *grassroots
movements*)243, in particolare le forme d’azione diretta come i sit-in, i
cortei, il boicottaggio di merci, l’occupazione di stabili e strade:

> L'evoluzione delle forme dell'attivismo sociale e della militanza politica che
> presuppongono un uso efficace degli strumenti di comunicazione, e in
> particolare dei computer, ha nel tempo favorito l'adozione di idee e tecniche
> proprie della cultura hacker da parte dei movimenti ambientalisti e pacifisti,
> per i diritti umani e civili. Così dai volantini siamo passati alle petizioni
> elettroniche e dalle manifestazioni di piazza ai sit-in elettronici.244

L’esistenza della protesta politica popolare è una caratteristica di tutte le
comunità, che si tratti di spettacolari manifestazioni di piazza, o di riunioni
di associazioni locali. Questo tipo di attività nasce dall’esigenza di alcune
persone di incidere e controllare gli spazi e i tempi in cui vivono. Questo
desiderio e le restrizioni che da sempre lo accompagnano si è manifestato negli
spazi e tempi della vita virtuale sia in azioni per controllare il cyberspazio,
sia in azioni per controllare la vita “offline” attraverso il cyberspazio.

Quindi l’unione di "hacker" e "attivismo" in "hacktivism" significa
sostanzialmente un uso non convenzionale del computer, “finalizzato al
miglioramento di qualcosa di utile per il mondo con implicazioni sociali,
politiche o culturali”245. La definizione resta comunque insufficiente a
chiarire chi siano i soggetti coinvolti e soprattutto quali siano la rete di
relazioni e i conflitti tra più fattori sociali. Il computer e le reti, infatti,
non sono più solo mezzi di produzione, ma diventano lo strumento per realizzare
una comunicazione indipendente eludendo, ma anche sabotando, i simboli della
comunicazione dominante e per creare luoghi e strumenti per una comunicazione
libera, orizzontale e indipendente.

L’hacktivism è l’emergere di un’azione politica popolare, di auto-attività di
gruppi di persone nel cyberspazio. Si tratta di “una sorta di utopia anarchica,
una specie di ideologia dell'autogestione che sentiva di fare a meno dei
direttivi, delle federazioni, delle forme associative e politiche tradizionali:
la rete era partecipazione dal basso e comunicazione diretta senza filtri”246.
Non significa semplicemente “ogni politica associata al cyberspazio”247 perché
in questo caso chiunque potrebbe essere considerato un attivista digitale, visto
che oggi sono poche le attività politiche, sociali e culturali che in una forma
o in un’altra non coinvolgano anche forme di virtualità. 

Piuttosto, l’hacktivism che emerge dalla fine del ventesimo secolo, è un
fenomeno sociale e culturale specifico, in cui l’azione diretta delle politiche
popolari è stata tradotta e riportata nei mondi virtuali.248 Volendo ampliare la
riflessione, a partire dalla storia e dagli sviluppi della cultura hacker, si
può affermare che non è molto facile tracciare una linea netta che divida
l’hacking dall’acktivism. Ripercorrendo la storia, nel 1971 Habbie Hoffman
fondava la rivista *Youth International Pary Line* (poi rinominata *TAP*) in cui
le tecniche di *phone phreaking* erano funzionali alle proteste contro le
politiche del governo americano. 

TAP cessò le pubblicazioni nel 1984, anno in cui nacque un’altra rivista dello
stesso tipo dal nome 2600 (cioè la frequenza espressa in hertz del tono che i
centralini americani emettevano per dare via libera alle chiamate interurbane,
scoperta da Captain Crunch), il cui fondatore era noto come Emmanuel Goldstein.
Il nome d’arte si riferisce al celebre romanzo di George Orwell 1984, in cui
Goldstein era nemico del partito, oppositore del *Grande Fratello* e leader di
un’organizzazione segreta di resistenza. Già questa scelta rende l’idea
dell’impronta politica che intendeva avere la rivista.

In Germania, nel 1981, nasceva il Chaos Computer Club (CCC), gruppo di hacker
con l’obiettivo di affrontare direttamente le implicazioni politiche di uno
degli slogan degli hacker di prima generazione: “Tutte le informazioni devono
essere libere”. Il concetto di hackeraggio sociale può essere considerato
strettamente legato all’etica degli hacker delle prime generazioni: già negli
anni ’60, nei laboratori del MIT, nell’Homebrew Computer Club, personaggi come
Captain Crunch, Lee Felsenstein, Bill Gosper e Richard Stallman agivano per
consentire un utilizzo collettivo della tecnologia, al fine di una diffusione
generalizzata del sapere e dei mezzi di comunicazione. Hackeraggio sociale, in
questo contesto, è un modo per definire tali pratiche.

Il concetto assunse maggiore forza negli anni ’80, quando si sviluppò una
maggiore coscienza e volontà collettiva di agire nei processi sociali, definendo
obiettivi e metodi da realizzare per migliorare lo stato delle cose. Pratiche di
hackeraggio sociale in senso stretto (sabotaggi, incursioni, campagne che ebbero
rilevanza da un punto di vista sociale) furono poche anche in questi anni, ma se
si allarga la definizione del concetto alla più generale creazione di comunità
virtuali che avevano come obiettivo la diffusione più o meno legale di saperi,
competenze e strumenti digitali, o la rivendicazione dei diritti “cyber” e la
diffusione della protesta verso le nuove tecnologie della comunicazione e delle
sue motivazioni, allora gli anni ’80 assumono una certa rilevanza. In questi
anni, infatti, si è assistito alla crescita esponenziale di fanzine, riviste
cartacee, elettroniche e BBS su cui circolavano idee e proposte di nuovi modelli
tecnologici e comunitari.

Negli anni ’80 nacque anche il movimento cyberpunk, che ebbe il merito di
portare alla luce alcuni aspetti dell’etica hacker, contribuendo alla sua
diffusione tra persone inizialmente distanti dal mondo dell’informatica:

> Di contro però i media hanno approfittato per intessere colorate, quanto
> assurde, descrizioni dei comportamenti e delle finalità hacker basandosi su
> talune rappresentazioni estreme dei protagonisti della letteratura cyberpunk
> sfuggita al controllo dei loro stessi fondatori. La confusione tra finzione e
> realtà mentre produceva seduzione, creava anche confusione tra chi faceva
> hacking sociale e chi inseguiva mondi narrati e forniva ai media lo strumento
> per creare consenso intorno alla repressione.249

Tornando al Chaos Computer Club, il gruppo nasce con l’obiettivo di diffondere
la socializzazione dei saperi tecnologici, cioè diffondere gli strumenti per
permettere ad ogni individuo di creare autonomamente la propria conoscenza e
l’informazione.

Nel 1981 a Berlino avvenne il primo incontro del gruppo, in cui i membri
iniziano a discutere sulle potenzialità del computer e di hackeraggio sociale:
si resero conto, per esempio, che era possibile riuscire a determinare con
esattezza il consumo di energia elettrica di una città come Berlino, oppure fare
un censimento delle case sfitte e distribuire questi dati al movimento delle
occupazioni.

In un’intervista rilasciata alla rivista *Decoder* nel 1989, Wau Holland, uno
dei fondatori del CCC dichiara, a proposito di questi primi incontri:

> Si capisce che chi ha il potere in questa società trae parte di questo potere
> dall'elaborazione dei dati e che non solo la polizia o il potere possono
> utilizzare le banche dati, ma anche i movimenti. Nacque così la pratica
> dell’hacking che definirei come quella pratica che ti permette di essere
> dentro una situazione appena questa accade e da questa creare nuovi
> significati. Gli strumenti che ti permettono di fare questo sono tecnologici.
> […] Certo, per realizzare ciò è necessario fare un lavoro collettivo ed è su
> questa strada che si siamo mossi organizzando con regolarità dei meeting ed
> esercitazioni pratiche sui media.[…] La nostra attività principale si è
> sviluppata sui computer e sulle forme di hacking ad questi relative. Con
> questa pratica entri nelle bache dati, ti fai un giro, dai un’occhiata a
> quello che c’è e collezioni informazioni. Lo spirito con il quale entriamo è
> però di tipo comunitario, cioè prendiamo informazioni relativamente all’uso
> sociale che se ne può fare, facciamo dell’hacking sociale in sostanza. […] La
> nostra filosofia è una sola: “libertà”, ed in questa prospettiva cerchiamo di
> lavorare, attraverso lo scambio di idee sociali ed invenzioni sociali con le
> altre persone. Imparando da queste e insegnando loro ciò che noi sappiamo. E
> ricorda che ogni informazione è anche deformazione. E' come costruire una
> bottiglia partendo dal materiale grezzo e fuso: con le tue mani attraverso il
> processo di informazione tu dai una forma precisa a quel materiale che prima
> era non in forma e deformandolo otterrai la tua bottiglia, otterrai cioè uno
> strumento per scambiare idee.250

Nel 1984 il CCC *hackera* il servizio di comunicazioni Telebox BTX, realizzato
dalle poste tedesche in collaborazione con l'IBM, per dimostrarne la
vulnerabilità sia per quanto riguarda la sicurezza degli utenti, sia per la
sicurezza del sistema in sé. In quel periodo il governo tedesco stava
realizzando anche un progetto per il censimento informatico dei cittadini.
Trattandosi di un luogo in cui i cittadini scambiavano messaggi privati,
acquistavano e prenotavano prodotti, il CCC iniziò a chiedersi in che modo
venissero tutelati i dati e la sicurezza dei cittadini. Per dimostrare queste
vulnerabilità, i membri del CCC riuscirono ad entrare nel sistema e a farsi
accreditare una grossa somma di denaro.

Ovviamente l’azione era puramente dimostrativa, fu resa nota e i soldi furono
restituiti.  Il fine, infatti, era quello di sensibilizzare l’opinione pubblica
sul tema della sicurezza dei dati personali e ottenne come risultato quello di
costringere il governo tedesco a rivedere le sue politiche a riguardo.
L’attività del gruppo continuerà per molti anni e sarà documentata sulla rivista
*Datenschleuder*, nata nel 1984.

Per quanto riguarda l’Italia, il gruppo che più di ogni altro ha avuto la
capacità di promuovere la diffusione dell'etica hacker, attraverso un lungo
lavoro di teorizzazione, promozione, organizzazione ed azione diretta, è stato
il gruppo Decoder di Milano.

Nato nel 1986, il gruppo per tutta la seconda metà degli anni ’80 si focalizzerà
sull’idea dell’uso sociale delle reti telematiche. Tra i fondatori troviamo G.
"uVLSI" Mezza, E. "Gomma" Guarneri e R. "Valvola" Scelsi. Alcuni di loro avevano
vissuto in prima persona la nascita del movimento punk in Italia e tutti si
dedicano alla creazione della fanzine *Decoder*, oltre che al dibattito sulla
creazione di una rete telematica alternativa:

> Il gruppo di Decoder viene in contatto con la rete Fidonet attraverso A.
> Persivale (sysop del nodo milanese e terzo nodo italiano), con cui partono le
> prime ipotesi di un'area messaggi interna alla Fidonet. Persivale contatta al
> riguardo la bbs Mimax di Roma, The Doors di Prato e altre bbs della rete
> Fidonet, senza giungere però per il momento a far partire nessun progetto.
> Sarà in seguito attraverso tale circuito all'interno della rete Fidonet che,
> grazie principalmente agli sforzi del gruppo Decoder, nascerà l'area messaggi
> "Cyberpunk" nel 1991.251

Sempre nella seconda metà degli anni ’80 iniziano i contatti tra Decoder e le
altre esperienze europee, come il CCC, che saranno fondamentali per la
diffusione e la conoscenza di eventi come il meeting di hacker “Icata 89” i cui
principi etici saranno poi inseriti nell’ *Antologia cyberpunk* edita dallo
stesso gruppo di Decoder nel 1990.

Quella appena descritta non è che una minima parte, forse solo la nascita, di
una nuova concezione dell’hacking. Le evoluzioni dei gruppi appena citati,
quelli che sono nati successivamente, i personaggi, le nuove reti, le azioni, la
politica repressiva che ne è conseguita, gli arresti, i sequestri, hanno una
storia così lunga e complessa da non poter essere esplorata esaustivamente in
questo lavoro.

Ciò che piuttosto è opportuno sottolineare è che l'accesso a forme alternative
di informazioni e soprattutto la capacità di raggiungere il pubblico con
versioni dei fatti alternative a quelle ufficiali sono per natura processi
politici. Il cambiamento di forma e di livello nell'accesso alle informazioni
indica un cambiamento di forma e di livello nei rapporti di potere.

Il movimento cyberpunk e in generale tutta la scena controculturale, soprattutto
americana, hanno ricevuto nuovi spunti di dibattito e riflessione grazie agli
scritti di Hakim Bey, pseudonimo di Peter Lamborn Wilson, maestro sufi, artista
d’avanguardia, esperto di misticismo e uno dei pensatori più influenti nelle
correnti antagoniste degli anni ’90, soprattutto nell’area cyberculturale per le
sue peculiari visioni di dichiarata discendenza cyberpunk.252

Bey ha avuto il merito di riportare il dibattito ad un livello più profondo, in
un momento in cui le categorie più innovative e interessanti del cyberpunk
“stavano per essere sminuite per l’effetto perverso dell’amplificazione
mediatica e della banalizzazione che ne consegue”,253 soprattutto grazie al suo
saggio *T.A.Z. Zone temporaneamente autonome*, (1991). La definizione di una TAZ
è volutamente problematica, perché nasce per spiazzare, confondere e depistare i
mass media e la loro azione “normalizzante”, che riconduce a ordinario quello
che non lo era, per rendere innocua ogni azione contestatrice.

Volendo semplificare, si può considerare una TAZ come una sorta di isola, reale
o virtuale, libera dal dominio del sistema capitalista. Un’isola come quelle
usate nel XVIII dai corsari e pirati dei mari, che avevano creato una vera e
propria “rete d’informazione”, che copriva tutto il mondo, costituita da isole
in cui ci si poteva nascondere, rifornire, fare scambi e affari. Alcune di
queste isole sostenevano “comunità intenzionali”, cioè piccole società che
vivevano intenzionalmente al di fuori della legge e decise a rimanerci. Si
tratta di quelle che Bey definisce “utopie pirata”.

All’interno del saggio, Bey riprende i concetti e le differenze tra rivoluzione
e sollevazione: “Sollevazione, o la forma latina insurrezione, sono parole usate
dagli storici per etichettare rivoluzione fallite –movimenti che non si
conformano alla forma prevista, la traiettoria approvata dal consenso;
rivoluzione, reazione, tradimento, la fondazione di uno Stato ancora più
opprimente- il girare della ruota, il ritornare della storia ancora e ancora
nella sua forma più alta: stivale sulla faccia dell’umanità per sempre”.254

La sollevazione e l’insorgere permettono invece di uscire da questo circolo
vizioso e di alzarsi, sorgere, per prendersi cura di se stessi e dei propri
interessi. La Rivoluzione è qualcosa che cerca permanenza e durata, mentre la
sollevazione è qualcosa di temporaneo che lascia sempre un cambiamento profondo.
In quest’ottica, l’insurrezione si configura come una rottura sia dal punto di
vista temporale che istituzionale, non avendo la volontà di creare un nuovo
ordine sociale: “Se la Storia è Tempo come dice di essere, allora la
sollevazione è un momento che salta su e fuori dal Tempo, viola la Legge della
Storia. Se lo Stato è Storia, come dice di essere, allora l’insurrezione è il
momento proibito, un’imperdonabile negazione della dialettica”255. In questo
senso, la dialettica in questione “non è altro che quella tra Rivoluzione e
Stato, Stato interpretato come totem liberticida contro cui combattere tentando
di creare degli spazi alternativi ed antagonisti”256.

La TAZ non deve essere considerata come un fine esclusivo in sé che prenda il
posto di tutte le altre forme di organizzazione; può però procurare una forma di
arricchimento se associata alla sollevazione senza comportare uno scontro
diretto con lo Stato: si tratta di una sommossa che libera temporaneamente
un’area (che riguarda una porzione di terra, di tempo o di immaginazione) che si
dissolve prima che lo Stato la possa schiacciare. Queste aree possono essere
clandestinamente “occupate” e agire indisturbate anche per parecchio tempo
grazie all’anonimato: “Appena la TAZ è nominata (rappresentata, mediata) deve
svanire, svanirà, lasciandosi dietro una corteccia vuota, solo per poi saltare
fuori ancora da qualche parte, ancora una volta invisibile perché indefinibile
in termini dello Spettacolo”.257

È una tattica perfetta per questa era in cui lo Stato sembra essere onnipresente
e onnipotente, ma allo stesso tempo è pieno di spazi vuoti e di crepe.
Abbandonando il concetto di rivoluzione in favore di quello di sollevazione,
bisognerà realizzare quest’ultima quanto più spesso è possibile, anche a rischio
della violenza. La tattica migliore e più radicale resterà comunque quella di
evitare il coinvolgimento in atti di violenza spettacolare, per ritirarsi
piuttosto dall’area di simulazione e scomparire.

Oltre al concetto di insurrezione, una seconda forza della TAZ emerge dallo
sviluppo storico che Bey definisce “la chiusura della mappa”258. Dal 1899 non
esistono territori che non siano di proprietà di una nazione/stato, non esistono
più terre incognite: “Ma più la precisione cartografica si avvicina a
rappresentare dettagliatamente il territorio, più sono numerosi gli ambiti
sociali che sfuggono al controllo”259. La “mappa” è quindi una griglia politica
astratta, che non può coprire la terra con una precisione 1:1 perché il
territorio da “controllare” è identico a quello della mappa. Di conseguenza,
esisteranno sempre degli spazi che abbiano potenziale per svilupparsi come TAZ. 

I due aspetti appena descritti rappresentano le fonti negative della TAZ, ma “la
Reazione non può produrre l’energia necessaria a “manifestare” una TAZ. Una
sollevazione deve pur essere per qualcosa”260. Quindi, si introduce il concetto
di antropologia naturale della TAZ che non si fonda sulla famiglia nucleare,
unità base della società del consenso, ma su un più elementare e al contempo
radicale modello costituito dalla banda. La banda tipica,
cacciatrice/raccoglitrice nomade o semi-nomade, consiste in un gruppo di circa
50 persone ed è costituita dall’apertura, non a chiunque, ma al gruppo di
affinità, agli iniziati legati da un patto d’amore. La banda non si trova
all’interno di una gerarchia più ampia, ma fa parte di un modello orizzontale di
costume, affinità spirituale, parentela estesa, ecc.

Per spiegare il secondo aspetto che associa la TAZ al concetto di festival, Bey
riprende un concetto usato da Stephen Pearl Andrews in *The Science of Society*
(1852): il pranzo è una buona immagine della società anarchica, in cui tutte le
strutture di autorità si dissolvono nella celebrazione e l’individualità di
ciascuno è ammessa. Le feste sono già degli spazi liberati, delle potenziali
TAZ. Sono sempre “aperte” e “non ordinate”; possono essere preventivamente
progettate, ma quando si realizzano sono sempre caratterizzate dall’elemento
della spontaneità. Secondo Bey, trattandosi di un gruppo di individui che si
incontrano per realizzare desideri comuni che possono andare dal bere e mangiare
al creare un lavoro artistico comune, si tratta già di una forma elementare di
TAZ, o anche di una forma semplice di “unione degli egoisti” che richiama quanto
detto in apertura di questo lavoro su Max Stirner.

Si realizza così il “nomadismo psichico”: non solo artisti o intellettuali, ma
anche lavoratori migranti, rifugiati, turisti, chi viaggia nella rete senza
lasciare mai la propria stanza. Si può affermare che “questi nomadi praticano la
razzia, sono corsari, sono virus, hanno bisogno e voglia di TAZ, campi di tende
nere sotto le stelle del deserto, interzone, zone fortificate nascoste lungo
carovaniere segrete, parti di giungla e di pianure “liberate” , aree proibite,
mercati neri e bazar sotterranei”.261

A questo punto viene introdotto il concetto di Rete, definito come “la totalità
di tutto il trasferimento di informazione e comunicazione”.262 Alcuni
trasferimenti sono privilegiati e spettano solo ad alcune categorie (ad esempio
i dati militari), il che conferisce alla Rete un aspetto gerarchico; la maggior
parte resta però aperta a tutti, conferendo ad essa anche un aspetto
orizzontale. All’interno della Rete si è sviluppata una Contro-Rete, definita
“Tela”, costituita da una struttura aperta alternativa orizzontale di scambio
informatico. Il termine di Contro-Rete continuerà ad indicare l’uso ribelle e
illegale della Tela, come l’haking: “Rete, Tela e Contro-Rete sono tutte parti
dello stesso intero modello complesso. Si confondono l’una con l’altra in
innumerevoli punti”.263

Il rapporto tra Tela e TAZ non è solo di supporto logistico: a causa del suo
carattere temporaneo, alla TAZ necessariamente mancheranno delle libertà
connesse a una durata e una località più o meno fissa, ma in questo caso la Tela
può agire in sostituzione di tali mancanze attraverso la diffusione di tempo e
spazio “compattati” e trasformati in dati. Il supporto si realizza attraverso la
possibilità di trasmettere informazioni da una TAZ all’altra, di renderle
invisibili se necessario, o conferendo degli strumenti di difesa. La Tela non ha
bisogno, per la sua esistenza, di nessuna tecnologia-computer: sono sufficienti
la posta, il passaparola, le fanzine a creare un’intelaiatura d’informazione.
L’importanza non è data dal tipo o il livello di tecnologia necessaria, ma
dall’apertura e orizzontalità della struttura.

Il concetto di TAZ non può essere esclusivamente legato al cyberspazio, il
computer resta un mezzo:

> La TAZ per sua stessa natura si impossessa di ogni mezzo ottenibile per
> realizzarsi – verrà alla luce sia in una caverna sia una città Spaziale – ma
> soprattutto vivrà, ora o appena possibile[…] Userà il computer, perché il
> computer esiste, ma userà anche poteri che sono così completamente dissociati
> dall’alienazione o dalla simulazione, da garantire un certo paloliticismo
> psichico alla TAZ, uno spirito sciamanico-primordiale che “infetterà” anche la
> rete stessa.264

4\.1 Anonymous
--------------

Le vicende accadute negli ultimi anni nei Paesi nordafricani che affacciano sul
Mediterraneo, manifestazioni e contestazioni come quelle della Tunisia o in
piazza Tahrir in Egitto, in Medioriente, nei Paesi del golfo Persico, hanno
tutte in comune un aspetto *“social”*.265 Twitter, facebook, Tumblr, ma anche
piattaforme locali hanno giocato un ruolo fondamentale nell’organizzazione di
mobilitazioni rapide e imprevedibili, ma soprattutto hanno avuto un ruolo di
sensibilizzazione verso l’esterno. Il loro contributo particolare è che esso
aiutavano a far capire cosa stesse succedendo.

In tempi ancora più recenti, in Turchia con *#Occupygezy*, oltre all’aspetto
informativo, “le stesse piattaforme sono servite invece come piattaforme di
coordinamento e scambio d’informazioni. Per costruire una rete fra chi, magari,
non era mai sceso in piazza”266. Dopo tre anni di mobilitazioni, si può
considerare la nascita di una nuova forma di protesta che, secondo Zeynep
Tufekci, studiosa di social media, membro del Center for Information Technology
Policy di Princetown e docente alla University of North Carolina, Chapel Hill, è
caratterizzata da otto elementi comuni, elencati nel suo blog,
http://www.technosociology.org. In particolare, l’attenzione è focalizzata su
Egitto, Tunisia, il Movimiento 15-M degli *indignados* spagnoli, Grecia e
Turchia.

Il primo aspetto è la mancanza di una leadership organizzata. Questo rende
difficile la negoziazione per dare uno sbocco alla protesta: “D’altra parte
significa anche che il movimento, molto spesso, non è in grado di guadagnare più
di tanto dalle proprie mobilitazioni”267.  Inoltre, spesso manca una controparte
istituzionale. Con il fallimento della politica di opposizione e dei media
mainstream, i manifestanti protestano per la mancanza di luoghi in cui
manifestare il dissenso. Ad esempio nel caso dell’Egitto, perché le elezioni
erano state manipolate e la vera politica eliminata.

Un terzo aspetto riguarda la partecipazione di non-attivisti a questo tipo di
manifestazioni. Prima del 2010, ad esempio in Turchia, molte grandi
mobilitazioni erano state promosse e realizzate da attivisti che partecipavano
spesso a queste proteste. Le proteste tunisine nel dicembre 2010, quelle di
piazza Tahrir nel 2011 e le ultime a Gezi Park hanno invece coinvolto un
altissimo numero di non-attivisti. Si tratta cioè di persone che non erano mai
scese in piazza.

Quarto aspetto è definito da Tufekcy come la rottura dell’ignoranza
pluralistica: “Significa la fine dell’idea che si sia da soli, gli unici a
pensarla in un certo modo, ad avere un certo punto di vista”.268 Le
dimostrazioni in strada sono dunque esse stesse una forma di social media:
lanciano agli altri cittadini un segnale di solidarietà, danno l’idea che non si
è isolati nelle proprie rivendicazioni: “ Il meccanismo, dunque, può essere
digitale o offline: l’importante è capire quanto è sociale, cioè visibile”269.

Le strutture esistenti dei social media facilitano l’organizzazione collettiva
delle masse intorno a tematiche che sono quasi sempre d’opposizione. Più che
posizioni politiche nuove, si costruisce una contrarietà. Quest’aspetto è
strettamente legato ad un altro, cioè alla difficile azione politico-strategica
successiva: molto spesso l’azione è mirata esclusivamente all’opposizione, ad
esempio ad un tipo di politica, e non riesce ad andare oltre.

I social media sono in grado di bypassare la censura e sono in grado di
catturare l’attenzione globale. Ad esempio, com’è successo in Egitto:
 
> Questa saggezza collettiva, pari all’abilità tecnica, si è messa in luce anche
> nei momenti più duri e violenti della repressione. Più grandi e numerose erano
> le provocazioni, più si moltiplicavano i messaggi e il lavoro di
> informazione/documentazione/sollecitazione che portava ancora più gente il
> giorno dopo in piazza Tahrir. A nulla è valso il blocco di internet imposto
> dal governo egiziano, aggirato dai Twippers grazie ai numeri di telefono messi
> a disposizione da Google e dall’immediata traduzione retwittata a cura degli
> studenti del MIT di Boston270.

Infine, l’ultimo aspetto riguarda i social media come strutture narrative. Nel
caso turco e in molte altre proteste le piattaforme social permettono agli
attivisti di costruire veri e propri  racconti partecipativi: “Le storie che
raccontiamo sulla politica e sui politici sono molto importanti nella formazione
delle stesse politiche future. I social media hanno aperto un nuovo e complicato
percorso-romanzo in cui meta-narrazioni su azioni politiche emergono e si
fondono in continuazione”271. Il social, insomma, permette di fornire la propria
versione, costruendo in tempo reale una visione del fenomeno in corso.

Una forma particolare di hacktivism, che negli ultimi anni gode di grande
visibilità, è senza dubbio quella di Anonymous. Non si tratta di
un’organizzazione formalmente costituita e in questo senso essa non esiste. È un
modo per indicare persone che agiscono in modo anonimo con un obiettivo comune.
Le difficoltà di fornire una definizione del fenomeno derivano dal fatto che
Anonymous è, come suggerisce il nome, volutamente avvolta nel mistero. Non ci
sono leader, nessuna struttura gerarchica, nessun epicentro biografico.
Tuttavia, come osserva Gabriella Coleman, “mentre ci sono forme di
organizzazione e di logiche culturali che innegabilmente plasmano le sue
molteplici espressioni, resta un nome che qualsiasi individuo o gruppo può
adottare come proprio”.272

In principio c’era un’immageboard, http://www.4chan.org, cioè un sito internet
basato sulla condivisione di immagini da parte di utenti sulle quali poi
iniziare una discussione. Questo tipo di sito permetteva agli utenti di
pubblicare immagini e commenti in forma anonima. Il nick "Anonymous" veniva
quindi assegnato ai visitatori che lasciavano commenti senza identificarsi.

4chan nasce nel 2003 ad opera di Chirstopher Poole, 15 anni, che crea la bacheca
con una sola regola: niente pedo-pornografia. Il sito diventa subito un richiamo
di massa per i *troll*, ovvero “persone che interagiscono con gli altri utenti
tramite messaggi provocatori, irritanti, fuori tema o semplicemente senza senso,
con l'obiettivo di disturbare la comunicazione e fomentare gli animi”273. Questi
troll iniziano ad organizzare operazioni di massa scegliendo di chiamarsi
*Anonymous*, utilizzando cioè il nick di chi non sceglie il nick: “Anonymous è
un'entità composta da diversi utenti anonimi con un unico modo di pensare”.274

Il 12 luglio del 2007 si realizza uno dei primi imponenti attacchi di questo
gruppo ad un gioco multiplayer online, “Habbo Hotel”. Ad un orario prestabilito,
gli utenti si registrarono al sito di Habbo, selezionando tutti lo stesso
avatar: un uomo di colore in smoking grigio e acconciatura afro, entrarono nella
piscina virtuale e ne bloccarono l'accesso, dichiarando che era “chiusa per
AIDS”, inondando il sito con frasi stupide prese dalla rete che si disponevano
in modo da formare una svastica. Quando gli autori degli attacchi vennero
espulsi, si lamentarono di razzismo. L’attacco fu una reazione a quanto successo
poco prima in un parco di divertimenti in Alabama, in cui fu vietato ad un
bambino di due anni affetto da AIDS di immergersi in piscina. L’aspetto da
sottolineare è che questo tipo di attacco nasceva ancora e principalmente dalla
voglia di divertimento, l’azione era ancora guidata dalla voglia di essere e
fare i “troll”.

Nello stesso anno, fu attaccato il sito web di un conduttore radiofonico, Hal
Turner, sostenitore della supremazia della razza bianca e, attraverso la tecnica
del “*pedobaiting*”, fu denunciato e arrestato un molestatore canadese, sempre
ad opera di Anonymous. Alcuni membri, detti Anons, iniziavano quindi a vedere se
stessi come uno strumento di “supporto” alla giustizia. La svolta in questo
senso arriva nel 2008, quando all’interno del movimento si crea una prima
scissione: da una parte i “moralisti”, dall’altra quelli che vogliono solo
divertirsi: “Sono venuto per il *lulz*, sono rimasto per l’indignazione”275,
questa la dichiarazione di un anons rilasciata alla scrittrice Gabriella
Coleman, antropologa, autrice di molti lavori che si focalizzano sulla cultura
hacker e l’hacktivism. Il *lulz*, cioè fare una cosa per il gusto di farla, non
è scomparso: gli attivisti continuano ad impegnarsi per coniugare la
tradizionale protesta agli elementi più selvaggi, gretteschi e offensivi che
sono parte integrante del *lulz*.

Nel 2008 nasce il progetto Chanology, nato in risposta ai tentativi della chiesa
di Scientology di rimuovere da internet, nello specifico da Youtube, del
materiale proveniente da una nota intervista rilasciata da un suo membro,
l'attore Tom Cruise, nel gennaio 2008, per violazione del copyright.
Considerando l'azione di Scientology una forma di censura, i membri del Progetto
Chanology organizzarono una serie di attacchi contro i siti di Scientology,
scherzi telefonici e fax neri ai suoi centri.

Il 21 gennaio 2008, degli individui, dichiarandosi membri di Anonymous,
annunciarono i loro obiettivi e le loro intenzioni tramite un video pubblicato
su YouTube intitolato “Message to Scientology”, insieme ad un comunicato stampa
con il quale dichiaravano “Guerra a Scientology”. Nel comunicato, il gruppo
affermava che gli attacchi contro Scientology sarebbero continuati allo scopo di
proteggere il diritto alla libertà di parola, e mettendo fine a quello che era
visto come uno sfruttamento dei membri della chiesa.

Un nuovo video “Call to Action” apparve su YouTube il 28 gennaio 2008,
annunciando delle proteste davanti alla chiesa Scientology per il 10 febbraio
2008. Il 2 febbraio 2008, 150 persone si riunirono fuori dalla chiesa di
Scientology in Florida, per protestare contro le pratiche dell'organizzazione.
Altre piccole proteste si organizzarono in California e Inghilterra. Il 10
febbraio 2008, circa 7000 persone manifestarono in più di 93 città in tutto il
mondo. Molti dei manifestanti indossavano le maschere del personaggio *V* dal
film *V per Vendetta* (che a sua volta ha preso ispirazione da Guy Fawkes276),
oppure camuffati in altro modo, in parte per proteggersi da eventuali vendette
da parte della chiesa.

La difesa della libertà digitale in tutte le sue forme diventa lo scopo
principale d’azione del gruppo. Pur non essendo, come già detto, un “gruppo
formale”, ha comunque una struttura: “È costituito da un nucleo centrale, esteso
e democratico, che si occupa del coordinamento. E poi c’è una folta schiera di
supporter, se li possiamo chiamare così, che in base alle decisioni prese dal
nucleo, sceglie se entrare in gioco e far parte delle varie missioni”277. Questi
contatti avvengono a su piattaforme di comunicazione internazionale come
http://www.anonops.com, (Anonymous Operation), che sono delle reti IRC. Queste
reti, sostanzialmente delle chat, hanno un loro funzionamento: esistono degli
utenti che hanno maggiore autorità, investiti di potere infrastrutturale. Questi
“amministratori” hanno il compito di mantenere l’ordine nei canali, possono
temporaneamente o definitivamente “bannare”, cioè cacciare, un utente se questo
viola norme di comportamento o infastidisce. Per essere un “Op” non occorrono
particolari abilità tecniche e, anche se il loro giudizio ha un peso maggiore
durante i numerosi dibattiti che si svolgono in queste reti, non determinano il
corso di ogni azione o operazione. Alcuni sono lì semplicemente per fornire
supporto infrastrutturale, altri anche impegnarsi in molte delle operazioni
politiche.

Una missione tipica di Anonymous consiste di tre fasi. La prima, durante la
quale si stabilisce, come detto, l’obiettivo. La seconda, in cui si annuncia a
tutti i componenti del gruppo, di solito con un tweet o dei messaggi in apposite
bacheche elettroniche. Infine si attua il piano. 

Questa strategia permette di richiamare all’ordine un’enorme massa di seguaci,
con preavvisi anche brevi, ma allo stesso tempo è anche un rischio perché un
annuncio “pubblico” può essere facilmente intercettabile. È importante
raggiungere il maggior numero di persone possibile perché buona parte degli
attacchi di Anonymous si basano sul *Distributed Denial of Service* (DDoS), cioè
l’invio di una grossa quantità di dati a un sito, al fine di saturarne le
connessioni e renderlo inaccessibile: “Il gruppo di hacktivisti sostiene che gli
attacchi *Distributed Denial of Service* siano pratiche di protesta simili a
quelle messe in atto dal movimento Occupy: gli hacktivisti impediscono l'accesso
ai siti Web presi come bersaglio nello stesso modo con cui gli attivisti
ostacolano l'ingresso agli edifici”278.

In un’analisi della situazione italiana, Carola Frediani cerca di fare anche un
“identikit” dei membri di Anonymous, perché 

> anche se sono pochissime le informazioni personali divulgate dagli anon, è
> evidente, e lo riconoscono loro stessi, che qui ci sta una generazione. Tra i
> 16 e i 30 anni. Studenti, qualcuno lavora e qualcuno no, passano le serate
> online invece che in discoteca. Hanno molte qualità e poche risorse. Hacker,
> attivisti, ragazzini, curiosi; ma anche disoccupati, qualche cinquantenne,
> qualche elemento borderline. Sparsi dal Nord al Sud Italia. Eccentrici
> geograficamente o culturalmente. Più collaborativi che competitivi. Più cani
> sciolti che inquadrati ideologicamente. Caratterialmente miti, socialmente
> arrabbiati. Un’ armata brancaleone che comunque porta a casa risultati a volte
> clamorosi, una realtà troppo spesso giudicata con supponenza dalla comunità
> tecnologica e da  vecchi, si fa per dire, attivisti.279

Un nuovo punto di svolta è rappresentato dall’iniziativa *OpTunisia*. Il 2
gennaio 2011, moltissimi siti del governo tunisino furono sottoposti ad una
serie di attacchi *Ddos* ad opera di Anonymous a causa della censura dei
documenti di Wikileaks e a sostegno delle proteste dei tunisini tra il 2010 e il
2011. Oltre agli attacchi ai siti, furono anche divulgati video che
testimoniavano le violenze che stavano accadendo per le strade tunisine e creati
pacchetti per cyberactivisti tunisini e manifestanti che fornivano informazioni
per eludere la sorveglianza governativa.

Secondo Gabriella Coleman, OpTunisia ha rappresentato un altro punto di svolta
nella formazione politica di Anonymous come un movimento di protesta.
Considerando che la maggior parte delle operazioni precedenti riguardavano
principalmente Internet o censure, questa operazione si trasferisce esattamente
nel’attivismo per i diritti umani in quanto convergenti con un movimento sociale
esistente”280. A partire da questo momento, moltissime saranno le operazioni di
questo tipo: dopo la Tunisia, l’attenzione si è spostata in Egitto, Libia, Nuova
Zelanda, Italia.

Quello che si può sottolineare, in conclusione, è che Anonymous offre discrete
possibilità di micro-protesta che non sono altrimenti presenti in un modo che
consenta alle persone di far parte di qualcosa di più grande. Non è necessario
compilare un modulo con i dati personali, non si chiede di inviare denaro, non
c’è bisogno di dare il proprio nome, ma si avrà la sensazione di essere parte di
qualcosa di più grande. La decisione di intraprendere l'azione politica deve
accadere in qualche modo, attraverso un percorso concreto di azione, una serie
di eventi, o di influenze. Anonymous, per molti, è proprio quel percorso.

Note
====

1. STIRNER (2002).
2. FERRI (1993), p. 73.
3. GOFFMAN, JOY (2004).
4. LEVY (1996).
5. LEVY (1996), p. 40.
6. CARETTONI, LANIADO (2005), p. 6.
7. http://www.fsf.org
8. IPPOLITA, (2005), p. 33.
9. IPPOLITA (2005), p. 42.
10. http://www.fsf.org/it-it/about-ita
11. IPPOLITA (2005), p. 44.
12. GUBITOSA (2005), p. 34.
13. MONCERI, (2011), p. 144.
14. MONCERI, (2011), p. 145.
15. ALVANINI, (2009), p.389.
16. LESSIG (2005), p. 17. 
17.
Per un approfondimento sugli obiettivi, si può consultare http://creativecommons.org/about
18.
Il testo completo è disponibile all’indirizzo http://www.gnu.org/encyclopedia/free-encyclopedia.it.html
19. STALLMAN (1999), p. 1.
20. PACCAGNELLA (2007), p. 1.
21. GUBITOSA (1999), p. 9.
22. JORDAN, TAYLOR (2004), pp. 8-9.
23. JORDAN, TAYLOR (2004).
24. BEY (1993), pp. 12-13.
25. Ibidem, p. 13.
26. D’ORAZIO (2012), p. 208.
27. Ibidem p. 24.
28. Ivi
29. COLEMAN (2011).
30. SCIANNAMBLO (2013).
31. COLEMAN (2011).
32. HUNEKER, (1907).
33. PENZO, (1971), p. 18.
34. CARUS, (1911), p. 377.
35. CARUS, (1911), p. 378.
36. SIGNORINI (1994), p.9.
37. PENZO (1993), P.8.
38. PENZO (1971); (1996).
39. BERTO (1997), P.174.
40. FEITEN (2013), p. 118.
41. WOODCOCK (1980), p. 6.
42. Ivi
43. Ibidem, p. 7.
44. Ibidem, p. 8.
45. WOODCOCK (1980), p. 9.
46. WOODCOCK (1980), p.11.
47. FEITEN (2013), p. 119.
48. Ibidem, p.120.
49. Ibidem, P. 122.
50. Ivi
51. Ibidem, p. 127.
52. FEITEN (2013), p. 127.
53. PENZO (1971), p. 58-59.
54. Ivi
55. BERTO (1997), p. 174.
56. SIGNORINI (1994), p.10.
57. SIGNORINI (1994), p. 46.
58. ivi
59. FERRI (1993), P.73
60. ivi
61. FERRI (1993), p. 73.
62. FERRI (1993), p.75.
63. D’AMBROSIO (2006), p. 8.
64. FERRI (1993), pp.79-80.
65. STIRNER (2002), p.330.
66. WELSH (2010), p. 108.
67. PENZO (1971), p. 315.
68. STIRNER (2002), p.330.
69. WELSH, (2010), p.110.
70. SIGNORINI (1994), p. 163.
71. FERRI (1993), p.88.
72.
Stanford Encyclopedia of Philosophy, Max Stirner http://plato.stanford.edu/entries/max-stirner/
73. STIRNER (2002), p. 323.
74. FERRI (1993), p.89.
75. STIRNER (2002), p. 331-332.
76. Ivi.
77. FERRI (1994), p. 91.
78. FERRI (1994), p. 92.
79. PENZO (1994), p.21.
80. Ibidem, p. 22.
81. STIRNER (2002), p.189.
82. WELSH (2010).
83. Ibidem, p. 95.
84. Ibidem, p. 96.
85. STIRNER (1983), P.113.
86. PASSAMANI (1993).
87. Ibidem, p.163-164.
88. Ibidem, p. 164.
89. STIRNER (2002), p.325-326.
90. STIRNER (2002), p. 51-52.
91. Ivi
92. PASSAMANI (1993), p.167.
93. STIRNER (2002), p.272.
94. PASSAMANI (1993), p.168.
95. SIGNORINI (1994), p. 183.
96. PASSAMANI (1993), p.171.
97. Ibidem, p.173.
98. SIGNORINI (1994), p. 187.
99.
Enciclopedia online Treccani, http://www.treccani.it/enciclopedia/controcultura/
100. GOFFMAN, JOY (2004).
101. Ibidem, pp. 10-11.
102. DURANTE (2008), p 13.
103. GOFFMAN, JOY (2004), p. 35.
104. ROSZAK, (1975).
105. HEBDIGE (1979).
106. D’ORAZIO (2012), p. 69.
107. MEYROWITZ (1975).
108. D’ORAZIO (2012), p. 74.
109. McLUHAN (1967);(1976).
110. D’ORAZIO (2012)
111. Ibidem, p. 95.
112. Ibidem, p. 98.
113. LEARY (1995, 1996).
114. Maggiori approfondimenti nel prossimo capitolo.
115. BRAND (1972). L’articolo completo si può consultare all’indirizzo
http://www.wheels.org/spacewar/stone/rolling_stone.html
116. GOFFMAN, JOY (2004), P. 406.
117. CARETTONI, LANIADO (2005), p. 6.
118. http://it.wikipedia.org/wiki/Hacking
119. IPPOLITA (2012).
120. LEVY (1996)
121. Ibidem, p.22.
122. CARETTONI, LANIADO (2005), p. 4.
123. CARETTONI, LANIADO (2005). P. 5
124. MASTROLILLI, (2001).
125. WILLIAMS (2003), p. 25.
126. WILLIAMS (2003), p. 26.
127. Ivi
128. GOFFMAN, JOY (2004), pp. 406- 407.
129. LEVY (1996), pp. 40-46.
130. Al concetto di open source è dedicato il prossimo paragrafo.
131. http://www.dizionarioinformatico.com/cgi-lib/diz.cgi?frame&key=opensource
132. CARETTONI, LANIADO (2005), p. 5-6.
133. CARETTONI, LANIADO (2005), p. 6.
134. MASTROLILLI, (2001), p. 57.
135. Ibidem, p. 60.
136. GOFFMAN, JOY (2004), p. 409.
137. MASTROLILLI, (2001), p. 58.
138. MASTROLILLI, (2001), p. 60.
139. IPPOLITA (2005), p. 29.
140. CARETTONI, LANIADO (2005), p. 6.
141. MASTROLILLI, (2001), p. 62.
142. WILLIAMS (2003), p. 96.
143. Ivi
144. FEATHERSTONE, BURROWS (1999), p. 22.
145. CARONIA, GALLO (1997).
146. Ibidem, p. 8.
147. Ivi
148. PROIETTI (1998).
149. CARONIA, GALLO (1997), p. 16.
150. IPPOLITA, (2005), pp. 26-27.
151. Ibidem p. 27.
152.
Il testo integrale si può trovare su “EraCyberpunk - La comunità italiana del cyberpunk”, (http://eracyberpunk.altervista.org)
153. GUBITOSA, (2007).
154. http://www.gnu.org
155. http://www.fsf.org
156.  IPPOLITA (2005), p. 30.
157. PIANA, ALIPRANDI (2012), p. 80.
158. IPPOLITA (2005), p. 31.
159. ALIPRANDI, (2005), p.35.
160. Ivi
161. WU MING (2003).
162. IPPOLITA (2005), p. 32.
163. Ibidem pp. 32-33.
164. RAYMOND, (2007).
165. IPPOLITA, (2005), p. 33.
166. ALIPRANDI, (2005), p. 22.
167. Ibidem, p. 23.
168. WILLIAMS (2003), p. 74.
169. ALIPRANDI, (2005), p. 24.
170. http://www.fsf.org/it-it/about-ita
171. GUBITOSA, (1999).
172. LESSIG, (2005) p. 9.
173. Ibidem, p. 10.
174. GUBITOSA (2005), p. 9.
175. Ibidem, p. 28.
176. GUBITOSA (2005).
177. THOMAS, (2005).
178. Ibidem, p. 600.
179. LESSIG, (2005), p. 7.
180. Ivi
181.
Per maggiori informazioni: http://www.wto.org/english/thewto_e/whatis_e/tif_e/fact5_e.htm
182. LESSIG, (2005), p. 10.
183. GUBITOSA (2005), p. 9.
184. BECCARIA (2006).
185. TUCKER (2000).
186. JEFFERSON, (1813).
187. BECCARIA (2006), p.16.
188. PIETROBON, TESSAROLO (2011).
189. LESSIG, (2005), p. 17.
190.
Il testo originale si può consultare all’indirizzo http://www.phrack.org/issues.html?issue=7&id=3#article
191. GUBITOSA (2005), p. 34.
192. GUBITOSA (2005), p. 35.
193. MONCERI, (2011).
194. Ibidem, p. 138.
195. Ibidem, p. 139.
196. MONCERI, (2011), p. 144.
197. Ibidem, p. 145.
198. ALIPRANDI, (2005), p. 85.
199. Ibidem, p. 80.
200. ALVANINI, (2009), p.389.
201.
Per un approfondimento sugli obiettivi, si può consultare http://creativecommons.org/about
202. LESSIG (2005), p. 135.
203. http://www.creativecommons.it/Licenze/Spiegazione
204. ALVANINI, (2009), p. 390.
205. ALIPRANDI, (2008), p. 35.
206. ALVANINI, (2009), p. 391.
207. ALVANINI, (2009), p. 391.
208. ALIPRANDI, (2008), p. 32.
209. Per maggiori informazioni http://www.debian.org
210. ALVANINI, (2009), p. 392.
211. http://ocw.mit.edu/about/
212. ALVANINI, (2009), p. 392.
213. INNIS, (1982; 2001).
214. MONACI (2007), p. 151.
215. Ivi
216. CASTELLS (2008).
217. MONACI (2007), p. 152.
218. CASTELLS, (2007). pp. 1-2.
219. CASTELLS, (2008), p. 47.
220. Ibidem, p. 411.
221. MONACI (2007), p. 155.
222. MONACI (2007), p. 155.
223.
Il testo completo è disponibile all’indirizzo http://www.gnu.org/encyclopedia/free-encyclopedia.it.html
224. STALLMAN (1999), p. 1.
225. Voce Wikipedia, http://it.wikipedia.org/wiki/Pagina_principale
226. PACCAGNELLA (2007), p. 1.
227. LANZARA (2011), p. 362.
228. MONACI (2007), p. 162.
229. PACCAGNELLA (2007), p. 3.
230. Ibidem, p. 4.
231. PACCAGNELLA (2007), p. 5.
232. MONACI (2007), p. 165.
233.
I dati aggiornati si trovano all’indirizzo http://stats.wikimedia.org/IT/TablesWikipediansEditsGt5.htm
234. PACCAGNELLA (2007), p. 8.
235. http://it.wikipedia.org/wiki/Wikipedia:Cinque_pilastri?match=zh
236. PACCAGNELLA (2007), p. 7.
237. PACCAGNELLA (2007), p. 9.
238. PACCAGNELLA (2007), p. 12.
239. LONGO (2009). p. 16.
240.
Il testo completo dell’intervista è disponibile all’indirizzo http://www.mediamente.rai.it/home/bibliote/intervis/d/dekerc05.htm
241. PACCAGNELLA (2007), p. 16.
242. CORINTO, TOZZI (2002).
243. CORINTO, TOZZI (2002). P. 7.
244. CORINTO, TOZZI (2002). P. 7.
245. CORINTO, TOZZI (2002).
246. GUBITOSA (1999), p. 9.
247. JORDAN, TAYLOR (2004), pp. 8-9.
248. JORDAN, TAYLOR (2004).
249. CORINTO, TOZZI (2002). P. 152.
250.
Il sito Decoder non è più disponibile online, l’intervista è riportata in ZICCARDI (2009). p. 273.
251. CORINTO, TOZZI (2002). P. 155.
252. D’ORAZIO (2012), p. 206.
253. BEY (1993), p. 7.
254. Ibidem pp. 12-13.
255. Ibidem, p. 13.
256. D’ORAZIO (2012), p. 208.
257. Ibidem p. 15.
258. Ibidem p. 17.
259. VECCHI, 1993.
260. BEY (1993), p. 18.
261. Ibidem p. 23.
262. Ibidem p. 24.
263. Ivi
264. Ibidem p. 29.
265. COSIMI (2013).
266. Ivi
267.
TUFEKCY (2013). Il post è disponibile all’URL http://dmlcentral.net/blog/zeynep-tufekci/networked-politics-tahrir-taksim-there-social-media-fueled-protest-style
268.
TUFEKCY (2013). Il post è disponibile all’URL http://dmlcentral.net/blog/zeynep-
tufekci/networked-politics-tahrir-taksim-there-social-media-fueled-protest-style
269. Ivi 
270. CORTIANA (2011), p. 33.
271.
TUFEKCY (2013). Il post è disponibile all’URL http://dmlcentral.net/blog/zeynep-tufekci/networked-politics-tahrir-taksim-there-social-media-fueled-protest-style
272. COLEMAN (2011).
273.
Definizione su Wikipedia all’indirizzo http://it.wikipedia.org/wiki/Troll\_(Internet)
274. GULIZIA (2012).
275. COLEMAN (2011).
276.
Guy Fawkes era un cittadino inglese che nel 1605 tentò, insieme ad un gruppo di cattolici, di far
saltare il palazzo di Westminster, a Londra, con dentro re Giacomo I e tutti i membri del Parlamento. Fu
scoperto, arrestato poco prima dell’attentato e condannato a morte. Ogni 5 novembre nel Regno Unito e in 
Nuova Zelanda si commemora quel fallimento, passato alla storia come “la Congiura delle Polveri”. La 
vicenda ha ispirato un fumetto, nato nel 1982, scritto da Alan Moore e disegnato da David Lloyd e un film,
del 2005, diretto da James McTeigue e sceneggiato da Andy e Lana Wachowski, entrambi dal titolo V per
Vendetta. 
277. MEGGIATO (2011).
278. SCIANNAMBLO (2013).
279. FREDIANI (2013).
280. COLEMAN (2011).

Bibliografia
============

* Aliprandi S., (2005), *Copyleft e Open Content, l’altra faccia del copyright*,
  Lodi, PrimaOra.  
* Aliprandi S., (2008), *Creative Commons: manuale operativo*, testo reperibile
  all’URL: http://www.copyleft-italia.it/libri/cc-manuale-operativo (letto il
  14-05-2013).  
* Alvanini S., (2009), *Creative Commons: condividere, modificare e riutilizzare
  legalmente*, in “Il Diritto Industriale”, vol. 17, fasc. 4, pp. 389- 398.   
* Aliprandi S., Piana C., (2012), *Il free software nell’ordinamento italiano:
  principali problematiche giuridiche*, in “informatica e diritto”, vol. 38,
  fasc.1, pp. 79-96.  
* Bazzichelli T., (2013), *Dell’arte, del cyberpunk e dell’hacking*, in “Hacker
  Kulture”, disponibile all’URL: http://www.dvara.net/hk/artehacker.asp (letto
  il 15-10-2013).  
* Beccaria A., (2006), *Permesso d’autore. Percorsi per la creazione di una
  cultura libera.* Testo reperibile all’URL:
  http://www.stampalternativa.it/liberacultura/books/permessodautore.pdf (letto
  il 25-09-2013).  
* Bey H., (1993), *T. A. Z. Zone temporaneamente autonome*, Milano, Shake.  
* Berto C., (1997), *Invito al pensiero di Stirner*, in “Rivista Internazionale
  di Filosofia del Diritto”, vol. 1, pp. 173- 174.  
* Brand S., (1972), *Fanatic Life and Symbolic Death Among the Computers Burns*,
  in “Rolling Stone”, reperibile all’URL:
  http://www.wheels.org/spacewar/stone/rolling_stone.html (letto il 15-06-2013).  
* Burrows R., Featherstone M., (1999), *Tecnologia e cultura virtuale.
  Cyberspace, cyberbodies e cyberpunk*, Milano, Franco Angeli.  
* Carettoni L., Laniado D., (2005), *Etica Hacker: l’imperativo è hands on.*
  Testo reperibile all’URL:
  http://airwiki.ws.dei.polimi.it/images/4/46/Hackdoc.pdf (letto il 10-10-2013).  
* Caronia A., Gallo D., (1997), *Houdini e Faust, breve storia del cyberpunk*,
  Milano, Boldini &Castoldi.  
* Carus P., (1911), *Max Stirner, the predecessor of Nietzsche*, in “The
  Monist”, vol. 21, n 3, pp. 376- 397.  
* Castells M., (2007), *Comunicazione, Potere e Contropotere nella network
  society*, in “International Journal of Communication”, pp. 1- 23.  
* Castells M., (2008), *La nascita della società in rete*, Milano, Università
  Bocconi.  
* Corinto A., Tozzi T., (2002), *Hacktivism: la libertà nelle maglie della
  rete*, Roma, Manifestolibri.  
* Coleman G., (2011), *Anonymous: From the Lulz to Collective Action*, in “The
  new everyday. A media commons project”, reperibile all’URL:
  http://mediacommons.futureofthebook.org/tne/pieces/anonymous-lulz-collective-action
  (letto il 30-10-2013).  
* Cortiana F., (2011), *Conoscenza e partecipazione al tempo di internet*, in
  “Questione Giustizia”, fasc. 3/4, pp. 29-37.  
* Cosimi S., (2013), *I segreti della rivoluzione social*, in “Wired”,
  reperibile all’URL:
  http://daily.wired.it/news/internet/2013/06/07/rivoluzione-social-twitter-275829.html
  (letto il 30-10-2013).  
* D’Ambrosio R., (2006), *Esistenza ed indicibilità in Max Stirner*, in “Collana
  di studi internazionali di scienze filosofiche e pedagogiche”, n 2, pp. 1- 29.  
* D’Orazio D., (2012) *Dall’utopia all’eterotopia. Viaggio nell’immaginario
  utopico tra Hippie e Virtual world*, Tesi di dottorato di ricerca in Scienze
  della comunicazione, Università La Sapienza, Roma.  
* Di Rienzo M., (2004), *L’organizzazione dei mondi open source: profili
  soggettivi*, in “AIDA. Annali italiani del diritto d'autore, della cultura e
  dello spettacolo”, vol. 13, parte 1, pp. 12- 45.  
* Durante N., (2008), *Max Stirner e la controcultura hacker*. Testo reperibile
  all’URL:   
* http://neuroneproteso.wordpress.com/2012/04/14/max-stirner-e-la-controcultura-hacker-nicola-durante/
  (letto il 10-04-13).  
* Feiten E., (2013), *Would the real Max Stirner please stand up?*, in
  “Anarchist Developments in Cultural Studies”, pp. 117- 137.  
* Ferri L., (1993), *Dimensioni della rivolta in Max Stirner*, in Talerico V.,
  Xerri E., a cura di, *Individuo e Insurrezione. Stirner e le culture della
  rivolta* Bologna, Il Picchio, pp. 73-94.  
* Fiegel M., (2012), *Il cyberpunk e il nuovo mito*, in “Haker Kulture”,
  reperibile all’URL: http://www.dvara.net/hk/nuovomito.asp (letto il
  25-10-2013).  
* Fontana A., (2013), *Nuovi miti e controculture: cyberpunk*, in “Haker
  Kulture”, reperibile all’URL: http://www.dvara.net/hk/fontana.asp (letto il
  25-10-2013).  
* Free Software Foundation, (2013), *What is Free software and why is it so
  important for society?*, reperibile on line all’URL:
  http://www.fsf.org/about/what-is-free-software, (letto il 10-09-2013).  
* Gallino, Martinotti, (2003), *Galassia Internet di Manuel Castells*, in “Stato
  e Mercato”, vol. 2, fasc. 68, pp.313-333.  
* Ghidini G., Falce V., (2004), *Open source, General Public License e incentivo
  all’innovazione*, in “AIDA. Annali italiani del diritto d'autore, della
  cultura e dello spettacolo”, vol. 13, parte 1, pp. 3-11.  
* Goffman K., Joy D.,  (2004), *Controculture. Da Abramo ai no global*, Roma,
  Fazi Editore.   
* Goldstein E., (2012), *I love Hacking: il meglio della rivista 2600*, Milano,
  Shake Edizioni.  
* Gubitosa C., (1999), *La vera storia di Internet*, Milano, Apogeo.  
* Gubitosa C., (1999), *Italian crackdown. BBS amatoriali, volontari telematici,
  censura e sequestri nell’Italia degli anni ’90*, Milano, Apogeo.  
* Gubitosa, (2005), *Elogio della Pirateria. Manifesto di ribellione creativa*,
  testo reperibile all’URL:
  http://www.stampalternativa.it/liberacultura/books/elogio_pirateria.pdf (letto
  il 23-05-2013)  
* Gubitosa C., (2007), *Hacker, scienziati e pionieri, Storia sociale del
  ciberspazio e della comunicazione elettronica*, testo reperibile all’URL:
  http://www.stampalternativa.it/liberacultura/books/hacker.pdf (letto il
  20-09-2013)  
* Gulizia S., (2012), *Anonymous: la storia. Dalle origini alla rivolta per la
  chiusura di Megaupload*, in “Comunità Digitali”,  reperibile all’URL :
  http://comunitadigitali.blogosfere.it/2012/02/anonymous-hacker-scientology-megavideo.html,
  (letto il 25-10-2013).  
* Hebdige D., (1981), *Subcuture: the meaning of style*, Londra, Routledge.  
* Huneker J., (1907), *Max Stirner*, in “The North America Review”, vol. 185, n
  616, pp. 332- 337.  
* Innis H., (1982), *Le tendenze della comunicazione*, Sugarco, Milano.  
* Innis H., (2001), *Impero e comunicazione*, Roma, Meltemi.  
* Ippolita, (2005), *Open non è free. Comunità digitali tra etica hacker e
  mercato globale*, Milano, Elèuthera.  
* Ippolita, (2012), *Nell’acquario di facebook*, Milano, Elèuthera.  
* Jefferson T., (1813), *Lettera a Isaac Mc Pherson*, in “America History. From
  revolution to reconstrution and beyond. Reperibile all’URL:
  http://www.let.rug.nl/usa/presidents/thomas-jefferson/letters-of-thomas-jefferson/jefl220.php
  (letto il 07-09-2013).  
* Jordan T., Taylor P., (2004), *Hacktivism and Cyberwars: Rebels with a
  Cause?*, New York, Routledge.  
* Knafo S., (2012), *Anonymous And The War Over The Internet (parte I e II)*, in
  “Huffington Post”, reperibile all’URL:
  http://www.huffingtonpost.com/2012/01/30/anonymous-internet-war_n_1233977.html
  (letto il 25-10-2013).  
* Knafo S., (2012), *TIMELINE: The Evolution Of The 'Anonymous' Internet
  Hacktivist Group*, in “Huffington Post”, reperibile all’URL:
  http://www.huffingtonpost.com/2012/01/30/infographic-anonymous-timeline_n_1241829.html
  (letto il 30-10-2013).  
* Lanzara GF., (2011), *Wikipedia e la cooperazione sociale di massa*, in “Il
  Mulino”, n. 3, pp. 361-375.  
* Leary T., (1995), *Caos e cybercultura*, Milano, Urra/Apogeo.  
* Leary T., (1996), *Fuga. Dall'LSD ad Internet*, Roma, Arcana.  
* Lessig L., (2005), *Cultura libera. Un equilibrio fra anarchia e controllo,
  contro l’estremismo della proprietà intellettuale*, Milano, Apogeo.  
* Lessig L., (2009), *Remix: il futuro del copyright (e delle nuove
  generazioni)*, Milano, Etas.  
* Lessig L., (2006), *Il futuro delle idee*, Milano, Feltrinelli.  
* Levy P., (1996), *L’Intelligenza Collettiva. Per un’antropologia del
  cyberspazio*, Milano, Feltrinelli.  
* Levy S., (1996), *Hackers. Gli eroi della rivoluzione informatica*, Milano,
  Shake Edizioni.  
* Longo G., (2009), *Tecnologia, reti sociali e intelligenza collettiva*, in
  “Sociologia della Comunicazione”, fasc. 40, pp. 12-34.  
* Massimini A., (1999), *Cyberdiritto d’Autore*, Napoli, Edizioni Giuridiche
  Simone.  
* Mastrolilli P., (2001), *Hackers – I ribelli digitali*, Roma, Edizioni
  Laterza.  
* McLuhan M., (1967), *Il medium è il messaggio*, Milano, Feltrinelli.  
* McLuhan M., (1976), *La galassia Gutemberg: nascita dell’uomo tipografico*,
  Roma Armando.  
* Meggiato M., (2011), *Chi sono gli hacker Anonymous*, in “Wired”, disponibile
  all’URL:
  http://daily.wired.it/news/internet/anonymous-attacco-informatico.html (letto
  il 30-10-2013).  
* Meyrowitz, (1995), *Oltre il senso del luogo. L’impatto dei media elettronici
  sul comportamento sociale*, Baskerville, Bologna.  
* Monaci S., (2007), *Modelli di elaborazione della conoscenza on line: Google e
  Wikipedia*, in “Quaderni di Sociologia”, vol. 51, Fasc. 43, pp. 151-167.  
* Monceri F., (2011), *Rip, mix, burn and do it again: The Internet, Knowledge
  and ‘anarchy’*, in “Teoria”, XXXI/2, pp. 133-149.  
* Paccagnella L., (2007), *La gestione della conoscenza nella società
  dell'informazione: il caso di Wikipedia*, in “Rassegna italiana di
  sociologia”, vol. 4, pp. 653-680.  
* Passamani M.,(1993), *L’utilizzazione reciproca: relazionalità e rivolta in
  Max Stirner*, in *“Fuori dal cerchio magico. Stirner e l’anarchia”*, a cura di
  Bertelli, Mangone, Catania, Centolibri.  
* Penzo G., (1971), *Max Stirner: la rivolta esistenziale*, Torino, Marietti.  
* Penzo G., (1993), *Max Stirner: la rivolta esistenziale*, in Talerico V.,
  Xerri E.,, a cura di, *Individuo e Insurrezione. Stirner e le culture della
  rivolta*, Bologna, Il Picchio, pp. 7-22.  
* Penzo G., (1996), *Invito al pensiero di Max Stirner*, Milano, Mursia.  
* Pietrobon A., Tessarolo M., (2011), *Elaborazione di testi nelle comunità di
  fan: indagine sulla produzione di fictions in Italia*, in “Testo e Senso”, n.
  12, pp. 1-12.  
* Proietti S., (1998), *Intorno al cyberpunk*, in “Ancoma”, n. 12, , anno V.  
* Raymond E., (1997), *La cattedrale e il bazar*, Testo completo reperibile
  all’URL: http://it.wikisource.org/wiki/La_cattedrale_e_il_bazaar (letto il
  18-09-2013).  
* Sciannamblo C., (2013), *Anonymous, DDoS come libera espressione*, in “Punto
  Informatico”, reperibile all’URL:
  http://punto-informatico.it/3686948/PI/News/anonymous-ddos-come-libera-espressione.aspx
  (letto il 30-10-2013).  
* Sicchiero G., (2004), *Linee di differenza tra contratti open e proprietari*,
  in “AIDA. Annali italiani del diritto d'autore, della cultura e dello
  spettacolo”, vol. 13, parte 1, pp. 313-337.  
* Signorini A., (1994), *L’Unico e la differenza*, Torino, Giappichelli.  
* Stirner M., (1983), *Scritti Minori*, Bologna, Patron.  
* Stirner M., (2002), *L’unico e la sua proprietà*, Milano, Adelphi.  
* Thomas J., (2005), *The Moral Ambiguity of  Social Control in Cyberspace: A
  Retro‑assessment of the  "Golden Age" of Hacking*, in “New Media and Society”,
  vol. 7, n. 5, pp. 599-624.  
* Tozzi T., (1992), *Comunità virtuali/opposizioni reali*, in “Flash Art Italia”
  anno XXV, n. 167, pp. 1-7.  
* Tufekcy Z., (2013), *Networked Politics from Tahrir to Taksim: Is there a
  Social Media-fueled Protest Style?*, reperibile all’URL:
  http://dmlcentral.net/blog/zeynep-tufekci/networked-politics-tahrir-taksim-there-social-media-fueled-protest-style
  (letto il 30-10-2013).  
* Tucker B., (2000), *Copia pure - Il diritto di copiare nei saggi
  dell'anarchico Benjamin R. Tucker*, Viterbo, Stampa Alternativa.  
* Vecchi B., (1993), *Pirati dell’utopia*, in “il Manifesto”, reperibile
  all’URL: http://www.shake.it/index.php?id=177 (letto il 12-10-2013).  
* Welsh J., (2010), *Max Stirner’s dialectical egoism*, Idaho, Lexington Book.  
* Williams S., (2003), *Codice Libero. Richard Stallman e la crociata per il
  software libero*.  Testo reperibile all’URL:
  http://www.copyleft-italia.it/pub/codice-libero.pdf (letto il 4-10-2013).  
* Woodcock G., (1980), *L’anarchia. Storia delle idee dei movimenti libertari*,
  Milano, Feltrinelli.   
* Wu Ming, (2003), *Il copyleft spiegato ai bambini*, in "Il Mucchio Selvaggio",
  n. 526, reperibile all’URL:
  http://www.wumingfoundation.com/italiano/outtakes/copyleft_booklet.html  
* Ziccardi G., (2009), *Etica e informatica*, Torino, Pearson.  

Sitografia
==========

* *A cultural anthropologist. She researches, writes, and teaches on computer
  hackers and digital activism:* http://www.gabriellacoleman.org  
* *Biblioteca virtuale dedicata alla storia dell'hacking a partire dal
  cyberpunk:* http://www.dvara.net/hk/  
* *Blog della sociologa Zeynep Tufekci:* http://www.technosociology.org  
* *Blog di filosofia, fantascienza, politica:*
  http://www.neuroneproteso.wordpress.com  
* *Blog ufficiale di Anonymous Italia:* http://www.anon-news.blogspot.it  
* *Contenitore di materiali pubblicati dal gruppo di ricerca Ippolita:*
  http://www.ippolita.net  
* *Comunità italiana cyberpunk:* http://www.eracyberpunk.altrevista.org  
* *Creative Commons:* http://www.creativecommons.org  
* *Enciclopedia libera e collaborativa:* http://www.wikipedia.it  
* *Enciclopedia su teorie e pratiche del movimento anarchico:*
  http://www.anarcopedia.org  
* *Free Software Foundation:* http://www.fsf.it   
* *GNU Operating System:* http://www.gnu.org  
* *Intercom: Science, Fiction, Station:* http://www.intercom-sf.com  
* *La filosofia e i suoi eroi:* http://www.filosofico.net  
* *MIT OpenCourseWare. Free Online Corse Materials:* http://www.ocw.mit.edu  
* *Piattaforma di comunicazione internazionale di Anonymous:*
  http://www.anonops.com,  
* *Portale per studiosi e appassionati di filosofia:* http://www.filosofia.it  
* *Phrack Magazine:* http://www.phrack.org  
* *Rivista internazionale underground:* http://www.decoder.it (sito non più
  attivo da luglio 2013)  
* *Rivista Wired: storie, idee e persone che cambiano il mondo:*
  http://www.wired.it  
* *Rolling Stone Magazine:* http://www.rollingstone.com  
* *Sito ufficiale della trasmissione di rai tre:* http://www.mediamente.rai.it  
* *Stanford Encyclopedia of Philosophy:* http://www.plato.stanford.edu  
* *Universal Operating System:* http://www.debian.org  
* *Wu Ming, from the good side of Italy:* http://www.wumingfoundation.com  
* *2600 The Hacker Quarterly:* http://www.2600.com  

Ringraziamenti
==============

A mio fratello, icona controculturale della mia vita.

Ai miei amici, sempre gli stessi da quasi venti anni.

Ai miei genitori, per il sostegno incondizionato.

Alla mia relatrice, i cui meriti sono talmente tanti che non saprei da dove
cominciare per elencarli.

A me, perché, alla fine, come direbbe Stirner, “nessuna cosa mi sta a cuore più
di me stesso”.

Licenza per Documentazione Libera GNU
=====================================

Version 1.3, 3 November 2008 Copyright (C) 2000, 2001, 2002, 2007, 2008 Free
Software Foundation, Inc. <http://fsf.org/> Everyone is permitted to copy and
distribute verbatim copies of this license document, but changing it is not
allowed.

0\. PREAMBLE
------------

The purpose of this License is to make a manual,
textbook, or other functional and useful document "free" in the sense of
freedom: to assure everyone the effective freedom to copy and redistribute it,
with or without modifying it, either commercially or noncommercially.
Secondarily, this License preserves for the author and publisher a way to get
credit for their work, while not being considered responsible for modifications
made by others.

This License is a kind of "copyleft", which means that derivative works of the
document must themselves be free in the same sense. It complements the GNU
General Public License, which is a copyleft license designed for free software.

We have designed this License in order to use it for manuals for free software,
because free software needs free documentation: a free program should come with
manuals providing the same freedoms that the software does. But this License is
not limited to software manuals; it can be used for any textual work, regardless
of subject matter or whether it is published as a printed book. We recommend
this License principally for works whose purpose is instruction or reference.

1\. APPLICABILITY AND DEFINITIONS
---------------------------------

This License applies to any manual or other work, in any medium, that contains a
notice placed by the copyright holder saying it can be distributed under the
terms of this License. Such a notice grants a world-wide, royalty-free license,
unlimited in duration, to use that work under the conditions stated herein. The
"Document", below, refers to any such manual or work. Any member of the public
is a licensee, and is addressed as "you". You accept the license if you copy,
modify or distribute the work in a way requiring permission under copyright law.

A "Modified Version" of the Document means any work containing the Document or a
portion of it, either copied verbatim, or with modifications and/or translated
into another language.

A "Secondary Section" is a named appendix or a front-matter section of the
Document that deals exclusively with the relationship of the publishers or
authors of the Document to the Document's overall subject (or to related
matters) and contains nothing that could fall directly within that overall
subject. (Thus, if the Document is in part a textbook of mathematics, a
Secondary Section may not explain any mathematics.) The relationship could be a
matter of historical connection with the subject or with related matters, or of
legal, commercial, philosophical, ethical or political position regarding them.

The "Invariant Sections" are certain Secondary Sections whose titles are
designated, as being those of Invariant Sections, in the notice that says that
the Document is released under this License. If a section does not fit the above
definition of Secondary then it is not allowed to be designated as Invariant.
The Document may contain zero Invariant Sections. If the Document does not
identify any Invariant Sections then there are none.

The "Cover Texts" are certain short passages of text that are listed, as
Front-Cover Texts or Back-Cover Texts, in the notice that says that the Document
is released under this License. A Front-Cover Text may be at most 5 words, and a
Back-Cover Text may be at most 25 words.

A "Transparent" copy of the Document means a machine-readable copy, represented
in a format whose specification is available to the general public, that is
suitable for revising the document straightforwardly with generic text editors
or (for images composed of pixels) generic paint programs or (for drawings) some
widely available drawing editor, and that is suitable for input to text
formatters or for automatic translation to a variety of formats suitable for
input to text formatters. A copy made in an otherwise Transparent file format
whose markup, or absence of markup, has been arranged to thwart or discourage
subsequent modification by readers is not Transparent. An image format is not
Transparent if used for any substantial amount of text. A copy that is not
"Transparent" is called "Opaque".

Examples of suitable formats for Transparent copies include plain ASCII without
markup, Texinfo input format, LaTeX input format, SGML or XML using a publicly
available DTD, and standard-conforming simple HTML, PostScript or PDF designed
for human modification. Examples of transparent image formats include PNG, XCF
and JPG. Opaque formats include proprietary formats that can be read and edited
only by proprietary word processors, SGML or XML for which the DTD and/or
processing tools are not generally available, and the machine-generated HTML,
PostScript or PDF produced by some word processors for output purposes only.

The "Title Page" means, for a printed book, the title page itself, plus such
following pages as are needed to hold, legibly, the material this License
requires to appear in the title page. For works in formats which do not have any
title page as such, "Title Page" means the text near the most prominent
appearance of the work's title, preceding the beginning of the body of the text.

The "publisher" means any person or entity that distributes copies of the
Document to the public.

A section "Entitled XYZ" means a named subunit of the Document whose title
either is precisely XYZ or contains XYZ in parentheses following text that
translates XYZ in another language. (Here XYZ stands for a specific section name
mentioned below, such as "Acknowledgements", "Dedications", "Endorsements", or
"History".) To "Preserve the Title" of such a section when you modify the
Document means that it remains a section "Entitled XYZ" according to this
definition.

The Document may include Warranty Disclaimers next to the notice which states
that this License applies to the Document. These Warranty Disclaimers are
considered to be included by reference in this License, but only as regards
disclaiming warranties: any other implication that these Warranty Disclaimers
may have is void and has no effect on the meaning of this License.

2\. VERBATIM COPYING
--------------------

You may copy and distribute the Document in any medium, either commercially or
noncommercially, provided that this License, the copyright notices, and the
license notice saying this License applies to the Document are reproduced in all
copies, and that you add no other conditions whatsoever to those of this
License. You may not use technical measures to obstruct or control the reading
or further copying of the copies you make or distribute. However, you may accept
compensation in exchange for copies. If you distribute a large enough number of
copies you must also follow the conditions in section 3.

You may also lend copies, under the same conditions stated above, and you may
publicly display copies.

3\. COPYING IN QUANTITY
-----------------------

If you publish printed copies (or copies in media that commonly have printed
covers) of the Document, numbering more than 100, and the Document's license
notice requires Cover Texts, you must enclose the copies in covers that carry,
clearly and legibly, all these Cover Texts: Front-Cover Texts on the front
cover, and Back-Cover Texts on the back cover. Both covers must also clearly and
legibly identify you as the publisher of these copies. The front cover must
present the full title with all words of the title equally prominent and
visible. You may add other material on the covers in addition. Copying with
changes limited to the covers, as long as they preserve the title of the
Document and satisfy these conditions, can be treated as verbatim copying in
other respects.

If the required texts for either cover are too voluminous to fit legibly, you
should put the first ones listed (as many as fit reasonably) on the actual
cover, and continue the rest onto adjacent pages.

If you publish or distribute Opaque copies of the Document numbering more than
100, you must either include a machine-readable Transparent copy along with each
Opaque copy, or state in or with each Opaque copy a computer-network location
from which the general network-using public has access to download using
public-standard network protocols a complete Transparent copy of the Document,
free of added material. If you use the latter option, you must take reasonably
prudent steps, when you begin distribution of Opaque copies in quantity, to
ensure that this Transparent copy will remain thus accessible at the stated
location until at least one year after the last time you distribute an Opaque
copy (directly or through your agents or retailers) of that edition to the
public.

It is requested, but not required, that you contact the authors of the Document
well before redistributing any large number of copies, to give them a chance to
provide you with an updated version of the Document.

4\. MODIFICATIONS
-----------------

You may copy and distribute a Modified Version of the Document under the
conditions of sections 2 and 3 above, provided that you release the Modified
Version under precisely this License, with the Modified Version filling the role
of the Document, thus licensing distribution and modification of the Modified
Version to whoever possesses a copy of it. In addition, you must do these things
in the Modified Version:

A. Use in the Title Page (and on the covers, if any) a title distinct from that
of the Document, and from those of previous versions (which should, if there
were any, be listed in the History section of the Document). You may use the
same title as a previous version if the original publisher of that version gives
permission.  B. List on the Title Page, as authors, one or more persons or
entities responsible for authorship of the modifications in the Modified
Version, together with at least five of the principal authors of the Document
(all of its principal authors, if it has fewer than five), unless they release
you from this requirement.  C. State on the Title page the name of the publisher
of the Modified Version, as the publisher.  D. Preserve all the copyright
notices of the Document.  E. Add an appropriate copyright notice for your
modifications adjacent to the other copyright notices.  F. Include, immediately
after the copyright notices, a license notice giving the public permission to
use the Modified Version under the terms of this License, in the form shown in
the Addendum below.  G. Preserve in that license notice the full lists of
Invariant Sections and required Cover Texts given in the Document's license
notice.  H. Include an unaltered copy of this License.  I. Preserve the section
Entitled "History", Preserve its Title, and add to it an item stating at least
the title, year, new authors, and publisher of the Modified Version as given on
the Title Page. If there is no section Entitled "History" in the Document,
create one stating the title, year, authors, and publisher of the Document as
given on its Title Page, then add an item describing the Modified Version as
stated in the previous sentence.  J. Preserve the network location, if any,
given in the Document for public access to a Transparent copy of the Document,
and likewise the network locations given in the Document for previous versions
it was based on. These may be placed in the "History" section. You may omit a
network location for a work that was published at least four years before the
Document itself, or if the original publisher of the version it refers to gives
permission.  K. For any section Entitled "Acknowledgements" or "Dedications",
Preserve the Title of the section, and preserve in the section all the substance
and tone of each of the contributor acknowledgements and/or dedications given
therein.  L. Preserve all the Invariant Sections of the Document, unaltered in
their text and in their titles. Section numbers or the equivalent are not
considered part of the section titles.  M. Delete any section Entitled
"Endorsements". Such a section may not be included in the Modified version.  N.
Do not retitle any existing section to be Entitled "Endorsements" or to conflict
in title with any Invariant Section.  O. Preserve any Warranty Disclaimers.

If the Modified Version includes new front-matter sections or appendices that
qualify as Secondary Sections and contain no material copied from the Document,
you may at your option designate some or all of these sections as invariant. To
do this, add their titles to the list of Invariant Sections in the Modified
Version's license notice. These titles must be distinct from any other section
titles.

You may add a section Entitled "Endorsements", provided it contains nothing but
endorsements of your Modified Version by various parties—for example, statements
of peer review or that the text has been approved by an organization as the
authoritative definition of a standard.

You may add a passage of up to five words as a Front-Cover Text, and a passage
of up to 25 words as a Back-Cover Text, to the end of the list of Cover Texts in
the Modified Version. Only one passage of Front-Cover Text and one of Back-Cover
Text may be added by (or through arrangements made by) any one entity. If the
Document already includes a cover text for the same cover, previously added by
you or by arrangement made by the same entity you are acting on behalf of, you
may not add another; but you may replace the old one, on explicit permission
from the previous publisher that added the old one.

The author(s) and publisher(s) of the Document do not by this License give
permission to use their names for publicity for or to assert or imply
endorsement of any Modified Version.

5\. COMBINING DOCUMENTS
-----------------------

You may combine the Document with other documents released under this License,
under the terms defined in section 4 above for modified versions, provided that
you include in the combination all of the Invariant Sections of all of the
original documents, unmodified, and list them all as Invariant Sections of your
combined work in its license notice, and that you preserve all their Warranty
Disclaimers.

The combined work need only contain one copy of this License, and multiple
identical Invariant Sections may be replaced with a single copy. If there are
multiple Invariant Sections with the same name but different contents, make the
title of each such section unique by adding at the end of it, in parentheses,
the name of the original author or publisher of that section if known, or else a
unique number. Make the same adjustment to the section titles in the list of
Invariant Sections in the license notice of the combined work.

In the combination, you must combine any sections Entitled "History" in the
various original documents, forming one section Entitled "History"; likewise
combine any sections Entitled "Acknowledgements", and any sections Entitled
"Dedications". You must delete all sections Entitled "Endorsements".

6\. COLLECTIONS OF DOCUMENTS
----------------------------

You may make a collection consisting of the Document and other documents
released under this License, and replace the individual copies of this License
in the various documents with a single copy that is included in the collection,
provided that you follow the rules of this License for verbatim copying of each
of the documents in all other respects.

You may extract a single document from such a collection, and distribute it
individually under this License, provided you insert a copy of this License into
the extracted document, and follow this License in all other respects regarding
verbatim copying of that document.

7\. AGGREGATION WITH INDEPENDENT WORKS
--------------------------------------

A compilation of the Document or its derivatives with other separate and
independent documents or works, in or on a volume of a storage or distribution
medium, is called an "aggregate" if the copyright resulting from the compilation
is not used to limit the legal rights of the compilation's users beyond what the
individual works permit. When the Document is included in an aggregate, this
License does not apply to the other works in the aggregate which are not
themselves derivative works of the Document.

If the Cover Text requirement of section 3 is applicable to these copies of the
Document, then if the Document is less than one half of the entire aggregate,
the Document's Cover Texts may be placed on covers that bracket the Document
within the aggregate, or the electronic equivalent of covers if the Document is
in electronic form. Otherwise they must appear on printed covers that bracket
the whole aggregate.

8\. TRANSLATION
---------------

Translation is considered a kind of modification, so you may distribute
translations of the Document under the terms of section 4. Replacing Invariant
Sections with translations requires special permission from their copyright
holders, but you may include translations of some or all Invariant Sections in
addition to the original versions of these Invariant Sections. You may include a
translation of this License, and all the license notices in the Document, and
any Warranty Disclaimers, provided that you also include the original English
version of this License and the original versions of those notices and
disclaimers. In case of a disagreement between the translation and the original
version of this License or a notice or disclaimer, the original version will
prevail.

If a section in the Document is Entitled "Acknowledgements", "Dedications", or
"History", the requirement (section 4) to Preserve its Title (section 1) will
typically require changing the actual title.

9\. TERMINATION
---------------

You may not copy, modify, sublicense, or distribute the Document except as
expressly provided under this License. Any attempt otherwise to copy, modify,
sublicense, or distribute it is void, and will automatically terminate your
rights under this License.

However, if you cease all violation of this License, then your license from a
particular copyright holder is reinstated (a) provisionally, unless and until
the copyright holder explicitly and finally terminates your license, and (b)
permanently, if the copyright holder fails to notify you of the violation by
some reasonable means prior to 60 days after the cessation.

Moreover, your license from a particular copyright holder is reinstated
permanently if the copyright holder notifies you of the violation by some
reasonable means, this is the first time you have received notice of violation
of this License (for any work) from that copyright holder, and you cure the
violation prior to 30 days after your receipt of the notice.

Termination of your rights under this section does not terminate the licenses of
parties who have received copies or rights from you under this License. If your
rights have been terminated and not permanently reinstated, receipt of a copy of
some or all of the same material does not give you any rights to use it.

10\. FUTURE REVISIONS OF THIS LICENSE
-------------------------------------

The Free Software Foundation may publish new, revised versions of the GNU Free
Documentation License from time to time. Such new versions will be similar in
spirit to the present version, but may differ in detail to address new problems
or concerns. See http://www.gnu.org/copyleft/.

Each version of the License is given a distinguishing version number. If the
Document specifies that a particular numbered version of this License "or any
later version" applies to it, you have the option of following the terms and
conditions either of that specified version or of any later version that has
been published (not as a draft) by the Free Software Foundation. If the Document
does not specify a version number of this License, you may choose any version
ever published (not as a draft) by the Free Software Foundation. If the Document
specifies that a proxy can decide which future versions of this License can be
used, that proxy's public statement of acceptance of a version permanently
authorizes you to choose that version for the Document.

11\. RELICENSING
----------------

"Massive Multiauthor Collaboration Site" (or "MMC Site") means any World Wide
Web server that publishes copyrightable works and also provides prominent
facilities for anybody to edit those works. A public wiki that anybody can edit
is an example of such a server. A "Massive Multiauthor Collaboration" (or "MMC")
contained in the site means any set of copyrightable works thus published on the
MMC site.

"CC-BY-SA" means the Creative Commons Attribution-Share Alike 3.0 license
published by Creative Commons Corporation, a not-for-profit corporation with a
principal place of business in San Francisco, California, as well as future
copyleft versions of that license published by that same organization.

"Incorporate" means to publish or republish a Document, in whole or in part, as
part of another Document.

An MMC is "eligible for relicensing" if it is licensed under this License, and
if all works that were first published under this License somewhere other than
this MMC, and subsequently incorporated in whole or in part into the MMC, (1)
had no cover texts or invariant sections, and (2) were thus incorporated prior
to November 1, 2008.

The operator of an MMC Site may republish an MMC contained in the site under
CC-BY-SA on the same site at any time before August 1, 2009, provided the MMC is
eligible for relicensing.

How to use this License for your documents
------------------------------------------

To use this License in a document you have written, include a copy of the
License in the document and put the following copyright and license notices just
after the title page:

> Copyright (c) YEAR YOUR NAME.  Permission is granted to copy, distribute
> and/or modify this document under the terms of the GNU Free Documentation
> License, Version 1.3 or any later version published by the Free Software
> Foundation; with no Invariant Sections, no Front-Cover Texts, and no
> Back-Cover Texts.  A copy of the license is included in the section entitled
> "GNU Free Documentation License".

If you have Invariant Sections, Front-Cover Texts and Back-Cover Texts, replace
the "with...Texts." line with this:

> with the Invariant Sections being LIST THEIR TITLES, with the Front-Cover
> Texts being LIST, and with the Back-Cover Texts being LIST.

If you have Invariant Sections without Cover Texts, or some other combination of
the three, merge those two alternatives to suit the situation.

If your document contains nontrivial examples of program code, we recommend
releasing these examples in parallel under your choice of free software license,
such as the GNU General Public License, to permit their use in free software.
