title: Lockdown
date: 2015-11-04 19:21
tags: software libero,hacking,sicurezza,privacy,rete,libertà,tecnologia
summary: La prossima guerra sul computer general-purpose, di Cory Doctorow

La prossima guerra sul computer general-purpose, di Cory Doctorow

Da [boingboing.net][1]

Lockdown
--------
**La prossima guerra sul computer general-purpose**

*di Cory Doctorow*

Questo articolo è tratto da un [discorso][2] al *Chaos Computer Congress* a
Berlino, dicembre 2011.

I computer general-purpose (ad uso generico) sono sbalorditivi. Sono così
sbalorditivi che la nostra società ha ancora difficoltà a venire a patti con
essi, a capire il loro scopo, come accoglierli e come affrontarli. Questo ci
riporta a qualcosa di cui potreste essere stufi di leggere: copyright.

Ma pazientate, perché ciò riguarda qualcosa di molto importante. L’aspetto
delle guerre del copyright, ci da indizi sull’imminente lotta riguardo al
destino del computer general-purpose stesso.

Al principio, avevamo il software confezionato e le [sneakernet][3] (lo scambio
di dati a mano, mediante supporti removibili). Avevamo floppy disk in bustine
ziploc, in scatole di cartone, appesi sui ganci dei negozi e venduti come
caramelle e riviste. Erano eminentemente suscettibili di duplicazione, venivano
duplicati velocemente e diffusamente, e questo con gran dispiacere delle persone
che producevano e vendevano software.

Ed ecco il [Digital Right Management][4] nelle sue forme più primitive:
chiamiamolo DRM 0.96. Introducevano segnali fisici che il software doveva
trovare – danni deliberati, chiavi hardware, settori nascosti – e protocolli di
domanda-risposta che richiedevano il possesso di grossi ed ingombranti manuali
che erano difficili da copiare.

Fallirono per due ragioni. Primo, perché erano commercialmente impopolari,
perché riducevano ai legittimi acquirenti l’usufruibilità del software. I
compratori onesti risentivano della non funzionalità dei loro backup, odiavano
la scarsità di porte libere lasciate delle chiavi hardware, ed erano irritati
dalla scomodità di doversi portare appresso dei grossi manuali quando volevano
far girare il loro software. In secondo luogo, ciò non fermò i pirati, che
trovarono banale modificare il software ad aggirare le autenticazioni. Le
persone che prendevano il software senza pagarlo, non venivano toccate.

Tipicamente, quello che accadeva era che un programmatore in possesso di
tecnologia ed esperienza sofisticate quanto quelle del fornitore del software
stesso, poteva fare [reverse-engineering][5] del software e farne circolare
delle versioni craccate. Anche se sembra una cosa altamente specializzata, in
realtà non lo era. Capire cosa un programma recalcitrante stava facendo ed
indirizzarlo per aggirare i difetti dei supporti, faceva parte delle competenze
di base per i programmatori di computer, specialmente in una era di fragili
floppy disk e nei primi rozzi giorni dello sviluppo del software. Le strategie
anti copia divennero solo più pesanti con il diffondesi delle reti; una volta
avute le [BBS][6], i servizi online, i gruppi di discussione in [USENET][7] e le
mailing list, l’esperienza delle persone che capivano come sconfiggere questi
sistemi di autenticazione, poteva venire “impacchettata” in software piccoli
come i file di crack. Con l’aumentare della capacità della rete, le immagini di
dischi già craccati o gli stessi eseguibili, potevano circolare per conto
proprio.

Questo ci portò il DRM 1.0. Nel 1996, divenne chiaro a chiunque nelle sale del
potere che stava per accadere qualcosa di importante. Stavamo per avere
un’economia dell’informazione, qualsiasi cosa questo significasse. Assunsero
che significava un’economia in cui comprare e vendere informazioni. La
tecnologia dell’informazione migliora l’efficienza, quindi immaginate che
mercati avrebbe un’economia dell’informazione! Si può comprare un libro per un
giorno, puoi vendere il diritto di guardare un film per un Euro, e possono
affittarti il pulsante pausa per un penny al secondo. Si potrebbero vendere
film per un prezzo in un paese, ed un altro prezzo in un altro, e così via. Le
fantasie di quei giorni erano come un noioso adattamento fantascientifico del
Libro dei Numeri dell’Antico Testamento, una noiosa enumerazione di ogni
permutazione di cose che le persone fanno con le informazioni e quello che le
[si potrebbe far pagare per ognuna][8].

Sfortunatamente per loro, niente di questo sarebbe stato possibile a meno di
poter controllare come le persone utilizzano i propri computer ed i file che vi
trasferiscono. Dopotutto, era facile parlare di vendere a qualcuno una canzone
da scaricare sul proprio lettore MP3, ma non non così facile parlare del
diritto di spostare la musica dal lettore ad un altro dispositivo. Ma come
diavolo potresti fermarlo, una volta che gli hai dato il file? Per fare ciò, è
necessario capire come impedire ai computer di eseguire certi programmi ed
ispezionare certi file e processi. Ad esempio è possibile crittografare un
file, e quindi richiedere all’utente di eseguire un programma che sblocchi il
file solo in determinate circostanze.

Ma, come si dice in internet, ora avete due problemi.

Ora devi anche impedire all’utente di salvare il file mentre è decrittato –
cosa che è necessaria – e devi impedire all’utente di capire dove il programma
di sblocco memorizza le sue chiavi, che gli permetterebbero di decrittare
permanentemente i file ed abbandonare completamente quella stupida applicazione
di lettore multimediale.

Ora avete tre problemi: dovete impedire agli utenti che hanno capito come
decrittare il file, di condividerlo con gli altri. Ed ora avete quattro
problemi, perché dovete impedire agli utenti che capiscono come estrarre i
secreti dai programmi di sblocco di dire agli altri come si fa. Ed ora avete
cinque problemi, perché dovete impedire agli utenti che sanno come estrarre
questi segreti di dire agli altri dov’erano questi segreti!

Sono un sacco di problemi. Ma entro il 1996, arrivò una soluzione. Abbiamo avuto
il [WIPO Copyright Treaty][9] approvato dalla United Nations World Intellectual
Property Organization. Questo ha creato delle leggi che rendevano illegale
estratte i segreti dai programmi di sblocco, ed ha creato leggi che resero
illegale estrarre media (come canzoni e film) dai programmi di sblocco mentre
erano in esecuzione. Creò leggi che resero illegale spiegare alle persone come
estrarre i segreti dai programmi di sblocco, e creò leggi che resero illegale
ospitare materiale coperto da copyright o segreti. Inoltre, stabilì un comodo e
diretto processo che permettesse di far rimuovere cose da Internet senza doversi
sbattere con avvocati, giudici e tutte quelle stronzate.

E con questo, la copia illegale finì per sempre, l’economia dell’informazione
sbocciò in un meraviglioso fiore che portò prosperità al mondo intero; come si
dice sulle portaerei, “Missione Compiuta”.

Non è così che finisce la storia, ovviamente, perché chiunque capisca i
computer e le reti, capirebbe che queste leggi creerebbero più problemi di
quanti ne possano risolvere. Dopotutto, queste leggi hanno reso illegale
guardare dentro il proprio computer quando sta eseguendo certi programmi. Hanno
reso illegale raccontare agli altri cos’hai trovato quando hai guardato dentro
il tuo computer, ed hanno reso facile censurare materiale in internet senza
nemmeno dover dimostrare che fosse accaduto qualcosa di male.

In breve, hanno fatto richieste irrealistiche alla realtà, e la realtà non gli
ha obbedito. Copiare è solo diventato più facile dopo il passaggio di queste
leggi – copiare può solo diventare più facile. Non diventerà mai più difficile
di come sia ora. “Dimmi ancora una volta, nonno, di quando era difficile
copiare le cose nel 2012, quando non si poteva ottenere un disco delle
dimensioni di un’unghia, che poteva contenere ogni canzone mai registrata, ogni
film mai fatto, ogni parola detta, ogni foto mai preso, tutto, e trasferirli in
un così breve periodo di tempo che non ti accorgi nemmeno di averlo fatto.”

La realtà si auto afferma. Come nella filastrocca dove si deve far inghiottire
al ragno la mosca, ed all’uccello il ragno, ed al gatto l’uccello, così questi
regolamenti hanno un aspetto attraente, ma sono disastrosi quando implementati.
Ciascun regolamento ne genera uno nuovo, mirato a correggere i propri stessi
fallimenti.

È allettante fermare qua la storia e concludere che il problema è che i
legislatori sono incapaci o cattivi, oppure malignamente incapaci. Questa non è
una strada molto soddisfacente da percorrere, perché è fondamentalmente un
consiglio di disperazione; suggerisce che i nostri problemi non possono essere
risolti fino a quando la stupidità o la malvagità sono presenti nelle stanze
del potere, il che significa che non potranno mai essere risolti. Ma io ho
un’altra teoria su ciò che è successo.

Non è perché i legislatori non capiscano la tecnologia dell’informazione.
Dovrebbe essere possibile non essere un esperto fare comunque una buona legge.
Membri del parlamento, del Congresso e via dicendo sono eletti per
rappresentare i territori e la gente, non discipline e temi. Non abbiamo un
Membro del Parlamenti per la biochimica e non abbiamo un Senatore per il grande
stato della pianificazione urbana. Eppure queste persone, esperte in
regolamenti e politica, non discipline tecniche, riescono ancora a passare
delle buone regole che abbiano senso. Questo è perché il governo si basa
sull’euristica: regole empiriche su come bilanciare il contributo di esperti su
diverse parti di un problema.

Sfortunatamente, la tecnologia dell’informazione confonde queste euristiche –
gli fa sputare la merda – notevolmente.

Le prove importanti per capire se un regolamento è adatto ad uno scopo sono,
primo, se funziona e, secondo, che funzioni o meno, se nello svolgimento del
compito avrà affetti su tutto il resto. Se io avessi voluto che il Congresso,
il Parlamento o l’U.E., regolamentasse la ruota, difficilmente avrei avuto
successo. Se avessi sollevato la questione e sottolineato che i rapinatori di
banche fanno sempre la loro fuga usando veicoli con ruote, ed avessi chiesto
“Possiamo fare qualcosa per questo?”, la risposta sarebbe stata “No”. Questo
perché non sappiamo come fare una ruota che sia ancora generalmente utile per
il suo utilizzo legittimo, ma inutile per i cattivi. Possiamo tutti capire che
i benefici portati dalle ruote sono così profondi, che sarebbe folle rischiare
di trasformarle in oggetti inutili per fermare le rapine in banca. Anche se ci
fosse un’epidemia di rapine in banca – anche se la società stesse per
collassare per colpa delle rapine in banca – nessuno potrebbe pensare che le
ruote siano il giusto posto da cui iniziare a risolvere i nostri problemi.

Tuttavia, se mi mostrassi in questo mio stesso corpo ad affermare che ho la
prova assoluta che i telefoni viva voce stanno rendendo le automobili
pericolose, e richiedessi una legge che proibisca i viva voce in macchina, il
legislatore potrebbe dirmi “Sì, approvo il suo punto, lo faremo.”

Potremmo non essere d’accordo sul fatto che questa sia o meno una buona idea, o
se la mia prova abbia senso, ma ben pochi di noi direbbero che, una volta tolti
i telefoni viva voce dalle automobili, esse smetterebbero di essere automobili.

Siamo consapevoli che le macchine rimangono macchine anche se togliamo da esse
delle caratteristiche, degli accessori. Le macchine hanno uno scopo preciso,
sono special-purpose, almeno in confronto alle ruote, e l’aggiunta del telefono
viva voce non fa che portare più caratteristiche ad una tecnologia già
specializzata. C’è un’euristica in questo: le tecnologie specializzate sono
complesse, ed è possibile togliervi caratteristiche senza fare violenza alla
loro fondamentale e sottostante utilità.

Questa regola empirica è molto utile al legislatore, ma viene nullificata dal
computer general-purpose e dalla rete general-purpose – il PC ed Internet. Se
pensate ai software come a caratteristiche, accessori, un computer che fa girare
un foglio di calcolo, ha l’accessorio del foglio di calcolo, ed uno che fa
girare World of Warcraft, ha l’accessorio del [MMORPG][10]. L’euristica potrebbe
portarvi a pensare che un computer incapace di eseguire fogli di calcolo e
giochi, non sarebbe un attacco al computing, più di quanto un divieto ai
telefoni per auto sarebbe un attacco ai trasporti su ruote.

E se si pensa ai protocolli ed ai siti come ad accessori della rete, allora
dire “aggiusta Internet in modo che non possa far andare BitTorrent”, o
“aggiusta Internet in modo che thepiratebay.org non risolva più”, suonerebbe
come dire “cambia il suono del segnale di occupato”, o “stacca il telefono a
quella pizzeria lì all’angolo”, e non come ad un attacco ai principi
fondamentali dell’internetworking.

La regola empirica funziona bene per macchine, case, e per ogni altra
sostanziale area di regolamento tecnologico. Non capire che non funziona per
Internet non ti rende malvagio, e non fa di te un ignorante. Ti rende solo
parte di quella vasta maggioranza di mondo per la quale idee come la
completezza di Turing ed il punto-punto non hanno significato.

Così i nostri legislatori si spengono, si passano allegramente queste leggi e
diventano parte della realtà del nostro mondo tecnologico. Improvvisamente, ci
sono numeri che non ci è permesso scrivere in Internet, programmi che non siamo
autorizzati a pubblicare, e tutto quello che serve per far sparire da Internet
materiale legittimo è la semplice accusa di violazione del copyright. Non
riesce a raggiungere l’obbiettivo della regolazione, perché non impedisce alle
persone di violare il copyright, ma porta con se una specie di superficiale
parvenza di imposizione del copyright – soddisfa il sillogismo della sicurezza:
“bisogna fare qualcosa, sto facendo qualcosa, qualcosa è stato fatto”. Come
conseguenza, qualsiasi fallimento che possa emergere può essere imputato
all’idea che la legge non si spinga abbastanza lontano, piuttosto che all’idea
che fosse sbagliata fin dal principio.

Questo genere di somiglianza superficiale e di divergenza di fondo si verifica
in altri contesti ingegneristici. Ho un amico, che una volta era un dirigente
anziano in una grande società di beni di consumo, che mi racconto cos’era
successo quando il reparto marketing aveva detto agli ingegneri di aver trovato
una grande idea per un detergente: d’ora in poi, faremo un detergente che
renderà i tuoi vestiti più nuovi ogni volta che li laverai!

Dopo che gli ingegneri avevano cercato invano di trasmettere il concetto di
entropia al reparto marketing, arrivarono ad un’altra soluzione: avrebbero
sviluppato un detergente che usava degli enzimi per attaccare le estremità
delle fibre sciolte, quel tipo di fibre che fanno apparire i tuoi abiti così
vecchi. Così, ogni volta che avresti lavato i tuoi vestiti col detergente,
sarebbero sembrati più nuovi. Sfortunatamente, questo accadeva perché il
detergente digeriva i tuoi abiti. Utilizzarlo avrebbe fatto letteralmente
sciogliere i tuoi vestiti nella lavatrice.

Questo era, manco a dirlo, l’opposto di rendere più nuovi i vestiti. Li
invecchiavi artificialmente ogni volta che li lavavi, e più avresti distribuito
la tua “soluzione” più drastiche sarebbero dovute diventare le misure per
mantenere gli abiti dell’età giusta. Alla fine avresti dovuto comprare dei
nuovi abiti, perché quelli vecchi sarebbero caduti a pezzi.

Oggi abbiamo reparti di marketing che dicono cose come “non abbiamo bisogno di
computer, abbiamo bisogno di elettrodomestici (appliances). Costruiscimi un
computer che non esegua tutti i programmi, ma solo un programma che faccia
questo specifico compito, come lo streaming audio, o il routing di pacchetti, o
i giochi della Xbox, e assicurati che non esegua programmi che io non abbia
autorizzato e che possano minare i nostri profitti.”

Superficialmente, questa sembra un idea ragionevole: un programma che faccia
uno specifico compito. Dopotutto, possiamo mettere un motore in un frullatore,
e possiamo installare un motore in una lavastoviglie, e non preoccuparci che
sia possibile eseguire un programma di lavaggio in un frullatore. Ma non è
questo che facciamo quando trasformiamo un computer in elettrodomestico. Noi
non stiamo facendo un computer che esegue solo l’applicazione
dell’elettrodomestico; quello che facciamo è prendere un computer che può
eseguire qualsiasi programma, quindi, usando una combinazione di rootkit,
spyware e codici firmati, evitiamo che l’utente possa conoscere che processi
stanno girando, installare il suo proprio software, e terminare i processi che
non desidera. In altre parole, un’appliance non è un computer ridotto, ma un
computer completamente funzionante con inseriti degli spyware e venduto così
com’è.

Noi non sappiamo come costruire un computer general-purpose che sia in grado di
eseguire qualsiasi programma tranne quelli che non ci piacciono. È proibito
dalla legge, o da chi ci perderebbe dei soldi. La più vicina approssimazione
che abbiano di questo computer è un computer con spyware: un computer sul quale
dei terzi remoti hanno impostato restrizioni all’insaputa dell’utente, o
malgrado l’opposizione del proprietario del computer. La gestione dei diritti
digitali (DRM), converge sempre verso il malware.

In un famoso incidente – un regalo alle persone che condividono questa ipotesi –
Sony caricò degli installatori di [rootkit][11] su [sei milioni di CD
audio][12], che eseguivano segretamente dei programmi che controllavano i
tentativi di leggere i file audio sui CD, per terminarli. Esso nascondeva
l’esistenza del rootkit facendo sì che il sistema operativo del computer
mentisse su quali processi fossero in esecuzione e su quali file erano presenti
sul disco. Ma questo non è l’unico esempio. Il 3DS Nintendo aggiorna
automaticamente il proprio firmware e fa un controllo sull’integrità per
assicurarsi che tu non abbia alterato in nessun modo il vecchio firmware. Se
rileva segni di manomissione, rende se stesso inutilizzabile.

Gli attivisti dei diritti umani hanno sollevato allarmi sul UEFI, il nuovo
bootloader dei PC che limita il tuo computer in modo da eseguire solo sistemi
operativi “firmati”, notando che dei governi repressivi potrebbero facilmente
nascondere le firme dai sistemi operativi a meno che non acconsentano
operazioni di sorveglianza segreta.

Sul lato della rete, i tentativi di creare una rete che non possa venire usata
per infrangere il copyright, convergono sempre con le misure di sicurezza che
conosciamo dai governi repressivi. Consideriamo [SOPA][13], lo U.S. Stop Online
Piracy Act, che vieta strumenti innocui come DNSSec – una suite di sicurezza che
che autentica le informazioni dei [domain name server][14] – perché possono
venire usati per sconfiggere le misure di blocco ai DNS. Blocca Tor, uno
strumento di anonimato online sponsorizzato dal U.S. Naval Research Laboratory
ed utilizzato dai dissidenti nei regimi oppressivi, perché può essere usato per
aggirare le misure di blocco degli IP.

Infatti, la Motion Picture Association of America, un promotore della SOPA,
diffuse un memo che citava una ricerca per la quale SOPA potrebbe funzionare
perché utilizza le stesse misure che vengono usate in Siria, Cina ed
Uzbekistan. Sostennero che, dato che quelle misure erano efficaci in quei
paesi, avrebbe funzionato anche in America!

Può sembrare che SOPA sia il finale di partita in una lunga battaglia sul
diritto d’autore ed Internet, e può sembrare che se sconfiggiamo SOPA, saremo
sulla buona strada per assicurare la liberà di PC e reti. Ma come ho detto
all’inizio di questo discorso, non si tratta di copyright.

Le guerre sul copyright sono solo la versione beta di lunga guerra a venire
sull’informatica. L’industria dell’intrattenimento è solo il primo belligerante
a prendere le armi, e tendiamo a pensare che essi stiano avendo particolarmente
successo. Dopotutto, ecco SOPA, che freme sul punto di passaggio, pronto a
spaccare Internet dalle fondamenta – tutto in nome della tutela della musica
Top 40, i reality show ed i film di Ashton Kutcher.

Ma la realtà è che la legislazione sul copyright colpisce lontano quando
colpisce, precisamente perché non è presa seriamente dai politici. Questo è il
motivo per cui, da una parte, il Canada ha avuto Parlamenti dopo Parlamenti che
introducevano proposte di legge sul copyright peggiori delle precedenti, ma
dall’altra parte, Parlamenti dopo Parlamenti fallivano nel votarle. È il motivo
per cui SOPA, una proposta di legge composta di pura stupidità e legata
assieme, molecola per molecola fino a formare una specie di “Stupidite 250” che
normalmente si trova solo nel cuore delle stelle appena nate, ha avuto le
udienze sospese per tutte le vacanze di Natale: in modo che i legislatori
potessero entrare nel pericoloso dibattito nazionale su una questione
importante, l’assicurazione di disoccupazione.

É il motivo per cui la World Intellectual Property Organization viene beffata
ancora ed ancora ad emanare proposte di copyright ignoranti e folli: perché
quando le nazioni del mondo mandano i propri emissari a Ginevra, mandano
esperti sull’acqua, non esperti di copyright, mandano esperti sulla salute, non
esperti di copyright. Mandano esperti di agricoltura, non esperti di copyright,
perché il copyright non è davvero così importante.

Il Parlamento del Canada non vota le proprie proposte di legge sul copyright
perché, di tutte le cose che il Canada ha bisogno, aggiustare il copyright si
classifica ben al di sotto di emergenze sanitarie, sfruttare la riserva
petrolifera dell’Alberta, intercedere nei risentimenti settari fra chi parla
inglese e chi francese, risolvere la crisi delle risorse nelle riserve ittiche,
ed un migliaio di altre questioni. La banalità del diritto d’autore ci dice che
quando altri settori dell’economia stanno iniziando a manifestare
preoccupazioni per Internet ed i PC, il copyright si rivelerà una schermaglia,
non una guerra.

Perché altri settori dovrebbero covare rancori contro i computer nello stesso
modo in cui fa l’industria dell’intrattenimento? Il mondo in cui viviamo oggi,
è fatto di computer. Non abbiamo più automobili; abbiamo computer nei quali ci
spostiamo. Non abbiamo più aeroplani, abbiamo delle workstation Solaris
collegate a secchiate di sistemi di controllo industriali. Una stampante 3D non
è un dispositivo, è una periferica, e funziona solo se connessa ad un computer.
Una radio non è più un cristallo: è un computer general-purpose che esegue del
software. Le rimostrante che si sollevano dalle copie non autorizzate delle
Confessioni di Snooki sono triviali se confrontate con le chiamate all’azione
che la nostra realtà ricamata di computer creerà presto.

Considerate la radio. Le leggi sulle radio, fino ad oggi, erano basate
sull’idea che le proprietà di una radio erano fissate al momento della
fabbricazione e che non potevano essere facilmente alterate. Non potevi girare
un interruttore sul tuo baby-monitor ed interferire con gli altri segnali. Ma
potenti radio software-definite (SDRs) possono venir trasformate da
baby-monitor a trasmettitori di servizi d’emergenza o controllori del traffico
aereo, solo caricando ed eseguendo un software diverso. È per questo che la
Federal Communications Commission (FCC) ha considerato cosa potrebbe accadere
quando metteremo in campo le SDRs e chiesto di decidere se dovesse obbligare ad
inserire le SRDs in macchine con il “trusted computing”. In definitiva, la
domanda è se qualsiasi PC dovrebbe venire bloccato, in modo che i suoi
programmi siano strettamente disciplinati dalle autorità centrali.

Anche questa è un’ombra di ciò che arriverà. Dopo tutto, questo è stato l’anno
in cui abbiamo visto il debutto degli schemi open source per convertire i
fucili AR-15 a completamente automatci. È stato l’anno dell’hardware open
source, finanziato da donazioni, per il sequenziamento genetico. E mentre la
stampa 3D può dar luogo a molte denunce banali, ci saranno giudici in Sud
America e mullah in Iran che perderanno il senno per via di gente nella loro
giurisdizione intenta a stampare giocattoli erotici. L’evoluzione della stampa
3D solleverà di sicuro molte critiche autentiche, dai laboratori a stato solido
per la sintesi di metanfetamine ai coltelli di ceramica.

Non serve uno scrittore di fantascienza per capire perché i legislatori possano
essere preoccupati da dei firmware di automobili a guida automatica
modificabili dall’utente, o limitare l’interoperabilità dei controllori aerei,
o il genere di cose che potresti fare con dei sequenziatori ed assemblatori su
scala biologica. Immagina cosa accadrà il giorno in cui Monsanto deciderà che è
molto importante assicurarsi che i computer non possano eseguire programmi che
permettano a periferiche specializzate di creare organismi personalizzati che,
letteralmente si mangerebbero il loro pasto.

Indipendentemente che pensiate questi siano problemi reali o paure isteriche,
sono tuttavia la moneta usata da politiche di lobby e gruppi d’interesse molto
più influenti di Hollywood e dei fornitori di contenuti. Ognuno di essi
arriverà alla stessa questione: “Puoi costruirci un computer general-purpose
che esegua tutti i programmi tranne quelli che ci spaventano e ci fanno
arrabbiare? Puoi farci un Internet che trasmetta ogni messaggio su qualsiasi
protocollo fra due qualsiasi punti, a meno che non ci indisponga?”

Ci saranno programmi che girano su computer general-purpose e periferiche, che
terrorizzeranno anche me. Quindi posso capire che le persone che sostengono di
limitare i computer general-purpose riescano a trovare un pubblico ricettivo.
Ma come abbiamo visto con le guerre sul diritto d’autore, vietare certe
istruzioni, protocolli o messaggi sarà completamente inefficace sia come
prevenzione che come rimedio. Come abbiamo visto nelle guerre sul diritto
d’autore, tutti i tentativi di controllare i PC, convergeranno sui rootkit, e
tutti i tentativi di controllate internet convergeranno su sorveglianza e
censura. Questa roba è importante perché abbiamo passato l’ultimo decennio
mandando i nostri migliori giocatori a combattere quello che credevamo fosse il
mostro finale alla fine del gioco, ma ora si scopre che è solo un guardiano di
fine livello. La posta in gioco non può che aumentare.

Come membro della generazione del Walkman, mi sono rassegnato al fatto che avrò
bisogno di un apparecchio acustico prima di morire. Non sarà un apparecchio
acustico, sarà realmente un computer. Così quando io entro in un’automobile –
un computer nel quale infilo il mio corpo – con il mio apparecchio acustico –
un computer che metto nel mio corpo – voglio sapere che queste tecnologie non
siano progettate per nascondermi dei segreti, o per impedirmi di terminare dei
processi che vanno contro i miei interessi.

L’ultimo anno, la Lower Merion School District, una scuola in un ricco sobborgo
borghese di Philadelphia, si trovo in una [gran quantità di problemi][15]. Venne
scoperta a distribuire ai suoi studenti dei portatili con un rootkit che
permetteva una sorveglianza remota e segreta attraverso la webcam e la
connessione di rete del computer. Fotografarono gli studenti migliaia di volte,
a casa ed a scuola, svegli ed addormentati, vestiti e nudi. Nel frattempo,
l’ultima generazione di tecnologia legale per l’intercettazione, può operare di
nascosto su telecamere, microfoni, GPS su PC, tablets e dispositivi mobili.

Non abbiamo ancora perso, ma prima dobbiamo vincere la guerra del copyright se
vogliamo mantenere i PC ed Internet liberi ed aperti. La libertà, in futuro,
richiederà di avere la capacità di monitorare i nostri dispositivi ed impostare
politiche significative su di essi, di esaminare e terminare i processi
software che vi girano, e di mantenerli come onesti servitori, non come
traditori e spie al soldo di criminali, delinquenti e maniaci del controllo.

[1]:https://boingboing.net/2012/01/10/lockdown.html
[2]:https://boingboing.net/2011/12/27/the-coming-war-on-general-purp.html
[3]:https://it.wikipedia.org/wiki/Sneakernet
[4]:https://boingboing.net/tag/DRM
[5]:https://it.wikipedia.org/wiki/Reverse_engineering
[6]:https://it.wikipedia.org/wiki/Bulletin_board_system
[7]:https://it.wikipedia.org/wiki/Usenet
[8]:https://boingboing.net/2011/12/28/wednesday-weird-bible-verse-1.html
[9]:http://www.wipo.int/treaties/en/text.jsp?file_id=295166
[10]:https://it.wikipedia.org/wiki/MMORPG
[11]:https://it.wikipedia.org/wiki/Rootkit
[12]:https://duckduckgo.com/?q=sony+rootkit&ia=about
[13]:https://boingboing.net/tag/sopa
[14]:https://it.wikipedia.org/wiki/Domain_Name_System
[15]:https://boingboing.net/2011/06/08/lower-merion-student.html
