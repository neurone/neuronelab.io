title: Omineide
date: 2015-01-28 11:37:35 +0100
tags: scienza, sperimentazione animale, etica, neuroscienze
summary: L’Istituto Nazionale della Salute statunitense (NIH) ha deciso di girare pagina sul fronte della sperimentazione animale, e di chiudere quasi tutte le linee di ricerca che utilizzano nei laboratori  metodi invasivi e non invasivi sugli scimpanzé.

Riporto da https://neuroantropologia.wordpress.com/2015/01/28/omineide/

L’Istituto Nazionale della Salute statunitense (NIH) ha deciso di girare pagina
sul fronte della sperimentazione animale, e di chiudere quasi tutte le linee di
ricerca che utilizzano nei laboratori  [metodi invasivi e non invasivi sugli scimpanzé][1].  
Segue grande dibattito tra sostenitori e detrattori della sperimentazione
animale. La questione etica è facile da capire, anche se poi restano sempre i
nodi contorti di un antropocentrismo radicato nell’anima. La sperimentazione
animale può arrivare a livelli di tortura incompatibili con quel dono empatico
che ci rende umani, c’è poco da dire. Ma allo stesso tempo la ragione principale
della difesa delle grandi scimmie continua a essere la loro somiglianza con noi
stessi, dando per scontato che il rispetto sia dovuto solo a quelli “che sono
come te”. Il concetto di rispetto della vita non dovrebbe essere proporzionale
al grado di parentela o somiglianza con il giudice. A parte essere moralmente
discutibile, è anche un criterio pericoloso, perché questi gradi di somiglianza
possono essere soggettivi e suscettibili di repentini cambiamenti. L’attenzione
dovuta alla questione etica da sempre però distoglie lo sguardo dai problemi
scientifici della sperimentazione animale, che sono senza dubbio più pragmatici,
oggettivi, e indiscutibili. In ordine sparso: la numerosità campionaria,
l’influenza delle condizioni esterne, la validità del modello biologico. Il
problema statistico del campione è realmente curioso. In altri campi dove ci
sono limiti in questo senso (per esempio la paleontologia) continuamente si
critica e si discredita l’approccio quantitativo in nome di una robustezza
statistica insufficiente. Invece nei settori che utilizzano modelli animali (per
esempio la neurobiologia) si accettano senza remore studi con pochi individui, a
volte due o tre esemplari, a volte solamente uno. A parità di robustezza
statistica, due pesi e due misure, evidentemente. Poi c’è la questione
dell’ambiente. In neurobiologia spesso quei due esemplari utilizzati nello
studio vengono da una vita trascorsa in uno stabulario, isolati da qualsiasi
contesto ecologico o sociale. Se davvero l’ambiente ha quell’importanza che
sospettiamo nel forgiare mente e cervello, è chiaro che quelle condizioni di
laboratorio genereranno risultati da prendere quantomeno con le pinze. Infine la
questione del modello. Diamo per scontato che affinità genetica voglia dire
corrispondenza biologica, ma ormai da tempo sappiamo che non è così, e certi
parallelismi possono essere davvero fuorvianti.

Questi limiti non devono necessariamente portare a un rigetto indiscriminato
della sperimentazione animale, ma devono essere opportunamente considerati
quando uno valuta il suo costo economico e morale. E ovviamente quando ne
interpreta i risultati. Sia come sia, le accademie neuroscientifiche bofonchiano
contro la scelta del NIH. Qualcuno sicuramente avrà le sue ragioni scientifiche
e morali, ma sono in gioco soldi e carriere, e c’è da pensare che la reazione
ostile non sia solo dovuta alla discordanza sul piano culturale. Il tutto mentre
lo stesso sistema accademico statunitense continua a presentare stereotipi e
riduzionismi nella ricerca che realmente ci saremmo dovuti lasciare dietro nel
secolo scorso. Sopratutto a livello evolutivo, le neuroscienze continuano a
negare approcci e risultati che non portino il loro marchio di fabbrica. In una
recente e titolata revisione su [neuroanatomia e evoluzione cerebrale][2], una
di queste revisioni-manifesto del mainstream che sembrano un messaggio alla
nazione, l’evidenza paleontologica si spazza via nelle prime righe dell’articolo
affermando che i fossili non possono rivelare cambi evoluzionistici
dell’organizzazione cerebrale, e sigillando questa condanna citando un articolo
del … 1968! Beh, però a questo punto dovendo riconsegnare i resti umani alle
popolazioni indigene per farle star buone, e dovendo liberare i prigionieri
antropomorfi dai laboratori, credo che non gli farebbe male dare un pó più di
credito alle informazioni disponibili sulle specie estinte, aggiornando le loro
bibliografie e magari considerando i contributi di altre discipline
complementari. L’errore non è pensare che la paleontologia abbia dei limiti, ma
credere che le altre scienze non ne abbiano.

E Bruner

[1]:http://www.sciencemag.org/content/341/6141/17.summary?sid=6ec8bab0-23df-4085-bc45-cd1a3bd7297c
[2]:http://www.cell.com/trends/cognitive-sciences/abstract/S1364-6613%2813%2900217-9
