title: Pignolerie su bei film
date: 2016-02-15 17:35
tags: film, filosofia, fantascienza
summary: Anche dei film molto belli possono poggiare su idee fallaci rimanendo comunque degli ottimi lavori, molto godibili ed ispiranti. **Pi**, o Il teorema del delirio, è un film psicologico e surreale di Darren Aronofsky, girato con uno stile molto personale ed una fotografia in bianco e nero con molto contrasto, tipica dell’espressionismo, poi ereditata da alcuni film di (meta)genere weird come Eraserhead, Tetsuo o Begotten. **The invention of lying**, o Il primo dei bugiardi, è una commedia di Ricky Gervais che affronta il tema della religione e della necessità per l’uomo di inventare storie.

Anche dei film molto belli possono poggiare su idee fallaci rimanendo comunque degli ottimi lavori, molto godibili ed ispiranti.

[Pi][1], o _Il teorema del delirio_, è un film psicologico e surreale di _Darren Aronofsky_, girato con uno stile molto personale ed una fotografia in bianco e nero con molto contrasto, tipica dell’espressionismo, poi ereditata da alcuni film di (meta)genere weird come [Eraserhead][2], [Tetsuo][3] o [Begotten][4].

[The invention of lying][5], o Il primo dei bugiardi, è una commedia di Ricky Gervais che affronta il tema della religione e della necessità per l’uomo di inventare storie.

Pi – Il teorema del delirio
---------------------------

Secondo le teorie del protagonista del film, la matematica sarebbe l’essenza dell’universo: ciò che lo origina e lo muove.

Quest’idea è molto affascinante, stimola la fantasia e permette molte divertenti speculazioni.

La matematica, tuttavia, non è altro che un linguaggio simbolico, un [sistema formale][6] creato dall’uomo. Il modo in cui è strutturata, la rende particolarmente adatta a descrivere e prevedere i fenomeni fisici che avvengono nell’universo.

È possibile fare altrettanto, talvolta con più difficoltà, utilizzando altri linguaggi sufficientemente completi. Potremmo provarci con la lingua parlata, con il mimare, con una danza o con un algoritmo, ed affermare quindi che l’universo fu creato e sia costituito dal [Verbo][7], che sia [Shiva][8], con la sua danza, a creare e distruggere tutto ciò che esiste, o che sia un programma per computer.

Questo, significherebbe, però, confondere il simbolo con l’oggetto; sarebbe, cioè, una caratteristica di ciò che viene definito [pensiero magico][9].

>La facilità con cui questa modalità di funzionamento del pensiero può essere colta nelle popolazioni primitive è legata all’esistenza, in questi popoli, di simboli in cui il rapporto tra significante (simbolo stesso) e significato (oggetto o evento rappresentato) non è reale, ma è stabilito dalla mente sulla base di una relazione partecipativa che talvolta giunge alla consustanzialità. Il simbolo, in questi casi, è il rappresentato ed è sentito come l’oggetto stesso che rappresenta, il quale viene reso dalla partecipazione attualmente presente.
>
>Una conseguenza significativa di questa modalità di pensiero è visibile nelle pratiche magiche, presenti in Occidente come nel resto del mondo, in cui l’azione sul simbolo (ad esempio un oggetto, quale una foto o un fazzoletto, di un soggetto che si desidera far innamorare) è ritenuta alla pari dell’azione sulla persona cui l’oggetto appartiene.

Sotto questo punto di vista, la parte del film in cui il protagonista si avvicina alla [torah][10] ebraica, trovandovi delle conferme alle proprie idee, appare in modo diverso da come era stata intesa dall’autore.

Ci si potrebbe chiedere se sia possibile che la descrizione di un fenomeno possa diventare talmente dettagliata da renderla indistinguibile e poi corrispondente al fenomeno stesso, cioè a che livello di dettaglio questo possa accadere. Posso chiedermi: “se riesco a creare una descrizione talmente fedele di un oggetto da renderla indistinguibile dall’oggetto stesso, perché ciò non può accadere con l’universo o la realtà?” – Da cui le numerose speculazioni filosofiche e fantascientifiche sulla realtà virtuale, anche se principalmente si concentrano su una rappresentazione soggettiva della realtà legata alla nostra possibilità di interagirvi – Chiedendosi questo, però, si trascura che quando si riproduce un oggetto, lo si fa esternamente all’oggetto stesso, mentre qualsiasi tentativo di descrivere l’universo, lo faremmo dall’interno di esso. Questo ci porta ad una situazione ricorsiva e paradossale. Le informazioni necessarie per descrivere nel massimo dettaglio possibile l’universo, devono essere le informazioni contenute in un universo che permette la creazione di una rappresentazione completa di sé stesso al proprio interno. Perché la simulazione sia completa, dovrebbe contenere anche una rappresentazione di se stessa, descrivente con il massimo dei dettagli l’universo, completo della simulazione dell’universo comprendente la simulazione, e via così all’infinito. Ad ogni ricorsione, la quantità di informazioni necessarie per descrivere completamente l’universo, crescerebbe. Il problema di descrivere alla perfezione un sistema, dall’interno del sistema stesso, non è risolvibile, ed è la matematica stessa ad affermarlo. Questo si applica a qualsiasi altro sistema formale completo: lingua parlata, algoritmi, mimi e danza compresi (nel caso lo fossero). Con tanti ringraziamenti a [Gödel][11] e [Turing][12]. ;)

Il primo dei bugiardi
---------------------

Il film è ambientato in un mondo alternativo dove non esiste il concetto di bugia. Ogni persona che vi vive non è in grado di esprimere altro che la verità.

Perché una situazione del genere sia possibile, è necessario che l’uomo sia perfetto ed infallibile, o che non sia in grado di fare alcun tipo di previsione, oppure che non abbia alcun concetto temporale ad esclusione del presente.

Fintanto che l’uomo è fallibile e può osservare che delle sue previsioni possono non avverarsi, è conseguenza naturale l’emergere dell’idea di poter pensare e raccontare qualcosa _che non è stata_ o _che non è_.

Lo scopo del film, tuttavia, non è di discutere sulla plausibilità di un mondo simile. Quest’idea viene usata come ipotesi per assurdo, per poter mettere in evidenza i temi principali su cui verte il film.

[1]:https://it.wikipedia.org/wiki/Π_-_Il_teorema_del_delirio
[2]:http://www.exxagon.it/eraserhead.html
[3]:http://www.exxagon.it/tetsuo.html
[4]:http://www.exxagon.it/begotten.html
[5]:https://some1elsenotme.wordpress.com/2010/10/10/the-invention-of-lying/
[6]:https://it.wikipedia.org/wiki/Sistema_formale
[7]:https://it.wikipedia.org/wiki/Verbo_(cristianesimo)
[8]:https://it.wikipedia.org/wiki/Shiva#Il_Signore_della_danza
[9]:http://www.benessere.com/psicologia/arg00/pensiero_magico.htm
[10]:https://it.wikipedia.org/wiki/Torah
[11]:https://it.wikipedia.org/wiki/Kurt_Gödel
[12]:https://it.wikipedia.org/wiki/Alan_Turing
