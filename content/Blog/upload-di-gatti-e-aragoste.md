title: Upload di gatti ed aragoste
date: 2016-07-20 21:20
tags: fantascienza, racconti, libri, filosofia, transumanesimo, veg
summary: Un interessante punto d'incontro tra antispecismo e transumanismo. Tratto da **Accelerando** di _Charles Stross_. Dove mettere la linea di demarcazione fra chi ha diritti e chi è una proprietà quando ogni mente, umana o animale, potrà essere caricata in rete e girare come fosse un software?

Un interessante punto d'incontro tra [antispecismo][1] e [transumanesimo][2].
Tratto da [Accelerando][3] di _Charles Stross_. Dove mettere la linea di
demarcazione fra chi ha diritti e chi è una proprietà quando ogni mente, umana o
animale, potrà essere caricata in rete e girare come fosse un software?


«Ai gatti», dice Pamela. «Sperava di cedere i loro upload al Pentagono come un
nuovo sistema di guida per le bombe intelligenti in cambio del pagamento delle
imposte sul reddito. Parlava di rimappare i bersagli nemici in modo che
sembrassero topi, uccelli o qualcosa del genere, prima di immetterli nel loro
apparato sensoriale. Il vecchio trucco del gattino e del puntatore laser».

Manfred le lancia uno sguardo duro. «Non è molto carino. I gatti uploadati sono
una _pessima_ idea».

«Nemmeno trenta milioni di dollari di tasse sono una bella cosa, Manfred. Sono
le cure per tutta la vita di una casa di riposo per anziani per un centinaio di
pensionati senza colpa».

Franklin si allunga indietro, tristemente divertito, in modo da tenersi fuori
dal fuoco incrociato.

«Le aragoste sono senzienti», insiste Manfred. «E quei poveri gattini? Non
meritano i diritti minimi? E tu? Ti piacerebbe svegliarti un migliaio di volte
all'interno di una bomba intelligente, ingannato per farti credere che l'attuale
bersaglio di un computer da battaglia nella Cheyenne Mountain è il tuo oggetto
del desiderio? Ti piacerebbe svegliarti un migliaio di volte solo per morire di
nuovo? Ancora peggio: ai gattini probabilmente non verrà permesso di correre.
Sono troppo pericolosi… crescendo diventeranno gatti, macchine per uccidere
solitarie ed estremamente efficienti. Con l'intelligenza e senza socializzazione
saranno troppo pericolosi per averli in giro. Sono prigionieri, Pam, allevati
per essere senzienti solo per scoprire di trovarsi sotto una sentenza permanente
di morte. Ti sembra giusto?».

«Ma sono solo upload». Pamela lo fissa. «Software, giusto? Si potranno
trasferire su un'altra piattaforma hardware come, per esempio, il tuo Aineko.
Quindi l'argomentazione sul fatto di ucciderli non si applica davvero, giusto?».

«E allora? Tra un paio d'anni faremo l'upload degli umani. Penso che sia
necessario non accettare questa filosofia utilitaristica prima che ci morda
sulla corteccia cerebrale. Aragoste, gattini, umani… è un pendio scivoloso».

Franklin si schiarisce la gola. «Avrò bisogno di un patto di non divulgazione e
di varie dichiarazioni di debita diligenza da te per l'idea del pilota con il
carapace», dice a Manfred. «Poi dovrò rivolgermi a Jim per acquistare l'IP».

«Non posso prendervi parte». Manfred si allunga all'indietro e sorride
pigramente. «Non mi renderò partecipe di un'azione per spogliarle dei diritti
civili. Per quanto mi riguarda, sono liberi cittadini. Oh, e stamattina ho
brevettato l'intera idea di usare intelligenze artificiali derivate da aragoste
come piloti automatici per velivoli spaziali… è loggato ovunque, e tutti i
diritti sono stati assegnati alla Fondazione per l'Intelletto Libero: o date
loro un contratto di lavoro, o salta tutto».

«Ma sono solo _software_! Software basato su fottute aragoste, per l'amor di
Dio! Non sono nemmeno sicuro che _siano_ senzienti, voglio dire… sono una rete
di dieci milioni di neuroni agganciati a un motore di sintassi e a una base di
conoscenza miserabile! Che razza di base è _questa_ per l'intelligenza?».

Manfred punta rapidamente un dito. «Questo è quello che diranno di _te_, Bob.
Fallo. Fallo o non ti azzardare nemmeno a _pensare_ a uploadarti fuori dallo
spazio reale quando il tuo corpo mollerà tutto, perché la tua vita non varrà la
pena di essere vissuta. Il precedente che stabilisci qui determina come verranno
fatte le cose domani. Oh, e sentiti libero di usare questa argomentazione con
Jim Bezier. Alla fine arriverà al punto, dopo che l'avrai martellato con questa
idea. Alcuni tentativi di acquisizione intellettuale non dovrebbero essere
permessi».

«Aragoste…». Franklin scuote la testa. «Aragoste, gatti. Parli sul serio, vero?
Pensi che dovrebbero essere trattati come equivalenti degli esseri umani?».
 
«Non è tanto il fatto che dovrebbero essere trattati come equivalenti degli
esseri umani, quanto che se _non_ vengono trattati come persone, è decisamente
possibile che anche altri esseri uploadati non verranno trattati come tali. Stai
stabilendo un precedente legale, Bob. So di altre società che al momento fanno
lavoro di upload, e nessuna sta pensando allo status legale dell'uploadato. Se
non cominciate a pensarci adesso, dove sarete fra tre o cinque anni?».

[1]:https://it.wikipedia.org/wiki/Antispecismo
[2]:https://it.wikipedia.org/wiki/Transumanesimo
[3]:http://www.antipope.org/charlie/blog-static/fiction/accelerando/accelerando-intro.html
