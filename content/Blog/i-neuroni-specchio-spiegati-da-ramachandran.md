title: I neuroni specchio spiegati da Ramachandran
date: 2015-07-07 18:35
tags: psicologia, scienza, neuroscienze
summary: Riporto dal blog di Gianluca questo video nel quale Ramachandran, neuroscienziato, spiega la scoperta dei neuroni specchio e ciò che essa implica per la nostra conoscenza della cultura umana, le scienze e la filosofia.

Riporto dal [blog di Gianluca][1] questo video nel quale [Ramachandran][2], neuroscienziato, spiega la scoperta dei neuroni specchio e ciò che essa implica per la nostra conoscenza della cultura umana, le scienze e la filosofia.

Collegamento al video: [VS Ramachandran: The neurons that shaped
civilization][3]

Trascrizione del video (da [TED][4]). Oggi vi parlerò del cervello umano, che è
ciò che studiamo all’Università della California. Pensiamo solo un attimo alla
questione. Qui c’è un pezzo di carne, meno di un chilo e mezzo, che potete
tenere nel palmo della vostra mano. Ma che può comprendere la vastità degli
spazi interstellari. può indagare il significato dell’infinito, chiedersi il
significato della sua stessa esistenza, riguardo alla natura di Dio.

E questa è veramente la cosa più straordinaria del creato. È il più grande
mistero riguardo l’essere umano: Com’è possibile tutto cio? Bene, come sapete,
il cervello è fatto di neuroni. Qui stiamo guardando dei neuroni. Ci sono 100
miliardi di neuroni nel cervello adulto. Ogni neurone ha dalle 1.000 ai 10.000
connessioni con gli altri neuroni del cervello. E su questa base, è stato
calcolato che il numero di trasformazioni e combinazioni dell’ attività del
cervello è superiore al numero di particelle elementari dell’universo.

Quindi, come è possibile studiare il cervello? Un modo è di osservare chi ha
avuto delle lesioni in diverse parti del cervello, e studiare i cambiamenti del
comportamento. Di questo vi ho parlato nell’ultimo incontro TED. Oggi vi
parlerò di un altro modo che consiste nel mettere degli elettrodi in diverse
aree del cervello, ed effettivamente registrare l’attività delle singole
cellule nervose nel cervello. Una specie di spionaggio dell’attività delle
cellule nervose nel cervello.

Ora, una scoperta fatta di recente da ricercatori italiani, a Parma, Giacomo
Rizzolatti e i suoi colleghi, e’ un gruppo di neuroni chiamati neuroni
specchio, sono davanti sul cervello, nei lobi frontali. Ora, ci sono dei
neuroni chiamati “neuroni dell’attività motoria” sulla parte anteriore del
cervello, che conosciamo da più di 50 anni. Questi neuroni si attivano quando
uno compie un’azione specifica Per esempio, se mi allungo e afferro una mela, i
neuroni dell’attività motoria, si attiveranno davanti sul cervello. Se mi
allungo e raccolgo un oggetto, un altro neurone si attiverà, comandandomi di
raccogliere l’oggetto. Questi neuroni dell’attività motoria sono conosciuti da
molto tempo.

Ma Rizzolatti ha scoperto un sottogruppo di questi neuroni, circa il 20%, che
si attivano pure quando sto guardando qualcun altro che compie un’azione.
Quindi, ci sono dei neuroni che si attivano quando mi allungo per prendere
qualcosa ma si attivano anche se vedo Joe che si allunga per prendere qualcosa.
E questo e’ veramente sorprendente. Perchè è come se questi neuroni adottino il
punto di vista di un’altra persona. Come se realizzassero una simulazione
virtuale dell’azione dell’altra persona.

Qual’è lo scopo di questi neuroni specchio? Certamente sono coinvolti in
processi come l’imitazione e l’emulazione. Perché imitare un’azione complessa
richiede che il mio cervello si metta nel punto di vista di un altro. Perciò,
questo è importante per l’imitazione e per l’emulazione. Perchè è importante?
Ecco, diamo un’occhiata alla prossima diapositiva. Allora, come avviene
l’imitazione? Perchè è importante? I neuroni specchio e l’imitazione.

Ora, guardiamo alla cultura, al fenomeno della cultura umana. Se andiamo
indietro nel tempo, 75/100.000 anni fa, e guardiamo all’evoluzione umana,
vediamo che accadde una cosa molto importante 75.000 anni fa. All’improvviso
sono emerse e si sono rapidamente diffuse diverse capacità esclusive
dell’essere umano l’uso di attrezzi, del fuoco, di rifugi, ed ovviamente anche
del linguaggio, e la capacità di “leggere” nella mente di un altro ed
interpretare il suo comportamento. Tutto ciò in modo relativamente veloce.

Anche se Il cervello umano ha raggiunto l’attuale dimensione quasi 3/400.000
anni fa, 100.000 anni fa tutto ciò è accaduto molto rapidamente. E io credo che
ciò che accadde sia stata l’improvvisa emersione di un sofisticato sistema di
neuroni specchio, che ci hanno permesso di imitare le azioni degli altri. Così
che, quando per caso avveniva un’improvvisa scoperta di un membro del gruppo,
come l’uso del fuoco, o l’uso di uno strumento, invece di scomparire veniva
diffusa rapidamente, orizzontalmente a tutta la popolazione o verticalmete
trasmessa attraverso le generazioni.

Ciò ha reso l’evoluzione, improvvisamente Lamarckiana, invece che Darwiniana.
L’evoluzione Darwiniana è lenta, richiede centinaia di migliaia di anni. L’orso
polare, per sviluppare la pelliccia, ci metterà migliaia di generazioni, forse
100.000 anni. Nell’essere umano, il figlio deve solo vedere il genitore
uccidere un altro orso polare, scuoiarlo e mettere la pelliccia sul suo corpo,
peli sul corpo, e lo impara subito. Ciò che all’orso polare capita in 100.000
anni, lo si può imparare in 5, forse 10 minuti. E poi ciò che si impara viene
diffuso in proporzione geometrica attraverso la popolazione.

Questo è il principio. L’imitare capacità complesse, è ciò che chiamiamo
cultura, la base della civiltà. Ma c’è anche un altro tipo di neuroni specchio,
che è coinvolto in qualcosa di molto diverso. Come ci sono i neuroni specchio
per l’azione ci sono i neuroni specchio per il contatto. Se qualcuno mi tocca
la mano, i neuroni della corteccia somato-sensoriale nella regione sensoriale
del cervello si attivano. Ma gli stessi neuroni, si attivano in certi casi
semplicemente se guardo un’altra persona che viene toccata. Si prova empatia
per l’altro che viene toccato.

Quindi, molti si attivano se vengo toccato in zone diverse. Neuroni diversi per
aree diverse. Ma alcuni di loro si attivano anche se vedo qualcun altro che
viene toccato nello stesso punto. Così, di nuovo abbiamo dei neuroni sviluppati
per l’empatia. E nasce la domanda: Se guardo una persona che viene toccata,
perchè non mi confondo e non sento anche il contatto soltanto guardando
qualcuno che viene toccato? Voglio dire, empatizzo con la persona ma non
“sento” letteralmente il contatto. Beh, ciò perchè abbiamo dei recettori sulla
pelle, di tatto e di dolore, che mandano dei segnali al cervello e dicono
“Tranquillo non sei stato toccato”. Così empatizziamo, sentiamo tutto con
l’altra persona. Ma non l’effettiva esperienza del contatto altrimenti saremmo
confusi.

Ok, cosi c’è un segnale di ritorno che blocca il segnale dei neuroni specchio
impedendo l’esperienza consapevole del contatto. Ma se elimini il braccio, se
anestetizzi il braccio con un’iniezione, fai un’iniezione nel mio braccio,
blocchi il plesso brachiale e il braccio è insensibile, e da li nessun segnale
ti arriva, se ora vedo qualcuno toccato, io lo sento veramente. in altre
parole, ho dissolto la barriera tra me e gli altri esseri umani. Io li chiamo
neuroni Gandhi o neuroni dell’empatia. (risate)

E questo non in modo astratto o metaforico, tutto cio che separa te da lui,
dalle altre persone è la tua pelle. Togliete la pelle e la vostra mente sarà in
contatto con gli altri. Dissolverete le barriera tra voi e gli altri esseri
umani. Questo è alla base di molte filosofie orientali, Che non esiste nessun
io indipendente separato dagli altri esseri umani, che indaga sul mondo, che
indaga su gli altri. In realtà noi siamo connessi, non solo con FB e Internet,
siamo connessi dai nostri neuroni. E in questa sala c’è un’intera connessione
di neuroni che dialogano tra loro. E non c’è nessuna reale distinzione tra la
vostra coscienza e quella di un altro.

Questa non è filosofia del mambogiambo. Emerge dalla nostra comprensione della
neuroscienza. Prendiamo un paziente con un arto fantasma. Se il braccio e’
stato tolto e hai un arto fantasma, e guardi qualcun altro che viene toccato,
lo senti anche se ti manca. Ma ancora più stupefacente, se hai un dolore
nell’arto fantasma, e prendi la mano dell’altra persona, e massaggi la sua
mano, questo attenua il dolore nella mano che tu non hai piu’, come se i
neuroni possano produrre un sollievo anche soltanto guardando un altro che
viene massaggiato.

E ora l’ultima diapositiva. Per molto tempo la gente ha guardato alla scienza e
all’umanesimo come due cose distinte. C.P. Snow ha parlato di due culture: la
scienza da una parte e l’umanesimo dall’altra; “mai le due s’incontreranno”. Io
dico che i neuroni specchio rappresentano l’interfaccia che ci consente di
rivedere questioni quali la consapevolezza, la rappresentazione del se’, di ciò
che ci separa dagli altri esseri umani, di cio che ci permette di entrare in
empatia con gli altri, ed anche altre cose come lo sviluppo della cultura e
della civiltà, che è solo dell’essere umano. Grazie (Applausi)

[1]:https://some1elsenotme.wordpress.com/2011/03/10/i-neuroni-specchio-spiegati-da-ramachandran/
[2]:https://it.wikipedia.org/wiki/Vilayanur_S._Ramachandran
[3]:https://www.youtube.com/watch?v=t0pwKzTRG5E
[4]:http://www.ted.com/talks/vs_ramachandran_the_neurons_that_shaped_civilization?language=en
