title: Gödel e gli antivirus
date: 2015-11-11 14:25
tags: tecnologia,sicurezza,matematica
summary: Tradotto da http://www.symantec.com/connect/articles/can-viruses-be-detected, di Jennifer Lapell.

Tradotto da [http://www.symantec.com/connect/articles/can-viruses-be-detected][1], di _Jennifer Lapell_.

Pensate all’Ebola
-----------------

Pensate all’[Ebola][2]. Questo è ciò che accadde a Denis Tumpic. Non il vero virus Ebola, ma il suo catastrofico gemello nel mondo del computer. Una mattina accese il proprio computer, e si rifiutò di avviarsi. All’inizio pensò che il messaggio che apparse fosse uno scherzo: “Il tuo [Amiga][3] è vivo.”. Invece, non era uno scherzo. “Com’era quella battuta da [Jurassic Park][4]?” rifletté. “La vita trova il modo.”

Come nella fiaba dell’apprendista stregone, un virus si è mangiato pezzo per pezzo il sistema di Tumpic, corrompendo i dati al suo passaggio. Ci vollero settimane prima che si chiarisse la gola e ruggisse per attirare l’attenzione. E nel momento in cui notò che c’era qualcosa di sbagliato, il suo sistema operativo, i programmi ed i dati erano corrotti da essere irriconoscibili.

Nella vita reale,temiamo il virus Ebola ed i suoi parenti non solo perché sono mortali, ma perché invadono i nostri corpi in segreto ed incubano silenziosamente dentro di noi. Se ci fosse un modo per trovarli prima che sia troppo tardi, ci piace pensare che una cura possa essere possibile. Infatti,con malattie come il cancro, la diagnosi precoce spesso porta a risultati migliori. Immagina se questo fosse possibile anche con i virus per computer.

Tumpic, uno sviluppatore di software con sede a Toronto, è sopravvissuto per raccontare questa storia; ma era solo un virus informatico, dopotutto. Racconta ancora questa storia, a dieci anni di distanza, perché la sensazione di tradimento di quando il tuo sistema viene invaso non svanisce mai completamente. Infatti, l’attacco del proprio Amiga è una delle delle prima cose che cita quando parla del lavoro di [Fred Cohen][5] sui virus informatici. Per Tumpic ed altri che si occupano quotidianamente di problemi di sicurezza informatica, le lezioni di Cohen sono cruciali.

Conosciuto come il grande anziano del mondo dei virus informatici, Cohen sottolinea con modestia che non ha effettivamente coniato il temine “virus” o scritto i primi virus sperimentali negli anni ’50 e ’60. Cohen fu colui che, però, rese chiaro il significato della parola. Già nel 1987, lui definì un virus come “un programma che può infettare altri programmi modificandoli per includervi una copia possibilmente evoluta di se stesso.” C’è stato qualche battibecco su alcuni punti della definizione di Cohen, ma fino ad oggi è ancora ampiamente accettato che catturi il vero significato di cosa sia una virus informatico.

Perché il problema peggiora?
----------------------------

Abbiamo conosciuto i virus fin dagli anni ’50. Sono stati sotto il microscopio sin da allora. La domanda ovvia, dopo così tanti anni è “Perché questo problema non è stato ancora risolto?”

Nel 1990, Cohen scrisse che c’erano “oltre 100 virus informatici ben conosciuti nel mondo reale.” Si consideri inoltre che oggi, secondo alcune stime, ci sono oltre 50.000 virus. Dato questo, non è sufficiente dire che il problema non sta svanendo. Mentre proliferano i PC ed il danno potenziale dei virus cresce esponenzialmente, la questione urgente dovrebbe, in realtà essere, “Perché il problema peggiora?”

Il vostro amministratore di sistema potrebbe non sapere la risposta, ed il vostro fornitore di software antivirus non lo spiega. Dobbiamo rivolgerci ancora a Cohen per questa risposta. Cohen non ha solamente definito il virus informatico. Ha anche conseguito un dottorato di ricerca dimostrando che è impossibile creare un programma antivirus preciso. Così come c’erano stati battibecchi sulla sua definizione di virus, gli esperti hanno discusso continuamente, da allora, sui dettagli del Teorema di Cohen. Nessuno fin’ora è riuscito a provare che avesse torto. La sua dimostrazione è così diabolicamente semplice che contraddirla sarebbe come sostenere che “Vero” equivale a “Falso”.

La dimostrazione di Cohen si basa sul lavoro fatto negli anni ’30 da [Gödel][6] ed altri filosofi nel campo della logica. Gödel usò il suo [Teorema dell’Incompletezza][7] per dimostrate che ci saranno sempre delle complessità che non possono essere gestite dalla matematica, dalla logica, o da altri “linguaggi” definiti dalla nostra immaginazione limitata. [Alan Turing][8] ed altri matematici hanno presto capito che fra questi “linguaggi” sono inclusi anche i linguaggi di programmazione.

Questi programmatori posero una domanda che ebbe un impatto irreversibile sulla scienza informatica: “È possibile scrivere un programma che determini se un qualsiasi programma dato funzioni correttamente?” La loro risposta, abbastanza sorprendentemente, è stata “No.” Anche se questo problema sembra abbastanza semplice, sono riusciti a dimostrare che non potrà mai esistere un programma che possa analizzare il comportamento dei programmi. Ora andiamo avanti veloce fino agli anni ’80, quando gli utenti dei computer iniziarono ad accorgersi che il “problema dei virus” era più grosso di quello che si aspettavano. Qui è dove entra in gioco Fred Cohen.

Cohen prese la palla al balzo con la sua tesi di dottorato nel 1986, ed estese il teorema di Gödel per provare che non potrà mai esistere l’antivirus perfetto. Il suo teorema è diabolicamente semplice e si basa su una tecnica conosciuta come “prova per contraddizione.”

Ecco come funziona: Immaginate per un secondo che Cohen abbia torto, e voi abbiate scritto l’antivirus perfetto che lo provi. Chiamiamolo “AVP” e pensiamo ad esso come ad una scatola nera. Dai qualsiasi programma in pasto ad AVP ed lui restituisce una risposta: “Sì” o “No”. Avete preparato la vostra scatola nera? Bene.

Ora che avete il vostro programma, scriverò anch’io un piccolo programma. Lo chiamerò Programma Testardo (PT), e contiene solo due linee:

PT: 1. Chiedi a AVP se io, PT, sono un virus. 2. A seconda di una delle due possibili risposte: * Se AVP dice “No” (Non sono un virus), allora (infetta) CONQUISTA IL MONDO! * Se AVP dice “Sì” (Sono un virus), allora (non infettare) non fare nulla.

Ricordate il vecchio [indovinello sul barbiere][9] che rade tutti gli uomini che non si radono da soli? Il paradosso qua, era che se il barbiere rade se stesso, non può radere se stesso, perché rade solo quelli che non si radono da soli. Ma dato che non si rade, quindi, è un uomo che non si rade da solo… che significa che deve radersi da solo.

Programma Testardo è l’equivalente informatico di quel barbiere. Se AVP, che è perfetto, decide che OP è un virus, allora esso non è un virus. PT può comportarsi come un virus solo se AVP determina che esso non è un virus, proprio come il nostro barbiere che può radersi solo se decide di farsi crescere la barba. E così ci troviamo, in entrambi i casi, a girare in tondo.

Un paradosso come questo non può esistere nel mondo reale, e quindi il nostro assunto di base – che possa esistere l’AntiVirus Perfetto – dev’essere intrinsecamente sbagliato. L’assunto crea una situazione paradossale, e così l’AVP “scatola nera” – l’antivirus perfetto – non sono non esiste, ma non può esistere. Questo è noto come “Teorema di Cohen”, ed è rimasto essenzialmente incontrastato fino ad oggi.

“Antivirus” reali
-----------------

Se Cohen ha effettivamente provato che un antivirus è teoricamente impossibile, cosa dire di quella piccola applicazione nella vostra system tray, o di quell’aggiornamento delle definizioni dei virus che avete appena scaricato la settimana scorsa? Cosa fanno, se non cercare virus?

In realtà, non sono dei veri antivirus. Esistono due modelli teorici per un programma antivirus: può analizzare l’attività di un programma oppure può esaminare il virus come semplice flusso di bit. Il teorema di di Cohen si riferisce al primo, cioè che l’antivirus analizzi il comportamento dei programmi. Ogni antivirus sul mercato, invece, usa il secondo approccio ed analizza la forma dei programmi, non il loro scopo.

Alcuni antivirus cercano di monitorare attività “simil-virus”, come la scrittura sui settori di boot del disco fisso, ma in generale, la loro intera forza risiede nell’approccio “bitstream”. Il tuo antivirus analizza i programmi prima che vengano eseguiti per trovare sospette sequenze di bit conosciute come “firme dei virus.” Gli antivirus non affrontano il teoriema di Cohen; lo schivano completamente e nel fare questo, suggerisce Cohen, possono portare più danni che benefici.

Gli attuali antivirus assumono che un virus sua solo una semplice sequenza di bit; uni e zeri secondo un ordine riconosciuto. Tramite questo modello,trovare un virus è facile. Basta confrontare meccanicamente i bit ed avvisare l’utente quando viene trovata la firma di un virus. Trovare un ago in un pagliaio non è così difficile se puoi setacciare il pagliaio per filtrarlo. Puoi anche, ricordandoti gli esperimenti delle scuole elementari, “pescarlo” usando un magnete.

Immagina ora che l’ago non abbia più l’aspetto dell’ago. Le sue dimensioni e la forma sono cambiati ed il filtro potrebbe non funzionare. Forse le sue proprietà fondamentali sono le stesse: il magnete potrebbe riuscire nel trucco. Ma cosa accadrebbe se l’ago potesse cambiare le sue proprietà in modo da non essere più fatto di metallo? Non funzionerebbe neppure il magnete. In realtà, non sapresti cosa stai cercando, o cosa fare quando lo troverai.

La vera ragione per cui gli antivirus non funzionano bene, è perché si basano tutti sugli stessi presupposti sbagliati, cioè che i virus avranno sempre lo stesso aspetto e che si comporteranno sempre nello stesso modo. Fingono che l’ago sarà sempre un ago.

Nella realtà, invece, i virus informatici possono mutare fino ad essere irriconoscibili. Modificano la proprie stesse firme e si diffondono in un numero di varianti quasi illimitato. Un antivirus veramente efficace dovrebbe controllare ogni possibile permutazione delle firme dei virus, ed anche se le possibilità possono non essere infinite, il problema è troppo vasto perché un computer reale possa affrontarlo.

Il commerciante che fiduciosamente vi ha venduto una soluzione antivirus probabilmente non ammetterà mai che non sanno cosa cercare. Ma, come ha dimostrato Cohen, essi non possono realmente cercare dei virus, così si accontentano di cercare i flussi di bit dei virus noti. È per questo che sta sempre scaricando questi file “DAT” per aggiornare il suo software con le nuove firme dei virus.

Il pericolo di questo approccio è che questi stessi venditori vi assicurano con orgoglio che avete la miglior protezione disponibile. Ciò che non dicono è che avete ancora poca o nessuna protezione contro i virus che non sono ancora stati creati. Affrontando questo falso senso di sicurezza, Cohen è chiaramente un uomo con una missione: “[questi prodotti] possono solo rilevare le cose che conoscono in anticipo. Si tratta di una fondamentale falla tecnologia che il pubblico ha deciso di comprare.”

Soluzioni reali
---------------

Sorprendentemente, nonostante il suo nome sia considerato un sinonimo di sicurezza informatica negli ultimi vent’anni, Cohen si vanta pubblicamente di non utilizzare nessun programma antivirus. La sorpresa più grande può essere che lui “non è stato ancora attaccato con successo fin’ora.”

Il Teorema di Cohen dimostra che non possiamo mai veramente sapere se nel nostro sistema si nasconde un virus. Allora come può fare, secondo Cohen, un utente tipico per proteggere il proprio sistema da un disastro?

La sua risposta è semplice. Per un tipico utente di computer, però, può essere più dura da mandar giù della puntura del virus stesso.

I virus informatici proliferano, scrive Cohen, perché noi abbracciamo proprio quelle tecnologie che rendono questo possibile. In una recente discussione online sul virus [LoveLetter][10], Cohen scrive: “Abbiamo un compromesso fra comodità e sicurezza, e per far sì che la popolazione possa usare i computer, la comodità è la chiave del successo.”

LoveLetter ed i suoi parenti hanno avuto così successo perché i consumatori chiedono comodità e Microsoft ed altri venditori sono felici di accontentarli. I consumatori hanno “bisogno” di programmi e-mail che aprano ed eseguano gli allegati. Se crediamo alle tendenze del settore, abbiamo “bisogno” di programmi di videoscrittura che eseguano macro, abbiamo “bisogno” di applicazioni (e sistemi operativi) che si aggiornino automaticamente tramite Internet. Ogni aspetto del nostro ambiente “deve” essere intelligente, grafico ed integrato. Questo è il sogno che spacciano le aziende di software come Microsoft – e noi siamo quelli che se lo comprano.

È facile saltare sul carro del “dagli-a-Microsoft”, ma il problema di sicurezza dei virus è molto più profondo di così. Cohen insiste che non riguarda Microsoft, anche se potrebbe sembrare così. “La virologia di base ci insegna che hai bisogno di una certa concentrazione di popolazione sensibile perché una malattia possa diventare pandemica. Microsoft ce l’ha… mentre altri no.”

Tumpic aggiunge, tuttavia, che Microsoft si è spinta troppo oltre col suo zelo di creare standard proprietari. Questo, dice, “alimenta la follia” e permette ai virus di svilupparsi dietro le porte chiuse degli standard delle informazioni proprietarie. Cohen potrebbe condividere questo sentimento. Come il genio malvagio dietro a Jurassic Park, Microsoft ha tentato di contenere la vita, ma la vita, sotto forma di virus informatici, si sta liberando ed affermando.

Nutrita dietro le porte chiuse degli standard proprietari di Microsoft, una nuova generazione di virus è solo in attesa di esplodere e predare i consumatori che smaniano di provare shareware, warez (copie “craccate” di software commerciali), file MP3, e contenuti attivi del Web di ogni tipo. Molti utenti stanno contrattaccando ma, come potreste probabilmente aspettarvi, Cohen è in testa.

La soluzione che sostiene Cohen non è un “pacchetto” che puoi comprare da un commerciante ed eseguire in background. Non è comoda, ma ha dimostrato tramite l’esempio, che funziona. Si tratta di un regime aggressivo di istruzione a tutti i livelli: amministratori di sistema, utenti, e futuri utenti. Con l’istruzione possiamo implementare una “Difesa Combinata”, dove la condivisione di informazioni e la gestione dei permessi sono strettamente legati alla crittografia, e dove vengono messi in atto un buon backup ed un piano di recupero disastri per minimizzare i danni da qualsiasi possibile catastrofe da virus. La difesa di Cohen si occupa del mondo reale; la catastrofe potrebbe essere inevitabile, ma non dev’essere paralizzante.

Parlando di catastrofi, infatti, i ceppi più comuni del virus LoveLetter hanno probabilmente più in comune con il comune raffreddore, che con Ebola Zaire. Sono fastidiosi, ma probabilmente ti riprendi. Ma per quanto riguarda lo spauracchio del prossimo email virus, o quello successivo ad esso? Virus come questi possono non essere sofisticati, ma prosperano sull’ignoranza degli utenti e se aziende come Microsoft hanno ragione riguardo a cosa un utente tipico voglia dal loro software, c’è molta più di quell’ignoranza in giro.

L’amorevole consiglio finale di Cohen per gli utenti che vogliono un ambiente sicuro: “Non usare i computer se non sei un vero esperto.” E se gli spedite una copia del virus LoveLetter, non sorprendetevi di ricevere una email di risposta che vi chiede “di spedigli un fax o di includere invece l’allegato nel corpo del messaggio.” Per Cohen, l’astinenza è un piccolo prezzo da pagare per un computer che si avvia felicemente ogni volta. Tumpic sembra concordare che ne valga la pena per non doversi più svegliare con l’Ebola.

**Vedi anche:**
- [Cure of Computer Viruses][11] Fred Cohen
- [Recent Viruses][12] Fred Cohen
- [Virus Glossary][13] McAfee
- [Amiga SCA Virus info][14] O. Meng & A. M. Rojas
- [That’s Not a Virus][15] Chengi Jimmy Kuo

[1]:http://www.symantec.com/connect/articles/can-viruses-be-detected
[2]:https://it.wikipedia.org/wiki/Ebolavirus
[3]:https://it.wikipedia.org/wiki/Amiga
[4]:https://it.wikipedia.org/wiki/Jurassic_Park_%28romanzo%29
[5]:https://en.wikipedia.org/wiki/Fred_Cohen
[6]:https://it.wikipedia.org/wiki/Kurt_G%C3%B6del
[7]:https://it.wikipedia.org/wiki/Teoremi_di_incompletezza_di_G%C3%B6del
[8]:https://it.wikipedia.org/wiki/Alan_Turing
[9]:https://it.wikipedia.org/wiki/Paradosso_del_barbiere
[10]:https://it.wikipedia.org/wiki/Love_Letter
[11]:http://www.all.net/books/virus/part4.html
[12]:http://www.all.net/journal/recentviruses.html
[13]:http://www.mcafee.com/us
[14]:http://arachnid.qdeck.com/quarc/00008/00008846.htm
[15]:https://www.av.ibm.com/1-2/Features/Feature1/