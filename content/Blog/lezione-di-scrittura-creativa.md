title:  Lezione di scrittura creativa
date:   19-08-2015 19:10
tags:   racconti, libri
summary: Regola numero uno: non usare il punto e virgola. È un ermafrodito travestito che non rappresenta assolutamente nulla. Dimostra soltanto che avete fatto l’università.

Preso dal libro [Un uomo senza patria][1], che raccoglie articoli scritti da [Kurt Vonnegut][2] per varie riviste.

Ecco una lezione di scrittura creativa.

Regola numero uno: non usare il punto e virgola. È un ermafrodito travestito
che non rappresenta assolutamente nulla. Dimostra soltanto che avete fatto
l’università.

E mi rendo conto che alcuni di voi potrebbero avere qualche difficoltà a capire
se sto scherzando o dicendo sul serio. Perciò da ora in poi quando scherzo velo
faccio notare espressamente.

Per esempio: arruolatevi nella Guardian Nazionale o nei marines e insegnate la
democrazia. Sto scherzando.

Stiamo per essere attaccati da Al Qaeda. Se avete della bandiere, sventolatele.
Sembra che sia un ottimo metodo per tenerli lontani. Sto scherzando.

Se volete davvero ferire i vostri genitori e non avete il coraggio di essere
gay, come minimo potreste darvi all’arte. Non sto scherzando. L’arte non è un
modo per guadagnarsi da vivere. Ma è un modo molto umano per rendere la vita
più sopportabile. Praticare un’arte, non importa a quale livello di
consapevolezza tecnica, è un modo per far crescere la propria anima, accidenti!
Cantate sotto la doccia. Ballate ascoltando la radio. Raccontate storie.
Scrivete una poesia a un amico, anche se non vi verrà una bella poesia. Voi
scrivetela meglio che potete. Ne avrete una ricompensa enorme. Avrete creato
qualcosa.

![Man in Hole]({filename}/images/lezione-man-in-hole.png)

Voglio spiegare anche a voi una cosa che ho imparato. Farò uno schema sulla
lavagna qui dietro in modo che possiate seguire più facilmente il discorso
(disegna una linea verticale sulla lavagna). Questo è l’asse F-S: fortuna –
sfortuna. Qui sotto ci sono la morte, la miseria più nera e la malattia, quassù
la grande ricchezza e l’ottima salute. La situazione media, normale, sta qui in
mezzo (indica rispettivamente il fondo, la sommità e la parte centrale della
linea).

Questo invece è l’asse I-E, dove I sta per inizio ed E per entropia. Benissimo.
Non tutte le storie hanno questa forma molto semplice, molto elegante, che
riesce a capire anche un computer (traccia una linea orizzontale che parte dal
centro dell’asse F-S).

Adesso vi voglio dare un consiglio di marketing. Alla gente che si può
permettere di comprare i libri e le riviste e di andare al cinema non piace
sentir parlare di gente povera o malata, per cui è meglio che cominciate la
vostra storia da quassù (indica il tratto superiore dell’asse F-S). Questo è il
tipo di storia che vi capiterà di vedere in continuazione. Al pubblico piace da
morire, e il copyright non è di nessuno. La storia si intitola “L’uomo in fondo
al fosso” ma non c’è bisogno che parli veramente di un uomo né di un fosso.
Funziona così: una persona si mette nei guai, ma poi riesce a tirarsene fuori
(disegna la linea A). Non è un caso che la linea finisca più in alto di dove
cominciava. Per i lettori questo è incoraggiante.

Un’altra storia si chiama “Lui incontra lei”, ma non ci deve essere per forza
un lui che incontra una lei (inizia a disegnare la linea B). È così: qualcuno,
una persona normalissima, un giorno qualunque, si imbatte in qualcosa di
assolutamente straordinario: «Accidenti, questa è la mia giornata fortunata!»
Ma poi: «Oh no, merda!» (disegna una linea che tende verso il basso). E poi la
parabola risale (disegna una linea che torna verso l’alto).

![Boy Meets Girl]({filename}/images/lezione-boy-meets-girl.png)

Dunque: non voglio mettervi in soggezione, ma fatto sta che dopo essermi
diplomato in chimica alla Cornell, dopo la guerra mi sono iscritto
all’Università di Chicago e ho studiato antropologia, e alla fine ci ho preso
anche un dottorato. Nel mio stesso dipartimento c’era Saul Bellow, e nessuno
dei due ha mai fatto una spedizione sul campo. Anche se senza dubbio
immaginavamo di farne. Io cominciai ad andare in biblioteca in cerca dei
racconti degli etnografi, dei missionari e degli esploratori – una serie di
imperialisti, in poche parole – per scoprire che tipo di storie avevano
raccolto dalle popolazioni primitive. In realtà fu un grosso errore per me
laurearmi in antropologia, perché io i popoli primitivi non li sopporto, mi
sembrano stupidi da morire. Ma comunque sia: leggevo quelle storie, una dopo
l’altra, raccolte dalle popolazioni primitive di tutto il mondo, ed erano tutte
piatte come una tavola, come l’asse I-E che vedete qui. Perciò, ci siamo
capiti. I primitivi non valgono un accidente, con quelle storie da poveracci.
Sono veramente indietro. Guardate che bello invece lo saliscendi delle nostre
storie.

Una delle più famose di tutti i tempi comincia quaggiù (fa partire la linea C
da sotto l’asse I-E). Chi è questo personaggio così avvilito? È una ragazzina
di quindici o sedici anni, e le è morta la mamma, quindi ha tutte le ragioni di
essere triste, no? E quasi subito il padre si è risposato con una donna che è
una megera insopportabile e ha pure due figlie stronze. Vi ricorda qualcosa?

Una sera c’è una festa a palazzo. La ragazzina deve aiutare le due sorellastre
e l’odiosa matrigna a prepararsi, ma lei non potrà uscire di casa. Questo la
rende ancora più triste? No, è già una ragazzina col cuore spezzato. La morte
della madre basta e avanza. Le cose non possono andare peggio di così. E
insomma, le altre tre se ne vanno tutte alla festa. Ma ecco che arriva la fata
madrina (disegna una linea che sale per gradi), e le regala le calze, il
mascara, e un mezzo di trasporto per andare al ricevimento.

Così, quando la ragazzina si presenta a palazzo, è la più bella della festa
(disegna una linea che punta in alto). È così tanto truccata che la matrigna e
le sorellastre non la riconoscono nemmeno. Un orologio non ci mette molto a
battere dodici volte, e quindi lei fa un bel capitombolo. Ma dopo il
capitombolo si ritrova allo stesso livello di prima? Eh no, col cavolo.
Qualunque cosa succeda da lì in poi, lei si ricorderà sempre del momento in cui
il principe si era innamorato di lei e lei era la più bella della festa. E così
la ragazzina continua a sbattersi per tirare avanti, a un livello decisamente
più alto di prima, fino a che la scarpetta non le calza a pennello e le fa
raggiungere un livello di felicità smisurato (disegna una linea rivolta verso
l’alto e poi un simbolo dell’infinito).

![Cinderella]({filename}/images/lezione-cenerentola.png)

Ecco invece la storia di Kafka (fa partire la linea D da un punto vicino
all’estremità inferiore dell’asse F-S). C’è un giovanotto piuttosto bruttino e
senza grande personalità. Ha una famiglia antipatica e ha fatto tanti lavori
senza ottenere mai una chance di avanzare nella carriera. Non ha una paga
abbastanza alta per portare a ballare la sua ragazza o andare a farsi una birra
con un amico. Una mattina si alza, è ora di andare a lavorare come al solito, e
scopre di essersi trasformato in uno scarafaggio (disegna una linea che curva
verso il basso e poi il simbolo dell’infinito). Questo è un racconto
pessimista.

![Kafka]({filename}/images/lezione-kafka.png)

La domanda è: questo sistema che ho architettato ci aiuta nella valutazione
della letteratura? Forse un vero capolavoro non può essere crocifisso su una
croce di questo genere. Come la mettiamo con l’Amleto? Quella direi che è
un’opera letteraria niente male. C’è qualcuno che potrebbe sostenere il
contrario? Ecco, non devo neanche disegnare un’altra linea, perché la
situazione di Amleto è la stessa di Cenerentola, solo che i sessi sono
invertiti.

Gli è appena morto il padre. Lui è depresso da morire. E in quattro e
quattr’otto la madre ha preso e sposato lo zio, che è un bastardo. Quindi
Amleto sta viaggiando sullo stesso livello di sfiga di Cenerentola, quando a un
certo punto arriva il suo amico Orazio e gli fa: «Senti, Amleto, c’è un coso in
cime alle mura, mi sa che è meglio se ci parli tu. È tuo padre». Amleto sale
sulle mura e parla con lo spettro, come sappiamo, abbastanza palpabile. E lo
spettro gli dice: «Sono tuo padre, mi hanno assassinato, mi devi vendicare, è
stato tuo zio, e adesso ti spiego come fare.».

E tutto questo per lui è un bene o un male? A tutt’oggi non sappiamo se il
fantasma era veramente il padre di Amleto. Se vi siete divertiti un po’ con
tavolini e pendolini, sapete che il mondo soprannaturale è pieno di spiritelli
dispettosi che svolazzano qua e là, pronti a dirvi tutto e il contrario di
tutto, e non bisogna dargli retta. Madame Blavatsky, una che sul mondo degli
spiriti ne sapeva più di chiunque altro, diceva che è sciocco prendere sul
serio gli spettri, perché spesso e volentieri sono dispettosi, e molte volte
sono le anime di gente che è stata assassinata, si è suicidata o è stata
terribilmente ingannata in una maniera o nell’altra durante la vita, e adesso
cerca vendetta.

![Hamlet]({filename}/images/lezione-hamlet.png)

Perciò non sappiamo se quel coso era veramente il padre di Amleto, o se per
Amleto la sua apparizione è stata un bene o un male. E non lo sa neanche lo
stesso Amleto. Però dice ok, conosco un modo per scoprirlo. Adesso ingaggio
degli attori che rappresentino l’assassinio di mio padre per mano di mio zio
così come me l’ha raccontato il fantasma, metto su questo spettacolino e vedo
come reagisce mio zio. E così Amleto mette su lo spettacolino. E non succede
come a Perry Mason. Lo zio non dà di matto e non dice: «Sì… sì… Mi hai beccato,
mi hai beccato, sono stato io, sono stato io». Il piano fallisce. Amleto ancora
non sa se è stato un bene o un male. Dopo questo fallimento, si ritrova a
parlare con la madre, le tende si muovono e lui pensa che dietro ci sia lo zio,
e si dice: «Va bene, adesso sono stufo di essere sempre così titubante, cazzo»,
e pianta il pugnale nella tenda. E chi casca fuori? Quel trombone di Polonio.
Un Rush Limbaugh1 ante litteram. Shakespeare lo ritiene un cretino, quindi un
personaggio sacrificabile.

Sapete, i genitori stupidi pensano che il consiglio che dà Polonio al figlio
quando parte sia un consiglio che si dovrebbe sempre dare ai figli, ma in
realtà è il più stupido di tutti, e Shakespeare lo trovava addirittura
ridicolo.

«Non chiedere né dar denaro in prestito». Ma che altro è la vita se non un
continuo prestare e prendere il prestito, dare e ricevere?

«Ma soprattutto tieni questo in mente: sii sempre, e resta, fedele a te
stesso». In altre parole, sii un egomaniaco!

Ma anche questa uccisione non è né un bene né un male. Amleto non viene
arrestato. È il principe. Può uccidere chi gli pare e piace. E così la storia
va avanti, e alla fine Amleto muore in duello. Va in paradiso o all’inferno?
Una bella differenza. Cenerentola o lo scarafaggio di Kafka? Secondo me
Shakespeare non credeva al paradiso e all’inferno così come non ci credo io. E
quindi non lo sappiamo, se certe cose sono state un bene o un male.

Vi ho appena dimostrato che che a raccontare storie Shakespeare non era più
bravo di un qualunque indiano Araphao.

Ma c’è un motivo per cui l’Amleto viene considerato un capolavoro, ed è che
Shakespeare ci ha detto la verità, ed è molto raro che in questo saliscendi qui
(indica la lavagna) qualcuno ci dica la verità. La verità è che noi sappiamo
pochissimo della vita, e non capiamo mai davvero che cosa è bene e che cosa è
male.

E se dovessi morire – Dio non voglia – mi piacerebbe andare in paradiso e
chiedere a chi comanda lassù: «Ehi, ma insomma, che cosa è stato un bene e che
cosa un male?»

1. Celebre opinionista radiofonico americano di area conservatrice. [n.d.t.]

[1]:http://www.minimumfax.com/libri/scheda_libro/73
[2]:https://it.wikipedia.org/wiki/Kurt_Vonnegut
