title: I guai che provocò la perfezione di Trurl
date: 2014-05-27 16:53:24 +0200
tags: fantascienza, filosofia, filosofia della mente, intelligenza artificiale
summary: di Stanislaw Lem, dalla raccolta "L'io della mente"

di Stanislaw Lem, dalla raccolta "L'io della mente"

L’universo è infinito ma limitato, e quindi un raggio di luce, in qualsiasi direzione viaggi,
se è abbastanza potente tornerà, dopo miliardi di secoli, al punto di partenza; lo stesso vale
per le chiacchiere, che volano di stella in stella e fanno il giro di tutti i pianeti. Un
giorno Trurl udì raccontare vaghe storie di due potenti costruttori-benefattori, così abili e
così saggi da non avere uguali; corse a riferire queste notizie a Klapaucius, il quale gli
spiegò che non si trattava di due misteriosi rivali, ma solo di loro due, Klapaucius e Trurl,
poiché la loro fama aveva fatto il giro dello spazio. La fama, tuttavia, ha il difetto di non
dire nulla dei nostri insuccessi, neppure quando certi insuccessi sono il risultato di una
perfezione somma. E chi dubitasse di ciò, mediti sull’ultima delle sette sortite di Trurl, che
egli compì senza Klapaucius, in quel momento trattenuto a casa da certe faccende urgenti.
<!--more-->
In quei giorni la vanità di Trurl era grandissima, ed egli accoglieva tutti i segni di
venerazione e di omaggio che gli venivano tributati come atti doverosi e perfettamente normali.
Era in viaggio con la sua nave diretto verso nord, poiché quella era la regione che meno
conosceva, e già da molto tempo voleva volare attraverso lo spazio vuoto, passando accanto a
sfere risonanti di clamori guerreschi e a sfere che avevano finalmente raggiunto la pace
perfetta della desolazione, quando all’improvviso avvistò un piccolo pianeta; in verità, più
che un pianeta, era un frammento vagante di materia.
Sulla superficie di questo pezzo di roccia c’era uno che correva avanti e indietro saltando ed
agitando le braccia in maniera stranissima. Stupefatto alla vista di una solitudine così
assoluta e preoccupato da quei gesti forsennati di disperazione, e fors’anche di rabbia, Trurl
si affrettò ad atterrare.

Si trovò davanti un personaggio straordinariamente altezzoso, tutto coperto d’iridio e vanadio
e accompagnato da un gran clamore e clangore, che si presentò come Excelsius il Tartarico,
sovrano di Pancreon e Cyspenderora; gli abitanti di entrambi i reami, in un accesso di pazzia
regicida, avevano cacciato Sua Altezza dal trono e lo avevano esiliato su quello sterile
asteroide, eternamente alla deriva fra le oscure correnti e i flutti della gravitazione.

Quand’ebbe a sua volta appreso l’identità del visitatore, il monarca deposto cominciò a
insistere perché Trurl – che in fin dei conti era una specie di professionista delle buone
azioni – lo reintegrasse subito nella sua antica posizione. Al pensiero di questa eventualità,
gli occhi del monarca mandarono fiamme di vendetta e le sue dita ferree si contrassero
nell’aria come se già stringessero la gola dei suoi amati sudditi.

Ora Trurl non aveva nessuna intenzione di esaudire la richiesta di Excelsius, poiché ciò
avrebbe causato mali e sofferenze indicibili; e tuttavia voleva in qualche modo consolare e
confortare il re umiliato. Rifletté qualche istante giunse alla conclusione che, anche in
questo caso, non tutto era perduto, poiché era possibile soddisfare completamente il re pur
senza mettere in pericolo i suoi ex sudditi. Così, rimboccatosi le maniche e facendo ricordo a
tutta la sua maestria, Trurl costruì al re un regno nuovo di zecca. In esso erano numerose
città, e fiumi, montagne, foreste, ruscelli, un cielo con le nubi, eserciti di grande
ardimento, cittadelle, castelli e appartamenti riservati alle dame; e vi erano mercati, fastosi
e splendenti sotto il sole, giorni di lavoro massacrante, notti piene fino all’alba di danze e
di canzoni, e un gaio cozzar di brandi. Trurl ebbe anche cura di porre in questo reame una
favolosa capitale, tutta di marmo e di alabastro, e vi radunò un consiglio di saggi canuti, e
palazzi d’inverno e ville estive, e trame, cospiratori, falsi testimoni, balie, delatori,
pariglie di magnifici destrieri e pennacchi che ondeggiavano rosseggianti nel vento; poi
fendette l’aria con fanfare argentine e salve di ventun colpi di cannone, vi gettò
l’indispensabile manciata di traditori, e un’altra di eroi, aggiunse un pizzico di profeti e di
veggenti, un messia e un grande poeta, dopo di che si chinò, mise in moto il meccanismo e
mentre tutto già funzionava diede gli ultimi ritocchi usando con destrezza i suoi microscopici
utensili, e alle donne di quel regno diede la bellezza agli uomini uno scontroso silenzio e
villania nell’ubriachezza, agli ufficiali arroganza e servilismo, agli astronomi l’entusiasmo
per le stelle e ai bambini una grande capacità di fare chiasso. E tutto ciò, collegato, montato
e rettificato al millesimo di millimetro, stava dentro una scatola, neanche troppo grande, ma
proprio delle dimensioni giuste per essere portata in giro con facilità. Tutto questo Trurl lo
regalò a Excelsius perché lo governasse e l’avesse per sempre in signoria; ma prima gli mostrò
dov’erano l’ingresso e l’uscita del suo regno nuovo fiammante, e gli insegnò a programmare le
guerre, a reprimere le rivolte, a esigere i tributi, a riscuotere le tasse e inoltre lo istruì
sui punti critici e sugli stati di transizione di quella civiltà in microminiatura – in altri
termini sui massimi e sui minimi delle congiure di palazzo e delle rivoluzioni – e gli spiegò
ogni cosa per bene che il re, il quale aveva lunga pratica nella conduzione della tirannide,
afferrò subito le istruzioni e immediatamente, senza esitare, sotto lo sguardo del costruttore,
promulgò alcuni editti di prova, girando correttamente le manopole di controllo su cui erano
intagliate aquile imperiali e leoni regali. Questi editti proclamavano lo stato di emergenza,
la legge marziale, il coprifuoco e un’imposta straordinaria.

Dopo che nel regno fu trascorso un anno, che corrisponde per Trurl e per il re a meno di un
minuto, con un atto di straordinaria magnanimità – cioè con un colpetto dato col dito alle
manopole di controllo – il re abolì la condanna a morte, alleviò l’imposta e si degnò di
annullare lo stato di emergenza; al che sorse dalla scatola un grido tumultuoso di gratitudine,
come lo squittio di topolini sollevati per la coda, e attraverso il vetro ricurvo si vide,
sulle strade polverose e lungo le sponde dei fiumi che riflettevano pigri le nubi di bambagia,
la gente che si rallegrava e lodava la grande e incomparabile benevolenza del suo sovrano e
signore.

E così. benché sulle prime si fosse sentito offeso dal dono di Trurl perché il reame era troppo
piccolo e assai simile a un giocattolo per bambini, il monarca vide che lo spesso coperchio di
vetro ingrandiva tutto ciò che vi era sotto; e fors’anche comprese confusamente che lì le
dimensioni non erano importanti, poiché la signoria non si misura in metri o chilogrammi e le
emozioni sono tutto sommato le stesse per i giganti e per i nani; ringraziò dunque il
costruttore, anche se con una certa freddezza. Chissà, magari gli sarebbe piaciuto farlo
gettare in catene e farlo torturare a morte, per sentirsi tranquillo: sarebbe stato un modo
sicuro per stroncare sul nascere ogni chiacchiera su come un rivendugliolo vagabondo aveva
regalato un regno a un possente monarca.

Excelsius tuttavia fu abbastanza assennato da capire che ciò era impossibile, per una questione
fondamentalissima di proporzione, perché sarebbe stato più facile per delle pulci far
prigioniero il loro ospite che per l’esercito del re catturare Trurl. Così con un altro freddo
cenno del capi si cacciò lo scettro sotto il braccio, sollevò con un grugnito il suo regno in
scatola e lo portò nella sua umile capanna da esiliato. E mentre fuori, col ruotare
dell’asteroide, i giorni fiammeggianti si alternavano alle notti tenebrose, il re, riconosciuto
dai suoi sudditi come il più grande del mondo, regnava diligentemente: ordinava questo,
proibiva quello, decapitava, premiava e insomma spronava senza posa in tutti i modi i suoi
piccolini a una fedeltà e a una venerazione assolute verso il trono.

Trurl, tornato a casa, riferì all’amico Klapaucius, non senza orgoglio, come avesse impiegato
il suo genio di costruttore per soddisfare le aspirazioni autocratiche di Excelsius,
salvaguardando al tempo stesso le aspirazioni democratiche dei suoi antichi sudditi. Ma, con
sua sorpresa, Klapaucius non ebbe parole di lode; anzi sul suo viso comparve un’espressione che
pareva di biasimo.

“Ho capito bene?” disse alla fine. “Tu hai messo nelle mani di quel despota brutale, di quello
schiavista nato, di quell’orrendo sadico dispensatore di tormenti, tu hai messo nelle sue mani
un’intera civiltà perché la governi e la signoreggi per sempre? E per giunta mi vieni a parlare
delle grida di gioia provocate dall’abolizione di una minuscola parte dei suoi crudeli decreti!
Trurl, come hai potuto mai fare questo?”.

“Ma tu stai scherzando!” esclamò trurl. “Guarda che tutto il regno sta in una scatola di un
metro per sessanta centimetri per settantacinque… è soltanto un modello…”.

“Un modello di che cosa?”.

“Come, di che cosa? Di una civiltà, naturalmente, solo che è cento milioni di volte più
piccolo”.

“E chi ti dice che non esistano civiltà cento milioni di volte più grandi della nostra? E se
esistessero, la nostra sarebbe allora un modello? Ma poi che importanza hanno le dimensioni? In
quel regno in scatola, un viaggio dalla capitale ai confini non richiede forse mesi, per gli
abitanti? E non soffrono forse, non conoscono il peso della fatica, non muoiono?”.

“Calma, calma, tu sai benissimo che tutti questi processi hanno luogo solo perché io li ho
programmati, e quindi non sono autentici…”.

“Non sono autentici? Vuoi dire che la scatola è vuota e che le parate, le torture e le
decapitazioni sono pure illusioni?”.

“No, non sono illusioni, poiché hanno realtà, benché sia unicamente la realtà di certi fenomeni
microscopici che io ho ottenuto manipolando gli atomi” disse Trurl. “Il punto è che queste
nascite, questi amori, questi atti di eroismo, queste denunce non sono altro che un minuscolo
volteggiare di elettroni nello spazio, guidato con precisione dall’abilità della mia arte non
lineare, la quale…”.

“Basta con queste tue vanterie, non una parola in più!” scattò Klapaucius. “Questi processi si
autp-organizzano o no?”.

“Ma certo!”.

“E avvengono fra nubi infinitesimali di cariche elettriche?”.

“Sai bene che è così”.

“E gli eventi fenomenologici delle albe, dei tramonti e delle battaglie sanguinose sono
generati dalla concatenazione di variabili reali?”.

“Naturalmente”.

“E non siamo forse anche noi, se si compie un esame fisico, meccanicistico, statistico e
meticoloso, nient’altro che il minuscolo volteggiare di nubi di elettroni? Cariche positive e
negative distribuite nello spazio? E la nostra esistenza non è forse il risultato di collisioni
subatomiche e di interazioni di particelle, ancorché noi percepiamo queste capriole molecolari
come paura, desiderio o meditazione? Quando tu sogni a occhi aperti, che cosa si aggira nel tuo
cervello se non l’algebra binaria di circuiti che si aprono e si chiudono e un serpeggiare
incessante di elettroni?”.

“Ma come, Klapaucius, vorresti paragonare la nostra esistenza a quella di un regno di
imitazione racchiuso in una scatola di vetro?!” esclamò Trurl. “Stai davvero esagerando! Il mio
scopo era semplicemente quello di foggiare il simulatore di uno Stato, un modello
ciberneticamente perfetto e nulla più!”.

“Trurl! La nostra perfezione è una maledizione, poiché attira su ciascuna delle nostre opere
un’infinità di conseguenze imprevedibili!” disse Klapaucius con voce stentorea. “Se un
imitatore imperfetto, desideroso di infliggere dolore, si costruisse un rozzo simulacro di
legno o di cera e poi gli conferisse un’apparenza grossolana di essere senziente, la tortura
inflitta a quella cosa sarebbe sì una ben misera imitazione! Ma supponi che questa pratica
subisca una serie di miglioramenti! Supponi che l’artigiano successivo costruisca un fantoccio
che abbia nella pancia un registratore e che sia quindi in grado di gemere sotto i suoi colpi;
pensa a un fantoccio che quando è picchiato chiede pietà, non più un rozzo simulacro, bensì un
omeostato; pensa a un fantoccio che versa lacrime, un fantoccio che sanguina, che ha paura
della morte, pur desiderando anche la pace che solo la morte può dare! Non capisci? Quando
l’imitatore è perfetto, anche l’imitazione deve essere perfetta e la somiglianza diventa
verità, la finzione realtà! Trurl, tu hai preso innumerevoli creature in grado di soffrire e le
hai abbandonate per sempre al dispotismo di un perfido tiranno… Trurl, tu hai commesso un
crimine tremendo!”

“Questo è un sofisma bello e buono!” urlò Trurl, tanto più forte in quanto sentiva la forza del
ragionamento dell’amico. “Gli elettroni volteggiano non solo nel nostro cervello, ma anche nei
dischi grammofonici, il che non dimostra nulla e certo non giustifica in alcun modo queste
analogie ipostatiche! I sudditi di quel mostro di Excelsius muoiono, è vero, quando sono
decapitati, e singhiozzano, combattono e s’innamorano, poiché è in questo modo che ho fissato i
parametri; ma non è possibile dire, Klapaucius, che essi sentano qualcosa: gli elettroni che
corrono su e giù nella loro testa non ti diranno un bel nulla!”.

“E se io guardassi dentro la tua testa, anche lì non vedrei altro che elettroni” replicò
Klapaucius. “Andiamo! Non far finta di non capire quello che dico, so che non sei così stupido!
Un disco di grammofono non svolge missioni per te, non ti chiede pietà, non cade in ginocchio!
Tu dici che non c’è alcun modo di sapere se, quando vengono battuti, i sudditi di Excelsius
gemono solo per via degli elettroni che corrono su e giù dentro di loro – come ingranaggi che
emettano l’imitazione di una voce – o se gemono per davvero, cioè perché sentono un dolore
autentico? Graziosa questa distinzione! No, Trurl, chi soffre non è uno che ti porge la sua
sofferenza perché tu possa toccarla, soppesarla o morderla come una moneta; chi soffre è uno
che si comporta da sofferente! Dimostrami qui e ora, una volta per tutte, che essi non sentono,
che non pensano, che non sono in alcun modo coscienti di essere racchiusi tra i due abissi
dell’oblio, quello che precede la nascita e quello che segue la morte… dimostrami questo,
Trurl, e ti lascerò in pace! Dimostrami che hai soltanto imitato la sofferenza e che non l’hai
*creata*!”

“Sai benissimo che ciò è impossibile” rispose calmo Trurl. “Anche prima di prendere in mano i
miei strumenti, quando la scatola era ancora vuota, dovetti considerare appunto la possibilità
di questa dimostrazione, per poterla eliminare: altrimenti il monarca di quel regno avrebbe
avuto prima o poi l’impressione che i suoi sudditi non fossero affatto sudditi reali, bensì
burattini, marionette. Cerca di capire, non potevo fare in altro modo! Qualunque cosa avesse
distrutto, anche di poco, l’illusione di una realtà completa, avrebbe anche distrutto
l’importanza e la dignità del governare, trasformandola in un puro e semplice gioco
meccanico…”.

“Capisco, capisco fin troppo bene!!” esclamò Klapaucius, “Tu avevi le più nobili intenzioni…
Volevi solo costruire un regno il più possibile simile a un regno autentico che nessuno,
assolutamente nessuno, potesse mai scoprire la differenza e temo che tu ci sia riuscito! Dal
tuo ritorno sono trascorse solo poche ore, ma per loro, per quelli imprigionati dentro la
scatola, sono passati secoli… Quanti esseri, quante vite distrutte! E tutto per appagare e
nutrire la vanità di Re Excelsius!”.

Senz’aggiungere molto, Trurl tornò di corsa alla sua nave, ma vide che l’amico gli veniva
dietro. Si lanciarono nello spazio e puntarono la prua fra due grandi ammassi di fiamme eterne.
Quando la leva della propulsione fu al massimo, Klapaucius disse:

“Sei incorreggibile, Trurl. Tu agisci sempre prima di riflettere. Che cosa intendi fare, ora,
andando là?”.

“Togliergli il regno!”.

“E che ne farai?”.

“Lo distruggerò!” stava per esclamare Trurl, ma si bloccò alla prima sillaba, accorgendosi di
ciò che stava per dire. Infine borbottò:

“Indirò le elezioni. Che scelgano dei governanti giusti fra di loro”.

“Li hai programmati per essere tutti signorotti feudali o vassalli inetti. A che servirebbero
le elezioni? Dovresti prima disfare tutta la struttura del regno e poi ricostruirlo da zero…”.

“E dov’è” esclamò Trurl “ il confine tra la modificazione delle cose e l’intervento sule
menti?!”. Klapaucius non seppe cosa rispondere, e così continuarono a volare immersi in un cupo
silenzio finché giunsero in vista del pianeta di Excelsius. Mentre lo circumnavigavano,
preparandosi all’atterraggio, video qualcosa che li sbalordì.

Tutto il pianeta era coperto da innumerevoli segni di vita intelligente. Ponti in miniatura,
come minuscole liane, traversavano ogni ruscello e rivoletto, mentre le pozzanghere, in cui si
specchiavano le stelle, erano piene di microscopiche barchette che parevano trucioli
galleggianti… L’emisfero in ombra era punteggiato dalle tremolanti luci delle città e
sull’emisfero illuminato si potevano distinguere metropoli fiorenti, benché gli abitanti
fossero troppo piccoli per essere visti anche con la lente più forte. Del re nessuna traccia,
come se la terra l’avesse inghiottito.

“Non c’è” bisbigliò Trurl sgomento. “Che ne hanno fatto? Sono riusciti in qualche modo a far
breccia nelle pareti della scatola e a occupare l’asteroide…”.

“Guarda!” disse Klapaucius, indicando una nuvoletta a forma di fungo, non più grande di un
ditale, che saliva lenta nell’aria. “Hanno scoperto l’energia atomica… E laggiù… quel frammento
di vetro. È quanto resta della scatola, ne hanno fatto una specie di tempio…”.

“Non capisco. Era solo un modello, in fin dei conti. Un processo con un gran numero di
parametri, una simulazione, un simulacro perché un monarca vi si esercitasse, con la necessaria
retroazione, con variabili, multistati…” borbottava Trurl stupefatto.

“Sì. Ma tu hai commesso l’imperdonabile errore di rendere ultraperfetta la tua riproduzione.
Non volendo costruire un semplice ingranaggio meccanico, inavvertitamente, da quel pignolo che
sei, hai creato ciò che era possibile, logico e inevitabile, ciò che è diventato l’antitesi di
un meccanismo…”

“Basta, ti scongiuro!” gridò Trurl. Si volsero a guardare in silenzio l’asteroide.
All’improvviso qualcosa urtò la nave o meglio la sfiorò leggermente. Video l’oggetto, poiché
era illuminato dal sottile getto di fiamme che usciva dalla coda. Una nave, probabilmente, o un
satellite artificiale, benché somigliantissimo a uno degli stivali d’acciaio che portava un
tempo il tiranno Excelsius. E quando i costruttori levarono lo sguardo, video un corpo celeste
che splendeva alto sopra il minuscolo pianeta – prima non c’era – e in quel globo freddo e
pallido riconobbero i tratti arcigni di Excelsius in persona, che era così diventato la Luna
dei microminiani.

*(The Seventh Sally or How Trurl’s Own Perfection Led to No Good”. Da “The Seventh Sally”, in
The Cyberiad, di Stanislaw Lem, trad. inglese di Michael Kandel. Copyright © 1974 by The
Seabury Press, Inc. Riprodotto per autorizzazione di The Continuum Publishing Corporation.)*

Riflessioni
-----------

> But sure as oft as wobem weep  
> It is to be supposed they grieve.  
> (Ma certo quando una donna piange  
> creder si deve ch’ella soffra)  
**Andrew Marvell**

“No, Trurl, chi soffre non è uno che ti porge la sua sofferenza, perché tu possa toccarla,
soppesarla o morderla come una moneta; chi soffre è uno che si comporta da sofferente”.

Sono interessanti le parole che Lem sceglie per descrivere le sue fantastiche simulazioni. Nei
suoi racconti ricorrono di continuo parole come “digitale”, “non lineare”, “retroazione”,
“auto-organizzante”, “cibernetico”. Esse hanno un sapore antiquato, diverso da quello di gran
parte dei termini che ricorrono oggi nelle discussioni sull’intelligenza artificiale. Molte
ricerche di IA hanno preso direzioni che hanno poco a che fare con la percezione,
l’apprendimento e la creatività. Più spesso, riguardano temi come la simulazione della capacità
d’impiegare il linguaggio – e diciamo “simulazione” a ragion veduta. Noi siamo dell’opinione
che molti degli aspetti più difficili e stimolanti dell’intelligenza artificiale debbano ancora
essere affrontati: e allora la natura “auto-organizzante” e “non lineare” della mente umana
tornerà a presentarsi come un importante mistero da indagare. Nel frattempo Lem ci fa sentire
alcuni degli aromi possenti e inebrianti che quelle parole dovrebbero emanare.

Nel romanzo *Even Cowgirls Get the Blues* di Tom Robbins si legge un passo che presenta una
sorprendente somiglianza con l’immagine del minuscolo mondo artificiale di Lem:

Quell’anno per Natale Julian donò a Sissy un villaggio tirolese in miniatura di fattura
meravigliosa.

C’era una minuscola cattedrale, le cui vetrate a mosaico rifrangevano la luce del sole come in
un caleidoscopio. C’era una piazza e ein Biergarten. Il sabato sera il Biergarten era animato e
vociante. C’era un forno sempre odoroso di pane fresco e di strudel. C’erano un municipio e un
commissariato di polizia con parti sezionate che rivelavano normali quantità di burocrazia e di
corruzione. C’erano piccoli tirolesi che portavano calzoncini di cuoio dalle complicate
impunture e, sotto i calzoncini, genitali di fattura altrettanto fine. C?erano negozi di sci e
molte altre cose interessanti, compreso un orfanotrofio. Quest’ultimo era fatto in modo da
incendiarsi e bruciare fino alle fondamenta tutte le vigilie di Natale. Gli orfani scappavano
fuori nella neve con la camicia da notte in fiamme. Una cosa terribile! Verso la seconda
settimana di gennaio arrivava un ispettore dei pompieri che rovistava fra le macerie
borbottando: “Se mi avessero ascoltato, a quest’ora i bambini sarebbero ancora vivi”. *(Da Even
Cowgirls Get the Blues, di Tom Robbins, pp. 191-92. Copyright © 1976 by Tom Robbins. Riprodotto
per autorizzazione della Bantam Books.)*

Benché l’argomento di questo brano somigli moltissimo a quello di Lem, il suo sapore è
completamente diverso. È come se due compositori avessero creato la stessa melodia, ma
l’avessero armonizzata in modo del tutto diverso. Lungi dal volerci persuadere che i suoi omini
provino sensazioni e sentimenti reali, Robbins ce li presenta semplicemente come incredibili
(se non incredibilmente stupidi) capolavori di orologeria.

L’annuale ripetizione della tragedia dell’orfanotrofio, che ricorda l’idea nietzcheana
dell’eterno ritorno – tutto ciò che è accaduto si ripeterà infinite volte – sembra privare il
piccolo mondo di ogni significato reale. Perché il lamento dell’ispettore dei pompieri è reso
così falso dalla ripetizione? L’orfanotrofio viene ricostruito dai piccoli tirolesi o da un
tasto “RESET”? Da dove vengono i nuovi orfani? O forse sono quelli “morti” che tornano in
“vita”? Come nel caso delle altre fantasie presentate in questo libro, è spesso istruttivo
riflettere sui particolari omessi.

Bastano piccoli tocchi di stile e stratagemmi narrativi per indurci a credere, o a non credere,
nell’autenticità di queste piccole anime. E voi per cosa propendete?

**D.R.H.**  
**D.C.D.**

*Tratto dal libro "L'io della Mente" edito in Italia da Adelphi.*
