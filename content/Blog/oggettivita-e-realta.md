title: Oggettività e realtà
date: 2014-12-05 13:37:49 +0100
tags: filosofia
summary: L’universo è probabilistico o deterministico? È possibile arrivare alla spiegazione oggettiva di esso, tramite la sola ragione? Entrambe queste ipotesi mi hanno attirato, ad intermittenza, seppur si escludessero a vicenda.

L’universo è probabilistico o deterministico? È possibile arrivare alla spiegazione oggettiva di esso, tramite la sola ragione? Entrambe queste ipotesi mi hanno attirato, ad intermittenza, seppur si escludessero a vicenda.

**og|get|ti|vi|tà**  
s.f.inv.  
1. TS filos., carattere intrinseco di ciò che è oggetto, che appartiene al mondo
   del reale e si distingue dal soggetto pensante
2. CO obiettività, imparzialità: giudicare con o.

Il termine viene normalmente utilizzato in modo ambiguo. L’oggettivo al quale si
pensa di tendere, non è l’oggettivo intrinseco di ciò che è oggetto, ma un
qualcosa che appare oggettivo perché *condiviso*, quindi appartenente non solo
al soggetto pensante. Usando il termine *oggettivo, come contrapposizione a
soggettivo*, solitamente si inferisce che l’oggettività alla quale si tende,
coincida con *l’oggettività intrinseca*. Io ritengo che sia una *falsa
inferenza* e che l’uso comune del termine, rendendola “scontata”, ci crei
difficoltà nell’accorgerci dell’ambiguità.  
Non riconoscere questa ambiguità, ritenendo che ciò che consideriamo oggettivo
coincida con la realtà, potrebbe crearci problemi ogniqualvolta si cerchi di
studiarla.  
In questo articolo cercherò di spiegarne il motivo, supponendo una separazione
fra le due oggettività. Utilizzerò il termine “oggettività” quando vorrò
intendere ciò che si distingue dal soggetto pensante in contrapposizione al
termine “soggettività”, e “realtà” per riferirmi al carattere intrinseco della
stessa, cioè l’oggettività in senso assoluto del termine, in mancanza di
qualsivoglia soggetto o punto di vista.
<!--more-->
Poniamo la realtà come data. Noi apparteniamo ad essa e la nostra esperienza è
*frutto di ciò che la mente seleziona* dagli stimoli dei nostri sensi. Senza i
processi di selezione, la nostra coscienza non sarebbe in grado di prestare
attenzione a tutto ciò che entra dai sensi e non sarebbe in grado di distinguere
quali stimoli siano più prioritari di altri. Solo dopo questi processi di
selezione, gran parte dei quali sono inconsci o subconsci, il nostro distillato
di realtà arriva all’intelletto e quindi alla ragione. La maggior parte dei
processi di selezione, non sono riconoscibili tramite introspezione.  
*L’introspezione* è un compito che richiede l’intelletto. Ne segue che solo
alcuni dei processi di selezione sono intellettualizzabili da noi stessi, mentre
altri rimangono pre-intellettuali.  
Superati i sensi ed i processi di selezione si arriva all’intelletto ed è qui
che, una persona sana, riesce naturalmente a distinguere fra *ciò che appartiene
solo a se stesso e ciò che è esterno a se stesso*. In mancanza di questa
capacità, gli uomini finirebbero per fuggire a gambe levate al solo pensiero di
una tigre o, viceversa, crederebbero che la tigre che hanno davanti sia solo
frutto della propria fantasia e finirebbero quindi masticati e digeriti ben
bene, perché non riuscirebbero a distinguere fra ciò che è rappresentazione
mentale e ciò che è frutto dei sensi. L’uomo, da questo, inferisce che ciò che
non è solo suo – soggettivo – ma condiviso fra altri individui, corrisponda alla
realtà oggettiva.  
Questo, non corrisponde al vero, perché l’"oggettività" così ricavata, non è
altro che un’intellettualizzazione del distillato di realtà dispensato dai
propri sensi e dai processi di selezione.

A questo punto ci si potrebbe chiedere: “ma se una grandissima quantità di
individui diversi, con moltissimi strumenti a disposizione, si incontrasse ed in
una grande discussione e mettesse assieme tutte le proprie singole "oggettività
personali", non si arriverebbe a descrivere la realtà?”.  
No, perché, sebbene siamo individui diversi, con conoscenze diverse e punti di
vista differenti, siamo comunque *macchine che funzionano in modo simile*. Non
ci è possibile essere sicuri che l’oggettività che emergerebbe da quella
situazione, non sarebbe contaminata dalle nostre caratteristiche comuni e quindi
diversa dalla realtà.

La storia umana è piena di esempi di teorie presunte-oggettive, che sono rimaste
tali finché non soppiantate da altre teorie migliorate o addirittura
completamente diverse. Non parlo solo di teorie scientifiche, ma anche
metafisiche, mistiche e religiose: l’idea della creazione divina, soppiantata
dalla teoria dell’evoluzione, la fisica newtoniana fagocitata dalla fisica
quantistica, o la teoria geocentrica sostituita da quella eliocentrica.  
Una fede religiosa è considerata reale solo finche vi sono persone che vi
credono.  
Questi esempi, sono una dimostrazione della natura condivisa di ciò che
chiamiamo ambiguamente oggettività.

In un particolare e preciso momento storico, non è possibile affermare con
certezza che l’oggettività di un determinato argomento, sia vicina in senso
assoluto alla realtà. Nel momento in cui Galileo espresse la sua idea, quella
condivisa, che considereremmo oggettiva, riguardo i moti dei pianeti era
completamente diversa. In seguito, l’idea di Galileo venne ritenuta più vicina
alla realtà e prese il posto di quella precedentemente considerata oggettiva.

L’idea di oggettività di natura condivisa spinge nella direzione della
probabilità, mentre quella di oggettività intrinseca spinge nella direzione
deterministica.

Chi è religioso solitamente sostiene che il divino esiste e che esso sia l’unico
essere ad avere la capacità di conoscere e manipolare veramente la realtà.
Questo non è negabile. Se esistesse un divino, non potrebbe non essere così,
altrimenti non sarebbe diverso da un qualsiasi altro essere. Questo però, non ci
da alcuna prova che il divino esista. L’idea stessa del divino, nella mente
umana, nasce dall’intelletto, con tutti i problemi che ho appena evidenziato. Da
ciò, seguirebbe che l’esistenza di un divino con queste caratteristiche non può
essere provata né spiegata in maggior modo della realtà.

Per questo motivo, spesso viene detto che la realtà intrinseca, il divino e ciò
che viene chiamato soprannaturale, non può essere conosciuto attraverso la
ragione, l’intelletto, ma solo sperimentandolo, ipotizzando così, l’esistenza di
una sorta di *coscienza pre-intellettuale*.

Le persone razionali che si affidano al metodo scientifico, sono anch’esse
soggette ai problemi intellettuali che ho descritto. Un vantaggio della scienza,
è che ammette implicitamente proprio questo. Essa fornisce probabilmente il
metodo più sicuro per *potersi accorgere di sbagliare*.  
Il modo in cui lo faccia, è ancora argomento di discussione fra filosofi della
scienza, partendo da chi propone uno schema molto rigido, come i
[popperiani][1], passando per schemi meno rigidi, ma con più elementi in gioco,
come per [Lakatos][2], per finire all’anarchia di [Feyerabend][3].  
Tutto ciò è assente nelle religioni dogmatiche. I dogmi, infatti, tendono ad
autoconfermarsi. Nonostante questo, la scienza non può dare garanzie di
comprendere la realtà, ma di avere meno possibilità di rimanere intrappolati in
idee che portano a false oggettività.

In conclusione, la realtà potrebbe essere deterministica, ma i metodi per
giungere ad essa, se devono passare attraverso l’intelletto, facendo quindi
tendere l’oggettività verso di essa, non possono che essere probabilistici.

[1]:https://it.wikipedia.org/wiki/Karl_Popper
[2]:https://it.wikipedia.org/wiki/Imre_Lakatos
[3]:https://it.wikipedia.org/wiki/Feyerabend
