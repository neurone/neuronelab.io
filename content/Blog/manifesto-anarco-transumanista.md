title: Manifesto Anarco-Transumanista
date: 2017-09-07 19:43
tags: anarchia, transumanesimo, tecnologia, filosofia, filosofia della mente, politica, veg, etica, libertà

Pubblico questa traduzione in italiano della versione datata 15 gennaio 2017 del [Anarchist-Transhumanist Manifesto][9]. Il manifesto è ancora un lavoro in corso, quindi questa traduzione va considerata un'istantanea in data odierna del lavoro originale. Eventuali aggiornamenti potrebbero venire riportati in ritardo, o non riportati.

L'anarco transumanesimo è una branca dell'anarchismo che, tramite l'approccio etico e pratico dell'anarchismo sindacalista e socialista, si estende in modo critico alle idee del transumanesimo dando enfasi allo sviluppo culturale e scientifico ed alle sue applicazioni pratiche. Mira a liberare la ricerca scientifica dal giogo del capitalismo, utilizzandola per espandere le capacità e le libertà degli individui e delle comunità.

Scaricamenti:
-------------
- Versione OpenDocument ODT: [Manifesto_Anarco-Transumanista.odt][1]
- Versione PDF: [Manifesto_Anarco-Transumanista.pdf][2]

Articoli a tema (o nei dintorni):
----------------------------------------------------------------------
- [Robota][3]
- [Political Transhumanism is a Deep, Dark Rabbit Hole][4]
- [L'incoerenza e precarietà del Transumanesimo Non-Anarchico][5]
- [ⒶH+ Domande frequenti][6]
- [La società è troppo complessa per avere un presidente, suggerisce la teoria della complessità][7]
- [La posizione dei primitivisti verso la scienza][8]
- Ed in generale gli articoli con tag "filosofia della mente"

Commenti
--------

**Enrico Sanna** _Venerdì 8 Settembre 2017_

Complimenti per il lavoro! Ho scaricato la versione pdf e ho cominciato a leggerla. Mi sembra molto buona e interessante.

---

**Tichy** _Venerdì 8 Settembre 2017_

Grazie! Non pensavo che seguissi il mio blog. Hai più esperienza di me in traduzioni, quindi se troverai qualche bruttura (ce ne sono sicuramente), segnalamela pure.
Mi è capitato più volte di non accorgermi che la forma di qualche frase fosse troppo contorta, perché avevo ancora in mente l'originale in inglese nel quale, invece, funzionava...

---

**Enrico Sanna** _Venerdì 8 Settembre 2017_

Capita anche a me, soprattutto a distanza di qualche tempo. Comunque mi sembra molto buona anche la forma. E poi è stato tradotto in tempi relativamente brevi, vista la lunghezza.


[1]:{filename}/misc/Manifesto_Anarco-Transumanista.odt
[2]:{filename}/pdfs/Manifesto_Anarco-Transumanista.pdf
[3]:{filename}/Blog/robota-parte-1.md
[4]:{filename}/Blog/political-transhumanism-is-a-deep-dark-rabbit-hole.md
[5]:{filename}/Blog/l-incoerenza-e-precarieta-del-transumanismo-non-anarchico.md
[6]:{filename}/Blog/anarco-transumanismo-domande-frequenti.md
[7]:{filename}/Blog/la-societa-e-troppo-complessa-per-avere-un-presidente.md
[8]:{filename}/Blog/la-posizione-dei-primitivisti-verso-la-scienza.md
[9]:https://docs.google.com/document/d/1wJrXYBXAmNH9zwyfgg1-yAYN_Cda-26pFCk0u_QhyBc/edit?pli=1
