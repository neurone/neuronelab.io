title: Utopie
date: 2013-10-27 15:08
tags: utopie, filosofia, evoluzione
summary: Comunemente pensiamo all'utopia come ad un traguardo da raggiungere per ottenere la perfezione per la società.

Comunemente pensiamo all'utopia come ad un traguardo da raggiungere per ottenere la perfezione per la società.

Anche se è più intuitivo pensare alla perfezione ed all'utopia come a qualcosa
di statico e finale, mi sembra più logico immaginarla come adattamento continuo.

La staticità, infatti, ci renderebbe presto inadeguati ed obsoleti, perché il
resto dell'ambiente non smetterebbe di mutare.

Come un surfista che, per andare avanti, deve mantenersi costantemente in
equilibrio, adattarsi all'onda. Se l'arrivo fosse l'utopia, cesserebbe di
esserlo nel momento in cui lo raggiungiamo.
