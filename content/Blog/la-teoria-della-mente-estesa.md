title:  La teoria della mente estesa (Nascere cyborg)
date:   2015-08-03 19:42
tags:   filosofia, filosofia della mente, scienza, neuroscienze, psicologia, cyberpunk
summary: Riporto un mio vecchio articolo del 2011. In coda i commenti lasciati sul vecchio blog.

Riporto un mio vecchio articolo del 2011. In coda i commenti lasciati sul vecchio blog.

> As our worlds become smarter, and get to know us better and better, it
> becomes harder and harder to say where the world stops and the person begins.
> [...]
> We cannot see ourselves aright until we see ourselves as nature's very own
> cyborgs: cognitive hybrids who repeatedly occupy regions of design space
> radically different from those of our biological forbears.  
> **[Andy Clark][1], da [Natural born cyborgs?][2]**

![Stelarc]({filename}/images/mente-estesa1.jpg){: style="float:right"}

Il filone fantascientifico del [cyberpunk][3], ha reso popolari le idee del
movimento culturale [transumanista][2], il quale auspica l'aumento delle
capacità fisiche e cognitive dell'uomo, mediante la tecnologia.
Nell'immaginario cyberpunk, questo avviene mediante la modifica del proprio
corpo, tramite innesti cibernetici. Questo genere di modifiche, trasforma le
persone soggette in una particolare variante di [cyborg][5], degli organismi
cibernetici, degli esseri umani potenziati. Tuttavia, possiamo notare che
attualmente siamo già degli umani potenziati dalla tecnologia che abbiamo a
disposizione quotidianamente e che utilizziamo con regolarità. Un'osservazione
che può apparire banale, ma che stimola ad elaborare una teoria della mente
diversa dalle precedenti basate su riduzionismo materialista o su un dualismo
che tiene rigidamente separati gli stati mentali da quelli materiali. La teoria
della mente estesa, può poggiare su un approccio di tipo funzionalista o
emergentista.

Definiamo meglio questi -ismi. Con [riduzionismo materialista][6] si intende
che gli stati mentali sono un'espressione del sistema nervoso, al quale si può
far corrispondere esattamente ogni configurazione di essi. Secondo il R.M., la
mente non può essere separata dal corpo, e lo studio della neurofisiologia è
sufficiente a comprenderla. All'opposto, il [dualismo sostiene][7]
l'irriducibilità degli stati mentali alla materia. Mente e materia sono
separati e non possono in alcun modo interagire.

In mezzo a questi due opposti si collocano [funzionalismo][8] ed
[emergentismo][9]. Il primo termine fu usato per la prima volta da [Hilary
Putnam][10] come opposizione al riduzionismo, basandosi sull'idea delle
[molteplici realizzazioni][11]. Ovvero: un particolare stato mentale non può
essere identico ad un certo stato cerebrale, perché i singoli stati mentali che
appartengono a quel tipo di stato cerebrale possono essere implementati da
stati cerebrali di genere diverso. Questo comporta che, data la molteplice
realizzabilità degli stati mentali, essi possono manifestarsi anche in sistemi
diversi da quello umano, ad esempio in calcolatori. L'idea di mente come
software che gira sopra ad un hardware di carne e neuroni, nasce da qui.

La teoria della mente estesa, come il funzionalismo, assume che la mente sia
indipendente dalla materia, la quale può anche essere di tipi diversi,
dall'organico, all'inorganico a combinazioni di questi, come vedremo più
avanti.

L'emergentismo è una corrente filosofica che parte dallo studio dei sistemi
complessi. In essa, la mente è una proprietà emergente dell'interazione di più
elementi di un sistema. In poche parole, da un sistema dinamico possono
scaturire, in modi completamente auto-organizzati, dei comportamenti che, nello
studio dei singoli componenti, non si sarebbero potuti prevedere. In questo
modo, l'emergentismo può permettersi di non rifiutare completamente tutte le
varie categorie epistemologiche contrapposte, quali meccanicismo e vitalismo,
monismo materialista e dualismo cartesiano, riduzionismo e olismo, oggettivismo
scientista e soggettivismo umanistico, ma di prendere da esse le parti utili e
di fare da tramite, mediante l'idea di quid emergente.

Un filosofo emergentista di cui ho già scritto su questo blog è Douglas R.
Hofstadter, mentre Daniel C. Dennett, altro filosofo di cui ho scritto, si
autodefinisce un funzionalista. La teoria della mente estesa, abbatte i presunti
limiti definiti dal confine del cervello fisico e dall'epidermide. Dal cervello,
partono assoni che vanno ad innervare tutto il corpo umano, formando così il
sistema nervoso, il quale è un sistema dotato di retroazione. Lo stesso feedback
è presente nell'interazione con l'ambiente, ad un livello superiore rispetto ai
"semplici” scambi chimici ed elettrici che avvengono all'interno del corpo. Un
cervello che nascesse senza corpo e completamente isolato, non potrebbe produrre
una mente come la intendiamo ora. Lo stesso accadrebbe con un essere vivente
nato e rimasto completamente isolato dall'esterno. Gli esperimenti sulla
deprivazione sensoriale sono indicativi: la mente ha bisogno di continue
interazioni con l'esterno per funzionare nel modo in cui conosciamo. In assenza
completa di interazione ambientale, la mente, se possiede già dei ricordi, tende
a sopperire mediante allucinazioni. Solo grazie a questo sistema dinamico
formato da ambiente, corpo e cervello, possono emergere le menti che conosciamo.
Questo viene chiamato approccio incarnato e situato.

> Il mondo non è una costruzione del cervello, non è il prodotto dei nostri
> propri sforzi coscienti. C'è per noi, noi siamo al suo interno. La mente
> cosciente non è dentro di noi; sarebbe meglio dire che essa rappresenta una
> forma di attiva sintonia con il mondo, un'integrazione realizzata. È il
> mondo stesso che fissa la natura dell'esperienza cosciente.  
> **Alva Noe (Perché non siamo il nostro cervello), filosofo di posizione
> enattivista.**

![Ciclista]({filename}/images/mente-estesa2.jpg){: style="float:left"}

Essendo tutto parte di un sistema dinamico complesso, diventa utile analizzare
il modo in cui i vari componenti interagiscono fra loro. Osservando il
comportamento e la storia del genere umano, possiamo accorgerci di quanto esso
modifichi continuamente l'ambiente per adattarlo alle proprie esigenze. Al
tempo stesso, dal punto di vista biologico, gli organismi umani si adattano
all'ambiente di generazione in generazione nel modo descritto dalla teoria
dell'evoluzione. Dal punto di vista cognitivo, l'essere umano apprende
continuamente nuovi modi per interagire con l'ambiente. Sia per
sperimentazione, che per imitazione. Modifica i propri schemi mentali per
renderli adatti a vivere nel luogo e nel tempo in cui è situato. Possiamo
quindi dire che la mente umana non è assolutamente isolata e separata dal mondo
e non c'è una totale distinzione fra interno ed esterno. Dal momento in cui
l'uomo inizia a costruirsi strumenti o anche solo ad interagire abitualmente
con parti dell'ambiente, queste diventano parte degli schemi mentali
dell'individuo. Ogni volta che esso vorrà fare un'azione, sarà automatico per
lui utilizzare questi supporti esterni al proprio corpo, con la stessa
naturalezza con cui esso utilizza gli strumenti del corpo. L'uso di essi ci
permette di eseguire compiti che altrimenti sarebbero decisamente più complessi
e forse irrealizzabili. Questo crea un circolo nel quale l'entità degli
adattamenti ambientali e degli adattamenti soggettivi che operiamo, aumenta
esponenzialmente.

Secondo Andy Clark e David Chalmers, le condizioni che un supporto esterno deve
soddisfare per avere una continuità con la mente sono:

1. Il supporto deve sempre essere disponibile ed essere usato regolarmente,
   cioè deve essere una costante nella vita della mente soggettiva di cui è
   candidato a essere estensione.
2. L'informazione contenuta nel supporto deve essere facilmente accessibile.
3. L'informazione contenuta nel supporto deve essere accettata come valida in
   modo automatico.

Alcuni esempi: pensiamo al bastone di un cieco, alla calcolatrice di un
contabile, all'automobile, al computer o al telefono cellulare sempre presenti,
all'orologio da polso. Il ciclista che sente le asperità del terreno trasmesse
tramite le vibrazioni della bicicletta, gli automatismi con i quali governa la
bicicletta e la fa muovere, curvare, frenare, senza dover coscientemente e
razionalmente decidere quali parti del proprio corpo muovere per compiere
quella particolare macro-azione. Semplicemenete pensa che deve rallentare, ed
in automatico fa rallentare tutto il sistema formato da lui più la bicicletta.
Il nostro corpo ed il cervello in esso contenuto, potrebbero essere considerati
come un'interfaccia fra la nostra mente emersa e l'ambiente. La nostra
epidermide diventa un confine solamente materiale fra il corpo e le altre
sostanze presenti nel cosmo, mentre dal punto di vista mentale non vi è alcun
confine tranne quello sfumato definito dalla possibilità di interazione.
Quindi, il cervello ed il corpo si adattano all'ambiente, il corpo modifica
l'ambiente adattandolo a sé, e la relazione fra cervello ed ambiente si fa
sempre più stretta. Al contrario di ciò che si potrebbe pensare, le capacità
cognitive non diminuiscono a causa degli strumenti a cui ci abituiamo, ma si
spostano dalla manipolazione diretta dell'ambiente naturale, all'utilizzo di
interfacce che permettono tale manipolazione.

![Mente estesa su xkcd]({filename}/images/mente-estesa3.png)

> Interessante a questo proposito l'esperimento di Maguire (2003), in cui si
> dimostra che il volume di materia grigia dell'ippocampo dei taxisti
> londinesi è superiore rispetto a quello dei comuni individui e
> proporzionale agli anni di carriera trascorsi nel taxi. In sostanza,
> secondo Malafouris (2010), l'esperimento potrebbe suggerire che il download
> delle memorie inerenti alla mappa topografica della città nei moderni
> sistemi GPS, potrebbe di fatto liberare spazio neurale all'interno
> dell'ippocampo, spostando il carico dal cervello all'oggetto-navigatore. Se
> ora si estende l'idea di Malafouris ad ogni fenomeno di download neurale
> possibile, sembrerebbe che l'avanzamento tecnologico porti ad un
> collaterale svuotamento del cervello, rasentando una prospettiva che
> ricalca quella descritta dal film "Idiocracy", nel quale un'ipotetica
> umanità del futuro ha sviluppato tecnologie così avanzate da ridurre
> qualunque sforzo cognitivo al minimo, schiacciando il QI medio a livelli
> infimi.
>
> Tuttavia, una prospettiva apocalittica di questo tipo non mi ha mai
> convinto seriamente e oltretutto mi è sempre parsa contrastare con le
> idee sviluppate dagli archeologi cognitivi e più volte discusse in
> questo blog. Se infatti nuove sfide ambientali avrebbero condotto in
> passato alla necessità di sviluppare nuove tecnologie litiche,
> richiedendo l'avvento di funzioni cognitive più avanzate, non è chiaro
> perché il processo dovrebbe invertirsi all'aumentare della complessità
> tecnologica degli artefatti realizzati dagli esseri umani. Il punto
> della questione, che scongiurerebbe l'ipotesi Idiocracy, è nuovamente
> nel concetto di interfaccia: per far funzionare correttamente i nostri
> contemporanei artefatti potrebbero essere necessarie nuove e più
> complesse funzioni di interfaccia tra realtà e cervello, sicchè le reti
> neurali ora libere verrebbero riciclate a questo scopo e le interfacce
> cervello/realtà continuamente sostituite o assemblate in nuove
> combinazioni più complesse, al crescere del livello tecnologico di
> fondo (Cfr. Dehaene, 2005; Malafouris, 2010 - Hypothesis of "Neuronal
> recycling”). Risultato di tutto ciò potrebbe essere una mente
> differente dalle varianti meno estese del passato, ma capace di
> confrontarsi probabilmente in maniera più efficace con le sfide del
> mondo a venire.
> 
> **dal blog [neuro@ntropologia][12] - [Neureplenish!][13]**

Commenti
--------
**[D Garofoli][12]**
*on 5 luglio 2011 at 15:25*

Grazie per la citazione. E´sempre un piacere leggere di questi argomenti sulla
blogosfera, prova che non tutti si sono arresi alle solite banalitá e
chiacchiere da bar che si trovano mediamente in giro per i blog.

Comunque, un´idea che sta emergendo in questi ultimi tempi é che il concetto di
metaplasticitá, aka la plasticitá del sistema nervoso in connessione con la
plasticitá culturale, che in pratica continuiamo a tirare in ballo con nomi
diversi, sembrerebbe essere in relazione anche con la forma del corpo, sia
durante lo sviluppo che durante l´evoluzione umana. Quindi é fondamentale
inserire anche il corpo e le sue variazioni in questa serie di biofeedback ed
in particolare cercare di comprendere come le variazioni nella struttura del
corpo vadano ad influenzare la struttura della mente e quella delle pratiche
culturali connesse, seguendo la logica dell´embodiment. Una sfida davvero
impegnativa….

A presto!

**Masque**
*on 5 luglio 2011 at 16:12*

Mi era piaciuta molto la parte che ho ripreso dal tuo blog. Volevo spiegare la
stessa cosa, ma avendola trovata già così bene esposta, ho preferito riportarla
per intero :) Sono convinto anch'io che la struttura del corpo influenzi la
struttura della mente e certamente anche l'ambiente ha altrettanta influenza.
Anche se preferisco non fare distinzioni nette fra soggetto ed ambiente… mi è
difficile esprimere con le parole certe cose. specialmente perché sono
cresciuto in una società nella cui cultura, il dualismo, che tendo a rifiutare,
è molto presente.  In una delle prossime parti, intendevo scrivere di qualcosa
di simile, anche se non avevo ancora visto usare il termine metaplasticità. Ho
anche un articolo della mia ex professoressa di filosofia della mente, che
parla di emozioni in un contesto incarnato, da cui potrei attingere. :) Quanto
alla modifica del corpo, potrebbe essere interessante anche uno studio
antropologico, specialmente sui rituali ed i trucchi e le modifiche che i
membri delle popolazioni tribali fanno su se stessi. Quanto la "trasformazione”
esteriore dell'uomo in sciamano, gli permette di ottenere gli schemi mentali di
uno sciamano prototipale. O in società più vicine alla notra, l'uso di abiti
"speciali” per certi eventi particolari. Inoltre, buona parte degli attori,
trova più semplice entrare nel personaggio, se prima modifica se stesso per
apparire come il personaggio che vuole interpretare.

In futuro mi piacerebbe anche parlare dell'esternalismo radicale, che è la
corrente a cui mi sento più vicino. Ma non ho ancora preparato nulla a parte
delle bozze, quindi vedremo cosa ne emergerà ;)

**[D Garofoli][12]**
*on 6 luglio 2011 at 09:14*

Non so qual é il tuo percorso accademico, ma io ad esempio mi sono trovato
sempre in un ambiente in cui un monismo ingenuo (sistema nervoso = mente) é
stato sempre combinato con un cognitivismo estremo, producendo un risultato
estremamente riduzionista e limitato.  Tornando al punto, consiglio di leggere
il seguente paper, che potrebbe essere realmente ció che stai cercando:

http://scan.oxfordjournals.org/content/early/2010/01/19/scan.nsp057

Sull´esternalismo radicale, vai avanti please. Magari cerchiamo di capire il
punto cruciale di questo approccio e cioé tutto il blocco metafisico. Io per
ora non riesco proprio a capire i passaggi da esternalismo attivo al concetto
di "coscienza come esistenza”….

**Masque**
*on 6 luglio 2011 at 10:50*

Ho frequentato per qualche anno la facoltà di scienze cognitive. Lì, c'erano
varie correnti. Sia il riduzionismo di cui parli, sia approcci più olistici.
Solitamente il primo era più presente nelle materie vicine alla
neurofisiologia. L'insegnante di filosofia della mente, al contrario, seguiva
la teoria della mente estesa.  Quando ho scritto del dualismo, mi riferivo al
pensiero ingenuo che è diffuso al di fuori dell'ambito accademico. (qua avevo
abbozzato qualche idea:
https://neuroneproteso.wordpress.com/2010/12/05/dualismo-incarnato/ ) Ho letto
l'abstract dell'articolo che mi hai segnalato. Penso che mi potrà essere utile.
Grazie :) Sull'esternalismo… vedremo cosa ne uscirà :)

**[Gianluca Bartalucci][14]**
*on 6 luglio 2011 at 16:10*

Articolo interessantissimo, che mi riprometto di commentare (magari scrivendo
un post a tema) sul blog, appena ho un po' di tempo - ultimamente è un casino.
:)

Il mio punto di vista è che su argomenti del genere dipenda, appunto, dal punto
di vista: ad un livello altissimo non vedo come le idee dell'esternalismo - per
dirne una - possano essere messe in discussione. Chi le nega, a mio avviso, sta
solo guardando la cosa da un altro punto di vista o - più banalmente- non ha la
sufficiente capacità di astrazione.

**Masque**
*on 6 luglio 2011 at 17:41*

Mi piacerebbe leggere un tuo post sull'argomento :) L'esternalismo, al momento,
penso che sia la posizione, dal punto di vista logico, più coerente e quella in
grado di dare le spiegazioni più soddisfacenti (anche se certamente
controintuitive).

**liongalahad**
*on 3 gennaio 2012 at 09:27*

non so se ne hai mai sentito parlare, ma volevo porre alla tua attenzione
questi due video che riguardano il Blue Brain Project. 

(NB: i video non sono più disponibili)

guardali entrambi… e se ti va guarda anche questa conferenza del padre del
progetto, Henry Markram

Per me siamo di fronte al più incredibile e affascinante progetto mai concepito
dall'uomo. E la sensazione è che siamo molto più vicini all'intelligenza
artificiale di quanto pensiamo, anche se attraverso un approccio totalmente
diverso da quello, diciamo così, "tradizionale”… speriamo che i finanziamenti
al progetto continuino

**Masque**
*on 3 gennaio 2012 at 20:03*

non ne avevo mai letto approfonditamente, ma non mi è nuovo. è molto
affascinante, infatti. sono curioso di vedere, quando riusciranno a
completarlo, se troveranno il modo di farlo funzionare e cosa potranno
scoprire.

così, mi parrebbe un approccio quasi solo riduzionista, di cui non sono un
grande fan :) mi rimane infatti un dubbio: se davvero riuscissero a costruire
una replica perfetta di un cervello, neurone per neurone, e funzionasse, cosa
potremmo apprendere riguardo cosa sia la mente? :)

**liongalahad**
*on 4 gennaio 2012 at 09:22*

L'approccio è diverso perché non c'è nulla di fisico (se non il supercomputer,
che però non avrebbe proprio nulla di intrinsecamente intelligente) ma è
completamente virtuale. Il cervello sarebbe una mera simulazione perfetta di un
cervello umano. Sono molteplici gli spunti per speculazioni sulle possibili
conseguenze del successo di tale esperimento.

Prima di tutto verrebbe confermata, secondo me, la teoria emergentista alla
Hofstadter, in quanto da un sistema del tutto privo di intelligenza come un
computer emergerebbe, grazie solo e unicamente all'organizzazione "software” e
non quella meramente seriale dei suoi processori e dei nano-transistors in essi
contenuti. Ad un livello più elevato del sistema, abbiamo un mirabile esempio
di "autorganizzazione”, che ricorda in parte l'analogia del formicaio che
Hofstadter fa nel suo GEB. Infatti il Blue Brain Project ha alla base la
simulazione software delle cellule cerebrali, ogni tipologia di neurone (una
cinquantina in tutto, mi pare) ha il suo corrispettivo in un modello software
perfettamente replicato dagli ingegneri del BBP. L'organizzazione degli stessi
neuroni virtuali e le varie interazioni fra loro segue ovviamente
minuziosamente le direttive dei neurologi e dei neuro-fisiologi, a replicare a
livello software le connessioni HARDWARE presenti nel cervello. Fin qui nulla
di nuovo, soltanto un monumentale lavoro di replicazione del cervello. La cosa
però che ha stupito tutti è stato quando, creata la prima colonna neuronale
virtuale, a simulare una colonna neuronale della corteccia cerebrale di un
mammifero (le colonne neuronali sono unità relativamente semplici del cervello,
costituite da circa 10.000 neuroni, e sono uguali in tutti i mammiferi) si è
visto come, immettendo un impulso elettrico in essa, la colonna neurale reagiva
ESATTAMENTE come una colonna neurale vera, e questo senza che nessun ingegnere
del software ne avesse progettato il complesso comportamento. Semplicemente la
suddetta colonna neurale, partendo dalle semplici regole che determinano il
funzionamento e il comportamento di ogni singolo neurone, è riuscita a simulare
AUTONOMAMENTE il comportamento enormemente più complesso di una colonna
neurale. Il punto è tutto qui. Un po' come se progettassimo la versione
software di una formica e mettessimo le semplici regole di interazione fra
formiche…arriveremmo infine ad ottenere una precisa replica virtuale del
comportamento, assai più complesso, di un formicaio.

Il passo successivo ovviamente è quello di porre insieme le milioni di colonne
neurali, con lo stesso principio, per poi ottenere a livello ancora più
elevato, il comportamento iper-complesso di un cervello, con tutte le sue
funzioni, coscienza compresa, la quale emergerebbe anch'essa da una complessa
autorganizzazione delle colonne neurali (la sto facendo un po' a grandi linee
ma più o meno dovrebbe essere così) e non da una progettazione umana "diretta”
che sarebbe, visto lo stato attuale delle cose e le nostra effettiva conoscenza
del funzionamento "software” del cervello, semplicemente impossibile.

Se avranno successo, e non è assolutamente detto data la complessità del
progetto, ma se dovessero farcela la nostra comprensione della mente sarà,
credo, totale. Sarà possibile studiare la reazione del cervello, ad ogni suo
livello, di fronte a qualsiasi tipo di stimolo, sarà possibile tenere
monitorato ogni singolo neurone, ogni colonna di neuroni, ogni settore del
cervello, e farlo interagire con qualsiasi cosa o stimolarlo in ogni modo
immaginabile ed ottenere ogni volta una mole di dati e informazioni oggi
impensabili. E sapremo che la teoria emergentista (che trovo assai convincente)
sia quella esatta.. Va da se che avremo moltissime altre conseguenza,
soprattutto in campo medico e farmaceutico….

Un'altra conseguenza potrebbe essere che, dopo aver creato questa mente
cosciente, intelligente e perfettamente funzionante, e aver compreso i minimi
dettagli del suo funzionamento ( e dunque anche del nostro) forse sarà
possibile capire quali sono le cause che determinino l'intelligenza, nel
senso…. che cosa determina un QI più o meno alto? Compreso questo sarà
relativamente semplice creare la versione virtuale del più intelligente
cervello umano possibile. Da qui alla singolarità tecnologica il passo è breve
poiché tale cervello avrebbe sì sembianze "virtuali” umane, ma sarebbe di gran
lunga molto più intelligente di qualsiasi cervello vero e dunque più
intelligente dell'uomo stesso. Esso potrebbe cominciare a studiare sistemi più
complessi del cervello stesso e generare un essere ancora più intelligente ..
eccetera eccetera. Tutto sta nello stabilire se sia possibile per un sistema
(in questo caso la mente umana) da solo, e senza autorganizzazioni di livello
superiore, riuscire a creare un sistema più complesso di se stesso, ma questa è
una questione filosofica a cui non è possibile dare una risposta..

Un'ultima conseguenza, leggermente off-topic in verità, a cui ho pensato è
questa: se è fattibile una versione virtuale "pensante” del cervello umano, che
è di gran lunga il sistema più complesso che esista in natura, allora nulla
vieta di poter, in futuro, una volta raggiunti livelli sufficienti della
conoscenza della realtà fisica, creare una versione virtuale dell'Universo. Mi
spiego: creiamo un sistema virtuale in cui valgano una ad una tutte le leggi
fisiche conosciute, con tutte le regole di interazione fra le particelle, ecc
ecc … A quel punto avremo creato un vero e proprio universo virtuale, che non
necessiterà di nulla ma che basterà osservare per apprezzarne la naturale
evoluzione. Al suo interno, se progettato davvero bene, potrà addirittura
sviluppare la vita. E qui mi fermo, la testa comincia a girare.

mi scuso per la lungaggine e per il linguaggio magari poco curato ma ho scritto
un po' in fretta e non ho troppo tempo per ricontrollare tutto :)

**Masque**
*on 4 gennaio 2012 at 13:37*

hai scritto benissimo :)

la speculazione finale, mi ha fatto venire in mente il racconto "Non serviam”
di Stanislaw Lem. C'è sulla raccolta "L'io della mente” curata da Dennett e
Hofstadter. L'avevo anche trascritto qua.  dal mio punto di vista, il fatto che
sia ricreato via software piuttosto che, per esagerare, meccanicamente, non
dovrebbe cambiare nulla (a parte che via software è molto più comodo fare
modifiche ed esperimenti). fintanto che, ciò che conta, sono le relazioni fra
le varie parti. come hai scritto giustamente, se funzionasse come vorremmo,
avremmo conferma delle teorie emergentiste e fino ad un certo livello anche del
funzionalismo (l'indipendenza dalla materia, ad esempio).  quello che mi
piacerebbe di meno, sarebbe il non avere una risposta alla domanda "perché
emegra una mente, è necessario un cervello fatto esattamente come quello delle
specie animali che conosciamo?”. avremmo la conferma che da un sistema
costruito esattamente come i cervello umano, otteniamo una mente, ma mi
piacerebbe scoprire, anche, come potrebbero essere possibili delle menti basate
su sistemi compleamente diversi. insomma… solaris è possibile? :)

comunque, come esperimento lo ritengo davvero molto importante. è vagamente
paragonabile alla mappatura del genoma umano. :)

**liv**
*on 15 gennaio 2012 at 13:52*

Piacerebbe anche a me che Solaris fosse possibile!  Mi potreste suggerire
qualche saggio (in italiano) su scienze cognitive? Sono incerta se iniziare da
Hofstader… Se ci sono equazioni non mi spavento ;-)

**Masque**
*on 15 gennaio 2012 at 15:16*

bella domanda :D l'argomento è vastissimo e, ti dirò, pure io sono molto
disordinato… (tant'è che, come vedi, la seconda parte di questo post non l'ho
ancora fatta! :D) se ti trovi a tuo agio con logica e matematica, vai con
goedel escher bach. comunque lì è molta logica, teoria e filosofia. non ci sono
molte cose su esperimenti cognitivi.  altrimenti, per una panoramica sulle
questioni di filosofia della mente, c'è la bella raccolta "l'io della mente”
che contiene racconti di vari autori, fra cui anche lem, commentati da
hofstadter e dennett.

per qualcosa di più "applicato”, magari qualche libro di oliver sacks o
ramachandran.  penso che qualche spunto lo potresti trovare anche sul blog di
gianluca. lui legge tantissimi libri sull'argomento:
http://some1elsenotme.wordpress.com/ altrimenti, se vuoi dare una sbirciata
alla mia libreria su anobii: http://www.anobii.com/masque/books ma
probabilmente ci troverai più fantascienza, che saggi :)

**liv**
*on 15 gennaio 2012 at 22:53*

Bene bene, ho già aperto il blog di Gianluca. Vedo che anche lui è per le
letture disordinate :-) Tra parentesi, ho dato un'occhiata ad Anobii, per la
fantascienza - tutti autori di altissimo livello! Tornando in tema, tra i libri
che mi suggerisci penso che inizierò da GEB o da "L'Io della mente”.

**Masque**
*on 15 gennaio 2012 at 22:59*

ottimo! se poi avrai voglia di scrivere da qualche parte delle considerazioni,
dillo! mi piace leggere cosa ne pensano gli altri su questi argomenti :)

geb/l'io della mente: sono entrambi lunghini, ma il secondo è decisamente meno
impegnativo. può essere un buon antipasto che farà venire voglia di "mangiarne”
ancora :)

[1]:https://en.wikipedia.org/wiki/Andy_Clark
[2]:http://edge.org/conversation/natural-born-cyborgs
[3]:https://it.wikipedia.org/wiki/Cyberpunk
[4]:https://it.wikipedia.org/wiki/Transumanesimo
[5]:https://it.wikipedia.org/wiki/Cyborg
[6]:https://it.wikipedia.org/wiki/Riduzionismo_(filosofia)
[7]:https://it.wikipedia.org/wiki/Dualismo_(filosofia_della_mente)
[8]:https://it.wikipedia.org/wiki/Funzionalismo_(filosofia_della_mente)
[9]:https://it.wikipedia.org/wiki/Emergentismo
[10]:https://it.wikipedia.org/wiki/Hilary_Putnam
[11]:https://it.wikipedia.org/wiki/Molteplici_realizzazioni
[12]:https://neuroantropologia.wordpress.com/
[13]:https://neuroantropologia.wordpress.com/2010/09/20/neureplenishin/
[14]:https://some1elsenotme.wordpress.com/
