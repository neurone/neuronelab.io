title: La fault tolerance dei sistemi di governo
date: 2015-06-23 12:55:17 +0200
tags: politica, società, anarchia, sistemi distribuiti
summary: L’importanza della tolleranza ai guasti, detta anche *fault tolerance*, deriva dall’assunto che non sia sufficiente chiedersi *se* avverrà mai un guasto, e quindi lavorare per evitare che avvenga, ma dare per scontato che i guasti *avvengono sempre* ed il punto cruciale diventa *quando* e *come salvarsi* in quei casi.

L’importanza della [tolleranza ai guasti][1], detta anche *fault tolerance*, deriva dall’assunto che non sia sufficiente chiedersi *se* avverrà mai un guasto, e quindi lavorare per evitare che avvenga, ma dare per scontato che i guasti *avvengono sempre* ed il punto cruciale diventa *quando* e *come salvarsi* in quei casi.

Una gestione che privilegi una organizzazione di tipo centralizzato e
gerarchico, cercherà di evitare il più possibile la caduta dell’unità centrale
di governo, fortificandolo e difendendolo il più possibile. Molte risorse
verranno quindi usate per la preservazione e la manutenzione di esso, perché un
errore o un guasto si ripercuoterebbero su tutta la rete e su tutti gli utenti.

Nel caso di un organizzazione [fra pari][2], *peer-to-peer*, si privilegia la
sopravvivenza della rete e la continuità di servizio per gli utenti, i quali
sarebbero utilizzatori e contemporaneamente produttori. Lo si fa, evitando
d’assegnare troppo peso a pochi singoli nodi e distribuendolo, bilanciandolo con
opportune regole. Lo scopo è di far sì che un errore o un guasto su un singolo o
su gruppi isolati di nodi, non si ripercuota sull’intera rete e quindi sugli
utenti.

Dopo queste premesse, si può tentare di trovare delle analogie con i sistemi di
organizzazione della società, tenendo bene a mente che vi sono sempre diversi
sistemi di governo che si sovrappongono ed intrecciano. La società non è
influenzata, governata, determinata esclusivamente dal governo statale, ma anche
da mercato, religioni, media e relazioni fra gruppi che si possono
contraddistinguere per interessi, ideologie o amicizia.

Le società governate centralmente sono più simili ad una gestione di tipo
*client-server*. Più un governo accentra i poteri, più diventa totalitario, e
più esso necessita di fortificare e proteggere il proprio nucleo centrale.
Grande parte delle risorse verrà utilizzata per mantenere funzionante e di
sufficiente potenza il centro del sistema. Il motivo è già stato spiegato: in
caso di errore o di guasto o caduta di esso, tutta la rete ed i singoli utenti
rischierebbero il tracollo e l’interruzione dei servizi. I singoli utenti
infatti, sarebbero come dei semplici *client*, utilizzatori, consumatori del
servizio, senza possibilità di contribuire al sistema o di modificarlo, perché
non sono “progettati”, ossia educati, per farlo, oppure perché non è permesso
loro. Il governo è fornitore dei beni e dei sevizi di cui i governati
beneficiano e dipendono. La dipendenza è un fattore molto forte, perché
influisce molto sul comportamento delle persone, ad esempio su quanto convenga
anche a loro che il fornitore e tutta la rete rimangano operativi e ben
funzionanti. In caso di "malfunzionamenti", essi se sarebbero gravemente
danneggiati, non potendo provvedere autonomamente ai servizi ed i beni che
venivano altrimenti loro offerti. Conseguentemente la [tolleranza ai guasti][1] di
questo tipo di gestione è molto debole.

Dando per certa la possibilità di errore – umanamente naturale ed inevitabile –
e progettando la rete di relazioni in modo che essa e gli utenti possano
sopravvivere in caso di guasti ed errori si possono limitare di molto i danni in
caso di *guasti*. Si fa questo, rendendo gli utenti dei *peer*, ovvero dei pari,
che siano sia utilizzatori che produttori. Si tenderà all'uguaglianza fra di
essi per evitare che dei singoli possano accrescere il proprio potere. Nel caso
accadesse infatti, troppi altri utenti diventerebbero dipendenti da essi e la
rete si sbilancerebbe. In caso di errore o guasto, un numero maggiore di utenti
ne avrebbe danno. Questo tipo di organizzazione parte dalle [democrazie
dirette][3], fino ad arrivare all'[anarchia][4]. Andando in senso opposto, ad un
maggior accrescimento di potere ed influenza sulla rete da parte di singoli
nodi, si passa attraverso le democrazie rappresentative, fino ad arrivare ai
governi totalitari.

[1]:https://it.wikipedia.org/wiki/Tolleranza_ai_guasti
[2]:https://it.wikipedia.org/wiki/Peer-to-peer
[3]:https://it.wikipedia.org/wiki/Democrazia
[4]:https://it.wikipedia.org/wiki/Anarchia
