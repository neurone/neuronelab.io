title: Come la NSA ha tradito la fiducia del mondo
date: 2013-11-12 23:30
tags: tecnologia, sicurezza, privacy, software libero, rete
summary: I recenti eventi hanno messo in evidenza il fatto che gli Stati Uniti stanno portando avanti un controllo globale su qualunque straniero i cui dati passano attraverso entità americane, che siano sospettati o meno di illeciti.

I recenti eventi hanno messo in evidenza il fatto che gli Stati Uniti stanno portando avanti un controllo globale su qualunque straniero i cui dati passano attraverso entità americane, che siano sospettati o meno di illeciti.

Questo significa sostanzialmente che ogni utente internazionale di Internet è sotto sorveglianza, dice Mikko Hypponen, direttore dell'azienda di sicurezza F-Secure. Un'importante invettiva, abbinata ad una preghiera: trovare soluzioni alternative all'uso di società americane per il bisogno di informazioni del mondo.

Video: TED - [Mikko Hypponen: Come la NSA ha tradito la fiducia del mondo, è tempo di agire][1]

Trascrizione del testo
----------------------

Le due invenzioni più grandi della nostra generazione sono probabilmente
Internet e il telefonino. Hanno cambiato il mondo. Eppure, un po' a sorpresa, si
sono anche rivelate gli strumenti perfetti per lo stato di sorveglianza. È
emerso che la capacità di raccogliere dati, informazioni e connessioni su
praticamente chiunque di noi e su tutti noi è proprio quella che abbiamo sentito
per tutta l'estate attraverso le rivelazioni e le fughe di notizie riguardanti
le agenzie di *intelligence* occidentali, in gran parte agenzie di
*intelligence* statunitensi, che sorvegliano il resto del mondo.
<!--more-->

Ne abbiamo sentito parlare a cominciare dalle rivelazioni del 6 giugno scorso.
[Edward Snowden][2] ha cominciato a far trapelare informazioni, informazioni
classificate top secret, provenienti dalle agenzie di *intelligence* statunitensi,
e abbiamo cominciato a conoscere cose come [PRISM][3], [XKeyscore][4] e altre. E
questi sono esempi del tipo di programmi che le agenzie di *intelligence*
statunitensi stanno impiegando in questo momento contro il resto del mondo.

E se ripensate alle previsioni sulla sorveglianza espresse da George Orwell,
beh, viene fuori che [George Orwell][5] era un ottimista.

*(Risate del pubblico)*

Stiamo assistendo ora a tracciamenti di singoli cittadini su scala molto più
vasta di quanto Orwell avrebbe mai potuto immaginare.

Questo è il famigerato centro dati della NSA nello Utah. Aprirà prossimamente e
sarà sia un centro di supercalcolo che un centro di archiviazione dati. Potete
immaginarlo come una sala immensa, piena di dischi rigidi per l'immagazzinaggio
dei dati che stanno raccogliendo. È un edificio piuttosto grande. Quanto grande?
Posso darvi i numeri – 140 000 metri quadrati – ma questo non vi dice molto.
Forse è meglio immaginarlo con un termine di paragone. Pensate al più grande
negozio IKEA in cui siate mai stati. Questo è cinque volte più grande. Quanti
dischi rigidi ci stanno in un negozio IKEA? Giusto? È piuttosto grande. Stimiamo
che la sola bolletta dell'elettricità per far funzionare questo centro di
archiviazione dati sarà dell'ordine delle decine di milioni di dollari all'anno.
E questo tipo di sorveglianza a tappeto significa che è possibile raccogliere i
nostri dati e conservarli sostanzialmente per sempre, conservarli per lunghi
periodi di tempo, conservarli per anni, per decenni. Questo crea dei rischi
completamente nuovi per tutti noi. Questa è sorveglianza a tappeto, globale, di
tutti.
Beh, non esattamente di tutti, perché l'*intelligence* americana ha solo il
diritto legale di monitorare gli stranieri. Possono monitorare gli stranieri
quando le connessioni dati degli stranieri terminano negli Stati Uniti o passano
per gli Stati Uniti. Sorvegliare gli stranieri non sembra tanto grave, finché
non ci si rende conto che io sono uno straniero e voi siete degli stranieri. Di
fatto, il 96 per cento del pianeta è abitato da stranieri.

*(Risate del pubblico)*

Giusto?

Quindi è in effetti una sorveglianza a tappeto, globale, di tutti noi, tutti noi
che usiamo le telecomunicazioni e Internet.

Ma non fraintendetemi: ci sono dei tipi di sorveglianza che vanno bene. Amo la
libertà, ma anch'io riconosco che certa sorveglianza va bene. Se le forze
dell'ordine stanno cercando di trovare un assassino, o catturare un signore
della droga o prevenire una sparatoria in una scuola, e hanno delle piste e un
sospettato, va benissimo che intercettino le telefonate di questo sospettato e
le sue comunicazioni via Internet. Questo non lo metto affatto in discussione.

Ma non è di questo che si occupano i programmi come [PRISM][3]. Non hanno nulla
a che vedere con la sorveglianza di persone per le quali ci sono buone ragioni
per sospettare che abbiano commesso qualche reato, ma sono sorveglianza di
persone che loro sanno essere innocenti.

Quindi le quattro grandi argomentazioni a sostegno di questo tipo di
sorveglianza sono: prima di tutto, non appena si comincia a discutere di queste
rivelazioni, ci sono quelli che negano cercando di minimizzare l'importanza di
queste rivelazioni, dicendo che sapevamo già tutto, sapevamo che stava
accadendo, non c'è niente di nuovo. E non è vero. Non lasciatevi dire da nessuno
che lo sapevamo già, perché non lo sapevamo. Le nostre peggiori paure potevano
essere qualcosa del genere, ma non sapevamo che stava accadendo. Ora sappiamo
per certo che sta accadendo. Non lo sapevamo. Non sapevamo di PRISM. Non
sapevamo di [XKeyscore][4]. Non sapevamo di Cybertrans. Non sapevamo di
DoubleArrow. Non sapevamo di Skywriter -- tutti questi programmi diversi gestiti
dalle agenzie di *intelligence* statunitensi. Ma ora lo sappiamo.

E non sapevamo che le agenzie di *intelligence* degli Stati Uniti si spingessero
al punto di infiltrare organismi di normazione per sabotare intenzionalmente gli
algoritmi di cifratura. Questo significa prendere una cosa sicura, un algoritmo
di cifratura che è così sicuro che se lo usate per criptare un singolo file,
nessuno può decifrare quel file. Anche se prendono ogni singolo computer del
pianeta solo per decifrare quel singolo file, ci vorranno milioni di anni. In
sostanza è perfettamente sicuro, inattaccabile. Una cosa di quel livello viene
presa e indebolita di proposito, con il risultato di renderci tutti meno sicuri.
Un equivalente nel mondo reale sarebbe che le agenzie di *intelligence*
imponessero un PIN segreto in tutti gli antifurto di ogni singola abitazione in
modo da poter entrare in ogni singola casa. Perché sapete, magari i cattivi
hanno l'antifurto. Ma alla fine ci renderebbe anche tutti meno sicuri. Inserire
una *backdoor* negli algoritmi di cifratura è semplicemente sconvolgente.

Ma certo, queste agenzie di *intelligence* stanno soltanto facendo il proprio
lavoro. È quello che è stato detto loro di fare: spiare i segnali, monitorare le
telecomunicazioni, monitorare il traffico Internet. Questo è quello che stanno
cercando di fare, e dato che la maggior parte del traffico Internet oggi è
cifrato, stanno cercando modi per aggirare la cifratura. Un modo è sabotare gli
algoritmi di cifratura, e questo è un grande esempio di come le agenzie di
*intelligence* americane stiano operando a briglia sciolta. Sono totalmente fuori
controllo, e si dovrebbe riprenderne il controllo.

Cosa sappiamo in realtà di queste fughe di notizie? Tutto si basa sui file fatti
trapelare dal signor [Snowden][2]. Le prime slide di PRISM dell'inizio di giugno
descrivono un programma di raccolta in cui i dati vengono raccolti dai service
provider, e fanno anche i nomi dei service provider ai quali hanno accesso.
Hanno persino una data specifica in cui è iniziata la raccolta di dati per ogni
service provider. Per esempio, specificano che la raccolta da Microsoft è
iniziata l'11 settembre 2007, per Yahoo il 12 marzo 2008, e poi gli altri:
Google, Facebook, Skype, Apple e così via.

E ognuna di queste aziende nega. Dicono tutte che semplicemente non è vero, che
non danno accesso tramite *backdoor* ai loro dati. Eppure abbiamo questi file.
Quindi una delle due parti sta mentendo, o c'è una spiegazione alternativa?

Una spiegazione sarebbe che queste parti, questi service provider, non stanno
collaborando. Invece hanno subìto un attacco informatico. Sarebbe una
spiegazione. Non stanno collaborando. Sono stati attaccati. In questo caso, sono
stati attaccati dal loro stesso governo. Potrebbe sembrare assurdo, ma si sono
già verificati casi di questo tipo. Per esempio, il caso del malware [Flame][6
che crediamo fermamente sia stato creato dal governo americano, e che per
diffondersi ha sovvertito la sicurezza della rete di aggiornamenti Windows.
Questo significa che l'azienda è stata attaccata dal proprio governo. Ci sono
altre prove che supportano questa teoria. Der Spiegel, in Germania, ha fatto
trapelare altre informazioni sulle operazioni gestite dalle unità di hacker di
elite che operano all'interno di queste agenzie di *intelligence*. All'interno
della NSA, l'unità si chiama [TAO][7], Tailored Access Operations, e all'interno
del [GCHQ][8], che è l'equivalente britannico, si chiama NAC, Network Analysis
Centre.

Queste recenti fughe di notizie di queste tre slide descrivono in dettaglio
un'operazione gestita dall'agenzia di *intelligence* GCHQ del Regno Unito che
aveva come obiettivo una società di telecomunicazioni qui in Belgio. Questo
significa quindi che un'agenzia di *intelligence* europea sta violando
intenzionalmente la sicurezza di una telecom di un vicino paese dell'Unione
Europea, e ne parlano nelle loro slide molto disinvoltamente, come se fosse una
cosa normale. Ecco l'obiettivo primario, ecco l'obiettivo secondario, ecco il
team. Probabilmente fanno team building il giovedì sera al bar. Usano
addirittura stucchevoli clip art su PowerPoint come: "Successo", quando riescono
ad accedere a servizi come questo. Che diavolo sta succedendo?

E poi si discute che OK, certo, probabilmente sta succedendo ma, di nuovo, lo
fanno anche altri paesi. Tutti i paesi spiano. E magari è vero. Molti paesi
spiano, non tutti, ma prendiamo un esempio. Prendiamo per esempio la Svezia.
Parlo della Svezia perché la Svezia ha leggi abbastanza simili agli Stati Uniti.
Quando i vostri dati attraversano la Svezia, la sua agenzia di *intelligence* ha
il diritto per legge di intercettare il traffico. Bene, quanti soggetti di
potere, politici, leader d'azienda svedesi usano quotidianamente servizi basati
negli Stati Uniti, come Windows o OS X, o usano Facebook o LinkedIn, o salvano i
propri dati in cloud come iCloud o Skydrive o DropBox, o magari usano servizi
online come Amazon Web Services o SalesForce? La risposta è: lo fa ogni singolo
leader d'azienda, ogni giorno. Ribaltiamo la prospettiva. Quanti leader
americani usano webmail e servizi di cloud svedesi? La risposta è nessuno.
Quindi non c'è bilanciamento. Non c'è assolutamente equilibrio, neanche
lontanamente.

E quando abbiamo la sporadica storia di successo europea, anche quella,
tipicamente finisce per essere venduta agli Stati Uniti. Per esempio, Skype una
volta era sicuro. Era cifrato da un capo all'altro della comunicazione. Poi è
stato venduto agli Stati Uniti. Oggi non è più sicuro. Ancora una volta,
prendiamo una cosa sicura e la rendiamo meno sicura di proposito, con la
conseguenza di rendere tutti noi meno sicuri.

E poi c'è l'argomentazione che gli Stati Uniti stanno solo combattendo i
terroristi. È la guerra al terrorismo. Non dovete preoccuparvi. Beh, non è la
guerra al terrorismo. Certo, è in parte guerra al terrorismo, e sì, ci sono
terroristi, che uccidono e mutilano, e dovremmo combatterli, ma sappiamo da
questa fuga di notizie che hanno usato le stesse tecniche per ascoltare
telefonate di leader europei, per intercettare le email dei presidenti del
Messico e del Brasile, per leggere il traffico di mail nelle sedi delle Nazioni
Unite e del Parlamento Europeo, e non credo che stiano cercando terroristi
all'interno del Parlamento Europeo, giusto? Quindi non è guerra al terrorismo.
Potrebbe esserlo in parte, e ci sono terroristi, ma pensiamo veramente ai
terroristi come a una minaccia esistenziale tale da essere disposti a fare
davvero qualunque cosa per combatterli? Gli americani sono pronti a buttare via
la Costituzione, buttarla nel cestino, solo perché ci sono i terroristi? La
stessa cosa per il Bill of Rights (la Carta del Diritti) e tutti gli
emendamenti, la [Dichiarazione Universale dei Diritti dell'Uomo][9], le
convenzioni europee sui diritti dell'uomo e le libertà fondamentali e la libertà
di stampa? Pensiamo veramente che il terrorismo sia una tale minaccia
esistenziale da essere disposti a fare qualunque cosa?

Ma la gente ha paura dei terroristi, e pensa che la sorveglianza sia accettabile
perché tanto non ha niente da nascondere. Siete liberi di sorvegliarmi, se può
aiutare. E chiunque vi dica che non ha niente da nascondere semplicemente non ci
ha riflettuto abbastanza.

*(Applausi)*

Perché abbiamo questa cosa chiamata privacy, e se pensate veramente di non avere
niente da nascondere, assicuratevi che sia la prima cosa che mi direte, perché
saprò che non dovrò affidarvi nessun segreto, perché ovviamente non siete in
grado di mantenere un segreto.

Ma la gente è brutalmente onesta su Internet, e quando sono iniziate queste
fughe, molta gente ha cominciato a interrogarmi. Non ho niente da nascondere.
Non sto facendo niente di male o illegale. Eppure, non c'è niente che ho piacere
di condividere con un'agenzia di *intelligence*, specialmente una agenzia di
*intelligence* straniera. E se proprio abbiamo bisogno di un Grande Fratello,
preferirei avere un Grande Fratello nazionale che un Grande Fratello straniero.

Quando sono iniziate le fughe di notizie, la prima cosa che ho twittato è stato
un commento su come, usando i motori di ricerca, stiamo potenzialmente mostrando
tutto all'*intelligence* americana. Due minuti dopo ho ricevuto una risposta da
una persona di nome Kimberly dagli Stati Uniti che controbatteva: perché dovevo
preoccuparmene? Cosa sto mandando che mi dovrebbe preoccupare? Sto mandando foto
di nudo o qualcosa del genere? La mia risposta a Kimberly è stata che quello che
stavo mandando non erano affari suoi, e non dovrebbero essere neanche affari del
suo governo. Perché si tratta di questo, si tratta di privacy. La privacy non è
negoziabile. Dovrebbe essere parte integrante di tutti i sistemi che usiamo.

(Applausi)

Una cosa che dovremmo capire è che siamo brutalmente onesti con i motori di
ricerca. Mostratemi la cronologia delle vostre ricerche, e vi troverò qualcosa
di incriminante o di imbarazzante in cinque minuti. Siamo più onesti con i
motori di ricerca di quanto non lo siamo con le nostre famiglie. I motori di
ricerca sanno più di noi di quanto non sappiano di voi le vostre famiglie. E
queste sono tutte informazioni che diamo via, le stiamo dando agli Stati Uniti.

La sorveglianza cambia la storia. Lo sappiamo da esempi di presidenti corrotti
come Nixon. Immaginate se avesse avuto il tipo di strumenti di sorveglianza
disponibili oggi. Fatemi citare testualmente il presidente del Brasile, la
signora [Dilma Rousseff][10]. È stata uno degli obiettivi della sorveglianza
della NSA. Le sue mail sono state lette, e lei ha parlato alla sede delle
Nazioni Unite e ha detto: *"Se non c'è nessun diritto alla privacy, non può
esistere nessuna vera libertà di espressione e opinione, e quindi non può
esistere una democrazia efficace."*

Ecco di cosa si tratta. La privacy è il mattone fondamentale delle nostre
democrazie. E per citare un collega ricercatore della sicurezza, Marcus Ranum
gli Stati Uniti oggi stanno trattando Internet come se fosse una delle loro
colonie. Siamo tornati all'epoca della colonizzazione, e noi, gli "stranieri
che usiamo Internet, dovremmo vedere gli Americani come i nostri padroni.

Il signor Snowden è stato accusato di molte cose. Qualcuno lo accusa di avere
causato, con le sue rivelazioni, problemi all'industria del cloud e alle società
di software. Dare la colpa a Snowden di aver causato problemi all'industria del
cloud americano sarebbe come dare la colpa del riscaldamento globale ad [Al
Gore][11].

*(Risate e applausi)*

Allora, cosa dobbiamo fare? Dovremmo preoccuparci? No, non dovremmo
preoccuparci. Dovremmo arrabbiarci, perché tutto questo è sbagliato ed è
prepotente e non si dovrebbe fare. Ma questo non cambierà la situazione. Quello
che cambierà la situazione per il resto del mondo è cercare di stare lontani dai
sistemi costruiti negli Stati Uniti. È molto più facile a dirsi che a farsi.
Come si fa? Un singolo paese, qualunque singolo paese in Europa non può
sostituire o creare dei sostituti dei sistemi operativi e dei servizi cloud
statunitensi.

Ma magari non lo si deve fare da soli. Magari lo si può fare insieme ad altri
paesi. La soluzione è l'open source. Creando insieme dei sistemi aperti, liberi
e sicuri, possiamo aggirare questa sorveglianza, e così il singolo paese non
deve risolvere il problema da solo. Deve solo risolvere un piccolo problema. E
per citare un collega ricercatore sulla sicurezza, Haroon Meer, un paese deve
solo far partire una piccola onda, queste piccole onde insieme diventano una
marea, e la marea solleverà tutte le barche nello stesso tempo, e la marea che
costruiremo con sistemi sicuri, liberi e open source, diventerà la marea che ci
innalzerà tutti al di sopra dello stato di sorveglianza.

Grazie.

*(Applausi)*

(C) by Paolo Attivissimo - www.attivissimo.net.
Distribuzione libera, purché sia inclusa la presente dicitura.

[1]:http://www.ted.com/talks/lang/it/mikko_hypponen_how_the_nsa_betrayed_the_world_s_trust_time_to_act.html
[2]:https://en.wikipedia.org/wiki/Edward_Snowden
[3]:https://en.wikipedia.org/wiki/PRISM_(surveillance_program)
[4]:https://en.wikipedia.org/wiki/XKeyscore
[5]:https://it.wikipedia.org/wiki/George_Orwell
[6]:https://en.wikipedia.org/wiki/Flame_(malware)
[7]:https://en.wikipedia.org/wiki/Tailored_Access_Operations
[8]:https://en.wikipedia.org/wiki/Gchq
[9]:https://it.wikipedia.org/wiki/Dichiarazione_universale_dei_diritti_umani
[10]:https://it.wikipedia.org/wiki/Dilma_Rousseff
[11]:https://it.wikipedia.org/wiki/Al_Gore
