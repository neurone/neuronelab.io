title: Sulle anime e le loro dimensioni - Douglas Hofstadter
date: 2015-04-16 18:56:15 +0200
tags: filosofia, filosofia della mente, psicologia, scienza, veg
summary: Questa è una trascrizione del primo capitolo di "Anelli nell'io" di Douglas Hofstadter.

Questa è una trascrizione del primo capitolo di "Anelli nell'io" di Douglas Hofstadter.

Inviatomi da Gianluca, il quale ne scrisse anche [sul suo blog][3].

*Un'altra recensione del libro: Martin Gardner, "Do Loops Explain Consciousness?
[Review of I Am a Strange Loop][4]"*

Schegge d'anima
---------------

In una cupa giornata agli inizi del 1991, un paio di mesi dopo la morte di mio
padre, mi trovavo in cucina della casa dei miei genitori, e mia madre, guardando
una foto dolce e commovente di mio padre, scattata forse quindici anni prima, mi
disse con un tono di sconforto: "Che senso ha quella foto? Nessuno. È solo un
pezzo di carta piatto, con delle macchie scure qui e là. Non serve a nulla". La
desolazione del suo commento, così intriso di tristezza, scatenò nella mia mente
un turbinio, perché istintivamente sapevo di non essere d'accordo con lei, ma
non sapevo bene con quali parole esprimere ciò che sentivo sul modo in cui la
fotografia andasse considerata.

Dopo alcuni minuti di riflessione spesi a lasciar correre le mie emozioni – un
"cercare l'anima", letteralmente – mi imbattei in un'analogia che sentivo
avrebbe potuto trasmettere a mia madre il mio punto di vista, e che speravo
potesse offrirle una sia pur minima consolazione. Ecco in linea di massima che
cosa le dissi.

"In soggiorno abbiamo un volume con gli *Studi per pianoforte* di Chopin. Tutte
quelle pagine sono soltanto pezzi di carta con delle tracce scure qui e là,
bidimensionali, piatti e ripiegabili proprio come la fotografia di papà – eppure
pensa al potente effetto che hanno avuto su tante persone in tutto il mondo
negli ultimi 150 anni. Grazie a quei segni neri su quei piatti fogli di carta,
migliaia e migliaia di persone hanno trascorso complessivamente milioni di ore
muovendo le dita sui tasti del pianoforte in complicate configurazioni,
producendo suoni in grado di dar loro un piacere indescrivibile e il senso di un
messaggio profondo. Quei pianisti, a loro volta, hanno trasmesso a molti milioni
di ascoltatori, tra cui te e me, le intense emozioni che si dibattevano nel
cuore di Fryderyk Chopin, consentendo così a tutti noi un qualche parziale
accesso all'interiorità di Chopin – all'esperienza di vivere nella mente, o
meglio nell'anima di Fryderyk Chopin. I segni su quei fogli di carta non sono
niente di meno che schegge d'anima – resti sparsi dell'anima dispersa di
Fryderyk Chopin. Ognuna di quelle strane geometrie di note ha il potere
straordinario di riportare in vita, dentro i nostri cervelli, qualche minuscolo
frammento delle esperienze interiori di un altro essere umano – le sue
sofferenze, le sue gioie, le sue più profonde passioni e tensioni – e così
sappiamo, almeno in parte, che cosa si provava a essere quell'essere umano, e
molti nutrono per lui un amore intenso. In modo altrettanto potente, guardare
quella fotografia di papà riporta, a noi che lo conoscevamo intimamente, la
nitida memoria del suo sorriso e della sua gentilezza, attiva dentro i nostri
cervelli viventi alcune delle rappresentazioni più centrali che di lui
sopravvivono in noi, fa sì che piccoli frammenti della sua anima danzino ancora,
sia pure in cervelli diversi dal suo. Come le note sullo spartito di uno studio
di Chopin, quella fotografia è una scheggia d'anima di qualcuno che se n'è
andato, ed è qualcosa di cui dovremmo fare tesoro finché viviamo.

Anche se ciò che ho appena scritto è un po' più fiorito del discorso che feci a
mia madre, rende l'essenza del mio messaggio. Non so che effetto abbia avuto sui
suoi sentimenti riguardo a quell'immagine, ma quella foto è ancora lì, su un
ripiano della sua cucina, e ogni volta che la guardo mi ricordo di quello
scambio di idee.

 
Che cosa si prova a essere un pomodoro?
---------------------------------------

Io affetto e divoro pomodori senza il minimo senso di colpa. Non vado a dormire
inquieto dopo aver consumato un pomodoro fresco. Non mi succede di domandarmi
quale pomodoro ho mangiato, o se mangiandolo ho spento una luce interiore, né
credo che sia sensato provare a immaginare come il pomodoro si sentiva mentre se
ne stava sul mio piatto e veniva fatto a fette. Per me, un pomodoro è un'entità
priva di desideri, senza anima né coscienza, e non ho scrupoli a fare del suo
"corpo" tutto quello che mi piace. Di fatto, un pomodoro non è altro che il suo
corpo. Per i pomodori non esiste un "problema mente-corpo". (Spero, cari
lettori, che almeno su questo siamo d'accordo!)

Schiaccio anche zanzare senza il minimo scrupolo, benché cerchi di evitare di
pestare le formiche, e se c'è in casa un insetto diverso da una zanzara, di
solito tento di catturarlo e portarlo fuori, dove lo libero senza avergli fatto
male. Qualche volta mangio pollo e pesce [Nora: questo non succede più – vedi il
Post Scriptum a questo capitolo], mentre ho smesso di mangiare la carne di
mammiferi molti anni fa. Né porchetta, né strutto, cotoletta o prosciutto –
proprio niente di tutto! Sia chiaro, mi piacerebbe ancora il gusto di un panino
al prosciutto o di un hamburger ben cotto, ma per ragioni morali mi astengo
semplicemente dal consumarne. Non voglio imbarcarmi qui in una crociata, eppure
è il caso che parli un poco delle mie inclinazioni vegetariane, perché hanno in
tutto e per tutto a che fare con le anime.
<!--more-->
*Nota del blogger: Attualmente, Hofstadter è vegano, come si può leggere dalla
recensione di Martin Gardner*

 
Porcellino d'India
------------------

All'età di quindici anni avevo un lavoro estivo in cui spingevo pulsanti su un
calcolatore meccanico Friden in un laboratorio di fisiologia dell'Università di
Stanford. (A quel tempo, nell'intero campus di Stanford c'era un solo computer e
la maggior parte dei ricercatori nemmeno sapeva della sua esistenza, né tanto
meno pensava di usarlo per i propri calcoli.) "Pigiare numeri" per ore di fila
era un lavoro piuttosto snervante, e un giorno Nancy, la studentessa di
dottorato per cui il progetto stava facendo tutto questo, mi chiese se, per
staccare un po', volessi cimentarmi con altri tipi di mansioni.  Dissi "Certo!",
e così quel pomeriggio mi condusse al quarto piano e mi mostrò le gabbie dove
tenevano gli animali – per la precisione, porcellini d'India – che usavano come
cavie nei loro esperimenti. Ricordo ancora l'odore pungente e lo zampettare
frenetico di tutti quei piccoli roditori dal pelo arancione.

Il pomeriggio seguente, Nancy mi chiese se potevo salire al quarto piano a
prendere due animali per la sua prossima serie di esperimenti. Non ebbi neppure
il tempo di replicare perché, non appena iniziai a immaginarmi nell'atto di
infilare la mano in una di quelle gabbie e scegliere due soffici esserini pelosi
da uccidere, cominciò a girarmi la testa e in un attimo svenni, sbattendo il
capo sul duro pavimento. Il ricordo successivo è di me sdraiato che guardavo il
viso del direttore del laboratorio, George Feigen, un vecchio amico di famiglia,
molto preoccupato che mi fossi fatto male nella caduta. Per fortuna stavo bene,
e lentamente mi rialzai, per poi tornare a casa in bici e rimanerci per il resto
della giornata. Nessuno mi chiese più di andare a prendere animali da
sacrificare per il bene della scienza.

 
Porcellino
----------

Strano a dirsi, malgrado quel tete-a-tete assai problematico con l'idea di
togliere la vita a una creatura vivente, per diversi anni continuai a mangiare
hamburger e altri tipi di carne. Non penso che riflettessi molto sulla cosa,
dato che nessuno dei miei amici lo faceva, e certo nessuno ne parlava. Mangiare
carne era un fatto assolutamente normale nella vita di tutti quelli che
conoscevo. Per di più, confesso con imbarazzo che per me, a quei tempi, la
parola "vegetariano" evocava l'immagine di strani tipi svitati e moralmente
inflessibili (nel film di Billy Wilder *Quando la moglie è in vacanza* c'è una
scena stupenda in un ristorante vegetariano di Manhattan che ritrae alla
perfezione questo stereotipo). Ma un giorno, all'età di ventun anni, lessi un
racconto intitolato *Pig* dello scrittore anglo-norvegese Roald Dahl, e questa
storia ebbe un profondo effetto sulla mia vita – e, attraverso di me, anche su
quella delle altre creature.

*Pig* inizia in modo leggero e divertente – un giovane un po' ingenuo di nome
Lexington, cresciuto dalla zia Glosspan ("Pangloss" a rovescio) nella più rigida
osservanza dei principi vegetariani, dopo la morte di costei scopre che gli
piace il gusto della carne (benché, di fatto, non sappia cosa stia mangiando).
Ben presto però, come in tutti i racconti di Dahl, le cose prendono una piega
bizzarra.

Spinto dalla curiosità per questa gustosa sostanza chiamata "carne suina", su
consiglio di un nuovo amico Lexington decide di prendere parte ad una visita
guidata a un macello. Lo seguiamo mentre siede nella sala d'attesa con altri
visitatori. Oziosamente osserva come vengano chiamati, uno dopo l'altro, a fare
il loro giro. Alla fine arriva anche il turno di Lexington, che dalla sala
d'attesa è condotto in una zona in cui i maiali vengono incatenati, dove osserva
come questi vengono appesi per le zampe posteriori ai ganci di una catena
mobile, come vengono sgozzati, e come, sanguinando copiosamente dalla gola,
procedono a testa in giù lungo la "catena di smontaggio" per poi piombare in un
calderone d'acqua bollente dove vengono spelati, dopodiché, tagliati via teste e
arti, sono pronti per essere squartati e spediti in linde vaschette avvolte nel
cellophane a supermercati sparsi in tutto il paese, dove se ne staranno in
vetrina insieme ad altri concorrenti dal colore rosato, in attesa di acquirenti
che li ammirino e, se tutto va bene, li scelgano per portarseli a casa.

Mentre osserva la scena con una sorta di distaccato rapimento, Lexington stesso
viene all'improvviso strattonato per una gamba e messo a testa in giù, e si
rende conto che ora anche lui è appeso a penzoloni dalla catena mobile, proprio
come i maiali che stava guardando. Svanita del tutto la sua placidità, si mette
ad urlare "C'è stato un terribile errore!", ma gli operai ignorano le sue grida.
Presto la catena lo trascina accanto a un tipo dall'aspetto amichevole che
Lexington spera afferrerà l'assurdità della situazione, ma invece il gentile
"sgozzatore" afferra il giovane penzolante per un orecchio, lo tira un po' più
vicino a sé e poi, sorridendogli benevolo e amorevole, con destrezza gli
squarcia la vena giugulare con un affilato coltello. Mentre il giovane Lexington
continua il suo inaspettato viaggio capovolto, il cuore gli pompa con forza
sangue fuori dalla gola e sul pavimento di cemento, e benché si trovi a testa in
giù e stia perdendo rapidamente coscienza, percepisce in maniera indistinta i
maiali davanti a lui che cadono, a uno a uno, nel calderone fumante. Uno di
essi, curiosamente, sembra indossare dei guanti bianchi sulle zampe anteriori, e
questo richiama alla mente la giovane donna coi guanti che poco prima lo aveva
preceduto nel percorso dalla sala d'attesa alla zona di visita. E con questo
ultimo singolare pensiero nella mente Lexington scivola confusamente fuori da
questo, "il migliore dei mondi possibili", in quello successivo.

La scena finale di *Pig* ha continuato a riverberarmi nella mente per molto
tempo.  Dentro di me oscillavo senza posa nell'immagine di essere, a turno, un
maiale che grugnisce a più non posso appeso a un gancio a testa in giù e
Lexington mentre rotola nel calderone…

 
Riverberazione, rivelazione, rivoluzione
----------------------------------------

Un mese o due dopo aver letto questo racconto che non voleva uscirmi dalla
testa, andai con i miei genitori e mia sorella Laura a Cagliari, all'estremità
meridionale dell'aspra isola di Sardegna, dove mio padre avrebbe partecipato a
un congresso di fisica. Per concludere il convegno secondo il più solenne stile
locale, gli organizzatori avevano preparato un sontuoso banchetto in un parco
nei sobborghi della città, durante il quale un maialino da latte avrebbe dovuto
essere arrostito e poi tagliato a pezzi davanti ai commensali. Quali ospiti
illustri del convegno, ci si aspettava che tutti noi prendessimo parte a questa
venerata tradizione sarda. Io però ero ancora sotto la profonda influenza del
racconto di Dahl, che avevo letto da poco, e non potevo neppure concepire di
prendere parte a un simile rituale. Nella mia nuova disposizione d'animo, non
riuscivo nemmeno lontanamente a immaginare come qualcuno potesse mai desiderare
di trovarsi là, per non parlare di ingerire qualcosa del corpo del porcellino.
Venni poi a sapere che anche mia sorella Laura era inorridita al solo pensiero,
e così noi due restammo in hotel e fummo molto contenti di mangiare un po' di
pasta e di verdure.

I due colpi ravvicinati del "Maiale" norvegese e del maialino sardo ebbero come
risultato che seguissi l'esempio di mia sorella e smettessi del tutto di
mangiare carne. Mi rifiutavo anche di comprare scarpe o cinture di pelle.
Presto divenni un fervente predicatore in cerca di proseliti per il mio nuovo
credo, e ricordo la soddisfazione che mi diede il fatto di essere riuscito a
persuadere per alcuni mesi un paio di amici, che però con mio disappunto a poco
a poco abbandonarono i loro propositi.

In quel periodo mi chiedevo spesso come alcuni dei miei idoli personali – Albert
Einstein, per esempio – avessero potuto mangiar carne. Non trovai una
spiegazione, sebbene di recente, con mio grande piacere, una ricerca in rete mi
ha fornito qualche indizio di come le simpatie di Einstein andassero, in
effetti, ai principi vegetariani, e non per motivi di salute ma per compassione
verso gli esseri viventi. Io però non ne sapevo nulla a quel tempo, e in ogni
caso molti altri miei eroi erano senz'altro carnivori e perfettamente
consapevoli di quello che stavano facendo. Era una realtà che mi rattristava e
mi disorientava.

 
Riconversione, rievoluzione
---------------------------

La cosa davvero strana è che, soltanto alcuni anni più tardi, sentii anch'io le
pressioni del vivere quotidiano nella società americana a un livello tale che
abbandonai il mio un tempo appassionato vegetarianismo, e per un certo periodo
tutte le mie intense ruminazioni finirono sepolte e quasi dimenticate. Penso che
il me stesso della metà degli anni Sessanta avrebbe trovato questa riconversione
assolutamente imperscrutabile, eppure le due versioni di me hanno entrambe
vissuto nell'unico e medesimo cranio.  Ero davvero la stessa persona?

Trascorsero molti anni in questo modo, quasi come se non avessi mai avuto alcuna
epifania, ma poi un giorno, poco tempo dopo essere diventato professore
assistente all'Università dell'Indiana, incontrai Sue, una donna molto attenta e
riflessiva che aveva adottato la stessa filosofia vegetariana che a suo tempo
avevo adottato io e per ragioni simili alle mie, ma che aveva perseverato per un
tempo più lungo di quanto non avessi fatto io. Sue e io diventammo buoni amici,
e io ammiravo la purezza delle sue convinzioni. La nostra amicizia mi portò a
riconsiderare tutto ancora una volta e in breve tempo mi ero riconvertito alla
mia posizione post-*Pig* di non uccidere mai.

Nel corso degli anni seguenti ci furono alcune ulteriori oscillazioni, ma in
prossimità dei quarant'anni mi assestai infine su una posizione stabile – un
compromesso che rispecchiava la mia nascente intuizione che esistessero anime di
diverse dimensioni. Benché la cosa mi risultasse tutt'altro che chiara e
cristallina, ero disposto ad accettare la vaga idea che alcune anime, purché
fossero "abbastanza piccole", potessero venire legittimamente sacrificate perché
così desideravano anime "più grandi", quali la mia e quelle di altri esseri
umani. Anche se tracciare la linea di demarcazione in corrispondenza dei
mammiferi era naturalmente piuttosto arbitrario (come lo è per forza di cose
ogni linea di demarcazione di questo tipo), questo divenne il mio nuovo credo e
vi tenni fede per i due successivi decenni.

 
Il mistero della carne inanimata
--------------------------------

In alcune lingue, come l'inglese e anche l'italiano, non si usa dire che
mangiamo il maiale o la mucca, ma che mangiamo la carne di maiale o la carne di
manzo. Mangiamo sì pollo – ma non mangiamo i polli. Una volta la giovanissima
figlia di un mio amico raccontò tutta allegra al padre che la parola usata per
un certo uccello d'allevamento che chiocciava e faceva le uova era anche quella
per definire una sostanza che lei trovava spesso nel suo piatto all'ora di cena.
Alla bimba questa sembrava una coincidenza davvero buffa, simile alla buffa
coincidenza per cui, per esempio, "venti" indica sia il plurale di "vento" che
il numero successivo al diciannove.  Rimase molto turbata, inutile dirlo, quando
le spiegarono che quel cibo saporito e l'animale che deposita uova e fa coccodè
erano esattamente la stessa cosa.

Probabilmente passiamo tutti per questo stato di confusione quando, da bambini,
scopriamo che stiamo mangiando animali che la nostra cultura ci dice essere
carini e graziosi – agnellini, coniglietti, vitellini, pollastrelle, e così via.
Ricordo, seppur vagamente, la mia stessa sincera confusione infantile di fronte
a questo mistero, ma siccome il mangiar carne era un così incontestato luogo
comune, di solito mettevo tutto sotto il tappeto e non stavo troppo a pensarci
su.

Tuttavia, i negozi alimentari avevano un loro modo sgradevole di sollevare
chiaramente il problema. C'erano grandi banconi che mettevano in mostra ogni
genere di masse informi dall'apparenza viscida e di vari strani colori,
etichettate "fegato", "trippa", "cuore" e "reni", e qualche volta perfino
"lingua" e "cervello". Non solo queste suonavano come parti animali, ne avevano
anche tutta l'*apparenza*. Fortunatamente, quello chiamato "manzo macinato" non
era poi così tanto simile a una parte animale, e dico "fortunatamente" perché
aveva un così buon sapore. Non avrei mai voluto essere disilluso dal mangiare
*quello*! Anche la pancetta era buonissima, e le strisce di quella roba erano
così sottili e, una volta cotte, così croccanti che non facevano proprio pensare
a un animale.  Che fortuna!

Erano le banchine di scarico sul retro dei negozi di alimentari a riproporre il
mistero a viva forza. A volte si fermava un grosso camion, e quando il
portellone posteriore si spalancava vedevo enormi pezzi di carne e ossa
penzolare senza vita da spaventosi ganci metallici. Guardavo ogni volta con
morbosa curiosità queste carcasse che scorrevano lungo traverse sopraelevate,
così da poter essere spostate con facilità. Tutto questo metteva molto a disagio
il preadolescente che ero, e contemplando una carcassa non potevo impedirmi di
riflettere: "Chi era quell'animale?". Non mi stavo chiedendo il suo *nome*,
sapevo bene che gli animali d'allevamento non ne avevano; stavo cercando di
afferrare qualcosa di più filosofico – come ci si era sentiti a essere
*quell*'animale piuttosto che un *altro*. Che cos'era quella irripetibile luce
interiore che si era improvvisamente spenta quando quell'animale era stato
ammazzato?

Quando da ragazzo andai in Europa, il problema si ripresentò in modo ancor più
crudo. Là, corpi di animali senza vita (di solito scuoiati, privati di testa e
coda, ma a volte anche interi) erano in bella mostra di fronte a tutti i
clienti. Il mio ricordo più vivo è quello di una grossa drogheria che, nel
periodo natalizio, aveva esposto su un bancone una testa di maiale. Se a uno
capitava di avvicinarglisi da dietro, poteva vedere una sezione piatta con tutte
le strutture interne del collo dell'animale, proprio come se fosse stato
ghigliottinato. C'erano tutte le fitte linee di comunicazione che una volta
avevano collegato le varie parti remote del corpo di questo individuo al
"quartier generale" nella sua testa. Visto dalla parte opposta, questo animale
aveva stampato in faccia quello che sembrava un sorriso congelato, e la cosa mi
faceva accapponare la pelle.

Ancora una volta non potei fare a meno di chiedermi: "Chi c'era stato un tempo
dentro quella testa? Chi aveva vissuto lì? Chi aveva guardato attraverso quegli
occhi, sentito con quelle orecchie? Chi era stato davvero questo pezzo di carne?
Era un maschio o una femmina?". Non ottenni alcuna risposta, ovviamente, e
nessun altro cliente pareva prestare attenzione a quell'esposizione. Mi sembrava
che nessun altro si stesse confrontando con le intense domande su vita, morte, e
"identità porcinale" che quella testa muta e immobile suscitava in modo così
potente e tumultuoso nella mia.

A volte mi ponevo la stessa domanda dopo aver schiacciato una formica o una
farfallina di tarma o una zanzara – ma non tanto spesso. D'istinto ero portato a
pensare che in questi casi avesse meno senso chiedersi "Chi c'è ‘lì dentro'?".
Ciononostante, la vista di un insetto parzialmente schiacciato che si agita in
modo convulso sul pavimento mi induce sempre a un qualche tipo di riflessione
interiore. E in effetti, il motivo per cui ho evocato tutte queste immagini
cruente non è quello di fare una crociata per una causa sulla quale con tutta
probabilità molti dei miei lettori avranno già riflettuto a lungo; quanto
piuttosto sollevare la scottante questione di che cosa sia un'"anima", e di chi
o cosa ne possieda una. È una questione che riguarda ciascuno di noi per tutta
la durata della nostra vita – almeno implicitamente, e per molte persone anche
esplicitamente – ed è la questione centrale di questo libro.

 
Datemi degli uomini con un'anima robusta e gagliarda
----------------------------------------------------

Ho accennato prima al mio profondo amore per la musica di Chopin. Da ragazzo e
da giovane ho suonato al pianoforte molti pezzi di Chopin, spesso presi dalle
edizioni di colore giallo vivace pubblicate da G. Schrimer a New York. Ognuno
di questi volumi si apriva con un saggio scritto ai primi del Novecento dal
critico americano James Huneker. Oggi molti troverebbero la prosa di Huneker un
po' troppo magniloquente ma per me non lo era; il suo dirompente impeto emotivo
risuonava con la mia percezione della musica di Chopin, e tutt'ora amo il suo
stile di scrittura e le sue ricche metafore. Nella sua prefazione al volume
degli *Studi* di Chopin, a proposito dello *Studio in la minore* op. 25 n. 11
(un'eruzione titanica spesso chiamata "Vento d'inverno", benché questo
certamente non fosse né il titolo dato da Chopin né l'immagine che lui ne
aveva), Huneker esprime la seguente notevole considerazione: "Uomini dall'anima
piccola, per quanto agili siano le loro dita, non dovrebbero cimentarvisi".

Posso testimoniare personalmente la spaventosa difficoltà tecnica di
quell'incredibile marea montante che è questo pezzo di musica, avendo io
coraggiosamente tentato di impararlo all'età di circa sedici anni ed essendo
stato tristemente costretto a rinunciarvi quando ero a metà strada, dal momento
che suonare anche solo la prima pagina alla giusta velocità (cosa che infine
riuscii a fare dopo diverse settimane di durissimo esercizio) mi faceva pulsare
dal dolore la mano destra. Ma la difficoltà tecnica non è, ovviamente, ciò a cui
si riferiva Huneker. Di questo pezzo, Huneker dice a ragione che è maestoso e
nobile ma poi, e qui è assai più discutibile, traccia una linea di demarcazione
tra differenti livelli o "dimensioni" delle anime umane, suggerendo che alcune
persone semplicemente non siano all'altezza di suonarlo, non per una qualche
limitazione corporea, ma perché le loro anime non sono "abbastanza grandi". (Non
starò a criticare il sessismo delle parole di Huneker: era quello lo standard
dell'epoca e, purtroppo, lo è ancora oggi in molte lingue e culture.)

Un'opinione di questo tipo non sarebbe bene accetta nell'America egualitaria di
oggi. Non sarebbe molto popolare. A essere onesti, suona estremamente elitaria,
forse perfino ripugnante, alle nostre moderne orecchie democratiche. Eppure devo
ammettere che in un certo qual modo sono d'accordo con Huneker, e non riesco a
non chiedermi se noi tutti non crediamo implicitamente alla legittimità di
qualcosa di vagamente simile all'idea che ci siano esseri umani "dall'anima
piccola" ed esseri umani "dall'anima grande". Di fatto, non posso fare a meno di
suggerire che questa sia in realtà la convinzione che quasi tutti noi abbiamo,
per quanto egualitari ci professiamo pubblicamente.

 
Esseri umani dall'anima piccola ed esseri umani dall'anima grande
-----------------------------------------------------------------

Alcuni di noi credono nella pena capitale – la deliberata pubblica
soppressione di un'anima umana, non importa quanto ardentemente
quell'anima possa implorare misericordia, tremare, scuotersi, urlare,
dibattersi disperatamente per sfuggire mentre viene condotta lungo il
corridoio che porta al luogo della sua condanna.

Alcuni di noi, forse quasi tutti, credono che uccidere soldati nemici in
guerra sia legittimo, come se la guerra fosse una circostanza speciale
che fa restringere le anime dei soldati nemici.

In passato, forse alcuni di noi avrebbero creduto (come lo credettero,
ognuno a modo suo, almeno per un certo periodo di tempo, George
Washington, Thomas Jefferson e Benjamin Franklin) che non fosse immorale
possedere schiavi e venderli e comprarli disperdendo famiglie
arbitrariamente, proprio come facciamo al giorno d'oggi, per esempio,
con cavalli, cani e gatti.

Alcune persone religiose credono che gli atei, agli agnostici e i
seguaci delle altre fedi – o, ancora peggio, i traditori che hanno
abbandonato "la" fede – non abbiamo affatto un'anima, e siano perciò
particolarmente meritevoli di morte.

Altre (incluse alcune donne) credono che le donne non abbiano un'anima –
o forse, un po' più generosamente, che abbiano "anime più piccole" degli
uomini.

Alcuni di noi (incluso il sottoscritto) credono che il defunto
presidente Reagan fosse in pratica "del tutto andato" molti anni prima
che il suo corpo rendesse l'anima, e più in generale crediamo che le
persone agli ultimi stadi del morbo di Alzheimer siano in pratica del
tutto andate. Ci colpisce il fatto che, sebbene all'interno di ognuna di
quelle scatole craniche si trovi ancora un cervello umano, qualcosa sia
scomparso da quel cervello – qualcosa di essenziale, qualcosa che
conteneva i segreti dell'anima di quella persona. L'io è del tutto o in
parte svanito, si è dileguato, non lo si ritroverà mai più.

Alcuni di noi (di nuovo, metto me stesso in questo gruppo) credono che
né un ovulo appena fecondato né un feto di cinque mesi possiedano una
piena anima umana, e che, in un certo senso, la vita di una potenziale
madre conti più della vita di quella piccola creatura, per quanto
incontestabilmente vivente quella creatura sia.

 
Hattie il labrador cioccolato
-----------------------------

KELLIE: Dopo mangiato usciamo a vedere il tacchino di Lynne, che è qualcosa che
non abbiamo ancora visto.

DOUG: Qualcosa o qualcuno?

KELLIE: Qualcosa direi. Un tacchino non è qualcuno.

DOUG: Capisco… Allora Hattie è un qualcuno o un qualcosa?

KELLIE: Oh, lei è un qualcuno, non c'è dubbio.

Ollie il golden retriever
-------------------------

DOUG: Allora è piaciuta a Ollie la gita al lago Griffy di oggi pomeriggio?

DANNY: Oh sì, lui si è divertito parecchio, ma non ha giocato molto con gli
altri cani. Però gli è piaciuto giocare con le persone.

DOUG: Davvero? Come mai?

DANNY: Ollie non è il tipo di persona da cani, è più un tipo da gente.

Dove tracciare la fatidica linea fatale?
----------------------------------------

Tutti gli esseri umani – almeno quelli con un'anima di dimensione abbastanza
grande – devono affrontare questioni quali lo schiacciare mosche o zanzare, il
piazzare trappole per topi, il cibarsi di conigli o aragoste o tacchini o
maiali, forse perfino di cani o cavalli, l'acquisto di stole di visone o statue
d'avorio, l'uso di valigie in pelle o di cinture di coccodrillo, perfino
l'attacco a base di penicillina contro orde di batteri che hanno invaso il
proprio organismo, e così via. Il mondo ci impone in ogni momento dilemmi morali
piccoli e grandi – come minimo, a ogni pasto – e siamo tutti costretti a
prendere posizione.

Un agnellino possiede un'anima che conta qualcosa, o il sapore delle costolette
d'agnello è troppo delizioso per stare a pensarci su più di tanto? Una trota che
ha mangiato l'esca e ora si sta dibattendo senza rimedio all'estremità di un
filo di nylon merita di sopravvivere o si deve solo darle un secco "colpo di
grazia" in testa e "mettere fine alle sue sofferenze" così da poter assaporare
l'indescrivibile eppure stranamente prevedibile consistenza tenera e friabile
dei suoi bianchi muscoli? Le cavallette e le zanzare e perfino i batteri
possiedono al loro interno una minuscola "luce accesa", non importa quanto
fioca, o è tutto buio "là dentro"? (Dentro dove?) Perché non mangio i cani? Chi
era il maiale la cui pancetta stavo gustando a colazione? Che pomodoro è quello
che sto masticando? Dobbiamo abbattere quel magnifico olmo nel nostro giardino?
E già che ci sono, devo sradicare il cespuglio colmo di more selvatiche? E tutte
le erbe che crescono lì vicino?

Che cosa dà a noi utilizzatori di parole il diritto di prendere decisioni di
vita o di morte nei riguardi di altre creature viventi che non hanno la facoltà
della parola? E perché quando ci pensiamo siamo così tormentati (almeno alcuni
di noi)? In ultima analisi, è soltanto perché *la forza fa il diritto*, e noi
umani, grazie all'intelligenza resa possibile dalla complessità dei nostri
cervelli e dal nostro essere immersi in linguaggi e culture di grande ricchezza,
siamo in effetti potenti ed eminenti rispetto agli "inferiori" animali (e
vegetali). In virtù della nostra potenza, siamo costretti a stabilire una sorta
di graduatoria delle creature, sia che lo facciamo come risultato di lunghe e
attente riflessioni personali sia che, semplicemente, ci lasciamo trascinare dal
potente flusso delle masse.  Le mucche sono sacrificabili con la stessa
tranquillità con cui uccidiamo le zanzare? Vi sentireste in qualche modo meno
turbati dallo schiacciare una mosca che si sta strofinando le zampette su una
parete che dal decapitare un pollo tremante su un ceppo? È ovvio che queste
domande potrebbero essere moltiplicate all'infinito, ma mi fermo qui.

Riporto qui sotto il mio personale "cono di coscienza". Non vuole essere esatto,
è puramente indicativo, ma presumo che nella vostra testa, così come nella di
testa di ciascun essere umano dotato di parola, esista qualche struttura
analoga, anche se nella maggioranza dei casi non è mai fatta oggetto di attenta
osservazione dal momento non è nemmeno formulata in modo esplicito.

![Cono di coscienza]({filename}/images/anelli-illustrazione1.jpg "Cono di coscienza")

Interiorità – cosa la possiede, e in che grado?
-----------------------------------------------

È molto improbabile che voi, lettori di questo libro, vi siate persi tutti i
film della serie di *Guerre stellari*, con i loro pressoché indimenticabili
D-3BO e C1-P8. Per quanto assiduamente irrealistici siano questi due robot,
specie per come li può percepire qualcuno che come me ha lavorato per decenni
cercando di comprendere con l'aiuto di modelli computazionali anche solo i più
primitivi meccanismi dell'intelligenza umana, servono tuttavia a uno scopo
molto utile – sono degli apri-mente. Vedere D-3BO e C1-P8 "in carne e ossa
sullo schermo ci fa capire che, quando guardiamo un'entità fatta di metallo o
plastica, non siamo giocoforza destinati a saltare alla dogmatica conclusione:
"Quella cosa è necessariamente un oggetto inanimato poiché è fatta ‘con il
materiale sbagliato'". Scopriamo piuttosto, forse con nostra sorpresa, che
possiamo facilmente immaginarci un'entità pensante e senziente fatta di un
materiale freddo, rigido, e così poco simile alla carne.

In uno dei film di *Guerre stellari*, ricordo di aver visto un enorme
battaglione di centinaia di robot che marciavano in modo uniforme – e quando
dico "uniforme", intendo davvero uniforme, tutti che avanzavano impettiti in
perfetta sincronia, e tutti con le stesse espressioni del viso, impassibili,
vacue, meccaniche. Sospetto che dopo questa inequivocabile immagine di assoluta
interscambiabilità, virtualmente nessuno degli spettatori senta la benché
minima punta di tristezza quando una bomba cade sul battaglione che sta
caricando e tutti i suoi membri – queste "creature" prodotte in serie – vengono
istantaneamente fatti esplodere in mille pezzi. Dopotutto, in modo
diametralmente opposto a D3-BO e C1-P8, *questi* robot non sono affatto
creature – ma semplici pezzi di metallo! In questi involucri metallici non c'è
più *interiorità* di quanta ce ne sia in un apriscatole, in un'automobile o in
una nave da guerra, un fatto rivelatoci dal loro essere perfettamente identici.
O altrimenti, se per caso ci dovesse essere al loro interno un qualche
minuscolo grado di interiorità, sarebbe dello stesso ordine di grandezza del
grado di interiorità di una formica. Questi marciatori metallici sono solo
robot soldati, membri di una casta di fuchi di una più grande colonia di robot,
e stanno solo obbedendo, in perfetto stile zombie, agli inflessibili impulsi
meccanici impiantati nei loro circuiti. Se da qualche parte lì dentro c'è
dell'interiorità, è di un livello irrilevante.

Cos'è, allora, che ci da il senso innegabile che D-3BO e C1-P8 abbiano una
"luce accesa" dentro, che ci sia un bel po' di autentica interiorità nei loro
crani inorganici, situata da qualche parte dietro ai loro buffi occhi tonti? Da
dove viene il senso innegabile del loro io che noi percepiamo? E d'altra parte,
cos'era che *mancava* nell'ex presidente Reagan nei suoi ultimi anni di vita o
in quella massa di tronfi robot in marcia tutti uguali fra loro, e cos'è invece
che *non* manca in Hattie il labrador cioccolato e in C1-P8 il robot, e che fa
per noi tutta la differenza?

 
La crescita graduale di un'anima
--------------------------------

Ho detto in precedenza di essere tra coloro che rifiutano la nozione di
un'anima umana già pienamente sviluppata che viene alla luce nel momento in cui
uno spermatozoo umano si unisce a un ovulo umano a formare uno zigote. Al
contrario, credo che un'anima umana – e, per inciso, è mia intenzione in questo
libro chiarire cosa intendo con questa parola sfuggente, cangiante, spesso
carica di connotazioni religiose, anche se qui non ne ha alcuna – si formi in
modo graduale nel corso di molti anni di sviluppo. Può sembrare grossolano
metterla in questi termini, ma vorrei suggerire, almeno metaforicamente, una
scala numerica di "gradi di possesso d'anima", o "gradi di animatezza".
Possiamo immaginare in prima battuta che questa scala vada da 0 a 100, e le sue
unità di misura si chiamino, giusto per divertimento, "huneker". Dunque noi,
cari lettori, siamo tutti a 100 huneker di animatezza, o giù di lì. Qua la
mano!

Oops! Mi sono appena accorto di aver commesso un errore che deriva dai lunghi
anni di indottrinamento a tutte le ammirevoli tradizioni egualitarie del mio
paese natale – vale a dire che ho inconsapevolmente dato per scontato che ci
sia un valore raggiunto il quale il possesso d'anima "è al massimo", e che
tutti gli adulti normali raggiungano questo tetto e non possano andare oltre.
Per quale motivo, però, dovrei dare per scontata una cosa del genere? Perché
l'animatezza non potrebbe essere come, poniamo, l'altezza? C'è un'altezza media
per gli adulti, ma c'è anche una variabilità considerevole rispetto a quella
media. Perché non dovrebbe esserci allo stesso modo un grado medio di
animatezza per adulti (diciamo 100 huneker) più un'ampia gamma intorno a quella
media, magari (come per il QI) che arriva fino ad altezze di 150 o 200 in casi
rari, e anche giù fino a 50 o ancora più in basso in altri casi?

Se le cose stanno così, allora ritiro quanto ho affermato per riflesso
condizionato che noi, cari lettori, abbiamo tutti quanti 100 huneker di
animatezza. Invece, preferirei suggerire che abbiamo tutti un valore
considerevolmente *più alto* di quello previsto dall'hunkerometro! (Spero che
siate d'accordo.) Tuttavia, in questo modo sembra che ci stiamo inoltrando in
un territorio moralmente pericoloso, avvicinandoci all'idea che alcune persone
*valgano più* di altre – idea che è un anatema nella nostra società (e che
disturba anche me), dunque non dedicherò molto tempo qui a cercare di capire
come calcolare l'animatezza di una data persona in huneker.

Mi colpisce l'idea che, dopo l'unione di spermatozoo e ovulo, il bio-grumo
infinitesimale che ne risulta abbia un'anima che vale essenzialmente zero
huneker. Quello che è successo, però, è che si è generata un'entità dinamica
che cresce a valanga, la quale nel giro di alcuni anni sarà in grado di
sviluppare un insieme complesso di strutture o pattern interni – e la presenza,
in grado via via più elevato, di questi intricati pattern è ciò che doterà
quella entità (o piuttosto, le entità enormemente più complesse nelle quali man
mano si trasforma, passo dopo passo) di un valore sempre maggiore nella scala
di Huneker, puntando verso un qualche valore prossimo a 100.

Il cono mostrato qui sotto dà un'idea approssimativa ma vivida del valore di
huneker che potrei attribuire a esseri umani di età compresa fra zero e
vent'anni (o, in alternativa, a un solo essere umano in diversi stadi di vita).

![Scala Huneker]({filename}/images/anelli-illustrazione2.jpg "Scala Huneker")

In breve, quello che vorrei sostenere qui, riecheggiando e generalizzando la
provocatoria dichiarazione di James Huneker, è che l'"animatezza" non è affatto
una variabile discreta, on-off, bianca-nera, che ha solo due stati possibili
come un bit, un pixel, o una lampadina, ma che è piuttosto una variabile
numerica sfumata, fuzzy, che varia in modo continuo tra diverse specie e varietà
di oggetti, e che può anche salire o scendere nel corso del tempo come risultato
della crescita o del declino, all'interno dell'entità in questione, di uno
speciale tipo di pattern sottile e complesso (la cui natura, e il tentativo di
delucidarla, ci terranno impegnati per gran parte di questo libro). Vorrei
sostenere altresì che i pregiudizi in larga misura inconsapevoli della
maggioranza delle persone, che riguardano per esempio se mangiare o meno questo
o quel cibo, se comprare o meno questo o quel capo di abbigliamento, se
schiacciare o men questo o quell'insetto, se fare il tifo o meno per questo o
quel tipo di robot in un film di fantascienza, se essere tristi o meno quando il
personaggio di un film o di un romanzo va incontro a una fine violenta, se
ritenere o meno che una particolare persona in là con gli anni "non c'è più", e
così via, riflettono precisamente questo tipo di continuum numerico nelle menti
di queste persone, che lo ammettano o no.

Forse vi chiederete se il mio aver disegnato un cono che senza scrupoli
raffigura "gradi di possesso d'anima" durante lo sviluppo di un dato essere
umani implica che sarei più incline, se sottoposto a enorme pressione (come nel
film *La scelta di Sophie*), a spegnere la vita di un bimbo di due anni
piuttosto che la vita di un adulto di venti. La risposta è: "No, non lo
implica".  Anche se credo sinceramente che ci sia molta più anima nel ventenne
che non nel bimbo di due anni (un punto di vista che senza dubbio potrà
sconcertare molti lettori), ho tuttavia enorme rispetto per la *potenzialità*
del bimbo di due anni di *sviluppare* un'anima molto più grande nel corso di una
dozzina di anni. Per di più, io sono stato costruito dal meccanismo di miliardi
di anni di evoluzione fino a percepire nel bimbo quella che, in mancanza di una
parola migliore, chiamerò "graziosità", e la presenza ben percepibile di questa
conferisce al bimbo un guscio di protezione straordinariamente robusto contro
ogni genere di attacco non solo da parte mia, ma da parte di ogni umano di
qualunque età, sesso e convinzioni.

Luci accese?
------------

L'intento principale di questo libro è cercare di mettere a fuoco la natura di
quel "genere speciale di sottile pattern" che sono giunto a credere stia alla
base, o dia origine, a quello che finora ho chiamato un'"anima" o un "io". Avrei
potuto allo stesso modo parlare di "avere una luce dentro", "possedere
interiorità", o di "essere cosciente", una vecchia espressione sempre a portata
di mano.

I filosofi della mente usano spesso i termini "possedere intenzionalità" (che
significa avere convinzioni, desideri, paure e così via) o "avere semantica
(che significa la capacità di pensare realmente *in merito* alle cose, in
opposizione alla "pura e semplice" capacità di manipolare segni senza
significato in pattern complicati – una distinzione messa in evidenza nel
dialogo fra le mie versioni di Socrate e Platone).

Sebbene ognuno di questi termini metta a fuoco un aspetto leggermente diverso
dell'elusiva astrazione che ci interessa, dal mio punto di vista essi sono tutti
pressoché intercambiabili. E tutti questi termini vanno intesi, lo ripeto, come
se fossero posizionati a diversi livelli lungo una scala mobile, piuttosto che
come interruttori in/off, bianco/nero, sì/no.

Post Scriptum
-------------

La prima versione di questo capitolo era stata scritta due anni fa, e benché
discutesse del mangiar carne e dell'essere vegetariani, era sull'argomento
decisamente più povera di quanto lo sia questa versione finale. Alcuni mesi più
tardi, mentre lo stavo "rimpolpando" con il riassunto del breve racconto *Pig*,
mi sono ritrovato improvvisamente a mettere in discussione la linea di
demarcazione che avevo accuratamente tracciato due decenni prima e con la quale
da allora avevo sempre convissuto (anche se talvolta con un certo malessere) –
vale a dire la linea divisoria fra i mammiferi e gli altri animali.

D'un tratto, ho cominciato a sentirmi fortemente a disagio all'idea di mangiare
pollo e pesce, sebbene l'avessi fatto per circa vent'anni, e così, con mia
stessa sorpresa, sono ritornato di punto in bianco "vivo e vegetariano", per
così dire. E per una notevole coincidenza, i miei due figli sono arrivati in
modo indipendente a simili conclusioni quasi esattamente nello stesso periodo,
cosicché, nell'arco di appena un paio di settimane, la nostra dieta in famiglia
è diventata completamente vegetariana. Sono ritornato allo stesso punto di cui
mi trovavo all'età di ventun anni in Sardegna, ed è il punto in cui ho
intenzione di fermarmi.

Scrivere questo capitolo ha quindi provocato sul suo autore un effetto boomerang
totalmente inaspettato – e, come vedremo in successivi capitoli, un tale
imprevedibile rimbalzo di scelte appena compiute, seguito dall'incorporazione
delle loro ripercussioni nel proprio modello di sé, rappresenta un esempio
eccellente di strano anello nel nucleo centrale di un io. Possiamo così
considerare questo anello come il primo a presentarsi nella serie offerta da
questo libro di anelli nell'io.

[3]:https://some1elsenotme.wordpress.com/2010/03/18/lillusorio-luccichio-del-proprio-io/
[4]:http://www.ams.org/notices/200707/tx070700852p.pdf
