title: Il Culto del Cargo all'Inverso
date: 2018-02-12 20:25
tags: politica, antropologia, società

La definizione originale nasce da un [commento su reddit][1] dell'utente "idioma" nel contesto delle menzogne dell'amministrazione di Trump negli Stati Uniti, ma l'osservazione si applica ampiamente alla politica contemporanea internazionale, in special modo alle varie destre.  
Le varie teorie cospirazioniste, la cui popolarità può essere politicamente trasversale, quelle tendenze antiscientifiche che tendono ad equiparare rimedi esoterici alla medicina affermando che entrambe non funzionano ma almeno quelli esoterici non fanno male, ottengono di acuire questo effetto, di allenare le persone ad accettare questa specie di qualunquismo secondo il quale non esiste verità e tutto diventa riconducibile ad opinioni soggettive e fedi.

----

> L'amministrazione Trump mente continuamente, ma non fa alcuno sforzo per far sembrare che non stia mentendo.

Dopo il collasso dell'Unione Sovietica, ci si riferiva a questo genere di cinismo come "effetto culto del cargo inverso".

Nel culto del cargo che conosciamo, troviamo persone che vedono una pista d'atterraggio e del cargo che viene rilasciato, quindi ne costruiscono una con della paglia, sperando nello stesso risultato. Non conoscono la differenza tra una pista d'atterraggio di paglia ed una reale, vogliono solo il cargo.

Nel culto del cargo all'inverso, abbiamo persone che vedono una pista d'atterraggio e del cargo che viene rilasciato, quindi ne costruiscono una di paglia, ma con una differenza:

Quando costruiscono la pista di paglia, non lo fanno perché sperano nello stesso esito. Conoscono la differenza, e lo sanno perché la loro pista è fatta di paglia e non porterà loro alcun cargo, ma essa ha uno scopo diverso. Non mentono ai campagnoli dicendo loro che una pista di paglia porterà loro del cargo. Sarebbe una bugia facile da scoprire. Al contrario, ciò che fanno è dire chiaramente che la pista è fatta di paglia e che non funziona, ma aggiungono che anche la pista dell'altro tizio non funziona. Ti dicono che **nessuna pista porta del cargo**. Tutta _l'idea_ del cargo è una menzogna, e lo sono questi folli, con le loro elaborate piste di legno, cemento e metallo che sono superflue e sciocche quanto quella di paglia.

I sovietici degli anni '80 sapevano che il governo stava mentendo sulla forza ed il potere della loro società, il Partito Comunista non poteva nascondere tutte le storture che il popolo vedeva quotidianamente. Questo non fermò la direzione sovietica dal mentire. Al contrario, essi accusarono l'occidente di essere parimenti ingannevole. _"Certo, le cose possono star andando male qua, ma sono altrettanto pessime in America, ed in America il popolo è talmente sciocco da credere nelle bugie! Non come voi, persone intelligenti. Voi lo capite. Voi lo sapete che è una bugia."_

Ai sostenitori di Trump non interessa venire ingannati. Puoi continuare ad evidenziare le bugie fino a diventare paonazzo, ma per loro non farà differenza. Perché? Perché per loro è solo un gioco. I media mentono, i blogger mentono, i politici mentono, è tutto un mucchio di menzogne. I fatti non sono importanti, perché anch'essi sono menzogne. Tutti quei troll su Twitter, 4Chan, T_D, ecc. si stanno facendo delle grasse risate. Si congratulano a vicenda per essere così scaltri. Siamo noi gli sciocchi che ancora credono in qualcosa. Non esiste e probabilmente non è mai esistito alcun cargo.

[1]: https://np.reddit.com/r/politics/comments/5rru7g/kellyanne_conway_made_up_a_fake_terrorist_attack/dd9vxo2/
