title: Antropologi del futuro
date: 2015-11-16
tags: racconti, fantascienza
summary: Il seguente frammento è stato ritrovato nel computer di un'astronave generazionale abbandonata, orbitante attorno alla stella Epsilon Eridani.

Il seguente frammento è stato ritrovato nel computer di un'astronave generazionale abbandonata, orbitante attorno alla stella Epsilon Eridani.

[...] (1)Ci sono molti vocaboli che non hanno un corrispettivo nella nostra
lingua. “Guerra”, “confini”, “prigione” (2). Abbiamo difficoltà a descrivere
brevemente il loro significato. [...] da ciò che abbiamo compreso, nella loro
rete di relazioni vi erano fattori ambientali che mantenevano costanti i
sentimenti di paura, impotenza e rabbia. Quando un individuo e dei gruppi si
sentono minacciati, tendono a concentrare più potere per contrastare la
minaccia, oppure abbandonano la propria responsabilità, affidando le decisioni
a individui o gruppi che hanno già un grande potere. Questo li metteva sempre a
rischio di perdere il controllo. Era molto facile, quindi, per chi desiderava
conservare tale potere, far percepire costantemente un senso di minaccia ed
insicurezza nelle persone.

Restavano così bloccati in una fase infantile dello sviluppo della coscienza.
Solo con enorme sforzo potevano arrivare ad una dimensione matura dell’essere,
grazie alla quale si sviluppa una collettività di individui connessi.[...] (3)

Note di traduzione
------------------

1. L’archivio contiene un documento parziale.

2. I termini virgolettati sono stati riportati dal testo
direttamente, senza alcuna traduzione.

3. La loro lingua è composta da un numero non molto elevato di vocaboli base,
ma è molto dinamica e combinabile. Il loro vocabolario non contiene termini per
guerra, confini, dittatura e molti altri. Altri termini che a noi paiono
comuni, da loro vengono tradotti in modo inaspettato. Possono descrivere queste
cose, ma in una traduzione letterale, apparirebbero come complicati giri di
parole. Termini come “capo” o “governo”, vengono tradotti in “genitore”, perché
nella loro cultura, solamente quando l’individuo è ancora poco sviluppato ed
immaturo, nella prima infanzia, la volontà del genitore può prevalere su quella
del bambino. Successivamente, per via dell’influenza culturale, il bambino
inizia presto, spontaneamente, a scegliere per sé ed a partecipare
responsabilmente alle attività della società.