title: I reietti dell'altro pianeta
date: 2016-10-07 21:58
tags: recensioni, libri, anarchia, società, fantascienza
summary: Da appassionato di fantascienza, ho involontariamente trascurato _Ursula Le Guin_ per troppo tempo, ed ho quindi deciso di rimediare iniziando da questo romanzo.  Mi è piaciuto decisamente molto. Fa ciò che la migliore fantascienza è in grado di fare, cioè analizzare il presente con occhi diversi per permetterci di osservare noi stessi dall'esterno.

![I reietti dell'altro pianeta][1]{: style="float:right"}

Da appassionato di fantascienza, ho involontariamente trascurato [Ursula Le
Guin][2] per troppo tempo, ed ho quindi deciso di rimediare iniziando da questo
romanzo.  Mi è piaciuto decisamente molto. Fa ciò che la migliore fantascienza è
in grado di fare, cioè analizzare il presente con occhi diversi per permetterci
di osservare noi stessi dall'esterno.

La storia di uno studioso, un fisico che ha sviluppato una rivoluzionaria teoria
sul tempo, nato su un pianeta, Anarres, la cui società è organizzata
anarchicamente in seguito all'esilio volontario di un gruppo di ribelli del
pianeta gemello Urras, dominato da un'organizzazione di tipo gerarchico e
capitalista. La vita ed i viaggi di questo studioso, su Anarres ed Urras,
metteranno in evidenza le differenze fra le due società e le caratteristiche
positive e negative di entrambe.

Se le critiche riguardo ad Urras sono facilmente prevedibili, essendo uno
specchio della nostra società capitalista, consumista ed ossessionata dal
possedere (il titolo originale del libro è, infatti _"The Dispossessed"_),
quelle riguardo ad Anarres sono meno scontate e riescono a mettere in evidenza
come sia possibile che anche in una società che mira alla massima libertà e
benessere per tutti, possano comunque emergere situazioni di dominio e
repressione, sotto diversa forma. Vi si trovano i temi più cari dell'anarchismo,
fra cui il principio di mutuo appoggio, la coerenza fra scopo e mezzi, il
rapporto fra rivolta, rivoluzione ed il pericolo che una rivoluzione si
sedimenti degenerando in governo e burocrazia.

Un libro che metterei senza remore tra i grandi classici della fantascienza
sociale distopica, assieme a [1984][3] e [Farentheit 451][4].

[1]: {filename}/images/i-reietti-dell-altro-pianeta.jpg
[2]: https://it.wikipedia.org/wiki/Ursula_Le_Guin
[3]: https://it.wikipedia.org/wiki/1984_(romanzo)
[4]: https://it.wikipedia.org/wiki/Fahrenheit_451
