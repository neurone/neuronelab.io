title: Idee sulla Natura
date: 2015-02-16 20:37:29 +0100
tags: filosofia
summary: Quando penso razionalmente alla natura, coincide con l'esistente ed è quindi un'idea omni-inclusiva, ma nel comune discorrere, si tende ad usare un concetto di natura relativo e localizzato: quella parte della natura con cui abbiamo frequenti relazioni e che si avvicina all'idea comune d'ambiente floristico e faunistico selvatico.

Quando penso razionalmente alla natura, coincide con l'esistente ed è quindi un'idea omni-inclusiva, ma nel comune discorrere, si tende ad usare un concetto di natura relativo e localizzato: quella parte della natura con cui abbiamo frequenti relazioni e che si avvicina all'idea comune d'ambiente floristico e faunistico selvatico.

Come [Goethe][1], non riesco ad ammettere che qualcosa possa essere contro-natura o
esterno alla natura. *“Nel tentare di opporci alla Natura noi, nell'atto stesso
di farlo, operiamo secondo le leggi della natura!”*

Quelle che chiamiamo *leggi della natura*, sono deduzioni ed induzioni che
deriviamo dall'analisi dell'osservabile e razionalizzabile. Lo si osserva nel
succedersi delle varie teorie scientifiche, dei pensieri filosofici e religiosi.
Razionalmente ed istintivamente convincenti, fino a quando non emergono teorie e
idee che risuonano ancora meglio con l'ambiente fisico e culturale del momento e
che riescono a spiegare in modo più completo ciò che veniva descritto dalle
precedenti.

Forse non potremmo mai conoscere perfettamente ed oggettivamente la natura nel
suo totale complesso. Abbiamo moltitudini di menti all'opera, siamo in grado di
costruirci strumenti per estendere queste menti, ma fra noi siamo *macchine*
simili, con caratteristiche comuni. Come potremmo accorgerci di eventuali vizi
sistemici?

Questo motivo mi porta ad essere critico sul positivismo scientifico. Capita
spesso che proprio infrangendo il metodo, si riesca a fare quel pensiero
*laterale*, fuori dallo schema, che riesce a portare la ricerca al di fuori del
vicolo cieco. A tal proposito, sono molto interessanti le posizioni di due
filosofi della scienza: [Imre Lakatos][2] e [Paul Feyerabend][3].

Tanto meno affidandosi ad altri dogmi, di tipo religioso ad esempio, si riuscirà
a trascinare avanti questa conoscenza così aleatoria. È necessario essere sempre
critici riguardo ad ogni idea, ascoltare i saggi, i maestri, ma mai diventare
fedeli ad essi.

Comprenderli e poi abbandonarli allo stesso modo in cui, nella maturazione
dell'individuo, esso ama ed eleva a divinità i genitori durante l'infanzia, per
poi gradualmente contrastarli, cercare indipendenza nell'adolescenza e quindi
staccarsene, abbandonando anche l'antagonismo dell'adolescenza mano a mano che
sviluppa una personalità indipendente.

La natura include, ma non è corrispondente a quel *fatterello* che ci coinvolge
da vicino e a cui noi diamo moltissima importanza, chiamato *vita*.

La natura non è stata distrutta durante il periodo delle glaciazioni o
dell'estinzione dei dinosauri. L'ambiente terrestre si è modificato diventando
inadatto alla continuazione della vita di alcune specie, mentre altre hanno
continuato a vivere. Eppure, dal punto di vista dei dinosauri, saremmo stati
tentati di affermare che la caduta del meteorite avrebbe distrutto l'ambiente,
la vita e forse la natura tutta. Saremmo stati dinosauro-centrici, come ora
siamo antropocentrici, ed in entrambi i casi bio-centrici.

Quando pensiamo di poter distruggere la natura, ci sopravvalutiamo. Quello che
facciamo quando crediamo di *distruggere la natura* è *solamente* rendere meno
vivibile per la nostra specie ed altre l'ambiente nel quale siamo immersi, da
cui dipendiamo e dal quale ci siamo formati.

Da tutti i punti di vista, si tratta sempre di adattamenti ed aggiustamenti. La
differenza fra l'uomo ed un castoro, sta nella portata delle modifiche che esso
può effettuare.

Contenere le nostre capacità di modifica del territorio, della flora e fauna
terrestre ci conviene. Facendo così, evitiamo che le generazioni future si
trovino in situazioni di sopravvivenza sempre più difficili. È una necessità
morale e di sopravvivenza per la nostra specie.

L'ambiente, come la natura, non si può né distruggere, né estinguere. Chiamiamo
*ambiente* anche la superficie di Marte, Venere, o altri pianeti dove non
potremmo vivere, ma se l'ambiente terrestre si trasformasse fino a diventare
come quello marziano, saremmo tentati di pensare che sia stato distrutto, perché
lì vi moriremmo.

[1]:https://it.wikipedia.org/wiki/Johann_Wolfgang_von_Goethe
[2]:https://it.wikipedia.org/wiki/Imre_Lakatos
[3]:https://it.wikipedia.org/wiki/Paul_Feyerabend
