title: La comoda partitura per sceneggiature di Blake Snyder (più Arrival)
date: 2017-05-31 14:32
tags: film

Il libro [Save the Cat][1] di _Blake Snyder_ è un testo per scrittori e sceneggiatori che definisce, formalizza e cristallizza in una serie di fasi fisse la classica struttura narrativa in tre atti. Ho letto commenti molto positivi su di esso da parte di scrittori, che testimoniano quanto sia stato loro utile. Tutta Hollywood ormai sembra essersi orientata verso questa precisa struttura, motivo per cui - ed ecco il lato negativo - gli eventi che si vedono accadere nei film, specialmente in quelli di maggior successo commerciale, appaiono sempre simili e lo svolgersi della trama diventa spesso troppo facilmente prevedibile.
Se avete visto degli adattamenti da romanzi o racconti e vi siete chiesti per quale motivo abbiano aggiunto delle cose che non hanno nulla a che fare con l'originale, oppure abbiano cambiato la sequenza di certi avvenimenti, o inserito alcune forzature, la ragione può essere cercata nella necessità di rispettare questa rigida partitura.
Premetto che non ho personalmente letto il libro, quindi mi rifaccio ad osservazioni personali ed articoli che ho letto riguardo ad esso.

Ecco come appare la struttura:

**Immagine di apertura** - Un'immagine che rappresenti il movente ed il tono della storia. Un'istantanea dei problemi del protagonista prima che l'avventura inizi.

**Preparazione** - Espande ciò che si è visto nell'apertura. Presenta il mondo del protagonista e ciò che manca nella sua vita.

**Affermazione del tema** - Avviene durante la preparazione e spiega ciò di cui parla la storia; il messaggio, la _verità_. Solitamente viene raccontato dallo stesso protagonista oppure in sua presenza. A questo punto della storia però lui non è ancora in grado di comprendere la _verità_... fino a quando non avrà una qualche esperienza personale ed un contesto a supporto di essa.

**Catalizzatore** - Il momento durante il quale la vita per come il protagonista la conosce, cambia. Può essere un telegramma, scoprire l'amata che lo tradisce, l'arrivo di un mostro sull'astronave, incontrare l'amore della vita, ecc... Ciò che c'era prima sparisce ed un cambiamento è in arrivo.

**Dibattito** - Il cambiamento, però, è spaventoso e per un momento il protagonista ha dei dubbi sul percorso da compiere. Può affrontare la sfida? Ne vale la pena? È l'ultima possibilità per l'eroe di tirarsi indietro.

**Entrata del secondo atto** - Il protagonista fa la sua scelta ed il viaggio inizia. Si abbandona il mondo delle teorie e si entra nel mondo al rovescio dell'Atto Due.

**Storia B** - In questo punto c'è una discussione sul Tema - il nocciolo della Verità. Solitamente, questa discussione è tra il protagonista e la persona di cui è innamorato. Per questo motivo, la Storia B è solitamente chiamata la "storia d'amore".

**La promessa della premessa** - Questa è la parte divertente della storia. Quando Indiana Jones cerca di battere i nazisti ne _I predatori dell'arca perduta_, quando l'investigatore trova la maggior parte delle prove e schiva tutte le pallottole. Questo è il momento in cui il protagonista esplora il nuovo mondo ed il pubblico viene intrattenuto con ciò che era stato promesso nella premessa.

**Metà** - A seconda della storia, questo è il momento in cui tutto è "fantastico", oppure tutto è "terribile". Il protagonista ottiene tutto ciò che pensava di volere ("fantastico"), oppure non lo ottiene ("terribile"). Ma tutto ciò che crediamo di volere potrebbe rivelarsi non essere ciò di cui abbiamo veramente bisogno.

**I cattivi si avvicinano** - Dubbi, gelosia, paura, avversari fisici o psicologici che si riorganizzano per impedire al protagonista di raggiungere l'obiettivo e distruggono la situazione "fantastica"/"terribile".

**Tutto è perduto** - L'inverso di ciò che accade nella Metà. Il momento in cui il protagonista si accorge di aver perso tutto ciò che aveva ottenuto, oppure che ciò che aveva ottenuto non ha alcun valore. L'obiettivo iniziale ora sembra più lontano che mai. È a questo punto che solitamente qualcuno o qualcosa muore, che sia fisicamente o emotivamente. Ma la morte di qualcosa di vecchio fa spazio per la nascita di qualcosa di nuovo.

**La notte oscura dell'anima** - Il protagonista tocca il fondo e sprofonda nella disperazione. Il momento _"Dio, perché mi hai abbandonato?"_. Il lutto per ciò che è "morto" - il sogno, l'obiettivo, il personaggio mentore, l'amore della tua vita, ecc. Ma è necessario che il protagonista cada completamente prima che si possa rialzare e tentare di nuovo.

**Entrata nel terzo atto** - Grazie ad una nuova idea, un'ispirazione, oppure un suggerimento riguardo al Tema da parte della Storia B (solitamente l'innamorato), il protagonista decide di provare di nuovo.

**Finale** - In questa fase, il protagonista riesce ad incorporare il Tema - quel nocciolo di Verità che ora finalmente viene compreso - nella propria lotta per l'obiettivo finale, perché è riuscito a fare esperienza con la Storia A ed ha il contesto fornito dalla Storia B. Il terzo, è un atto di sintesi.

**Immagine finale** - All'oppposto dell'immagine di apertura, questa mostra che vi è stato un cambiamento nel protagonista.

**FINE**

Su questa pagina, una lista parziale di film con la relativa _partitura_: [Beat Sheets - Alphabetical Listing | Save the Cat!®][2]

Prendo ad esempio il recente film [Arrival][3], tratto dal racconto [Storia della tua vita][4] di [Ted Chiang][5]. **Saltate questo paragrafo per evitare anticipazioni, nel caso non abbiate visto il film o letto i racconto.**
Il racconto è situato in un periodo in cui la figlia è già nata e, si suppone, ancora non in grado di capire ciò che le dice la madre. In quel punto della vita della figlia, la madre, che ha già vissuto la vicenda dell'incontro con gli eptapodi alieni, le racconta alternativamente di questo incontro e di ciò che accadrà nel futuro della figlia. La vicenda degli eptapodi è il fulcro della storia. Durante essa, grazie agli studi sul loro linguaggio parlato e scritto si scoprirà come gli alieni concepiscono la realtà ed il tempo, capendo che essi concepiscono il tempo in modo simultaneo, mentre gli umani lo fanno in modo sequenziale. Il [principio di Fermat][6] sul fenomeno della rifrazione gioca un ruolo molto importante in questa spiegazione. Il racconto è tutto uno studio ed un ragionamento su questo ed il rapporto con menti diverse dalle nostre. Non vi sono conflitti o situazioni di pericolo.
Perché allora, nel film, il racconto alla figlia è diventato una serie di flashforward inaspettati, che iniziano ad avvenire ancora prima che la protagonista riesca a pensare alla maniera degli eptapodi? Perché ad un certo punto ci dev'essere la vicenda di un militare paranoico che cerca di far esplodere la navicella aliena; cosa che causerà una crisi di sfiducia nel mondo e rischierà di mandare all'aria tutto ciò che si era ottenuto, fino a quando la protagonista non riuscirà a mettere in pratica ciò di cui ha fatto esperienza fermando il generale cinese?
La risposta, ora dovrebbe essere chiaro a tutti, va cercata nella "necessità" di rispettare le fasi della sceneggiatura di Blake Snyder.

[1]:https://en.wikipedia.org/wiki/Blake_Snyder
[2]:http://www.savethecat.com/beat-sheets-alpha
[3]:https://it.wikipedia.org/wiki/Arrival_(film)
[4]:https://it.wikipedia.org/wiki/Storie_della_tua_vita#Storia_della_tua_vita
[5]:https://it.wikipedia.org/wiki/Ted_Chiang
[6]:https://it.wikipedia.org/wiki/Principio_di_Fermat
