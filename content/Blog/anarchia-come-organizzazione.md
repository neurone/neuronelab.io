title: Anarchia come organizzazione
date: 2015-09-29 18:44
tags: anarchia, politica, filosofia, società
summary: Lo stile di Colin Ward è sempre piacevole. Un bravissimo divulgatore che riesce ad approfondire, mantenendo una scrittura discorsiva e piacevole. Lo scopo di questo saggio è dimostrare come una società organizzata anarchicamente sia possibile anche qui ed ora, senza la necessità di mitiche rivoluzioni e sfatando molti stereotipi comuni.

Lo stile di Colin Ward è sempre piacevole. Un bravissimo divulgatore che riesce ad approfondire, mantenendo una scrittura discorsiva e piacevole. Lo scopo di questo saggio è dimostrare come una società organizzata anarchicamente sia possibile anche qui ed ora, senza la necessità di mitiche rivoluzioni e sfatando molti stereotipi comuni.

![Anarchia come organizzazione]({filename}/images/anarchia-come-organizzazione.jpg "Copertina: Anarchia come organizzazione"){: style="float:right"}

Scopriamo che in moltissime occasioni ed ambiti, la società si auto organizza anarchicamente già ora, nonostante molti non se ne rendano conto, e che questo tipo di organizzazione, a fronte di una maggiore responsabilizzazione dei singoli e di un periodo di "rodaggio" superiore a quello delle organizzazioni centralizzate, tende ad essere, nel lungo periodo più efficiente, stabile ed "adattativa". Il libro spazia all'antropologia, alla psicologia sociale, portando esempi di esperimenti e di situazioni reali, che mostrano come funzioni l'auto organizzazione nella società umana. In un capitolo si fa anche un parallelo con la cibernetica e l'organizzazione naturale del cervello umano. Inoltre, vengono spiegate certe posizioni degli anarchici nei confronti del sistema penitenziario, delle forze dell'ordine, della legge, e del sistema previdenziale, difficili da comprendere per chi non è mai entrato a contatto con questa ideologia. Se non avete mai letto nulla riguardo all'anarchia, ma pensate di avere già una sicura opinione in merito, vi consiglio la lettura di questo libro. Vi sorprenderà.

Il libro è pubblicato dalla casa editrice [Elèuthera][3]. Sul sito di
[Tecalibri][4], potete trovare degli [estratti][5] e in coda a questo post,
pubblicherò una parte di un capitolo che fa un parallelo fra anarchia e
cibernetica.

Quarta di copertina:

> "L'anarchismo non è la visione, basata su congetture, di una società
> futura, ma la descrizione di un modo umano di organizzarsi radicato
> nell'esperienza di vita quotidiana, che funziona a fianco delle tendenze
> spiccatamente autoritarie della nostra società e nonostante quelle"
> 
> Per molti l'anarchia è un improbabile modello sociale basato sulla
> disorganizzazione caotica. Per altri è invece un'utopia generosa ma
> impraticabile. Ribaltando entrambe le interpretazioni, Ward la intende come
> un'efficace forma di organizzazione non gerarchica, una vivente realtà sociale
> che è sempre esistita e tutt'ora esiste nelle pieghe della prevalente società
> del dominio. Utilizzando un'ampia varietà di fonti, l'autore articola in modo
> convincente la sua tesi volutamente paradossale, con argomenti tratti dalla
> sociologia, dall'antropologia, dalla cibernetica, dalla psicologia industriale,
> ma anche da esperienze nel campo della pianificazione, del lavoro, del gioco...
> 
> Colin Ward (1924 – 2010) è stato architetto, insegnante, giornalista e
> scrittore, ma soprattutto uno degli anarchici più influenti e innovativi del
> secondo Novecento. A partire dagli anni Sessanta, elabora un anarchismo
> pragmatico del «qui e ora» che rintraccia in particolare nei modi non ufficiali
> con cui la gente una l'ambiente urbano e rurale, rimodellandolo secondo i
> propri bisogni. Autore di una ventina di libri, per elèutera sono inoltre
> usciti Dopo l'automobile, Acqua e comunità, L'anarchia e Conversazioni con
> Colin Ward, a cura di David Goodway.

Anarchia e cibernetica
----------------------

Trascrivo una parte del capitolo 4, *L'armonia nasce dalla complessità.*

L'anarchia risulta non dalla semplicità di una società priva di organizzazione
sociale, ma dalla complessità e dalla molteplicità di forme di organizzazione
sociale. La cibernetica, scienza dei sistemi di comunicazione e controllo, può
aiutare a comprendere la concezione anarchica dei sistemi complessi
auto-organizzanti. Se paragoniamo la struttura biologica ai sistemi politici,
ha scritto il neurobiologo [Grey Walter][6], il cervello umano sembra
illustrare i limiti e le potenzialità di una comunità anarco-sindacalista. «Nel
cervello non c'è nessun capo, nessun neurone oligarchico, nessun dittatore
ghiandolare.  All'interno delle nostre teste la nostra vita dipende
dall'eguaglianza di possibilità, dalla specializzazione non specialistica,
dalla libera comunicazione con il minimo di limiti, insomma da una libertà
senza ingerenze.  Qui le minoranze locali hanno la possibilità di controllare i
loro mezzi di produzione e di espressione in un rapporto di libertà e di
eguaglianza con i vicini(1)». Partendo da queste indicazioni John D. McEwan ha
sviluppato in modo approfondito lo studio del modello cibernetico.
Sottolineando l'importanza del principio di complessità sufficiente («se si
vuole conseguire la stabilità, la complessità del sistema di controllo deve
essere almeno pari alla complessità del sistema che deve essere controllato»),
riporta il discorso di [Stafford Beer][7] sulla diversità a questo principio
della tradizionale concezione manageriale dell'organizzazione. Beer immagina
che un osservatore extraterrestre esamini le attività ai livelli più bassi di
qualche grossa impresa, i cervelli dei lavoratori che le adempiono, il piano
organizzativo che ha la pretesa di mostrare come è controllato il lavoro: ne
deduce che gli individui al vertice della gerarchia devono avere la testa con
una circonferenza di vari metri. McEwan contrappone due modelli diversi di
controllo e formazione delle decisioni:

> Per primo abbiamo il modello comune tra i teorici del management
> industriale, che la il suo corrispettivo nell'idea convenzionale di governo
> centrale della società. È un modello che prevede una rigida gerarchia
> piramidale, con linee di «comunicazione e comando» che corrono
> verticalmente dal vertice alla base della piramide. C'è una suddivisione
> rigida delle responsabilità, ogni elemento ha un suo ruolo specifico, le
> procedure da seguire a ogni livello sono prefissate con limiti abbastanza
> ristretti e possono essere modificate solo per decisione di qualcuno che
> occupi una posizione superiore nella gerarchia. La funzione del gruppo che
> sta al vertice della piramide è spesso ritenuta paragonabile a quella del
> «cervello» dell'organizzazione.
> 
> L'altro modello ci viene dalla cibernetica, è il modello dei sistemi che si
> auto-organizzano progressivamente, in grado di affrontare situazioni
> complesse e imprevedibili. È una struttura mutevole, che si trasforma per
> continuo ritorno di informazioni dall'ambiente, che mostra una «ridondanza
> di comandi potenziali» e comprende strutture di controllo complesse e
> interdipendenti. L'apprendimento dei dati e la capacità decisionale sono
> distribuiti su tutto il sistema, magari un po' più concentrati in alcune
> aree(2).

Lo stesso tipo di critica alla concezione gerarchica e centralizzata
dell'organizzazione è stato espresso più di recente (e con un linguaggio direi
meno efficace) da [Donald Schon][8] nelle BBC Reith Lectures del 1970. Echi
scrive che «il modello centro-periferia è stato quello che nella nostra società
ha presieduto al formarsi e diffondersi di strutture organizzative con
caratteristiche di elevata specificità. In un sistema di questo tipo è
essenziale tanto la semplicità quanto l'uniformità del messaggio. La capacità
di affrontare situazioni complesse si basa su un messaggio semplice e una
crescita attraverso un'uniformità di risposte». Come gli anarchici, anche lui
vede l'alternativa in una rete «di elementi connessi tra loro direttamente
invece che mediante il centro», caratterizzata da «libertà d'azione,
complessità, stabilità, omogeneità e flessibilità»; rete di elementi in cui
«nuclei di leadership emergano e si dissolvano» e che possieda
«un'infrastruttura tale da tenere insieme il sistema... senza alcun intervento o
appoggio centrale»(3). Tra i recensori del saggio di Schon solo [Mary
Douglas][9] nota l'analogia tra questa struttura a rete e la società tribali
prive di governo:

> Una volta gli antropologi ritenevano che le tribù senza autorità centrale non
> potessero avere unità politica. Il prestigio che godeva la teoria centralista
> ci impediva di comprendere quello che avevamo sotto gli occhi.
> Successivamente, negli anni Quaranta, [Evans-Pritchard][10] ha analizzato il
> sistema politico dei Nuer e [Fortes][11] quello dei Tallensi. Dai loro studi è
> risultato qualcosa di sorprendentemente simile al sistema a rete di cui parla
> Schon: una struttura politica senza alcun centro o capo, liberamente tenuta
> insieme dalla opposizione delle sue parti. Le funzioni in altri contesti
> delegate a un'autorità centrale erano distribuite tra l'intera popolazione.
> In ogni situazione gli avvenimenti politici erano affrontati in un linguaggio
> molto generale, il linguaggio delle relazioni interparentali, che si adeguava
> solo molto approssimativamente ai fatti della politica. Le diverse versioni
> delle loro norme di governo avevano solo una vaga somiglianza nei diversi
> contesti. In queste condizioni il sistema si rivelava molto agile e
> difficilmente deteriorabile(4).

È chiaro così che sia l'antropologia sia la teoria cibernetica convalidano
l'opinione espressa da [Kropotkin][12]: che in una società senza governo l'armonia è
una risultante di «una continua acquisizione e riacquisizione di equilibrio tra
un gran numero di forze e influenze», che si esplicano in «una fitta rete
composta da una infinita varietà di gruppi e federazioni di ogni tipo e
dimensione: locali, regionali, nazionali o internazionali; che possono essere
temporanei o pressoché permanenti; unificati da ogni possibile scopo:
produzione, commercio e consumo, tutela sanitaria, istruzione, protezione
reciproca, difesa del territorio e così via; che permettono di rispondere a un
numero sempre crescente di bisogni sociali, artistici, scientifici,
letterari»(5).

Il modello che prevede strutture centrali di governo appare estremamente rozzo
al confronto, dal punto di vista dei servizi sociali, dell'industria,
dell'istruzione, della pianificazione economica. Non c'è da stupirsi se non è
in grado di rispondere ai bisogni attuali. E non c'è da stupirsi se, quando si
tenta di usare mezzi come la fusione, la razionalizzazione, la coordinazione
per risolvere gli attuali problemi di funzionamento, l'unico risultato è
l'incepparsi delle linee di comunicazione.

L'alternativa anarchica è quella che propone la frammentazione e la scissione
al posto della fusione, la diversità al posto dell'unità, propone insomma una
massa di società e non una società di massa.

Note:
-----

1. W. Grey Walter, The Development and Significance of Cybernetics, «Anarchy»,
   n .25, marzo 1963.
2. John D. McEwan, Anarchism and the Cybernetics of Self-organizing Systems,
   «Anarchy», n. 31, settembre 1963; ristampato in A Decade of Anarchy, cit.
   (trad. it.: Cibernetica dei sistemi auto-organizzati, «Volontà», XXXIV, n.
   3, 1990).
3. Donald Schon, Beyond the Stable State, London 1971.
4. Mary Douglas, «The Listener», 1971.
5. Piotr Kropotkin, voce Anarchismo, scritta nel 1905 per la Encyclopedia
   Britannica, 11a edizione; ristampata in Anarchism and Anarchism Communism,
   London 1987.

- [Colin Ward su Wikipedia][1]
- [Colin Ward su Anarchopedia][2]

[1]:https://it.wikipedia.org/wiki/Colin_Ward
[2]:http://ita.anarchopedia.org/Colin_Ward
[3]:http://www.eleuthera.it/scheda_libro.php?idlib=328
[4]:http://www.tecalibri.info/
[5]:http://www.tecalibri.info/W/WARD-C_anarchia.htm
[6]:https://en.wikipedia.org/wiki/William_Grey_Walter
[7]:https://en.wikipedia.org/wiki/Stafford_Beer
[8]:https://en.wikipedia.org/wiki/Donald_Sch%C3%B6n
[9]:https://en.wikipedia.org/wiki/Mary_Douglas
[10]:https://en.wikipedia.org/wiki/E._E._Evans-Pritchard
[11]:https://en.wikipedia.org/wiki/Meyer_Fortes
[12]:https://en.wikipedia.org/wiki/Peter_Kropotkin
