title: Una nuova teoria fisica sulla vita
date: 2016-06-17 11:01
tags: scienza
summary: Perché esiste la vita? Le ipotesi popolari l'attribuiscono ad una zuppa primordiale, un fulmine, ed una colossale dose di fortuna. Ma se questa nuova provocativa teoria fosse corretta, la fortuna avrebbe poco a che fare con essa. Al contrario, secondo il fisico che ha proposto l'idea, l'origine e la successiva evoluzione della vita derivano dalle leggi base della natura e "dovrebbe sorprendere quanto una roccia che rotola in discesa".

Tradotto da [Quanta Magazine][1]

### Perché esiste la vita?
![Jeremy England][12]

Le ipotesi popolari l'attribuiscono ad una zuppa primordiale, un fulmine, ed una
colossale dose di fortuna. Ma se questa nuova provocativa teoria fosse corretta,
la fortuna avrebbe poco a che fare con essa. Al contrario, secondo il fisico che
ha proposto l'idea, l'origine e la successiva evoluzione della vita derivano
dalle leggi base della natura e "dovrebbe sorprendere quanto una roccia che
rotola in discesa".

Dal punto di vista della fisica, c'è una differenza essenziale tra le cose
viventi e degli aggregati di atomi di carbonio: i primi tendono ad essere
migliori nel catturare energia dall'ambiente e dissiparlo come calore. [Jeremy
England][2], un trentunenne assistente professore al Massachusetts Institute of
Techology, ha derivato una formula matematica che ritiene possa spiegare questa
capacità. La formula, basata su fisica affermata, indica che quando un gruppo di
atomi è azionato da una fonte esterna di energia (come il sole o un carburante
chimico) e circondata da un bagno caldo (come l'oceano o l'atmosfera), spesso si
ristrutturerà gradualmente per poter dissipare sempre più energia. Questo può
significare che in alcune circostanze, la materia acquisisce inesorabilmente
l'attributo fisico chiave associato alla vita.

![Cellule][13]{: style="float:right"}
"Parti da un aggregato casuale di atomi, e se gli dirigi contro una luce per
sufficiente tempo, non dovresti sorprenderti di ottenere una pianta," afferma
England.

La teoria di England intende essere alla base, piuttosto che rimpiazzare, la
teoria dell'evoluzione di Darwin sulla selezione naturale, la quale fornisce una
potente descrizione della vita a livello di geni e popolazioni. "Non sto
certamente dicendo che le idee darwiniane siano sbagliate," spiega. "Al
contrario, sto solo dicendo che dal punto di vista della fisica, si potrebbe
dire che l'evoluzione darwiniana è un caso speciale di un fenomeno più
generale."

La sua idea, descritta nel dettaglio in un recente [documento][3] ed
ulteriormente elaborata in una [conferenza][4] che sta portando nella università
mondiali, ha scatenato controversie fra i suoi colleghi, che la trovano debole o
come potenziale innovazione, o entrambe.

England ha intrapreso "un passo molto coraggioso ed importante", afferma
Alexander Grosberg, un professore di fisica all'università di New York City che
seguì il lavoro di England fin dagli inizi. La "grande speranza" è che abbia
identificato il principio fisico base che guida l'origine e l'evoluzione della
vita, afferma Grosberg.

"Jeremy è praticamente il più brillante giovane scienziato che mi sia mai
capitato d'inconrtare," disse Attila Szabo, un biofisico del Laboratorio di
Fisica Chimica all'Istituto Nazionale di Sanità che corrispose con England sulla
sua teoria dopo averlo incontrato ad una conferenza. "Sono rimasto colpito
dall'originalità dell'idea."

Altri, come Eugene Shakhnovich, un professore di chimica, biologia chimica e
biofisica all'università di Harvard, non sono convinti. "Le idee di Jeremy sono
interessanti e potenzialmente promettenti, ma in questo momento sono
estremamente speculative, specialmente per come vengono applicate al fenomeno
della vita," afferma Shakhnovic.

I risultati teorici di England sono generalmente considerati validi. È la sua
interpretazione - che la sua formula rappresenti la forza motrice che soggiace
una classe di fenomeni naturali che comprendono la vita - a rimanere non
provata. Ma ci sono già delle idee su come testare questa interpretazione in
laboratorio.

"Sta tentando qualcosa di radicalmente diverso," dice Mara Prentiss, professore
di fisica ad Harvard che sta prevedendo un simile esperimento dopo aver
conosciuto il lavoro di England. "In un ottica di organizzazione, penso che
abbia avuto un idea favolosa. Giusta o sbagliata, sta meritando di venir
investigata."

![Particelle][14]{: style="float:left"}
Al centro dell'idea di England c'è la seconda legge della termodinamica,
conosciuta anche come la legge della crescita entropica o "freccia del tempo".
Le cose calde si raffreddano, i gas si diffondono nell'aria, le uova si rompono,
ma non ritornano mai integre spontaneamente; in breve, l'energia tende a
disperdersi o a sparpagliarsi col passare del tempo. L'entropia è la misura di
questa tendenza, quantificando quanto sia dispersa l'energia tra le particelle
di un sistema, e quanto siano sparpagliate queste particelle attraverso lo
spazio. Aumenta per una semplice questione di probabilità: ci sono più modi in
cui l'energia può disperdersi di quanti ce se siano per concentrarsi. Quindi,
man mano che le particelle di un sistema si spostano ed interagiscono, esse, per
una semplice questione di probabilità, tenderanno ad adottare configurazioni in
cui l'energia viene dispersa. Infine, il sistema arriverà ad uno stato di
massima entropia chiamato "equilibrio termodinamico", nel quale l'energia è
uniformemente distribuita. Una tazza di caffè e la stanza un cui è messa
diventano della stessa temperatura, ad esempio. Fintanto che la tazza e la
stanza vengono lasciate stare, il processo è irreversibile. Il caffè non si
riscalderà spontaneamente perché le probabilità sono avverse in modo
schiacciante rispetto al concentrarsi casuale dell'energia negli atomi della
stanza.

Benché l'entropia debba aumentare nel tempo in un sistema isolato o "chiuso", un
sistema "aperto" può mantenere la propria entropia bassa - cioè dividere
irregolarmente l'energia fra i propri atomi - incrementando di molto l'entropia
circostante. Nella propria influente monografia del 1944 ["What is life?"][5]
l'eminente fisico quantistico Erwin Schrödinger sostenne che questo è ciò che le
cose viventi devono fare. Una pianta, ad esempio, assorbe l estremamente
energetica luce solare, l'utilizza per produrre zuccheri, ed espelle luce
infrarossa, una forma molto meno concentrata di energia. L'entropia complessiva
dell'universo aumenta durante la fotosintesi mentre la luce solare viene
dissipata, anche se la pianta previene il proprio deperimento mantenendo una
struttura interna ordinata.

La vita non viola la seconda legge della termodinamica, ma fino ad ora, i fisici
non erano stati in grado di utilizzare la termodinamica per spiegare perché essa
nasca, in primo luogo. Nei giorni di Schrödinger, potevano risolvere le
equazioni della termodinamica solo per i sistemi chiusi in equilibrio. Negli
anni '60 il fisico belga Ilya Prigogine fece progressi nel prevedere il
comportamento dei sistemi aperti mossi da deboli fonti di energia esterne (per i
quali vinse nel 1977 il Premio Nobel per la chimica). Ma il comportamento di
sistemi lontani dall'essere in equilibrio, connessi con l'ambiente esterno e
fortemente mossi da fonti energetiche esterne, non potevano venire predetti.

Questa situazione cambiò nei tardi '90, principalmente grazie al lavoro di Chris
Jarzynski, ora all'Università del Maryland, e Garvin Crooks, ora al Laboratorio
Nazionale Lawrence Berkeley. Jarzynski e Crooks [mostrarono][6] che l'entropia
prodotta da un processo termodinamico, come il raffreddarsi di una tazza di
caffè, corrisponde ad un semplice rapporto: la probabilità che gli atomi seguano
quel processo divisa la probabilità che seguano il processo contrario (cioè, che
spontaneamente interagiscano in modo da scaldare il caffè). Col crescere
dell'entropia, cresce il rapporto: il comportamento di un sistema diventa sempre
più "irreversibile". La semplice ma rigorosa formula potrebbe, come principio,
venire applicata a qualsiasi processo termodinamico, non importa quanto veloce o
lontano dall'equilibrio. "La nostra comprensione delle meccaniche statistiche
del lontano-dall'equilibrio è migliorata enormemente," afferma Grosberg.
England, che ha ricevuto istruzione sia in biochimica che in fisica, aprì il
proprio laboratorio al MIT due anni fa e decise di applicare le nuove conoscenze
di fisica statistica alla biologia.

Usando la formulazione di Jarzynski e Crook, derivò una generalizzazione della
seconda legge della termodinamica adatta a sistemi di particelle con certe
caratteristiche: sistemi fortemente guidati da un'energia esterna come un onda
elettromagnetica, e che possono scaricare calore in un bagno circostante. Questa
classe di sistemi include tutte le cose viventi. England quindi determinò come
tali sistemi tendano ad evolvere nel tempo mentre aumenta la loro
irreversibilità. "Possiamo mostrare molto semplicemente dalla formula che i
risultati evolutivi più probabili saranno quelli che per arrivare qui hanno
assorbito e dissipato più energia dalle fonti ambientali esterne," afferma. La
scoperta ha un senso intuitivo: le particelle tendono a dissipare più energia
quando risuonano con una forza motrice, o si muovono nella direzione in cui essa
li spinge, e sono più propense a muoversi in qualsiasi momento in quella
direzione che in qualsiasi altra.

"Questo significa che aggregati di atomi circondati da un bagno di una certa
temperatura, come l'atmosfera o l'oceano, dovrebbe tendere nel tempo ad
organizzarsi per risuonare sempre meglio con la sorgente di lavoro meccanico,
elettromagnetico o chimico del proprio ambiente," spiega England.

![Auto replicazione][15]{: style="float:right"}
L'auto replicazione (o riproduzione, in termini biologici), il processo che
guida l'evoluzione della vita sulla Terra, è uno di questi meccanismi per i
quali un sistema può dissipare un sempre maggior ammontare d'energia nel tempo.
Come spiega England, "Un ottimo modo di dissipare di più è di fare più copie di
se stessi." In [un documento di settembre][7] nel Journal of Chemical Physics,
riportò l'ammontare minimo teorico di dissipazione che può avvenire durante
l'auto replicazione delle molecole e cellule batteriche del RNA, e mostrò quanto
esso sia molto vicino alla reale quantità che essi dissipano durante la
replicazione. Mostrò anche che lo RNA, l'acido nucleico che molti scienziati
credano serva come precursore alla vita basata sul DNA, è un materiale da
costruzione particolarmente economico. Quando lo RNA nacque, lui spiega, il suo
"subentro darwiniano" non fu forse sorprendente.

La chimica della zuppa primordiale, mutazioni casuali, geografia, eventi
catastrofici e innumerevoli altri fattori hanno contribuito ai piccoli dettagli
della  variegata flora e fauna della Terra. Ma secondo la teoria di England, il
principio sottostante che muove l'intero processo è l'adattamento della materia
guidato dalla dissipazione.

Il principio si applicherebbe anche alla materia inanimata. "È molto attraente
speculare su quale fenomeno naturale possiamo ora infilare sotto questa grande
tenda dell'organizzazione adattiva guidata dalla dissipazione," dice England.
"Molti esempi potrebbero essere proprio qua sotto il nostro naso, ma non averli
mai notati perché non li cercavamo."

Gli scienziati hanno già osservato l'auto replicazione in sistemi non viventi.
In accordo con nuove ricerche condotte da Philip Marcus dell'Università della
California a Berkeley, e [riportate sul Phisical Review Letters][8] in Agosto,
vortici in fluidi turbolenti si replicano spontaneamente traendo energia dal
fluido circostante. E in un [documento apparso online questa settimana][9] in
Proceedings della Accademia Nazionale di Scienze, Michael Brenner, un professore
di matematica applicata e fisica ad Harvard, ed i suoi collaboratori presentano
dei modelli teorici e simulazioni di microstrutture che si auto replicano.
Questi aggregati di microsfere rivestite in modo speciale dissipano energia
legando assieme sfere vicine fino a formare identici aggregati. "Questo si
collega molto a quando Jeremy sta affermando," dice Brenner.

Oltre all'auto replicazione, un altro modo di incrementare la dissipazione di
energia nei sistemi fortemente spinti è una maggior organizzazione strutturale.
Una pianta, ad esempio, riesce molto meglio a catturare ed indirizzare l'energia
solare attraverso se stessa, rispetto ad un non strutturato mucchietto di atomi
di carbonio. Pertanto, England sostiene che sotto certe condizioni, la materia
tende spontaneamente ad auto organizzarsi. Questa tendenza può tener conto
dell'ordine interno delle cose viventi, ma anche di molte strutture inanimate.
"Fiocchi di neve, dune sabbiose e vortici turbolenti hanno tutti in comune
l'essere strutture sorprendentemente modellate che emergono in sistemi di molte
particelle guidati da qualche processo dissipativo," afferma. Condensazione,
vento e resistenza viscosa sono i processi rilevanti di questi casi particolari.

"Mi sta facendo pensare che la distinzione fra materia vivente e non, non sia
netta," dice [Carl Franck][10], un fisico biologo dell'Università di Cornell in
un email. "Sono particolarmente impressionato da questa nozione quando si
considerano sistemi piccoli quanto i circuiti chimici che coinvolgono poche
biomolecole."

![Fiocco di neve][16]{: style="float:left"}
L'idea coraggiosa di England verrà probabilmente sottoposta ad attento esame nei
prossimi anni. Lui sta attualmente eseguendo simulazioni al computer per testare
la sua teoria secondo cui i sistemi particellari adattano le proprie strutture
per migliorare nella dissipazione di energia. Il prossimo passo sarà eseguire
esperimenti su sistemi viventi.
 
Prentiss, che gestisce un laboratorio di biofisica sperimentale ad Harvard,
afferma che la teoria di England può venire testata confrontando cellule con
diverse mutazioni e cercando una correlazione tra la quantità di energia che le
cellule dissipano ed il loro tasso di replicazione. "È necessario essere
prudenti perché ogni mutazione può causare diverse cose," dice. "Ma continuando
a fare molti di questi esperimenti su diversi sistemi e se [la dissipazione ed
il successo di replicazione] sono effettivamente correlati, questo potrebbe
suggerire essere il corretto principio organizzante."

Brenner dice di sperare di poter collegare la teoria di England alla propria
costruzione di microsfere e determinare se la teoria prevede correttamente quali
processi di auto replicazione ed auto assemblamento possono verificarsi - "una
domanda fondamentale nella scienza," afferma.

Avere un generale principio della vita e dell'evoluzione darebbe ai ricercatori
una più ampia prospettiva sull'emergere di strutture e funzioni nelle cose
viventi, affermano molti dei ricercatori. "La selezione naturale non spiega
certe caratteristiche," dice Ard Louis, un biofisico dell'Università di Oxford
in un'email. Queste caratteristiche includono un cambio di espressione genica
ereditabile chiamato metilazione, [incrementi di complessità in mancanza di
selezione naturale][11], ed alcune modifiche molecolari che Louis ha studiato
recentemente.

Se l'approccio di England regge ad ulteriori test, potrebbe liberare i biologi
dal dover cercare una spiegazione darwiniana per ogni adattamento e permettere
loro di pensare più genericamente in termini di organizzazione basata sulla
dissipazione. Potrebbero scoprire, ad esempio, che "la ragione per cui un
organismo mostra la caratteristica X invece che Y potrebbe non essere perché X è
più adatto di Y, ma perché i vincoli fisici rendono più semplice l'evoluzione di
X piuttosto che di Y," afferma Louis.

"Le persone rimangono spesso bloccate nel pensare a problemi individuali," dice
Prentiss. Sia che l'idea di England si riveli esatta, afferma, "il pensare più
ampiamente è ciò che costituisce molti progressi scientifici."

[1]:https://www.quantamagazine.org/20140122-a-new-physics-theory-of-life/
[2]:http://www.englandlab.com/   
[3]:http://www.englandlab.com/uploads/7/8/0/3/7803054/2013jcpsrep.pdf
[4]:http://www.englandlab.com/talks.html
[5]:http://whatislife.stanford.edu/LoCo_files/What-is-Life.pdf
[6]:http://arxiv.org/pdf/cond-mat/9901352v4.pdf
[7]:http://www.englandlab.com/uploads/7/8/0/3/7803054/2013jcpsrep.pdf
[8]:http://prl.aps.org/abstract/PRL/v111/i8/e084501
[9]:http://www.pnas.org/content/early/2014/01/17/1313601111.abstract
[10]:http://www.physics.cornell.edu/professorspeople/professors/?page=website/faculty&action=show/id=14
[11]:https://www.quantamagazine.org/20130716-the-surprising-origins-of-lifes-complexity/
[12]:{filename}/images/una-nuova-teoria-fisica-sulla-vita1.png
[13]:{filename}/images/una-nuova-teoria-fisica-sulla-vita2.png
[14]:{filename}/images/una-nuova-teoria-fisica-sulla-vita3.png
[15]:{filename}/images/una-nuova-teoria-fisica-sulla-vita4.png
[16]:{filename}/images/una-nuova-teoria-fisica-sulla-vita5.png
