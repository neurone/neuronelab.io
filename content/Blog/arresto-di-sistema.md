title: Arresto di sistema
date: 09-09-2015 18:45
tags: fantascienza, cyberpunk, recensioni, libri
summary: Questo romanzo di Charles Stross è decisamente divertente ed originale. Può essere inserito nel filone post-cyberpunk per il tipo di ambientazione e la storia, ma al tempo stesso l'accuratezza con cui vengono descritte le tecnologie e l'uso dell'informatica, lo porterebbe nel filone della hard science fiction. Tutto questo gioca al servizio di una trama da giallo spionistico.

Questo romanzo di [Charles Stross][1] è decisamente divertente ed originale. Può essere inserito nel filone [post-cyberpunk][2] per il tipo di ambientazione e la storia, ma al tempo stesso l'accuratezza con cui vengono descritte le tecnologie e l'uso dell'informatica, lo porterebbe nel filone della [hard science fiction][3]. Tutto questo gioca al servizio di una trama da giallo spionistico.

![Arresto di sistema]({filename}/images/arresto-di-sistema.jpg "Copertina: Arresto di sistema"){: style="float:right"}

Per la narrazione è stata fatta l'originale scelta della seconda persona, che dà
al lettore la sensazione di partecipare ad un gioco di ruolo con il narratore
nei panni del [game master][4].

La storia è ambientata di pochissimi anni nel futuro, la tecnologia è circa
quella attuale, solo leggermente ampliata, migliorata e più diffusa. Un
intreccio che mescola le [realtà virtuali][5] di [giochi di ruolo massivi
online][6], ai [GDR dal vivo][7] ed ai [giochi di realtà alternativa][8]
potenziati grazie alla cosiddetta [realtà aumentata][9], alla finanza
speculativa ed alle agenzie di spionaggio e controspionaggio. Le suggestioni e
le paranoie alla [Philip K. Dick][10] sulle realtà virtuali ed alternative,
narrate con precisione scientifica - tanto da necessitare di un glossario dei
termini a fine libro, per permettere ai profani di capire di cosa si stia
parlando - ed un tono spesso divertente ed ironico.

Il libro è pubblicato in Italia da una casa editrice emergente, la [Zona42][11].

> I giocatori saccheggiano le tombe di divinità defunte, rapinano le segrete del
> governatore della Giamaica, colonizzano la galassia di Andromeda e non si può
> tassarli o far perdere valore al denaro, perché non sarebbe divertente.  E se
> il gioco smette di essere divertente… bè, che motivo ci sarebbe per giocare?
> È questa la differenza tra l’economia del gioco e quella della banca
> nazionale: la banca non ha bisogno di chiedersi se ci stiamo divertendo.
> 
> Edimburgo, anno 2017, il sergente Sue Smith della polizia scozzese è chiamata
> a occuparsi di un caso decisamente inconsueto. Una rapina in banca ha avuto
> luogo alla Hayek Associates - una start-up digitale appena sbarcata alla borsa
> di Londra. Ma il crimine in questione sembra andare ben oltre le competenze di
> Smith: i principali sospettati sono una banda di orchi con un drago al seguito
> per il supporto di fuoco.  La banca si trova infatti nel territorio virtuale
> di Avalon Four e una rapina del genere doveva risultare impossibile. Quando la
> voce si sparge, la Hayek Associates e tutte le economie virtuali rischiano una
> crisi senza precedenti.  Più Sue Smith indaga meno le cose sembrano avere
> senso. Una cosa è certa: la rapina è solo l’inizio, altri giocatori, ben più
> potenti, stanno osservando ogni sua mossa, pronti a trasformare la crisi
> finanziaria virtuale in un problema nazionale di proporzioni globali.
> 
> In Arresto di sistema si passa con nonchalance dalle rapine ai danni di banche
> virtuali alle meraviglie della realtà aumentata, dai giochi on-line a quelli
> pervasivi per le strade scozzesi, dai conflitti di competenze tra forze di
> polizia alla dura vita di freelance e dipendenti alle prese con superiori più
> o meno competenti, il tutto insaporito da una spruzzata di spionaggio
> internazionale e qualche omicidio assortito. Charles Stross non si accontenta
> di sfornare trovate e speculazioni a raffica, lo fa pure in maniera divertente
> (e divertita), con più di una strizzatina d’occhio a tutti i geek qua fuori.

[1]:https://it.wikipedia.org/wiki/Charles_Stross
[2]:https://en.wikipedia.org/wiki/Cyberpunk_derivatives#Postcyberpunk
[3]:https://it.wikipedia.org/wiki/Fantascienza_hard
[4]:https://it.wikipedia.org/wiki/Game_master
[5]:https://it.wikipedia.org/wiki/Realt%C3%A0_virtuale
[6]:https://it.wikipedia.org/wiki/MMORPG
[7]:https://it.wikipedia.org/wiki/Gioco_di_ruolo_dal_vivo
[8]:https://it.wikipedia.org/wiki/Alternate_reality_game
[9]:https://it.wikipedia.org/wiki/Realt%C3%A0_aumentata
[10]:https://it.wikipedia.org/wiki/Philip_K._Dick
[11]:http://www.zona42.it
