title: Dossier Cyberpunk
date: 2015-10-20 20:06
tags: fantascienza, cyberpunk, hacking, libri, politica, società, anarchia, psichedelia, filosofia, rete, tecnologia, film, musica, arte
summary: Un testo ottimo e molto completo sulla cultura cyberpunk, era disponibile da parecchio tempo sul sito della storica rivista *Decoder*. Ora Decoder on-line non esiste più, ma è accessibile mediante la wayback-machine, il sito che si occupa di archiviare tutti i siti esistenti a futura memoria. Spazia dalla letteratura, alla politica, la controcultura hacker in ambito internazionale e nazionale con particolare riferimento all’Italian Crackdown, per finire con la psichedelia e le arti musicali e visive.

Un testo ottimo e molto completo sulla cultura cyberpunk, era disponibile da parecchio tempo sul sito della storica rivista *Decoder*. Ora Decoder on-line non esiste più, ma è accessibile mediante la [wayback-machine][1], il sito che si occupa di archiviare tutti i siti esistenti a futura memoria. Spazia dalla letteratura, alla politica, la controcultura hacker in ambito internazionale e nazionale con particolare riferimento all’[Italian Crackdown][2], per finire con la psichedelia e le arti musicali e visive.

È molto interessante, sia come testimonianza del periodo nel quale è stato
scritto, gli anni ’90, caratterizzati dalla rinascita di molte controculture e
movimenti che promuovevano un’evoluzione della società ed una maggiore
indipendenza mediante l’autogestione. Dopo il decennio precedente,
caratterizzato dalla possibilità di accesso a tecnologie potenti ed economiche
che davano ai singoli, capacità di elaborazione e comunicazione altrimenti
disponibili solo alle grosse aziende ed ai governi, vi era un grande entusiasmo
per le molteplici possibilità date dall’informatica. Tutto questo entusiasmo, è
percepibile in questo dossier.

È interessante anche l’attualità di moltissime situazioni descritte. Le
critiche alle leggi restrittive in campo informatico e di comunicazione, sono
analoghe a quelle che ancora oggi sentiamo e leggiamo ovunque, con la
differenza che, attualmente, la consapevolezza del potere che l’informatica e
la rete può dare alle persone, è ben chiara a governi e multinazionali, i quali
cercano di limitarlo in modo molto più efficace e scaltro. Se conoscete tutte
le problematiche che nascono riguardo a Google, Facebook, Apple, il DRM, i
brevetti software, sapete a cosa mi sto riferendo.

Riporto l’intero dossier, sotto la forma di uno spaventoso muro di testo, in
modo da conservarne una copia a scopo storico, ma vi invito a proseguire la
lettura anche sul sito originario, per poterne apprezzarne l’aspetto estetico,
anch'esso rappresentativo del periodo storico.

Cyberpunk Culture
=================

Che cos’è il cyberpunk
---------------------- 
Questa è una domanda a cui è difficile dare una risposta univoca, giacché il
termine oramai denota sia un aspetto [letterario][3] che un ambito più
propriamente [politico][4].  All’inizio questa definizione è stata coniata per
indicare un variegato movimento di fantascienza, essenzialmente ma non solo
americano.  Composto da persone per lo più giovani di età, la media di ognuno
di essi è sui trent’anni, esso ha attraversato in maniera partecipe gli anni
Ottanta, vivendone completamente le intime contraddizioni. Sono scrittori
quindi, come ci segnala [Sterling][5] nella sua prefazione a Mirrorshades, che
hanno vissuto, dentro e persino sotto la propria pelle, un rapporto intimo con
la tecnologia, diversamente da quanto successe negli anni Sessanta, tutta
lavatrici e lavastoviglie. I micidiali anni Ottanta fatti di walkman, stereo
portatili, videoregistratori, batterie elettroniche, videocamere portatili,
televisioni ad alta definizione, telex, fax, laser-disc, antenne paraboliche
per captare i segnali dei satelliti, cavi a fibre ottiche, personal computer,
chirurgia plastica, la rete semiotica onnicomprensiva, il tendenziale
superamento del sistema-mondo in “un globale sistema nervoso che pensa per se
stesso”. Tutto l’intero sistema delle merci fonda in maniera sotterranea, ma
decisiva, la costituzione di senso nella produzione letteraria del cyberpunk.
Per la prima volta nella storia della letteratura tale rapporto con la macchina
non viene visto quasi fosse una dimensione negativa, ineluttabile, da scansare
non appena possibile. Orwell è dietro l’angolo, Frankeinstein un lontano
ricordo dell’epoca del moderno. Il cyber presuppone un nuovo rapporto organico
con la tecnologia. Essa permette, difatti, l’estensione delle capacità
dell’uomo e finalmente il superamento dei suoi limiti. Nessuna ferita
altrimenti mortale spaventa più l’uomo del futuro prossimo, la neurochirurgia
saprà implantare nuove membra artificiali in corpi, oggi, al più buoni per il
solo cimitero del rottame. Viene risolto con un colpo di spugna il problema
della morte, un tema questo che, per altra via, anche lo stesso [Leary][6] (uno
dei capofila della tendenza [cyber-psichedelica][7]) considera risolvibile
tramite automanipolazioni psichiche del proprio DNA.  Si potrebbe suggerire a
questo punto che nulla di nuovo in effetti è apparso sotto il sole. Il tema
dell’immortalità è un sogno da sempre ricorrente nella letteratura, soprattutto
in quella dove più forte è il tributo all’ispirazione religiosa. Allora in cosa
consiste la novità?

Alla ricerca del cyberpunk
--------------------------
Ancora una volta lo scritto, in precedenza citato, di Sterling può permetterci
di intuire la strada più fertile per un approccio esaustivo al problema. Egli
difatti richiama con dovizia di particolari il debito che tutti questi
scrittori nutrono da una parte, come è logico che sia, verso il tradizionale
filone della fantascienza, ma dall’altra anche verso tutti quei movimenti
giovanili di resistenza che hanno contrassegnato la storia, dagli anni Sessanta
in avanti. Movimenti questi che hanno sempre avuto un rapporto intenso con le
tecnologie, con gli strumenti elettrici, con la produzione di musica e degli
effetti speciali. Analogamente all’hard rock, ad esempio, lo stile letterario
del cyberpunk vuole coscientemente essere un muro del suono, un tutto pieno,
dettagliato, analitico, dove venga a mancare il tempo per tirare il fiato e
quindi adagiarsi nella riflessione. Questo stile ha un che di assolutamente
nervoso, alcune volte difficile da seguire nelle sue circonlocuzioni, spesse
volte derivate dallo slang di strada. Esso difatti pone al centro delle proprie
trame dei personaggi che sono completamente “altro” rispetto alla tradizione
letteraria. Come ci segnala acutamente Saucin nel suo saggio contenuto in
Cyberpunk Antologia, essi sono puttane, biscazzieri, punk, trafficanti, ladri,
hackers, pirati informatici, balordi di strada, con poca o nessuna voglia di
lavorare, immersi solamente in ciò che produce gioia. E’ un filone letterario
che recupera organicamente alcune delle tensioni sociali esistenti. Giustamente
Downham, postsituazionista londinese, ha definito il genere “una scrittura
tecno-urbana, fantascienza sociale, postsituazionista, tecno-surrealista”. Ma
cyberpunk è anche “strategia operazionale di resistenza, estetica da dura
garage-band, cultura pop(olare)”. Quindi descrive un ambito sociale che sempre
è stato tagliato fuori dalla scrittura ufficiale, ignorato, vilipeso o, molto
peggio, dichiarato come assolutamente non esistente.  Viene assunto il mondo
dei reietti da Dio come protagonista ufficiale di uno scenario assolutamente
nuovo, di una scrittura assolutamente nuova. Uno stile quindi superrealista.
Viene quindi inventato un diverso immaginario sociale, che d’altronde
sotterraneamente è già esistente da tempo, che unisce insieme fascinazioni
tecno-pop e pratiche esistenziali di resistenza e sopravvivenza quotidiana.
Per la prima volta dai tempi dell’esperienza hippie viene quindi forgiato un
immaginario collettivo vincente, che sa collocare in maniera adeguata e
accattivante alcune delle aspirazioni che percorrono i senza parola della
società post-industriale. Vi è quindi da parte nostra un’adesione d’istinto a
ciò che il cyberpunk finora, come scrittura e socialmente, è stato.

Possiamo ipertextare la storia del cyberpunk in 4 livelli:

- Letteratura Cyberpunk
- Cyberpunk Politico
- Cyberpunk Psichedelico
- Immaginari Cyber

Letteratura Cyberpunk
=====================

L'argomento è assai vasto. Ecco dunque a seguire una storia delle **Origini**
del cyber e poi una storia della **Prima Generazione** e della **Seconda
Generazione**.

Sterling afferma che la letteratura cyber ha un debito nei confronti
dell'esperienza del punk, e segnala tre piani differenti della questione. In
primo luogo un'istanza di depurazione del mainstream (la fantascienza classica)
rispetto agli orpelli costruiti sopra di esso. "Il cyberpunk è un liberare la
fantascienza stessa dall'influenza principale, così come il punk svestì il rock
and roll dalla sinfonica eleganza del progressive rock degli anni Settanta.
Come la musica punk, il cyberpunk è in un certo senso un ritorno alle radici."
Un secondo piano di analisi sul rapporto tra cyber e punk viene rintracciato
nell'enucleazione della questione tra teoria e prassi: "Nella cultura pop, per
prima viene la pratica e la teoria ne segue zoppicando le tracce". Chiunque
abbia frequentato la scena punk sa bene che una delle tensioni principali del
movimento si condensò nel praticare e quindi nel teorizzare il diritto comunque
a esprimersi suonando, indipendentemente dalle capacità teorico musicali
possedute. Il caso dei Sex Pistols in questo senso è esemplare.  Sono la
pratica del vivere collettivo e dell'autogestire la propria vita e i luoghi di
socializzazione a definire gli impegni esterni del movimento, non viceversa.
D'altronde lo stesso Sterling si riferisce ancora a questo rapporto tra prassi
e teoria, quando indica nella cultura di strada, e nella cultura hip-hop in
particolare, il luogo di genesi dell'unione operativa tra tecnologia e pratiche
controculturali di resistenza quotidiana. Il terzo piano del debito cyberpunk
nei confronti dell'ambito underground viene infine rintracciato proprio
allorquando egli evidenzia che "il cyberpunk proviene da quel regno dove il
pirata del computer e il rocker si sovrappongono". "Il cyberpunk ne è la
letteraria incarnazione", difatti nel frattempo si è costituita "una Non Santa
Alleanza del mondo tecnologico e del mondo del dissenso organizzato".

Origini
-------

*Gli ispiratori*

L'ispirazione nei confronti del mondo underground non può però essere limitato
a quanto riferito precedentemente. Tra gli ispiratori ritroviamo due grandi
nomi, due grandi vecchi: William Burroughs e J.G. Ballard. In realtà Ballard
non può essere visto come un frequentatore delle situazioni "contro". Bisogna
comunque considerare che da sempre egli è stato adottato dal movimento
antagonista e controculturale internazionale, a causa del suo stile
assolutamente analitico, descrittivo, perturbante, quasi psicoscientifico,
spregiudicato. La sua scrittura è un bisturi, si è detto in parecchie
occasioni. Inoltre un'altra sua intuizione, conseguente del resto allo stile
fenomenologico della sua scrittura clinica, ha fatto sì che egli diventasse un
totem assoluto per il cyberpunk: lo spazio interno. Per spazio interno si
intende coerentemente quell'implosione psichica senza ritorno che i
protagonisti dei suoi romanzi vivono, in concorrenza di avvenimenti esterni
spaesanti. Come in Deserto d'acqua, uno dei suoi romanzi più forti ed
evocativi, in cui Kerens "Ricordò le iguane strillanti sui gradini del museo.
Proprio come la distinzione tra il significato latente e quello manifesto del
sogno aveva perso ogni valore, così non aveva senso qualsiasi distinzione fra
il reale e il super-reale nel mondo esterno. Fantasmi scivolavano
impercettibilmente dall'incubo alla realtà e viceversa; il panorama terrestre e
quello psichico erano ora indistinguibili, come lo erano stati a Hiroshima e ad
Auschwitz, sul Golgota e a Gomorra". Così Bodkin gli rispose: "I residui del
tuo controllo cosciente sono gli unici speroni che tengono in piedi la diga".
"I meccanismi di liberazione innati, impressi nel tuo citoplasma milioni di
anni fa, sono stati risvegliati, il sole in espansione e la temperatura in
aumento ti stanno spingendo indietro, lungo i vari livelli spinali, nei mari
sepolti, sommersi sotto gli strati infimi del tuo inconscio, nella zona
interamente nuova della psiche neuronica. Si tratta di trasposizione lombare,
di memoria biopsichica totale. Noi ricordiamo veramente queste paludi e queste
lagune." Abbiamo riportato questa lunga citazione da Ballard proprio perché
estremamente significativa rispetto al concetto in esame. Del resto non sono
forse spazio/tempo interni l'esagerato implodere del disagio psichico in
angoscia o, per altri versi, il fare all'amore in maniera ubriacante? In realtà
il concetto stesso si presta a numerose valutazioni e contaminazioni. Nel
cyberpunk in particolare ciò ha attivato l'ispirazione per l'utilizzo del
termine di spazio virtuale. In realtà il rapporto di filiazione tra i due
termini è sufficientemente diretto, così come del resto non sono
necessariamente contraddittori per certi versi quelli di spazio interno ed
esterno. Alcuni hanno creduto difatti di poter distinguere due diversi centri
di irradiazione culturale alla base della fantascienza più recente. Da una
parte Ballard stesso col suo teorema letterario di spazio interno, da cui
deriverebbe la tendenza Umanista. Dalla parte dello spazio esterno [Dick][8] e,
per ragioni immaginativo-sociali, Blade Runner, da cui ne conseguirebbe in
linea diretta la produzione più propriamente cyberpunk. In realtà in più
occasioni sia Sterling che lo stesso Gibson sono intervenuti sulla questione,
suggerendo che questa fosse tutta una storia inventata dai critici, visto che i
due concetti di spazio interno/esterno debbono essere considerati
sostanzialmente come non contraddittori tra loro.

Per ritornare ancora a Ballard, la stessa rivista americana "Research", che in
più occasioni ha tratteggiato quelli che sono i miti, le letture, i film più
seguiti dalle nuove generazioni alternative, ha dedicato a questo grande
scrittore addirittura un intero numero monografico. Stessa sorte peraltro è
stata riservata, dal collettivo redazionale di Research, a Burroughs, il quale
nonostante tutto è più interno alla storia del movimento, giacché ancor oggi vi
partecipa occasionalmente, in quelle che considera le situazioni più
stimolanti. Di Burroughs in particolare, Sterling richiama del resto i suoi
esperimenti degli anni Sessanta sulla tecnica del cut-up. Questa pratica
suggerisce che, tagliando e rimontando casualmente qualsiasi tipo di
informazione, alla fine si otterrà di comprendere il vero senso del messaggio,
indipendentemente dalle manipolazioni nel frattempo intervenute (cfr. La
rivoluzione elettronica).

Sia Ballard che Burroughs tendono a esprimere nella loro scrittura le
contraddizioni che si danno nel reale, fino ad assumerle come indici generali
intorno a cui far ruotare le dinamiche narrative. Come suggerisce Mei ne La
giungla del futuro lo stesso Ballard "tende a rendere questa profonda
dissociazione dell'esperienza contemporanea con bizzarri collages verbali,
ripresi da riviste di moda e di armi, inserti tecnologici, avvisi pubblicitari,
listini di borsa". Similitudini queste che richiamano la scrittura mediale del
misterioso Thomas Pynchon, anche lui osannato da Sterling. Come riporta Riotta
in un suo servizio, Pynchon nel suo ultimo libro, dopo diciassette anni di
silenzio, unisce insieme argomenti apparentemente poco letterari quali "il
cioccolato solubile Nestlé, i sarti Clavin Klein, Cerruti, Azzedine Alaia, Yves
St. Laurent, la grinta di Clint Eastwood, continui riferimenti a titoli di
film, indicati persino con la data di produzione, Guerre Stellari III, Jason il
mostro di Venerdì 13, Titti e il Gatto Silvestro, Nixon, Reagan, i terroristi,
Mario Savio (il Capanna americano), la Diet Pepsi Cola, il dottor Spock di Star
Trek, la birra messicana Dos Equis e il campionato di basket della NBA". Per
certi versi un analogo percorso segue Burroughs, il quale parte "dalla
dissociazione psichedelica per presentare sotto forma di dissolvenze incongrue
e casuali, esperienze al limite della disgregazione psichica e del
delirio"(Mei). E' questo il caso difatti de il Pasto nudo e de La morbida
macchina.

**Philip Dick: In lotta con l'universo impazzito**

*di Vittorio Curtoni*

Nato a Chicago nel 1928 e morto nel 1982, Philip Kendred Dick è stato (e
rimane) un autore fondamentale per l'evoluzione della fantascienza. Il giudizio
che di lui ha dato una delle più prestigiose scrittrici americane, Ursula Le
Guin[1], riassume con sintetica efficacia il valore rivoluzionario dell'opera
di Dick: "Nessuno si accorge che abbiamo qui in America un nostro Borges, e lo
abbiamo da trent'anni." Dopo avere lavorato come commesso nel reparto di musica
classica di un negozio di dischi ed essere stato disc-jockey (sempre per la
musica classica) di una stazione radiofonica, Dick esordisce come scrittore di
racconti nel 1952. Il suo primo romanzo, Il disco di fiamma, esce nel 1955, e
pur essendo chiaramente influenzato dagli stereotipi della fantascienza
avventurosa dell'epoca, svela già un interesse del tutto particolare per i
meccanismi della casualità, che in un ipotetico futuro diventano la chiave in
base alla quale si decidono le sorti politiche del nostro pianeta. Molto più
personale e affascinante è un romanzo del 1957, L'occhio nel cielo, dove si
comincia a delinare il tema della frantumazione del reale: ognuno dei
personaggi che, per un incidente, vengono investiti da un fascio protonico da
sei miliardi di volt, crea un proprio mondo soggettivo, modellato sulle sue
nevrosi. Ovviamente, si tratta di universi da incubo, in cui non esistono più
freni inibitori e nessuna barriera tra il raziocinio cosciente e le pulsioni
dell'inconscio. Tra gli anni Cinquanta e i Sessanta, ormai diventato autore
professionista, Dick pubblica a getto continuo romanzi e racconti, rivelandosi
uno dei talenti più prolifici che la science fiction abbia mai avuto[2]. A
differenza di tanti suoi colleghi, però, Dick riesce sempre a mantenersi su
livelli di eccellente qualità, sicché anche le sue opere minori sono al di
sopra della media, e le opere maggiori costituiscono un'imponente serie di
capolavori. La consacrazione ufficiale del suo talento è il premio Hugo che
viene assegnato a uno splendido romanzo del 1962, La svastica sul sole,
classica ucronia ambientata in un universo parallelo dove le forze dell'Asse
hanno vinto la seconda guerra mondiale e l'America contemporanea è una nazione
sconfitta a tutti i livelli. Nel tessuto narrativo si insinua una seconda
realtà alternativa, sotto le spoglie di un romanzo di fantascienza che parla di
un mondo in cui l'Asse ha perso la guerra; però l'assetto socio-politico che ne
è risultato è piuttosto diverso da quello del nostro universo... Il 1964 è un
anno particolarmente significativo per Dick. I simulacri e Le tre stimmate di
Palmer Eldritch, due pietre miliari della sua produzione, introducono temi e
spunti che resteranno tipici della sua arte fino agli ultimissimi romanzi. Nel
primo libro compaiono le creature artificiali cui allude il titolo, i
simulacri, androidi perfezionatissimi che il potere (la classe dei G, l'élite
dominante) usa per mantenere nella più totale delle soggezioni il resto
dell'umanità (la classe dei B). Riprendendo una profetica intuizione di George
Orwell[3], Dick ipotizza che i G, per meglio esercitare la loro oppressione,
riscrivano in continuazione i testi di storia che i B sono obbligati a
studiare; e, spingendosi ancora più in là, prevede addirittura l'uso del
viaggio nel tempo per apportare modifiche concrete al passato storico. Come già
in La svastica sul sole, l'analisi delle strutture politiche è spietata,
lucidamente amara, e non lascia speranze: il potere, cristallizzato in vere e
proprie caste, tende a perpetuarsi all'infinito, mentendo su tutto, sulla vita
come sulla morte, come sulla realtà stessa. Le tre stimmate di Palmer Eldritch
può, a buon diritto, essere considerato un libro epocale, nel senso che
rappresenta e interpreta le tensioni ideali di un'intera fascia di cultura e di
un preciso periodo storico: la cultura della droga, dell'LSD, che ha avuto
molti profeti, molti teorici, e molti devoti praticanti, all'interno dei
movimenti giovanili di contestazione. La lotta fra le due corporazioni che si
contendono il predominio del mercato interplanetario della droga (usata come
panacea al "male di vivere" dei coloni sbattuti su mondi ostili) porta a un
crescendo di allucinata confusione ontologica; il qui e l'ora, la concretezza
del reale, non esistono più, sostituiti da un caos di percezioni alterate e di
stati di coscienza "acidi". Il Dio che si delinea in queste pagine possiede una
natura fondamentalmente maligna, e la figura di Palmer Eldritch (che si
moltiplica all'infinito, apparendo ovunque, sfiorando tutto con la sua
presenza) ha spiccati sottofondi messianici. La ricerca del trascendente, il
tentativo di definire l'essere superiore che regge le fila del destino umano, è
un'altra costante della narrativa di Dick. Col tempo, tenderà a portarlo a un
misticismo radicale (è il caso di Divina invasione, un romanzo del 1981, e di
Valis, apparso nello stesso anno); ma la sincerità del suo anelito è fuori
discussione, e in Le tre stimmate... il tema religioso aggiunge echi molto
suggestivi a una vicenda già ricca di affascinanti risvolti onirici. A
proposito della droga, va precisato che Dick stesso ne ha fatto uso, per quanto
in epoche successive si sia affrettato a smentirlo, e per quanto alcune delle
sue opere più tarde (Episodio temporale, 1974; Scrutare nel buio, 1976) siano
drammatici, convincenti atti d'accusa contro gli allucinogeni. Come tanti altri
artisti della cultura popolare degli anni Sessanta (valga per tutti l'esempio
dei Beatles), Dick ha cercato nell'LSD motivi di ispirazione, visioni che
fossero slegate dalla percezione normale, piatta, del mondo; e se è lecito
giudicare dai risultati senza perdersi in troppo facili moralismi, va detto che
i suoi sforzi sono stati tutt'altro che inutili. Le geniali allucinazioni di
cui si nutrono romanzi come Il cacciatore di androidi (1968) o Ubik, mio
signore (1969) non sarebbero forse mai nate da una mente che non avesse provato
l'ebbrezza del trip acido. Queste due opere portano alle conseguenze più
estreme il processo di sfaldamento del reale: gli androidi che Rick Deckard
deve individuare ed eliminare sono, per certi versi, più umani degli uomini che
li hanno creati, tanto che diventa praticamente impossibile riuscire a
distinguere fra l'essere artificiale e l'essere vero, biologico[4]; e Ubik,
l'enigmatica presenza che si materializza sotto infinite spoglie, è l'ente
superiore ma allo stesso tempo è anche un banalissimo spray per uso domestico,
e in ogni caso, come annuncia la singolare rivelazione finale, buona parte
della storia si è svolta in un mondo che non esiste, in un sogno creato da
qualcuno che dovrebbe essere morto... Negli ultimi anni di vita, divenuto un
autore di culto anche al di fuori della cerchia dei lettori di fantascienza (in
particolare in Europa: Francia e Italia sono fra i paesi che più hanno amato la
sua narrativa), Dick si è lanciato in veementi battaglie contro la droga.
Sempre più simile a tanti dei personaggi dei suoi romanzi, ha sostenuto
dapprima di essere perseguitato dalla CIA per le sue posizioni politiche
radicali, e più tardi di essere stato invasato da Dio stesso. Dio sarebbe
entrato nella sua mente, fornendogli nuove chiavi interpretative per
l'esistenza e provvedendo, fra le altre cose, a rimettere ordine nel caos della
sua situazione finanziaria. Certo è difficile giudicare le dichiarazioni di cui
Dick è stato tutt'altro che avaro in interviste e articoli e che sembrerebbero
in netta opposizione con la perenne lucidità della sua opera letteraria.
Personalmente, pur avendo spesso sospettato che questa repentina metamorfosi
fosse solo una beffa, un ironico scherzo giocato per puro divertimento da un
grande tessitore di trame schizofreniche, resto perplesso di fronte al tono
predicatorio, aspramente biblico, di romanzi come Divina invasione e Valis,
lontani miglia e miglia dalla fredda capacità di analisi dei lavori precedenti.
Ma se, come Dick ha sempre sostenuto, la realtà ha un numero infinito di facce
che si sovrappongono in continuazione, creando la semplice apparenza di un
ordine, di una logica, allora può veramente darsi che lo scrittore abbia
incontrato a tu per tu una delle facce più segrete e ne sia rimasto cambiato
per il poco tempo che gli restava da vivere... Per amare bisogna anche odiare,
o saper odiare: è questo uno dei messaggi che si ripetono con martellante
intensità nel corpus narrativo di Dick (messaggio, fra l'altro, puntualmente
confermato dai ritmi sincopati della sua vita sentimentale). Nelle relazioni
fra uomo e donna che l'autore americano ci racconta, il nucleo amore-odio è la
chiave essenziale. Porta a capovolgimenti di ruolo, al desiderio di infliggersi
dolore a vicenda, a lacerazioni insanabili; sicché, alla fine delle sue storie,
è un dato tipico che i protagonisti si ritrovino al punto di partenza, con le
stesse incertezze e un nuovo ammasso di problemi, spesso peggiori di quelli
iniziali, da risolvere. Perché il punto è questo: nessuno riesce mai ad avere
un rapporto di completo amore con se stesso, e quindi non può pretendere di
averlo col mondo esterno. Antichi sensi di colpa, ossessioni che ci trasciniamo
dietro da anni, mineranno sempre la nostra pace interiore, e la nostra visione
del mondo. Essere schizofrenici (o ebefrenici, o quello che meglio vi pare:
scegliete la vostra malattia mentale preferita![5]) diventa una sorta di
necessità, per poter operare un distacco fra l'io e l' altro dall'io, fra
l'emotività e la logica, fra i problemi che forse ammettono una soluzione e le
situazioni che non hanno via d'uscita. Tutti, o quasi, i personaggi di Dick
sono schizofrenici, nel senso che le loro percezioni tendono a scindersi su
livelli distinti, portando a un incessante lavoro di analisi che ha poco di
razionale e molto di intuitivo. Si tratta di un meccanismo di difesa nei
confronti di un universo folle, all'interno del quale la schizofrenia è un dato
di base ineliminabile; detto in parole povere, siamo alla tattica del
combattere il nemico sul suo stesso terreno, nella speranza non di vincere
(illusione assurda di fronte allo stato delle cose che ci circonda) ma, più
semplicemente, di riuscire in qualche modo a sopravvivere. Schizofrenica e
alienante è la tecnologia: strumento di repressione nelle mani del potere, si
trasforma in tortura quotidiana, spicciola e sfibrante, nelle lotte con le
macchine che si rifiutano di funzionare come vorremmo noi, negli impianti audio
che cominciano a trasmettere una musica che non può esistere, nelle porte
(elettroniche, intelligenti) che non accettano di aprirsi se prima non abbiamo
inserito una monetina nell'apposita fessura. Schizofrenica è la realtà, pronta
a sfaldarsi sotto le nostre mani da un momento all'altro, a presentarci il
quadro apparentemente rassicurante di cose e persone che sono tutt'altro da ciò
che crediamo, a illuderci con ricordi di un'esistenza che non ci è mai
appartenuta. Schizofrenico è il concetto stesso di vita, quando ci rendiamo
conto che il confine tra biologico e artificiale è tanto labile da oscillare
avanti e indietro come un pendolo, magari per portarci alla raccapricciante
scoperta di essere a nostra volta, senza averlo mai saputo, creature sintetiche
(il che accade in molti, memorabili racconti, come il celeberrimo Impostore del
1953 e Le formiche elettriche del 1969). Schizofrenica è la divinità, l'essere
superiore, il Dio che ha molti nomi e molte nature non fuse in maniera armonica
ma contrapposte l'una all'altra in un groviglio di tensioni ambivalenti: si
passa dalla bontà all'indifferenza alla cattiveria alla malvagità
quintessenziale senza soluzione di continuità, ed è come osservare le molte
facce di una pietra preziosa su cui la luce cade e viene rifratta in modo
sempre diverso. Come Immanuel Kant, Dick giunge alla conclusione che il noumeno
sia inconoscibile all'attività teoretica dell'uomo; ma, a differenza di Kant,
non ammette una funzione redentrice della morale, e la possibilità di una
effettiva conoscenza è negata a priori. L'universo fenomenologico in cui si
agitano le (più o meno) spasmodiche crisi della nostra coscienza, del nostro
vissuto, è un insieme di dati non ordinabili in sequenza coerente. Per
tracciare un diagramma attendibile del mondo che abbiamo attorno e che abbiamo
dentro ci mancano sia le coordinate, sia le ascisse; tutto viene inghiottito
dallo spaventoso baratro del nulla esistenziale, e dopo che si è precipitati al
fondo dell'abisso, non resta che rassegnarsi alla lunga risalita. Destinata,
peraltro, a fallire. Perché non ci sono più veri appigli: la realtà si
sfilaccia, i rapporti interpersonali vacillano, la stessa autocoscienza è
soggetta a drastiche, repentine revisioni. E persino gli strumenti più canonici
di imposessamento della realtà esterna, primo fra tutti il linguaggio, si
rivelano inadeguati al compito. I vistosi, ripetuti slittamenti nel rapporto
significante\significato, con la perenne ambiguità delle parole e del senso che
esse dovrebbero rappresentare, mutano il processo dialettico in un circolo
vizioso che non fa altro che mordersi la coda: crediamo di andare avanti, di
muoverci, di agire, e invece siamo fermi allo stesso identico punto da cui
eravamo partiti. In un mondo che è solo apparenza, illusione, inganno. Fantasma
di se stesso e delle categorie del nostro conoscere. La maggiore grandezza di
Philip Dick sta, a mio giudizio, nella sua capacità di essere a un tempo
scrittore e filosofo, ideologo e intrattenitore. Sin dagli inizi della sua
carriera, senza mai rinnegare le origini "popolari" del genere che ha scelto
come mezzo di espressione, Dick ha accettato le convenzioni della fantascienza,
i luoghi comuni, i topoi canonizzati (e talora sclerotizzati) dal passato
storico della science-fiction. Ha saputo usare gli stereotipi più classici, dal
viaggio nel tempo agli universi paralleli, dai poteri extrasensoriali alla
colonizzazione di nuovi pianeti, dall'estrapolazione sociologica all'avventura
spaziale, piegandoli alle necessità del complesso discorso che andava tessendo.
I suoi romanzi e i suoi racconti sono colmi di sorprese, di trovate, di azione,
e anche di molto altro. Li si può leggere a svariati livelli; ci si può
limitare al piacere epidermico di trame che spesso brillano per vivacità
d'invenzione fantastica, oppure si può scendere più in profondità ed esplorare
gli abissi del nulla che la sua narrativa spalanca sotto i nostri piedi. Il
divertimento (intelligente) è comunque assicurato.

*NOTE*

1. Ursula Kroeber Le Guin, autrice americana di fantascienza e fantasy
   (Berkeley, California, 1929).
2. Il corpus totale delle sue opere, in trent'anni di carriera, assomma a più
   di trenta romanzi e oltre un centinaio di racconti. E' opportuno ricordare
   che il mercato americano (e internazionale in genere) della fantascienza ha
   iniziato a essere realmente redditizio solo dalla metà degli anni Settanta
   in poi, il che ha costretto gli autori professionisti a una produttività
   forzata. Il caso di Dick, infatti, è tutt'altro che isolato.
3. George Orwell (pseudonimo di Eric Blair), romanziere e saggista inglese
   (Motihari, Bengala, 1903; Londra, 1950). La "riscrittura della storia" è uno
   dei temi portanti del suo romanzo più celebre, 1984 (1949).
4. Da Ubik, mio signore il regista Ridley Scott (Tyne and Wear, Gran Bretagna,
   1939) ha tratto quello che si può senz'altro considerare il più splendido
   film di fantascienza degli anni Ottanta, Blade Runner (1982), interpretato
   da Harrison Ford. Scott è riuscito a tradurre le allucinazioni e le angosce
   di Dick in un compatto universo visivo che è la straordinaria trasposizione
   in immagini di una "zona del disastro" psichico.
   (NB. La nota contiene un errore: Blade Runner è tratto da "Ma gli androidi
   sognano pecore elettriche?", noto anche come "Cacciatore di androidi")
5. In un romanzo del 1964, Follia per sette clan (Clans of the Alphane Moon),
   Dick offre la rappresentazione di una società strutturata in clan, ognuno
   dei quali è portatore di una specifica malattia mentale.

*Questo articolo è apparso sul n. 52 della rivista "Abstracta", Stile Regina
Editrice, Roma, ottobre 1990*

Prima Generazione
-----------------

*di Sandrone*  
tratto da [DECODER #6][9]

> In questo articolo Sandrone, uno dei maggiori esperti di letteratura di
> fantascienza della Milano Alternativa, descrive e colloca il fenomeno
> letterario in quell'area che, di diritto, deve occupare: quella
> artisticamente rivoluzionaria. Infatti, al di là delle mode e dei ricicli,
> spesso le correnti di S.F. radicali sono lo specchio di un sociale in
> mutazione e delle sue implicazioni comportamentali e culturali (underground).
> Così non si può dimenticare la mutua influenza tra da P.K. Dick e la cultura
> psichedelica o non è possibile interpretare Ballard se non in riferimento
> alle tendenze socio/politiche di metà anni '70. Necessariamente anche gli
> stili di queste forme letterarie diventano un segno del loro tempo, delle sue
> contraddizioni e, come i comportamenti controculturali da cui traggono
> parzialmente ispirazione, produrrano mutamenti radicali nel linguaggio,
> nell'immaginario e nel sistema semantico.

Possiamo definirla come la fantascienza radicale degli anni '90, una corrente
che da pochi anni sta svecchiando e ribaltando gli schemi dell'ormai vetusta
fantascienza classica, quella alla Asimov, amata soprattutto da chi
fantascienza non ne legge. Di legami con il passato ce ne sono parecchi, perché
la fantascienza radicale nei contenuti e nelle forme, è sempre esistita, magari
in parallelo con quella convenzionale. Negli anni Sessanta-Settanta l'esponente
più di spicco fu senz'altro [Philip K. Dick][8]. I suoi libri più riusciti sono
dei veri e propri trip. L'universo si scompone, la realtà è solo quella
soggettiva dei personaggi che con il loro agire la modificano completamente,
anche se involontariamente. Come esempio basti pensare al romanzo Le tre
stimmate di Palmer Eldtrich, in cui i consumatori di una droga aliena non solo
creano dei propri universi personali all'interno dell'universo, per cosi' dire,
convenzionale, ma riescono anche a penetrare e a fondersi con gli universi
creati dagli altri. L'universo per Dick non è più un luogo affascinante da
esplorare ma un sogno molto spesso terrificante dal quale non si riesce a
uscire. Nello stesso periodo in Inghilterra viene fondata la rivista "NEW
WORLDS" (Nuovi Mondi) da M. Moorcock che, interprete e traspositore in chiave
S.F. dei fermenti culturali del periodo, riesce a raccogliere intorno a s
numerosi giovani autori di talento. Nasce cosi' la cosiddetta New Wave,
letteratura fantascientifica che affronta tutta una serie di tematiche, (sesso,
droga, religione, conflitti sociali) fino ad allora escluse dal genere. Per le
innovazioni stilistiche che si ritroveranno in molte opere, si apriranno
dibattiti in cui si discute la loro reale appartenenza alla fantascienza, ma
sarebbe sterile ricordarli. Importa dire invece che, nonostante molti lavori
non fossero all'altezza delle idee contenute, la forza della corrente fu
prorompente ed influenzò tutta la nuova generazione di scrittori anglosassoni e
statunitensi. Il leader è sicuramente [Ballard][10], che conia il termine
"spazio interno" contrapposto allo spazio esterno extraterrestre. Per Ballard
lo spazio meno conosciuto è quello della Terra con i suoi cinque miliardi di
alieni che l'abitano e con le loro menti inesplorate. La quotidianità non
esiste. Tutto si può sconvolgere. La terra può smettere di girare, o smettere
di piovere, o piovere sempre. La gente può smettere di dormire, o dormire
sempre di più. Non vi è spiegazione di quello che succede ed i suoi personaggi
non la chiedono. Si limitano a guardare e a cercare di sopravvivere. Al limite
neanche quello.  Altri autori sono Disch che raggiunge l'apice con lo splendido
"Campo Archimede" e la sua droga per l'espansione dell'intelligenza, Brunner e
Aldiss.  Negli States abbiamo Delaney, con la sua Dhalgren metropoli del futuro
prossimo in disfacimento, Zelazny, Spinrad. Tutte le tematiche che ho accennato
saranno riprese, anche se in chiave differente, dalla corrente Cyberpunk.
Cerchiamo di rintracciarne i dati più innovatori. Il primo dato importante
della visione cyber mi pare essere l'unicità del tutto. Dopo l'"io frammentato"
o quello attonito di Ballard in balia degli eventi, contrapposti all'universo
estremo o interno, qui vi è il superamento della dualità. Non esiste più il
normale e l'alieno, l'uomo o la macchina, la realtà o il sogno, la psiche e la
materia.  Esiste solo il dato, il l'unità di informazione che accomuna ogni
cosa. Ogni cosa è tale in quanto dato in un network informatico che è possibile
manipolare e che a sua volta manipola. Niente è realmente alieno perché per
esistere, perché ne sia riconosciuta l'esistenza, deve essere inserito nella
stessa rete in cui tutto e tutti fanno parte. L'oggettività delle cose diventa
ininfluente, puro supporto ed il "flatline" di Neuromante, epico romanzo di
[Gibson][11], ne è l'esemplificazione: la registrazione di una personalità
funziona tanto quanto una personalità legata ad un (supporto) corpo. Anche
l'oggettività del corpo è comunque manipolabile. La tecnologia entra in questa
oggettività trasformandola e rafforzandola in un processo di contaminazione che
parte dal cervello, per arrivare ai muscoli e al sangue. La battaglia per il
potere non può quindi che diventare la battaglia per controllo dei dati, dei
mezzi di produzione e manipolazione dei dati, quindi in ultima istanza di
produzione della realtà.  Immagine speculare di questo universo è quella di
Blood Music (La Musica del Sangue) di Greg Bear. Qui l'elemento unificante è il
"noocita", il microorganismo intelligente che, perfetto sostituto del bit,
unisce tutto ciò a cui entra in contatto, sussumendo ogni elemento oggettivo in
parte dello stesso circuito bio/logico. Un universo chiuso in cui entrarvi
significa diventarne parte in modo irreversibile. Qui la distopia è ancora più
radicale: se le logiche dell'interfaccia non permettono discostamenti ma
concedono spazi di intervento e cambiamento, nella Musica del sangue la logica
della sopravvivenza del "Gigaverso" impedisce l'azione volitiva del singolo.
Nell'universo cyber, la tecnologia cessa di essere quindi un supporto
all'avvenimento narrato. Se in Ballard è mero espediente ed in Dick spesso
causa scatenante quanto molto spesso incomprensibile, qui diventa habitat,
nuova natura. Se i personaggi si muovono tra flussi di dati, in città
fatiscenti o in satelliti è perché non potrebbero esistere in altri luoghi. Gli
spazi non tecnologici sono ininfluenti. La tecnologia è il motore
dell'evoluzione. Una tecnologia che pervade l'essere umano che introietta e ne
viene introiettato in un rapporto di mutuo scambio. Per parafrasare N. Spinrad
"l'ideologia di fondo è l'accettazione dell'evoluzione tecnologica e
dell'alterazione della nostra definizione di umanità, la romantica accettazione
della modificazione tecnologica della nostra specie". Questa accettazione
dell'evoluzione, alla fine, è ciò che determina l'angolazione dell'utilizzo
delle tematiche sopra scarnamente accennate. La tensione superoministica è
stemperata dalla visione pragmatica o cinica, ma esiste. Il personaggio è un
mutante iperattrezzato alla sopravvivenza nel nuovo habitat decisamente
superiore al vecchio "sapiens sapiens", e si muove alla conquista dei propri
obiettivi contro tutto e tutti, nichilista e solo, senza verità da dare o da
cercare, ma intento solo alla soddisfazione delle proprie necessità.

Seconda Generazione
-------------------

*Il cyberpunk è morto*

La conclusione del "cyberpunk" come esperienza letteraria è stata sancita, nel
giugno del 1991, da un articolo di Bruce Sterling apparso sulla rivista inglese
"Interzone" (*Cyberpunk in the Nineties*, tradotto come *Il cyberpunk negli
anni Novanta* sul n. 2 dell'"Isaac Asimov Science Fiction Magazine"). In quelle
pagine, il teorico riconosciuto del Movimento storicizzava i meriti di se
stesso e dei propri colleghi, incitava a "superare" il lavoro già compiuto,
inveiva contro gli imitatori superficiali e concludeva profetizzando che "gli
anni Novanta non apparterranno al cyberpunk".

Ciò nonostante, la fantascienza dell'ultimo decennio deve moltissimo al lavoro
di Sterling e soci: i loro temi e le loro tecniche narrative si sono ormai
diffuse a macchia d'olio nella narrativa di genere, aggiornandola. E se molti
autori che facevano parte del movimento sono passati a occuparsi di altre cose,
non è mancato chi negli anni successivi ha saputo riprodurne in un libro lo
spirito originario, a cominciare naturalmente da Neal Stephenson con il suo
ormai mitico *Snow Crash*.

Ma oltre a Stephenson, anche altri autori hanno scritto "vero" cyberpunk dopo
la data fatidica del 1991. Innanzitutto Pat Cadigan (di cui la ShaKe ha
presentato il romanzo d'esordio *Mindplayers*), che continua a scrivere romanzi
e racconti di chiara ispirazione cyberpunk. Ma la lista dei titoli recenti è
ben nutrita e, solo per limitarci a quanto è stato tradotto in italiano, spazia
dagli incubi surreali dell'inglese Richard Calder (*Virus ginoide*, Nord 1996)
alle imitazioni giapponesizzanti e non del tutto riuscite di Alexander Besher.
Una gamma di tentativi che testimonia, ancora una volta, la vitalità della
formula letteraria lanciata da William Gibson nel 1984.

Cyberpunk Politico
==================

*Cogliere l'occasione*

La difficoltà più grande che incontra oggi ogni ricercatore, che lavori sulle
interzone prodotte dall'avanzare tritatutto del moderno, è quella di non poter
esprimere sintesi. Questo non tanto per cautela ocorrettezza scientifica,
quanto proprio per la frammentazione accelerata subita dal mondo negli ultimi
quindici anni. La sintesi onnicomprensiva in effetti sembra essere
completamente inattuale. Nonostante questa cautela metodologica di fondo,
sembrano emergere alcuni elementi significativi, riguardanti la mutazione
antropologica in atto e anche le dinamiche di resistenza controculturali. Il
percorso precedente ci sembra possa darne atto.

Vorremmo perciò qui insistere su un altro elemento, più propriamente politico.
Essenziale, difatti, appare oggi il condurre una battaglia per il diritto
all'informazione, tramite la costruzione di reti alternative sempre più
ramificate. È questa una lotta che può essere vinta, tenuto conto che lo stesso
capitale non può arrestare, per ragioni di opportunità politica, un movimento
economico intrinseco al suo stesso progredire. Il computer è uno strumento
potenzialmente, estremamente democratico, l'importante è acquisirne la
consapevolezza a livello collettivo. Per di più la letteratura cyberpunk sembra
essere un ottimo cavallo di Troia, buono per interessare quei settore attigui,
oggi non ancora coivolti, che gravitano nelle orbite più lontane dal movimento.
Oggi tramite il cyberpunk si offre l'opportunità, a tutti gli operatori
culturali e di movimento, di aprire un nuovo enorme campo di produzione di
immaginario collettivo, capace di scardinare la tenace cappa immaginativa
esistente, dalla quale da più tempo si è compressi. I temi ispiratori del
cyberpunk, come si è dimostrato, appartengono per storia, evocazioni e
fascinazioni future ai movimenti controculturali. Bisogna collettivamente
riappropriarsene.

Potrebbe essere questa la risposta da offrire al paradosso comunicativo che
caratterizza la fase attuale della società: un mondo che mai èstato così
mediatico, ma anche mai così povero quanto a comunicazione reale.

Dalla metà degli anni Ottanta fino ai nostri giorni, il cyberpunk ha fatto
molta strada, passando dalle cantine alla ribalta di Hollywood. Ma alle istanze
politiche che il cyber ha contribuito a lanciare ben poche risposte sono state
ancora date.

Volendo fare una sintesi di ciò che è successo in questi ultimi anni, si può
dire che il cyberpunk è servito da collante simbolico per una serie di
esperienze prima scollegate fra loro. Gli hacker di tutto il mondo hanno in
qualche modo capito che esisteva una percezione sociale più ampia di quella
immaginata rispetto ai problemi da loro sollevati come per esempio la libertà
d'accesso all'informazione. E così pure una serie di artisti, ricercatori
sociali, attivisti di diversa natura sono stati stimolati a ragione in termini
moderni e alternativi sulle tematiche relative all'informazione digitale e non.
Ma il cyberpunk è stato presago anche di una grande trasformazione del mondo
del lavoro, dei diritti e della società nel suo complesso.

In questo senso il nostro percorso si divide ora in:

- Hacking
- Le riviste
- Cyberfemminismo
- Lavoro cyber
- La situazione italiana

Hacking
=======

Hacking è la pratica di liberazione delle informazioni, anche se dai media
viene spesso criminalizzata.

La storia dell'hacking è lunga, percorrila con noi:

Le origini
----------

*NB: capitolo mancante*

La criminalizzazione
--------------------

**"Cyberview"**  
**Il congresso degli hacker**

*di Bruce Sterling*  
tratto da [DECODER #8][12]

L'organizzatore, il ventunenne "Knight Lighting"(aka di Crag Naidorf), ha
recentemente evitato una accusa per "Computer Fraud and Abuse ", che poteva
farlo mettere in galera per trenta anni. Mi sembrava che una certa attenzione
dovesse essere prestata.

L'hotel della riunione, un logoro ma accomodante motor-in appena fuori
l'aeroporto di Saint Louis, aveva già ospitato altre volte dei SummerCon. Il
cambiamento del nome era stata una buona idea. Se il personale del Motor Inn
fosse stato messo in allerta o se avesse capito che quelli erano ancora gli
stessi dell'anno prima, la questione sarebbe diventata spinosa.

L'hotel del SummerCon nel '88 fu completamente messo fuori servizio. I Servizi
Segreti USA quell'anno avevano messo su un negozietto nella sala delle
informazioni e avevano videoregistrato le buffonate alcoliche dell'ormai
universalmente conosciuta "Legion of Doom" attraverso uno specchio speciale. Lo
svolgimento del SummerCon del '88 costituì il maggior capo d'accusa della
cospirazione criminale contro il giovane Knight Lighting, durante il processo
federale subito nel 1990.

Quell'hotel ispirava tristi pensieri. D'altronde, la gente, come agli incontri
del SummerCon, era già abbondantemente innervosita, costretta a giocare a
"caccia al cibo". Al SummerCon generalmente è presente almeno uno spione
federale. Agli hacker e ai phreak piace molto parlare. Essi parlano di telefoni
e computer e gli uni degli altri.

Per chi ci è dentro, il mondo del computer hacking è molto simile al Messico.
Non c'è classe media. Ci sono milioni di ragazzi che smanettano con i loro
modem, cercando di carpire un numero per le telefonate a lunga distanza,
cercando di succhiare software piratato - i "Kodez Kidz" e i "Warez Doodz". Ci
sono i peones, "rodents" (roditori). Poi ci sono alcuni zelanti "wannabes"
(aspiranti), molto promettenti, i pupilli. Non molti. Molti di meno ogni anno,
ultimamente.

E poi ci sono i tipi massicci. I giocatori. I Legion of Doom sono decisamente
massicci. I membri del tedesco "Chaos Computer Club" sono decisamente massici e
perciò già riabilitati sulla parola dopo la loro disastrosa relazione con il
KGB. I "Master of Destruction" (M.o.D.) di New York sono un dolore al culo per
i loro rivali nell'underground, ma devo ammettere che sono proprio massicci.
"Phiber Optik" dei M.o.D. ha appena terminato la condanna al servizio civile,
come pure... "Phoenix" e la sua banda giù in Australia, sono stati massicci, ma
nessuno ha più sentito molto di "Nom" e "Electron" da quando il "caldo"
(intensificazione dell'attività di investigazione) australiano è calato su di
loro. La gente in Olanda è veramente attiva, ma lì, per vari motivi, gli hacker
non possono essere classificati come "massicci". Probabilmente perché il
computer-hacking in Olanda è legale e quindi per questo nessuno è stato mai
cuccato. Gli olandesi hanno, in qualche modo, perso la loro giusta attitudine
irriverente.

La risposta americana all'approccio olandese cominciò con l'arrivo, in una
disciplinata confusione, di un bus navetta dell'aeroporto o di uno scassato e
decadente automezzo per il trasporto di collegiali. Un pirata del software, uno
tra i congressisti più benestanti, ostentava un dispositivo per accecare i
radar utilizzati per intercettare le veloci fuoriserie sportive. In una oscura
era, prima dell'era dei jet, questo lembo di Saint Louis era un caldo, fertile
paesaggio alla Samuel Clemens. Le camicette estive fiorivano lungo l'autostrada
a 4 corsie e lungo la strada che porta all'aeroporto.

Lo sgraziato hotel del CyberView sembrava sbattuto sul paesaggio come se fosse
stato sganciato da un B-52. Una piccola torre a uffici si profilava in un
angolo, accanto un grande parcheggio. Il resto era una incoerente confusione di
lunghi, stretti e scarsamente illuminati corridoi, con una piccola piscina, un
negozio di souvenir con una grande vetrina e una tetra sala da pranzo. L'hotel
era abbastanza pulito e il personale, malgrado le provocazioni, si dimostrava
propenso a pensare al proprio tornaconto. Per quanto li riguardava, gli hacker
sembravano completamente soddisfatti del posto. Il termine hacker ha avuto una
storia variegata. Al vero hacker, quello tradizionale, piace scrivere
programmi. Piace "macinare codice", immergendosi dentro le più dense astrazioni
finché fuori, nel mondo, il giorno sbianca i terminali dei computer. Gli hacker
se la tirano da tecnologi in camice bianco con fitte e ritorte barbe, parlano
completamente in gergo, fissano a lungo il vuoto e ridono brevemente senza
nessuna ragione. I tipi del CyberView, sebbene essi si autodefiniscano hacker,
sono meglio identificabili come intrusori di computer. Essi non hanno
l'aspetto, non parlano, non agiscono come gli hacker stile MIT anni Sessanta.

Gli intrusori di computer degli anni Novanta non sono tecnici affidabili. Essi
sono giovani maschi bianchi di periferia, e sembrano abbastanza inoffensivi, ma
sono scaltri. Sono proprio il tipo di ragazzo che puoi trovare a "nuotare nudo"
(curiosare in computer altrui) alle 2 di mattina in una piscina della
periferia. Proprio quel tipo di ragazzo che potrebbe immobilizzare con il lampo
di un flash il proprietario di casa, poi afferrare freneticamente i suoi
pantaloni e saltare lo steccato, lasciandosi dietro una mezza bottiglia di
tequila, una t-shirt dei Metallica e, probabilmente, il suo portafoglio. Uno
potrebbe chiedersi perché, nella seconda decade della rivoluzione del personal
computer, molti intrusori di computer sono ancora teenager bianchi di periferia
unusualmente intelligenti.

L'hackeraggio-come-intrusione è durato abbastanza a lungo da aver potuto
produrre una intera generazione di adulti, seri e coscienziosi criminali del
computer. Ma ciò non è proprio avvenuto. Il motivo è che tutti gli intrusori
smettono dopo i 22 anni. Si annoiano. Intrufolarsi nelle piscine altrui perde
semplicemente il suo fascino. Escono dalla scuola. Si sposano. Si comprano la
"piscina". Devono trovare dei simulacri della vita reale. La Legion of Doom - o
piuttosto, l'ala Texana dei L.o.D. - quel week-end di Giugno ha decisamente
impressionato Saint Luis col proprio stile elitario. La Legion of Doom viene
descritta come "una banda di strada altamente tecnologica" dal Servizio
Segreto, ma questo è sicuramente una delle più chiacchierate, delle più
sciocche e meglio pubblicizzate cospirazioni criminali nella storia americana.
Negli ultimi anni, non molto si è saputo di "Lex Luthor", il fondatore della
Legion. L'ala della Legion di Atlanta, "Prophet", "Leftist" e "Urvile", sta
uscendo, proprio adesso, da varie prigioni per entrare in centri di
riabilitazione in Georgia. "Mentor" si è sposato e per vivere scrive
computer-game di fantascienza. Invece "Erik Bloodaxe", "Doc Holiday" e
"Malefactor" sono stati presenti sia di persona sia sull'ultima edizione del
"Time" e del "Newsweek". Il CyberView ha offerto una magnifica opportunità per
i Doomster del Texas per annunciare la costituzione della loro ultima, ehm,
organizzazione high-tech, la "Comsec Data Security Corporation".

La ComSec vanta un ufficio per la corporate a Houston, un analista di marketing
e un vero software di certificazione del bilancio. I ragazzi della Legion sono
ormai pistole digitali da affittare. Se possiedi una società sotto "pressione",
allora potete assumere a giornata, più il biglietto aereo, il più famoso hacker
di computer d'America, il quale ti mostrerà, appena messo piede in azienda,
come stanno le cose, mettendoti ordine nella tua casa (digitale). Garantito!

Il discorso lo tenne Bloodaxe, un giovane texano flessuoso e sorprendentemente
bello, con i capelli biondi lunghi fino alle spalle, occhiali a specchio,
cravatta e una formidabile parlantina. Prima, una trentina di suoi simili si
erano radunati ai piani superiori dell'hotel Mark Twain Suite esagerando con il
caffè nei bicchierini al polistirene e con Coca stagionata. Bloodaxe sentenziò
severamente circa inconfutabili fatti sulle più aggiornate metodologie di
sicurezza dei computer.

Molti dei cosiddetti "esperti di sicurezza dei computer" (i concorrenti del
ComSec) sono strapagati quasi come gli artisti. Essi addebitano alle ingenue
corporation migliaia di dollari al giorno, solo per raccomandare che i
dirigenti di notte chiudano a chiave gli uffici ed adottino i distruggitori di
documenti. La ComSec Corp. (con l'occasionale consulenza di "Pain Hertz" e
"Prime Suspect") d'altronde può vantare il più formidabile gruppo di autentici
esperti d'America che effettivamente irrompono nei computer.

La ComSec, aggiunse pacatamente BloodAxe, non si è messo in testa di convertire
i propri compatrioti ex-hacker. Questo solo nel caso che qualcuno si sia,
capite, preoccupato... D'altro canto, colui che fosse abbastanza pazzo e
sconsiderato da sfidare un sistema assicurato da ComSec dovrebbe essere ben
preparato ad una seria sfida tra hacker. "Perché le compagnie dovrebbero
fidarsi di 't?" Qualcuno chiese pacatamente. Malefactor, un texano giovane e
muscoloso, con un taglio di capelli costosissimo e la struttura del
line-backer, fece notare che, una volta preso l'incarico, la ComSec sarebbe
stata ammessa nel sistema computerizzato e non avrebbe quindi più avuto nessuna
ragione per "violarlo". D'altronde, gli agenti della Comsec sarebbero provvisti
di regolare licenza e di regolare assicurazione. BloodAxe insistette
appassionatamente sul fatto che la L.o.D. ne avrebbe avuto abbastanza del
hackeraggio fine a se stesso. Semplicemente perché non avrebbe futuro. Sarebbe
venuto il tempo per la L.o.D. di andare oltre e quindi la consulenza alle
corporate sarebbe diventata la loro nuova frontiera. (Le opportunità di
carriera di un intrusore di computer di professione sono, quantunque si dedichi
completamente, decisamente scarse). "Noi non vorremmo trovarci a trent'anni ad
arrostire hamburger o a vendere assicurazioni sulla vita", "oppure fantasticare
di quando Tim Foley pigliava a calci la nostra porta" strascicò BloodAxe
(L'agente speciale Timothy M. Foley del Servizio Segreto USA ha pienamente
meritato la reputazione del più formidabile poliziotto anti-hacker d'America).

BloodAxe sospirò con aria pensosa. "Quando guardo indietro alla mia vita... io
vedo che sono stato a scuola per 11 anni, studiando per diventare proprio un
consulente sulla sicurezza dei computer".

Dopo una ulteriore divagazione, BloodAxe finalmente andò al cuore della
questione. "C'è qualcuno qui che ha qualcosa da rimproverarci?" Domandò
piuttosto timidamente. "Ci sono persone che pensano che la Legion si è
svenduta?" Nessuno sposò questa tesi. Gli hacker scossero la testa, guardarono
in basso alle scarpe da ginnastica e diedero un altro sorso di Coca. Sembravano
non vedere come questo potesse fare la differenza, sinceramente. Almeno non a
questo punto. Circa la metà dei partecipanti al CyberView aveva pubblicamente
affermato di essersi tirato fuori dal gioco dell'hacking. Almeno uno degli
hacker presenti - (che ostentava, per ragioni note solo a se stesso, una
parrucca bionda e un diadema da grande magazzino e che adesso stava
raccogliendo dei Cheetos lanciatigli nel bicchiere di polistirene) -
attualmente tirava a campare facendo il consulente per qualche investigatore
privato. Quasi tutti i presenti sono stati arrestati, hanno avuto i loro
computer sequestrati, sono stati interrogati o almeno, da quando la polizia
federale aveva messo sotto pressione gli hacker teenager, avevano sputato
sangue. Dall'87, dopo quasi un anno da quando si erano lanciati seriamente
nell'offensiva anti-hacker, i Servizi Segreti avevano già raccolto dossier
d'indagine su tutti quelli che contavano veramente. Dal '89, essi avevano
fascicoli su praticamente ogni anima pia dell'underground digitale americano.
Il problema generato dell'offensiva poliziesca è che non fu mai scoperto chi
erano gli hacker. Il problema è sempre stato far capire cosa diavolo volessero
davvero combinare, ora con più difficoltà, cercando di convincere l'opinione
pubblica quanto la minaccia possa essere davvero importante e pericolosa per la
pubblica sicurezza. Dal loro punto di vista gli hacker sono convinti che gli
sbirri abbiano agito incomprensibilmente tardi. Gli sbirri e i loro sponsor
nelle compagnie telefoniche, proprio non riescono a capire il moderno mondo dei
computer e quindi ne sono spaventati. "Essi pensano che ci siano grandi vecchi
che oltre a coltivare giri di spie, ci assoldino", così mi confidò un hacker.
"Loro non capiscono che noi non lo facciamo per soldi, bensì lo facciamo per
potenza e per conoscenza".

Le persone adibite alla sicurezza dei telefoni che hanno contattato
l'underground sono state accusate di doppio gioco e di agire sotto la minaccia
di licenziamento dei loro impauriti datori di lavoro. Un giovane del Missouri
ha così freddamente psicanalizzato il contrasto. "I loro padroni sono
dipendenti da cose che non capiscono. Hanno affidato la loro sopravvivenza ai
computer".

"Conoscenza e potenza" possono sembrare motivazioni bizzarre. "Soldi" è una
motivazione molto più facile da capire. Ci sono montanti "armate" di ladri
professionisti che rapinano servizi per lucro. Gli hacker, però, sono ben
dentro la potenza e la conoscenza. Questo li ha resi più facili da prendere,
certo più degli attivi balordi di strada che rubano codici di accesso negli
aeroporti. Questo li ha resi molto diffidenti. Consideriamo i problemi sempre
più rischiosi posti da i "Bulletin Board System". I Board sono computer
casalinghi attaccati alla linea telefonica, che possono trasmettere dati, testi
scritti, programmi, giochi per computer e posta elettronica. I board furono
inventati nel lontano Settanta, ma mentre la grande maggioranza dei board sono
totalmente inoffensivi, alcuni board pirata divennero velocemente l'autentica
spina dorsale dell'underground digitale degli anni Ottanta. Più della metà dei
presenti al CyberView gestiva un proprio board. "Knight Lighting" ha tenuto una
rivista elettronica, "Phrack", che fu disponibile su molti board underground
d'America.

I board suggeriscono misteri. I board suggeriscono cospirazioni. I board sono
stati accusati di dare asilo a: satanisti, anarchici, ladri, pedofili, nazisti
ariani, fanatici religiosi, spacciatori - e naturalmente pirati di software,
phreack telefonici e hacker.

I board underground di hacker hanno fatto di tutto per essere poco
rassicuranti, spesso ostentando terrificanti nomi fanta-heavy metal come "Speed
Demon Elite" "Demon Roach Underground" e "Black Ice".

I board degli hacker oggigiorno tendono a mettere in evidenza denominazioni più
defilate come "Uncensored BBS", "Free Speech" e "Fifth Amendment". I board
dell'underground accolgono cose equivoche e che incutono paura come, diciamo, i
giornali underground degli anni Sessanta - il tempo in cui gli yippies
shockavano Chicago e il Rolling Stone regalava spillette agli abbonati. Gli
"Anarchy Files" sono caratteristiche comuni ai board fuorilegge, e tali file
descrivono come costruire bombe-carta e bombe-molotov, come prepararsi
metedrina e LSD, come forzare ed entrare in un edificio, come far saltare un
ponte, la via più facile per uccidere qualcuno con un solo colpo di un oggetto
senza punta. Questi board annoiano a morte la gente regolare. Non dimenticate
che tutti questi dati sono disponibili a tutti in pubbliche librerie dove sono
garantiti dal Primo Emendamento. C'è qualcosa nel fatto di risiedere su un
computer - dove ogni ragazzo con un modem ed una tastiera può leggerli e
stamparli e quindi diffonderli, in piena libertà - c'è qualcosa in tutto ciò
che fa rabbrividire.

"Brad" è un pagano della Nuova Era di Saint Louis che tiene un servizio
chiamato "Weirdbase" disponibile su una rete di BBS a diffusione internazionale
chiamato FIDONET. Brad fu coinvolto in un interminabile scandalo quando i suoi
lettori costituirono una spontanea e sotterranea catena per aiutare uno
stregone della Nuova Era a far uscire dal Texas clandestinamente la figlia
adolescente, allontanandola dalla vigente legislazione
cristiano-fondamentalista in fatto di famiglia, la quale fu convinta del tutto
che il padre aveva ucciso la moglie e che intendeva sacrificare sua figlia a
Satana! Lo scandalo fu montato da una TV locale di Saint Louis. Gli sbirri
prima circuirono, poi misero in galera Brad. Il tanfo di patchouli di Aleister
Crowley aleggiava pesante nell'aria. Non c'era fine allo stato di confusione.

Se ti senti un po' confuso e un po' esitante e hai un board sul problema,
significa che quello è un vero problema. L'editore di giochi di fantascienza
Steve Jackson ha avuto il suo board confiscato nel 1990. Alcuni sostenitori
dell'ibernazione in California, che avevano congelato una donna per la
conservazione dopo la morte prima che fosse ufficialmente, ehm, "morta" hanno
avuto il loro board confiscato. Gente che vendeva apparecchiature per la
coltivazione di droghe hanno avuto il board confiscato. Nel 1990 furono chiusi
board in tutta l'America: Illuminati, Clli Code, Phoenix Project, Dr. Ripco. I
computer vennero confiscati come "prove", ma siccome questi potrebbero essere
trattenuti indefinitivamente dalla polizia per accertamenti, l'operazione
assomigliava molto ad una confisca e una condanna senza processo. Questa era
una buona ragione per cui Mitchell Kapor si è fatto vedere al CyberView.

Mitch Kapor fu il co-inventore del programma best-seller delle vendite, Lotus
1-2-3 e fu il co-fondatore del colosso del software Lotus Development
Corporation. Egli è stato uno dei fondatori del gruppo sulle libertà civili
elettroniche "Electronic Frontier Foundation". Kapor, ora quarantenne,
abitudinariamente veste magliette hawaiane ed è il tipico multimilionario
post-hippie cibernetico. Kapor e "Johnny Mnemonic", il capo del comitato legale
del EFF, avevano volato fin qui con il jet privato di Kapor per l'evento.

Kapor fu preso senza scampo nella rete dell'underground digitale quando
ricevette per posta un floppy, non sollecitato, da un gruppo fuorilegge
conosciuto come la "Nuprometheus League". Questi bricconi (non ancora
arrestati) avevano rubato alla Apple Computer Inc. del software proprietario e
confidenziale e poi lo avevano distribuito in lungo e in largo allo scopo di
bruciare i segreti commerciali di Apple oltre che umiliarla. Kapor pensò che il
disco potesse essere uno scherzo, o più verosimilmente un geniale stratagemma
per infettargli i computer con un virus. Ma quando si presentò la FBI, su
suggerimento di Apple, Kapor si scandalizzò della loro evidente manifestazione
di ingenuità. C'erano questi agenti federali ben vestiti che educatamente
dicevano "Mr. Kapor" di su e "Mr. Kapor" di giù, pronti a condurre una guerra
al coltello contro i malvagi e razziatori hacker. Essi non sembravano afferrare
che gli hacker hanno costruito l'intera industria del personal computer. Jobs
fu un hacker, pure Wozniak, anche Bill Gates, il più giovane miliardario nella
storia d'America, tutti hacker. Il nuovo regime conservatore alla Apple aveva
fatto saltare i suoi vertici anche i federali erano "desiderosi", ma senza uno
straccio di prove. Bene, siamo benevoli - i federali furono "sfidati da molti
indizi" "indeboliti nelle prove." "fuorviati dalle prove..."

Addietro negli anni '70 (come Kapor raccontò agli agitati e rispettabili
giovani hacker) egli stesso aveva praticato il "pirataggio di software" -
ovvero come sarebbero conosciute oggi quelle attività. Naturalmente, allora il
"software per computer" non era un'industria strategica - invece oggi, gli
hacker hanno la polizia alle calcagna per aver fatto cose che proprio i
pionieri di quell'industria avevano messo in pratica quotidianamente. Kapor era
incavolato per questo. Era incavolato che la sua storia personale, lo stile di
vita che aveva contraddistinto la sua pionieristica gioventù, potesse venire
boriosamente stravolta nella realtà storica dagli ultimi arrivati androidi di
corporation. Questo è il motivo per cui oggigiorno viene taciuto che Kapor
sinceramente dichiarò di aver assunto, negli anni '60, LSD.

Molti, tra gli hacker più giovani, cominciarono ad allarmarsi per questa
ammissione di Kapor e lo fissarono con stupore come se avesse potuto esplodere.

"La legge è capace di dare solo mazzate, quando quello di cui ci sarebbe
bisogno sono invece multe per divieto di sosta o per eccesso di velocità".
Aggiunse Kapor. L'isteria anti-hacker nel 1990 aveva attirato l'attenzione
della nazione. Pesanti inasprimenti della legge furono messi in campo contro
quelle che erano minacce inesistenti. Nello stesso giorno dell'annuncio della
formazione del Eletronic Frontier Foundation, a Washington DC, il problema
venne formalmente presentato da un comitato del Congresso con una
caratterizzazione degna di thriller film - Die Hard II, in cui
terroristi-hacker si impadroniscono di un computer dell'aeroporto - come se una
fantasia di Hollywood potesse essere un chiaro e immediato pericolo per la
repubblica americana. Un film simile sul pericolo degli hacker, Wargames, venne
sottoposto al Congresso nella metà degli anni Ottanta. L'isteria non servì a
nessuno scopo e creò un'accozzaglia di assurde e inapplicabili leggi atte a
fare più danno che bene.

Kapor non intendeva "nascondere le differenze" tra la sua Fondazione e la
Comunità underground. La convinta opinione della EFF, era che introdursi nei
computer di nascosto è moralmente sbagliato. Come pure rubare i servizi
telefonici, merita una punizione. Non certo leggi draconianamente spietate.
Leggi che non devono essere la rovina dell'intera vita del giovane-gangster.

Dopo un dibattito vivace e piuttosto serio sui problemi della libertà di parola
digitale, l'intero gruppo andò a cena in una trattoria italiana del locale
centro commerciale, a carico del capace rimborso spese di Kapor. Avendo già
esposto la propria tesi e avendo ascoltato con attenzione, Kapor a quel punto
cominciò a lanciare furtive occhiate al suo orologio. A Boston, suo figlio di
sei anni lo stava aspettando a casa per impegnarlo su un nuovo gioco per
MacIntosh. Fece una veloce telefonata per far scaldare il jet dopodiché Kapor e
il suo avvocato lasciarono la città.

Abbandonate ormai le formalità - diventate ormai tipiche del suo comportamento
- la Legion of Doom cominciava a farsi pesantemente coinvolgere dai "Mexican
Flags".

Un Mexican Flag è un letale intruglio multistrato di granatina rossa, tequila
bianca e crema di menta verde. Il tocco finale è un sottile strato di rum a
75deg., a cui viene dato fuoco, e il tutto viene succhiato con una cannuccia.

Il rito formale del fuoco e della cannuccia presto viene abbandonato, mentre
contemporaneamente le cose attorno cominciavano a disintegrarsi. Vagando di
camera in camera, la folla divenne fragorosamente tumultuosa, senza tuttavia
creare problemi, tanto che la moltitudine del CyberView riuscì a mettere
completamente sottosopra un'intera ala del hotel.

"Crimson Death", un vivace e giovane esperto hardware dalla faccia di bambino
con tanto di un orecchino al naso e una file di tre alle orecchie, cercò di
hackerare il centralino telefonico del hotel, riuscendo solo ad interrompere il
servizio telefonico della sua camera.

Qualcuno annunciò che c'era uno sbirro che sorvegliava dall'altra ala del
hotel. Ne seguì un blando panico. Gli hacker "brilli" si affollarono alla
finestra. Un signore, che vestiva boxer di seta e accappatoio di spugna, stava
silenziosamente sgusciando via da una porta dell'altra ala.

Nella vicina ala del hotel stava aveva luogo un raduno di "scambio di mogli"
con annessa orgia di fine settimana. Era un gruppo di "disinibiti" di Saint
Louis. Si venne a sapere che la guardia era anch'egli uno sbirro disinibito
fuori servizio. Minacciò di fare a pezzi "Doc Holiday". Un altro disinibito
pestò ben bene "Bill da RNOC", la cui pruriginosa curiosità di hacker,
naturalmente, non conosceva limiti. Non era stata proprio uno scontro. Così
come il week-end trascorreva tranquillamente così pure la sbronza fluiva
felicemente e gli hacker lentamente, ma decisamente, si infiltrarono tra i
disinibiti, che a loro volta si dimostrarono sorprendentemente aperti e
tolleranti. Ad un certo punto, un gruppo di hacker vennero invitati ad unirsi
ai loro festeggiamenti, a patto che "portassero anche le loro donne".

A causa del polverizzante effetto dei numerosi Mexican Flags, il Comsec Data
Security sembrava ora aver proprio qualche piccolo problema. Gli hacker
svanirono nel centro della città brandendo le loro colorate fotocolor
pubblicate sul "Time" e poi tornarono con alcuni impressionanti esempi
mozzafiato di esemplari femminili di Saint Louis, e una di quelle, in un
momento di pausa, irruppe nella camera di Doc Holiday, gli vuotò il portafoglio
e gli rubò il Sony e tutte le magliette.

Gli eventi si esaurirono completamente in concomitanza dell'episodio finale di
Star Trek: The next generation. Lo show venne seguito con rapita attenzione, ma
al termine ricominciarono ad assillare gli scambia-partner. Bill da RNOC riuscì
astutamente ad aggirare la guardia degli scambia-partner, infiltrarsi
nell'edificio e a decorare tutte le porte chiuse con grumi di mostarda sparati
da una bottiglia a spruzzo.

Nell'accecante luce della post-alcolica domenica mattina, un hacker
orgogliosamente mi mostrò una grande placca scritta a mano che recitava
"PRIVATO - STOP", la quale era stata rubata a uno sfortunato scambia-partner
sulla via della fuga dall'altra ala del palazzo. In un qualche modo, egli si
era ingegnato per trovare una via d'accesso all'edificio. Poi aveva ingaggiato
con un'agente di viaggio, anche lui scambia-partner, dopo averlo intortato con
modi persuasivi nella sua camera, una lunga e assai istruttiva conversazione
circa la sicurezza dei terminali di computer degli aeroporti.

La moglie dell'agente di viaggio, in quel frangente, era sdraiata sul letto
indaffarata in un distratto coito orale con una terza persona. Nella
discussione saltò fuori che in passato lei aveva lavorato molto con il Lotus
1-2-3. Fu allora molto perplessa di sentire che l'inventore del programma,
Mitch Kapor, era stato in quel hotel quella stessa settimana.

"Mitch Kapor. Proprio qui? Qui a Saint Louis? Wow. Come è strana la vita."

L'hacking sociale
-----------------

È la pratica dell'hacking a fini sociali e politici, ed è incentrata sull'etica
hacker, una serie di principi stabiliti dagli hacker del MIT nel 1961(!). Ha
trovato terreno fertile negli Usa, già negli anni Sessanta, e in [Europa][13],
particolarmente Germania (Chaos Computer Club) e in Olanda (Hacktic) negli anni
Ottanta e Novanta. Di seguito i punti dell'etica così come sono stati
tramandati da Steven Levy.

I principi dell'etica hacker
----------------------------

> L'accesso ai computer - e a tutto ciò che potrebbe insegnare qualcosa su come
> funziona il mondo - dev'essere assolutamente illimitato e completo. Dare
> sempre precedenza all'imperativo di metterci su le mani!

Gli hacker credono nella possibilità d'imparare lezioni essenziali sui sistemi
e sul mondo smontando le cose, osservando come funzionano, e usando questa
conoscenza per creare cose nuove, ancor più interessanti. Detestano qualsiasi
persona, barriera fisica o legge che tenti d'impedirglielo.

Questo è vero soprattutto quando un hacker vuole aggiustare qualcosa che (dal
suo punto di vista) è guasta o necessita di miglioramento. I sistemi imperfetti
fanno infuriare gli hacker, il cui istinto primario è di correggerli. Questa è
la ragione per cui in genere gli hacker odiano guidare le auto: il sistema dei
semafori collocati a casaccio unitamente all'inspiegabile dislocazione delle
strade a senso unico provocano ritardi così inutili che l'impulso è quello di
riordinare la segnaletica, aprire le centraline di controllo e riprogettare
l'intero sistema.

Nel mondo degli hacker chiunque s'incazzasse abbastanza da aprire una
centralina dei semafori e la smontasse per farla funzionare meglio, sarebbe più
che incoraggiato a tentare. Le regole che ci impediscono di prendere in mano
questioni del genere sono ritenute troppo ridicole per dovervisi conformare.
Questo modo di pensare spinse il Model railroad club a istituire, su una base
estremamente informale, un'organismo chiamato *Midnight requisitoring
committee*, il Comitato per la requisizione di mezzanotte. Quando il Tmrc aveva
bisogno di procurarsi un certo quantitativo di diodi, o qualche relè in più,
per implementare qualche nuova funzionalità nel "sistema", un manipolo di
membri dell'S&P attendeva l'oscurità e s'intrufolava nei luoghi in cui queste
cose erano reperibili. Nessuno degli hacker, che erano persone assolutamente
scrupolose e oneste in altre occasioni, sembrava ritenerlo un "furto".
Un'intenzionale cecità!

**Tutta l'informazione dev'essere libera.**

Se non avete accesso alle informazioni di cui avete bisogno per migliorare le
cose, come farete? Un libero scambio di informazioni, soprattutto quando
l'informazione ha l'aspetto di un programma per computer, promuove una maggiore
creatività complessiva. Per una macchina come il Tx-0, arrivato quasi senza
software, chiunque si sarebbe forsennatamente messo a scrivere programmi di
sistema per facilitarne la programmazione, *strumenti per fare strumenti*,
riponendoli in un cassetto della consolle a portata di mano di chiunque volesse
usare la macchina. Questo comportamento evita la temuta e rituale perdita di
tempo per reinventare la ruota; invece di stare tutti a scrivere la propria
versione dello stesso programma, la migliore dovrebbe essere disponibile per
chiunque, e ognuno dovrebbe essere libero di dedicarsi allo studio del codice e
perfezionare proprio quello. Sarebbe un mondo ingioiellato di programmi
completi di ogni caratteristica, che non danno problemi, corretti fino alla
perfezione.

La convinzione, talora accettata acriticamente, che l'informazione dovrebbe
essere libera era un conseguente tributo al modo in cui un ottimo computer o un
valido programma lavorano: i bit binari si muovono lungo il percorso più
logico, diretto e necessario a svolgere il loro complesso compito. Cos'è un
computer se non qualcosa che beneficia di un libero flusso di informazione? Se,
per esempio, l'accumulatore si trova impossibilitato a ricevere informazioni
dai dispositivi di input/output (i/o) come il lettore del nastro o dagli
interruttori, l'intero sistema collasserebbe. Dal punto di vista degli hacker,
qualsiasi sistema trae beneficio da un libero flusso d'informazione.

**Dubitare dell'autorità. Promuovere il decentramento.**

Il modo migliore per promuovere il libero scambio delle informazioni è avere
sistemi aperti, qualcosa che non crei barriere tra un hacker e un'informazione,
o un dispositivo di cui egli possa servirsi nella sua ricerca di conoscenza.
L'ultima cosa di cui c'è bisogno è la burocrazia. Questa, che sia industriale,
governativa o universitaria è un sistema imperfetto, ed è pericolosa perché è
inconciliabile con lo spirito di ricerca dei veri hacker. I burocrati si
nascondono dietro regole arbitrarie (agli antipodi degli algoritmi logici con
cui operano le macchine e i programmi): si appellano a quelle norme per
rafforzare il proprio potere e percepiscono l'impulso costruttivo degli hacker
come una minaccia.

Il simbolo dell'universo burocratico è incarnato da quell'enorme società
chiamata International business machine: l'Ibm (tenete ben presente che i fatti
si stanno svolgendo negli anni Cinquanta e Sessanta). La ragione per cui i suoi
computer, i "bestioni", si basassero sull'elaborazione batch era soltanto
parzialmente riconducibile alla tecnologia delle valvole elettroniche. La
ragione vera stava nel fatto che l'Ibm era una società goffa e mastodontica,
che non aveva compreso la carica innovativa dell'hackeraggio. Se l'Ibm avesse
avuto mano libera (questa cosa la pensavano molti hacker del Tmrc), il mondo
sarebbe diventato una macchina a elaborazione batch, basata su quelle noiose
schede perforate, e soltanto ai più privilegiati sacerdoti sarebbe stato
concesso di interagire effettivamente col computer.

Bastava vedere qualche essere del mondo Ibm, osservare il suo camice bianco ben
abbottonato, l'impeccabile cravatta nera, i capelli dalla scriminatura ben
curata, e il vassoio di schede perforate in mano. Nel centro di calcolo,
dov'erano alloggiati il 704, il 709 e più tardi anche il 7090 - il meglio che
l'Ibm avesse allora da offrire - e fino alle aree riservate, oltre le quali era
proibito l'accesso al personale non autorizzato, regnava un ordine soffocante.
Niente a che vedere con l'atmosfera estremamente informale che circolava
nell'ambiente del Tx-0, dove gli abiti consunti erano la norma e pressoché
chiunque poteva entrare.

**Gli hacker dovranno essere giudicati per il loro operato, e non sulla base di
falsi criteri quali ceto, età, razza o posizione sociale.**

L'immediata accoglienza del dodicenne Peter Deutsch nella comunità degli hacker
(sebbene non da parte dei laureati non hacker) ne è stato un buon esempio.
Parimenti, gente che poteva esibire credenziali particolarmente appariscenti
non veniva presa sul serio prima di aver dato prova di sé dietro la consolle di
un computer.

Questo atteggiamento meritocratico non nasceva necessariamente dall'innata
bontà di cuore degli hacker; derivava invece dal fatto che gli hacker si
curavano meno delle caratteristiche superficiali di ciascuno, e prestavano più
attenzione al potenziale dell'individuo di far progredire lo stato generale
dell'hackeraggio, nel creare programmi innovativi degni d'ammirazione e nella
capacità di contribuire a descrivere le nuove funzioni del sistema.

**Con un computer puoi creare arte.**

Il programma per musica di Samson ne era un esempio, ma per gli hacker
l'artisticità del programma non stava nei suoni piacevoli che uscivano
dall'altoparlante. Il codice del programma possedeva una bellezza propria.
(Samson, in effetti, era stato piuttosto misterioso rifiutandosi di aggiungere
commenti al suo codice originario, spiegando cosa questo stesse facendo in un
determinato momento. Un famoso programma scritto da Samson conteneva centinaia
di istruzioni in linguaggio assembly con un solo commento accanto a
un'istruzione che conteneva il numero 1750. Il commento era Ripjsb, e la gente
naturalmente s'era lambiccata il cervello intorno ai suoi possibili
significati, prima che qualcuno si rendesse conto che il 1750 era l'anno di
morte di Bach, e che Samson aveva scritto un acronimo per *Requiescat in pacem
Johann Sebastian Bach*.)

**I computer possono cambiare la vita in meglio.**

Le riviste
==========

Intro
-----

Sono state l'ossatura del movimento e gli strumenti che hanno reso popolare il movimento nel mondo.
Le dividiamo tra hacker e cyberpunk.

Riviste hacker
--------------

Eredi di "YIPL", le riviste di hacking europee, le prime sono state le tedesche
"Labor" e "Datenschleuder" che hanno avuto come punto di riferimento il [Chaos
Computer Club][13].

Altra "colonna" è l'olandese "Hacktic" legata all'omonimo gruppo, che ha deciso
di non uscire più su carta ma solo in formato digitale. Il gruppo di Hacktic è
oggi gestore del progetto di democrazia telematica [Xs4All][15].

Fondamentale per questa scena è stata ed è l'americana "[2600][16]", guidata da
Emmanuel Goldstein, con sede a New York, che prende il titolo dai toni di
frequenza che governano la linea telefonica americana. Tutte sono state
perseguite e perseguitate dalla polizia, anche se non sono mai state raccolte
abbanstanza prove per farle chiuderle. Il loro spirito è quello di rendere
l'informazione libera a tutti perciò pubblicano senza censure tecniche di
pirataggio di telefonia (phone phreaking), hacking di hardware e software,
televisione satellitare ecc.

È da chiarire che tali azioni non vengono mai rese pubbliche a fine di lucro o
vengono mai suggerite azioni di tipo "criminale". Scopo di tali pubblicazioni è
di far apparire con evidenza i limiti della ricerca attuale nel campo
dell'informazione digitale.

Riviste cyberpunk
-----------------

Hanno usato lo spirito dell'hacking (l'informazione deve essere disponibile a
tutti) ma su scala allargata, tanto che la più famosa ("Mondo 2000"),
inizialmente si chiamava "Reality Hackers" (grazie al suo provocatorio nome non
trovò nessun editore disposto a finanziarla). Si tratta di riviste che si
occupano generalmente di cybercultura e di problemi che ruotano intorno alla
società digitale.

Dalla carta al digitale
-----------------------

Esistono al momento centinaia di cosiddette e-zine, pubblicazioni cioè che non
escono su carta, ma esclusivamente in formato digitale, quindi totalmente
cyber.

Cyberfemminismo
===============

Intro
-----

È una teoria e una pratica che considera la questione femminista inserendola
all'interno di un contesto sociale dove l'impatto della scienza-tecnica ha un
ruolo fondamentale. Perché la rivoluzione tecnologico/informatica non dovrebbe
coinvolgere anche il femminismo?

Riconoscendo nelle donne, come del resto in tutte le altre categorie sociali,
un soggetto costruito dalla cultura e nella società una frammentazione che
annulla la possibilità di qualsiasi modello unico, Donna Haraway con il suo
saggio [Manifesto Cyborg][17], ipotizza l'obiettivo femminista di un mondo
post-genere. Un mondo in cui gli schemi di ruolo sessuale si superano
attraverso liberazione da ordini ancora più generali quali quello dell'umano in
opposizione all'animale, in modo ancora più estremo dell'organismo in
opposizione alla macchina e più generalmente attraverso l'eliminazione di tutti
i bipolarismi che hanno caratterizzato la cultura occidentale fino a questo
momento: "Le dicotomie fra corpo e mente, animale e umano, organico e
meccanico, pubblico e privato, primitivo e civilizzato, ideologicamente sono
tutte in questione".

Altra ispiratrice, questa volta letteraria, è Pat Cadigan, una delle componenti
più interessanti della [seconda generazione][18] del cyberpunk, la quale
afferma: "La donna è la protagonista del futuro, un protagonismo senza la
nostalgia di un mondo pre-patriarcale tipica della fantascienza classica".

Anticipatrici delle cyborg sono le cosiddette Donne infuriate.

Gruppi e contatti
-----------------

Sulle pagine di "Decoder" (sul [numero 10][19] vi è uno speciale di 30 pagine sul
tema del cyber-femminismo) scrive il gruppo italiano Cromosoma X, che ha
pubblicato e curato testi su:

- [Identità fratturate][20], il manifesto introduttivo e programmatico del
  cyber-femminismo italiano.
- [Slittamenti di genere on-line][21], il punto di vista cyb-femm sulle MUD.
- [Mindplayers][22], uno stralcio dall'omonimo romanzo di Pat Cadigan.
- [Il cyborg come antimaterno][23], le tecnologie di riproduzione artificiale.

In Decoder BBS è stata aperta un'area chiamata Cyberwomen dove potrai scambiare
idee sull'argomento.

Links
-----

Collegati a [Geekgirl][24], la più importante cyber-zine del mondo.

Raggiungi le nostre sorelle australiane di [VNS Matrix][25].

La pagina di [Allucquère Rosanne (Sandy) Stone][26], una delle più importanti
teoriche del transgender.


**Manifesto Cyborg - Un estratto**

*di Donna Haraway*  
tratto da *Manifesto Cyborg*, ed. Feltrinelli, collana InterZone

Nell'immaginario occidentale, i mostri hanno sempre tracciato i confini della
comunità. I centauri e le amazzoni dell'antica Grecia, immagini della
disgregazione del matrimonio e della contaminazione del guerriero con
l'animalità e la donna, hanno stabilito i limiti dell'accentrata polis del
maschio umano greco. I gemelli indivisi e gli ermafroditi erano il confuso
materiale umano della Francia agli albori della modernità, il cui discorso si
fondava sulle categorie di naturale e soprannaturale, medico e legale, portento
e malattia, che sono centrali nella definizione dell'identità moderna. Le
scienze evoluzioniste e comportamentiste di scimmie e scimpanzé hanno disegnato
i confini multipli delle identità industriali del tardo Ventesimo secolo. I
mostri cyborg della fantascienza femminista delineano possibilità e confini
politici piuttosto diversi da quelli proposti dalla finzione terrena dell'Uomo
e della Donna.

Molto consegue dal riuscire a pensare le immagini dei cyborg come altri dai
nostri nemici. I nostri corpi, noi stessi: i corpi sono mappe del potere e
dell'identità. I cyborg non fanno eccezione; un corpo cyborg non è innocente,
non è nato in un giardino, non cerca un'identità unitaria e quindi non genera
antagonistici dualismi senza fine (o fino alla fine del mondo). Il cyborg
presume l'ironia; uno ètroppo poco, e due è solo una possibilità. L'intenso
piacere della tecnica, la tecnica delle macchine, non è più un peccato, ma un
aspetto dello stare nel corpo. La macchina non è un quid da animare, adorare e
dominare; la macchina siamo noi, i nostri processi, un aspetto della nostra
incarnazione. Noi possiamo essere i responsabili delle macchine, loro non ci
dominano né ci minacciano; noi siamo i responsabili dei confini, noi siamo
loro. Fino a ora (sembra un secolo) avere un corpo femminile sembrava scontato,
organico, necessario, e consisteva nella capacità di fare da madre e nelle sue
estensioni metaforiche. Solo stando fuori posto abbiamo potuto godere
dell'intenso piacere delle macchine e quindi appropriarcene, col pretesto che
in fondo si trattava di un'attività organica. Il mito dei cyborg considera più
seriamente l'aspetto parziale, a volte fluido, del sesso e dell'abitare
sessualmente il corpo. Il genere in fondo potrebbe non essere l'identità
globale, pur avendo un respiro e una profondità radicati nella storia.

La complessa questione ideologica di cosa conti come attività quotidiana, come
esperienza, può essere esplorata sfruttando l'immagine dei cyborg. Le
femministe hanno sostenuto di recente che le donne sono dedite alla
quotidianità, che le donne in certo qual modo provvedono alla vita quotidiana
più degli uomini, e che quindi occupano, potenzialmente, una posizione
epistemologica privilegiata. Questa è in parte un'affermazione innegabile, che
rende visibile la svalutata attività femminile e la colloca alla base della
vita. La base della vita? Ma allora, tutta l'ignoranza delle donne, le
esclusioni e le carenze di abilità e conoscenza? Che dire dell'accesso maschile
alla competenza quotidiana, al saper costruire, smontare, giocare con le cose?
Che dire delle altre assunzioni di corpo? Il genere cyborg è una possibilità
locale che si prende una vendetta globale. La razza, il genere, e il capitale
richiedono una teoria cyborg di parti e di interi. Nei cyborg non c'è la
pulsione a produrre una teoria totale, ma c'è un'intima esperienza dei confini,
della loro costruzione e decostruzione. C'è un sistema di miti in attesa
didiventare un linguaggio politico su cui basare un modo di guardare la scienza
ela tecnologia e di sfidare l'informatica del dominio per un'azione potente.

Un'ultima immagine. Gli organismi e la politica organismica, olistica,dipendono
dalle metafore di rinascita e invariabilmente attingono alle risorse del sesso
riproduttivo. Vorrei suggerire che i cyborg hanno più a che fare con la
rigenerazione e guardano con sospetto alla matrice riproduttiva e alla nascita
in genere. Per le salamandre, dopo una ferita, come per esempio la mutilazione
di un arto, c'è una rigenerazione che comporta la ricrescita di una struttura e
il recupero di una funzione, con la possibilità costante di una gemellazione o
di altre strane produzioni topografiche al posto della mutilazione. L'arto
ricresciuto può essere mostruoso, doppio, potente. Siamo stati tutti feriti, in
profondità. Abbiamo bisogno di rigenerazione, non di rinascita, e le
possibilità della nostra ricostituzione includono il sogno utopico della
speranza in un mondo mostruoso senza il genere.

Le immagini possono aiutarci a esprimere due tesi cruciali a questo saggio:
primo, la produzione di teorie universali e totalizzanti è un grave errore che
esclude gran parte della realtà, e questo forse sempre, ma certamente ora; in
secondo luogo, assumersi la responsabilità delle relazioni sociali della
scienza e della tecnologia significa rifiutare una metafisica antiscientifica,
una demonologia della tecnologia, e di conseguenza significa accettare il
difficile compito di ricostruire i confini della vita quotidiana, in parziale
connessione ad altri, in comunicazione con tutte le nostre parti. Il punto non
è solo che la scienza e la tecnologia offrono all'umanità il mezzo di ottenere
grandi soddisfazioni e sono matrici di complesse dominazioni. Le immagini
cyborg possono indicarci una via di uscita dal labirinto di dualismi attraverso
i quali abbiamo spiegato a noi stessi i nostri corpi e i nostri strumenti.
Questo è il sogno non di unlinguaggio comune, ma di una potente eteroglossia
infedele. È l'immaginazione di una femminista invasata che riesce a incutere
paura nei circuiti dei supersalvatori della nuova destra. Significa costruire e
distruggere allo stesso tempo macchine, identità, categorie, relazioni, storie
spaziali.

Anche se entrambe sono intrecciate nella danza a spirale, preferisco essere
cyborg che dea.

Lavoro cyber
============

**Dal fordismo al post-fordismo**

Il nostro tempo è caratterizzato da una serie di mutazioni strutturali nei
sistemi di produzione definibile come passaggio dal sistema fordista al sistema
post-fordista.

Il *fordismo*, sistema egemone a parire dagli anni Venti del nostro secolo e che
prendeva il nome da Henry Ford, padrone dell'omonima casa automobilistica
americana, si incentrava sulla produzione di massa attraverso economie di scala
di prodotti omogenei. A livello sociale il fordismo è segnato da un compromesso
fra capitale e lavoro che vede come attori da una parte le borghesie nazionali,
dall'altra la classe operaia di fabbrica. Quest'ultima, rinunciando a pratiche
più radicali, ottiene in cambio dei massicci incrementi di produttività, un
notevole innalzamento degli standard di vita, sia attraverso un aumento dei
consumi, sia attraverso un sistema di garanzie sanitarie e pensionistiche.

Fra gli anni Settanta e gli anni Ottanta questo compromesso entra in crisi per
molteplici ragioni (evoluzione tecnologica, mondializzazione dell'economia,
crescente importanmza dei mercati finanziari) e si assiste così al passaggio
ancora fluido a un nuovo sistema produttivo chiamato *post-fordismo* che è
caratterizzato dai seguenti elementi:

- Utilizzo massiccio delle nuove tecnologie
- Produzione in piccola scala di prodotti differenziati
- I "saperi" diventano merce di scambio sul mercato del lavoro
- Il lavoro autonomo rimpiazza il lavoro salariato e dipendente
- Progressivo smantellamento dello stato sociale

Naviga negli approfondimenti:
- Economia post-fordista
- I lavoratori del post-fordismo
- La critica al post-fordismo

Economia post-fordista
----------------------

**Flash sull'economia post-fordista**

Ecco a seguire dei percorsi all'interno dell'economia post-fordista, che
possono chiarire arte del problema della mutazione di paradigma produttivo di
fine secolo e che la redazione di *"Decoder"* sta seguendo da svariati anni:

- GROUPWARE, un [articolo][27] e un [box][28], su una delle più innovative
  strategie di divisione del lavoro.
- [T.V. INTERATTIVA][29], a cura di UVLSI, illuminazioni sulla TV del futuro.
- [144: RACCONTO ORALE DI UNA LAVORATRICE VERBALE][30]
- MICROSOFT, due articoli critici (di Raf Valvola e [Gomma][31]) sul passato,
  presente e futuro della più grande azienda di software del mondo.
- SPECULAZIONI SULLA STORIA DELLA PROPRIETÀ di Doug Brent, tratto da No
  copyright, sulla "proprietà nel cyberspazio".

I lavoratori del post-fordismo
------------------------------

**Flash sulle condizioni di alcuni lavoratori del post-fordismo**

A seguire alcuni testi sulle condizioni di lavoratori post-fordisti.

- PER UNA CRITICA DELLA GENIALITÀ INDIVIDUALE, di Raf Valvola, la fine del
  sogno del programmatore americano.
- USA, I NUOVI BRACEROS SONO INGEGNERI ELETTRONICI, di Tony Reseck, sul mercato
  dei corpi e delle menti in USA e nel mondo.
- INTELLIGENZA MILITARE-ARTIFICIALE, di Tom Athanasiou, la vita di un
  ricercatore di intelligenza artificiale che lavora per l'esercito americano.

*Se sei un lavoratore post-fordista, mettiti in contatto con noi per raccontarci
la tua esperienza!*

La critica al post-fordismo
---------------------------

**Contro il capitale globale**

Globalizzazione è la parola chiave per capire come cambia l'economia mondiale.
Nella sua espansione su scala planetaria il capitale tritatutto elimina ogni
specificità delle realtà locali e nazionali: facendo leva sull'information
technology e quindi sulla possibilità di esprimere "comando capitalistico" in
tempo reale, la globalizzazione sta radicalmente cambiando i cromosomi stessi
dello sviluppo economico, orientandolo sempre più su attività di tipo
finanziario, invece che industriali.

Le grandi corporation richiedono ai governi nazionali un complesso di norme
deregolate, come parchi tecnologici con personale qualificato a costi ridotti,
condizioni fiscali estremamente vantaggiose per i profitti, flessibilità
assoluta nella gestione della forza lavoro, imposizione dei diritti di
proprietà intellettuale, il cui effetto rovisnoso sta spingendo intere società
civili a un'inarrestabile corsa "verso il basso".

Ma ci sono diverse organizzazioni che si oppongono a tutto ciò. Visitale con
noi:

- [Centro Nuovo Modello di Sviluppo/Coordinamento Nord-Sud per la dignità del lavoro][32]
- [ILC International Liaison Committee for a Workers' International][33]
- [New Internationalist (rivista)][34]
- [World Trade Organization (Wto=Omc)][35]
- [Multinational Monitor (rivista)][36]
- [Clean Clothes Campaign][37]
- [LaborNet (Igc, Institute for Global Communication)][38]


**Dentro il sistema Microsoft**

*di Gomma*, pubblicato originalmente da "Il manifesto"

Sembra che la voglia di critica nei confronti dell'industria informatica crei
identità e paghi anche a livello commerciale. Se così non fosse il signor
Andrew Schulman sarebbe considerato solo un topo da biblioteca del software e
non riuscirebbe a vendere, nei soli USA, 150.000 copie dei suoi testi e a
essere tradotto in tutto il mondo. Il suo ultimo libro Windows 95 - Dentro il
sistema, pubblicato in Italia da McGraw Hill, pp. 532, lit. 75.000 (con incluso
un dischetto per penetrare nei "segreti" di Windows 95), è un atto d'accusa
contro l'industria di programmi più potente del mondo, che trae spunto da una
minuziosa critica tecnica al nuovo sistema operativo (effettuata sulla
beta-version, in quanto la definitiva non era stata ancora stata messa in
commercio), per diventare analisi economico-strutturale dell'intera industria
del software.

Schulman è una persona dall'intelligenza moderna che ha colto le tensioni
presenti nell'immenso target degli utilizzatori di programmi e le diverse
imposizioni che costoro sono costretti a subire dalla grande industria. La
prima imposizione avviene, all'atto dell'acquisto, nei confronti degli
utilizzatori meno accorti dal punto di vista tecnico: il software si compra
sempre a scatola chiusa, ma purtroppo la sua qualità raramente corrisponde alle
aspettative generate dai media del settore, troppo influenzati dalle veline
pubblicitarie delle società produttrici. La seconda avviene alle spese dei
tecnici e dei ricercatori che si trovano nelle mani non più una tecnologia ma
un prodotto di consumo, assimilabile a qualsiasi altra merce da supermercato.
La terza forzatura viene esercitata contro i produttori più piccoli, ditte
formate da pochissime persone, una volta il "sale" di softwarelandia,
schiacciati dall'imposizione di standard e programmi "tuttofare" che
difficilmente lasceranno loro abbastanza spazio per lavorare senza continue
difficoltà di progettazione creativa e ricerca di nuovi spazi di mercato.

Ciò che viene messo a nudo da questo manuale è l'oscuro processo che sta dietro
a una delle industrie più promettenti del momento (per i grandi capitali).
Perché Wall Street ha premiato Microsoft all'annuncio che "Windows 95" sarebbe
uscito con cinque mesi di ritardo? Perché questo prodotto, è stato annunciato
come "il più sensazionale programma mai creato". Perché un software viene
spacciato come "integrato" (dovrebbe nelle promesse sostituire totalmente DOS)
quando invece utilizza ancora ampiamente funzionalità del buon vecchio DOS e
s'"accresce" solamente di utility e programmini che finora abbiamo comprato
separatamente? E cosa produrrà tutto ciò sul mercato? Perché con "Chicago", il
nome in codice del nuovo prodotto, avremo a disposizione "compresi nel prezzo"
strumenti per scrivere, per mandare posta elettronica e fax, compiere semplici
operazioni sui file, insomma tutto ciò che la "maggior parte dei clienti"
aspettava per poter usare semplicemente e al primo colpo il proprio computer.
Peccato che ogni utility che Microsoft aggiunga ai suoi sistemi operativi (vedi
la compressione nel DOS 6) faccia scattare licenziamenti presso le società che
prima producevano quelle stessa utility.

Ciò che si configura è, nelle stesse parole di Bill Gates, l'"uomo dei codici"
in tutti i sensi, la "creazione di una sorta di monopolio naturale", in cui la
Microsoft impone quasi tutte le regole del gioco, anche le più banali, come
quelle relative alla possibilità di utilizzare il logo Windows da apporre sulle
scatole di software prodotto da altre case a segnalazione della compatibilità.
Secondo Schulman la maggior parte dei requisiti richiesti per utilizzare il
logo sembra aver a che fare più con i desideri di Gates che non con quelli del
potenziale utente finale. E riguardo alla tendenza monopolistica sembra che di
"naturale" ci sia ben poco. L'autore critica a questo riguardo l'operato della
Federal Trade Commission che si è occupata per quattro anni di indagare sulle
presunte violazioni della legge antitrust da parte di Microsoft. Ciò che la
sentenza ha imposto è stato il blocco di una pratica che legava le mani ai
produttori di computer i quali, per anni, hanno dovuto pagare una sorta di
gabella a Gates per ogni processore installato sulle macchine con processori
Intel x86. Questo ha costituito il 25% del fatturato Microsoft e una sorta di
ipoteca per i produttori di hardware sulla possibilità di poter scegliere di
utilizzare sistemi operativi diversi e una chiusura delle possibilità per i
creatori di altri sistemi operativi. Ma per Schulman l'intervento della FTC è
stato troppo blando. Infatti una consuetudine di Gates, tuttora esistente e non
rilevata dalla Commissione, è quella di mantenere segrete le "funzioni non
documentate" ovvero parti di codici alla base del funzionamento del sistema
operativo la cui conoscenza permette la progettazione dei programmi che girano
sul sistema stesso: questo comporta che solo Microsoft, o chi vuole Microsoft,
possa scrivere il software che abbia una vera compatibilità.

Schulman, che già rivelò al pubblico il baco del chip del Pentium Intel e di un
altro baco di Windows 3.1, non solo rinforza la buona tradizione, purtroppo
inesistente in Italia, della critica del consumatore nei confronti del
produttore, ma grazie a 400 pagine di disamina analitica del codice di un
sistema operativo, entra anche nel "sistema" economico e strategico alla base
dell'industria informatica. Dice di esser stato, una volta, marxista e di
essere ora solo un liberal... le sue precise analisi dei "processi" ci lasciano
qualche piacevole dubbio in proposito.

Cyberpunk Psichedelico
======================

**Intro**

Con cyber psichedelico intendiamo un movimento politico-esistenziale, che ha
numerose analogie sia col cyberpunk di tipo letterario (il concetto di spazio
virtuale) sia con alcuni obiettivi propugnati dalla parte dura degli hacker:
l'istanza di democratizzazione dei dati, delle informazioni e dell'uso del
territorio sia esso digitale o "reale"(non a caso uno dei punti di riferimenti
teorici è l'Hakim Bey di TAZ). Esso ha il proprio principale centro di
irradiazione nella California, e particolarmente negli ambienti che più
attivamente hanno attraversato da protagonisti gli anni Sessanta.

Il nome di [Timothy Leary][5] è uno di questi a cui si è aggiunto colui che può
essere considerato il suo successore: Terence McKenna.

A esso si sono collegate altre situazioni, parzialmente disomogenee tra loro,
che hanno avuto una certa importanza nel far esplodere a livello di massa
fenomeni apparentemente modaioli. È il caso della tendenza musicale
neopsichedelica esplosa in [Inghilterra][39] che ha avuto in particolare nello
staff di [Fraser Clarke][40] uno dei principali ispiratori. La tendenza
inglese, iniziata verso il 1987 si è poi evoluta nella scena [raver][41] e
traveller.

Accanto a questa scena bisogna aprire una parentesi anche sulle cosiddette
[droghe cyber][42].

Scena americana
---------------

**Il sincretismo neoplatonico di Timothy Leary**

Come si segnalava poc'anzi, questo atteggiamento di misticismo derivato dal
mezzo computer si coniuga a una ben più vigorosa ispirazione teosofica, da
sempre presente in questo tipo di movimento. In particolare Timothy Leary
consciamente opera in questa direzione. Basti vedere ad esempio come parla
della nuova scienza neurologica, scoperta fin a partire dai suoi esperimenti
sugli psichedelici. La Neurologica si definisce propriamente come il "controllo
del proprio sistema nervoso da parte di ognuno". Da esso ne emerge
conseguentemente una nuova mitica concezione della natura umana, che consiste
nel leggere il microcosmo nell'individuale e quindi scoprire la più completa
visione dell'universo.

Ed è proprio in questo momento della teoria che emergono i pensieri e le
aspirazioni più profonde di Leary. Non a caso a questo punto comincia a
infervorarsi per una sorta di nuovo sincretismo religioso-filosofico che gli
avvenimenti tenderebbero a determinare. Per sincretismo s'intende
un'unione/miscelamento di differenziate visioni religiose, le quali peraltro
hanno tra loro una comune aspirazione. Ecco quindi miscelate tra loro
neognosticismo con ermetismo, neoplatonismo, alchimia, miti faustiano e
jeffersoniano. Pensieri questi che solo parzialmente possono essere avvicinati
tra loro. Alcuni di essi difatti sono proiettati, da un punto di vista di
filosofia della storia, verso l'età aurea dell'infanzia dell'umanità
(neoplatonismo, ermetismo, alcune correnti di neognosticismo), mentre altri
sono più orientati verso la costruzione del futuro, dell'uomo nuovo
(l'alchimia, Faust, Jefferson).

E' questa una contraddizione immaginativa che mina al proprio interno
profondamente le aspirazioni di Leary. Da una parte infatti vi è una torsione
verso il sacro, l'iconico, dall'altra una forte esaltazione dell'utensile,
visto quasi fosse l'uomo in "fieri".

**Il suo sincretismo**

Nell'accentuarsi di quest'ultima prospettiva, Leary in realtà oscilla tra le
due posizioni e non può fare altro che cadere in una piatta esaltazione del
progresso scientifico, visto ormai acriticamente come scientismo.

L'intera opera di Leary difatti fin a partire dai suoi primi scritti degli anni
Cinquanta è completamente intrisa di filosofia scientifica e di pragmatismo
americani. Bisogna richiamare qui che già la sua tesi di dottorato era
orientata a descrivere i comportamenti, nei termini il più possibile oggettivi.
Gli stessi studi degli anni Sessanta sulla somministrazione e le reazione
all'LSD, inizialmente rispondevano alla medesima ispirazione scientifica. Un
rischio quindi da sempre agente non solo nella riflessione di Leary, ma più in
generale nella controcultura americana (vedi l'esperienza di Hubbard).

Questa ansia positivista trova un'ulteriore conferma nel suo porsi di fronte
alla storia umana quasi fosse un processo evolutivo già predeterminato
geneticamente in partenza, che a questo punto non deve far altro, finalmente,
che compiersi.

Si congiungono finalmente i due diversi corni dell'ispirazione di Leary: una
sorta di misticismo positivista/neoplatonico. "Noi cercavamo operazionalmente
di ridefinire gli antichi insegnamenti e di offrire un neoplatonismo di tipo
sperimentale". E' per questo che Leary si accorda acriticamente al mito, al
sogno dell'individualismo americano: fatto di "Gold Rush", di anticomunismo, di
calvinismo d'importazione, ma anche di forte autointrospezione.

La nuova filosofia, a cui da sempre (anni Sessanta) tende è quindi la
risultante delle diverse ispirazioni prima descritte. Essa sarà quindi
nell'essenza scientifica, ma fantascientifica nello stile. Ecco quindi spiegato
da una parte il suo interesse per tutto il cyberpunk. Ma sarà basata sul
concetto di espansione della coscienza, della comprensione e del controllo del
sistema nervoso, temi questi derivatigli dalle esperienze psichedeliche, e poi
ritrovati nel suo enfatico approccio mistico nei confronti del computer.
Politicamente sarà un movimento individualista, ma al contempo insofferente
dell'autorità centralizzata, miope com'è verso la differenza.

Una filosofia quindi profondamente radicalborghese, quanto a diritti civili ma
saldamente a favore della proprietà: giacché, nella sua visione,
l'arricchimento è sempre possibile in questo sistema.

Il contatto con Gibson peraltro è dovuto ad altre due ragioni. Da una parte la
medesima ispirazione immaginativa rispetto al possibile utilizzo del concetto
di spazio virtuale, ma dall'altra una serie di affari economici che tra i due
sono stati nel frattempo allacciati.

Non esistono limiti rispetto all'utilizzo della spazio virtuale. Esso nella
visione un po' profetica di Leary permetterà a ognuno di spaziare all'interno
dell'intero universo dei dati, senza limiti, né costrizioni, gratuitamente.
L'eroe di *Neuromancer*, Case, sembra essere a questo punto il parto della
fantasia di entrambi.

Come si leggerà in maniera chiara negli scritti selezionati nell'antologia,
Leary sta cercando anche di sfruttare economicamente l'intuizione filosofica
dello spazio virtuale. Non a caso ha fondato la Futique alcuni anni orsono, una
casa di produzione di software oggi impegnata anche nella commercializzazione
di alcuni prodotti per la colonizzazione dello spazio virtuale. Ma impegnata
anche nel tentativo di rendere interattivi i due strumenti più potenzialmente
democratici degli anni Ottanta: il videoregistratore e il personal computer.
L'intuizione che ispira Leary è che difatti ci si stia avviando verso una
società iconica di tipo nuovo, nella quale finalmente verrà superata, seppure
con artifici, la divisione babelica tra i diversi popoli. In Leary così come in
Sterling, è infatti continuamente operante il lucido insegnamento di McLuhan.
Una lettura quindi potenzialmente rivoluzionaria, ma che necessita del classico
disvelamento per operare in maniera conclusiva.

Scena inglese
-------------

**Enciclopædia psichedelica**

Fraser Clarke è l'editore della rivista "Enciclopædia Psychedelica", edita a
Londra, in circa 3.000 copie trimestrali. Rivista questa nata con l'obiettivo
di tratteggiare tutto il sapere psichedelico conosciuto, in esattamente 100
numeri, da pubblicarsi tutti entro l'anno duemila: l'anno della nuova era! Il
primo numero della sua rivista non a caso delinea un manifesto della nuova
umanità. La stessa parola Umanità viene trasformata in WoMan, a sancire anche
dal punto linguistico l'avvenuta parificazione dei due sessi. E' un'umanità più
consapevole, tollerante, in armonia col flusso del tutto, che evita
attentamente di agire con violenza. Lo stesso Fraser Clarke non a caso è uno
dei principali organizzatori del Convoy che, in occasione del solstizio
d'estate, muove da Londra verso Stonehenge. Due anni fa sono però accaduti
violenti scontri tra la polizia e i giovani zippies, freaks, punks e cyberpunks
che erano accorsi all'appuntamento. Il divieto della Thatcher di avvicinarsi al
tempio, che quindi venne recintato, diede esca a una notte infuocata, dove
numerosi "fratelli travellers" si sono distinti per la loro tenacia nel
difendere il diritto a praticare il rito propiziatorio. Noi non sappiamo come
siano andati in realtà i fatti, quale sia stato il motivo scatenante e quale il
comportamento dei giovani freak-punk, fatto sta che nell'ottobre di quell'anno
è girata per l'Europa una lettera di Fraser Clarke, in cui spingeva per
raccogliere firme contrarie a qualsivoglia uso della violenza. Un approccio
quindi politicamente molto simile all'atteggiamento di Leary intorno ai diritti
civili. La rivista, del resto, più in generale è omologa alla produzione
intellettuale di Leary, essendone del resto una parziale derivazione. Il
soggetto che spingerà verso la nuova umanità, e che parzialmente l'anticipa, è
lo zippie: strano, ma non troppo, miscuglio di hippismo e tecnologia. La
rivista per il resto si occupa pariteticamente di annunci sulla nuova epoca,
consigli tecnici sul come industriarsi in casa l'Ecstasy, premonizioni
astrologiche sul prossimo futuro e in particolare sulla positività a livello
sociale del 1994, ecc. Da rimarcare il quarto numero dedicato interamente
all'origine dei festival e a Stonehenge, e l'ottavo e nono volume interamente
dedicati agli eroi psichedelici. Anche in questi due ultimi numeri si può
notare il tributo verso Leary, in quanto edizione accresciuta degli editoriali
biografici inclusi nel libro *Flashbacks*, sempre dello stesso Leary.

**Il computer e il mito**

Elemento però coagulante l'intera produzione di "Enciclopædia Psichedelica" è
sempre il misticismo, che trae alimento da una parte, sicuramente,
dall'esoterismo mistico e dalle potenzialità immaginative che il computer
scatena e dall'altra dall'importante influenza fortemente neoplatonica che è
sempre stata presente in questo tipo di controcultura.

Che il computer difatti induca a un approccio sostanzialmente mistico, lo si
deduce del resto anche dalla numerosa bibliografia presente sull'argomento.
Valga per tutti ad esempio l'atteggiamento di Peter Glaser, nome d'arte
Poetronic, uno scrittore programmatore di Amburgo legato alla scena ben più
politicizzata degli hacker tedeschi, il quale in una lunga intervista riportata
in *Hacker für Moskau* così si esprime: "Quando io programmo spesso mi vedo
seduto come una specie di mago Merlino con cappello a cono in testa, allora lo
schermo si trasforma in una specie di sfera di cristallo, nella quale appaiono
visioni che si possono capire, divinare attraverso formule magiche divinatorie
incomprensibili. E' veramente una specie di magia scrivere un programma lungo
sei pagine, che quando si schiaccia un bottone sullo schermo sviluppa una sfera
magica che getta un'ombra, un'ombra strana. (...) Un elemento tipico di questa
lirica arcaica dei riti magici che si ritrova anche nel computer è proprio la
ripetizione, (...) a volte posso sentire questo come una specie di tam-tam che
ossessivamente ripete 01010101." Un atteggiamento quello di Glaser che non può
essere definito come semplicemente episodico, ma molto più profondamente
coerentemente mitico, sprofondato nelle radici del tempo. "Io programmo per
ore, per giorni e per notti, e nel tempo di un fulmine attraverso i secoli e mi
vedo, senza volerlo, portare avanti le opere degli antichi sacerdoti, dei
signori del culto del fuoco, della divinità indiana, dei parsi persici che
curavano il fuoco per Zarathustra e mi vedo come custode della luce eterna. Io
so quale è la ricerca della perfezione: il programma senza errori e quello che
io sacrifico è il tempo."

Raver
-----

**Potenziale trasgressivo del rave**

*scritto da un raver illegale*

Il rave, attraverso l'esperienza dell'ebbrezza collettiva, crea un annullamento
temporaneo delle identità e dei ruoli consolidati, suggerendo la necessità di
una loro ridefinizione. Quanto più quest'esperienza viene pianificata, quindi
codificata, in modo tale da essere funzionale ad interessi anche direttamente
economici, tanto più ci avviciniamo alla categoria di rituali previsti per la
conservazione della gerarchia sociale dal sistema capitalista. Tali rituali
sociali costituiscono la risposta al bisogno insopprimibile di sfondamento
dell'individuale nell' indistinto della folla, che deve avere un suo tempo e un
suo luogo deputati e soprattutto deve essere controllato : la partita di calcio
è l'esempio più appariscente e allo stesso tempo più contradditorio, visto il
permanere di un residuo margine di imprevedibilità e di incontrollabilità. Lo
svolgimento dovrebbe rispondere a comportamenti predefiniti che vanno a
costituire da una parte le regole del gioco, dall'altra le funzioni che il
giocatore e lo spettatore sono tenuti a interpretare. Questi rituali sociali
coincidono con il momento "festivo" opposto e complementare al ciclo feriale.
La "vacanza" é il periodo festivo per eccellenza, stabilito, fissato nella sua
durata, durante il quale possiamo confonderci con gli altri, lasciarci andare
alla socievolezza, alla convivialità. A mio modo di vedere ciò che distingue il
rave illegale è proprio l'assenza di questo preciso carattere rituale, in barba
agli antropologi e ai sociologi di "tendenza" che vogliono spiegare questi
eventi attraverso concetti ammuffiti tipo neo-tribalismo, trance, sciamanesimo
e quant' altro. Il rito "primitivo" cerca di (ri)stabilire, in comunità
circoscritte, una coesione sociale e un equilibrio individuale. Sin dalle
origini della civiltà l' abolizione rituale-festiva delle differenze è servita
al rinsaldamento dell' ordine gerarchico , a rendere accettabili le differenze
stesse. Nelle complesse società occidentali di oggi l'edonismo è la forma
rituale attenuata ,"laica". Il techno-party gestito orizzontalmente agisce
precisamente e conflittualmente su questo terreno tentando di sfondare la
nozione di festa, immettendole elementi di rottura , di critica , di
sperimentazione. Il momento ludico diventa capace di creare disordine e crisi
nello stesso apparato che gestisce il divertimento.L'allarmismo isterico degli
operatori delle discoteche (vedi il signor Bornigia, patron del Piper e di
altri locali romani),che temono di perdere clienti mentre dicono di
preoccuparsi per la sicurezza e la salute dei giovani,lo dimostra
sufficientemente.

Mentre il rave commerciale può essere paragonato ad una vacanza in un villaggio
turistico o comunque pre-organizzata, un consumo del tempo libero, il rave
illegale rappresenta una sorta di vacanza fai-da-te, nel senso che comporta
un'attitudine dei viaggiatori all'esplorazione. Da un punto di vista più
strettamente economico, il rave commerciale può avvicinarsi ad una ipotetica
"società a responsabilità limitata", quello illegale ad un altrettanto
ipotetica "società a responsabilità illimitata". Ciò non vuol dire che la scena
illegale e quella commerciale rimangano come due mondi separati non
comunicanti: al di là di queste distinzioni, il rave in generale costituisce un
intreccio complesso e mutevole di implicazioni sociali, politiche, culturali e
non solo, come spazio e tempo privilegiati della mescolanza, della
trasversalità culturale, è irriducibile a qualsivoglia tentativo di indagine o
analisi specialistica e allo stesso tempo la sua complessità fa sì che
costituisca comunque un terreno sul quale convergono stimoli e interessi
(diversi), spesso lontani fra loro, se non in conflitto.

La scena techno si è sviluppata seguendo direzioni inedite e imprevedibili se
confrontate con le storie recenti - e ancora in atto - di altri fenomeni
musicali. E possibile individuare, durante gli anni '80, correnti specifiche e
collegarle a contesti, a luoghi altrettanto specifici. Sicuramente il punk ha
rappresentato una attitudine nei confronti della musica che comportava la
rivendicazione di una scelta esistenziale - tale scelta si manifestò in un
primo tempo attraverso la provocazione come tentativo di rendersi visibili (le
irruzioni nello show-business), più tardi attraverso la conflittualità come
azione sociale, sia offensiva che difensiva. L'urgenza di difendere il proprio
stile di vita evidentemente è stata prioritaria fino ad ora: il movimento del
'77 e le successive esperienze delle occupazioni, dei centri sociali, hanno
certamente un legame forte con il punk come cultura underground. Il termine
"underground" può essere utile per definire situazioni che si muovono
all'interno di queste esigenze opposte e complementari: da una parte uscire
fuori, dall'altra preservare la propria identità, sopravvivere con la propria
diversità. Aprirsi o chiudersi.

Il successo mondiale di "Never mind" dei Nirvana (1991) sigla ufficialmente
l'ingresso del punk, nelle sue potenzialità di prodotto commerciale, nel "main
stream" del pop con ben 14 o 15 anni di ritardo: solo che ora si chiama
"grunge" ed entra nel rumore massmediologico come "nuova tendenza giovanile" e
nel mercato discografico, come categoria musicale che costituisce un preciso
settore di vendita il cui consumatore è identificabile mediante sofisticate
strategie di indagine di mercato. Tuttora il genere "funziona", soprattuto la
sua versione più scanzonata e scacciapensieri, vista la popolarità di gruppi
come Green Day, Offspring, No Fx ecc. . "L'impiego quotidiano della musica come
colonna sonora commerciale" fa sì che "per l'importanza della musica popolare
il nostro secolo è inscindibilmente legato all'uso di essa da parte degli altri
mass media - radio, cinema, tv, video - cosicchè l'organizzazione ed il
regolamento di questi plasmano le possibilità del pop". 1 Il sistema
capitalistico ha imparato molto bene l'arte del riciclaggio ed è il primo a
decontestualizzare ed a cambiare di segno la musica. I prodotti musicali devono
fare i conti con il loro possibile ri-uso a scopo commerciale: almeno
potenzialmente, una volta uscito un disco qualsiasi ha delle concrete
possibilità di venire utilizzato come sottofondo.Gli spots dei servizi sportivi
di Tele Più 2 sono commentati musicalmente con il meglio di quella che viene
definita "scena alternativa". Oggi questo processo si è velocizzato(il punk era
particolarmente indigesto,di conseguenza la sua metabolizzazione è stata più
lenta) e riguarda tutta la musica, per questo mi sembra oramai impossibile
distinguere fra pop e avanguardia. Un aspetto che distingue alcune vicende
musicali degli anni '80 è proprio (come nel caso stesso dei Nirvana)
l'involontarietà, autentica o simulata che sia, il disinteresse dimostrato, a
volte ostentato dall' "artista" rispetto al proprio successo, alla propria
popolarità. Ancora oltre, rappers come Snoop Doggy Dog hanno costruito la loro
fortuna sull' immagine da criminali (che apparentemente si rivolgono ad altri
criminali). In un certo senso più si comunica con un pubblico ristretto
cosicchè esso si possa identificare e sentire un'appartenenza, più è facile che
in un breve volgere di tempo si crei una categoria, una tendenza identificabile
e utilizzabile commercialmente. L'autenticità e la simulazione finiscono così
per essere difficilmente distinguibili.

Se per "fenomeno musicale" si intende appunto una categoria o un genere
musicale che vende dischi e che sia riferibile ad un contesto preciso, la scena
techno non può definirsi tale: il suo sviluppo, la sua crescita, il suo
"successo", non dipendono dalle vendite dei dischi, dal fatto di rappresentare
un buon prodotto commerciale; inoltre non è possibile identificare in maniera
precisa i suoi acquirenti, tantomeno i suoi luoghi deputati in quanto essi
vengono cercati, creati, oppure semplicemente vissuti di volta in volta in modo
diverso. Un rave in una discoteca è quasi una contraddizione di termini.

**Il raver non è un fan**

Il raver cioè non è (ancora) identificabile come acquirente da strategie di
mercato.

Non c'è dubbio che senza ravers, i Djs e i gestori dei locali da ballo, non
avrebbero guadagnato così tanto denaro; si potrebbe dire lo stesso per una
qualsiasi band di successo senza i suoi fans. Ma il fan appunto, come
affezionato dell'artista o di un determinato genere musicale, è in primo luogo
un'acquirente di dischi. L'house, la techno e tutte le sottoetichette
rappresentano un settore in controtendenza del mercato discografico. Mentre si
è ormai da tempo imposto il CD come supporto-formato privilegiato, i mix di
musica techno vengono venduti prevalentemente in vinile; questi non sono
facilmente reperibili perchè il numero di copie stampate è piuttosto basso in
quanto costituiscono un "esclusivo" strumento di lavoro per i Djs, i quali
anche quando sono essi stessi produttori di musica, devono fondamentalmente
lavorare dal vivo per esprimersi al meglio. Per il pubblico più vasto ci sono
le compilations in CD pre- mixate dai Djs di grido, i singoli brani sono spesso
accorciati secondo uno standard di durata medio, in modo da fornire la colonna
sonora di un eventuale mini rave casalingo. Gli albums sono più facilmente
reperibili,ma non suonano forte e potente come i singoli su vinile . La
produzione di musica è legata al contesto della pista da ballo, situazione
riproducibile all'infinito anche a livello immaginativo: ciò che conta é
l'immediatezza dell'efficacia ritmica delle tracce sonore,anche per le versioni
più estreme e rumoriste di techno.La cassa della batteria elettronica è
l'elemento di continuità,il perno di questa musica. Un'altra fonte di
diffusione e di guadagno sono i passaggi radiofonici : molte radio in italia
hanno smesso di essere contenitori musicali per specializzarsi , facendo
condurre le trasmissioni ai Djs più quotati del momento.

In secondo luogo il fan è uno spettatore, mentre il raver non è assimilabile
allo spettatore del concerto; il rave rappresenta il superamento della
performance "live", la quale prevede la presenza di uno o più protagonisti sul
palco e di un pubblico che si gusta l'esecuzione. L'esecuzione musicale assume
durante il rave valenze del tutto diverse, il Dj manipola musica registrata, di
solito la consolle è molto più "nascosta" rispetto al palco, la folla non si è
radunata per guardare verso un' unica direzione, semmai per guardarsi, quindi
per farsi guardare.

Tradizionalmente la canzone è costruita formalmente attorno al testo,
assegnando così un ruolo predominante alla comunicazione verbale. La parola ha
però assunto sempre più valore di puro suono: la tecnologia elettronica
permette di ricreare, di campionare, di decontestualizzare la voce umana e
nella techno la parola è andata progressivamente scomparendo, favorendo lo
sviluppo di modalità comunicative meno facilmente definibili . Nel 1990 in un
pezzo prodotto a Londra ( " Maggie's last party ", Very Important Minister) uno
degli ultimi sproloqui come ministro in carica di Margareth Thachter contro gli
acid-parties veniva campionato, spezzato, ripetuto diventando un eccitante
sproloquio a favore delle feste illegali.

La condivisione dell'eccitazione (determinata dalla prestazione dei musicisti)
tipica della situazione del concerto si trasforma nel rave in un moltiplicarsi
esponenziale di inputs, di stimoli che si riproducono; ognuno è protagonista
ricevendo e trasmettendo energia. In questo contesto la musica finisce per
costituire un sottofondo. Musica di sottofondo che esplode,musica immaginifica,
portatrice di immagini, capace di lasciare un maggiore margine creativo alla
fantasia del corpo e della mente di chi ascolta,aiutato dal supporto di uno
spazio sonoro.

Le colonne sonore dei films, degli spots pubblicitari televisivi e radiofonici,
i motivetti elettronici dei videogames, la musica (muzak) per ascensori, per
supermercati, per piscine e quant'altro, hanno un referente obbligato: servono
ad accompagnare un prodotto commerciabile o un momento che rientra nella
gestione dell' attività produttiva (la stessa new age che sembra fatta apposta
per liberi professionisti giovanili e stressati). Affrontare la valutazione dei
margini residui di creatività artistica è un'altra questione, comunque credo
che la colonna sonora di un film è tanto più evocativa quanto meno è vincolata
alle immagini, agendo cioè parallelamente, in contrappunto rispetto ad esse.

Il riciclaggio, la frammentazione di questi suoni , dall'avvento del
campionatore e ancora prima delle tecniche di "taglia e cuci"con i giradischi e
i mixer, dà loro nuova vita:se accostati a rumori ambientali e appoggiati sopra
una scansione ritmica predominante nella sua costanza,operano in maniera più
sotterrana , creando effetti di feedback emotivo spesso subliminale. Tutti i
suoni sono utilizzabili.La diffusione di tecnologia,anche quella più "bassa",
costituisce un'occasione di vendetta e di espressione per l'ascoltatore.

Il rave rappresenta a suo modo la realizzazione di ipotesi di ricerche da lungo
tempo sperimentate.

Mentre i compositori del passato intendevano comunicare un determinata
impressione estetica, e per farlo miravano alla chiarezza, subordinando il
dettaglio alla melodia e al ritmo di ampio movimento, con un apporto
accuratamente sfumato tra certezze e ambiguità, gli autori d'avanguardia
preferiscono la saturazione e la prolissità dei fenomeni musicali con la
finalità di cancellare le proprie tracce e quindi di creare quello che può
essere definito un effetto magico. Questa musica va percepita istantaneamente
in uno stato di shock creato da alterazioni rapide o in stati quasi onirici
creati dall'estensione apparentemente senza fine di schemi pressochè identici
che si ripetono costantemente. La musica deve circondare costantemente
l'ascoltatore, eliminando così il divario convenzionale tra mittente e
destinatario. Alcuni ritengono che il suo effetto sia migliore nella musica
registrata, con gli alti livelli sonori e gli altoparlanti di alta qualità
impiegati, che estendono (e talvolta trasformano) le posizioni e la
distribuzione dei suoni. L'ascoltatore è dunque materialmente immerso nel
suono. Non è neppure richiesta un'attenzione esclusiva: il compositore spera di
creare una nuova comunità, forse un nuovo mondo, e non di trasmettere
informazioni specifiche.

*Alexander Goehr*

compositore inglese, insegnante della facoltà di musica dell'Università di
Cambridge. In questo passo viene testimoniato uno spostamento di interesse da
parte del compositore: il suo sforzo non è più volto ad esprimere pienamente il
proprio talento secondo codici consolidati, piuttosto la ricerca è volta a
creare i presupposti per coinvolgere l'ascoltatore in un'esperienza fisica e
quindi più immediatamente emotiva. In senso più generale, l'attegiamento delle
avanguardie artistiche di questo secolo mostra la necessità di superare la
concezione aristotelica /occidentale dell'arte: l'opera d'arte intesa come
mimesi, imitazione - rispecchiamento di un riflesso - porzione di realtà,
rappresentazione compiuta nel suo sviluppo preordinato dall'autore, di fronte
alla quale il pubblico non può far altro che porsi come spettatore.

Tale necessità testimonia la consapevolezza del fatto che tutta l'arte è
diventata prodotto, un prodotto atto ad allietare, o a lenire il dolore, della
società dei consumi e dello spettacolo. In altri termini l'esperienza estetica
non è più grado di fornire un'esperienza reale anche per un problema, diciamo
così, congenito: l'attenzione, l'immedesimazione, lo stesso trasporto emotivo,
poggiano sulla consapevolezza che questa esperienza non può turbare più di
tanto il nostro fragile equilibrio, soprattutto fisico. Siamo insomma al sicuro
da sorprese.Inoltre la fruizione dell'opera d'arte è individuale.

La minimal music, la generazione successiva a quella di John Cage, (P. Glass,
T. Riley, S. Reich, ed altri ) tenendo presente le ricerche sul suono dalle
sperimentazioni dei futuristi (l'intonarumori di Luigi Russolo per esempio) in
avanti, ridefiniva gli schemi musicali usando la tecnologia elettronica
privilegiando la ripetizione, la variazione intesa come alternanza anche
casuale di entrata e uscita delle singole parti, senza interessarsi alla
costruzione-progressione coerente di un tema melodico. L'intento era quello di
fornire un ambiente sonoro all'interno del quale muoversi, spostarsi,
esplorare, fare esperienza immediata. Da un punto di vista strutturale sono
innegabili le influenze delle culture musicali tradizionali o extra-occidentali
legate alla danza, ai fenomeni della trance, dell'esperienza estatica, del
rituale. Le cerimonie di cui si interessava l'antropologia durante gli anni '60
e '70, il fatto che mostrassero una coincidenza fra il momento del rituale e
quello dello spettacolo qualificandosi come qualcosa di più profondo
dell'intrattenimento, spinsero gruppi come il Living Theatre, il Performance
Group, sperimentatori come Allan Kaprow, a creare eventi, happenings aperti,
passibili di sviluppi diversi, capaci di situarsi sulla soglia, sul limite fra
il "per finta" e il "per davvero" e di irrompere nella consuetudine quotidiana
mettendola beneficamente in crisi. La scommessa era coinvolgere lo spettatore
in una performance collettiva che gli permettesse di ritrovare e riprovare la
dimensione corporea e ludica, di sperimentare una trasformazione in
atto.------Il legame di queste ricerche con i rituali tradizionali non è
ovviamente un legame di continuità in quanto tali manifestazioni culturali,come
ho già detto, hanno il fine di (ri)stabilire la coesione sociale di determinate
comunità seguendo un preciso percorso simbolico.L'evento performativo, oltre a
svolgersi per lo più in contesti urbani , ha un carattere sperimentale non
prevedibile.--------

Il rave, rispetto a questa evoluzione dell'evento spettacolare come momento di
aggregazione partecipe, aggiunge un elemento più dichiaratamente edonistico -
la danza come ricerca del piacere estatico,collettivo. La cultura techno si è
abilmente inserita in un settore ben collaudato dell'industria del
divertimento: la scena "dance".Gli operatori delle discoteche italiane hanno
immediatamente capito che dovevano appropriarsi di questa tendenza prima di
altri per poterla sfruttare al meglio. Così è stato: a parte la riviera
romagnola, quella veneta e quella toscana,ben fornite di strutture adatte,c'era
bisogno di spazi sempre più grandi, così hanno cominciato ad affittare edifici
industriali, tendoni, megabalere, decentrandosi rispetto al contesto urbano.
Nell' estate del '91 a Roma fu organizzato un rave alla Città del Mobile di
nonno Ugo Rossetti, lo stesso spassoso personaggio che da un emittente privata
proponeva graziose "cammerette" per ragazzi ed eleggeva ogni anno "la donna più
bella del mondo"( fu insignita del titolo anche Moana Pozzi, all'inizio della
sua carriera). Fascisti riciclati come Chicco Furlotti erano e sono tuttora i
promotori di rave legali: costoro pretendono di rassicurare le mamme apprensive
con l'affermazione della loro professionalità, parlano di anti-proibizionismo
in materia di orari di chiusura ma si guardano bene dal farlo riguardo alle
droghe. Tale perbenismo ipocrita fa sì che in questi locali sia molto più
complicato fumarsi una canna in santa pace che consumare trip o ecstasy.Queste
droghe sono relativamente nuove, ma si differenziano per la loro praticità: non
richiedono cioè alcun rituale di preparazione,possono essere semplicemente
ingerite come un qualsiasi psicofarmaco risultando così meno visibili.Su questa
consapevolezza contano gli organizzatori per rivendicare una legalità di
facciata che garantisce loro lauti guadagni. Alcuni locali notturni celebrano
il rito della trasgressione riproponendo la gerarchia sociale della
quotidianeità: nei lussuosi priveè vengono ospitati gratuitamente vip freschi e
riposati che sorseggiano i loro drinks e magari consumano droghe di ottima
qualità, mentre nell' arena pischelli che hanno pagato lire 50.000 di entrata
spesso e volentieri si scannano fra loro. Se si aggiunge un apparato repressivo
di buttafuori culturisti pronti a punire qualsiasi eccesso si capisce bene come
questi luoghi siano pieni di violenza e conflittualità a stento controllate.La
vendita indiscriminata di superalcolici (che malissimo si accoppiano con le
droghe sopra citate) testimonia il disinteresse totale per il benessere dei
partecipanti. L'atmosfera di sensualità liberatoria propagandata consiste in
una squallida esibizione (retribuita) di stereotipi erotici degna delle veline
di "Striscia la notizia", accentuando la frustrazione.

Per contro la condivisione di responsabiltà dei partecipanti ad un rave
illegale crea un atmosfera che realmente si può dire empatica.L'abolizione
delle differenze riesce a concretizzarsi , sospesa ad un filo ma tangibile.

Alle mie orecchie la stessa musica suona diversa, a seconda che io mi ritrovi
ad un illegale o ad un commerciale. É l'utilizzo che cambia le cose.

Da un punto di vista musicale la techno fa esplodere il concetto stesso di pop,
tutto questo continuo rumore di sottofondo commerciale, comunicativo,
industriale, urbano, creando uno spazio sonoro che ci fa compiere un
accellerazione temporale portandoci in un futuro prossimo, testando i nostri
livelli di tolleranza in previsione di un' ulteriore esasperazione dei ritmi di
vita. Il piacere è determinato dal propellente ritmico.

L' armonia, la melodia, il groove, che ancora rimanevano nell'house sono quasi
spariti. Il caos ricostruito tecnologicamente e ordinato ritmicamente in modo
da imparare a muoversi meglio all' interno di esso. L'impulso ritmico
incessante crea una tensione che non si risolve ed è questo che ci procura
piacere, come in un interminabile preliminare amoroso .

Arthur Kessler, "The ghost in the machine", 1975?

"We are best at bashing each other's brains out. And since that's no longer
appropriate - if it ever was - we need a drug to make us able to live in ways
that we must live togheter if we're going to have cities of 10 million people
and a global civilization".

[Leggi il racconto orale di un raver inglese.][43]

**Links**

- [Global Rave Informational Database][44]
- [The alt.rave FAQ][45]
- [Raves & regional mailing list archives][46]
- [Raveland Story][47]
- [Techno][48]
- [Goa & Psychedelic Trance][49]
- [Thigpen's Techno Music Page][50]

Droghe cyber
------------

**Intro**

La sintesi chimica delle sostanze ha aperto sin dagli anni Sessanta orizzonti
amplissimi per la creazione di droghe atte ad alterare gli stati di coscienza.

Il laboratorio diventa quindi un sostituto della natura, che finora è stata
l'unico artefice per la produzione di questo tipo di sostanze.

La tendenza sembra per lo più quella di creare droghe con effetti di breve
durata e controllabili, come nel caso dell'[ecstasy][51], oppure una sorta di
anti-droghe, dette [smart-drugs][52], che servono più che altro a recuperare
energie interne mnemoniche o fisiche. Vi sono in realtà sostanze più potenti,
come la [ketamina][53], che pare essere abbastanza usata nella scena rave
inglese, che è da considerarsi un allucinogeno molto potente.

In laboratorio vengono anche prodotte le cosiddette designer drugs, ovvero
sostanze che alterano lo stato di coscienza, ma attraverso composti non
illegali, quindi produttori e consumatori di tali droghe non possono essere
puniti.

**Link interessanti**

*N.B.  Questa pagina e i suoi link sono da considerarsi esclusivamente un
supporto di tipo informativo-scientifico e non un'induzione al consumo delle
sostanze segnalate.*

- [The Hyperreal Drugs Archives][54]
- [Just the FAQs, Ma'am][55]
- [Hyper-PIHKAL][56]
- [E is for Ecstasy][57]

Arte cyberpunk
==============

**Intro**

Scenari e immaginari cyber si ritrovano non solo in letteratura, ma anche in
altri campi artistici ed espressivi. Non si tratta di banali ambientazioni
simil-BladeRunner, ma di riflessioni su un cambiamento epocale avvertito da
sensibilità spesso radicali.

Questa mutazione ha agito su:

- Cinema
- Musica
- Teatro - Performance
- Grafica

Cinema
------

**Intro**

Che fine farà la nostra anima quando Chris Winter della Brithish Telecom la
frantumerà nel silicio di un chip? La notizia di un grosso investimento per lo
studio del microchip dell'immortalità, il "soul catcher", capace di
immagazzinare in presa diretta i pensieri e le sensazioni dell'intera vita d'un
uomo è di pochi mesi fa. Come previsto, l'invasività di una tecnologia ad
altissima densità aumenta a vista d'occhio, una rivoluzione biotecnocibernetica
che sta mutando gli esseri umani. Situazioni e scenari ben noti a chi frequenta
le sale cinematografiche o, ancor meglio, il magmatico mercato dell'home-video,
che da qualche anno sforna sempre più di frequente film basati sulle situazioni
e le idee dei romanzieri cyberpunk. Action-movies prodotti in serie per
l'intrattenimento, che pur nelle ristrettezze di opere spesso poco brillanti,
non tradiscono la visione di fondo di questi scrittori che hanno scommesso su
un mondo (futuro?) sempre più hardwarizzato e softwarizzato, dove la carne è il
circuito stampato sul quale innestare metallo e dare energia. Così, da Tetsuo
in poi, l'uomo ha confuso i muscoli con il ferro e l'acciaio, cyborghizzando
sempre più il suo corpo con protesi ed escrescenze metalliche, a mano a mano
assorbite e digerite al punto da non sapere quanto resti di umano. Un cinema,
quello cyber, che mette al centro di tutto computer, controllo
dell'informazione e potere temporale esercitato con ogni tipo di androidi,
cyborg e replicanti, esseri umani spinti verso l'omologazione con le macchine o
verso l'estinzione. I registi, dai più ai meno conosciuti, si divertono a
comporre affascinanti panoramiche su mondi virtuali e vertiginosi tuffi nella
virtual reality, in città stato dalla dubbia transnazionalità, a mettere sullo
sfondo delle loro avventure la frantumazione di paesi e frontiere e che ai
governi si sono sostituite potentissime Zaibatsu. 

Ciò che era nei libri vive sull'immacolato candore dello schermo e nel
traslucido vetro dei televisori. Gli indovini credevano molto al potere
divinatorio del cristallo, e osservandone le profondità prevedevano l'avvenire,
come noi quando guardiamo la superficie di un monitor.

Quindi intendiamo per cinema cyber quei film ove la tecnologia si è fatta
quotidianità, dove la protesi è normalità, dove la macchina ha mutato
definitivamente la struttura corporea.

Ecco un'indicativa lista curata da *Roberto S. Tanzi* e *Gianluigi Negri* che
stanno preparando una pubblicazione sull'argomento:

- *Liquid Sky* di Slava Tsukerman (1982)
- *Tron* di Steven Lisberger (1982)
- *Videodrome* di David Cronenberg (1982)
- *Blade Runner* di Ridley Scott (1982)
- *Wargames* di John Badham (1983)
- *Decoder - Il film* di Klaus Maeck (1984)
- *Terminator* di James Cameron (1984)
- *Max Headroom* di Rocky Morton e Annabel Jankel (1985)
- *Aliens - Scontro Finale* di James Cameron (1986)
- *Robocop* di Paul Verhoeven (1987)
- *Split* di Chris Shaw (1988)
- *Misteriose Forme di Vita* di T.C. Blake (1988)
- *Akira* di Katsuhiro Otomo (1989)
- *Cyborg* di Albert Pyun (1989)
- *Sotto Shock* di Wes Craven (1989)
- *Tetsuo - L'uomo d'acciaio* di Shinya Tsukamoto (1989)
- *Cyberpunk videozine vol. I* Shake Edizioni (1990)
- *Robocop 2* di Irvin Kershner (1990)
- *Hardware* di Richard Stanley (1990)
- *Atto di forza* di Paul Verhoeven (1990)
- *Terminator 2* di James Cameron (1991)
- *Mindwarp - Futuro virtuale* di Steve Barnett (1991)
- *W.S. Burroughs - Commissioner of Sewers* di Kalus Maeck (1991)
- *Cyberpunk videozine vol.II* Shake Edizioni (1991)
- *Il Pasto Nudo* di David Cronenberg (1991)
- *Fino alla fine del mondo* di Wim Wenders (1991)
- *Il Tagliaerbe* di Brett Leonard (1992)
- *Tetsuo II - Martello di carne* di Shinya Tsukamoto (1992)
- *Killer Machine* di Rachel Talalay (1993)
- *Robocop 3* di Fred Dekker (1994)
- *Rivelazioni* Barry Levinson (1994)
- *Cyborg: La Vendetta - Nemesis* di Albert Pyun (1995)
- *Nemesis II - Cyborg Terminator 3* di Albert Pyun (1995)
- *Cybertech PD* di Rick King (1995)
- *Johnny Mnemonic* di Robert Longo (1995)
- *Strange Days* di Kathryn Bigelow (1995)
- *Ghost in the Shell* di Mamoru Oshii (1995)
- *La Città dei Bambini Perduti* di Jean Pierre Jeunet e Marc Caro (1995)
- *Il Tagliaerbe II* di Farhad Mann (1995)
- *Hackers* di Ian Softley (1996)
- *Timothy Leary's Dead* di Paul Davis (1996)
- *The Net* di Irwin Winkler (1996)
- *Sleepstream* di Steven Lisberger

Musica
------

**Intro**

Si dice che il primo inserto di elettronica in una canzone sia stato *Good
Vibrations* dei Beach Boys, ma sarà quello il primo vagito del cyber musicale?

In realtà è molto difficile dare una definizione di cybermusic, proprio perché
lo stesso cyberpunk contiene nella sua radice il riferimento a una musica che
non è elettronica (il punk, tirato in mezzo per il suo potenziale trasgressivo,
più che per sonorità del futuro) e che non tutta la musica elettronica è
chiaramente cyberpunk.

A seguire dei percorsi di riflessione su:

- La musica techno (Intervista ai Grey Area)
- Clock DVA (intervista ad uno dei gruppi chiaramente cyberpunk)
- Musica e copyright e Campionare musica (uno sguardo teorico e pratico su come
  il digitale ha cambiato il modo di fare musica) (NB: articolo mancante)

**Intervista au Grey Area**

*di Gomma*  
tratto da [DECODER #7][58]

> Della musica techno hanno paura in molti a parlare, forse perché troppo
> post-modernamente vuota o perché sound amato dalla teppa di strada ovvero
> perché fa sballare eccessivamente. Le riviste underground italiane non ne
> hanno mai parlato, più una radio è "di sinistra" e meno la fa sentire (e le
> radio commerciali godono). Tuttavia esistono sicuramemte motivazioni
> culturali tali per cui tale forma musicale si è diffusa sotto la pelle di
> migliaia di giovani nel mondo ed è diventata il "tam-tam" del rito estatico
> collettivo della danza e dei rave trasformandosi essa stessa in una "nuova
> droga", nel rinato spauracchio del quieto vivere familiare terrorizzato dalle
> "stragi del sabato sera". Nella piena convinzione che l'Apocalisse Cyber è in
> pieno corso, Decoder è andato alla ricerca dei Grey Area (un duo, Stefano e
> Fred che hanno anche costituito l'etichetta indipendente Evolution), uno tra
> i più intelligenti gruppi della pur consistente scena italiana, per scoprire
> quali siano gli strumenti usati per costruire questa musica "fatta in casa",
> le sue radici e le relazioni con l'"house music", il rapporto con il business
> e il fantasma del rock'n'roll.

*Che strumenti usate per suonare e come funzionano?*

*Stefano*: Abbiamo iniziato dai primi computerini come il "Commodore 64", il
"CX 5" fino a arrivare all'Atari con i vari programmi che usiamo oggi come il
"Pro 24" e il "Q Base". L'Atari è comodo perché ha un'interfaccia "midi"
incorporata, sugli altri computer la devi aggiungere. Questo sistema midi è la
cosa importante e rivoluzionaria, perché è una interfaccia universale per tutti
gli strumenti che permette di avere un controllo centralizzato dal computer
sugli strumenti stessi, che possono essere campionatori, tastiere, batterie
elettroniche ecc. Usiamo anche strumenti analogici che danno dei bassi molto
subsonici e i campionatori che ti permettono di "rubare" voci o altre fonti
sonore o di manipolare anche le tue idee. Le tastiere sono fondamentali per
inserire la musica dentro nel computer, altrimenti dovresti conoscere la musica
e comporla attraverso la tastiera del computer. Quella che usiamo è tecnologia
povera. Se hai una buona conoscenza delle macchine oggi, con una spesa tra i 5
e i 10 milioni, puoi fare dischi.

*Perché usate macchine elettroniche?*

*Fred*: Per comodità, opportunismo e per un odio profondo per tutto ciò che era
strumento tradizionale. La cultura del musicista classico vuole che tu abbia
studiato la musica, che tu sappia suonare perfettamente il tuo strumento per
poter fare assoli, per creare chissàcosa. Noi ci siamo avvicinati agli
strumenti elettronici perché eravamo vicino a certe necessità a livello
ideologico, a livello pratico perché era l'unica possibilità per staccarsi da
una cultura rock che è andata avanti per trent'anni. Noi non vogliamo scrivere
canzoni pop o rock.  
*Stef*: Però quando abbiamo iniziato, dodici anni fa, queste cose le abbiamo
fatte in gruppi punk o post punk, è stato un graduale arrivare al rifiuto degli
strumenti tradizionali per avvicinarsi a questi strumenti alternativi come
computer, sintetizzatori, campionatori...  
*Fred*: É stato il punk a suscitare in noi l'interesse per fare musica: la
musica pre-77 era soprattutto rock sinfonico a solo beneficio di musicisti
diplomati al conservatorio che facevano del virtuosismo la loro bandiera. Ma
dopo il punk è cambiato tutto e noi abbiamo cercato di andare sempre avanti e
di cercare nuovi stimoli anche perché abbiamo notato che, in campo musicale,
qui in Italia, nonostante tutto, la scena non si era evoluta molto. Cerchiamo
costantemente di crearci nuovi orizzonti come "musicanti". Negli anni Ottanta
c'è stata un'altra evoluzione: la possibilità di accedere alla tecnologia che è
diventata sempre più alla portata di tutti. Questo però non deve far pensare
che fare musica techno o dance sia così facile. La macchina fa solo quello che
l'individuo gli dice di fare. Ci vuole di base la creatività e la voglia di
dire qualcosa. Per noi questa voglia è partita dall'amore per certe sonorità:
Kraftwerk, D.A.F., Front 242, l'acid-house, la techno di Detroit e l'house di
Chicago. Tutte influenze che sono confluite naturalmente nel nostro modo di far
musica e nelle nostre idee sulla musicha: lontani dalla tradizione di mercato
della casa discografica e contro l'idea di artista da commercializzare, da
vendere in formato video-tape. Per noi non ci sono artisti, la musica parla da
sola, è il disco che, se vende, vende per le sue qualità e non per il nome
dell'artista. Oggi la musica che funziona in discoteca è fatta da gente che non
ha interesse a esporsi, a farsi ritrarre in copertina per farsi notare, ma
vuole farsi notare solo per la musica.

*Come mai la dance-music è diventata, o pare essere diventata, undeground?*

*Fred*: Alla base c'è un rifiuto dei media tradizionali. Il rock è e resterà il
grande business delle case discografiche. La dance invece nei negozi occupa gli
angolini, non è di massa. Se poi ci sono dei successi da discoteca, questo
avviene perché il pubblico che ascolta rock va in discoteca e si innamora di un
pezzo, ma succede raramente. A livello mondiale un disco di dance vende
10.000-15.000 copie, non di più, a livello italiano circa sulle 1.000. I negozi
che li vendono sono negozi strani, quelli che non trovi sulle guide
consigliate. È una musica che va per la sua strada, che se entra nel circuito
ufficiale viene recepita con non meno di un anno di distanza dall'uscita.
Quando la dance va in classifica nell'undeground c'è già una nuova tendenza e
quel disco è già stato dimenticato. La tendenza all'evoluzione è forte.  

*Stef*: Tutte le nuove idee vengono gestite da piccole etichette indipendenti e
underground. Quando la musica non è più underground entrano le major e diventa
pop.  

*Fred*: All'inizio degli anni Ottanta, dopo la fine della disco-music, in
discoteca c'è stato un buco pazzesco e andavano gruppi pop come gli Spandau
Ballet. Parallelamente esistevano in Europa gruppi elettronici duri che sono
arrivati all'orecchio di alcuni D.J.'s afro-americani i quali hanno iniziato a
suonare questo tipo di musica europea, fredda, che era così lontana dalle loro
origini. Così, nell'84-85, sono usciti i primi dischi di house come quelli di
Marshall Jefferson, Larry Herd, Derek Main, Juan Hatkins che nell'82 aveva un
gruppo che si chiamava "Cybotron", Model 500, Future, Mister Finger: erano
tutti neri e facevano cose da fantascienza. Questa contaminazione di due
generi: come la musica elettronica europea con una impostazione ritmica funky
ha generato la musica house. Si chiama "house" perché venivano date delle feste
occasionali in scantinati o case, organizzate alla spicciolate dove dai D.J.'s
veniva suonata questa musica che altrimenti in discoteca non avrebbero potuto
mettere. Questa comunque è una delle tante definizioni, credo la più vera
perché è la più scalcinata, poi ognuno dà la sua. Del resto anche i dischi
stessi vengono fatti "in casa" con una strumentazione di fortuna, prestata o
noleggiata. Questo ha creato una vera e propria rivoluzione a livello della
produzione della musica. Fino a quel momento la musica da discoteca stagnava,
era un ripescaggio di cose vecchie, un continuo deja vù. Oggi invece anche
nessun amante della musica elettronica può restare incontaminato dalle sonorità
dell'house. Quando ho iniziato a sentire i primi dischi ho pensato subito che
fosse l'unico genere rivoluzionario musicale degli anni Ottanta. Perché house
vuol dire musicalmente un sacco di cose: puoi fare tecno, garage, c'è la deep
house, sono nati parallelamente molti modi di fare questo tipo di musica che
influenzano tutta la scena musicale. Viene anche usata come musica
tradizionale, o per fare jingle in televisione, ma poi alla fin fine la cosa la
devi vedere in determinate discoteche e non ovunque. Se uno sta attento e non
si ferma sul passato ma guarda a quello che sta accadendendo nel presente
orientandosi verso il futuro, vede sempre una costante evoluzione in questa
scena perché c'è della gente che vuole andare sempre avanti. Se ti fermi, dopo
aver scoperto la ricetta per fare soldi e fai venti dischi uno sulla falsariga
dell'altro, allora sei finito.

*Quali sono le altre scene che chiamate "parallele"?*

*Fred*: Da Chicago è nato Detroit, da Detroit è nato Sheffield, dopo Sheffield
Francoforte e Berlino. In Olanda c'è una scena molto grossa e anche in Belgio
da dove sono usciti moltissimi dischi del new beat degli ultimi anni, dischi
molto belli e anche molte schifezze. C'è una scena in Spagna e nei paesi
dell'Est: si incominciano a organizzare rave anche in Russia. C'è anche una
scena italiana, molto rinomata nel mondo, soprattutto commerciale, che ha
sfornato dischi da milioni di copie, gente da Reggio Emilia, Bologna, Bergamo,
Brescia, Milano. L'house è stata una specie di virus che si è diffuso non solo
nei paesi solitamente produttori di tendenze nuove, non ci sono più frontiere.
Dalla provincia più sperduta puoi fare qualcosa che funziona a Francoforte,
Bruxelles, Amsterdam, Detroit. É una sorta di "united house nation", dove tutti
operano a modo loro, individualmente... uno dei pochi elementi comuni è il
fascino di certe sonorità elettroniche, anche vecchie, suonate con gli
strumenti di oggi, e l'attitudine dei musicisti a essere schivi, senza fare le
rock-star, ma a creare emozioni senza essere troppo notati. Non è solo infatti
musica per ballare, ma anche per sognare, si cerca di creare una dimensione di
spazialità che con certa musica da discoteca non si produce perché concepita a
mo' di canzone. La struttura dell'house è abbastanza libera e non schematica
come una canzone pop, e questo è anche il senso per cui noi facciamo tre o
quattro versioni della stessa canzone: per destinarle a luoghi e umori diversi.

*Qual è il pubblico dell'house?*

*Stef*: Se un disco vende sulle 1.000-2.000 copie è sicuramente stato comprato
solo da D.J.'s. Dipende poi dal D.J. quanto questo disco gira, quante volte lo
mette in una serata o se lo passa in una radio. Quindi non si può proprio dire
quale sia il pubblico, a meno che, proprio attraverso i D.J.'s, il disco
diventi un successo commerciale e allora lo comprano i ragazzini. Se ti manda
ad esempio Radio D.J. puoi star sicuro che vendi, ma lì la cosa cambia, perché
quel giro è pieno di produttori che pensano solo a far soldi e di altri che
sfruttano la moda. 

Se c'è moda la cosa è diversa, non è più tecno. Guarda ad esempio Derek Main,
uno degli inventori dell'house che ha sempre fatto i suoi dischi con la sua
etichetta "Transmat"; ha prodotto 16 dischi fino al '90 poi si è rifiutato di
far uscire roba nuova perché critico sulla questione della moda e con i gruppi
che si proclamavano tecno ma non c'entravano niente.

*Che cos'è l'Evolution, la vostra etichetta?*

*Fred*: A noi piace considerarci e farci considerare come una unità di lavoro
indipendente da qualsiasi influenza esterna.

Facciamo musica soprattutto per amore ma, essendo questa anche una merce,
guardiamo a quel ritorno economico che serve per evolverci. Alla base, come per
qualcuno in questo giro, non c'è la volontà di far soldi fine a se stessa ma
quella di produrre delle cose in cui poterci rispecchiare ora e anche nel
futuro. Non andiamo a implorare o a pagare da nessuno D.J. per avere dei
passaggi. Se una cosa piace, bene, altrimenti niente. 

Una scelta di libertà che però è anche rischiosa: da un giorno all'altro può
crollarti tutto addosso per problemi economici. 

Noi usiamo i canali convenzionali per fare musica con la nostra maniera: il
giorno che ce lo impediranno troveremo un altro modo.

**Un'intervista ai Clock DVA**

*di Kom-Fut Manifesto*  
tratto da [DECODER #7][58]

*Passioni che ancora bruciano...*

I Clock DVA sono stati nel corso degli anni Ottanta una delle formazioni di
punta dell'underground inglese più radicale. Padrino della formazione di
Sheffield nei primi anni di vita del gruppo fu Genesis P.Orridge (Throbbing
Gristle e Psychic TV) che curò la produzione del loro primo tape per la
Industrial Records - quel White souls in black suits ristampato di recente su
vinile - e che scrisse l'introduzione "Il leone in gabbia" per il loro LP di
debutto.

*Thirst*, primo LP del gruppo uscito nel 1980, è disco che ancora affascina per
l'elaborazione coraggiosa di suoni freddi e acuminati - punk, jazz e
sperimentazione - domati dalla splendida voce dalla timbrica sferzante di Adi
Newton. L'artwork dell'album è affidata a Neville Brody, all'epoca sconosciuto
autore della grafica di Cabaret Voltaire e della Fetish Records, che in seguito
diventerà il punto di riferimento per il rinnovamento della grafica inglese non
solo underground (leggi "The Face").

Dopo un rimpasto di formazione nel 1983 esce Advantage, secondo LP del gruppo,
che si ritaglia la fama di capolavoro maledetto degli anni Ottanta.

Advantage è una celebrazione cosciente dell'immaginario noir; in una Parigi
fredda e indolente, Newton descrive un mondo sotterraneo dominato da
ossessioni, pulsioni viscerali, popolato di "Beautiful losers" con il gelo
nell'anima.

L'album vive di secchi refrain funky e squarci di jazz notturno in piccoli
capolavori come *Dark Encounters*, *Eternity in Paris* e *Breakdown*, imbevuti
di una sensibilità oscura che avvolge tutto il disco. Nel mezzo del tour
europeo che segue l'uscita di Advantage, Adi Newton cantante e figura
carismatica all'interno del gruppo, abbandona i propri compagni.

Mentre Clock Dva sembrano svanire nel nulla, consegnati per sempre alla storia,
Adi inizia a lavorare a "The Anti Group", progetto video/musicale
elettronico-sperimentale.

I temi prediletti da "The Anti Group" sconfinano nell'esoterismo più oscuro e i
nuovi lavori discografici sono rebus sonori di difficile fruibilità;
all'interno del nuovo progetto emerge con prepotenza l'inserimento di video
assemblati dai TAG (The Anti Group) stessi e utilizzati nei live-act, costante
questa che rimarrà tratto distintivo dei rinati Clock DVA.

*Clock DVA come energia che tende al cambiamento*

Il terremoto epocale avvenuto nella società occidentale e portatore di un nuovo
modello di sviluppo dominato dalla tecnologia non poteva non avere effetti
anche nel microcosmo della "deviazione temporale".

La grande trasformazione degli anni Ottanta ha avuto almeno un merito, quello
di offrire tecnologie sofisticate a prezzi contenuti, permettendone quindi un
utilizzo sociale con conseguente creazione di nuovi linguaggi e comportamenti
urbani. Adi Newton recepisce positivamente questo nuovo spirito dei tempi. Sul
finire del 1987 si fanno sempre più insistenti le voci di una imminente
rifondazione della band. L'anno successivo esce l'EP *The Hacker*, prima prova
discografica dal lontano 1983.

E' un esordio fulminante e dirompente, il suono è diventato un'arma, ossessivo
e sinistro, pura elettronica anti-dance, glaciale e vorticosa, dove il ritmo
lancinante dei sequencers ti trasporta nel mondo digitale di terroristi
matematici, di un'algebra del male.

Il brano viene dedicato a Karl Koch, un hacker tedesco assassinato in un falso
incidente automobilistico nei pressi di Amburgo da agenti della CIA.

Nel 1989 vengono pubblicati altri due singoli, *The Act* e *Sound Mirror*, che
delineano definitivamente la nuova fisionomia sonora dei Clock DVA, con basi
elettroniche stratificate e ipnotiche create da computers e sampling machines.

*Quando David Lynch incontra il cyberpunk*

*Buried Dreams*, il terzo LP di CDVA, esce nel gennaio del 1990 e risulta per
originalità, rigore formale e spessore intellettuale, una pietra miliare del
suono elettronico del nuovo decennio. Ancora una volta il mondo connesso ai
mille aspetti insondati della psiche umana vengono esplorati in brani-gioiello
come *Velvet Realm*, *The Reign* e *Hide*.

*Buried Dreams* è un manifesto in cui, come ha rilevato Jonathan Selzer di
"Melody Maker", la perversione sottile e quotidiana alla David Lynch viene
proiettata nell'immaginario violento e ipertecnologico del cyberpunk.

Ma le suggestioni culturali/estetiche entro cui si muovono i CDVA comprendono
anche il Divino Marchese e Camus, Man Ray e Baudelaire, Moran e Mandelbrot,
intrigando e sorprendendo.

È quindi certamente riduttivo vedere il gruppo di Sheffield come "un gruppo
cyberpunk" perché - a parte la stupidità delle etichette - le influenze più
evidenti affondano le radici nel Novecento europeo e non solo nella
fantascienza contemporanea.

*Il futuro contiene molte forme e Clock DVA è una di esse*

Il 1992 inizia con la pubblicazione del quarto LP ufficiale della formazione ed
è intitolato *Man-Amplified*. Dal punto di vista sonoro non ci sono grosse
variazioni rispetto a Buried Dreams a parte una maggiore sintesi per quanto
riguarda la forma espressiva delle canzoni che ricorda per certi versi la
"classicità" dei Kraftwerk. Si tratta ancora una volta di un bellissimo album
che rende definitivamente CDVA il gruppo seminale dell'elettronica europea.

Il tema centrale di questo lavoro è il rapporto dell'uomo nei confronti della
scienza e in particolare verso le macchine e le innovazioni che ne mettono in
dubbio il ruolo e il futuro. Impressionante a questo proposito musica e
immagini di N.Y.C. Overload dove le sensazioni di violenza, saturazione e
frenesia, tipiche delle megalopoli occidentali, diventano reali, palpabili. Il
1992 li vede finalmente approntare un tour europeo a supporto dell'album appena
uscito e, in occasione della data italiana di Castelfranco Emilia, abbiamo
chiaccherato amichevolmente con il gruppo di fronte a un'enorme quantità di
pizze fumanti...

*INTERVISTA*

*Parliamo del primo singolo dopo la riunificazione, The Hacker, lo si può
definire come un brano sulla libertà d'informazione?*

*Andy Newton*: The Hacker parla di potere e tecnologia. Della libertà di
informazione. Parla soprattutto di resistenza contro le restrizioni imposte
dalla società. Trovare una via di scampo dalle restrizioni. Infatti a fronte di
qualsiasi tipo di società autoritaria che cerca di imporre il proprio sistema,
ci saranno sempre degli hackers di qualche tipo. Adesso con il sistema dei
computers ci sono gli hackers ma il loro spirito è sempre esistito. L'hacker è
anche una antica analogia per il simbolo della morte, della distruzione, di una
universale oscurità entro tutte le culture. Preferisco pensare ad uno sviluppo
euristico e che fare dell'hacking sarà in futuro una cosa non necessaria. Ma
sento che non sarà cosi' e forse è impossibile all'interno di una società
strutturata come quella attuale. Ci vorrebbe prima un cambiamento completo
attraverso lo sviluppo dell'human bio computer.

*The Hacker è una canzone politica anche se voi non siete un gruppo politico in
senso stretto?*

*A. N.*: È diventata politica perché lo è la natura dell'argomento: la
restrizione della libertà. La possibilità di accedere all'informazione è sempre
più ristretta e viene utilizzata dalla società, dal governo e dalle potenti
organizzazzioni economiche. La conoscenza è potere. L'informazione è controllo
potenziale. Siamo sommersi dai numeri."Il numero è in tutto" Baudelaire.

*Cosa ne pensi di William Gibson e Bruce Sterling e in generale della corrente
letteraria definita cyberpunk?*

*A. N.*: Mi piace quello che ho letto. È interessante. L'idea del cyberspazio è
un'area interessante. Penso che tali idee possono essere recepite da un
pubblico giovane. Quello di cui si parla probabilmente potrà accadere.
Tecnologia e sviluppo apriranno questo spazio. Il problema è che le idee sono
molto buone mentre la parte visuale corrisponde a cose già viste. Per esempio
la realtà virtuale è interessante ma la sua rappresentazione grafica al momento
è molto primitiva. Ci vorrà tempo per svilupparla e per farla conoscere ad un
pubblico più ampio. Ci sarà uno sviluppo secondo me soprattutto da parte delle
generazioni più giovani che non hanno idee rigide, lo trovano nuovo ed
eccitante, non sono spaventati o sospettosi come le generazioni più vecchie.

*Siamo rimasti colpiti dai video che vengono proiettati durante lo spettacolo.
Da chi vengono realizzati?*

*A. N.*: Li realizziamo noi. Utilizziamo un Amiga 2000 e del software sia
standard sia scritto appositamente per noi. Le possibilità grafiche dell'Amiga
sono molto buone, anche rapportandole ad altri computer più costosi. La gente
oggi è ossessionata dall'hardware, ma ciò che permette di avere dei buoni
risultati è la creatività dell'uomo, che è più importante del tipo di computer
e di programma usato. Comunque la parte video del nostro lavoro è molto più
costosa di quella sonora, sia in termini economici che di tempo impiegato.
Registriamo i video in formato Hi-Band fotogramma per fotogramma, con un VCR
apposito.

*Avete espressamente richiesto dei proiettori video LCD della General Electric.
C'è un motivo particolare? (I proiettori LCD sono simili a un proiettore per
diapositive, al posto della diapositiva c'è un display a cristalli liquidi a
colori. La luce esce in un fascio unico, mentre nei proiettori tradizionali ci
sono tre fonti, rosse, verdi e blu che si combinano sullo schermo)*

*Dean Dennis*: La tecnologia LCD risparmia molto tempo che andrebbe perso a
regolare la convergenza dei fasci luminosi.  
*A. N.*: Si, l'unico motivo è la praticità d'uso. Anche con poco tempo a
disposizione riusciamo a raggiungere un'ottima qualità d'immagine.

*Anni fa abbiamo assistito alla esibizione di TAG (The Anti Group) al festival
della musica contemporanea a Prato. Le immagini che proiettavate allora erano
molto più hard.*

*A. N.*: Sì, è vero, adesso siamo interessati ad aspetti diversi della ricerca
video. La tecnologia che usavamo allora era essenzialmente analogica, mentre
adesso passa tutto per il computer. Erano dei cut-up di immagini relative agli
argomenti che trattavamo come "Anti Group", con effetti video analogici.
Ricercavamo effetti ipnotici mediante la ripetizione di brevi sequenze, con una
modulazione della luminosità studiata per interagire con le onde alfa del
cervello.

Molte sequenze erano prese da studi clinici sull'epilessia, che può essere
appunto scatenata da stimoli audiovisivi a determinate frequenze. Noi non
riteniamo l'epilessia una malattia, ma un diverso stato della coscienza. Nelle
civiltà antiche gli epilettici erano considerati profeti e veggenti. 

Ci sono diverse malattie, come il ballo di San Vito, che provocano gli stessi
effetti delle droghe allucinogene come l'LSD. I video che facciamo adesso hanno
meno impatto immediato perchè non riprendono soggetti organici.

Sono in tema con la ricerca che facciamo sul rapporto con la tecnologia.

*Dopo la rinascita di CDVA è ancora attivo il fronte TAG?*

*A. N.*: Dire che TAG e` attivo non è sufficiente. L'idea dell'"Anti Group" è
molto complessa e, a parte la sperimentazione sonora con frequenze e
psicofisica, stiamo attualmente lavorando ad un documento di ricerca estesa,
basato sulla sonologia e le sue connessioni con scienza, arte, misticismo,
voodoo e tecnologia. 

Direi comunque che dal 1988 sono stato coinvolto dalla riformazione e dallo
sviluppo di CDVA e a mantenere il fronte TAG.

*Alcuni spettatori si sono lamentati per la scarsa durata dello spettacolo (45
minuti) e per il fatto che gran parte del suono fosse registrato.*

*A.N.*: Io ho cantato in diretta.  
*D. D.*: È impossibile eseguire completamente dal vivo la nostra musica.  
È già molto difficile come facciamo adesso, dobbiamo sincronizzare le varie
sorgenti sonore e i video.  
*A. N.*: La durata dello spettacolo è limitata dal fatto che vogliamo proporre
solo il meglio della nostra produzione e le cose più recenti. Ci interessa
concentrare in poco tempo le cose migliori. Riteniamo di raggiungere una
intensità maggiore.

*Negli anni Ottanta Sheffield ha dato i natali a gruppi come CDVA, Cabaret
Voltaire, In the nursery ecc., è una scena tuttora attiva?*

*A. N.*: Ci sono alcune compagnie che stanno facendo diverse cose. Esistono
diversi gruppi. Ognuno lavora ai propri progetti. Non ho molto tempo per vedere
cosa succede nella scena musicale. Comunque quello che ho sentito sono cose
convenzionali, niente di nuovo, niente che veramente mi ispiri a nuovi
orizzonti.

*Nel primo periodo di CDVA si percepiva un grosso amore per la musica nera, in
particolare il funk e il jazz. Cosa ne pensi della nuova black music, il rap ad
esempio?*

*A. N.*: Non ne penso molto. L'ho sentito alla radio ma non mi tocca. Non è che
abbia un'idea sul rap. Esiste. Ascolto musica elettronica e sperimentale.
Occasionalmente ascolto jazz o classica.

Quando ho il tempo per farlo. Non sempre ho voglia di ascoltare musica, magari
ascolto musica diversa da quella che faccio io, per rilassarmi ascolto qualcosa
d'altro. Mi piacciono diverse cose.

*Quali progetti stanno preparando CDVA per il futuro?*

*A. N.*: Stiamo lavorando a due libri: il primo uscirà quest'anno e conterrà i
nostri testi più alcuni scritti inediti e un documento sonoro, il secondo
conterrà la storia dei CDVA dal 1978 in poi.

Collegato a questo uscirà un album delle nostre prime registrazioni
rimasterizzate in digitale. Il libro è un grosso progetto e servirà tempo per
prepararlo e indicativamente sarà pronto per la fine del 1992.

Mi piace pensare che Clock DVA sia un progetto al di là delle classificazioni,
vergine di per se stesso.

Teatro - Performance
--------------------

**Intro**

L'entrata in scena delle macchine come attrici, in una sorta di post-futurismo
del 2000, il corpo mutato dove carne e macchina si compenetrano, la distruzione
dello spazio scenico tradizionale (il palcoscenico) con l'abolizione tra
personaggio e spettatore sono le caratteristiche principali della performance e
del teatro cyber. Ecco un percorso con i gruppi e gli sperimenatori più
interessanti:

- SRL (SURVIVAL RESEARCH LABORATORIES)
- Mutoid Waste Company
- La Fura Dels Baus
- Stelarc

**Survival Research Laboratories**

*di Monica Mascarella*  
tratto da [DECODER #5][63]

Mark Pauline è il fondatore del gruppo Survival Research Laboratories, che ha
base a San Francisco dal 1978. Con i collaboratori più stretti, Matt Heckert ed
Eric Werner, ha progettato o creato delle macchine di leggendaria distruttività
e orrore, nelle quali si integrano minacciose metafore meccaniche e cadaverici
residui animali. Jon Reiss ha raccolto varie riprese video della loro attività
e, ultimo, un film in 16 mm. coprodotto con gli stessi SRL, che documenta le
laceranti gesta degli ultimi predatori costruiti, coinvolti in combattimenti
Incendiari dalle mitiche dimensioni, sullo sfondo di uno scenario
incredibilmente apocalittico, progettato da Michele.

> La miseria di prima mano che il pubblico potrebbe potenzialmente soffrire è
> parte significativa di una dichiarazione creativa.  ("Re/Search" Pranks)

*Cosa pensi del tentativo di considerare i tuoi spettacoli come una conseguenza
di una nevrosi personale?*

Per me questo è un esito positivo. Solitamente le nevrosi non hanno nemmeno un
esito, si rinchiudono semplicemente in se stesse, con comportamenti eccessivi e
poco interessanti, senza andare molto lontano. Per cui sarò il primo ad
ammettere che i motori che guidano quello che faccio sono molto simili a quelli
di vari comportamenti ossessivi. Cosicché l'ossessione diventa un mio
strumento. In che modo la gente si trascina nel far qualcosa a parte svaccarsi,
fumare sigarette e bere birra tutto o il giorno? Qualsiasi modo in cui tu
riesca a raggirarti per metterti a fare qualcosa è valido.

*A proposito dello spettacolo a Copenaghen hai detto, precedentemente, di voler
portare alla luce ciò che è sommerso. Interpreti le grottescherie di
quell'architettura come rappresentative della soffocata realtà interiore della
gente.*

Ci siamo mossi come in una sorta di missione, conferitaci proprio dalle
caratteristiche di quella città; in quel tipo di democrazia sociale dove il
benessere viene livellato, poiché la maggior parte della gente appartiene allo
stesso strato sociale, ci sono aspetti che devono essere repressi, tensioni e
impennate di creatività e auto riflessione che diventano atrofiche. Il fatto di
essere sul posto, ci permetteva di essere l'innesco dello stravolgimento.
Abbiamo vagato per una settimana attraverso tutta Copenaghen, cercando di
coglierne le atmosfere, abbiamo parlato senza posa con migliaia di persone.
Restavamo, però, ancora limitati a una visione superficiale, e io volevo
arrivare alla giugulare. La vena vitale si trova tra i monumenti, il loro
aspetto grottesco e l'apparenza dimessa della gente. Ci chiedevamo come mai non
fossero felici. L'alcolismo è un fatto reale: tutti prendono il tono
dell'autocommiserazione nel darvi una spiegazione "siamo così repressi qui". E
poi in quella sirenetta si esprime l'intera identità della città; attributo
strano per una sirenetta alta solamente un paio di piedi. Inoltre, la Danimarca
ha un'agricoltura impostata sulla pastorizia e le sue genti amano considerarsi
un popolo di agricoltori pacifici. Tutto è estremamente pulito ed esente da
sgradevoli odori.

*E allora, come ti rapporti con tutto questo?*

Abbiamo regalato a quella piccola sirenetta una struttura con due teste alte
oltre 2 metri, costruita con una carcassa di vacca sistemata su un aggeggio in
grado di scorrazzare ovunque, le sue zampe incrociate proprio sopra un tino, di
circa 900 litri, pieno di formaggio andato a male e in ebollizione sopra un
enorme fuoco di carbone. Avevamo anche innalzato enormi spire e altre strutture
angolari in perfetto stile danese moderno: come le pile di legno sopra a un
piedistallo ottagonale, molto regolare, molto razionalizzato, con un enorme
teschio di vetro in cima. C'erano odoracci e, sullo sfondo, un enorme
battello... con un'incredibile quantità di fumogeni, un gigantesco vascello che
trascinavamo fuori dal molo, dove si svolgeva una scena di disastro vichingo,
arricchita da un impatto intensamente emozionale sul finale.  In definitiva,
era un modo per sollevare qualche interrogativo sui loro antenati che erano
stati così pieni di energia vitale. E la reazione è stata incredibile, poiché,
in quei luoghi, non avviene mai nulla di particolarmente straordinario. I media
vi apposero il chiavistello, creando il caso. Rilasciai almeno una ventina di
interviste, dove facevo dichiarazioni come: "quel cranio di vetro, perché la
psiche scandinava è così opaca, il piedistallo ottagonale rappresenta la vostra
psiche intrappolata nella struttura regolare che la cultura vi impone". Tutti
si dimostrarono estremamente coinvolti da queste riflessioni che si aprivano
sulla loro stessa cultura, e alle quali non avevano mai pensato. Facemmo sold-
out.

*Niente rogne da parte delle autorità?*

Al contrario, gli è proprio piaciuto; anche i pompieri vi hanno partecipato. Ci
diedero anche un po' di fumogeno, degli esplosivi, per loro era come fare un
giro gratis sulla giostra. Ci lanciavano contro degli oggetti, sul finire
puntarono gli idranti contro tutto, urlando e ridendo... finché con le asce si
misero a spaccare le finestre del battello che era sul palco. In questa
atmosfera anarchica, tra quei ragazzi in uniforme con quegli strani cappelli,
il capo incendi diceva: "Qui non ci sono mai incendi, vi siamo grati per
avercene forniti un po' da spegnere". Dopo sono venuti anche al party.

*Chi ha pagato?*

Il governo. Là, non c'è un dipartimento della difesa, o altro, che faccia da
spugna per il capitale in eccedenza.

*Avete avuto contatti anche con degli squatter di Amsterdam?*

Sono venuti da noi prima di uno scontro, ci hanno detto: "Senti, tu conosci un
sacco di tecniche e cose simili; la polizia farà un raid al nostro posto e noi
pensiamo di sapere quando sarà, più o meno. Probabilmente il giorno seguente il
vostro show. Non hai nessuna idea da darci?", "Butteranno giù tutto, no?" dissi
io; alla risposta affermativa, continuai "Beh, potreste fargli un bello
scherzetto. Penso che potreste usare la nostra macchina del fumo", uno di quei
grossi generatori militari della seconda guerra mondiale. Produce 28.000 metri
cubici di fumo al minuto. Diedi un'occhiata alla piantina che avevano fatto del
posto. C'era un punto in cui i poliziotti non potevano arrivare subito poiché
vi era di mezzo un rivolo. "Questo è il posto giusto, coprirà tutte le strade
di fumo," suggerii, "non saranno in grado di beccarvi qui. Poi potreste
incendiare tutto il posto: rompere tutte le finestre di ventilazione, ammassare
materiale cartaceo. Procuratevi 20 litri di olio, raccogliete legna secca e
piazzateli sopra dei pneumatici. Poi pigliate gli estintori e riempiteli di
benzina. Fate in modo che tutto sia pronto dal giorno precedente, versate
l'olio cosicché s'impregni a fondo. Rompete le finestre del retro per creare
una buona corrente, fate dei buchi nel pavimento così le fiamme potranno salire
molto velocemente. I ragazzi fecero: "O.K." e l'hanno realmente fatto! Ci
avevano anche chiesto di aiutarli, ma la gente che ci portava in giro ci
sconsigliò di venirne coinvolti. Siamo però restati a vedere. La polizia
arrivò. Loro erano tutti sui tetti di queste 20 costruzioni tutte in fila. Due
grosse fila di edifici di quattro piani dalle dimensioni di un isolato e mezzo.
C'erano alcune centinaia di poliziotti. Gli squatter accesero il generatore,
impedendo ai poliziotti di vedere alcunché e obbligandoli a battere in
ritirata. Nel frattempo un bel po' di gente uscì sul tetto e iniziò a lanciare
pietre e tubi ai poliziotti, mentre gli altri a pianterreno portavano un
attacco contemporaneo. Una ventina di "uniformi" riportarono delle lesioni e
furono obbligati ad abbandonare il luogo. I ragazzi avevano ancora 200 litri di
olio, sufficienti per quasi un'ora di fumo, che intanto continuava
incessantemente ad uscire.

*Sono fatte delle riprese?*

Sì, dalla polizia. Ma siamo riusciti ad averne una copia, avevamo detto che gli
squatter ci avevano rubato la macchina. Nella confusione sono usciti tutti in
strada e gli squatter se la sono svignata dal retro. Il giorno seguente
chiamammo la polizia denunciando il furto della macchina del fumo ed esigendola
indietro, pensando che fosse nello squat.  La polizia, però, rispose che non
c'era nessuna macchina sul posto; stavano chiaramente cercando di tenerla come
prova.

*Te la restituirono poi?*

No. Però, qualcuno me ne ha data una nuova di zecca proprio ieri.

*Nella storia di Amsterdam la frontiera tra l'intento artistico e l'azione
politica diretta è più indistinta, ma spesso non è così.*

Non succede qui; la gente non è abbastanza impegnata per fare qualcosa di così
rischioso. A volte, mi viene chiesto di fare delle cose qui, mi si chiede di
portare delle macchine quando questi idioti scendono in centro a protestare su
qualsiasi cosa. Gli rispondo: "Quando farete qualcosa di veramente serio e la
smetterete di fare dei giochini". Loro (il potere) non scherzano, perché noi
dovremmo scherzare con loro? Allora meglio ignorarli... Sappiamo come sono le
proteste. Abbiamo una struttura categorizzata di come fare una protesta,
attraverso l'interpretazione di un piccolo dramma politico. E siccome tutti
sanno cosa aspettarsi, non ha alcun effetto... è funzionale al governo. Negli
Stati Uniti le cose sono così dissociate che un atto e le proprie implicazioni
non hanno alcuna relazione con la maggior parte della gente. Spesso è solo
l'arroganza della disperazione che si esprime qui, poiché tutto è così sottile,
e ci vuole un grande sforzo per analizzare quello che succede nella cultura
americana. E chiaro ciò che succede: qualsiasi cosa debba essere, in realtà è
qual cos'altro.

*Non era la prima volta che la tua realtà, o S/r/l/ealtà, artistica si
sovrapponeva alla realtà sociale con un'azione diretta... sto pensando alle
manipolazioni che facevi un tempo sul cartelloni pubblicitari.*

Cerco di tenermi a distanza dalla "politica", perché sento che non va
sufficientemente lontano. A un certo livello, è finta; penso che la politica
organizzata sia una contraddizione in termini. Il vero lavoro avviene in modo
molto più strisciante. Del tipo di politica che appoggerei, se la facessi, non
parlerei. Mi spiego: ci sono cose che faccio che potrebbero venire lette come
estremamente politiche, ma io... L'intensità passa quando nessuno ne è a
conoscenza. Questa è un po' la filosofia che si celava dietro la mia attività
di "prankster". I "pranks" ("scherzi" o "burle", N.d.T.), come ad esempio le
manipolazioni sui cartelloni pubblicitari, sono degli attacchi costruiti contro
la struttura della società, uno scoppio inaspettato. L'inaspettato, l'elemento
di sorpresa trasposto in un atto mordace, che è, in ultima istanza, una
violenza contro la società costituita.

*Hai mai l'impressione di celebrare una sorta di rituale sciamanico attraverso
lo spettacolo?*

Mi piacerebbe pensarlo; il modo in cui percorro questa strada è, probabilmente,
solo una specie di esposizione triste di come stanno le cose qui. Forse è come
se l'unica speranza che tu possa avere sia essere irrazionale. O almeno per me.
Come atto politico deve essere non specifico. Ogni volta che faccio una
dichiarazione definitiva, o che gli spettacoli dicono qualcosa di definitivo,
poi finisco per contraddirmi a più livelli.

*La sensazione di pericolo fisico subita dal pubblico è sicuramente un elemento
del tuoi spettacoli...*

In passato gli spettacoli erano molto più pericolosi di adesso. Ma ora sembrano
esserlo molto di più di quanto lo fossero allora. E questo perché nei primi
spettacoli io non ero consapevole della tecnologia, e di come controllare e
imbrigliare questo tipo di cose. Voglio dire che, fondamentalmente, hai a che
fare con una situazione dove ci sono un po' di macchine che fanno parte di uno
spettacolo al quale delle persone assistono, e che tu devi riuscire a
trascinare nel clima, seppur non ci sia un'"azione" nel senso tradizionale del
termine. Devi avere qualcosa che garantisca il flusso dell'azione e ne conservi
l'unità; in passato questo qualcosa doveva essere veramente intenso, e, per
farmi capire, aggiungo che è in questo modo che mi sono ferito alla mano. Stavo
usando dei razzi non guidati, con la testata ad alto potenziale esplosivo, roba
che se ti colpisse t'ammazzerebbe. E noi li usavamo negli show dei primi anni,
assieme ad altre macchine che sparavano benzina... All'ultimo spettacolo non
avevamo nessun vero esplosivo, sono delle esplosioni alimentate a gas che hanno
un impatto molto più intenso. Aggeggi bizzarri a sei tamburi che allo scoppio
fanno seguire l'onda d'urto; ti colpiscono e ti sbattono indietro sulla
seggiola, come paralizzato, e questa è la paura dell'inconosciuto.

*Hai avuto fortuna durante i primi due anni...*

Direi che sono stato maledettamente fortunato che un sacco di cose non siano
successe, devo ammetterlo. D'altro canto, però, mi è capitata questa cosa alla
mano che ha veramente cambiato il mio atteggiamento. Fondamentalmente mi ha
fatto capire che si sono situazioni cosi pericolose da rasentare la follia; e
ce ne sono altre che pur essendo azzardate possono essere controllate da
persone affidabili. Decisi bene di non costruire più elicotteri da volo libero.
Non costruirò più razzi a motore simili ai missili contraerei Stinger. Non
voglio più fabbricare dinamite nel mio giardino. Lavorerò con materiale più
sicuro e di migliore effetto.

*Hai subito su te stesso ciò che sarebbe altrimenti toccato al pubblico.*

Be', ha definitivamente moderato quella specie di mio atteggiamento da ragazzo
arrogante borghese bianco al quale non è mai successo nulla.

*Si crea una tensione, nello spettacolo, tra gli elementi deliberatamente
organizzati e quelli casuali? C'è tensione tra simbolismo e immaginarlo onirico
nelle tue performance?*

Totalmente, credo interamente nell'involontaria soluzione di tutti i problemi,
dal momento che non sono una persona particolarmente razionale, anche se ho a
che fare con sistemi probabilmente sviluppati da persone estremamente
razionali, creatori di valuta e profitti. Penso intensamente alle questioni per
trovarvi delle soluzioni lineari, che arrivano da sé, in seguito, senza al cuna
relazione con quello che avevo pensato. Gli spettacoli funziona no allo stesso
modo: tu fai un pia no, per cercare di ottenere determinati effetti e sai
perfettamente, fin dall'inizio, che tutte le casualità, che accadono ogni
qualvolta metti assieme un'attività orchestrata e densa, possono fare o disfare
lo spettacolo. Questa è la tensione: se succede, riuscirò a vederlo? E la
ragione per cui non guido neanche più le macchine, durante gli spettacoli e mi
preoccupo esclusivamente di quegli elementi casuali, quei segni che indicano
l'andamento dell'azione. Come in Illusions of Shamleess Abundance, dove a un
certo punto vidi quel vecchio braccio inserirsi per cercare di abbattere i
pianoforti in fiamme e salvare la gente dal calore cauterizzante. Combatté
valorosamente avendo la meglio sui pianoforti e io dissi: "Questa macchina deve
sacrificarsi". Così, e quelle erano cose sulle quali avevo perso mesi di
lavoro, dissi: "Jonathan. Mandala tra le fiamme", e lui: "Co sa?", "Mandala in
mezzo ai pianoforti!". E alla fine abbiamo sacrificato questo enorme animale,
per restituire l'azione al meglio.

*In quale misura questi meccanismi sono delle sculture?*

Qualcuno ne ha parlato come di sculture cinetiche... Se tu mi dessi dei soldi
per aver fatto delle sculture, ti direi che sono delle sculture. Ma cosa
significa scultura? Vuol dire che qualcosa è stato progettato per essere messo
in una teca da qualche parte e restare dissociato dal resto del mondo? Da
qualsiasi tipo di mondo? Secondo me la definizione è contaminata dal passato e
dalla storia di ciò che scultura ha significato. Sfortunatamente la maggior
parte delle sculture, come quasi tutte le forme d'arte, sono in giro per
servire la struttura del potere. A me piace pensare che non sia lo stesso per
ciò che facciamo noi.

*Una posizione dialettica...*

La posizione dialettica è molto importante in quanto, secondo me, si lavora
veramente in opposizione quando si cerca di far qualcosa di nuovo o realmente
diverso. Comunque, di loro penso che siano "performing machines"; saranno
sculture quando io sarò morto e tu sarai in grado di trovarvi molti elementi al
riguardo: "Questa era in quel dato show e quest'altra nell'altro".  Sono
quantomeno composizioni, in particolar modo quelle che utilizzano parti di
animali o sembianze umane, mescolate a parti meccaniche puramente astratte...
Allo stesso modo degli F-16.  Per me è una specie di cosa frankensteiniana:
costruisco degli elementi del carattere, ogni macchina riflette il carattere
delle persone che l'hanno messa assieme; allo stesso modo, io le progetto
sull'idea che ho del loro carattere. Ad esempio, il "grande braccio"... avevo
visto quelle scavatrici e le avevo trovate veramente stupide perciò avrebbero
dovuto essere vive. Ci va sistemato su qualcosa che gli impedisca di scavare,
ad esempio una mano che gli dia la possibilità di raccogliere oggetti e senza
venir guidato da qualcuno. Ha bisogno di un computer, sensori elettronici sul
posto.. . era così che me lo vedevo, ed era veramente antropomorfico, pur non
essendo molto diverso nella sostanza da una scavatrice, il grande braccio aveva
quel piccolo tocco in più che lo faceva sembrare una creatura che serviva a uno
scopo veramente significativo in molti show. Ho fornito la macchina di una
personalità che è, per questa, il modo di avvicinarsi all'intelligenza: una
personalità reale che la renda idiosincratica. Prendiamo gli elementi e li
concentriamo insieme, comprimendoli al massimo rispetto a come sarebbero
normalmente... componenti, idee, possibilità che conducono alla
situazione/sorpresa.

*Gli SRL sono l'attività lavorativa della tua vita?*

Fino a questo momento. L'ho fatto per dieci anni e continua a soddisfare le
esigenze che originalmente mi appagavano. Mi dà qualcosa in costante mutazione,
che rappresenta una sfida reale e in cui mi sento libero di manovrare. Non mi
sento di essere parte del mondo che ho sempre odiato, il mondo del commercio o
come vuoi chiamarlo. Ma non sono inattivo, riesco a muovervi e a lavorare,
riesco a sentirmi vivo, un punto di collegamento...

*Ciò che ti permette di fare qualcosa a cui tieni e sopravviverci.*

Esatto. E non appena non sarà più così, non sarà più Survival Research
Laboratories, ma qualcosa di diverso.

*Cosa dici dei tecnici e loro conoscenze, che avete portato via dall'industria
della difesa per integrarli in cose come gli SRL.*

Abbiamo persone che lavorano ai laser di *Guerre Stellari*, che ci accompagnano
nei loro laboratori come si andasse a fare compere: otteniamo pezzi e
attrezzature. Alcuni di loro prendono materiale dai loro laboratori o dalle
loro ditte per portarlo qui da noi e poi lavorano con noi. Non so se
considerino il loro lavoro così immorale, lo fanno perché possono sbizzarrirsi
con giochi alquanto interessanti, perché rappresenta una sfida. Hai strumenti
che non avresti ottenuto altrove e solo attraverso il Dipartimento della Difesa
vieni pagato per usarli. Le cose cambiano molto rapidamente e non penso che
dovrò penare per coinvolgere altre persone appartenenti a quel mondo: l'intero
consorzio della scienza militare sta crollando. Fra un anno o due ci saranno
migliaia di esperti ricercatori disoccupati. (Nota bene che l'intervista è
stata raccolta assai prima di ogni ipotesi di guerra, N.d.T.) Ho paura che ci
sarà qualcuno che salterà fuori con qualche battaglia batteriologica che
spazzerà via qualche città, qualcuno semplicemente annoiato che non ha più
lavoro. Dove sarà tutto il plutonio di Lawrence Livermore? O Rocky Flats? Io so
cosa è successo: qualche addetto l'ha fatto sgusciare via dal retro e l'ha
piazzato da qualche parte. Succede spesso, questi incidenti saranno un effetto
della demilitarizzazione dell'industria.

*Personaggi filobellici che si mettono assieme per iniziare le loro piccole,
personalizzate e indipendenti milizie.*

Per fortuna anche i militari hanno dei problemi di gestione della loro stessa
disinformazione; sono così fuori dalla realtà che non riescono neanche a
portare avanti le loro piccole politiche conservatrici, proprio a causa del
loro essere così spiazzati, nel distinguere il reale dall'irreale. Certamente,
sono un sognatore diurno, penso sempre alle macchine; è una specie di tecnica
di massaggio mentale Ho fatto delle ricerche su generatori acustici a bassa
frequenza che producono suoni estremamente potenti. Ho circa 400 documenti e
articoli sull'argomento e ho stabilito che, se usati in un certo modo, non sono
affatto pericolosi, ma sono un potente strumento di manipolazione dell'umore:
una dimostrazione della potenza delle onde sonore impercettibili e interagenti
con le strutture. Queste applicazioni non sono mai state fatte prima e quando
ne ho sentito parlare ne sono stato totalmente coinvolto. I resoconti
indicavano una simultaneità di reazioni... ti sentirai molto stordito, il volto
ti si arrossirà, avrai le vertigini. Se fossi ubriaco, lo diventeresti ancor di
più. Perderai circa il 20% del punteggio in un test d'intelligenza e circa il
15-20% della capacità di stare in equilibrio. Ti fa vibrare la cassa toracica e
tremare così tanto gli occhi che non riesci più a vedere chiaramente, come se
qualcuno ti afferrasse e scuotesse violentemente.

*Userai queste cose ai tuoi spettacoli?*

Sicuro, associate ad altri strumenti diversi. Non sono nocive, e poi ci sono
centinaia di studi sui vari effetti. Era stato testato sulla gente perché la
NASA e i servizi militari pensavano di utilizzarle come arma e temevano che
potesse danneggiare i razzi in volo.

*Non è a disposizione dei poliziotti antimmossa francesi per il controllo della
folla?*

Loro adoperano uno strumento molto pericoloso: gli ultrasuoni... onde
ultrasoniche a modulazioni diverse. L'ho visto al telegiornale, dove c'è un
tipo coi binocoli e questi transduttori a ultrasuoni, che lanciano 2 diverse
frequenze, che spara a un cavallo durante una corsa e questo perde totalmente
il controllo. Ha beccato il tipo, ma questi pericolosi giocattolini sono gli
stessi che ha in dotazione la polizia francese. Questi trasduttori a onde
ultrasoniche differentemente attribuiti su due frequenze differenziate di circa
30 Hz, così succede che emetti un grido dalla frequenza elevatissima, che in
realtà non odi ma che danneggia seriamente, fino a lacerazioni interne se
prolungato in eccesso. E' estremamente destabilizzante, soprattutto se
associato a frequenze bassissime, poiché esperisci due toni diversissimi,
16.000 e 30 Hz, al tempo stesso, un colpo altissimo ed uno bassissimo: questo
ti fa crollare.  A San Francisco fu fatto, nel 1969, un concerto sperimentale
cogli infrasuoni, musica della nuova era che avrebbe dovuto euforizzare la
gente. Cani e gatti fuggirono, la gente era a disagio e abbandonb la sala. Ogni
parte del tuo corpo ha una frequenza di risonanza, che è la ragione per cui
devi essere estremamente accorto nell'usarle. Le frequenze veramente pericobse
si trovano nell'area dei 2.000 Hz.

*Qual è l'effetto ideale che vorresti raggiungere se avessi la tecnologia
adeguata a tua disposizione.*

Lo vorrei utilizzare come una sorta di tranduttore emozionale, che facesse
sentire il pubblico come dei bambini, in un modo molto particolare. Staranno a
guardare una scena e, che lo vogliano o no, dovranno sentirserne felici, come
se fossero su di giri, euforici... una forma di piacere... Insomma, cerco di
ricalcare il ruolo che ha il suono in ogni tipo di produzione, utilizzo il
suono per evocare le stesse emozioni tradizionalmente attribuite alla musica,
quel potenziale trasformazionale che essa aveva. Intendo ridurre il suono al
comun denominatore basilare, usandone i toni puri, con la differenza che dovrò
attrezzarmi con almeno 30.000 watt acustici. Considera che un fischietto di un
poliziotto alla massima intensità arriva a 1 watt, che la tua voce è 1
milliwatt, e che se avessi un'amplificazione da 100.000 watt sputerebbe fuori
solo 3.000 watt acustici. Non vorrei donne in gravidanza allo spettacolo: è
sicuro, ma comunque di estremo disturbo. Penso che vi siano degli interessanti
paralleli tra quello che facciamo noi, nelle performance e nelle presentazioni,
e le cose connesse alla R.V. Nonostante le inevitabili limitazioni, noi
tentiamo di creare delle situazioni che scatenino degli interrogativi e che
permettano alla gente di troncare con la limitata realtà che hanno ora a
disposizione, giocando con i simboli e prendendo in considerazione la
confusione reale della nostra cultura. Sfruttiamo questo aspetto della cultura
occidentale per realizzare degli spettacoli dove la gente interagisce come
vittima, con un mondo abitato da macchine, costruito per soddisfare le esigenze
di questi congegni meccanici antropomorfizzati. In un certo modo, le macchine
che costruiamo sono molto sofisticate, siamo però limitati dal fatto che nella
nostra cultura è possibile, perlomeno secondo la pubblicità di Ron Reesman,
diventare ricchissimi, grazie all'utilizzo della tecnologia sperimentale, o per
salvare la gente o per ammazzarla. Puoi ricevere po' meno soldi per costruire
cose che abbiano applicazioni pratiche o di consumo, di interesse di massa. E
puoi ricevere praticamente nulla per creare situazioni molto intense e
deflagranti, come quelle irreali che si presentano ai nostri spettacoli, che
qualcuno può definire "arte", ma che per noi sono solo parte di un processo di
restituzione sociale.


**Mutoid Waste Company**  
**Statement & intervista**

*a cura della Berliner Posse*  
tratto da [DECODER #6][8]

> Non solo hanno assorbito a fondo la modernità, ma, addirittura, i Mutoid
> Waste Company si sentono già mutati: fisicamente, psichicamente e, di
> conseguenza, comportamentisticamente. Sono una tribù di creativi riciclatori,
> che da inutili rottami ricavano sculture gigantesche e mobili, sono
> predicatori urbani mutanti, nomadi, per scelta, della nuova era: sono esempi
> viventi della "junk modernity". A Berlino, davanti all'ex- muro, hanno
> lasciato un paio di ricordini e un'intervista.

Nel caso che i veicoli dietro di te siano un incrocio fra un'astronave e un
vecchio camion arrugginito, niente paura. Gli occupanti sono pacifici. I Mutoid
Waste Gang sono una off-beat band di transumanti con un'insolita filosofia

*MASTERS OF METAL MUTAZION MACHT SPASS*

Sono dei provocatori, meccanici artisti e artisti meccanici e, per mezzo della
mutazione del mondo circostante, cambiano l'ambiente dove vivono.

Comic stip helden, scultura dub e ritmi tribali, gente selvaggia, predicatori,
performer e pazzi ispirati.

Ma queste definizioni sono per loro intercambiabili: il lavoro tocca tutto lo
spettro dell'arte: spaziano dalla musica alla performance, dalle esposizioni
alle parate a bordo dei loro macabri bus.

I Mutoid Waste Gang Company sono un gruppo di 8-20 persone fra i 18 e i 34
anni. Vivono dentro caravan fra rottami e pezzi meccanici.

E in un vecchio trattore inutilizzato si è sistemato Reverend Mutant Preacher
King Mutoid Obi alias Joe Rush che in un comodo caravan ci accoglie e riferisce
sul gospel della mutazione.

La lezione per oggi, o per un altro giorno, a seconda di quando ci si incontra,
è questa: "Necessità del cambiamento" ma, si badi bene, si sta parlando di un
bus. Per la maggioranza il bus è niente più che un mezzo di trasporto a poco
prezzo, ma per Joe e i suoi compagni dei M.W.C. il bus è il nocciolo della loro
filosofia, dove si vede combinata la doppia funzione di mutazione e mobilità.

La M.W.C. crea un'avvincente provocazione, una variopinta mutazione di
ambiente. Il road-show si parcheggia in vecchi supermarket, cantieri in rovina,
vecchi hangar per bus, parcheggi inutilizzati ecc.

Questa loro mobilità consente alla Company di disporre sempre nuovi spazi e di
occupare dei posti che altre persone non potrebbero usare, ma che loro, mutati
alla vita della waste land, adoperano. E la loro mobilità parla per loro
stessi: la mutazione, si sa, è una faccenda complessa. Joe intanto si adatta
alla nuova situazione e cerca di spiegarci la sua filosofia: "Uomini e cose
devono mutare fisicamente e i cambiamenti in un disastro o una post-apocalisse
devono essere profondi se si vuole sopravvivere."

Un bus può essere un appartamento o diventare un atelier e i rottami possono
essere fonte di sostentamento e ci si può guadagnare vendendoli, oppure essere
dei pezzi di ricambio indispensabili o, ancora, sono buoni per fare delle
sculture.

Un bus può esser dipinto o decorato, in modo che esso stesso diventi un pezzo
d'arte e se un bus non può essere lavorato se ne possono ricavare dei pezzi che
possono essere utilizzati per altre macchine.


"Noi viviamo per questa idea della mutazione dei nostri veicoli e della nostra
arte," dice Joe, "l'idea è di rappresentare sempre qualcosa di originale e di
lasciarsi trasformare", continua "niente è finito per sempre e la natura delle
cose commerciabili è solo pattume: se tu non riesci a lavorare ed a intervenire
sopra queste cose avrai solo pattume" e la filosofia della mutazione si può
rapportare agli uomini e alle cose.

"Di questi tempi" dice Joe "ognuno ha la sua mutazione in se stesso ed essa
corrisponderà ai suoi bisogni e al suo lavoro. Gli impiegati vedranno spuntare
sulle loro teste matite gigantesche, e i reporter avranno delle escrescenze a
forma di block-notes e di tasti martellanti di macchine da scrivere".

Probabilmente Joe vorrebbe la sua testa trasformata in un gigantesco saldatore
in modo che non ci sia più differenza fra le sue mani e la sua testa.


Pazzi? Forse sì, ma i M.W.C. hanno dimostrato praticamente che la loro
filosofia funziona.

Il gruppo, che abbiamo già detto che è composto di 8-20 persone più un cane,
stanno nella capitale (Londra) ma viaggiano spesso fuori città, nel continente,
con i loro veicoli decorati con gusto funebre. Sono di casa in ogni posto dove
si possono trovare dei rottami e pezzi di motore.

Vivono in una quantità enorme di iniziative che va dalla rivendita di rottami
all'organizzazione di parties a 5.000 lire di entrata. In qualche loro
manifestazione ci sembra di riconoscere gli ultimi resti degli hippies anni
Sessanta, ma loro sono suscettibili a ogni paragone.

La loro filosofia ha qualche somiglianza con Mad-Max (il film girato nel
deserto australiano dopo l'olocausto del XXI secolo). Come Mad-Max usano i più
disparati attrezzi e in mancanza di pezzi se li costruiscono rovistando fra i
rottami. Joe però sostiene che la scelta della loro vita è precedente alla
visione del film, e pensa che lo sceneggiatore sia stato, come lui, ispirato
dai [2000 AD Comics][64].

Gli altri sono un cuoco che vive in caravan con la sua compagna, un'infermiera
che tutte le mattine va a lavorare. C'è Sandy un novizio che fa l'acrobata. Poi
c'è Greg detto "Spaceman" che è quello che costruisce le astronavi. Ancora c'è
Hari Chromehead (meglio conosciuto come Joshua Gospel), la guardia del corpo di
Joe, colui che predica i mutant gospel, c'è ancora Envy Girl e qualche volta la
ragazza di Joe.

Joe lavora in questo momento ad una particolare scultura mobile che si può
ammirare e comprare alla "Crucial Gallery" in Notting Hill, ma la mostra sembra
quasi una copertura, un aspetto secondario del suo lavoro.

Se siete tentati di pensare che la gang è fatta di scoppiati e che le loro
attività siano senza senso, vi sbagliate. E sbaglia chi crede che la Company
sia in ferie permanenti. I Mutoid sono un gruppo di lavoro che si dedica alle
sue speciali attività con abbastanza impegno da riuscire ad autofinanziarsi. Al
contrario degli hippies, loro pensano che non è sufficiente sentirsi
alternativi "felici" separati dalla società e chissà cos'altro.

Credono nel duro lavoro, ma non hanno interesse in tutto ciò che non è compreso
nella loro ortodossa ideologia. Ma non credono nemmeno che il loro stile di
vita sia una risposta alla crisi in cui è piombato tutto il mondo industriale
qui all'Ovest.

Ciò che dicono è che bisogna mutare e lasciarsi mutare.

Molti giovani, giovani che non sono in condizione di affittarsi una casa o di
separasi dai propri genitori trovano molto attraente la happy-go-lukky e
vengono affascinati dallo stile di vita dei Mutoids , nonostante la mancanza di
comforts come la vasca da bagno, elettricità e televisione.


"Molti vogliono seguirci ma non possiamo permetterci di portare tutti con noi."

Con la legge non hanno problemi. Per quattro automezzi hanno uno speciale
permesso come quello dei circhi.

Così M.W.C. accoglie una valanga di attività: un road-show, parate di mutanti,
musica e performances teatrali, esposizioni, sculture, prediche e mutazioni dal
vivo.

La musica è suonata su strumenti a percussione che hanno realizzato
personalmente. E' una musica industriale, quasi un blues da rotaia mischiato
con gospel di Neanderthal.

La musica che viene suonata da tutti i componenti del gruppo fa da sottofondo
durante le prediche mutanti.

Il mutante predicatore reverendo King Rat Mutoid God Obi si è specializzato
nello speciale vocabolario mutante e con molta teatralità e rambazamba
(velocità, scioltezza) si lascia trasportare dalle sue prediche 

Lo show viene smontato. I Mutoid partono nella notte verso un nuovo campo
mobile.

Il processo di mutazione continua e da qualche parte una nuova mostra viene
montata.


*Come si è formata la M.W.C.?*

Veniamo più o meno tutti dalla scena del punk londinese. Abbiamo incominciato a
fare teatro da strada, e delle sculture utilizzando materiale di recupero.
Dopodiché ci siamo organizzati con altre persone, e abbiamo formato la M.W.C.

*Le persone che fanno parte del gruppo, sono tuttora le stesse?*

Praticamente no! Nel giro di 5 anni hanno lavorato con noi circa 60 persone.
Attualmente siamo in 18, e pensiamo che questo sia il numero ideale per poter
lavorare e andarsene in giro. Continuamente si aggregano persone a noi. Anche
qui a Berlino abbiamo conosciuto qualcuno, che molto probabilmente continuerà
il tour con noi.

*Che cosa fate quando non siete in tour? Dove vivete?*

Noi siamo praticamente sempre in tour. In Inghilterra non viviamo in nessun
posto fisso. Giriamo in continuazione con la nostra carovana e ci accampiamo
dove ci capita. Ogni tanto a Londra occupiamo degli edifici vuoti, che
utilizziamo per i nostri shows e per viverci. Finito lo show ce ne riandiamo.

*Come vi finanziate le vostre tourne?*

Per i nostri tours non abbiamo grosse spese, a parte i costi di benzina e gli
attrezzi da lavoro. Il resto ci viene tutto regalato. Riguardo le spese
maggiori, cerchiamo di coprirle vendendo birra oppure organizzando parties a
pagamento.

*E come vi finanziate quando non siete in tour?*

Noi costruiamo su ordinazione sedie e tavoli, oppure vendiamo le nostre
sculture.

*Che tipo di acquirenti sono quelli che vi comprano le sculture?*

Differenti: club, compagnie cinematografiche e a volte qualcuno che deve
produrre videoclip.

*C'è qualcosa che vi ha particolarmente colpito qui a Berlino?*

Quello che ci ha particolarmente sorpreso è che in altri posti bisogna pagare
per avere un rottame, qui invece si paga per portare la propria macchina dal
rottamaio, per cui un casino di gente ci ha regalato delle macchine, alcune
delle quali erano proprio in buono stato (la revisione delle macchine qui in
Germania si fa più o meno ogni due o tre anni e sono veramente intolleranti).

*Che cosa ne sarà delle sculture che avete costruito qui?*

Le migliori le portiamo via, alcune le abbiamo regalate ai posti dove abbiamo
suonato, mentre altre sono state vendute. Il resto è rimasto al campo, ma
praticamente sono già dei rottami.

*Dove prosegue il vostro tour?*

Prossimamente andremo in una piccola città di porto nei pressi di Amsterdam e
poi sono incluse nel programma: Parigi, Barcellona e Ibiza.

*Vi sentite in qualche modo un gruppo politico?*

Noi siamo completamente coscienti di una scelta, cioè di non essere più
politici. Al di fuori di questo pensiamo che il modo in cui stiamo vivendo è
una chiara espressione delle nostre scelte. E' veramente così, il mondo nel
quale viviamo sta andando a puttane. Distruzioni, catastrofi ecc. Noi esseri
umani non possiamo cambiare più niente. Così cerchiamo, per il tempo che ancora
ci rimane, di divertirci il più possibile.

*E voi che tendenza politica avete con il vostro giornale?*

Qualcuno ci definisce degli anarchici. Vedi, noi invece siamo dei mutanti!!


**Intervista a La fura dels baus**

tratto da [DECODER #6][9]

> La "Fura", come ormai viene chiamata familiarmente in Italia da chi segue le
> forme culturali sperimentali, ha saputo affermarsi in campo teatrale come
> gruppo portatore di una ventata di novità. Chi ha seguito gli spettacoli non
> può dimenticarsi del coinvolgimento fisico provato durante le loro performance
> multimediali, fatte di recitazione, musica, scontro fisico e uno strano uso di
> macchine-mostri meccanico cibernetiche. Proprio su queste ultime si è
> incentrata la nostra attenzione perché era evidente che erano percepite dal
> gruppo come "estensioni del corpo". Non a caso quelli della "Fura" si
> definiscono *CYBERPRIMITIVES.*

Cortile del Centro Sociale di via Conchetta n. 18, a Milano. Davanti a noi
Carlos...

*Qual è il senso delle vostre macchine e come le costruite?*

Come prima cosa bisogna dire che le macchine ci servono per amplificare la
forza della nostra azione per il fatto che noi siamo in nove in mezzo a un
pubblico di mille persone, così come usiamo dei computer o la batteria
elettronica per la musica.

Ma nei primi spettacoli, come Suz/o/Suz di due anni fa, le macchine che
chiamavamo "automatics", avevano una funzione diversa, ispirata ai futuristi
italiani.

Erano autonome ed iniziavano a funzionare quattro o cinque minuti prima
dell'azione, producendo effetti sonori e rumori. Per costruirle avevamo usato
un motore di lavatrice, al quale abbiamo applicato una ruota di bicicletta che
faceva a sua volta funzionare una ruota dentata che metteva in azione un
braccio meccanico. Questo colpiva diversi oggetti sonori come una lamina
metallica, un bidone, una bottiglia, dei piatti. Avevamo anche un'affettatrice
che faceva vibrare una corda di banjo. Tutte queste macchine avevano un nome
differente: ce n'era una che si chiamava folklorica, che produceva suoni molto
acuti, una che si chiamava jazz che suonava la corda del banjo e il charleston,
un'altra che si chiamava heavy che suonava un bidone e una spranga di ferro,
un'altra che si chiamava bomberò che aveva incorporata una cisterna d'acqua che
veniva messa in circolo come fosse una doccia e produceva un rumore... fssss...
che ricordava un estintore. La funzione di tutte queste macchine era separata
dall'azione teatrale vera e propria che era invece ispirata all'energia umana
più simile ad una concezione africana o tribale-rituale ma nel senso
positivista cioè del vecchio che insegna al giovane, e il rito di iniziazione
di questo.

Le macchine costituivano i limiti simbolici dell'azione, all'inizio e alla fine
di questa, un contrasto tra il rumore automatico e il significato di tutto
questo. Con queste macchine abbiamo anche fatto una mostra aggiungendo un
sistema con una parete di ventilatori di fronte ad una costruzione metallica
che si attivavano automaticamente. Questa è stata la prima macchina grande che
abbiamo costruito.

Nello spettacolo nuovo, invece, le macchine sono di tipo diverso. Non solamente
una cosa che si attiva prima o dopo, ma che funziona insieme allo spettacolo.
Sono state pensate anche, ma non solo, per la musica. Qualcuna che funziona con
l'acqua creando un effetto di pioggia artificiale, altre che funzionano con un
sistema ad aria compressa. C'è un compressore a cui è collegato un tubo munito
di elettrovalvola che ad un impulso si apre e si chiude, facendo uscire un
odore di carne marcia o un profumo. Oppure quest'aria fa funzionare dei clacson
o dei flauti o un martello pneumatico che a sua volta percuote una lastra
metallica. C'è una macchina più complicata a cui è collegato anche il motore di
una lavatrice e che suona, secondo la partitura musicale, un tamburo. Comunque
tutti questi ventisette strumenti (clacson di auto, camion, barche, tamburi più
altri azionati elettricamente come due campane) vengono coordinati con la
musica. Abbiamo costruito un apparato con un'interfaccia computerizzata che
permette di sincronizzare questi rumori con il ritmo della musica e delle basi
preregistrare e accordarne le tonalità.

Adesso stiamo progettando per il prossimo spettacolo una macchina che reagirà
diversamente a seconda del comportamento del pubblico: quando la gente si
avvicina e la tocca questa sputa o da una scossa a basso voltaggio, secondo il
principio del "pastore automatico" che consiste in un filo elettrificato da 12
a 25 volt che impedisce alle vacche di uscire dal recinto. La nostra macchina
verrà azionata da una cellula fotoelettrica e potrà produrre un rumore o tirare
un poco di acqua o un colpo o un odore.


*Dove recuperate il materiale necessario per la costruzione?*

Di nuovo abbiamo comprato pochissime cose, solo quelle che è praticamente
impossibile trovare in giro, come le elettrovalvole, o le campane che è
impossibile costruirsi da soli, perché pesano dai 100 (la nota FA) ai 300 Kg ed
ogni nota differisce dall'altra di circa 35 Kg, più grande e la campana e più
bassa è la nota. Per le altre cose andiamo dai rottamai o al cimitero delle
barche.

Di norma preferiamo il materiale riciclato perché ci piace di più, è più
interessante, non è normale per il teatro tradizionale, ed è anche per questo
che preferiamo suonare nelle fabbriche. Abbiamo recuperato materiale nei
reattori nucleari, nei cantieri navali e, una volta, in un'impresa di pompe
funebri. Il grosso del materiale lo recuperiamo nel posto dove facciamo lo
spettacolo.

Abbiamo dei tamburi grandi di pelle di vacca che è molto difficile sostituire
quando si rompono perché sono della "banda di Calanda", una tradizione spagnola
molto antica del luogo dov'era nato Luis Bunuel, in Aragona, dove durante la
settimana santa, il venerdì, tutte le famiglie con dei tamburi molto grandi
suonano tutto il giorno e tutta la notte. Questi tamburi hanno un suono molto
basso, ma vengono percossi secondo ritmi precisi e antichissimi. Le pelli che
li costituiscono sono di vacche molto grandi e ogni famiglia ha i suoi tamburi
che vengono tramandati di generazione in generazione. La mazza per suonare
questi tamburi e molto corta e quindi ad ogni percussione parte della mano
colpisce la pelle. Con il passare delle ore la mano comincia a sanguinare
sporcando di sangue la pelle di vacca. Con gli anni i tamburi diventano neri di
sangue che si coagula...una specie di rituale. La gente va avanti a percuotere
i tamburi camminando tutto il giorno e tutta la notte e, cosa rara per queste
feste, non beve vino. Le vibrazioni basse del tamburo si trasmettono al corpo
creando uno stato di estasi e tutti, giovani e vecchi, battono e camminano. Ci
sono tre o quattro ritmi diversi, suonati da gruppi di una ventina di persone
in diverse piazze e quando si cambia piazza si cambia anche il ritmo.

Noi usiamo due di questi grandi tamburi percossi da una macchina.

Questo perché noi della Fura tentiamo di unire ciò che c'è di più primitivo, il
rituale, il sangue, mangiare la carne cruda, con l'idea cibernetica.

Due elementi contrastanti ma positivisticamente uniti, con ironia rispetto alle
macchine, ma anche con un certo fascino nei confronti di queste. Per noi un
motore d'aeroplano e bello come "L'ultima cena" di Leonardo. E proprio lui era
in grado da una parte di dipingere motivi religiosi, ma dall'altra era fanatico
delle tecnologie.

Per noi la scena migliore è quella di Berlino, dove abbiamo collaborato con
Einsturzende Neubauten e abbiamo dei contatti con Survival Research
Laboratories di S. Francisco che lavorano solo con macchine che si scontrano
tra di loro. Macchine che hanno una tale forza da rompere le catene con le
quali sono legate e da essere realmente pericolose per il pubblico.

Le nostre macchine sono più legate all'azione corporea e plastica. Infatti
un'altra macchina in progetto è come una nutrice automatica, per allattare, che
si applica come un corpetto, con molte mammelle. Può anche ricordare le
macchine dei sex-shop. Una macchina non di plastica però, che ti dà
soddisfazione, più umana, una specie di estensione del corpo anche se non
completamente assimilata da questo.


**Stelarc - Analisi di un cybercorpo**

*di Kix*  
tratto da [DECODER #8][12]

STELARC è un performer, o meglio, uno sperimentatore, di origine australiana,
la cui attività artistica iniziale può essere collocata all'interno di
quell'area che poneva il corpo al centro della sua ricerca.

Elementi tipici di correnti artistiche esplose soprattutto negli anni 60/70,
come Body Art, Comportamentismo, Land Art/Ecologic Art, Performance, convergono
soprattutto nelle sue prime azioni (o eventi). Ad esempio, la "sospensione"
operata tramite la struttura in riva al mare (vedi foto) vuole ricollocare il
corpo umano nel suo ambiente originario, primario, naturale, definendo un nuovo
rapporto con lo spazio circostante ma, allo stesso tempo, dimostrandone
l'attuale obsolescenza, l'artificialita'di un corpo "contemporaneo" in un
ambiente "arcaico".

L'iniziale impiego di imbragature per operare la sospensione del corpo, fu
sostituito dall'applicazione di ganci infilzati nella pelle; scelta che, se da
un lato eliminava quell'ingombro visivo che poteva distogliere l'attenzione
dalle motivazioni centrali dell'operazione, dall'altro determinò critiche su
presunte valenze masochiste che le opere potevano evocare. Ma, anche tenendo
conto delle affermazioni dello stesso Stelarc, non è sostenibile la presenza di
un tale elemento; le "azioni" di deprivazione sensoriale, di stress
psico-fisico e resistenza alle sollecitazioni, avevano come scopo principale la
verifica dei limiti dell'involucro biologico in situazioni estreme. Una volta
esplorati e confermeti come retaggio della "vecchia carne", Stelarc procede
verso il loro superamento grazie all'innesto della tecnologia.

Quindi un'intenzionalita'ben diversa da quella presente in altri artisti che
utilizzavano il corpo come luogo espressivo; opposta, ad esempio, a quella
dominante nell'"Azionismo viennese", dove il corpo veniva sistematicamente
sottoposto a interventi autolesionisti che potevano terminare in vere e proprie
mutilazioni (emblematico il caso di Rudolf Schwarzkogler, morto nel 1969 a
seguito delle conseguenze di una performance autolesionista). Azioni spesso
caratterizzate da un pesante nichilismo, intensamente drammatiche e permeate da
tensioni misticheggianti, una visione negativa della tecnologia, percepita come
presenza opprimente e castrante, costituiscono il repertorio di questa corrente
della Body Art, in netto contrasto quindi con gli assunti di Stelarc. Maggiori
analogie possono essere riscontrate con pratiche iniziatiche primitive, con
prove tendenti ad "educare" la struttura psico-biologica alla resistenza e al
superamento di condizioni limite, elementi presenti in varie forme di ritualità
orientale e di teatro giapponese 

Nell'evento del 1982 "Movimento/modificazione. Sospensione per corpo obsoleto"
(foto), Stelarc connette il corpo "primordiale" con l'apparato tecnologico
moderno: gli impulsi dei muscoli in tensione/modificazione venivano percepiti
dai sensori applicati sulla pelle e quindi elaborati e convertiti in segnali
sonori. Amplificazione delle sonorità corporee in condizioni limite.

Ma è con il progetto relativo alla "terza mano" che avviene il definitivo
passaggio verso l'amplificazione reale della struttura corporea. Un braccio
interamente tecnologico è connesso con il corpo di Stelarc, evento in sè non
così innovativo visto l'uso ormai frequente di protesi anatomiche nei casi di
mutilazione. Ma nelle intenzioni che sottendono la proposta stelarchiana non si
può più parlare di "protesi" se intesa come sostituzione reintegrativa di un
"pezzo" naturale. Non c'è volontà di ristabilire una condizione originaria, ma
il superamento di questa. Espansione corporea, non costruzione robotica
separata, autonoma e controllata a distanza, ma estensione innestata nella
struttura originaria. Uno pseudo cut-up di carne e tecnologia dove la sintesi
risultante varca la soglia dell'umano tradizionalmente inteso. La tecnologia è
vista da Stelarc come prodotto dell'uomo che ne determina il superamento,
allargando l'area dell'esperienza e aprendo la strada verso nuove potenzialità.
Ma non tutto è così roseo.

Da un testo di Stelarc, pubblicato sulla rivista tedesca "Warten - Das Magazin"
nel 1991 (da cui sono tratte le parti citate), emergono idee e considerazioni
sulla sua visione dell'"uomo" futuro, il post-uomo. Per Stelarc il corpo umano
non deve più essere considerato luogo della memoria, depositata-registrata
fuori da esso, ma oggetto manipolabile, amplificabile, estendibile: "Ciò che
conta è il corpo come oggetto, non come soggetto. Il corpo come oggetto può
essere accelerato, amplificato e ridisegnato. Il corpo non deve essere
ipnotizzato dalla memoria. La memoria culturale è storia registrata".

La pelle, come uno schermo, è illuminata e irradiata da rapide immagini,
effimero tatuaggio elettronico: "I media sono diventati la membrana
dell'esperienza umana. Un sistema di supporto vitale che sostiene il corpo. Il
ruolo dei media non è semplicemente quello di trasmettere informazioni.
Piuttosto, esso fonde il tessuto umano in un immenso schermo planetario di
pelle. Un tessuto palpitante, sensibile, che si contrae in sintonia con le più
remote stimolazioni."

Un'apoteosi dell'artificialità, dell'"illusione ad alta fedeltà". La carne si
muta/dissolve in un coacervo di dati, immersa in uno spazio di dati
(cyberspace).

In una visionarietà di esaltato candore profetico (nel senso che non viene
minimamente paventata alcuna difficoltà, rischio o trauma individuale, sociale
o "politico" che una simile sconvolgente mutazione potrebbe comportare. E in
questo siamo ben lontani dalle atmosfere cronenberghiane che aleggiano sulla
transizione dalla vecchia alla nuova carne) viene implicitamente prefigurata e
consacrata la divisione tra corpo (costrizione materiale, guscio, gabbia,
limite), definitivamente negato, e la sua componente immateriale (anima?
spirito? immaginazione? intelligenza?) ora finalmente liberata. Non è la "nuova
carne" ma la "carne immateriale", la "non-carne" abitante nel "non-luogo".

La sparizione del corpo come ultimo stadio del "desiderio
post-evoluzionistico", è auspicata come estrema possibile conquista, che
spalanca a questo nuovo essere immateriale nuovi spazi privi di limitazioni
biologico-corporee.

Provocazione? Delirio di onnipotenza? Mistica comunione con il cosmo?

Indubbiamente è presente in questa visione una forte spinta verso la
trascendenza: "E'tempo di sparire dalla storia umana, di tendere alla velocità
di fuga terrestre e di raggiungere una condizione post-umana. E'tempo di
svanire, di essere dimenticati nell'immensità dello spazio extraterrestre.
[...] L'importanza della tecnologia potrebbe essere quello di culminare in una
coscienza aliena - che sia post-storica, trans-umana o addirittura
extraterrestre." Quindi la dipartita dal corpo, il definitivo capolinea
dell'evoluzione umana (la fine della storia?), a favore della totale
esaltazione della funzionalità e libertà formale determinate dall'immersione
nello spazio virtuale del dato tecnologico: "In questa epoca di sovraccarico
informativo, non è più tanto significativa la "libertà delle idee" quanto
piuttosto la "libertà di forma". Il punto non è più se la società ti permetterà
di esprimere te stesso, ma se la specie umana ti lascerà infrangere i vincoli
dei tuoi parametri genetici".

"Ciò che è importante non è più vedere il corpo come oggetto di desiderio, ma
come oggetto da ridisegnare. Per me la premessa è che se alteri l'architettura
del corpo, ne alteri la sua visione del mondo, e questo è affascinante. Noi
siamo alla fine della filosofia data l'obsolescenza della nostra fisiologia.
[...] Il pensiero umano si ritira nel passato dell'uomo".

Negato il pensiero e la filosofia, pratica anch'essa obsoleta come il corpo,
Stelarc sottolinea invece l'importante funzione dell'artista nella definizione
della nuova forma post-umana: "L'artista può diventare un architetto degli
spazi corporei interiori, ristrutturando il territorio umano e ridefinendo il
nostro ruolo di individui. [...] Informato e intelligente, l'artista può essere
un Agente Post-Evoluzionistico, che traccia nuove traiettorie, elabora
strategie, focalizza desideri alieni." Quindi, una soluzione compresa
essenzialmente in una dimensione estetico-funzionalista.

Azioni, visioni, proiezioni futuribili e teorie fortemente innestate con la
condizione dell'uomo contemporaneo in ambienti hi-tech, uniscono intuizioni
molto acute, affermazioni piuttosto contraddittorie e carenti sotto vari
aspetti. D'altronde la ricerca di Stelarc, come lui stesso lascia intravedere,
è da considerare come prettamente artistica, concettuale, di sperimentazione
percettiva, di ricognizione su nuove potenzialità, dimostrando scarso interesse
alle implicazioni psicologiche, sociali, economico-politiche legate al
dispiegamento tecnologico, alla ricerca scientifica, ai fattori che la
determinano e agli esiti a cui conduce.

Per Stelarc, "ciò che è filosoficamente rilevante non è più il dilemma
mente-corpo, ma piuttosto la divisione corpo-specie. E proprio come la fissione
dell'atomo sprigiona enormi quantità di energia, così la scissione della specie
umana, determinata dalla tecnologia implosiva (innestata all'interno del
corpo), genererà un enorme potenziale biologico, risolvendosi in una
arricchente e stimolante diversificazione del genere umano. [...] Una volta che
la tecnologia avrà munito ogni corpo del potenziale per progredire
individualmente nella sua evoluzione, la coesione della specie non sarà più
importante."

Una differenziazione/frammentazione biologica globale. Quali rischi di
superiorità genetiche?


"Noi aspiriamo alla creazione di un tipo non umano nel quale saranno aboliti il
dolore morale, la bontà, l'affetto e l'amore [...] Noi crediamo alla
possibilità di un numero incalcolabile di trasformazioni umane, e dichiariamo
senza sorridere che nella carne dell'uomo dormono delle ali. [...] Il tipo non
umano e meccanico, costruito per una velocità onnipresente, sarà naturalmente
crudele. [...] Sarà dotato di organi inaspettati: organi adatti alle esigenze
di un ambiente fatto di urti continui. Possiamo prevedere fin d'ora uno
sviluppo a guisa di prua della sporgenza esterna dello sterno, che sarà tanto
più considerevole, in quanto l'uomo futuro diventerà un sempre migliore
aviatore.

Queste non sono parole di Stelarc ma di un certo Filippo Tommaso Marinetti,
principale "ideologo" del primo Futurismo italiano, fondamentale avanguardia
degli anni Dieci.Tra folgoranti intuizioni, deliri superomistici, nevrastenie
belliche, radicali innovazioni artistiche e tecniche, definizioni di nuove
futuribili forme, patriottismi isterici, misoginia e nazionalismo, si consumò
la proiezione nel futuro di Marinetti e soci. Il metallico guerriero
(Terminator?), ultima versione del super-uomo, è il "top" della commistione
umano-meccanica futurista, onnipotente dominatore, super-maschio alla ennesima
potenza, reso invincibile dalla tecnica moderna. Se la "piega" ideologica che
attraversò tutto il Futurismo italiano resta per noi evidentemente riprovevole,
è tuttavia innegabile la sua acuta attenzione agli elmenti fondamentali della
modernità: proliferazione tecnologica, scenari urbani in continua espansione
(La città che sale di Boccioni),modificazione radicale della natura a opera
dell'uomo dominatore della tecnica, mutazione dell'uomo stesso, nuovi mezzi di
comunicazione e loro implicazioni nella percezione del mondo, velocità,
compenetrazioni dinamiche, alterazioni spazio-temporali, nuove sonorità
presenti nell'ambiente (gli Intonarumori di Russolo, anticipatori di molta
musica contemoranea).

I futuristi riuscirono a prefigurare forme ampiamente radicatesi nei territori
della presente realtà (vedi le fantascientifiche - per quei tempi -
architetture di Sant'Elia) o dell'immaginario collettivo. Ma questa esaltazione
iper-positivista delle potenzialità della tecnica, sostenuta da una dissennata
cultura di dominio, potenza e presunte superiorità biologico-razziali, si
concluse nelle "tempeste d'acciaio" del '15-'18 e, una volta trovatisi in
mezzo, molti futuristi compresero che avevano sbagliato qualcosina.

Attenzione, caro Stelarc! Questa è storia.


*L'intervista che riportiamo di seguito è stata tradotta dalla rivista inglese
"VARIANT", n. 11, primavera 1992.

L'intervistatore è Stuart Mc Glinn.*

*Nelle tue prime opere vi erano numerosi elementi scultorei con riferimenti
arcaici e naturali, come, a esempio, ceppi muniti di aculei, strutture
totemiche. Riguardo le ultime "sospensioni" ci sono state errate
interpretazioni delle immagini con riferimenti sciamanici. Come replichi a
queste?*

Sono sempre stato affascinato dall'uso di legno, pietra e acciaio, ma li
considero materiali di base, primari, che possono essere messi in relazione con
il corpo. Usando questi materiali insieme al corpo o sospendendolo a un albero,
il corpo viene ricollocato nel suo regno naturale, amplificandone
l'obsolescenza: in tal modo l'uso di quei materiali non è in senso sciamanico o
simbolico. E' piuttosto una relazione strutturale con il corpo e i rapporti con
il suo scenario naturale, primitivo.

Per quanto riguarda le opere in sospensione, ciascuno ha diritto a interpretare
le immagini come vuole. Queste immagini sono state estrapolate dai media in
modo tale che si ha l'impressione che ciascuna di queste sia tipica del lavoro.
Ma non è così, essa è parte di una successione di interventi comprendenti la
produzione di tre film sull'interno del corpo, in cui si pratica una serie
completa di privazioni sensoriali e prove di sospensione con funi e imbragature
appese a palloni aerostatici, con il corpo totalmente amplificato con raggi
laser in tutte le performance iniziali.

*Quali sono le motivazioni alla base delle "sospensioni"?*

Sono sempre stato affascinato dall'immagine del corpo nello spazio - immagine
tanto primordiale quanto legata alla contemporaneità. Spesso sognamo di
fluttuare e volare, e molti rituali primitivi comportano la sospensione del
corpo secondo varie modalità, in più oggi il corpo può galleggiare in assenza
di gravità. Queste "sospensioni" si collocano tra la fantasticheria e la realtà
dell'astronauta, così sono sempre esistiti casi di collocazione del corpo nello
spazio, questo è impulso iniziale. Nelle prime sospensioni io appendevo qualcun
altro ma, dal momento in cui i progetti implicarono maggiori difficoltà,
dovetti io stesso assumermi le conseguenze fisiche e mi preparai a eseguirle da
solo.

*Quando decidesti di passare dalle "sospensioni" con imbragature a quelle
tramite ganci attraverso la carne per sostenere il corpo, adducesti come
ragione l'eliminazione dell'ingombro visuale dell'imbragatura. Come reagiresti
all'asserzione secondo la quale l'ingombro visivo è stato sostituito da un
"ingombro ideologico", con allusioni non intenzionali?*

Per me la transizione ai ganci fu un'azione molto semplice. Molto onestamente
posso dire che non avevo pensato alle implicazioni sado-maso e mi hanno sempre
lasciato perplesso quelli che ricorrevano a performance con crocifissioni o
ganci con appesa della carne morta. Allora, come ancora adesso, le mie
intenzioni erano rivolte ai rapporti strutturali. C'era un ingombro visuale
dovuto alle imbragature e alle funi e l'effetto era quello di sostenere il
corpo piuttosto che sospenderlo. Così usai ganci infilzati nella pelle
attaccati a sottili cavi. Avrei potuto utilizzare qualcosa di "invisibile" come
filo da pesca, ma mi piaceva l'idea di utilizzare funi o cavi di acciaio poichè
questi definivano delle linee di forza e tensione, e ciò era parte del congegno
visivo del corpo sospeso. In tal modo, deliberatamente, non ho voluto farne una
sorta di evento illusionistico con un corpo fluttuante nello spazio.
L'intenzione era di usare semplicemente il minimo sostegno per il corpo; la
pelle tirata diventava parte della struttura di supporto e una specie di campo
gravitazionale.

*Il rapporto con la scienza e la ricerca è metaforico o diretto, considerando
ad esempio il tuo lavoro sullo sviluppo del braccio meccanico?*

Io non ho una formazione di tipo scientifico o ingegneristico ma, avendo
vissuto in Giappone per 19 anni, con amici impiegati nei settori robotica e
ingegneria, ho una buona conoscenza generale dell'attuale tecnologia. Tuttavia
il progetto "terza mano" non era un progetto ingegneristico nuovo, si basava su
un prototipo sviluppato presso la Wassau University. Lo modificai leggermente.
La ditta costruttrice che mi aiutò lo elaborò secondo le mie esigenze - io
disegnai la struttura di supporto e vi applicai un depressore di un'altra
ditta. Volevo una mano adattata alle dimensioni della mia mano destra e
ricoperta da uno stampo cosmetico, anche se non l'ho mai usato, poichè ritengo
che la tecnologia presente dovesse essere visibile. Questa è la mia strategia
generale. Io ho delle idee su come possiamo connettere appendici tecnologiche
in simbiosi con il corpo e, stabiliti questi concetti, non mi interessa
elaborare semplicemente un progetto di finzione scientifica o un'idea non
realizzabile. Io metto sullo stesso piano l'espressione con l'esperienza, con
l'esperienza della realtà.

*Tu hai affermato che per sospendere il corpo lontano dalla superficie
terrestre avremmo bisogno di temprare e disidratare il corpo per renderlo più
resistente - una pelle sintetica per ridisegnarlo radicalmente nella nuova era
tecnologica. Tutto ciò lo intendi alla lettera?*

In primo luogo, la percezione dell'obsolescenza del corpo fu molto profonda. Ho
la sensazione che siamo giunti ai limiti della filosofia, non perché siamo ai
limiti del linguaggio, ma a causa dell'obsolescenza della nostra fisiologia. I
parametri strutturali dei nostri corpi stanno determinando la nostra
consapevolezza percettiva e la comprensione cerebrale del mondo e, alterando la
nostra architettura, regola ed estende la nostra consapevolezza: usando dei
robot sostitutivi con remote-control per proiettare la presenza umana e
l'effetto dell'azione fisica in luoghi remoti. I generi di teorie presenti
nelle mie performance provengono dalla lettura della letteratura scientifica,
dal mio interesse generale per la filosofia e la psicologia e non da un punto
di vista accademico. Sono interessato e affascinato proprio dal modo in cui le
idee si evolvono e dal rapporto tra idee, cultura e tecnologia che le genera,
così queste idee non si propongono di giustificare le performances. Questo è il
motivo per cui io non ritengo che le performances debbano essere descritte.
Talvolta il pubblico ha assistito a una performance e non sapeva ciò che stava
accadendo, anche con questi elettrodi e fili metallici inseriti in tutto il
corpo. Tuttavia hanno chiesto: "cos'erano quei suoni?" Non avevano motivo di
comprendere che quelli erano in realtà segnali corporei amplificati. Qualcuno
venne da me e mi disse: "hai un tremendo controllo del braccio sinistro", ma
questo scattava su e giù automaticamente tramite un paio di stimolatori
muscolari. Inversamente, pensano che la "terza mano" fosse automatica o
programmata mentre sono io a controllarla totalmente. Se scopri queste cose
dopo la performance, va bene, se le sai già prima, va bene lo stesso. Quando
visito un museo odio leggere qualcosa riguardante quelle opere - ho la tendenza
a confrontarmi con i lavori, a trovarmici di fronte e interpretarli senza una
necessaria mentalità di visione pre-determinata, senza la necessità di far
riferimento alla memoria o alla cultura. Questo potrebbe essere un obiettivo
impossibile dato che l'esistenza umana si basa sulla memoria; ma il mio
desiderio è di staccarmi dalla memoria umana e alla fine essere in grado di
svanire, differenziarmi, partire da questo particolare habitat evolutivo - cosa
che non significa necessariamente abbandonare il pianeta. Potrebbe voler dire
muoversi sott'acqua o nel sottosuolo o dentro di noi.

*Ritieni che il desiderio di lasciare il corpo o il pianeta abbia le sue radici
nella curiosità e che questa sia una buona ragione su cui basarsi?*

Si potrebbe dimostrare che uno dei motivi per i quali noi siamo creature
adattabili e abbiamo sviluppato sia l'intelligenza sia la curiosità. La nostra
curiosità potrebbe essere la causa della nostra mobilità...

*Ma sicuramente se uno parla di vivere senza il corpo perché questo è diventato
una maledizione, la curiosità stessa può non essere giustificata?*

In un certo senso, io sostengo che questo corpo è obsoleto, non dico che
possiamo vivere senza "incarnazione". Siamo giunti a un punto nel nostro
sviluppo post-evoluzionistico in cui la normale evoluzione organica darwiniana
non è più determinata dai fattori presenti nella biosfera, dalle forze
gravitazionali. Adesso lo è dalla spinta delle informazioni, abbiamo accumulato
questo input che produce questi desideri di esplorare, estendere, amplificare,
valutare, diagnosticare maggiormente. Così ciò che ha inizio come strategia
evoluzionistica, questa curiosità che è essenzialmente il risultato della
nostra mobilità e percezione, ora giunge a un punto in cui questa accumulazione
(di informazioni) comincia ad avere una propria dinamica e direzione e agisce
da propulsore per il corpo e lo forgia in nuove forme. Il campo
dell'informazione ora modella la struttura del corpo.

*Questo slancio tecnologico è sostenuto e finanziato dall'apparato militare e
dalle multinazionali. Come ti senti, come artista, in un coinvolgimento così
diretto e nella promozione di questo tipo di ricerca?*

Io, in generale, non ho una visione della vita cinica o pessimista. Non vedo il
disastro ovunque. Sicuramente, una gran parte di questa ricerca è sostenuta dai
militari e certamente le forze economiche governano molte innovazioni
tecnologiche, ma mi piace pensare che alcune tecnologie abbiano una propria
"raison d'être". Possono essere giustificate come ricerca pura, realizzate
senza finanziamenti da nessuno, non-distruttive, connettibili al corpo umano
per esplorare nuove frontiere di conoscenze e informazioni. In tal modo tendo
ad avere una visione abbastanza ottimistica. La ricerca umana è ancora fondata
sulla potenza distruttiva, il potere, l'aggressione e la guerra ma,
ultimamente, si sta dirigendo in altre direzioni come l'estensione
dell'intelligenza, la percezione e l'abitazione di un più vasto contesto
spazio-temporale extra-terrestre. E' difficile discutere sulle questioni etiche
e morali da un punto di vista sociologico o politico. Ciò che mi interessa è
l'integrità concettuale che ciò produce. Non posso sempre pretendere che questo
sia il giusto modo di procedere o che non ci possa essere una strategia
sbagliata, ma non mi sento qualificato per trattare questi argomenti, sono
questioni sociali molto complesse. Come artista non puoi indugiare su ostacoli
politici o ingiustizie sociali altrimenti non riusciresti ad adempiere alla tua
funzione. Uno non farebbe arte del tutto se esaminasse realmente il mondo, la
povertà, il disagio o l'ingiustizia. Come può una persona produrre arte, come
può avere la sicurezza di non perseguire la vanità implicita nel processo
artistico? Proprio come uno scienziato può essere accusato di immoralità per
certe invenzioni, così un artista può essere criticato di trascorrere la sua
vita in futili esplorazioni visuali e concettuali.

*Puoi spiegare cosa intendi quando affermi che nell'attuale epoca
dell'informazione la libertà importante non è quella delle idee, ma "la libertà
di mutare il proprio corpo"?*

Ho affermato ciò perché a quel tempo pensavo che in un ambiente sovraccarico di
informazioni, dove sono aumentate le interconnessioni tra computer, si sarebbe
creata una situazione in cui la libertà di informazione/comunicazione non
costituiva più un problema. Il punto in discussione era se un governo, o un
gruppo religioso, o una società, ti avessero permesso di modificare il tuo
corpo. Ritengo che la libertà di forma piuttosto che quella di informazione, ti
permetteranno di modificare la tua attuale struttura del DNA. Per quale motivo
l'intelligenza dovrebbe essere incassata soltanto in questa forma bipede
secondo questa chimica del carbonio e queste particolari funzioni? Adesso il
punto è come espandere l'intelletto, creare antenne sensoriali e sperimentare
soggettivamente uno spettro di realtà più ampio, come estendere la nostra
ampiezza vitale. Penso che il problema non sia il perpetuarsi tramite
riproduzione ma piuttosto concentrarsi sul soggetto per ridisegnarlo. Forse,
quando gli uomini si saranno spinti fuori dalla terra, le rigide credenze
religiose, politiche e sociali non saranno più così tenaci e ci saranno spinte
che renderanno più facile la possibilità di ridisegnare il corpo senza traumi
per la nostra cultura planetaria.

*Ti è stata mossa la critica di perpetuare, con il tuo lavoro e le tue idee, le
strutture e le ideologie del potere maschile. Come replichi?*

Non penso che la tecnologia sia un prodotto maschile. Esiste una critica
femminista che asserisce che la tecnologia sia essenzialmente opera del maschio
e sia utilizzata per perpetuare il suo dominio. Ritengo che questo sia
sostanzialmente un concetto seducente per le femministe, ma è possibile che sia
stata proprio la parte femminile della nostra specie a dare inizio all'uso
della tecnica nella raschiatura delle pelli e in altri utensili inventati dalla
donna, così come le lance e altri strumenti di caccia furono probabilmente
costruiti dal maschio. Non abbiamo prove certe sull'esistenza di una netta
divisione tra la caccia e la cura della casa, a eccezione del fatto che la
gravidanza costringesse la donna all'immobilità. Ma, anche ammettendo che la
tecnologia sia stata un'invenzione del maschio, le sue implicazioni ed effetti
attuali sono di eguagliare le nostre possibilità fisiche e uniformare la
sessualità umana. Ad esempio una donna che guarda in un microscopio ha la
stessa acutezza di vista di un uomo, una donna che guida un veicolo può andare
alla stessa velocità e potenza di un uomo. La tecnologia ha potenzialmente la
capacità di eliminare completamente il peso della gravidanza dal corpo della
donna, procedendo così ci sarà allora l'eventualità della perdita di importanza
della sessualità e della sua graduale eliminazione; in tal modo non ci sarà
motivo di essere sessualmente differenti tranne che per il piacere personale.
Se saremo in grado di fecondare e nutrire il feto fuori dal corpo della donna,
allora tecnicamente non ci sarà il parto. Se potremo sostituire le parti
malfunzionanti, tecnicamente non ci dovrebbe essere morte. Così la tecnologia,
dopo aver parificato le nostre potenzialità fisiche e uniformato le nostre
sessualità, ridefinisce la nostra identità di umani. L'esistenza potrebbe
semplicemente voler dire essere operativi o non-operativi.

*Se qualcuno dovesse percepire questi lavori come performance musicali, i
riferimenti nelle tue opere non dovrebbero forse essere meno forti per
permettere a queste interpretazioni di esserci senza soffocare le strategie di
cui parli?*

Ciò che mi affascina è che spesso c'è stata difficoltà nel catalogare questi eventi e inoltre negli ultimi 6 o 7 anni ho ricevuto più inviti a partecipare a festivals di "New-music" che ad altro; dato che non amo essere uno specialista, mi piace il fatto che questi eventi abbiano attraversato l'ambito musicale; il continuo confondersi e modificarsi dei confini rende questo campo di attività molto più stimolante.

[1]:https://web.archive.org/web/20120206133509/http://www.decoder.it/index.php
[2]:https://it.wikipedia.org/wiki/Italian_Crackdown
[3]:https://web.archive.org/web/20120210124031/http://www.decoder.it/archivio/cybcult/letterat/index.htm
[4]:https://web.archive.org/web/20120210124031/http://www.decoder.it/archivio/cybcult/politico/index.htm
[5]:https://web.archive.org/web/20120210124031/http://www.decoder.it/archivio/shake/decoder/cybview.htm
[6]:https://web.archive.org/web/20120210124031/http://www.decoder.it/archivio/cybcult/psich/cybpsius.htm
[7]:https://web.archive.org/web/20120210124031/http://www.decoder.it/archivio/cybcult/psich/index.htm
[8]:https://web.archive.org/web/20070717081001/http://www.decoder.it/archivio/cybcult/letterat/pdick.htm
[9]:https://web.archive.org/web/20110714174829/http://www.decoder.it/archivio/shake/decoder/idcd6.htm
[10]:https://web.archive.org/web/20110714174829/http://www.decoder.it/archivio/shake/catalogo/research/aprirep.htm
[11]:https://web.archive.org/web/20110714174829/http://www.decoder.it/archivio/shake/decoder/gbsn.htm
[12]:https://web.archive.org/web/20070809023945/http://www.decoder.it/archivio/shake/decoder/idcd8.htm
[13]:https://web.archive.org/web/20070826101556/http://www.decoder.it/archivio/cybcult/politico/eur.htm
[14]:https://www.ccc.de/
[15]:https://web.archive.org/web/20080627195529/http://www.xs4all.nl/
[16]:http://www.2600.com
[17]:https://web.archive.org/web/20111223155608/http://www.decoder.it/archivio/cybcult/politico/haraw.htm
[18]:https://web.archive.org/web/20111223155608/http://www.decoder.it/archivio/cybcult/letterat/secondag.htm
[19]:https://web.archive.org/web/20111223155608/http://www.decoder.it/archivio/shake/decoder/idcd10.htm
[20]:https://web.archive.org/web/20111223155608/http://www.decoder.it/archivio/shake/decoder/idfratt.htm
[21]:https://web.archive.org/web/20111223155608/http://www.decoder.it/archivio/shake/decoder/mud.htm
[22]:https://web.archive.org/web/20111223155608/http://www.decoder.it/archivio/shake/decoder/mindplay.htm
[23]:https://web.archive.org/web/20111223155608/http://www.decoder.it/archivio/shake/decoder/antimat.htm
[24]:https://web.archive.org/web/20111223155608/http://www.next.com.au/spyfood/geekgirl/
[25]:https://web.archive.org/web/20111223155608/http://sysx.apana.org.au/artists/vns/
[26]:https://web.archive.org/web/20111223155608/http://www1.arcade.uiowa.edu/gw/comm/GenderMedia/cyber.html
[27]:https://web.archive.org/web/20060620191352/http://www.decoder.it/archivio/shake/decoder/grupw1.htm
[28]:https://web.archive.org/web/20060620191352/http://www.decoder.it/archivio/shake/decoder/grupw2.htm
[29]:https://web.archive.org/web/20060620191352/http://www.decoder.it/archivio/shake/decoder/tvinter.htm
[30]:https://web.archive.org/web/20060620191352/http://www.decoder.it/archivio/shake/decoder/144.htm
[31]:https://web.archive.org/web/20060620191352/http://www.decoder.it/archivio/cybcult/politico/schu.htm
[32]:https://web.archive.org/web/20060620191404/http://www.alfea.it/coordns
[33]:https://web.archive.org/web/20060620191404/http://www.labornet.org/workers
[34]:http://www.oneworld.org/
[35]:https://web.archive.org/web/20060620191404/http://www.unic.org/wto
[36]:https://web.archive.org/web/20060620191404/http://www.essential.org/monitor/monitor.html
[37]:http://www.cleanclothes.org/
[38]:https://web.archive.org/web/20060620191404/http://www.igc.apc.org/labornet
[39]:https://web.archive.org/web/20111213045922/http://www.decoder.it/archivio/cybcult/psich/cybpsiuk.htm
[40]:https://web.archive.org/web/20111213045922/http://www.decoder.it/archivio/shake/decoder/fraser.htm
[41]:https://web.archive.org/web/20111213045922/http://www.decoder.it/archivio/cybcult/psich/rave.htm
[42]:https://web.archive.org/web/20111213045922/http://www.decoder.it/archivio/cybcult/psich/droghe.htm
[43]:https://web.archive.org/web/20120523064917/http://www.decoder.it/archivio/shake/decoder/nomads2.htm
[44]:https://web.archive.org/web/20120523064917/http://www.hyperreal.com/raves/grid/
[45]:https://web.archive.org/web/20120523064917/http://www.hyperreal.com/raves/altraveFAQ.html
[46]:https://web.archive.org/web/20120523064917/http://www.hyperreal.com/raves/
[47]:http://www.subspace.net:5000/
[48]:https://web.archive.org/web/20120523064917/http://www.yahoo.com/Entertainment/Music/Genres/Techno/
[49]:https://web.archive.org/web/20120523064917/http://mkn.co.uk/help/extra/people/goatrance
[50]:http://www.ccs.neu.edu/home/thigpen/html/music.html
[51]:https://web.archive.org/web/20080704125522/http://www.decoder.it/archivio/shake/decoder/extsy.htm
[52]:https://web.archive.org/web/20080704125522/http://www.decoder.it/archivio/shake/decoder/smart.htm
[53]:https://web.archive.org/web/20080704125522/http://www.decoder.it/archivio/shake/decoder/keta.htm
[54]:https://web.archive.org/web/20080704125522/http://www.hyperreal.com/drugs/
[55]:https://web.archive.org/web/20080704125522/http://www.hyperreal.com/drugs/faqs/
[56]:https://web.archive.org/web/20080704125522/http://www.hyperreal.com/drugs/pihkal/
[57]:https://web.archive.org/web/20080704125522/http://www.hyperreal.com/drugs/e4x/
[58]:https://web.archive.org/web/20071118101647/http://www.decoder.it/archivio/shake/decoder/idcd7.htm
[59]:https://web.archive.org/web/20070809023922/http://www.decoder.it/archivio/shake/decoder/srl.htm
[60]:https://web.archive.org/web/20070809023922/http://www.decoder.it/archivio/shake/decoder/mutoid.htm
[61]:https://web.archive.org/web/20070809023922/http://www.decoder.it/archivio/shake/decoder/fura.htm
[62]:https://web.archive.org/web/20070809023922/http://www.decoder.it/archivio/shake/decoder/stela.htm
[63]:https://web.archive.org/web/20070206133124/http://www.decoder.it/archivio/shake/decoder/idcd5.htm
[64]:https://web.archive.org/web/20070823234309/http://www.decoder.it/archivio/cybcult/immagina/grafica.htm
