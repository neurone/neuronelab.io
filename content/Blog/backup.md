title: Backup
date: 2015-06-16 20:33:25 +0200
tags: racconti, fantascienza
summary: La fine del terzo conflitto mondiale fu una svolta storica per la civiltà.

La fine del terzo conflitto mondiale fu una svolta storica per la civiltà.

Terminò con il ritorno a casa di migliaia di soldati sotto shock, incapaci
d’imbracciare qualsiasi arma.

Non si sa chi causò la fine. Probabilmente un gruppo di *hacker* o forse solo un
ragazzino.

Già da molti decenni, grazie agli addestramenti che permettevano il necessario
distacco emotivo, combattere era come giocare ad un videogame.

Negli ultimi due anni, tutte le nazioni si dotarono della tecnologia che traduce
la coscienza in un flusso di dati, e viceversa.

Ogni militare aveva un apparecchio per il backup della coscienza e della memoria
collegato al proprio sistema nervoso. In caso di morte, entrambe venivano
inviate ai sistemi di memorizzazione del proprio esercito, che avrebbero
successivamente clonato il soldato mantenendone pressoché intatta l'esperienza.

*Empathy_s-loop*, la botnet pirata, diffusasi sulla quasi totalità dei
terminali, non venne scoperta fino all’attivazione, quando i flussi di backup
dei soldati morenti, vennero deviati in modo inaspettato.

Fu così che tutte le guerre si fermarono. La coscienza di chi stava morendo,
copiata in quella dell’uccisore, improvvisamente all'unisono, capivano di aver
appena ammazzato… se stesso.
