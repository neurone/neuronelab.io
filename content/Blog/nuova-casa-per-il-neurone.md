title: Nuova casa per il neurone
date: 2018-06-17 17:11
tags: blog

Il blog ora è ospitato su [GitLab][gitlab] anziché su GitHub. Qua metterò a disposizione anche i [sorgenti][sorgenti] in markdown degli articoli.
Il vecchio blog verrà tenuto online ancora per un po', in modo che anche i quattro gatti che lo seguono possano aggiornare i propri link e feed reader.

È molto probabile che nei prossimi giorni ci saranno ancora aggiustamenti e che il blog non funzioni al 100%. Ci sto lavorando.

![MS_Love_OSS][microsoft_and_github]

[gitlab]:https://gitlab.com
[sorgenti]:https://gitlab.com/neurone/neurone.gitlab.io
[microsoft_and_github]:{filename}/images/github_ms.jpg
