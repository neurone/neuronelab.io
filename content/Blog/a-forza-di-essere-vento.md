title: A forza di essere vento
date: 2015-06-24 16:21:16 +0200
tags: recensioni, documentari, società, etica
summary: Il documentario è stato prodotto dalla *Editrice A*, ed è ordinabile dal loro sito, dov'è presente anche una scheda descrittiva.

Il documentario è stato prodotto dalla *Editrice A*, ed è ordinabile dal loro sito, dov'è presente anche una [scheda descrittiva][1].

![A forza di essere vento]({filename}/images/a_forza_di_essere_vento.png "Copertina:
A forza di essere vento")

Si possono ascoltare interviste ad alcuni anziani, sopravvissuti ad *Auschwitz*
ed agli esperimenti di [Josef Mengele][2], le spiegazioni dei dati raccolti fino
ad ora da parte di un esperto del [Centro di Documentazione Ebraica
Contemporanea][3], ed altre raccolte dall'[Opera Nomadi][4]. Infine, vi è un
concerto con [Moni Ovadia][5] assieme a musicisti rom, ed uno spettacolo di
lettura e musica anch'esso realizzato da musicisti ed attori rom, sempre in
collaborazione con l'*Opera Nomadi*.

Comunemente, si sa e si parla molto poco di quello che era accaduto agli
zingari.

È diffusa l'idea che essi venissero mandati nei campi di sterminio perché,
secondo i nazisti, antisociali, truffatori e ladri, ma la motivazione principale
era di origini razziali.

Semplificando la questione, sappiamo che per l'[antropologia razziale][6],
adottata e promossa dai nazisti, esiste una razza ariana, di origini
indoeuropee, che secondo loro rappresentava la maggior evoluzione genetica
raggiunta dalla specie umana. In questa teoria andava però in conflitto col
fatto che i [rom][7] ed i [sinti][8] sono [indoeuropei][9] e quindi ariani. Per
conciliare questo con il loro disprezzo per gli zingari, gli scienziati
sostenitori della [pseudoscientifica][10] teoria della razza, decisero che le
caratteristiche che rendevano gli zingari così disprezzabili, erano causate da
una degenerazione, cioè che nell'incrociarsi con gli altri popoli, la loro
purezza era stata contaminata e che la maggior parte di loro aveva perso le
caratteristiche pure originarie. I nazisti ritenevano che la presenza degli
zingari, potesse contaminare anche la società tedesca. <!--more-->

Quindi, se gli ebrei venivano sterminati quanto più puramente ebrei erano, gli
zingari venivano sterminati quanto meno puri erano.

Per questo motivo, tutti venivano inizialmente schedati, misurati, le loro
caratteristiche fisiche confrontate con il modello ariano ed interrogati sul
loro albero genealogico. Come ci si può aspettare, quasi tutti non
soddisfacevano i requisiti nazisti.

Il dottor Mengele prediligeva i bambini (ed in particolare i gemelli) degli
zingari per i suoi crudeli esperimenti, proprio perché originari della stirpe
indoeuropea.

Bisogna precisare che la teoria della razza è in contrasto con la teoria
dell'evoluzione e con tutta la moderna psicologia sociale. Per questo motivo, se
accettassimo la correttezza della teoria della razza, dovremmo abbandonare una
buona parte delle scoperte fatte grazie alla teoria dell'evoluzione e tutto ciò
che nel campo della genetica è stato provato sperimentalmente e nelle teorie
psicologiche che trovano quotidiano riscontro nella realtà. Ci troveremmo quindi
con un gran numero di scoperte e prove, che non sapremmo più come spiegare e
collocare. Per questi motivi, la teoria della razza è scientificamente
inaccettabile.

A questo proposito vi rimando ad una lettura molto interessante. Un articolo
scritto da [Stephen Jay Gould][11], che si intitola
[Human Equality Is a Contingent Fact of History][12]. Ringrazio [Gianluca][13
per la segnalazione).

Gli zingari furono gli unici ad aver organizzato una resistenza all'interno del
campo di concentramento. In particolare, furono le donne degli zingari a
procurarsi armi di fortuna e lottare. Il primo scontro finì a favore degli
zingari, che riuscirono a difendersi. Ma come prevedibile, in seguito, la
pagarono duramente. Una cosa del genere, non si ripeté più. Vennero fatte più
eliminazioni di massa, migliaia di persone alla volta, nelle camere a gas o
direttamente nei forni crematori. Si stima che nella sola Auschwitz vennero
ammazzati circa 23.000 rom e sinti, mentre in tutto il territorio del “reich”
circa mezzo milione.

Anche in Italia, i fascisti iniziarono a [seguire le teorie della razza][14] ed
a collaborare con i nazisti. C'erano infatti campi di prigionia in quasi tutte
le regioni. Spesso, facevano da anticamera per i campi tedeschi. Il più famoso è
un [campo nei pressi di Bolzano][15], dal quale partivano quelli destinati ad
[Auschwitz][16].

È un documentario molto importante perché, se di quanto accaduto agli ebrei si
sa già molto, degli zingari che hanno subito la stessa sorte e lo stesso
trattamento, si sa pochissimo. Ovviamente, non intendo sminuire ciò che è
accaduto agli ebrei, credo però che sia importante avere più informazioni
possibile per potersi avvicinare alla verità. Specialmente quando la mancanza di
questa conoscenza ancora influenza le convinzioni attuali. È importante notare
come attualmente gli zingari si trovino ancora in condizione di emarginazione
sociale e che molto spesso, le motivazioni sono pericolosamente simili a quelle
addotte dai nazisti. Le stesse motivazioni e lo stesso disprezzo verso gli
zingari, inoltre, si estende trasversalmente ai vari schieramenti politici.

[1]:http://www.arivista.org/lo-sterminio-dei-rom
[2]:https://it.wikipedia.org/wiki/Josef_Mengele
[3]:http://www.cdec.it/
[4]:https://it.wikipedia.org/wiki/Opera_Nomadi
[5]:https://it.wikipedia.org/wiki/Moni_Ovadia
[6]:https://it.wikipedia.org/wiki/Razzismo_scientifico
[7]:https://it.wikipedia.org/wiki/Rom_(popolo)
[8]:https://it.wikipedia.org/wiki/Sinti
[9]:https://it.wikipedia.org/wiki/Indoeuropei
[10]:https://it.wikipedia.org/wiki/Pseudoscienza
[11]:https://it.wikipedia.org/wiki/Stephen_Jay_Gould
[12]:http://www.scribd.com/doc/18185121/Human-Equality-Is-a-Contingent-Fact-of-History
[13]:https://some1elsenotme.wordpress.com/
[14]:https://it.wikipedia.org/wiki/Leggi_razziali_fasciste
[15]:https://it.wikipedia.org/wiki/Campo_di_transito_di_Bolzano
[16]:https://it.wikipedia.org/wiki/Campo_di_concentramento_di_Auschwitz
